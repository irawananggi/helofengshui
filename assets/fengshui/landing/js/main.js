(function () {

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 56)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 156
        // offset: 56
    });

    $(window).scroll(function () {
        var sticky = $('.header-nav'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) sticky.addClass('scoller');
        else sticky.removeClass('scoller');
    });

    $('.btn-nav-mobile').click(function (e) {
        e.preventDefault();
        $('.nav-main').toggleClass('show');
        $('.menu-overlay').toggleClass('show');

    });
    $('body').on('click', '.menu-overlay', function () {
        $('.nav-main').removeClass('show');
        $('.menu-overlay').removeClass('show');
    });


})();