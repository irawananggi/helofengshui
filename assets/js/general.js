$(document).ready(function() {
	$('.btn-form').click(function() {
		$('#form-overlay').show();
		$('#box-main').hide();
		$.ajax({
			url: $(this).attr('href'),
			success: function(cont) {
				$('#form-overlay').hide();
				$('#form-content').html(cont);
				$('#form-box').slideDown();
			},error:function(){
				alert('Sistem bermasalah. Hubungi Developer');
			}
		});
		return false;
	});
	$('.btn-tolak').click(function() {	   
		$('.form-delete-msg').html('Anda akan menolak data tersebut !!');
		$('.form-title').html('<i class="fa fa-ban"></i> Tolak');
		$('.form-delete-url').attr('href',$(this).attr('href')).children().html('<i class="fa fa-ban"></i> Ok');
		$('#modal-delete').modal('show');
		return false;
	});
	
	$('.btn-edit').click(function() {
		$('#form-overlay').show();
		$('#box-main').hide();
		$.ajax({
			url: $(this).attr('act'),
			success: function(cont) {
				$('#form-overlay').hide();
				$('#form-content').html(cont);
				$('#form-box').slideDown();
			},error:function(){
				$('#modal-alert').modal('show');

				show_error('Sistem bermasalah. Hubungi Developer');
			}
		});
		return false;
	});

	$('.btn-stop').click(function() {
		$('.form-delete-msg').html('Apakah anda ingin mengakhiri ?');
		$('.form-title').html('<i class="fa fa-check-square-o"></i> Mengakhiri');
		$('.form-delete-url').attr('href',$(this).attr('href')).children().html('<i class="fa fa-check-square-o"></i> OK');
		$('#modal-delete').modal('show');
		return false;
	});

	$('.btn-delete').click(function() {

		var ac = $(this).attr('act');
		var msg = $(this).attr('msg');
		var icn = $(this).attr('t_icn');
		var tl = $(this).attr('t_text');
		var tbtn = $(this).attr('t_btn');
		var ispos = $(this).attr('is_pos');
		$('.form-title').html('<i class="fa '+(icn?icn:'fa-trash')+' fa-btn"></i> '+(tl?tl:'Konfirmasi Hapus'));
		var btn_del = '<i class="fa '+(icn?icn:'fa-trash')+' fa-btn"></i> '+(tbtn?tbtn:(tl?tl:'Hapus'));
		$('.form-delete-msg').html(msg);
		if(ispos){
			if (ac) $('#form_modal_dialog').attr('action',$(this).attr('act')).attr('method','POST');
			else $('.form-delete-url').attr('href',$(this).attr('href')).children().html(btn_del).show();
			$('.form-delete-url').children().html(btn_del).prop('type', 'submitform').show();
			$('#modal-delete').modal('show').removeAttr('tabindex');               
		}else{
			if (ac) $('.form-delete-url').attr('href',$(this).attr('act')).children().html(btn_del).show();
			else $('.form-delete-url').attr('href',$(this).attr('href')).children().html(btn_del).show();
			$('#modal-delete').modal('show');
		}

		return false;
	});

	$('.btn-back').click(function() {
		var ac = $(this).attr('act');
		var msg = $(this).attr('msg');
		var icn = $(this).attr('t_icn');
		var tl = $(this).attr('t_text');
		$('.form-title').html('<i class="fa '+(icn?icn:'fa-check-square-o')+' fa-btn"></i> '+(tl?tl:'Konfirmasi'));
		var btn_del = '<i class="fa '+(icn?icn:'fa-check-square-o')+' fa-btn"></i> '+(tl?tl:'Set');
		$('.form-delete-msg').html(msg);
		if (ac) $('.form-delete-url').attr('href',$(this).attr('act')).children().html(btn_del).show();
		else $('.form-delete-url').attr('href',$(this).attr('href')).children().html(btn_del).show();
		$('#modal-delete').modal('show');
		return false;
	});

	$('.btn-off').click(function() {

		$('.form-delete-msg').html('Apakah anda ingin mengakhiri pemakaian aplikasi?');
		$('.form-title').html('<i class="fa fa-power-off fa-btn"></i> Logout');
		$('.form-delete-url').attr('href',$(this).attr('href')).children().html('<i class="fa fa-power-off"></i> &nbsp; Logout').show();
		$('#modal-delete').modal('show');
		return false;
	});

	$('.btn-check').click(function() {
		var msg = $(this).attr('msg');
		$('.form-delete-msg').html(msg);
		$('.form-title').html('<i class="fa fa-check-square-o"></i> &nbsp; Konfirmasi');
		$('.form-delete-url').attr('href',$(this).attr('act')).children().html('<i class="fa fa-check-square-o"></i>  &nbsp;  Ya').show();
		$('#modal-delete').modal('show');
		return false;
	});

	$('.btn-profil').click(function() {
		$.ajax({
			url: $(this).attr('href'),
			cache: false,
			success: function(msg) {
				$('#modal-profil').html(msg);
				$('#load-profil').hide();
				
			},error:function(error){
				$('#load-profil').html('<i class="fa fa-error"></i> ERROR : '+error).show();
			}
		});
		$('#modal-profil').modal('show');
		return false;
	});

	$('.cek-all').click(function() {
		if ($(this).is(':checked')) {
			$('.cek').prop('checked', true);
			$('.btn-delete-all').show();
		} else {
			$('.cek').prop('checked', false);
			$('.btn-delete-all').hide();
		} 
	});

	$('.cek').click(function(){
		var ju = 0;
		$('.cek').each(function() { if($(this).is(':checked')) ju+=1; });

		if (ju > 0) $('.btn-delete-all').show();             
		else $('.btn-delete-all').hide();
	});

	$('.btn-delete-all').click(function() {

		$('.form-delete-msg').html('Apakah ingin menghapus item yang tercentang?<br>Aksi ini tak dapat dibatalkan!');
		$('.form-delete-url').hide();
		$('.form-delete-btn').show();
		$('#modal-delete').modal('show');
		return false;

	});

	$('.form-delete-btn').click(function() {

		$('#form_delete').submit();

	});

	
});

function show_error(e) {
	$(document).ready(function() {
		$('.alert-dialog').removeClass('alert-success');
		$('.alert-dialog').addClass('alert-danger');
		$('.alert-icon').html('<i class="icon fa fa-ban"></i> Kesalahan!');
		$('.alert-message').html(e);
		$('#modal-alert-btn').removeClass('btn-success').addClass('btn-danger');
		$('#modal-alert').modal('show');
		
	});
}

function show_message(e) {
	$(document).ready(function() {
		
		$('.alert-dialog').removeClass('alert-danger');
		$('.alert-dialog').addClass('alert-success');
		$('.alert-icon').html('<i class="icon fa fa-check"></i> Pesan');
		$('.alert-message').html(e);
		$('#modal-alert-btn').removeClass('btn-danger').addClass('btn-success');
		$('#modal-alert').modal('show');
		
	});
}


function setExpiration(cookieLife){
	var today = new Date();
	var expr = new Date(today.getTime() + cookieLife * 24 * 60 * 60 * 1000);
	return  expr.toGMTString();
}

function numberToCurrency(a){
	if (parseInt(a) < 0) d = 1;
	else d = 0;
	if(a!=''&&a!=null){
		a=a.toString();       
		var b = '';
		if (a == '-') b = a.replace(/[^\d\,\-]/g,'');
		else b = a.replace(/[^\d\,]/g,'');
		var dump = b.split(',');
		var c = '';
		var lengthchar = dump[0].length;
		var j = 0
		for (var i = lengthchar; i > 0; i--) {
			
			j = j + 1;
			if (((j % 3) == 1) && (j != 1)) c = dump[0].substr(i-1,1) + '.'+ c;
			else c = dump[0].substr(i-1,1) + c;
		}
		
		if(dump.length>1){
			if(dump[1].length>0){
				c += ','+dump[1];
			}else{
				c += ',';
			}
		}

		if (d == 1) return '-' + c;
		else return c;
	} else {
		return '';
	}
}


function formatNumber(obj) {
	var a = obj.value;
	obj.value = numberToCurrency(a);
}

function number_format(number, decimals, dec_point, thousands_sep) {
	number = (number + '')
	.replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
	s = '',
	toFixedFix = function(n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k)
		.toFixed(prec);
	};
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
	.split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '')
		.length < prec) {
		s[1] = s[1] || '';
	s[1] += new Array(prec - s[1].length + 1)
	.join('0');
}
return s.join(dec);
}

/* -- Cookies -- */


function getCookie(w){
	cName = "";
	pCOOKIES = new Array();
	pCOOKIES = document.cookie.split('; ');
	for(bb = 0; bb < pCOOKIES.length; bb++){
		NmeVal  = new Array();
		NmeVal  = pCOOKIES[bb].split('=');
		if(NmeVal[0] == w){
			cName = unescape(NmeVal[1]);
		}
	}
	return cName;
}

function printCookies(w){
	cStr = "";
	pCOOKIES = new Array();
	pCOOKIES = document.cookie.split('; ');
	for(bb = 0; bb < pCOOKIES.length; bb++){
		NmeVal  = new Array();
		NmeVal  = pCOOKIES[bb].split('=');
		if(NmeVal[0]){
			cStr += NmeVal[0] + '=' + unescape(NmeVal[1]) + '; ';
		}
	}
	return cStr;
}

function setCookie(name, value, expires, path, domain, secure){
	cookieStr = name + "=" + escape(value) + "; ";
	
	if(expires){
		expires = setExpiration(expires);
		cookieStr += "expires=" + expires + "; ";
	}
	if(path){
		cookieStr += "path=" + path + "; ";
	}
	if(domain){
		cookieStr += "domain=" + domain + "; ";
	}
	if(secure){
		cookieStr += "secure; ";
	}
	
	document.cookie = cookieStr;
}

function setExpiration(cookieLife){
	var today = new Date();
	var expr = new Date(today.getTime() + cookieLife * 24 * 60 * 60 * 1000);
	return  expr.toGMTString();
}


function contentloader(urldesire,contentbox){
	$.ajax({
		url: urldesire,
		cache: false,
		success: function(msg) {
			$(contentbox).html(msg);
		},error:function(error){
			show_error(error);
		}
	});
}
function contentjson(urldesire,contentbox, attr){
	$.ajax({
		url: urldesire,
		dataType: 'JSON',
		cache: false,
		success: function(msg) {
			$(contentbox).attr(attr, msg);
		},error:function(error){
			show_error(error);
		}
	});
}

function din_combo(url, to){
	$.ajax({
		url:url,
		success:function(data){
			$('#' + to).empty('');
			$.each(JSON.parse(data), function(key, val){
				$('#' + to).append('<option value="' + val.key + '">' + val.val + '</option>');
             //   alert(val.key+val.val);
         });
		},
		error:function(){
			alert('sistem bermasalah..');
		}
	});
}


// select2 combo
function global_select(kelas, additionalParam={}, onSel=null,incVal=false, toStr=false){
		// console.log(kelas);
		// console.log($('.'+kelas));
		$("."+kelas).select2({
			ajax: {
				url: $('.'+kelas).data('url'),
				dataType: "JSON",
		    // delay: 250,
		    beforeSend:function(){
		    	// console.log($('.select-kunkom').data('url'));
		    },
		    data: function(params) {
		    	var defaultParam = {
		    		combo: true,
		    		term: params.term || '',
		    		page: params.page || 1,
		    	};

		    	let returnParam = defaultParam;
	        	// console.log(additionalParam.length);

	        	if(additionalParam.length > 0){ //[{key:, val:}]
	        		var rObj = [];
	        	additionalParam.forEach(function(item){
	        		rObj[item.key] = $('.'+item.val).val();
	        	});
	        	returnParam = Object.assign(defaultParam, rObj);
					// console.log(returnParam);
				}

				// console.log(returnParam);
				return returnParam;
	            // return {
	            //     term: params.term || '',
	            //     page: params.page || 1,
	            // }
	        },
	        processResults: function (data, params) {
	        	let {page} = params;
	        	let {limit,results, pagination} = data;
	        	page = page || 1;
			    // console.log(data.count);
			    var mapdata = $.map(results, function (obj) {      
			    	obj.id = obj.id;
			    	obj.text = obj.text;
			    	return obj;
			    });
			    // console.log(pagination)

			    return {
			    	results: mapdata,
			    	pagination
			        // pagination
			    };
			},
			cache: true
		},
		  // data: [{id: 3, text: '14'}], //set default value 
		  minimumInputLength: 0,
		  templateSelection: myCustomTemplate
		}).on('select2:select', function(e){
				// var $emptyOption = $("<option></option>").val('').text('-Kosongkan-');
				var $newOption = $("<option selected='selected' style=z-index:99999 !important;'></option>").val(e.params.data.id).text(e.params.data.text);
				// $("."+kelas).html($emptyOption);
				$("."+kelas).append($newOption).trigger('change');
				
				if(onSel){
					//alerts "Hello World!" (from within bar AFTER being passed)
					// console.log(e);
					if(incVal) {
						if(toStr) $("."+kelas).attr('func',onSel+'("'+e.params.data.id+'")');
						else $("."+kelas).attr('func',onSel+'('+e.params.data.id+')');
					}else{
						$("."+kelas).attr('func',onSel+'()');
					}
					let newFunc = new Function($("."+kelas).attr('func'));
					newFunc(e);
				}

			});
	}

	function myCustomTemplate(item) {
		return item.text;
	}
// ./select2 combo

function isNull() {
	for (var i = 0; i < arguments.length; i++) {
		if (
			typeof arguments[i] !== 'undefined'
			&& arguments[i] !== undefined
			&& arguments[i] != null
			&& arguments[i] != NaN
			&& arguments[i] 
			) return arguments[i];
	}
}

// report

$(function(){
	$('.btn-print').unbind('click', function(e){
		e.preventDefault();
		printReport('cetak-area');
	});
})

function printReport(divID) {
	var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
	disp_setting+="scrollbars=yes,width=1024, height=550, left=50, top=25"; 
	var contet_disp = `<style type="text/css">
	@media print {
		nav, .btn, .no-print {
			display: none;
		}
	}
	table {
		width: 100% !important;
		border-collapse: collapse;
		margin-bottom: 5px;
	}

	table, th, td {
		border: 1px solid black;
	}

	.printhead-instansi {
		flex-direction: column;
		align-content: center;
		flex-wrap: wrap;
	}
	.printhead-instansi > p {
		text-align:center;
		font-size:18pt;
		font-weight: bold;
	}
	.printhead-instansi > hr {
		border-top: 3px solid;
		margin-bottom:25px !important;
	}

	</style>`;

	contet_disp += $('.print-head').html();
	contet_disp += document.getElementById(divID).innerHTML;

	var w = window.open("","", disp_setting);

		w.document.write(contet_disp); //only part of the page to print, using jquery
		w.document.close(); //this seems to be the thing doing the trick
		w.focus();
		w.print();
		w.close();
	}
// ./report


// tinymce:
function initMce(par={}){
	console.log(par.menu);
	tinymce.init({
		selector: '.text-area',
		skin: 'oxide',
            // width: '100%',
            height: par.height || 450,
            plugins: par.plugins || [
            'noneditable advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table directionality emoticons template paste',
            'noneditable lorumipsum',
            ],

            contextmenu: par.contextmenu || "link inserttable | cell row column deletetable",
            // content_css: '../../../assets/plugins/tinymce/js/tinymce/skins/content/writer/content.min.css',
            content_css: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/skins/content/writer/content.min.css',
            toolbar: par.toolbar || 'link image insertfile undo redo lorumipsum | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            // menubar: par.menubar || "file edit insert view format table tools help",
            menubar: par.menu || "table tools view",
            // menubar: "table tools view",
            // menubar:false,
    		statusbar: false,
            
            force_p_newlines : false,
            force_br_newlines : true,
            convert_newlines_to_brs : false,
            remove_linebreaks : true,
            /* enable title field in the Image dialog*/
            image_title: true,
            /* enable automatic uploads of images represented by blob or data URIs*/
            automatic_uploads: true,
		  /*
		    URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
		    images_upload_url: 'postAcceptor.php',
		    here we add custom filepicker only to Image dialog
		    */
		    file_picker_types: 'image',
		    /* and here's our custom image picker*/
		    file_picker_callback: function (cb, value, meta) {
			    	var input = document.createElement('input');
			    	input.setAttribute('type', 'file');
			    	input.setAttribute('accept', 'image/*');

			    /*
			      Note: In modern browsers input[type="file"] is functional without
			      even adding it to the DOM, but that might not be the case in some older
			      or quirky browsers like IE, so you might want to add it to the DOM
			      just in case, and visually hide it. And do not forget do remove it
			      once you do not need it anymore.
			      */

			      input.onchange = function () {
			      	var file = this.files[0];

			      	var reader = new FileReader();
			      	reader.onload = function () {
			        /*
			          Note: Now we need to register the blob in TinyMCEs image blob
			          registry. In the next release this part hopefully won't be
			          necessary, as we are looking to handle it internally.
			          */
			          var id = 'blobid' + (new Date()).getTime();
			          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
			          var base64 = reader.result.split(',')[1];
			          var blobInfo = blobCache.create(id, file, base64);
			          blobCache.add(blobInfo);

			          /* call the callback and populate the Title field with the file name */
			          cb(blobInfo.blobUri(), { title: file.name });
			      };
			      reader.readAsDataURL(file);
			  };

			  input.click();
			},
			// paste_data_images: false,

});
}


var blobURL;
var fileName;
// Convert a base64 string into a binary Uint8 Array 
// https://gist.github.com/borismus/1032746
function convertDataURIToBinary(dataURI) {
	var BASE64_MARKER = ';base64,';
	var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
	var base64 = dataURI.substring(base64Index);
	var raw = window.atob(base64);
	var rawLength = raw.length;
	var array = new Uint8Array(new ArrayBuffer(rawLength));

	for(i = 0; i < rawLength; i++) {
		array[i] = raw.charCodeAt(i);
	}
	return array;
}

// File Reader
// https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
function readFile(evt) {
    var f = evt.target.files[0]; 

    if (f) {
		if ( /(jpe?g|png|gif)$/i.test(f.type) ) {
			var r = new FileReader();
			r.onload = function(e) { 
				var base64Img = e.target.result;
				var binaryImg = convertDataURIToBinary(base64Img);
				var blob = new Blob([binaryImg], {type: f.type});
				blobURL = window.URL.createObjectURL(blob);
				fileName = f.name;
				document.getElementById('nameImg').value = fileName;
				document.getElementById('typeImg').value = f.type;
				document.getElementById('sizeImg').value = f.size;
				document.getElementById('base64Url').value = base64Img;
				document.getElementById('blobUrl').value = blobURL;
				document.getElementById('base64Img').src = base64Img;
				document.getElementById('blobImg').src = blobURL;
				document.getElementById('binaryImg').innerHTML = JSON.stringify(binaryImg, null, 2);
			}
			r.readAsDataURL(f);
		} else { 
			alert("Failed file type");
		}
    } else { 
		alert("Failed to load file");
    }
}