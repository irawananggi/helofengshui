'use strict';
var ajaxProc= false;
let bobot_pil= 0;
let bobot_ess= 0;

var tableRiw = $('#tab-riwayat-ujian');
$(document).ready(function(){
	loadResult();
	tableRiw.find('thead').after(`<tbody></tbody>`);

	$(function(){
		tableRiw.find('tbody').ready(function(){
			loadRiwayat();
		});
	})
});

function loadResult(){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/result_test',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val()},
	})
	.done(function(res) {
		// console.log(res);
		if(res.status == 'success'){
			const {result, jadwal} = res;
			let soal_dijawab =0;
			let soal_tidak_dijawab =0;
			let total_nilai =0;
			let nilai_max =jadwal.nilai_max;
			bobot_pil= jadwal.bobot_pil;

			$('.capt-pilihan-benar').text(0);
			$('.capt-pilihan-salah').text(0);
			$('.capt-nilai-pil').text(0);

			$('.capt-ess-dijawab').text(0);
			$('.capt-ess-tidak-dijawab').text(0);
			$('.capt-nilai-ess').text(0);
			
			$.map(result, function(item, index) {
				// return something;
				soal_dijawab += item.total_jawab;
				soal_tidak_dijawab += item.tidak_jawab;

				if(item.pilihan == 1){
					$('.capt-pilihan-benar').text(item.pilihan_benar);
					$('.capt-pilihan-salah').text(item.pilihan_salah);
					$('.capt-nilai-pil').text(item.nilai_pil);
					total_nilai += item.nilai_pil;
				}else{
					$('.capt-ess-dijawab').text(item.total_jawab);
					$('.capt-ess-tidak-dijawab').text(item.tidak_jawab);
					$('.capt-nilai-ess').text(item.nilai_ess == 0 ? 'Menunggu Penilaian' : item.nilai_ess);
					total_nilai += item.nilai_ess;
				}
 
			});
			$('.capt-dijawab').text(soal_dijawab);
			$('.capt-tidak-dijawab').text(soal_tidak_dijawab);
			$('.capt-total').text(total_nilai);
			let skor_akhir = total_nilai/nilai_max*100;
			// console.log(nilai_max);
			// console.log(total_nilai);
			$('.capt-skor-akhir').html(skor_akhir.toFixed(2));
			updateSkor(skor_akhir);
		}
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}

function updateSkor(n){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/update_skor',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), n: n},
	})
	.done(function(res) {
		console.log("update skor success");
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
	
}

function loadRiwayat(page){
	if(ajaxProc) return;
	ajaxProc = true;
	page  = page || 1;
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/soal',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), page: page, limit: 10},
		beforeSend: function(){
			tableRiw.find('tbody').children().remove();
			$('.loading-table').removeClass('hide');
		},
	})
	.done(function(res) {
		$('.loading-table').addClass('hide');
		tableRiw.find('tbody').children().remove();
		if(res.status=='success'){
			const {result,offset,total,limit, jawabs} = res;
			let {page} = res; 
			let no=1+offset;
			let jawaban;
			let question = {};
			let answer = {};
			let kunciEss = {};
			$.each(result,function(idx, item){
                        const {soal} = item;
                        let pils = {1:'Pilihan', 0:'Essay'};
                        let options = {'':'',1:'A',2:'B',3:'C',4:'D',5:'E',6:'F',7:'G',8:'H'};
                        let par_aksi = {
                        	'id':item.id
                        };
                        let encode_par = item.param;
                        let link_soal;
                        let kunci;
                        jawaban = jawabs[item.id] ?  jawabs[item.id].jawab : '';
                        let optColor = 'text-danger';
                        let optIcon = ' <small>(<i class="fas fa-times"></i>)</small>';
                        let bobot_jawab = 0;

                        if(jawabs[item.id]){
                        	if( jawabs[item.id].jawab == item.kunci){
                        		optColor =  'text-success';
                        		optIcon =  ' <small>(<i class="fas fa-check"></i>)</small>';
                        	}
                        	if(item.pilihan == 0) answer[item.id] = jawabs[item.id].jawab;
                        }

                        if(item.pilihan == 1){
                        	jawaban = `<b class="`+optColor+`">`+optIcon+`<b>`;
                        }else if(item.pilihan == 0){
                        	jawaban = `<a href="#" data-toggle="modal" data-target="#msgModal" data-tipe="a" data-id="`+item.id+`" title="klik untuk melihat isi">(...)</a>`;
                        	kunciEss[item.id] = item.kunci;
                        }

                        kunci = (item.pilihan == 1 ? options[item.kunci] : item.kunci.length > 0 ? `<a href="#" data-toggle="modal" data-target="#msgModal" data-tipe="k" data-id="`+item.id+`" title="klik untuk melihat isi">(...)</a>` : `-`);

                        switch (item.tipe) {
                        	case 'teks':
                        	question[item.id] = soal.pertanyaan;
                        	break;
                        	case `vid_upload`:
                        	question[item.id] = `<iframe style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></iframe>`;
                        	break;
                        	case `pdf`:
                        	question[item.id] = `<iframe style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></iframe>`;
                        	break;
                        	case `vid_link`:
                        	question[item.id] = `<iframe width="100%" height="650" src="`+soal.pertanyaan+`" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                        	break;
                        	case `link`:
                        	question[item.id] = `<iframe is="x-frame-bypass" style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+soal.pertanyaan+`"></iframe >`;
                        	break;
                        	case `gambar`:
                        	question[item.id] = `<image width="450" src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></image>`;
                        	break;
                        	default:
                        	question[item.id] = soal.pertanyaan;
                        	break;
                        }

                        if(item.pilihan == 1){
                        	let nOpt = 1;
                        	let optCapt = `<ul style="list-style-type: none;">`;
							$.map(soal.pilihan, function(pil, ipil) {
								let checked = 'fa-circle-o';
								let optClass = '';
								if(item.kunci && item.kunci == nOpt) optClass = 'bg-gradient-success rounded-lg';
								if(jawabs[item.id] && nOpt == jawabs[item.id].jawab) checked = 'fa-circle';
								optCapt += `<li class="d-flex flex-row `+optClass+`">`;
								// optCapt += `<input `+checked+` class="radio-options" type="radio" name="jawab" value="`+nOpt+`" disabled>`;
								optCapt += `<i class="fa `+checked+`" style="margin-top: 6px;margin-right: 3px;"></i>`;
								optCapt += `<b class="mr-2">`+ipil+`.</b>`;
								optCapt += `<div>`+pil+`</div>`;
								optCapt += `</li>`;
								nOpt++;
							});
							optCapt += `</ul>`;

                        	question[item.id] += '<p style="font-weight: 400;">Jawab:</p>';
							question[item.id] += optCapt;

							if(jawabs[item.id] && item.kunci == jawabs[item.id].jawab) bobot_pil > 0 ? bobot_jawab = bobot_pil : bobot_jawab = item.bobot;
                        }else{

                        	question[item.id] += '<p style="font-weight: 400;">Kunci:</p>';
                        	question[item.id] += `<ul style="list-style-type: none;" class="bg-gradient-light"><li>`+item.kunci+`</li></ul>`;

                        	let optCapt = `<ul style="list-style-type: none;">`;
                        	let jwbEss = '';
                        	if(jawabs[item.id]) jwbEss = jawabs[item.id].jawab;
							optCapt += `<li>`+jwbEss+`</li>`;
							optCapt += `</ul>`;
                        	question[item.id] += '<p style="font-weight: 400;">Jawab:</p>';
							question[item.id] += optCapt;

							if(jawabs[item.id]) bobot_jawab = jawabs[item.id].bobot;
                        }

                        link_soal = `<a href="#" data-toggle="modal" data-target="#msgModal" data-tipe="q" data-id="`+item.id+`">Lihat Soal</a>`;

                        tableRiw.find('tbody').append(`
                        	<tr>
                        	<td>`+no+`</td>
                        	<td>`+pils[item.pilihan]+`</td>
                        	<td>`+item.tipe+`</td>
                        	<td style="white-space:nowrap;">`+link_soal+`</td>
                        	<td style="white-space:nowrap;">`+kunci+`</td>
                        	<td style="white-space:nowrap;">`+jawaban+`</td>
                        	<td>`+bobot_jawab+`</td>
                        	</tr>
                        	`);
                        no++;
                    });

			$('#msgModal').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var recipient = button.data('id') // Extract info from data-* attributes
				  var tipe = button.data('tipe') // Extract info from data-* attributes
				  var modal = $(this)
				  var modalContent;
				  if(tipe == 'q'){
				  	modal.find('.modal-body').html(question[recipient])
				  }else if(tipe == 'a'){
				  	modal.find('.modal-body').html(answer[recipient])
				  }else if(tipe =='k'){
				  	modal.find('.modal-body').html(kunciEss[recipient])
				  }
				  
				});

			let pag = new Pagination('loadRiwayat', total, limit, page);
			pag.init();

			// scroll to bottom;
			// if(page > 1) window.scrollTo(0,document.querySelector("#tab-riwayat-ujian").scrollHeight);

		}else if(res.status=='failed'){
			tableRiw.find('tbody').children().remove();
		}
		ajaxProc = false;
	})
.fail(function() {
	console.log("error");
})
.always(function() {
	console.log("complete");
});

}


$(function(){
	$('#msgModal').on('hidden.bs.modal', function (e) {
			   var modal = $(this);
			   modal.find('.modal-body').html('');
			})
})