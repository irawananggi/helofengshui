'use strict';
var ajaxProc= false;
let batas_waktu = 0;//menit
let total_detik = 0;
let sisa_detik = 0;

let next = 0;
let more_next = false;
let captJawab = false;
let curPage = 0;
let totalSoal = 0;
let bobot_ess = 0;

$(document).ready(function(){
	batas_waktu = $('input[name=durasi]').val();
	loadSoal();
	$('.info-test').removeClass('hide');
	loadInfo();
	soalNav();
	start_timer();
	// countJawab();
});

function start_timer(){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/start_timer',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), 'batas_waktu': batas_waktu},
	})
	.done(function(res) {
		// console.log("success");
		// console.log(res);
		if(res.status == 'success'){
			const {sisa_waktu} = res;
			total_detik = res.total_detik;
			sisa_detik = sisa_waktu;
			timer();
		}
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
	
}

function loadInfo(){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/info',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val()},
	})
	.done(function(res) {
		// console.log("success");
		const {id_peserta,nama,no_peserta} = res.data;
		$('.info-test .nama').html(nama);
		$('.info-test .no_peserta').html(no_peserta);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}

function soalNav(page){
	page = page || 1;
	var navContent = $('.soal-nav .caption');
	// var ctSoal = 300;
	$.ajax({
		url: $('input[name=site_url]').val()+`cbt_latihan/request/init_soal`,
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), page: page},
	})
	.done(function(res) {
		const {status} = res;
		if(status=='success'){
			const {ids, offset, more, jawabs, total} = res;
			// total soal:
			totalSoal = total;
			$('.jml_soal').html(totalSoal+` Soal`);
			if(page==1) navContent.html('');
			let no = 1+offset;
			let jwb;
			let jwbAlpha = {0:'', 1:'a', 2:'b', 3:'c', 4:'d',5:'e',6:'f',7:'g',8:'h',9:'i'};
			$.map(ids, function(soalId, isoalId) {
				// console.log(jawabs[soalId]);
				// let btClass = 'btn-default';
				if(jawabs[soalId]) jwb = jawabs[soalId].pilihan == 1 ? `(`+jwbAlpha[jawabs[soalId].jawab]+`)` : '(..)'; else jwb = '';
				// if(jawabs[soalId]) btClass = 'btn-success';
				navContent.append(`<div class="col-4"><button data-page=`+no+` class="bt-nav-soal btn btn-xs btn-default mb-2 capt-soal-`+no+`" style="width:46px;">`+no+`<span class="capt-jawab">`+jwb+`</span></button></div>`);
				if(jawabs[soalId]){
					$('.soal-nav .caption .capt-soal-'+no).removeClass('btn-default');
					$('.soal-nav .caption .capt-soal-'+no).addClass('btn-success');
				}else{
					$('.soal-nav .caption .capt-soal-'+no).removeClass('btn-success');
					$('.soal-nav .caption .capt-soal-'+no).addClass('btn-default');
				}
				no++;

			});
			if(more) soalNav(page+1);
			// setActive(curPage);
			$('.bt-nav-soal').unbind('click').on('click', function(){
				loadSoal($(this).data('page'));
			})
		}
		setActive(curPage);
		countJawab();
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
}
function updateNav(page){
	page = page || 1;
	var navContent = $('.soal-nav .caption');
	// var ctSoal = 300;
	$.ajax({
		url: $('input[name=site_url]').val()+`cbt_latihan/request/init_soal`,
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), page: page},
	})
	.done(function(res) {
		const {status} = res;
		if(status=='success'){
			const {ids, offset, more, jawabs, total} = res;
			// total soal:
			totalSoal = total;
			$('.jml_soal').html(totalSoal+` Soal`);
			// if(page==1) navContent.html('');
			let no = 1+offset;
			let jwb;
			let jwbAlpha = {0:'', 1:'a', 2:'b', 3:'c', 4:'d',5:'e',6:'f',7:'g',8:'h',9:'i'};

			$.map(ids, function(soalId, isoalId) {
				// console.log(jawabs[soalId]);
				// let btClass = 'btn-default';
				if(jawabs[soalId]) jwb = jawabs[soalId].pilihan == 1 ? `(`+jwbAlpha[jawabs[soalId].jawab]+`)` : '(..)'; else jwb = '';
				// if(jawabs[soalId]) btClass = 'btn-success';
				// navContent.append(`<div class="col-4"><button data-page=`+no+` class="bt-nav-soal btn btn-xs btn-default mb-2 capt-soal-`+no+`" style="width:46px;">`+no+`<span class="capt-jawab">`+jwb+`</span></button></div>`);
				navContent.find('.capt-soal-'+no+' .capt-jawab').html(jwb);
				if(jawabs[soalId]){
					navContent.find('.capt-soal-'+no+' .capt-jawab').text(jwb);
					navContent.find('.capt-soal-'+no).removeClass('btn-default');
					navContent.find('.capt-soal-'+no).addClass('btn-success');
				}else{
					navContent.find('.capt-soal-'+no).removeClass('btn-success');
					navContent.find('.capt-soal-'+no).addClass('btn-default');
				}
				no++;

			});
			if(more) updateNav(page+1);
			// if(more) more_next = true;else more_next=false;
			// setActive(curPage);
			/*$('.bt-nav-soal').unbind('click').on('click', function(){
				loadSoal($(this).data('page'));
			})*/
		}
		setActive(curPage);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
}

function setActive(current){
	$('.soal-nav .caption').find('.bt-nav-soal').removeClass('btn-info');
	$('.soal-nav .caption').find('.bt-nav-soal:not(:has(.btn-success))').addClass('btn-default');
	$('.soal-nav .caption .btn-success').removeClass('btn-default');

	$('.soal-nav .caption .capt-soal-'+current).removeClass('btn-default');
	$('.soal-nav .caption .capt-soal-'+current).addClass('btn-info');

	// if(!$('.soal-nav .caption').hasClass('.capt-soal-'+current)) $('.soal-nav .caption').find('.bt-nav-soal').addClass('btn-default');
}

function timer(){
	var redir = $('input[name=site_url]').val()+'cbt_latihan/request/test_result/'+$('input[name=param]').val();
	$('.batas_waktu').html(batas_waktu+` Menit.`);
	var tm = new easytimer.Timer({ countdown: true });
	let target = total_detik;
	tm.start({ precision: 'seconds', startValues: { seconds:sisa_detik } });
	if(sisa_detik > 0){
		tm.addEventListener('secondsUpdated', function (e) {
			$('.task-tm').html(tm.getTimeValues().toString());
			// $('.task-tm .days').html(tm.getTotalTimeValues().days);
			// $('.task-tm .hours').html(tm.getTotalTimeValues().hours);
			// $('.task-tm .minutes').html(tm.getTotalTimeValues().minutes);
			// $('.task-tm .seconds').html(tm.getTotalTimeValues().seconds);
			// $('.task-tm .secondTenths').html(tm.getTotalTimeValues().secondTenths);
			// if(tm.getTotalTimeValues().seconds == 0) console.log('ciluk Baaaaaaa!');
			if(tm.getTotalTimeValues().seconds == 0) window.location.href = redir;
			// if()
		});
	}else{
		// console.log('Ujian sudah lewat !');
		window.location.href = redir;
	}
	
	// tm.addEventListener('targetAchieved', function (e) {
	// 	console.log('KABOOM!!');

	// });
}

function loadSoal(page){
	page = page || 1;
	curPage = page;
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/soal',
		type: 'GET',
		dataType: 'json',
		data: {page: page, param: $('input[name=param]').val()},
	})
	.done(function(res) {
		if(res.status == 'success'){
			const {result, offset, more_page, jawabs} = res;
			var preview = '';
			$.map(result, function(item, index) {
				const {soal, pilihan} = item;
				preview += `<ul style="list-style-type: none;margin-left:-28px;">`;
				preview += `<li class="d-flex">`;
				preview += `<span class="mr-2 float-left">`+( offset+1 )+`. </span>`;
				// console.log(soal.pertanyaan);
				switch (item.tipe) {
					case 'teks':
					preview += `<div class="row">`+soal.pertanyaan+`</div>`;
					break;
					case `vid_upload`:
					preview += `<iframe style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></iframe>`;
					break;
					case `pdf`:
					preview += `<iframe style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></iframe>`;
					break;
					case `vid_link`:
					preview += `<iframe width="100%" height="650" src="`+soal.pertanyaan+`" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
					break;
					case `link`:
					preview += `<iframe is="x-frame-bypass" style="width:100%;height:650px;" frameborder="0" allowfullscreen src="`+soal.pertanyaan+`"></iframe >`;
					break;
					case `gambar`:
					preview += `<image width="450" src="`+$('input[name=base_url]').val()+'uploads/soal/'+soal.pertanyaan+`"></image>`;
					break;
					default:
					preview += `<div class="row">`+soal.pertanyaan+`</div>`;

					break;
				}

				preview += `</li>`;
				preview += `</ul>`;
				preview += `<h6 class="ml-3 text-md font-italic">Jawaban:</h6>`;

				preview += `<input name="id_soal" type="hidden" value="`+item.id_soal+`">`;

				preview += `<ul style="list-style-type: none;">`;
				if(pilihan == 1){
					let nOpt = 1;
					$.map(soal.pilihan, function(pil, ipil) {
						let checked = false;
						if(jawabs[item.id] && nOpt == jawabs[item.id].jawab) checked = 'checked';
						preview += `<li class="d-flex flex-row">`;
						preview += `<input `+checked+` class="radio-options" type="radio" name="jawab" value="`+nOpt+`">`;
						preview += `<b class="mr-2">`+ipil+`.</b>`;
						preview += `<div>`+pil+`</div>`;
						preview += `</li>`;
						nOpt++;
					});
					captJawab = true;
				}else{
					let jawabText = '';
					if(jawabs[item.id]) jawabText = jawabs[item.id].jawab;

					preview += `<textarea id="jawab" name="jawab" class="form-control text-area">`+jawabText+`</textarea>`;
					captJawab = false;
				}
				preview += `</ul>`;
				$('.soal').html(preview);
				if(pilihan == 0) tinymce.remove();
				if(pilihan == 0) initMce({height:200,plugins:['table image lorumipsum'], contextmenu:false, toolbar:'image lorumipsum', menu:'table tools view'});
			});
			if(more_page) next = page+1;else next = page;
			if(more_page) more_next = true;else more_next = false;

			if(more_next == false) {
				$('.bt-next-soal').addClass('hide');
				$('.bt-save-soal').html(`<i class="fa fa-save mr-1"></i> Simpan`);

			}else{
				$('.bt-save-soal').removeClass('hide');
				$('.bt-next-soal').removeClass('hide');
				$('.bt-save-soal').html(`<i class="fa fa-save mr-1"></i> Simpan & Lanjutkan`);
			}
			$('.bt-save-soal').removeClass('hide');

		}
		setActive(curPage);
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});

}

function countJawab(){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/countJawab',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val()},
	})
	.done(function(res) {
		// console.log("success");
		let totalDijawab = res;
		let belumJawab = parseInt(totalSoal)  - parseInt(totalDijawab);
		$('.dijawab').html(totalDijawab);
		$('.belum_dijawab').html(belumJawab);
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
	
}

function loadResult(){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/result_test',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val()},
	})
	.done(function(res) {
		// console.log(res);
		if(res.status == 'success'){
			const {result, jadwal} = res;
			let soal_dijawab =0;
			let soal_tidak_dijawab =0;
			let total_nilai =0;
			let nilai_max =jadwal.nilai_max;
			bobot_ess = jadwal.bobot_ess;
			$.map(result, function(item, index) {
				// return something;
				soal_dijawab += item.total_jawab;
				soal_tidak_dijawab += item.tidak_jawab;

				if(item.pilihan == 1){
					total_nilai += item.nilai_pil;
				}else{
					total_nilai += item.nilai_ess;
				}

			});
			let skor_akhir = total_nilai/nilai_max*100;
			updateSkor(skor_akhir);
		}
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}

function updateSkor(n){
	$.ajax({
		url: $('input[name=site_url]').val()+'cbt_latihan/request/update_skor',
		type: 'GET',
		dataType: 'json',
		data: {param: $('input[name=param]').val(), n: n},
	})
	.done(function(res) {
		console.log("update skor success");
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
	
}

$(function(){
	$('#submit-ujian').on('submit', function(evt){
		tinyMCE.triggerSave();
		$.ajax({
			url: $('#submit-ujian').attr('action'),
			type: 'POST',
			dataType: 'json',
			data: $('#submit-ujian').serializeArray(),
		})
		.done(function(res) {
			if(res.status == 'success'){
				updateNav();
				if(more_next) loadSoal(next);
				countJawab();
				loadResult();
				if(more_next == false) {
					$('.bt-save-soal').addClass('hide');
					$('.bt-next-soal').addClass('hide');
					// $('.btn-selesai').click();
				}
			}else{
				// console.log(res);
				var modal= $('#msgModal');
				modal.find('.modal-body').html(`
					<p>`+res.msg+`</p>
					`); 
				modal.find('.modal-footer').html(`
					<button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">Batal</button>
					`);
				modal.modal('show');
			}
			

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		evt.preventDefault();
		
	})

	$('.bt-next-soal').on('click', function(){
		loadSoal(next);
	})

	$('.btn-selesai').on('click', function(){
		// loadSoal(next);
		var redir = $('input[name=site_url]').val()+'cbt_latihan/request/test_result/'+$('input[name=param]').val();
		var modal= $('#msgModal');
		modal.find('.modal-body').html(`
			<p>Masih ada waktu tersisa, yakin menyelesaikan ujian ?</p>
			`); 
		modal.find('.modal-footer').html(`
			<button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">Batal</button>
			<button type="button" class="btn btn-sm btn-primary float-right btn-konfirm-selesai">Selesai</button>
			`); 
		modal.modal('show');

		$('.btn-konfirm-selesai').unbind('click').on('click', function(){
			$.ajax({
				url: $('input[name=site_url]').val()+'cbt_latihan/request/update_timer',
				type: 'GET',
				dataType: 'json',
				data: {param: $('input[name=param]').val(), 'batas_waktu': batas_waktu, self_abort : 1},
			})
			.done(function(res) {
				// console.log("success");
				if(res.status == 'success') window.location.href = redir;
			})
			.fail(function() {
				// console.log("error");
			})
			.always(function() {
				// console.log("complete");
			});
			
			
		})
	})
	
})