'use strict';
var ajaxProc= false;
$(document).ready(function(){
$('#cbt-login').submit(function(e){
	// alert('okeee');
	var act = $(this).attr('action');
	$.ajax({
		url: act,
		type: 'GET',
		dataType: 'json',
		data: $(this).serialize(),
	})
	.done(function(res) {
		console.log("success");
		if(res.status == 'success'){
			window.location.href = res.link;
		}else{
			$('.err-capt').html(`<p>`+res.msg+`</p>`)
		}
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
	e.preventDefault();
})
});