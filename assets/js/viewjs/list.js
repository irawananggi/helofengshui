'use strict';
var ajaxProc= false;
var table = $('#tab_data');
$(document).ready(function(){
    table.find('thead').after(`<tbody></tbody>`);

});

$(function(){
    table.find('tbody').ready(function(){
        list();

        $('input[name=q]').on('keyup', function(){
            console.log($(this).val())
            list();
        });

        $('.btn-print').on('click', function(){
            printReport('cetak-area');
            return false;
        });
    });
   
});


// reload
$(document).on('hover', '[data-click=panel-reload]', function(e) {
    if (!$(this).attr('data-init')) {
        $(this).tooltip({
            title: 'Reload',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
        $(this).attr('data-init', true);
    }
});
$(document).on('click', '[data-click=panel-reload]', function(e) {
    e.preventDefault();
    var target = $(this).closest('.panel');
    if (!$(target).hasClass('panel-loading')) {
        var targetBody = $(target).find('.panel-body');
        var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
        $(target).addClass('panel-loading');
        $(targetBody).prepend(spinnerHtml);
        setTimeout(function() {
            $(target).removeClass('panel-loading');
            $(target).find('.panel-loader').remove();
        }, 1000);

    }
// reload table:
    list();
});

function list(page){
    if(ajaxProc) return;
    ajaxProc = true;

    page = page || 1;
    let q = $('input[name=q]').val();
    var table = $('#tab_data');
    let listUrl = table.data('list');
    let param1 = table.data('param1');
    if(listUrl == undefined)listUrl = 'listdata';
    $.ajax({
            url: table.data('url')+'/'+listUrl,
            method: "GET",
            dataType: "JSON",
            data: {page:page,q:q, param1:param1},
            beforeSend: function(){
                table.find('tbody').children().remove();
                $('.loading-table').removeClass('hide');
            },
            success: function(res){
                $('.loading-table').addClass('hide');
                table.find('tbody').children().remove();
                if(res.status=='success'){
                    const {result,offset,total,limit} = res;
                    let {page} = res; 
                    let no=1+offset;
                    var koloms = table.data('kolom');
                    // console.log(koloms);
                    // koloms = JSON.parse(koloms);

                    var rd;

                    $.each(result,function(idx, item){
                        // console.log(item);
                        let par_aksi = {
                            'id':item.id
                        };
                        let kolom =null;

                        $.map(koloms, function(kol) {
                        	// console.log(item[kol]) ;
                        	kolom +=`<td>`+item[kol]+`</td>`;
                        });

                        // update & delete
                        
                        if(item.param){
                        	let encode_par = item.param;
                        	rd = `<td style="white-space:nowrap;">
                                    <a href="`+table.data('url')+`form/`+encode_par+`" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    <button data-url="`+table.data('url')+`remove/`+encode_par+`" data-act="hapus" data-msg="Yakin akan menghapus data ?" class="btn btn-xs btn-danger" data-target="#modal-message" data-toggle="modal"><i class="fa fa-trash"></i></button>
                                </td>`;
                        }
                        


                        table.find('tbody').append(`
                            <tr>
                                <td>`+no+`</td>
                                `+kolom+`
                                `+rd+`
                            </tr>
                        `);
                        no++;
                    });

                    let pag = new Pagination('list', total, limit, page);
				    pag.init();

                }else if(res.status=='failed'){
                    table.find('tbody').children().remove();
                }
                ajaxProc = false;
            }
        });
}

