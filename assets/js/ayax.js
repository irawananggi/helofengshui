(function($) {

    $.fn.ayaX = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            url         : null,
            data        : null,
            method    : 'POST'
        }, options);

        return this.each( function() {
            $.ajax({
                url: settings.uri,
                type: settings.method,
                cache: false,
                dataType: 'json',
                data: settings.data,
                success: function(msg) {
                    $(this).html(msg.htm);
                },
                error:function(error){
                    console.log(error);
                }
            });        
        });
    }

}(jQuery));