'use strict';
let ajaxProc= false;

$(function(){
	$('#tab_jadwalujian').ready(function(){
	        list();
	        $('.btn-print').on('click', function(){
	            printReport('cetak-area');
	            return false;
	        });
	    // });
	})
   
});


function list(page){
    if(ajaxProc) return;
    ajaxProc = true;

    page = page || 1;
    let q = $('input[name=q]').val();
    let table = $('table#tab_jadwalujian');
    // console.log(table.find('tbody').length);
    if(table.find('tbody').length == 0) table.append(`<tbody><tr><td></td></tr></tbody>`);
    $.ajax({
            url: table.data('url')+'/dashboard_jadwal',
            method: "GET",
            dataType: "JSON",
            data: {page:page,q:q},
            beforeSend: function(){
                table.find('tbody').children().remove();
                $('.loading-table').removeClass('hide');
            },
            success: function(res){
                $('.loading-table').addClass('hide');
                table.find('tbody').children().remove();
                if(res.status=='success'){
                    const {result,offset,total,limit} = res;
                    let {page} = res; 
                    let no=1+offset;
                    $.each(result,function(idx, item){
                        // console.log(item);
                        let par_aksi = {
                            'id':item.id
                        };
                        let encode_par = item.param;

                        let bgTdColor = '#fff';
                        if(item.color_status) bgTdColor = item.color_status;
                        table.find('tbody').append(`
                            <tr style="background-color:`+bgTdColor+`">
                                <td>`+no+`</td>
                                <td>`+item.pin+`</td>
                                <td>`+item.tipe_ujian+`</td>
                                <td>`+item.judul+`</td>
                                <td>`+item.tanggal+`</td>
                                <td>`+item.ct_siswa+`</td>
                                <td>`+item.pelaksanaan+`</td>
                                `+(item.link_peserta ? '<td>'+item.link_peserta+'</td>' : '')+`
                            </tr>
                        `);
                        no++;
                    });

                    let pag = new Pagination('list', total, limit, page);
				    pag.init();

                }else if(res.status=='failed'){
                    table.find('tbody').children().remove();
                }
                ajaxProc = false;
            }
        });
}

