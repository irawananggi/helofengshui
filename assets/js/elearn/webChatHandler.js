class webChatHandler {
	constructor(chatparam) {
		this.cp = chatparam;
	}

	sending(data) {
		let self= {};
		$.ajax({
			url: $('input[name=handler_url]').val()+'send',
			type: 'POST',
			dataType: 'json',
			async: false,
			data: {data: data},
		})
		.done(function(res) {
			self = res;
    	// console.log(res);
    })
		.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });

		return self;
	}

	loadchat(sendto, thisday,page) {
		page = page||1;
  	// return sendto;
  	// console.log(this.cp);
  	let self= {};

  	$.ajax({
  		url: $('input[name=handler_url]').val()+'loadchat',
  		type: 'GET',
  		dataType: 'json',
  		async: false,
  		data: {sendto: sendto, from: this.cp.id_client, thisday:thisday, page},
  	})
  	.done(function(res) {
  		self = res;
    	// console.log(res);
    })
  	.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });
  	
  	return self;
  }

  markasread(sendto, li) {
  	let self= {};
  	// console.log(li);
  	if(li.length > 0){
  		let lastload = li.data('chat-id');
  		// console.log(lastload);
  		
  		$.ajax({
  			url: $('input[name=handler_url]').val()+'markasread',
  			type: 'GET',
  			dataType: 'json',
  			async: false,
  			data: {sendto: this.cp.id_client, from:sendto ,lastload:lastload},
  		})
  		.done(function(res) {
  			self = res;
    	// console.log(res);
    })
  		.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });

  	}else{
  		self.status == 'failed';
  	}

  	return self;
  }

  loadoldchat(sendto, li,page) {
  	let self= {};

  	if(li.length > 0){
  		let lastload = li.data('chat-id');
  		// console.log(lastload);
  		$.ajax({
  			url: $('input[name=handler_url]').val()+'loadoldchat',
  			type: 'GET',
  			dataType: 'json',
  			async: false,
  			data: {sendto: sendto, from: this.cp.id_client,lastload:lastload, page},
  		})
  		.done(function(res) {
  			self = res;
    	// console.log(res);
    })
  		.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });

  	}else{
  		self.status == 'failed';
  	}

  	return self;
  }

  roomlist(page) {
  	page = page || 1;
  	// return sendto;
  	// console.log(this.cp);
  	let self= {};

  	$.ajax({
  		url: $('input[name=handler_url]').val()+'roomlist',
  		type: 'GET',
  		dataType: 'json',
  		async: false,
  		data: {sendto: this.cp.id_client,page:page},
  	})
  	.done(function(res) {
  		self = res;
    	// console.log(res);
    })
  	.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });
  	
  	return self;
  }

  chatprevbyid(from,page) {
  	page = page || 1;
  	// return sendto;
  	// console.log(this.cp);
  	let self= {};

  	$.ajax({
  		url: $('input[name=handler_url]').val()+'chatbyid',
  		type: 'GET',
  		dataType: 'json',
  		async: false,
  		data: {from: from, to: this.cp.id_client, page:page},
  	})
  	.done(function(res) {
  		self = res;
    	// console.log(res);
    })
  	.fail(function(xhr, status, error) {
        // error handling
        self = error;
    });
  	
  	return self;
  }

  // Getter
  get userlist() {
    // return this.cp;
    let self=null;
    $.ajax({
    	url: $('input[name=handler_url]').val()+'userlist',
    	type: 'GET',
    	dataType: 'json',
    	async: false,
    	data: {param: JSON.stringify(this.cp)},
    })
    .done(function(res) {
    	self = res;
    	// console.log(res);
    })
    .fail(function(xhr, status, error) {
        // error handling
        self = error;
    });
    return self;
    
}


  // Method

}

// exports.webChatHandler = webChatHandler;
