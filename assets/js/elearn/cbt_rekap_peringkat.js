'use strict';
var ajaxProc= false;
var table = $('#tab_data');
$(document).ready(function(){
    table.find('thead').after(`<tbody></tbody>`);

    global_select('sel-unit', {}, 'list');
	global_select('sel-kuri', [{key:'id_unit', val:'id_unit'}], 'list');
	global_select('sel-kelas', [{key:'id_unit', val:'id_unit'}, {key: 'id_kuri', val:'id_kuri'}], 'list');
	global_select('sel-mapel', [{key:'id_kelas', val:'id_kelas'}], 'list');
	global_select('sel-tipe-ujian', {}, 'list');


});

$(function(){
    table.find('tbody').ready(function(){
        list();

        $('input[name=q]').on('keyup', function(){
            console.log($(this).val())
            list();
        });

        $('.btn-print').on('click', function(){
            printReport('cetak-area');
            return false;
        });
    });
   
});


function list(page){
    if(ajaxProc) return;
    ajaxProc = true;

    page = page || 1;
    let q = $('input[name=q]').val();
    var table = $('#tab_data');
    let listUrl = table.data('list');
    let param1 = table.data('param1');
    // filter
	let id_unit = $('select[name=id_unit]').val();
	let id_kuri = $('select[name=id_kuri]').val();
	let id_kelas = $('select[name=id_kelas]').val();
	let id_mapel = $('select[name=id_mapel]').val();
	let id_tipe_ujian = $('select[name=id_tipe_ujian]').val();
	// ./filter

    $.ajax({
            url: table.data('url')+'/'+listUrl != undefined ? listUrl : 'ajax_peserta',
            method: "GET",
            dataType: "JSON",
            data: {page:page,q:q, param1:param1, id_unit:id_unit, id_kuri:id_kuri, id_kelas:id_kelas, id_mapel:id_mapel, id_tipe_ujian:id_tipe_ujian},
            beforeSend: function(){
                table.find('tbody').children().remove();
                $('.loading-table').removeClass('hide');
            },
            success: function(res){
                $('.loading-table').addClass('hide');
                table.find('tbody').children().remove();
                if(res.status=='success'){
                    const {result,offset,total,limit} = res;
                    let {page} = res; 
                    let no=1+offset;
                    var koloms = table.data('kolom');
                    // console.log(koloms);
                    // koloms = JSON.parse(koloms);

                    var rd;

                    $.each(result,function(idx, item){
                        // console.log(item);
                        let par_aksi = {
                            'id':item.id
                        };
                        let kolom =null;

                        $.map(koloms, function(kol) {
                        	// console.log(item[kol]) ;
                        	kolom +=`<td>`+item[kol]+`</td>`;
                        });

                        // update & delete
                        
                        if(item.param){
                        	let encode_par = item.param;
                        	rd = `<td style="white-space:nowrap;">
                                    <a href="`+table.data('url')+`form/`+encode_par+`" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    <button data-url="`+table.data('url')+`remove/`+encode_par+`" data-act="hapus" data-msg="Yakin akan menghapus data ?" class="btn btn-xs btn-danger" data-target="#modal-message" data-toggle="modal"><i class="fa fa-trash"></i></button>
                                </td>`;
                        }
                        
                        table.find('tbody').append(`
                            <tr>
                                <td>`+no+`</td>
                                `+kolom+`
                                `+rd+`
                            </tr>
                        `);
                        no++;
                    });

                    loadNilai();

                    let pag = new Pagination('list', total, limit, page);
				    pag.init();

                }else if(res.status=='failed'){
                    table.find('tbody').children().remove();
                }
                ajaxProc = false;
            }
        });
}

function loadNilai(){
	let secPil = $('.pes-id');
	if(secPil.length > 0){
		$.map(secPil, function(item, index) {
			// return something;
			// console.log($(item).data('id'));
			let url = $(item).data('url');
			let param = $(item).data('param');
			let id = $(item).data('id');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {param: param},
			})
			.done(function(res) {
				// console.log(res);
				const {status} = res;
				if(status == 'success'){
					const {result, jadwal} = res;
					let total = 0;
                    let nilai_max = jadwal.nilai_max;

					$.map(result, function(i, d) {
						// return something;
						// console.log(i.pilihan);
						if(i.pilihan == 0){
							$('.nilai-ess-'+id).html(i.nilai_ess);
							total += i.nilai_ess;
						}else if(i.pilihan == 1){
							$('.nilai-pil-'+id).html(i.nilai_pil);
							total += i.nilai_pil;
						}
					});
					
					$('.nilai-total-'+id).html(total);

                    let skor_akhir = total/nilai_max*100;
                    $('.skor-'+id).html(skor_akhir.toFixed(2));

				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			

		});
	}
}

