'use strict'
$(document).ready(function() {
	global_select('select-ta',{}, 'setMinMax', true);
	global_select('select-levelunit', {}, 'initEvent');
});

function setMinMax(id){
	$.ajax({
		url: $('input[name=url_tahun_ajaran]').val(),
		type: 'GET',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(res) {
		$.map(res, function(item, index) {
			$('#min-date').val(item.mulai)
			$('#max-date').val(item.selesai)
		});
		if($('#id_level_unit').val() !== "") initEvent();else $('#kalender').html('');
	})
	.fail(function() {
	})
	.always(function() {
	});

}

let calendar = null;

$(function() {
	$('#save-event').click(function() {
		saveEvent();
	});

	document.querySelector('#update-language').addEventListener('click', function() {
		calendar.setLanguage(document.querySelector('#language').value);
	});

/*
	document.querySelector('#kalender').addEventListener('clickDay', function(e) {
		if(e.events.length > 0){
			$.map(e.events, function(item, index) {
				if(item.mode == 'ref') {
					$('#event-modal').modal('hide');
					$('#reject-modal').modal();
				}
			});
		}
	})
	*/
});



function initEvent(){
	if($('#id_ta').val() !== "" && $('#id_level_unit').val() !== ""){
		$.ajax({
			url: $('input[name=url_data]').val(),
			type: 'GET',
			dataType: 'json',
			data: {id_level_unit: $('#id_level_unit').val(), id_ta:$('#id_ta').val()},
			success: initCalendar
		})
	}else{
		$('#kalender').html('');
	}


}

function initCalendar(eventData){
	var currentYear = new Date().getFullYear();
	let eve = eventData;
	let hariLibur = [];

	$.map(eve, function(item, index) {
		item.startDate = new Date(item.startDate);
		item.endDate = new Date(item.endDate);
		if(item.mode == 'ref') hariLibur.push(new Date(item.tanggal).getTime());

	});
	calendar = new Calendar('#kalender', { 
		disabledWeekDays:[0],
		enableContextMenu: true,
		enableRangeSelection: true,
		contextMenuItems:[
		{
			text: 'Ubah',
			click: formEvent
		},
		{
			text: 'Hapus',
			click: deleteEvent
		}
		],
		selectRange: function(e) {
			// console.log(e);
			let adaLibur = 0;
			if(e.events.length > 0){
				$.map(e.events, function(item, index) {
					if(item.mode == 'ref') adaLibur +=1;
				});
			}
			if(adaLibur > 0) $('#reject-modal').modal();
			else formEvent({ startDate: e.startDate, endDate: e.endDate }); 
		},
		mouseOnDay: function(e) {
			if(e.events.length > 0) {
				var content = '';

				for(var i in e.events) {
					content += '<div class="event-tooltip-content">'
					+ '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
					+ '<div class="event-keterangan">' + e.events[i].keterangan + '</div>'
					+ '</div>';
				}

				$(e.element).popover({ 
					trigger: 'manual',
					container: 'body',
					html:true,
					content: content
				});

				$(e.element).popover('show');
			}
		},
		mouseOutDay: function(e) {
			if(e.events.length > 0) {
				$(e.element).popover('hide');
			}
		},
		dayContextMenu: function(e) {
			$(e.element).popover('hide');
		},
		dataSource: eve,
		customDayRenderer: function(element, date) {
			if(hariLibur.includes(date.getTime())){
				$(element).css('font-weight', 'bold');
							// $(element).css('font-size', '14px');
							// $(element).css('background-color', '#e45544');
							$(element).css('color', '#e45544');
						}
					}
				});

	initOpsi();
	loadLegend(eve);
}

function formEvent(event) {
	let rejectDay = ['ref','semester'];
	if(rejectDay.includes(event.mode)){
		return false;
	}else{
		console.log(event);
		$('#event-modal input[name="event-index"]').val(event ? event.id : '');
		$('#event-modal select[name="event-tipe"]').val(event ? event.tipe : '');
		$('#event-modal textarea[name="event-name"]').val(event ? event.name : '');
		$('#event-modal textarea[name="event-keterangan"]').val(event ? event.keterangan : '');
		$('#event-modal input[name="event-start-date"]').datepicker('update', event ? event.startDate : '');
		$('#event-modal input[name="event-end-date"]').datepicker('update', event ? event.endDate : '');
		$('#event-modal').modal();
		formOptions();
	}

}

function formOptions(){
	$(function(){

		$("#event-name").on("keydown", function () {
			var newY = $(this).textareaHelper('caretPos').top + (parseInt($(this).css('font-size'), 10) * 1.5);
			var newX = $(this).textareaHelper('caretPos').left;
			var posString = "left+" + newX + "px top+" + newY + "px";
			$(this).autoComplete("option", "position", {
				my: "left top",
				at: posString
			});
		});

		$("#event-name ").autoComplete({
			source: function(term, response){
				let xhr;
				try { xhr.abort(); } catch(e){}
				xhr = $.getJSON($('input[name=auto_kegiatan]').val(), { term: term }, function(data){ response(data); });
			},
			renderItem: function (item, search){
				return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-val="'+item.kegiatan+'" data-tipe="'+item.tipe+'">'+item.kegiatan+'</div>';

			},
			onSelect: function(e, term, item){
				e.preventDefault();
				$('#id_kegiatan').val(item.data('id'));
				$('#event-tipe').val(item.data('tipe'));
			}
		});
	})
}

function deleteEvent(event) {
	let id = event.id;
	$.ajax({
		url: $('input[name=url_hapus_event]').val(),
		type: 'GET',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(res) {
		const {status} = res;
		if(status == 'success') initEvent();
	})
	.fail(function() {
	})
	.always(function() {
	});

}

function saveEvent() {

	let startDate =  $('#event-modal input[name="event-start-date"]').datepicker({ dateFormat: 'dd-mm-yy' }).val();

	let endDate =  $('#event-modal input[name="event-end-date"]').datepicker({ dateFormat: 'dd-mm-yy' }).val();

	var event = {
		id: $('#event-modal input[name="event-index"]').val(),
		kegiatan: $('#event-modal textarea[name="event-name"]').val(),
		tipe: $('#event-modal select[name=event-tipe]').val(),
		keterangan: $('#event-modal textarea[name="event-keterangan"]').val(),
		startDate,
		endDate,
		id_ta : $('#id_ta').val(),
		id_level_unit : $('#id_level_unit').val(),
	}

	$.ajax({
		url: $('input[name="url_simpan_event"]').val(),
		type: 'POST',
		dataType: 'json',
		data: event,
	})
	.done(function(res) {
		const {status} = res;
		if(status == 'success') initEvent();
	})
	.fail(function() {
	})
	.always(function() {
	});



	$('#event-modal').modal('hide');
}

function initOpsi(){
	calendar.setLanguage(document.querySelector('#language').value);
	calendar.setYear(document.querySelector('#current-year').value);
	calendar.setMinDate(document.querySelector('#min-date').valueAsDate);
	calendar.setMaxDate(document.querySelector('#max-date').valueAsDate);
	calendar.setAllowOverlap(true);
	calendar.setEnableRangeSelection(true);
}


function loadLegend(res){

	// console.log(res.sort(compare));
	let content = '<h4>Legenda:</h4>';
	$.map(res.sort(compare), function(item, index) {

		let startDate = new Date(item.startDate);
		startDate =  moment(startDate).format('DD/MM/YYYY');
		let endDate = new Date(item.endDate);
		endDate =  moment(endDate).format('DD/MM/YYYY');

		content += `
		<div class="event-tooltip-content">
		<div class="event-name" style="color:` + item.color + `">` + ((startDate == endDate) ? `<span class="pull-left"><i class="fa fa-calendar"></i> `+startDate+`</span>` : `<span class="pull-left"><i class="fa fa-calendar"></i> `+startDate+` - `+endDate+`</span> `)+`: <b>`+item.name+`</b></div>
		<div class="event-keterangan">` + item.keterangan + `</div>
		<div class="event-hr"><hr/></div>
		</div>`;
	});

	$('#legend').html(content);
}

function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  let startDateA = a.startDate;
  startDateA = new Date(startDateA);
  let startDateB = b.startDate;
  startDateB = new Date(startDateB);
  let comparison = 0;
  if (startDateA > startDateB) {
  	comparison = 1;
  } else if (startDateA < startDateB) {
  	comparison = -1;
  }
  return comparison;
}