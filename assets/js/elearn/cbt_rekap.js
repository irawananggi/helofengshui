'use strict';
var ajaxProc= false;
var table = $('#tab_data');
$(document).ready(function(){
	table.find('thead').after(`<tbody></tbody>`);
	global_select('sel-unit', {}, 'list');
	global_select('sel-kuri', [{key:'id_unit', val:'id_unit'}], 'list');
	global_select('sel-kelas', [{key:'id_unit', val:'id_unit'}, {key: 'id_kuri', val:'id_kuri'}], 'list');
	global_select('sel-mapel', [{key:'id_kelas', val:'id_kelas'}], 'list');
	global_select('sel-tipe-ujian', {}, 'list');

});

$(function(){
	table.find('tbody').ready(function(){
		list();

		$('input[name=q]').on('keyup', function(){
			console.log($(this).val())
			list();
		});

		$('.btn-print').on('click', function(){
			printReport('cetak-area');
			return false;
		});
	});

});


// reload
$(document).on('hover', '[data-click=panel-reload]', function(e) {
	if (!$(this).attr('data-init')) {
		$(this).tooltip({
			title: 'Reload',
			placement: 'bottom',
			trigger: 'hover',
			container: 'body'
		});
		$(this).tooltip('show');
		$(this).attr('data-init', true);
	}
});
$(document).on('click', '[data-click=panel-reload]', function(e) {
	e.preventDefault();
	var target = $(this).closest('.panel');
	if (!$(target).hasClass('panel-loading')) {
		var targetBody = $(target).find('.panel-body');
		var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
		$(target).addClass('panel-loading');
		$(targetBody).prepend(spinnerHtml);
		setTimeout(function() {
			$(target).removeClass('panel-loading');
			$(target).find('.panel-loader').remove();
		}, 1000);

	}
// reload table:
list();
});

function list(page){
	if(ajaxProc) return;
	ajaxProc = true;

	page = page || 1;
	let q = $('input[name=q]').val();
	var table = $('#tab_data');

	// filter
	let id_unit = $('select[name=id_unit]').val();
	let id_kuri = $('select[name=id_kuri]').val();
	let id_kelas = $('select[name=id_kelas]').val();
	let id_mapel = $('select[name=id_mapel]').val();
	let id_tipe_ujian = $('select[name=id_tipe_ujian]').val();
	// ./filter
	$.ajax({
		url: table.data('url')+'/listdata',
		method: "GET",
		dataType: "JSON",
		data: {page:page,q:q, id_unit:id_unit, id_kuri:id_kuri, id_kelas:id_kelas, id_mapel:id_mapel, id_tipe_ujian:id_tipe_ujian},
		beforeSend: function(){
			table.find('tbody').children().remove();
			$('.loading-table').removeClass('hide');
		},
		success: function(res){
			$('.loading-table').addClass('hide');
			table.find('tbody').children().remove();
			if(res.status=='success'){
				const {result,offset,total,limit} = res;
				let {page} = res; 
				let no=1+offset;
				$.each(result,function(idx, item){
                        // console.log(item);
                        let par_aksi = {
                        	'id':item.id
                        };
                        let encode_par = item.param;


                        table.find('tbody').append(`
                        	<tr>
                        	<td>`+no+`</td>
                        	<td>`+item.unit+`</td>
                        	<td>`+item.pin+`</td>
                        	<td>`+item.tipe_ujian+`</td>
                        	<td>`+item.kelas+`</td>
                        	<td>`+item.kuri+`</td>
                        	<td>`+item.kelompok+`</td>
                        	<td>`+item.tanggal+`</td>
                        	<td class="no-print">`+item.peserta+`</td>
                        	</tr>
                        	`);
                        no++;
                    });

				let pag = new Pagination('list', total, limit, page);
				pag.init();

			}else if(res.status=='failed'){
				table.find('tbody').children().remove();
			}
			ajaxProc = false;
		}
	});
}

