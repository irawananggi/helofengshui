$(document).ready(function() {
	
	// var tabChange = function () {
	// 	var tabs = $('.nav-pills > li');
	// 	var active = tabs.filter('.active');
	// 	var next = active.next('li').length ? active.next('li').find('a') : tabs.filter(':first-child').find('a');
	// 	next.tab('show');
	// };
	//var tabCycle = setInterval(tabChange, 25000);
	

    // to check login status of user
   
});


function validateLogin(login_type) {
    
    var flag = true;
    if(login_type=="mobile"){
        var mobile = $.trim($("#mobile").val());
        if(mobile==''){
            $("#errorMessage_mobile").css('display', 'block');
            $('#errorMessage_mobile').html('Harap masukkan nomor ponsel Anda yang terdaftar');
            flag = false;
        } else {
            $('#errorMessage_mobile').html('');
        }
    }
    if(login_type=="email"){
        if ($('#usernameLogin').val() == "") {
            $("#errorusernameLogin").css('display', 'block');
            $('#errorusernameLogin').html('Silakan masukkan alamat email Anda yang terdaftar');
            flag = false;
        } else {
            $('#errorusernameLogin').html('');
        }
    }
    if ($('#passwdLogin').val() == "") {
        $("#errorpasswdLogin").css('display', 'block');
        $('#errorpasswdLogin').html('Silakan masukkan kata sandi Anda');
        flag = false;
    } else {
        $('#errorpasswdLogin').html('');
    }
    if(flag==true) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            async: false,
            url: BASE_URL + '/user/registerby',
            data: {identity: $('#usernameLogin').val(), credential: $('#passwdLogin').val()},
            success: function(data) {
                if(data.output=='failure') {
                    $("#errorusernameLogin").css('display', 'block');
                    $('#errorusernameLogin').html(data.message);
                    $('#loginFailed').html('');
                    flag = false;
                } else if(data.output=='success') {
                    $('#errorusernameLogin').html('');
                } else {
                    flag = false;
                }
            }
        });
    }
    if (flag == false) {
        return false;
    } else {
        
        $('#loadding').show();
        var userEmail = $('#usernameLogin').val();

        var userPassword = $('#passwdLogin').val();
        var redirect = $("#urlRedirect").val();
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: BASE_URL + '/user/login?redirect=' + redirect,
            data: {identity: userEmail, credential: userPassword},
            success: function(data) {
                $('#loadding').hide();
                var proceed = '1';

                if (data.output == 'Masked') {

                    //                    location.href = BASE_URL + '/user/registermaskedemail?redirect=' + redirect +'&masked_email='+data.email;
                    location.href = BASE_URL + '/user/registermaskedemail';
                    return false;
                }
                if (data.usertypeId == '10') {

                    location.href = BASE_URL + '/admin';

                    return false;
                }

                if (data.loginuserexists == '1')
                {

                    $.cookie("previousSessionCheckVar", null);
                    $.cookie("previousSessionCheckVar", true);
                    if(data.alreadyLogin==false) {
                    if (confirm("Sesi terakhir Anda tidak keluar dengan benar. Dengan login ini kami menghancurkan semua login Anda sebelumnya."))
                    {
                        $.cookie("previousSessionCheckVar", null);
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: BASE_URL + '/user/previoussession?redirect=' + redirect,
                                data: {user_id: data.user_id, usersession_id: data.userssession_id, satPackage: data.satPackage, capPackage:data.capPackage,alreadyLogin:data.alreadyLogin},
                            success: function(data) {
                                calllogin(data, userEmail, userPassword);
                            }
                        });
                    }
                    else
                    {
                        location.href = BASE_URL + '/user/logoutspecial';

                        return false;
                    }
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: BASE_URL + '/user/previoussession?redirect=' + redirect,
                            data: {user_id: data.user_id, usersession_id: data.userssession_id, satPackage: data.satPackage, capPackage:data.capPackage,alreadyLogin:data.alreadyLogin},
                            success: function(data) {
                                calllogin(data, userEmail, userPassword);
                }
                        });
                    }
                }
                else
                {

                    calllogin(data, userEmail, userPassword);
                }

            }
        });
    }
}
function calllogin(data, userEmail, userPassword)
{   

    if (data.output == 'success') {
        //alert (BASE_URL+'"/user/user-dashboard');
        //location.href = BASE_URL + "/user/myprofile";
        //        location.href = BASE_URL + "/user/user-dashboard";

        if ($("#checkboxG1").is(":checked") == true) {
            // set cookies to expire in 1 days
            $.cookie('email', userEmail, {expires: 1});
            $.cookie('password', userPassword, {expires: 1});
            $.cookie('remember', true, {expires: 1});
        } else {
            // reset cookies
            $.cookie('email', null);
            $.cookie('password', null);
            $.cookie('remember', null);
        }

        if (data.redirect != '')
        {

            var urlredirect = data.redirect;
            
            if (urlredirect == 'buy-package') {
                location.href = BASE_URL + "/buy-package";
                return false;
            }
            if(urlredirect.indexOf('website/index/dashboard') > 0) {
                location.reload();
            } else if(urlredirect.indexOf('websearch/search') > 0) {
                location.reload();
            } else {
                location.href = data.redirect;
            }
        } else if (data.satPackage > 0) {
            location.href = BASE_URL + "/sat-exam-preparation";
        } else if (data.capPackage > 0) {
            location.href = BASE_URL + "/cap-package";
        } else {
            location.href = BASE_URL + "/user/student-dashboard";
        }

        return false;



        if (loginForm.checkboxG1.checked == true) {
            // set cookies to expire in 1 days
            $.cookie('email', userEmail, {expires: 1});
            $.cookie('password', userPassword, {expires: 1});
            $.cookie('remember', true, {expires: 1});
        } else {
            // reset cookies
            $.cookie('email', null);
            $.cookie('password', null);
            $.cookie('remember', null);

        }
        $('#loginFailed').html('');
        //alert(data.redirect_url);return false;
        if (data.redirect_url != '')
        {
            location.href = data.redirect_url;
        }
        else if (data.userTypeId == '1') {
            location.href = BASE_URL + "/my-profile";
        } else {
            location.href = BASE_URL + "/user";
            // location.href="/progress";
        }
    } else if (data.backurl) {
        location.href = data.backurl + "?previous_session=1";
    } else {
        $("#loginFailed").css('display', 'block');
        $('#loginFailed').html('Harap masukkan email / nama pengguna dan kata sandi yang valid');
    }
}
function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
function isNumberKey( evt ) {
            var charCode = ( evt.which ) ? evt.which : evt.keyCode
            if ( charCode > 31 && ( charCode < 48 || charCode > 57 ) )
                    return false;
            return true;
    }
    
    function forgotSubmit(e) {
    if (e.keyCode == 13) {
        resetPassword();
        return false;
    }
}

function resetPassword() {
    var flag = true;
    var emailcheck = $("#lupaemail").val().trim();
    if (emailcheck == '') {
        $("#erroremailreset").show();
        $("#erroremailreset").text("Silakan masukkan alamat email Anda yang terdaftar");
        flag = false;
    } else {
        if (checkEmail(emailcheck) == false) {
            $("#erroremailreset").show();
            $("#erroremailreset").text("Harap masukkan alamat email yang valid");
            flag = false;
        }
    }
    if (flag == false) {
        return false;
    } else {
            $('#resetlupa').prop("disabled",true);
            $('#resetlupa').attr('disabled','disabled');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: BASE_URL + '/user/checkemail',
                data: {email: emailcheck},
                success: function(data) {
                    if (data.output != 'success') {
                        $('#resetlupa').prop("disabled",false);
                        $('#resetlupa').removeAttr('disabled');
                        $("#erroremailreset").css('display', 'block');
                        $("#erroremailreset").text("Jika e-mail yang disediakan ada dalam basis data kami, Anda harus segera menerima instruksi pengaturan ulang kata sandi.");
                    } else {
                        $("#erroremailreset").html("");
//                        $("#hide_forgot").removeClass("logbt");
//                        $("#hide_forgot").removeAttr("onclick");
                        $("#loadding").show();

                        $("#f-password-box1").hide();
                        $("#f-password-box2").show();

                        $("#enter_email_p").hide();
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: BASE_URL + '/user/sentluparequest',
                            data: {email: emailcheck},
                            success: function(data) {
                                $("#loadding").hide();
                                $(".successMessage").hide();
                                if (data.output == 'success') {
                                    $("#erroremailreset").html("");
                                    $(".erroremailonlupa").show();
                                    $("#erroremailonlupa").show();
                                    $('#erroremailonlupa').text('Pengajuan Anda berhasil dan Anda akan menerima email untuk Kata Sandi baru.');
                                    setTimeout(function(){
                                        $("#lupaemail").val("");
                                        $('#erroremailonlupa').text("");
                                        $('#resetlupa').prop("disabled",false);
                                        $('#resetlupa').removeAttr('disabled');
                                        $('#thanks-form').show();
                                        $('#forgot-form').hide();
                                    },3000);
                                } else {
                                    $('#resetlupa').prop("disabled",false);
                                    $('#resetlupa').removeAttr('disabled');
                                    $("#erroremailreset").text("");
                                    $(".erroremailonlupa").show();
                                    $("#erroremailonlupa").text("Tidak berhasil");
                                }
                            }
                        });
                    }
                }
            });
    }
}
function checkEmail(emailStr) {
    var emailPat = /^(.+)@(.+)$/;
    var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars = "\[^\\s" + specialChars + "\]";
    var quotedUser = "(\"[^\"]*\")";
    var ipDomainPat = /^(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})$/;
    var atom = validChars + "+";
    var word = "(" + atom + "|" + quotedUser + ")";
    var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
    var matchArray = emailStr.match(emailPat);
    if (matchArray == null) {
        return false;
    }
    var user = matchArray[1];
    var domain = matchArray[2];
    if (user.match(userPat) == null) {
        return false;
    }
    var IPArray = domain.match(ipDomainPat);
    if (IPArray != null) {
        for (var i = 1; i <= 4; i++) {
            if (IPArray[i] > 255) {
                return false;
            }
        }
        return true;
    }
    var domainArray = domain.match(domainPat);
    if (domainArray == null) {
        return false;
    }
    var atomPat = new RegExp(atom, "g");
    var domArr = domain.match(atomPat);
    var len = domArr.length;
    if ((domArr[domArr.length - 1].length < 2) || (domArr[domArr.length - 1].length > 5)) {
        return false;
    }
    if (len < 2) {
        return false;
    }
}