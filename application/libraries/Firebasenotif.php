<?php  
class Firebasenotif 
{
	// url here
	private $url = 'https://fcm.googleapis.com/fcm/send';


	function notif($title='', $text='', $to='',$kode_status='', $id_histori_transaksi='', $click_action='', $extends_data=[], $sound='', $priority='high')
	{

		$ci =& get_instance();
		// if sound is null using default sound
		if ($sound == '') {
			$sound = 'content://settings/system/notification_sound';
		}

		// calling key
		$key ='AAAAfIemvrw:APA91bEqHB8ub4fYgb0AA_TtgZYIkAk0M65Bw0kdYnZsQ6WkmSjZ1H9A6VUDdPlHh7NqVe9QoownDrslpGh3JAPYRs4bu37qSbMaz-7Ob7kWmJVf8JW-LVulM2Tw4h8OLpirRi5xVdLQ';
		// if receiver is null return false
		if ($to <> '') {
			// set the headers
			//$list_header[] = "Content-Type:application/json";
			//$list_header[] = "Authorization:key=".$key;
			$headers = array(
			      'Authorization: key=' . $key,
			      'Content-Type: application/json'
			    );

			// default parameter for firebase
			

			$parameter['notification']['title'] = $title;
			$parameter['notification']['body'] = $text;
			$parameter['notification']['content_available'] = true;
			$parameter['notification']['click_action'] = "FLUTTER_NOTIFICATION_CLICK";
			$parameter['notification']['priority'] = $priority;
			$parameter['notification']['android_channel_id'] = "fengshui";
			$parameter['notification']['sound'] = $sound;

			$parameter['to'] = $to;
			$parameter['data']['kode_status'] = $kode_status;
			$parameter['data']['id_histori_transaksi'] = $id_histori_transaksi;
			$parameter['data']['priority'] = $priority;
			$parameter['data']['content_available'] = true;



			// if using some data to notif
			if (! empty($extends_data)) {
				for($i = 0; $i < count($extends_data); $i++) {
					$parameter['data'] = $extends_data[$i];
				}
			}
			/*cek($parameter);
			die();*/
			// send notif
			$send = $this->curl_request($this->url, $parameter, $headers);
			
			// decode hasil json
			$hasil = json_decode($send, true);
			$success = isset($hasil['success']) ? $hasil['success'] : 0;
			$failure = isset($hasil['failure']) ? $hasil['failure'] : 0;

			$result = ($failure == 1) ? FALSE : TRUE;

			return $result;
		} else {
			return FALSE;
		}
	}

	// curl request to firebase
	function curl_request($url='', $parameter='', $header='') 
	{
		$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($parameter),
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_FRESH_CONNECT => true
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

     /*   cek($response);
        die();
*/
        curl_close($curl);
        if ($err) {
            $response = ($err);
        } else {
            $response;
        }
        return $response;
	}
}
?>