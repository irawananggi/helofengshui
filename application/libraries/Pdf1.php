<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Pdf1 {
    
    function Pdf1()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($options=null)
    {
        include_once APPPATH.'/third_party/mpdf60/mpdf.php';
           if (!empty($options)) {
               $parameters=$options;
           }else{
           
   }
            
        
        return new mPDF($parameters['mode'], $parameters['format'],
                         $parameters['default_font_size'], $parameters['default_font'],
                         $parameters['margin_left'], $parameters['margin_right'],
                         $parameters['margin_top'], $parameters['margin_bottom'],
                         $parameters['margin_header'], $parameters['margin_footer'],
                         $parameters['orientation']);
    }
}


