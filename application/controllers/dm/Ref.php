<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* v:4.2
*/
class Ref extends CI_Controller {
    var $debug=1;
    function __construct() {
        parent::__construct();
        $this->load->helper('cmd');
        if(not_login(uri_string()))redirect('login');
        #$this->load->config('present');
        #if(sesi('id_pegawai')!=303)show_404();
    }

    function index(){
        redirect(uri(1).'/'.uri(2).'/data/');
    }
    
    function data($tipe=null,$search = null,$offset = null) {
        $this->benchmark->mark('awalz');
        $search_param=$o="";
        //cek(post(null));die();
        if(is_null($tipe)) $this->index();
        //$key = $this->input->post('search');
        $p=post(null);
        if(isset($p['search']))redirect(change_search_uri(uri_string(),$p['search']));
        
        //if($key = post('search'))redirect(uri_string().'/'.in_de(array('search'=>$key)));
        $modex = "list_data";
        if (!empty($search)) {
            $o = un_de($search);/*if(empty($o['modex']))$o['modex']=$modex;$modex = !empty($o['modex'])?$o['modex']:"list_data";*/
            if(empty($o['modex'])){$o['modex']=$modex;}else{$modex = $o['modex'];}
            $form_id = @$o['form_id'];
            $search_param = !empty($o['search']) ? $o['search'] : null;
        } 
        
        /*
        $search_param="";
        
        #if(is_null($tipe)) $this->index();
        //$key = $this->input->post('search');
        $p=post(null);
        if(isset($p['search']))redirect(change_search_uri(uri_string(),$p['search']));
        
        //if($key = post('search'))redirect(uri_string().'/'.in_de(array('search'=>$key)));
        $modex = "list_data";
        if (!empty($search)) {
            $o = un_de($search);
            if(empty($o['modex'])){$o['modex']=$modex;}else{$modex = $o['modex'];}
            $form_id = @$o['form_id'];
            $search_param = !empty($o['search']) ? $o['search'] : null;
        } 
        */
        
        if($tipe){
            $param=load_cnt(uri(1),'Param_'.uri(1),$tipe,$o); 
            $param['inti']=$tipe;
        }
        if(!empty($o['ret_save']))$param['ret_save']=$o['ret_save'];
        if(is_array(@$param)){
            $param = array_merge_recursive(array(
                    'search' => $search_param,
                    'param_search'=>$search,
                    'module' => $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/',
                    'page'  => $tipe,
                    'this'  => 'ref'
            ),$param);
            $param['heads']=array('select2');
            $this->benchmark->mark('akhirz');
            if($this->debug) echo komen($this->benchmark->elapsed_time('awalz','akhirz'));
            
            #if($tipe=='operator'){stop($param);}
            
            $this->load->library('Defa');
            
            switch($modex) {
                case "list_data" : 
                    if(isset($param['redir'])){
                        redirect($param['redir_path'].'/'.$param['redir'].'/'.$param['redir_func'].'/'. @$param['redir_param']);
                        //load_controller($param['redir_path'],$param['redir'],$param['redir_func'], $param['redir_param']); 
                    }else{
                        if(!(in_array($offset ,array('excel','cetak')))){
                            $param['return_object']=1;
                            if($tipe) $data=$this->defa->list_data( $param, $offset); 
                            $data['menu_nav']=load_cnt('dm','Builder','dmnav',uri2('1,2,3,4'));
                            $data['content'] = "dm/standard_view";
                            $this->load->view('dm/home', $data);
                            
                        }else{
                            $this->defa->list_data( $param, $offset); 
                            //load_controller('dm','def','list_data', $param, $offset); 
                        }
                    }
                    break;
                #case "form_mode" : load_controller('dm','def','form_data', $param, $form_id); break;//$this->folder
                case "form_mode" : 
                    if(@$o['ret_save'])$param['ret_save']=$o['ret_save'];
                    $this->defa->form_data( $param, $form_id); break;//$this->folder
                #case "save_mode" : load_controller('dm','def','save_data',$param); break;    //$this->folder
                case "save_mode" : 
                #stop(array($param,post()));
                $this->defa->save_data($param); break;    //$this->folder
                }
        }
            
    }
    
    function cekinde($inde="") {
        #$this->load->helper('appcmd');
        #cek(app_param());
        $tabel=form_open(uri2('1,2,3'))
            .form_label("in_de")
                .form_input('inde','','class="form_control" style="width: 50%;"')."<br>"
            .form_label("enkg")
                .form_input('enk','','class="form_control" style="width: 50%;" ')."<br>"
            .form_label("enkrg")
                .form_input('enkr','','class="form_control" style="width: 50%;"')."<br>"
            .form_label("enkser")
                .form_input('enks','','class="form_control" style="width: 50%;" ')."<br>"
            .form_label("enkrser")
                .form_input('enkrs','','class="form_control" style="width: 50%;"')."<br>"
            .form_submit('','cek')
            .form_close();
        if($p=post(null)){
            if(!empty($p['inde'])){
                $pe=$p['inde'];
                $retst=implode_array(un_de($pe));
            }elseif(!empty($p['enk'])){
                $pe=$p['enk'];
                $retst=implode_array(dekrip($pe,1,1));
            }elseif(!empty($p['enkr'])){
                $pe=$p['enkr'];
                $retst=implode_array(dekrip($pe,1));
            }elseif(!empty($p['enks'])){
                $pe=$p['enks'];
                $retst=dekrip($pe,1,1,1);
            }elseif(!empty($p['enkrs'])){
                $pe=$p['enkrs'];
                $retst=implode_array(dekrip($pe,1,0,1));
            }
            $ln=strlen($pe);
            $ln2=strlen($retst);
            $tabel.="<pre>"
                ."P : ".$pe."<br>"
                ."sz : ".$ln."<br>"
                .$retst."<br>"
                ."sz2 : ".$ln2
                /*.(!empty($p['inde'])?$p['inde']."\n sz: ".strlen($p['inde'])."\n"
                    .implode_array(un_de($p['inde']))
                    :"")
                .(!empty($p['enk'])?$p['enk']."\n sz: ".strlen($p['enk'])."\n"
                    .dekrip($p['enk'],1,1)
                    :"")
                .(!empty($p['enkr'])?$p['enkr']."\n sz: ".strlen($p['enkr'])."\n"
                    .implode_array(dekrip($p['enkr'],1))
                    :"")*/
                ."</pre>";
        }
        $data['tabel']=
            div(div('<button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>','class="pull-right box-tools"'),'class="box-header"')
            .div(
            $tabel
            ,'class="box-body"');
        $data['menu_nav']=load_cnt('dm','Builder','dmnav');
        $data['content'] = "dm/standard_view";
        $this->load->view('dm/home', $data);
    }
    
    function add_operator($param= null,$id = 0) {
    
        $data['title']    = ($id?'Ubah':'Buat').' Kewenangan Operator';

        $row = !empty($id) 
            ? $this->general_model->datagrabe(array(
                'tabel' => 'peg_pegawai','where' => array('id_pegawai' => $id),
                ))->row() 
            : null;
        
        $kwen="";
        if(($role=$this->general_model->datagrabe(array(
             'tabel'=>array(
                'ref_role r' => '',
                'ref_aplikasi a' => array('a.id_aplikasi = r.id_aplikasi','left'),
                'pegawai_role pr' => array('pr.id_role = r.id_role and pr.id_pegawai ='.$id,'left'),
             ),
             'select'=>'r.*,a.nama_aplikasi,if(isnull(pr.id_peg_role),0,1)ada',
             'order'=>'a.id_aplikasi',
             'where' => array('a.aktif'=>1),             
            )))&&( $role->num_rows() >0))
            foreach($role->result() as $ro)  
            $kwen .= '<p>
                <input type="checkbox" name="role[]" class="incheck" 
                    style="margin-top: -2px" 
                    value="'. $ro->id_role.'" '.($ro->ada==1?'checked':'').'> &nbsp; '.$ro->nama_role.' (<b>'.$ro->nama_aplikasi.'</b>) '.$ro->id_role.' </p>';
        #--------
        $form_data="";
        
        $form_data.=
            form_label('Nama Operator')
            .form_input('nama',!empty($row)?$row->nama:null,'class="form-control" placeHolder="Nama ..." '.(!empty($row->nama)?'readonly':'').' required')
            .form_label('Username')
            .form_input('username',!empty($row)?$row->username:null,'placeholder="Username ... " class="form-control" required')
            .'<b>Kewenangan</b></span><div class="clear" style="height: 10px">&nbsp;</div>'
            .$kwen;
    
        $data['hidden']=array(
            'id_pegawai'=>!empty($row)?$row->id_pegawai:null,
            'param'=>$param,
            );
        $data['form_data']=$form_data;
        $data['form_link']='dm/Ref/save_operator';
        $this->load->view('dm/form_view',$data);
        
    }
    
    function save_operator(){
        $p=post();
        $par=un_de($p['param']);
        $uname=str_replace(" ","_",strtolower(trim($p['username'])));
        #stop(array($p,$par,$uname));
        
        if ($uname != 'nimda') {
            /*if( $this->general_model->datagrab(array('tabel' => 'peg_pegawai','where' => array('username' => $uname)))->num_rows()>0){
                $this->session->set_flashdata('fail','Username sudah tersedia!');
            }else{*/
                $simpan = array('username' => $uname,);
                if (!$p['id_pegawai'] ) $simpan['password'] = md5($uname);
                
                $idp=$this->general_model->save_data('peg_pegawai',$simpan,'id_pegawai',$p['id_pegawai']);
                if ($p['id_pegawai'] )$idp=$p['id_pegawai'];
                #$this->db->where(array('id_pegawai'=>$p['id_pegawai']))->update('peg_pegawai',$simpan);
                
                $this->db->where(array('id_pegawai'=>$idp))->delete('pegawai_role');
                
                $simpan_role=array();
                if(!empty($p['role']) && count($p['role'])>0){
                    foreach ($p['role'] as $r) 
                        $simpan_role[]=array(
                            'id_pegawai'=>$idp,
                            'id_role'=>$r,
                        );
                    $this->db->insert_batch('pegawai_role',$simpan_role);
                }
                $this->session->set_flashdata('ok','Data berhasil di simpan');
            /*}*/
        }else{
            $this->session->set_flashdata('fail','Tidak diijinkan menggunakan <i>Username</i> &nbsp; tersebut ...');
        }
        
        if(!empty($par['ret']))redirect($par['ret']);
        
    }
    
    function db($fold,$param) {
        extract(un_de($param));
        $tabel="";
        if($tes=load_cnt($fold,'Db','init')){
            foreach ($tes as $key =>$val) $tabel.=$key." : ".$val."<br>";            
            $filename_unix= substr( $_SERVER['SCRIPT_FILENAME'],0,-9).'application/controllers/'.$fold."/db.php";
            $filename = str_replace( '/',"\\", substr( $_SERVER['SCRIPT_FILENAME'],0,-9).'application/controllers/'.$fold."/db.php" );
            if(file_exists($filename_unix))$filename=$filename_unix;
            $tabel.="Fs: ".($filename==$filename_unix?"Unix":"Windows")."<br>";
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));
            fclose($handle);
            $tabel.=form_textarea('db',$contents,'class="form-control"');
        }
        
        #$tabel.=implode_array($tes);
        #$retbtn=anchor($ret,'<< Balik')
        $data['title']="FOr ".$fold;
        $data['form_link']="";
        $data['form_data']=$tabel;
        $this->load->view('dm/form_view',$data);
    }
    
    
    function backup_db() {
        load_cnt(uri(1),'Builder','backup_db');
    }
    
    
    
    function rst_operator($param= null,$id = 0){
        extract(un_de($param));
        $simpan=array('password'=>md5('qwerty'));
        $this->db->where(array('id_pegawai'=>$id))->update('peg_pegawai',$simpan);
        setflashsesi('ok','Reset Done');
        if(!empty($ret))redirect($ret);
    }
    
    function info4() {
        $tabel="";
        $tabel.="Db: ".$this->db->database;
        
        $data['title']="info 4";
        $data['tabel']=$tabel;
        $data['menu_nav']=load_cnt('dm','Builder','dmnav');
        $data['content'] = "dm/standard_view";
        $this->load->view('dm/home', $data);
        
    }
    
    function app_set($id_app=0,$parm=0) {
        if($id_app){
            extract(un_de($parm));
            if(!empty($aktif))$this->db->where('id_aplikasi',$id_app)
                ->update('ref_aplikasi',array('aktif'=>$aktif));
            #stop(array($aktif,$id_app,$this->db->last_query()));
            if(!empty($ret))redirect($ret);
        }
    }
    
    function pegrole($id_role,$title) {
        redirect(uri2('1,2,').'data/pegrole/'.in_de(array(
            'id_role'=>$id_role,
            'title'=>urldecode( $title ),
            'ret'=>uri_string(),
        )));
    }
    
    function ref_app() {
        $app = $refapp=array();
        if(($dr=$this->general_model->datagrabe(array(
             'tabel'=>'ref_aplikasi',
            )))&&($dr->num_rows()>0))
            foreach ($dr->result() as $vdr) 
                $refapp[$vdr->folder]=array(
                    'id'=>$vdr->id_aplikasi,
                    'aktif'=>$vdr->aktif,
                );
        $this->load->helper('directory');
        $map = directory_map('./application/controllers/', 0,0);
        foreach($map as $x => $isi) 
            if(is_array($isi))$app[$x]=(array_search('db.php',$isi)>0)?(!empty($refapp[$x])?1:2):0;                
            
            //if (!preg_match("/\./i", $o) and file_exists('./application/controllers/'.$o.'/db.php') and $o <> "referensi") $app[] = $o;
        
        $tr="";
        if(is_array($app)){
            $tr.=tr(th("App"));
            foreach ($app as $x => $v) 
                $tr.=tr(td(
                    ($v==2?tag('a','<i class="fa fa-plus"></i>',
                        'class="btn btn-primary btn-xs btn-edit" 
                         act="'.(site_url($x.'/db')).'"'):($v==1?'<i class="fa fa-check bg-green"></i>':' <i class="fa fa-remove"></i>'))
                    ." ".$x." $v"
                    
                ));
            $tr=table($tr,'class="table "');
        }
        $script="
                \$('.btn-form-cancel').click(function() {
                    \$('#form-content,#form-title').html('');
                    \$('#form-box').slideUp('fast', function() {
                        \$('#box-main').slideDown();
                    });
                });
                \$('.btn-edit').click(function() {
                    var abc=\$(this);
                    \$.ajax({
                      url: \$(this).attr('act'),
                      type: 'POST',
                      cache: false,
                      dataType: 'json',
                      data:\$(this).serialize()+'&g=x',
                      success: function(msg) {
                        if(parseInt(msg.sign)==1){
                            abc.append('&nbsp; xyxyxx ');
                            alert('OK ' + msg.text);
                            \$(this).append('&nbsp; xxxx ');
                            \$(this).append('&nbsp; <div>'+ msg.text +'</div>');
                          /*console.log(msg);
                          if(parseInt(msg.done)==1){
                            alert('OK');
                          }else{
                            \$(this).append('<div>'+ msg.text +'</div>');
                          } */
                        }
                      },
                      error:function(error){
                      
                      }
                    });
                });
                ";
        echo 
            $tr
            .'<br><a class="btn btn-danger btn-md btn-form-cancel" type="button">Tutup</a>'
            .tag_jqready($script)
            ;
        ;
    }
    
    function info() {
        echo phpinfo();
    }
    
    function icon() {
        $tabel=load_cnt(uri(1),'Icon','seeicon');
        $data['title']="Icon";
        $data['tabel']=$tabel;
        $data['menu_nav']=load_cnt('dm','Builder','dmnav');
        $data['content'] = "dm/standard_view";
        $this->load->view('dm/home', $data);
    }
    
    function enc($data,$gz=0,$kode=1) {
        if($kode==1){
            $enc=var_export($data,true);
        }elseif($kode==2){
            $enc=json_encode($data);
        }elseif($kode==3){
            $enc=serialize($data);
        }
        if($gz){
            $data_on = base64_encode(gzcompress($enc));
        }else{
            $data_on = base64_encode($enc);
        }
        return preg_replace(array('/\=/','/\+/','/\//'), array('-','_','~'), $data_on);
    }
    
    function denc($data,$array=false,$gz=0,$kode=1) {
        $on_data = preg_replace(array('/\-/','/\_/','/\~/'), array('=','+','/'), $data);
        $ret=base64_decode($on_data);
        if($gz)$ret=gzuncompress($ret);
        
        if($kode==1){
            eval("\$ret=".$ret.";");
        }elseif($kode==2){
            $ret=json_decode($ret,$array);
        }elseif($kode==3){
            $ret=unserialize($ret);
        }
        return $ret;
    }   
    
    function tezzz() {
        $apa="??";
        $apaan="dah";
        $ilang="???";
        $tzz  ="eJzFGv1z2jj2d2b4HzTb6WBnXQ5z3dwmG2dKgFy5QsiAc3v9KZjCEQdMPMaZ3P33956eJEsyTp3uzK3bwfb7ftL7kOQczs_6p7_c~zQfjof9kDUbJ83G9Ww6cZqNZoOAzUbajlf36XoTvURxs_Gl7X2cwp31pzf9XuiMruH~zd147KTtzXoXZferdRrtPdZquUGrhXdFa1Owlut6DCRGSeQxkOQokiX8bqP9hgUoiE1nbDQ3tEgClKCpAJko0aZxWW~OUE3hyTsvW6Jrz~s4B3~UEw4AS7P1Yb3PJTVLN83GeHgdsn9MRzcMoAWGTQHQXhSDtAjSjfEOun7~PJwNmdAR_J1O5_xvXRjwKvg7sgL0PEbLKI~2LH0EqGUCuLaNcjCO27CxjHg03lnvZsBpDnmUPx8WgW~Iy9b~VqqyR5SXEb8AKnnyvcS9jFc4X9mSMy85McEWgZAlXsuOvDaWj9ZYqtFEjO4MOij04mgu9OGswgFqOhsMZ_zqKxOaNk_77HkxGM77Hg5XnuRyoEH5eDQZhewMLjTETbWo0JIGNBo5U0TZ~G4i0yV~8Tque8hfTHC6FQh8sDi2xLE1wck2Fyz8yRK3_0bSdt8sYREJi9BsHRH2bjkG7jZqFE4GXziSP5nI_WDOUXC3ECEJhLuJ6IccjkXGAA8EfGAjwi_CwVL9_Y4QcDcRgxHZBHfLybur4ZjcxCdLHMJIID5ZIoltYPEMCGoB5_Q53E3EmISMUQiUPcRwO1x2yTrMm~T_5aQEQDTT5~MpR1aggwhmnvg9QoBz~pSrev7uwx_59H5wQjZz88hcMtVrNkCPtAJCEcxoNn6WgHTH3zUIBZoBkgGmgSisdEB4awIoXgw5FCkGiGJEA1Bs6BRi2jUQzzj9PbJUX804wHX5WGvd88fHOzCu0rg7n5LoP~cwvCenHfjvXjCe9SvvE9w~wiQwuICOoDRBrD8cjSXkL8whTpedMGTyebzgjOka0p1UEDAoHiA~3WniEaQL5__m6HQnJKc7EpxvLzvep3yL4JRXMQRGHBgREKqR5wiP2GD0T3baYa4sa6REQqmigYTfh8Mvg95XkLTfbKKdG3z0up7vspftG2YkMP4Vo15Rx_EpW28jXAgJtfA06IXD__vpbALrEAH1Wu9XH94nH95~hUVMvtndxysgfMGfJDo84xikzzugJVdoeJne1uAVmhfeHUcsgUB5~BjvXZfIpzO6C27nZhqy45RCENBA7l12BFRipUX8Gdnx0c2knX_6eXKgLPskGN883rQYQ3NlhxrM8ZWaD281vK~wly~8Tl2DWoTsBrLwY6Hr3WKge2kEK99dFGOccjWI5FpKaKkbSzuot9HUIREZlkWToYAEM0tIaomIDMtaqS0CEhwqIclJQIKfJST5rhd101sxKCgan8vCi9GCilg27GrG8wRHGeXwtoetkvoiZTtmAqQ3EmKui9UPVonqDgb1sfyr96qkXWRn0qaw5Y_JjGp4VAmYtCkxtQ0Phk7W1rI9K9aQ~E3KydpFHHpxW0Q2f04AwatHe7nOou1DnHEiGCwtFdIHH5kenrer_OB6v5bQXR3tfwRpeZyuRf6oR6F__7RaSzuU11jaZeK1VepNZ46RkgUG18ygJUoD3_X5BkUVMZh~3~cA67lcQpqEhQq_J3HI3KDFs6ZFnFoG1eCEhBJ8KrXqcIW3kkvmXA0uSEHBpZKxDpdiKvOYc8sEWX3vIbWFcJXkNbgg5wWXyv4aXLwUCD6tLNSxEsmlnUW9qGOpVDior20secb1eQZSTX2OuYwgqmQ1Y_FqJoMBC2PRWmWFsvqqJrIoRbwrSopy7iIYlXK0TiMqFlFQay5QVMIMnKAPFJI3XfhBB5iLRZ0Zi4MKJ4QOVm75rLwuYIaX1WSGM~r6QSAYLI6JguDRfiXFxIf9825n5Z5caeDc8PZT3612PcesCXyNUJ8M0zfCsMuAJdqkvM090V~rOviaf69MGnTtC9WtWczNXsXsEO9j6iJcgOrS7KJ~N8NVtFNWY9knJmmr58_fMk0VEZiIAAwUCTnssXA0Gd6H0~v5sO~g82B0fa0EeSJeXXCOqYXR~8m~YkbeGIkXMhCxMlR5KEi8Qh5DJ2nRx8RVY9s8KXYpYmUomeWqT76LE1G_WPPS1DpHVCq9fm8eOmnSXvCxX~zMOKDV6Zx3~BYeHKMTrryzRNsn8UgsWI~jyN9Fom9gFBJPHLOn53RhwUVK2GCx9Tqq~bjaAqrh5Z45VbkH_2VtHoxDcArlNCnQ2vGtPCjn90MMu31_iJtqngUVfh6Roo7bj55oV01h8UQnw9rkamMW_DqCt_TJ9Cb8rA9C0OWZ8HXYmzn6LACi4~~KXFtC8XpElt95RdiZIUwczpc~BCDSTcTD27JDG2CnNj9cM9rqIIOdUfpWSMVSdjRYs4q5MndQajABjN8VDhphUfWYvQoyiq4YP91K2f3LQtWOTUFQJCS1BX1FuVkOj2gv_vMx~aUqQAYIJqZHs1Xfjs0IXifmK9Uz7WAsa2tHYy0fyhr_b8FjVzyq4maZK41QCcrnnmUmhfgeI1MgO5pNWTmZLCn8G83Rzzc6WaUuPdtsZWc~oMwVXroZ~6AlFQXFaoVkFFEe6CcHP5hx9Znwoi0~8tipfixWYu4v8uj5GFclKiD40YUFk4cYFlgdQRi5Dgjc9NgwdSyxMFNO0cNOs3fbojWePjRGMPLDiPi1fqKMYlnMZ5F0I3gRVBgtopnHkRoCEbLTmYKrYSCU4q9mP6tmPxOhxo2Mq0JKbiOF0AsrFHWplwXOfUskmkFlcaqZKA807TBY_uDzxk1HQZU_IP54OiFmkf8Xp8enI12lP9_m35bsuwZ0yYDudwzoVhrQlQZ0pQHqC3FBSpeY9LecTb57eVhna7Z~ypnYo2lrcUCjPCLR7P~rqX8qZ8SNjL9EEO5DLmX4RwIR9z7SojwqzgnJQUDyk9nFZeetH0VA_~KYdvz8~xBBimUPPMseuH6ELIIlP1QteBzxiZx~eToPfI~JD1rn0J04_COAO2KXxz8jFWTpjqjSnUmUb33zPYL3X9xki~b4fOVX~A2GOaxl_HfH5dUxsz7fNRsqfJbFaTTO4h~7JPvBbGgiOL41G3_fTe9uUZ3eiwAl2pjmp~mXPb_9waKffvsf3DIzxw--";
        $enc=$this->enc($apa,1,2);
        $denc=dekrip($enc,0,1,0);#$this->denc($enc);
        $dencz=dekrip($tzz,1,1,1);#$this->denc($tzz,1,1,3);
        $lndencz=strpos('#<--------- ilang di sini    
    (m.tanggal ',$dencz,0);
        $ln2=substr($dencz,$lndencz+15,20);
        #echo "<pre>".$ss1.$ss2."<br><br>";
        #echo $dencz."</pre>";
        cek(array(
            'l1'=>$lndencz,
            'l2'=>$ln2,
            'a'=>md5($apa),
            'xx'=>strcoll($ilang,$tzz),
            'sa'=>strlen($apa),
            'sil'=>strlen($ilang),
            'slntzz'=>strlen($tzz),
            'e'=>$enc,
            'se'=>strlen($enc),
            'dc'=>$denc,
            'd'=>md5($denc),
            'dz'=>md5($dencz),
            'sedz'=>strlen($dencz),
            
        ));
        
        $enc2=$this->enc($apa,1,2);
        $denc2=$this->denc($enc2,1,1,2);
        cek(array(
            'e'=>$enc2,
            'se'=>strlen($enc2),
            'd'=>md5($denc2),
        ));
        $enc3=$this->enc($apa,1,3);
        $denc3=$this->denc($enc3,1,1,3);
        cek(array(
            'e'=>$enc3,
            'se'=>strlen($enc3),
            'd'=>md5($denc3),
        ));
    }
    
    /**
    * redirect with par-> requ:url,key,add
    */
    function redir($par,$u1="",$u2="",$u3="",$u4="",$u5="") {
        extract(un_de($par));
        $sr=array();
        if(!empty($add))$sr=$add;
        if($u1)$sr[$key[0]]=$u1;
        if($u2)$sr[$key[1]]=$u2;
        if($u3)$sr[$key[2]]=$u3;
        if($u4)$sr[$key[3]]=$u4;
        if($u5)$sr[$key[4]]=$u5;
        redirect(add_uri_search($url,$sr));
    } 
    
    function form_move_unker($idgr,$ret) {
        $this->load->library('Defa');
        $param=array(
            'title'=>"Impor dari",
            'ret_save'=>un_de($ret),
            'save_to'=>uri2('1,2,').'/save_form_move_unker',
            'form_button'=>'<br><a class="btn btn-danger btn-md btn-form-cancel" type="button">Batal</a>
                            <button href="#" class="btn btn-success btn-md btn-save-act pull-right">Pindah</button>',
            'tabel'=>'present_group',
            'form'=>array(
                array(0,0,'-','id_gr',$idgr),
                array(9,false,'Grup','id_group',uri(1).'/autocomplete/grup_imp/'.$idgr,'','nama_group'),
            ),
            
        );
        $this->defa->form_data( $param); 
    }
    
    function save_form_move_unker() {
        $p=post();
        if($p)extract($p);
        $sql="INSERT INTO present_presensi ( id_pegawai,id_group)
            (
            SELECT p.id_pegawai,($id_gr)t
            FROM present_presensi p
            LEFT JOIN (SELECT id_pegawai FROM present_presensi WHERE id_group=$id_gr)s ON s.id_pegawai=p.id_pegawai
            WHERE p.id_group=$id_group AND ISNULL(s.id_pegawai)
            );";
        $this->db->query($sql);
        redirect($ret_save);
    }
    
    function add_kewenangan($id_role = null){
       $app_ac = $this->general_model->get_param('app_active');
       $d=array('unit' => true,'app' => 0,'dir' => 'dm/ref/data');
       $this->in_app = 'dm';
        $data['breadcrumb'] = array('' => $this->in_app, $d['dir'].'/kewenangan/add_kewenangan/'.$id_role => 'Kewenangan');
        
        $in=array('folder'=>dir_at());
        $data['aplikasi'] = $this->general_model->datagrabe(array('tabel' => 'ref_aplikasi','in' => $in,'order' => 'urut'));
        $data['param'] = $d;
        $data['combo_aplikasi'] = ($data['aplikasi']->num_rows() > 1) ? $this->general_model->combo_box(array('tabel' => 'ref_aplikasi','key' => 'id_aplikasi','val' => array('nama_aplikasi'),'in' => $in,'order' => 'urut')) : null;
        $data['unit'] = $d['unit'];
        
        $t_unit = $this->general_model->check_tab('ref_unit');
        
        if (isset($d['unit']) and $d['unit'] == TRUE and $t_unit) {
            
            if ($this->general_model->check_tab('ref_role_unit')) {

            $data['unit_data'] = $this->general_model->datagrab(array(
                'tabel' => 'ref_unit',
                'where' => array('aktif' => 1),
                'order' => 'urut,unit'
            ));
            
            if ($id_role != null) {
                $e = array();
                $unit_exist = $this->general_model->datagrab(array('tabel' => 'ref_role_unit','where' => array('id_role' => $id_role),'select' => 'id_unit'));
                foreach($unit_exist->result() as $u) { $e[] = $u->id_unit; }
                $data['exst_unit'] = $e;
            }
            
            }
        }
        
        #//---------
        $c = array();
        $dat =    $this->general_model->datagrabe(array(
            'tabel' => 'ref_role_nav',
            'where' => array('id_role' => $id_role)
        )); 
        
        foreach($dat->result() as $d) $codes[] = $d->id_nav;
        
        #//--------------    
        
        $app =$this->general_model->datagrabe(array(
            'tabel' => 'ref_aplikasi','in' => $in,'order' => 'urut'));
                
        $app_data = array();
        $nav = array();
        
        foreach($app->result() as $ap) {
            
            $data_nav = null;
            
            /* -- Untuk Referensi -- */
            
            if ($ap->folder == 'referensi') {
                $data_nav = array();
            
                foreach($app->result() as $apr) {
                    $data_refnav = null;
                    $n1 = $this->role_sub(2,$apr->id_aplikasi);

                    foreach($n1->result() as $ne1) {
                        $check_sub = (!empty($codes) and in_array($ne1->id_nav,$codes)) ? 'checked' : null;
                        $jud = ($ne1->tipe == 1) ? ' &nbsp;<strong class="text-orange"> <i class="fa fa-star fa-btn"></i> '.$ne1->judul.'</strong>' : $ne1->judul;
                        $data_refnav .= '<p><input type="checkbox" class="incheck" name="code[]" value="'.$ne1->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';

                        $n2 = $this->role_sub(2,$apr->id_aplikasi,$ne1->id_nav);
                        foreach($n2->result() as $ne2) {
                            $check_sub = (!empty($codes) and in_array($ne2->id_nav,$codes)) ? 'checked' : null;
                            $jud = ($ne2->tipe == 1) ? ' &nbsp;<strong class="text-orange"> <i class="fa fa-star fa-btn"></i> '.$ne2->judul.'</strong>' : $ne2->judul;
                            $data_refnav .= '<p style="padding-left: 30px"><input type="checkbox" class="incheck" name="code[]" value="'.$ne2->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';
                            
                            $n3 = $this->role_sub(2,$apr->id_aplikasi,$ne2->id_nav);
                            foreach($n3->result() as $ne3) {
                                $check_sub = (!empty($codes) and in_array($ne3->id_nav,$codes)) ? 'checked' : null;
                                $jud = ($ne3->tipe == 1) ? ' &nbsp;<strong class="text-orange"> <i class="fa fa-star fa-btn"></i> '.$ne3->judul.'</strong>' : $ne3->judul;
                                $data_refnav .= '<p style="padding-left: 60px"><input type="checkbox" class="incheck" name="code[]" value="'.$ne3->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';
                                
                            }
                            
                        }
                        
                    }
                    $data_nav[$apr->nama_aplikasi] = $data_refnav;
                }
            } else {
            
                $n1 = $this->role_sub(1,$ap->id_aplikasi);

            foreach($n1->result() as $ne1) {
                $check_sub = (!empty($codes) and in_array($ne1->id_nav,$codes)) ? 'checked' : null;
                $jud = ($ne1->tipe == 1) ? ' &nbsp;<strong class="text-orange"><i class="fa fa-star fa-btn"></i> '.$ne1->judul.'</strong>' : $ne1->judul;
                $data_nav .= '<p><input type="checkbox" class="incheck" name="code[]" value="'.$ne1->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';

                $n2 = $this->role_sub(1,$ap->id_aplikasi,$ne1->id_nav);
                foreach($n2->result() as $ne2) {
                    $check_sub = (!empty($codes) and in_array($ne2->id_nav,$codes)) ? 'checked' : null;
                    $jud = ($ne2->tipe == 1) ? ' &nbsp;<strong class="text-orange"><i class="fa fa-star fa-btn"></i> '.$ne2->judul.'</strong>' : $ne2->judul;
                    $data_nav .= '<p style="padding-left: 30px"><input type="checkbox" class="incheck" name="code[]" value="'.$ne2->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';
                    
                    $n3 = $this->role_sub(1,$ap->id_aplikasi,$ne2->id_nav);
                    foreach($n3->result() as $ne3) {
                        $check_sub = (!empty($codes) and in_array($ne3->id_nav,$codes)) ? 'checked' : null;
                        $jud = ($ne3->tipe == 1) ? ' &nbsp;<strong class="text-orange"><i class="fa fa-star fa-btn"></i> '.$ne3->judul.'</strong>' : $ne3->judul;
                        $data_nav .= '<p style="padding-left: 60px"><input type="checkbox" class="incheck" name="code[]" value="'.$ne3->id_nav.'" '.$check_sub.' style="margin: -2px 0 0 15px"> '.$jud.'</p>';
                        
                    }
                    
                }
                
            }
            }
            
            $app_data[$ap->id_aplikasi] = $data_nav;
            
        }
        $data['link_save'] = 'inti/akses/save_kewenangan';
        $data['app_data'] = $app_data;
        $data['title']    = !empty($id_role) ? 'Ubah':'Tambah';
        $data['def']    = !empty($id_role) ? $this->general_model->datagrab(array('tabel' => 'ref_role','where' => array('id_role' => $id_role)))->row() : null;
        
        $this->load->view('umum/kewenangan_form', $data);
    }
    
    function role_sub($ref,$app,$id = null) {
        
        $where_is = (!empty($id)) ? 'id_par_nav = '.$id : 'id_par_nav IS NULL';
        
        $dat = $this->general_model->datagrab(array(
            'tabel' => 'nav',
            'where' => array($where_is.' AND ref = '.$ref.' AND id_aplikasi = '.$app .' AND aktif = 1'=> null),
            'order' => 'urut'
        )); return $dat;
        
    }
    
    function kewenangan($folder) {
        cek($folder);
        $data= $this->data('kewenangan',in_de(array('ret_data'=>1))); 
        cek($data);
        $data['content'] = "dm/standard_view";
        $this->load->view('dm/home', $data);
    }
    
    function base64decode($str) {
        cek(base64_decode($str));
    }
    
    function list_table($tname) {
        $o['kolom_data'] = array('nip','nama','telepon','email','username','id_unit');
        $fld_tabel=field_list($tname);
        $ret=array_intersect($fld_tabel,$o['kolom_data']);
        cek($ret);
    }
}
  
?>