<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  class Param_dm{
      var $folder='dm';
      var $tipe='';
      var $btn_xs="btn btn-xs";
      
      function loadci() {
        $this->CI =& get_instance();
      }
          
      function kewenangan(){
        $sql_peg="SELECT id_role, COUNT(*)jm_peg FROM pegawai_role GROUP BY id_role";
        $sql_unker="SELECT id_role, COUNT(*)jm_un FROM ref_role_unit GROUP BY id_role";
        $pri_table="ref_role";
        $param = array(
            'title' => 'Kewenangan',
            'tabel' => array(
                "$pri_table rr"=>'',
                'ref_aplikasi ra'=>array('ra.id_aplikasi=rr.id_aplikasi','left'),
                "($sql_peg) pr"=>array('pr.id_role=rr.id_role','left'),
            ),
            'id' => 'id_role',
            'kolom' => array('Kewenangan','Aplikasi','Pengguna'),//header tabel
            'select' => "rr.*,ra.`nama_aplikasi`,pr.jm_peg,if(ra.aktif,0,1) is_app",//fild tabel yang dipilih
            'kolom_tampil' => array('nama_role','nama_aplikasi','jm_peg'),//isi tabel
            'kolom_data' => array('nama_role','id_aplikasi'),//jangan di kasih nama tabel di depan nya
            'order' => 'rr.id_aplikasi',
            'aksidikiri'=>1,
            'pageper'=>30,
            'form' => array(
                    array(1,TRUE,'Kode','kode','form-control read-only'),
                    array(1,TRUE,'Data','data','form-control'),
            ),
            'tombol'=>array(
                array(3,'dm/Ref/add_kewenangan','<i class="fa fa-plus-square"></i> &nbsp; Tambah','title="Tambah Kewenangan"'),
            ),
                        
        );
        if(is_tabel_exist('ref_role_unit')){
            $param['tabel']["($sql_unker) ur"]=array('ur.id_role=rr.id_role','left');
            $param['kolom'][]='Unit';
            $param['select'].=',ur.jm_un';
            $param['kolom_tampil'][]='jm_un';
        }
        $param['kolom_cari']=$param['kolom_tampil'];
        $param['tombol_aksi']=array(
            'Edit'=>array(3,
                $param['id'],
                anchor('#'
                    ,'<i class="fa fa-pencil"></i>'
                    ,'class="btn btn-xs btn-warning btn-edit" act="'
                        .site_url('dm/Ref/add_kewenangan/&&'.$param['id'].'&').'"'
                    ),
                '',
                
            ),
            'hapus'=>array(3,
                'jm_peg',
                '',
                anchor('#'
                    ,'<i class="fa fa-trash"></i>'
                    ,'class="btn btn-danger btn-delete btn-xs" act="'
                        .site_url('dm/Def/del/&&'.$param['id'].'&/'.in_de(array(
                            'id_tabel' =>$param['id'],
                            'tabel'=>$pri_table,
                            'ret'=>uri_string())))
                        .'" msg="Apakah Anda ingin menghapus data ini?"'),
                
            ),
            'cek'=>array(3,
                'jm_peg',
                anchor(
                    'dm/Def/redir/'.in_de(array(
                            'url'=>uri2('1,2,3,').'pegrole',
                            'key'=>array('id_role','title'),
                            'add'=>array(
                                'ret'=>uri_string(),
                            ),
                        ))."/&&".$param['id'].'&/&&nama_role&'
                    ,'<i class="fa fa-user"></i>'
                    ,'class="btn btn-primary btn-xs"'),
                ''
            ),
            'is_app'=>array(3,
                'is_app',
                div('<i class="fa fa-ban"></i>'
                    ,'class="btn btn-xs"'),
                '',
                
            ),
        );
        return $param;
    }
    
    function pegrole($o="") {
        $id_role=0;
        if(!empty($o['id_role']))$id_role=$o['id_role'];
        $uri_ret=(!empty($o['ret'])?$o['ret']:uri2('1,2,3,').'kewenangan');
        $param = array(
            'first_title'=>anchor( $uri_ret,'<< ','class="btn btn-default"')." ",
            'title' => 'User '.(!empty($o['title'])?urldecode( $o['title']):""),
            'tabel' => array(
                'peg_pegawai p'=>'',
                'pegawai_role pr'=>array('pr.id_pegawai=p.id_pegawai','left'),
            ),
            'tabel_save' => 'pegawai_role',
            'where'=>array('pr.id_role'=>$id_role),
            'id' => 'id_pegawai',
            'can' => array(),
            'kolom' => array('Nip','Nama','User'),
            'select' => "p.id_pegawai
                , p.nip
                , p.username
                , CONCAT(IF(IFNULL(p.gelar_depan, '')='','',CONCAT(p.gelar_depan, ' ')) , p.nama, IF(((p.gelar_belakang = '') OR ISNULL(p.gelar_belakang)),'',CONCAT(' ',p.gelar_belakang))) AS nama_pegawai
                , pr.id_peg_role",
            'kolom_data' => array(''),
            'kolom_tampil' => array('nip','nama_pegawai','username'),
            'kolom_cari' => array('nip','nama_pegawai','username'),
            'order' => 'nama_pegawai',
            'tombol' => array(),
            'aksidikiri'=>1,
            'form'=>array(),
            'no_aksi'=>1,
            #no plugins
            'pageuri'=>6,
            'pageper'=>20,
            'debug_list'=>1,
            'bc'=>uri2('1,2,3,').'kewenangan',
            'tombol_aksi'=>array(
                'hapus'=>array(3,
                    'id_peg_role',
                    anchor('#'
                        ,'<i class="fa fa-trash"></i>'
                        ,'class="btn btn-danger btn-delete btn-xs" act="'
                            .site_url('dm/def/del/&&id_peg_role&/'.in_de(array(
                                'id_tabel' =>'id_peg_role',
                                'tabel'=>'pegawai_role',
                                'ret'=>uri_string())))
                            .'" msg="Apakah Anda ingin menghapus data ini?"'),
                    '',
                    
                ),
            ),
            
        );
        
        return $param;
    }
    
    function operator($o=""){
        $sql_peg="SELECT id_pegawai, COUNT(*)jm FROM pegawai_role GROUP BY id_pegawai";
        $param = array(
            'title' => 'Operator',
            'tabel' => array(
                "peg_pegawai p"=>'',
                "($sql_peg) pr"=>array('pr.id_pegawai=p.id_pegawai','left'),
            ),
    //                    'tabel_save' => 'rs_ref_desa',
    //                    'where'=>array('b.kabupaten'=>'Kutai Kartanegara'),
            'id' => 'id_pegawai',
            #'can' => array('edit'),//bisa hapus dan edit
            'kolom' => array('Nama','Nip','Uname','Kewenangan','LL','Jumlah<br>Kewenangan'),//header tabel
            'select' => "p.id_pegawai
                        , p.nip
                        , p.username
                        , CONCAT(IF(IFNULL(p.gelar_depan, '')='','',CONCAT(p.gelar_depan, ' ')) , IFNULL(p.nama, ''), IF(((p.gelar_belakang = '') OR ISNULL(p.gelar_belakang)),'',CONCAT(' ',p.gelar_belakang)),'(',p.id_pegawai,')') AS nama_pegawai 
                        , pr.jm
                        ,p.last_login
                        , DATE_FORMAT(p.last_login,'%d/%m/%Y') lastlo
                        ,(0)jmx
                        ",//fild tabel yang dipilih
            'kolom_tampil' => array('nama_pegawai','nip','username','jmx','lastlo','jm'),//isi tabel
            'kolom_data' => array('id_role','id_pegawai'),//jangan di kasih nama tabel di depan nya
            #'kolom_cari' => array('nama_wilayah','kode_wilayah'),//harus di kasih nama tabel di depan nya jika ada join
            'order' => 'p.last_login DESC,pr.jm DESC',
            'detail_row'=>array(
                'jmx'=>array(1,'dm','param_dm','isi_wenang',array('row'=>'id_pegawai')),
            ),
            'debug_list' => 1,
            'aksidikiri'=>1,
            'pageper'=>50,
            'form' => array(),
            'tombol'=>array(
                array(3,'dm/Ref/add_operator/'.in_de(array('ret'=>uri_string())),'<i class="fa fa-plus-square"></i> &nbsp; Tambah','title="Tambah Operator"'),
            ),
            
        );
        $param['tombol_aksi']=array(
            'Edit'=>array(3,
                $param['id'],
                anchor('#'
                    ,'<i class="fa fa-pencil"></i>'
                    ,'class="btn btn-xs btn-warning btn-edit" act="'
                        .site_url('dm/ref/add_operator/'.in_de(array('ret'=>uri_string())).'/&&'.$param['id'].'&').'"'
                    ),
                '',
                
            ),
            'reset'=>array(3,
                $param['id'],
                anchor('dm/ref/rst_operator/'.in_de(array('ret'=>uri_string())).'/&&'.$param['id'].'&'
                    ,'<i class="fa fa-refresh"></i>'
                    ,'class="btn btn-xs btn-danger "'),
                '',
                
            ),
        );
        if(is_tabel_exist('peg_jabatan')){
            $param['tabel']['peg_jabatan pj']=array('pj.id_pegawai=p.id_pegawai AND pj.status=1','left');
            $param['tabel']['ref_jabatan rj']=array('rj.id_jabatan=pj.id_jabatan','left');
            $param['tabel']['ref_eselon re']=array('re.id_eselon=rj.id_eselon','left');
            $param['tabel']['ref_bidang rb']=array('rb.id_bidang=pj.id_bidang','left');
            $param['tabel']['ref_unit ru']=array('ru.id_unit=rb.id_unit','left');
            $param['tabel']['peg_pangkat pp']=array('pp.id_pegawai=p.id_pegawai AND pp.status=1','left');
            $param['tabel']['ref_golru rp']=array('rp.id_golru=pp.id_golru','left');
            $param['select'].=", CONCAT(
                CONCAT(IF(IFNULL(p.gelar_depan, '')='', '', CONCAT(p.gelar_depan, ' ')), p.nama, IF(IFNULL(p.gelar_belakang, '') = '', '', CONCAT(' ', p.gelar_belakang))), '(', p.id_pegawai, ')', '
                ::', IFNULL(rp.pangkat,''), '(', IFNULL(rp.golongan,''), ')', '[', IFNULL(pp.id_peg_pangkat,''), '/', IFNULL(pp.id_golru,''), ']', '
                ::', IFNULL(rj.nama_jabatan,''), '(', IFNULL(re.eselon,''), ')', '[', IFNULL(pj.id_peg_jabatan,''), '/', IFNULL(rj.id_eselon,''), ']', '
                ::', IFNULL(rb.nama_bidang,''), '[', IFNULL(pj.id_bidang,''), ']', '
                ::', IFNULL(ru.unit,''), '[', IFNULL(rb.id_unit,''), ']')namajabatan";
            $param['kolom_tampil'][0]='namajabatan';
            
        }
        $param['kolom_cari']=$param['kolom_tampil'];
        
        return $param;
    }

    function isi_wenang($id_peg) {
        $this->loadci();
        $ret="";
        $this->CI->db->_reset_select();
            
        if(($dt=$this->CI->general_model->datagrabe(array(
         'tabel'=>array(
            'pegawai_role pr'=>'',
            'ref_role rr'=>array('rr.id_role=pr.id_role','left'),
            'ref_aplikasi ra'=>array('ra.id_aplikasi=rr.id_aplikasi','left'),
         ),
         'where'=>array('pr.id_pegawai'=>$id_peg),
         'select'=>'pr.id_pegawai,rr.nama_role,ra.nama_aplikasi',
        )))&&($dt->num_rows()>0)){
            foreach ($dt->result() as $r) $ret.=$r->nama_role." (<b>".$r->nama_aplikasi."</b>)"."<br>" ;
        }
        return $ret;
    }
    
    function ref_app(){
        $sql_nav="SELECT id_aplikasi, COUNT(*)jm FROM nav GROUP BY id_aplikasi";
        $param = array(
            'title' => 'Referensi App',
            'tabel' => array(
                'ref_aplikasi a'=>'',
                "($sql_nav) n"=>array('n.id_aplikasi=a.id_aplikasi','left'),
                ),
            'can' => array(),//bisa hapus dan edit
            'select'=>'a.*,n.jm,if(a.aktif<>1,1,0)nona',
            'kolom' => array('ID','Kode','Nama','Fold','Aktif','jNav','Des'),//header tabel
            'kolom_tampil'  => array('id_aplikasi','kode_aplikasi','nama_aplikasi','folder','aktif','jm','deskripsi'),//isi tabel
            'kolom_data'    => array('id_eselon','tunjangan'),//jangan di kasih nama tabel di depan nya
            'id' => 'id_aplikasi',
            'order'=>'urut',
            'tombol' => array(),
            'form' => array(
            ),
            'pageper'=>50,
            'aksidikiri'=>1,
            'tombol_tambah'=>div(
                tag('a','<i class="fa fa-plus"></i>','class="btn btn-info btn-edit" title="Cek App" id="cekapp" act="'.site_url(uri2('1,2,4')).'"')
            ,'class="box-tools pull-right"'),
            'tombol_aksi'=>array(
                'Db'=>array(3,
                    'folder',
                    anchor('#'
                        ,'<i class="fa fa-table"></i>'
                        ,'class="btn btn-xs btn-warning btn-edit" act="'
                            .site_url('dm/Ref/db/&&folder&/'.in_de(array('ret'=>uri_string())).'/').'"'
                        ),
                    '',
                    
                ),
                'aktipin'=>array(3,
                    'nona',
                    anchor('dm/Ref/app_set/&&id_aplikasi&/'.in_de(array('aktif'=>1,'ret'=>uri_string()))
                        ,'<i class="fa fa-check"></i>'
                        ,'class="btn btn-xs btn-success" title="Aktifkan"'
                        ),
                    anchor('dm/Ref/app_set/&&id_aplikasi&/'.in_de(array('aktif'=>2,'ret'=>uri_string()))
                        ,'<i class="fa fa-ban"></i>'
                        ,'class="btn btn-xs btn-danger" title="Non Aktifkan"'
                        ),
                    
                ),
                
            ),
        );
        $param['kolom_cari']=$param['kolom_tampil'];
        return $param;
    }
    
    function parameter($o=''){
            $param = array(
                'title' => 'parameter',
                'tabel' => 'parameter',
                'id' => 'param',
                'select'=>'*,param idparam',
                'can' => array('edit','delete'),
                'kolom' => array('Param','Val'),
                'kolom_data' => array('param','val'),
                'kolom_tampil' => array('idparam','val'),
                'kolom_cari' => array('idparam','val'),
                'order' => 'param',
                'tombol' => array(array(1,'Tambah'),),
                'aksidikiri'=>1,
                'form'=>array(
                    array(1,-1,'Param','param','',''),
                    array(13,-1,'Val','val','class-textarea','pake-tinimce',''),
                ),
                'pageper'=>50,
                'pageplus'=>0,
                'debug_list'=>1,
            );
            return $param;
    }
    
    function unker(){
        $sql_bid="select id_unit,count(id_bidang)jm from ref_bidang group by id_unit";
            $param = array(
                'title' => 'unker',
                /*'first_title' => '',
                'last_title'=>' ',*/
                'tabel' => array(
                    'ref_unit ru'=>'',
                    "($sql_bid)rb"=>array('rb.id_unit=ru.id_unit','left'),
                ),
                #'tabel_save' => '',
                'where'=>array('ru.aktif'=>'1'),
                #'tambah_hidden'=>array(''=>''),
                'id' => 'id_unit',
                
                #'eid' => '',
                #'where_id' => '',
                
                #'can' => array('edit','delete'),
                'kolom' => array('Unit','Jm BIdang'),
                'select' => "ru.*,rb.jm,if(rb.jm>0,1,0)ad_bid",
                'kolom_data' => array(''),
                'kolom_tampil' => array('unit','jm'),
                'kolom_cari' => array('unit'),
                'order' => 'unit',
                'tombol_aksi'=>array(
                    'detail'=>array(3,
                        'ad_bid',
                        anchor('dm/ref/redir/'.in_de(array(
                            'url'=>uri2('1,2,3,').'unker_bidang/',
                            'key'=>array('id_unit'),
                            'add'=>array(
                                'ret'=>uri_string(),
                            )
                        )).'/&&id_unit&',
                        '<i class="fa fa-check"></i>',
                        'class="btn btn-warning btn-xs" title="cek bidang"'
                        ),
                        '',
                        ),
                    'lihat'=>array(3,
                        'ad_bid'
                        ,anchor('#'
                            ,'<i class="fa fa-up"></i>'
                            ,'class="btn btn-warning btn-delete btn-xs" act="'
                                .site_url("dm/def/delete/&&id_unit&/".in_de(array(
                                    'tabel'=>'ref_unit',
                                    'redirect'=>uri_string(),))
                                    )
                                .'" msg="Apakah Anda ingin menghapus data ini?"
                                title="Hapus ini"
                                ')
                        ,''
                    ),
                ),
                /*'detail_row'=>array(
                    'jmx'=>array(1,'dm','param_dm','isi_wenang',array('row'=>'id_pegawai')),
                ),
                'tombol' => array(
                    array(1,'Tambah'),
                    ),
                'tombol_tambah'=>'',
                'extra_tombol'=>'',
                'no_no'=>1,#tanpa nomor*/
                'no_search'=>1,
                #'no_page'=>1,#no paging
                'aksidikiri'=>1,
                /*'ret_save'=>uri_string(),
                'save_to'=>'present/presensi/simpan_group',
                'ret'=>array(
                    'fold'=>'present',
                    'def'=>'ref',
                    'func'=>'save_libur_next',
                ),
                'form'=>array(
                    array(-1,0,'Label'),
                    array(0,0,'','Hidden','[default]'),
                    
                    
                    
                ),
                #script at listview
                'list_script'=>"
                   \$('#reloadmsn').click(function(){
                        var htm=\$(this).html();
                        \$(this).html('<i class=\"fa fa-refresh fa-spin\"></i>');
                        \$.ajax({
                            url: $(this).attr('act'),
                            async: false,
                            type: 'POST',
                            dataType: 'json',
                            success: function(data) {
                                \$('#reloadmsn').html(htm+' reload '+data.cnt+' user');
                                console.log('updated '+data.cnt+' data');
                                window.location.reload();
                            }
                        })
                        return false;
                    });
                ",
                'no_aksi'=>1,*/
                #no plugins
                'heads'=>-1,
                /*'baris_custom'=>array(
                        'aktif'=>array('aktif','=','0','style="background-color: crimson;color: white;"'),
                ),
                'pageuri'=>6,
                'pageper'=>10,
                'pageplus'=>0,
                'debug_list'=>1,
                'tabs'=>array(
                    array('url'=>site_url(uri2('1,2,3,').'pegawai'),'text'=>'Aktif'),
                    array('on'=>1,'text'=>'Non Aktif'),
                ),
                'bc'=>uri2('1,2,3,').'pegawai',*/
            );
            
            return $param;
        }
        
    function unker_bidang($o=""){
            $param = array(
                'title' => 'Detail bidang',
                'tabel' => 'ref_bidang',
                'where'=>array('id_unit'=>$o['id_unit']),
                'id' => 'id_bidang',
                #'can' => array('edit','delete'),
                'kolom' => array('Nama'),
                'select' => "*",
                'kolom_data' => array(''),
                'kolom_tampil' => array('nama_bidang'),
                'kolom_cari' => array('nama_bidang'),
                'order' => 'urut',
                /*'tombol' => array(
                    array(1,'Tambah'),
                    ),
                'tombol_tambah'=>'',
                'extra_tombol'=>'',
                'no_no'=>1,#tanpa nomor
                'no_search'=>1,*/
                'no_page'=>1,#no paging
                'aksidikiri'=>1,
                /*'ret_save'=>uri_string(),
                'save_to'=>'present/presensi/simpan_group',
                'ret'=>array(
                    'fold'=>'present',
                    'def'=>'ref',
                    'func'=>'save_libur_next',
                ),*/
                'form'=>array(
                ),
                'heads'=>-1,
                'debug_list'=>1,
            );
            if($o['ret'])$param['first_title']=anchor($o['ret'],'<<','class="btn btn-default"');
            #cek($o);
            return $param;
        }
 }
  
?>