<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  /**
  * v:1.1
  */
  class Sampleparam_kinerja{
    var $folder='kinerja';
    var $tipe='';
    #can use with def version
    var $min_def_ver='1.0184';
    
    function form($i) {
        return array(
                #def
                array(-4,'id_apa'),
                array(-3,'HTML'),
                #load cnt
                array(-2,array(array(4,'id_hari'),array(5,'id_ref')), 'preset', 'ref', 'open'),
                array(-1,0,'Label'),
                array(0,0,'-','formENC',in_de(array(
                    'tahunan'=>$tahun,
                    'id_cuti_ref'=>$id_cuti_ref,
                    ))
                    ),#tanpa ambil dari db khusus
                array(0,0,'-','Hidden','[default]'),#tanpa ambil dari db
                array(0,0,'','Hidden','[default]','id_opo_gantifield_hidden'),
                
                #     0   1     2             3          4                      5              (6) extra_form
                array(1,false,'Tanggal',    'tanggal',  '',             'airdatepicker'),
                array(1,false,'Mulai',      'mulai',    'form-control', 'datepicker'),
                array(1,false,'Angka',      'angka',    'form-control', 'formatnumber',    'onkeyup="formatNumber(this)"'),
                array(1,false,'Tahun',      'tahun',    'form-control', 'yearpicker'),
                array(1,false,'Jam',        'jam',      'form-control', 'timepicker'),
                array(1,false,'Tanggal',    'tgl',      'form-control', 'airdatepicker'),
                array(1,false,'Tanggaljam', 'datetime', 'form-control', 'airdatetimepicker'),
                
                array(2,0,'DropLabel','dropname',array('1','2'),'form-control','present/presensi_yes/tz','dropname'),
                #text area
                array(3,false,'Kop','kop','class-textarea','pake-tinimce','isi_style tambahan jika ada'),
                
                //checkbox                   jika tidak
                array(4,0,'check Box','cb',1,[2]),
                
                #radio
                array(5,true,'Radio','radioname[]',array(1=>'satu',2=>'dua'),1),
                array(5,true,'Keputusan','statusx',array(2=>'Diterima',3=>'Ditolak',4=>'Ditangguhkan'),'2','pilcek','onclick="klik(this)"'),
                
                //INPUT HIDDEN default if isi ->text
                array(6,false,'label','name','form-control','style=""','',3),
                
                #upload                         #folder allowed_type
                array(7,false,'Logo','logo','','logo','jpg|jpeg|png|gif|pdf|word|doc|docx|xls|xlsx'),
                
                #8 type ahead
                
                array(9,false,'Grup','id_group',uri(1).'/autocomplete/grup','','nama_group'),
                array(9,true,'Pegawai','id_pegawai',
                    #4 url autocomplete
                    uri(1).'/autocomplete/pegawai_group',
                    #5 class
                    '',
                    #6 fieldname on select_table -> harus di isi
                    'nama',
                    #7 function onchange
                    'function on cnCange',
                    #8 name of fields
                    array('id'=>'id','data'=>'data'),
                    #9 other variable
                    array(
                        'param_data'=>",grup:\$('#id_group').val()",
                        'param_data'=>',u:$(\'#form_data input[name="id_unit"]\').val()',
                        'onplus'=>'.on(\'select2:select\', function (e) { #if selected
                            var xdata = e.params.data.ipeg;
                            console.log(xdata);
                        })',
                        'return_plus'=>',ipeg: item.ipeg',#plus item
                        'param_process'=>'?',
                        ),
                    #10
                    'title="Hey"'
                    ),
                array(9,false,'Pegawai','id_pegawai',
                    'pesta/autocomplete/pegawai_pin', //4
                    '',                               //5
                    'nama_pegawai'                     //6
                    ,"hey(e.currentTarget.value)"       //7
                    ,''                                 //8
                    ,array(
                        'param_process'=>            //9
                            "var x= item.data.split('|');
                            console.log(x[0]);
                            //if(x[2]!=null)
                            //bandol[item.id]=[{data:x[2],item:x[1]}];
                            
                            item.data=x[0];",//"\$('#id_unit').val()"
                        #--------    
                        'onplus'=>".on('select2:select', function (e) { 
                                var xdata = e.params.data;
                                var sli=$('select[name=id_unit]');
                                if($('input[name=id_ppegawai]').val()=='')
                                    $('select[name=id_unit]').select2({
                                        val:xdata.iunit,
                                        data:[{
                                                id:xdata.iunit,
                                                text:xdata.unit
                                        }]
                                            
                                    })
                                    .val(xdata.iunit)
                                    .trigger('change');
                                console.log(sli.val());
                        })",
                        'return_plus'=>',iunit: item.iunit,unit: item.unit',#plus item
                        'faram_set'=>array(
                            'tags'=>'true',
                        ),  
                    ),
                         
                #combobox  0 ke 6 = default
                array(10,true,'Posisi Kas','tipe',array('1'=>'Penerimaan','Pengeluaran',0),'combo-box',''),
                
                array( 11 , 
                    list($fr,$data)=$this->inp11_select2($fo,@$def,$data);
                    ),
           
                array(12 ,
                    $fr = '<p>'
                    .form_label($label2)
                    .form_dropdown($nama3,$this->CI->general_model->combo_box($apa4),(@$def?$def->$nama3:(@$fo[6]?$fo[6]:0)),'class="'.$apa5.' form-control " id="'.$nama3.'" style="width: 100%" '.($requ1  ? 'required' : ''))
                    .'</p>';
                ),
           
                #text area simpan tanpa entities
                array(13,false,'Kop','kop','class-textarea','pake-tinimce','isi_style tambahan jika ada'),
                
                #---select2_with if par1 then par2 else select2_par3
                array( 49 
                    if($fo[1]){
                        $fr=$fo[2];
                    }else{
                        array_shift($fo);
                        array_shift($fo);
                        list($fr,$data)=$this->inp9_select2($fo,@$def,$data);
                    }    
                )
                
                #---- if par1 then conv:par1::def else_if par2 then par2 else_if par3 and def then par3 else_if par4 then if new par4[0] else par4[1]
                array(50,
                    if($requ1){
                        $fr=$this->changeto( $requ1,@$def);
                    }elseif($label2){
                        $fr=$label2;
                    }elseif($nama3&&@$def){
                        $fr=$this->changeto( $nama3,@$def);
                    }elseif($apa4){
                        $fr=$apa4[(!$id?0:1)];
                    }
               ),
           
               #----- universal command
               array( 51 ,
                    #jika posisi tambah
                    if(is_array($requ1))
                    if(!$id){
                        list($fr,$data)=$this->inp3_textarea($fo,@$def,$data);
                    }else{
                        array_shift($fo);
                        list($fr,$data)=$this->inp3_textarea($fo,@$def,$data);
                    }
               ),
               
               #---- text area if tambah then par1 else inp3_textarea
               array( 53 : 
                #jika posisi tambah
                if(!$id){
                    $fr=$requ1;
                }else{
                    array_shift($fo);
                    list($fr,$data)=$this->inp3_textarea($fo,@$def,$data);
                }
               ),
           
            );
    }
  
    function sample_param(){
        $param = array(
            'inti' => $this->tipe,
            'title' => 'Grup Pegawai',
            'title_ft' => 'Nambah',//change title form tambah
            'title_fu' => 'Gubah',//change title form ubah
            'first_title' => 'tambah awal',
            'last_title'=>' '.div(date("d-m-Y").' '.date("H:i:s").' '.anchor(''
                ,'<i class="fa fa-refresh"></i>','class="badge" 
                act="'.site_url('present/sinkron/sinkmsn/').'"
                id="reloadmsn" title="sinkron aktif mesin"
                '),'class="badge"'),
            'tabel' => array(
                'present_presensi s'=>'',
                'present_group g'=>array('g.id_group=s.id_group','left'),
                'peg_pegawai p'=>array('p.id_pegawai=s.id_pegawai','left'),
                'present_pegawai pp'=>array('pp.id_pegawai=p.id_pegawai','left'),
                'ref_unit u'=>array('u.id_unit=pp.id_unit','left'),
            ),
            'tabel' => 'present_presensi',            
            'tabel_save' => 'present_presensi',
            'where'=>array('b.kabupaten'=>'Kutai Kartanegara'),
            'tambah_hidden'=>array('barang'=>2),#neww 180804 defaault khusus form tambah edit nda ada
            'id' => 'id_pre',
            #form id  auto
            'eid' => 's.id_pre',
            #or manual
            'where_id' => '4',
            
            'can' => array('edit','delete'),//bisa hapus dan edit
            'kolom' => array('Nip','Nama','Group','OPD'),//header tabel
            'select' => "
                id_pegawai
                , nip
                ,CONCAT(IF(IFNULL(gelar_depan , '')='','',CONCAT(gelar_depan,' ')), nama, IF(IFNULL(gelar_belakang,'')='','', CONCAT(' ',gelar_belakang))) AS nama_pegawai
                , IF(rj.nama_jabatan='KEPALA',CONCAT('KEPALA ',rb.`nama_bidang`),rj.nama_jabatan) nama_jabatans
                , DATE_FORMAT(d.mulai,'%d/%m/%Y') tgl_mulai
                ",//fild tabel yang dipilih
            'kolom_data' => array('id_pegawai','id_group'),//jangan di kasih nama tabel di depan nya
            'kolom_tampil' => array('nip','nama','nama_group','unit'),//jangan di kasih nama tabel di depan nya
            #'kolom_cari' => array('nip','nama','nama_group','unit'),//harus di kasih nama tabel di depan nya jika ada join
            'order' => 'id_kelurahan',
            'in' => array('id_kelurahan',array(1,2)),
            'not_in' => array('id_keluar',array(2,5)),
            'order' => 'id_kelurahan',
            'tombol' => array(
                array(0,'Tambah','btn-default'), # 0=> no btn-sucess , change with 
                array(1,'Tambah'),
                array(2,''),#anchor($t[1],$t[2],$t[3])),
                array(3,'referensi/kewenangan/add_kewenangan','<i class="fa fa-plus-square"></i> &nbsp; Tambah','title="Tambah Kewenangan"'/*[,'class="btn btn-info btn-edit"']*/),#btn-edit
                ),
            'tombol_tambah'=>div(
                    span("OPD : ")
                    .form_dropdown('x',$drop_down_unit,(!empty($o['id_unit'])?$o['id_unit']:""),'class="col-lg-10" id="pilih_unker"')
                ,'class="box-tools pull-right col-lg-10"'),
            'extra_tombol'=>'??',
            #tanpa pencarian
            'no_search'=>1,
            'aksidikiri'=>1,
            'ret_save'=>uri_string(),
            'tombol_aksi'=>array(
                #0::
                #0 -> param aksi((tipe 0.. 2,dt),data,title) ::
                #       0:ret=dt
                #       1:toggle aktif pd baperjakat
                #       2:dt=array::
                #            0=>1:if(1..2..op3; ret anchor(4==#?#:site_url4.data->5;6;4==#?act:site_url7.data->5.tile_7))      else 9
                #1 with act anchore(#,2,3, act=1)
                #2 ancor(1/+id+4,2,3)
                #3 -> exchange if find id, then, else
                #4 -> if ada_1:: then edit
                #5 -> del_if ada_1
                'hapus'=>array(3,
                    $id
                    ,anchor('#'
                        ,'<i class="fa fa-trash"></i>'
                        ,'class="btn btn-danger btn-delete btn-xs" act="'
                            /*.site_url("dm/def/del/&&$id&/".in_de(array(
                                'id_tabel' =>$id,
                                'tabel'=>$ntable,
                                'next_del'=>array(
                                    'fold'=>'present',
                                    'def'=>'ref',
                                    'func'=>'delete_libur_next',
                                ),
                                'ret'=>uri_string())))*/
                            .site_url("present/ref/delete_libur_pref/&&$id&/".in_de(array(
                                'ret'=>uri_string())))
                            .'" msg="Apakah Anda ingin menghapus data ini?"
                            title="Edit Last_Id_log"
                            t_text="Simpan"
                            t_btn="Simpan"
                            is_pos=1
                            ')
                    ,''
                ),
            ),
            #custom save
            'save_to'=>'present/presensi/simpan_group',
            #next process after save 
            'ret'=>array(
                'fold'=>'present',
                'def'=>'ref',
                'func'=>'save_libur_next',
            ),
            /*
            * 0:hidden
            * 1:text
            * 2:ajax
            * 3:tinymce
            * 4:check box
            * 6:hidden
            * 7:upload
            * 8:typeahead
            * 10:dropdown
            */
            #default form data on fields::
            'def'=>array(
                'tanggal'=>'2/2/209',
            ),
            'form'=>$this->form(1),
            #script at Formview
            'form_script'=>"var bandol=new Array();
                function hey(x){
                    //a1=bandol[x];
                    //console.log(a1);
                    //\$('#id_unit').select2({val:x,data:[{id:a1[0].item,text:a1[0].data}]});
                };",
            'out_script'=>'function tezz() {
                               return;
                           }'
            #script at listview
            'list_script'=>"
               \$('#reloadmsn').click(function(){
                    var htm=\$(this).html();
                    \$(this).html('<i class=\"fa fa-refresh fa-spin\"></i>');
                    \$.ajax({
                        url: $(this).attr('act'),
                        async: false,
                        type: 'POST',
                        dataType: 'json',
                        success: function(data) {
                            \$('#reloadmsn').html(htm+' reload '+data.cnt+' user');
                            console.log('updated '+data.cnt+' data');
                            window.location.reload();
                        }
                    })
                    return false;
                });
            ",
            #form-save new
            'script_submit'=>"
                e.preventDefault();
                \$.ajax({
                     url: \$(this).attr('action'),
                     type: 'POST',                  
                     dataType: 'json',
                     data:\$(this).serialize()+'&g=x',
                     success: function(msg) {
                        \$('#form-content').html(msg.form);
                     },
                     error:function(error){
                     
                     }
                });
                return false;
                ",
            'no_aksi'=>1,
            #no plugins
            'heads'=>-1,
            'heads'=>array(
                'datepicker',
                'wysihtml5',
                'select2',
                'inputmask',
                'fancybox',
            ),
           'detail_row'=>array(
           #                 1  load_cnt(1,2,3,4->is)
                'jmx'=>array(1,'dm','param_dm','isi_wenang',array('row'=>'id_pegawai')),
           #                 0   1    2          3            4
            ),
             
            'baris_custom'=>array(
                    'aktif'=>array('aktif','=','0','style="background-color: crimson;color: white;"'),
            ),
            'no_page'=>0,#tanpa paging
            'pageuri'=>6,
            'pageper'=>10,
            'pageplus'=>0,
            'debug_list'=>1,
            'debug_form'=>1,
            'tabs'=>array(
                array('url'=>site_url(uri2('1,2,3,').'pegawai'),'text'=>'Aktif'),
                array('on'=>1,'text'=>'Non Aktif'),
            ),
            'bc'=>uri2('1,2,3,').'pegawai',
            'list_head'=>table(tr(td('a'))),#TOP list tabel
            'list_foot'=>array(5,2,1,3,5),#bottom list tabel
            'col_tombol'=>array('col-md-6','col-md-6'),
            'css_form'=>'.apa{}',
            'css_list'=>'.apa{}',
            'form_button'=>'<br><a class="btn btn-danger btn-md btn-form-cancel" type="button">Batal</a>
                            <button href="#" class="btn btn-success btn-md btn-save-act pull-right">Impor</button>',#tombol_form
            'tombol_aksi'=>array(
                'Edit'=>array(4,'editz'),
                'Add' =>array(3,
                        'nambah',
                        anchor('dm/ref/redir/'.in_de(array(
                            'url'=>uri_string(),
                            'key'=>array('id_unit'),
                            'add'=>array(
                                'modex'=>'form_mode',
                                'new'=>1,
                                'ret'=>uri_string(),
                            )
                        )).'/&&iunit&'
                            ,'<i class="fa fa-plus"></i>'
                            ,'class="btn btn-primary btn-form btn-xs" '
                            )
                        ,''
                ),
                'hapus']=array(3,
                    $param['id']
                    ,anchor('#'
                        ,'<i class="fa fa-trash"></i>'
                        ,'class="btn btn-danger btn-delete btn-xs" act="'
                            .site_url("dm/def/delete/&&".$param['id']."&/".in_de(array(
                                'tabel'=>$param['tabel'],
                                'redirect'=>uri_string(),))
                                )
                            .'" msg="Apakah Anda ingin menghapus data ini?"
                            title="Hapus ini"
                            t_btn="Ya"
                            ')
                    ,''
                )
            ),
        );
        $param['kolom_cari']=$param['kolom_tampil'];
        return $param;
    }
    
    function show() {
        
        $data['b4_title']='<< back';
        $data['after_title']='amd';
        $data['no_user_info']=1;
        $data['menu_nav']=li(anchor('url','tex'));
        $data['no_home']=1;
        $data['title']="Presensi Hari Ini";
        $data['script']=$skrip;
        $data['script_cookies']=1;#uses cookies manager
        $data['tabel']=$tabel;
        $data['content']="dm/default";
        $this->load->view('dm/home',$data);        
    }
   
    function catetan_aja() {
        $this->load->config('present');
        $set_opd=0;
        if(null !=($this->config->item('present_opd'))) $set_opd= $this->config->item('present_opd');
        if(is_role(uri(1),'RESVR'))
            if( is_tabel_exist('peg_jabatan'))
                if($this->config->item('def_server')==2)
                    if(is_field_exist('id_bidang','present_pegawai'))
        if(!is_role(uri(1),'SEUNI')){
                if($roleunit=roleunit(0,uri(1))){
                    $where.=' and (rb.id_unit in('.implode(',',$roleunit).'))';
                }else{
                    $where.=' and (rb.id_unit='.sesi('id_unit').')';
                }
            }
        if(is_field_exist('shift','present_group')){
            
        }
        if(is_tabel_exist('present_shift')){
            
        }
        
        return;
    }
    
    
    function def_view() {
        $tes=load_cnt('pesta','notif','get_def_view');
        $data['form_link']='pesta/notif/save_def_view';
        $data['title']="Tampilan Standar beranda";
        $data['form_data']=
                form_label(form_radio('pilih',2,$tes==2).' Semua ').'<br>'
                .form_label(form_radio('pilih',1,$tes==1).' Struktural').'<br>'
                .form_hidden('ret',uri_string());
        $data['tombol_form']='
                <br><a class="btn btn-danger btn-md btn-form-cancel" type="button" <?php echo @$ret?'href="'.$ret.'"':''; ?>>Batal</a>
                <button href="#" class="btn btn-success btn-md btn-save-act pull-right">Simpan</button>';
        $data['content']='dm/form_view';
        $this->load->view('dm/home',$data);
    }
    
    function colorpicker_sample() {
        $s='<div class="input-group my-colorpicker2 colorpicker-element">
                      <input class="form-control" type="text">
                      <div class="input-group-addon">
                        <i style="background-color: rgb(105, 71, 71);"></i>
                      </div>
                    </div>';
            
        $data['include']=
            reqcss("assets/plugins/colorpicker/css/bootstrap-colorpicker.min.css")
            .reqjs('assets/plugins/colorpicker/js/bootstrap-colorpicker.min.js');
        $data['script']='//Colorpicker
                $(".my-colorpicker2").colorpicker();
        ';
        return $s;
    }
    
    #v1,05
    function datagrabe() {
        $x=$this->general_model->datagrabe(array(
            'tabel'=>,
            'where'=>array(=>),
            'wh'=>db->where('wh'),
            'order'=>='order_by',
            'select'=>,
            'group_by'=>='group',
            'not_in'=>array(f=>array()),
            'not_in_isi'=>,
            'in'=>array(f=>array()),
            'in_isi'=>,
            'search'=>='cari',
            'notin'=>db->where_not_in('notin'),
            'limit'=>,
            'offset'=>,
           ))
        return;
    }
  }
  
  function combo_box() {
      $x=$this->general_model->combo_box(array(
            'tabel'=>,
            'where'=>array(=>),
            'wh'=>db->where('wh'),
            'order'=>='order_by',
            'select'=>,
            'group_by'=>='group',
            'not_in'=>array(f,array()),
            'not_in_isi'=>,
            'in'=>array(f,array()),
            'in_isi'=>,
            'search'=>='cari',
            'notin'=>db->where_not_in('notin'),
            'limit'=>,
            'offset'=>,
            'key'=>'tahun',
            'val'=>array('tahun',),
            'pilih'=>'-',
           ))
        return;
  }
?>
<!-- div class="pull-right col-lg-11">
    <div class="col-lg-4">
        <div class="input-group date">
            <div class="input-group-addon">Bulan : </div>
            <input class="form-control" id="airtgl" data-language="id" data-min-view="months" data-view="months" data-date-format="MM yyyy" value="Juli 2019" style="width: 120px;" type="text">
            <input id="tgl" value="2019-7-01" type="hidden">
        </div>
    </div>
    <div class="col-lg-8">
        <div class="input-group">
            <div class="input-group-addon">OPD : </div>
            <select name="x" class="form-control select2-hidden-accessible" id="pilih_unker" tabindex="-1" aria-hidden="true">
                <option value="0"></option>
            </select>
            <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 303.767px;">
            <span class="selection">
            <span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-pilih_unker-container">
            <span class="select2-selection__rendered" id="select2-pilih_unker-container" title="">
            </span>
            <span class="select2-selection__arrow" role="presentation">
            <b role="presentation"></b>
            </span>
            </span>
            </span>
            <span class="dropdown-wrapper" aria-hidden="true">
            </span>
            </span>
        </div>
    </div>
</div -->