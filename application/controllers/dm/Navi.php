<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  /**
  *  Edito nav v:004
  */
  class Navi extends CI_Controller {
    var $debug=0;
    var $unit=array();
    var $bidang=array();
    var $dde=array();
    var $dage=array();
    var $pohong;
    var $partabel;
    var $def=array(
        'id'=>'id_nav',
        'tabel'=>'nav',
    );
    #var $jml=0;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cmd');
        if(!sesi('id_pegawai'))show_404();
        #if(sesi('id_pegawai')!=303)show_404();
    }
    function index(){
        redirect(uri2('1,2,').'nav');
    }
    
    function ol_li($data) {
        $li="";
        foreach ($data as $d) 
            $li.=li(
                    div(''
                    ,'class="dd-handle dd3-handle"')
                    .div(
                        span(' '
                            .form_checkbox('','c'.$d['id'],
                                $this->cek_is_bidang($d['text'])
                            )
                            ,'class="dd3-kanan"').
                        ' '.$d['text'].' '.$d['id'].' '
                    ,'class="dd3-content"')
                    
                    .(@$d['anak']?$this->ol_li($d['anak']):'')
                    ,'class="dd-item dd3-item" data-id="'.$d['id'].'"'
                    );
        return tag('ol',$li,'class="dd-list"');
    }
    
    function mli($dat,$ke) {
        $emli=array(
            'text'=>$dat[$ke]['text'],
            'id'=>$ke,
        );
        if(@$dat[$ke]['anak']){
            foreach ($dat[$ke]['anak'] as $anak) 
                $ret[]=$this->mli($dat,$anak);
            $emli['anak']=$ret;
        }
        #$this->jml++;
        return $emli;
    }
    
    function cek_is_bidang($tesdata) {
        $cek=array('INSPEKTORAT','PEMERINTAH','KELURAHAN','KABUPATEN',
            'TAMAN','SMA','SMP','SD','TK','SMK','SMU',
            'SEKOLAH','SLTA','SLTP','SMU/SLTA','SMP/SLTP','SMK/SLTA',
            'PUSKESMAS','KANTOR','UPTD','PUSTU','DESA',
            'DISTRIK','RUMAH','RSUD','SDN',
            'KOMISI','MADRASAH','BALAI',
            'BADAN','DINAS','INSTITUT','BAPPEDA',
        );
        
        $cek_khusus=array(
            'SEKRETARIAT DAERAH','SEKRETARIAT DPRD','RUMAH SAKIT',
            'SEKRETARIAT DEWAN',
        );
        /*$cek=array('SUB','SEKSI','BIDANG','SEKRETARIAT','BAGIAN'
            ,'ASISTEN','STAF','KELOMPOK','JABATAN','SUBAG','KEPALA'
            ,'SEKRETARIS','BIDAN','GURU','DOKTER','PERAWAT','APOTEKER','PRANATA','SANITARIAN'
            ,'SUBBAG','SUBBIDANG','SUBBAGIAN','SUBBID');*/
        $khusus=in_array($tesdata,$cek_khusus);
        if($spasi=strpos($tesdata,' '))$tesdata=substr($tesdata,0,$spasi);
        return  ! ((in_array($tesdata,$cek))||$khusus);        
    }

    function proses_unit($ntabel,$param) {
        extract(un_de($param));
        //cek($this->cek_is_bidang('BIDANG DENGKUL'));
        $sql_uptrim="UPDATE $ntabel SET f22=TRIM(UPPER(f22)),f23=TRIM(UPPER(f23))";
        $this->db->query($sql_uptrim);
        
        $requ=  reqjs('assets/plugins/nestable/jquery.nestable.js');
        /*$sql_joins="SELECT f22,f23 FROM $ntabel WHERE f23<>'' GROUP BY f22,f23";
        $sql_get="SELECT
r.f22 rf1,r2.f22 rf2,r3.f22 rf3,r4.f22 rf4,r5.f22 rf5
FROM (
    SELECT f22 FROM $ntabel GROUP BY f22,f23
    order by id
) r

LEFT JOIN ($sql_joins )r2 ON r2.f23=r.f22

LEFT JOIN ($sql_joins )r3 ON r3.f23=r2.f22

LEFT JOIN ($sql_joins )r4 ON r4.f23=r3.f22

LEFT JOIN ($sql_joins )r5 ON r5.f23=r4.f22

";#ORDER BY r2.f22,r3.f22,r4.f22,r5.f22
        */
        
        $sql_get="SELECT f22,f23 FROM $ntabel GROUP BY f22,f23 ORDER BY id";
        $pemerintah=0;
        $nog=1;
        $dage=$dde=$dnav=array();
        if($get_db=$this->db->query($sql_get))
            foreach ($get_db->result() as $g) {
                if(substr($g->f22,0,10)=='PEMERINTAH')$pemerintah=$nog;
                $dage[$nog]=array(
                    'text'=>$g->f22,
                    'up'=>$g->f23,
                );
                $dde[$nog++]=$g->f22;
                /*if($g->rf5){
                    $d_ag[$g->rf5]=array('no'=>$nog,'up'=>$g->rf4);
                    if(!@$d_ag[$g->rf4]){
                        $d_ag[$g->rf4]=array();
                    }else{
                        $d_ag[$g->rf5]['next']=@$d_ag[$g->rf4]['no'];
                    }
                    $d_d[$nog++]=$g->rf5;
                }elseif($g->rf4){
                    $d_ag[$g->rf4]=array('no'=>$nog,'up'=>$g->rf3);
                    if(!@$d_ag[$g->rf3]){
                        $d_ag[$g->rf3]=array();
                    }else{
                        $d_ag[$g->rf4]['next']=@$d_ag[$g->rf3]['no'];
                    }
                    $d_d[$nog++]=$g->rf4;
                }elseif($g->rf3){
                    $d_ag[$g->rf3]=array('no'=>$nog,'up'=>$g->rf2);
                    if(!@$d_ag[$g->rf2]){
                        $d_ag[$g->rf2]=array();
                    }else{
                        $d_ag[$g->rf3]['next']=@$d_ag[$g->rf2]['no'];
                    }
                    $d_d[$nog++]=$g->rf3;
                }elseif($g->rf2){
                    $d_ag[$g->rf2]=array(
                        'no'=>$nog,
                        'up'=>$g->rf1);
                    if(!@$d_ag[$g->rf1]){
                        $d_ag[$g->rf1]=array();
                    }else{
                        $d_ag[$g->rf2]['next']=@$d_ag[$g->rf1]['no'];
                    }
                    $d_d[$nog++]=$g->rf2;
                }elseif($g->rf1){
                    $d_ag[$g->rf1]=array('no'=>$nog,'up'=>'');
                    $d_d[$nog++]=$g->rf1;
                }*/
                
        }
        #cek($this->db->last_query());
        $sdde=array_flip($dde);
        foreach ($dage as $d_ => $dag) 
            if(($dag['up'])&&($up=@$sdde[ $dag['up']])){
                $dage[$d_]['next']=$up;
                $dage[$up]['anak'][]=$d_;                                
        }
        
        $tes_dnav=0;
        foreach ($dage as $d_ => $dag) {
            if(!@$dag['next']){
                $dnav[]=$this->mli($dage,$d_);
                $tes_dnav++;
            }    
        }
        
        $div=
         div('Sufix unit :'
            .form_input('sufix',$ntabel,'id="sufix_unit" class=""')
            ." Found at $pemerintah "
            .form_input('pmrt',$pemerintah,'id="pmrt" class=""')
            .'<br>'
            .'Start from :'
            .form_input('stf',10000,'id="stf" class=""').'<br>')
         .div('','id="comen"')
         .div(
            '<input type="hidden" name="dde" id="dde" value="'.in_de(json_encode($dde)).'"/>'
            .'<input type="hidden" name="dage" id="dage" value="'.enkrip($dage).'"/>'
         ,'style="display:none;"')
         .tag('a','<i class="fa fa-save "></i>','href="#" class="btn btn-success pull-right" id="simpan"')
         .div(
            div(
                $this->ol_li($dnav)
            ,'class="dd" id="nestable"')
                
            .div(''
            ,'class="dd" id="nestable"')
         ,'class="cf nestable-lists"')
         
         ;
         
       $tampilan= $div;
        
        $data['include_script']=$requ;
        
        $jq=
        #tag_jqready
        ("
        function cekdb(data) {
            $('#comen').html('Process');
            var ctes=new Array;
            $('input:checkbox:checked' ).each(function(){ctes.push($(this).val())});
            tes=window.JSON.stringify(ctes);
            dde=window.JSON.stringify($('#dde').val());
            sufix=$('#sufix_unit').val();
            stf=$('#stf').val();
            dage=$('#dage').val();
            pmrt=$('#pmrt').val();
            $.ajax({
              url: '".site_url(uri(1).'/'.uri(2).'/gettes')."',
              type: 'POST',
              data:'pmrt='+pmrt+'&dage='+dage+'&stf='+stf+'&nt=".$ntabel."&g='+data+'&c='+tes+'&dde='+dde+'&s='+sufix,
              cache: false,
              dataType: 'json',
              success: function(msg) {
                  console.log(msg);
                  $('#comen').html('done');
                  $('#hasil').html(msg.tbl);
              },
              error:function(error){
                $('#comen').html('..');
              }
          });
        }
        
        $('#simpan').click(function () {
            //alert('apa');
             if (window.JSON) {
                var outp=window.JSON.stringify($('#nestable').nestable('serialize'));
                cekdb(outp);
                //var ctes=new Array;
                //$('input:checkbox:checked' ).each(function(){ctes.push($(this).val())});
                //tes=window.JSON.stringify(ctes);
                //console.log(tes)
                
            } else {
                $('#comen').html('JSON browser support required.');
            }
        });
        
        var updateOutput = function(e){
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                var outp=window.JSON.stringify(list.nestable('serialize'));
                //output.val(outp);//, null, 2));
                //cekdb(outp);
            } else {
                //output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        })
        .on('change', updateOutput);

        // activate Nestable for list 2
        $('#nestable2').nestable({
            group: 1
        })
        .on('change', updateOutput);

        $('#nestable3').nestable().on('change', updateOutput);
        
        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        //updateOutput($('#nestable2').data('output', $('#nestable2-output')));
        //updateOutput($('#nestable3').data('output', $('#nestable3-output')));
        
        ");
        $data['script']=$jq;
        
        $csse = $this->cssnya();
        $data['css']=$csse;
        
        $data['tabel']=box_adminlte2(array(
            'cl_main'=>'box-success',
            'h3_head'=>'Data',
            'after_h3_head'=>anchor($ret,'<< Kembali','class="btn btn-default pull-right"'),
            'main'=>$tampilan,
          ))
          .div('','class="col-md-12 table-responsive" id="hasil"');
        $data['content']='dm/default';
        $data['requ_script']=array();
        $this->load->view('dm/home',$data);
    }
    
    function cssnya() {
        return /*$csse = */#tag_css
        ("
            .cf:after { visibility: hidden; display: block; font-size: 0; content: \" \"; clear: both; height: 0; }
            * html .cf { zoom: 1; }
            *:first-child+html .cf { zoom: 1; }

            a { color: #2996cc; }
            a:hover { text-decoration: none; }

            .small { color: #666; font-size: 0.875em; }
            .large { font-size: 1.25em; }
                    .dd { position: relative; display: block; margin: 0; padding: 0; max-width: 400px; list-style: none; font-size: 13px; line-height: 20px; }

            .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
            .dd-list .dd-list { padding-left: 30px; }
            .dd-collapsed .dd-list { display: none; }

            .dd-item,
            .dd-empty,
            .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

            .dd-handle { 
                display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                background: #fafafa;
                background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                -webkit-border-radius: 3px;
                        border-radius: 3px;
                box-sizing: border-box; -moz-box-sizing: border-box;
            }
            .dd-handle:hover { color: #2ea8e5; background: #fff; }
            
            .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
            .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
            .dd-item > button[data-action=\"collapse\"]:before { content: \"-\"; }

            .dd-placeholder,
            .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
            .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
                background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                  -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                     -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                          linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-size: 60px 60px;
                background-position: 0 0, 30px 30px;
            }

            .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
            .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
            .dd-dragel .dd-handle {
                -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
                        box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            }

            /**
             * Nestable Extras
             */

            .nestable-lists { display: block; clear: both;  width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

            #nestable-menu { padding: 0; margin: 20px 0; }

            #nestable-output,
            #nestable2-output,
            #nestable3-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

            #nestable2 .dd-handle {
                color: #fff;
                border: 1px solid #999;
                background: #bbb;
                background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
                background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
                background:         linear-gradient(top, #bbb 0%, #999 100%);
            }
            #nestable2 .dd-handle:hover { background: #bbb; }
            #nestable2 .dd-item > button:before { color: #fff; }

            @media only screen and (min-width: 700px) {

                .dd { float: left; width: 98%; }
                .dd + .dd { margin-left: 2%; }

            }

            .dd-hover > .dd-handle { background: #2ea8e5 !important; }

            /**
             * Nestable Draggable Handles
             */

            .dd3-content { 
                display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                background: #fafafa;
                background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                -webkit-border-radius: 3px;
                        border-radius: 3px;
                box-sizing: border-box; -moz-box-sizing: border-box;
                width:500px;
            }
            .dd3-content:hover { color: #2ea8e5; background: #fff; }

            .dd-dragel > .dd3-item > .dd3-content { margin: 0; }

            .dd3-item > button { margin-left: 30px; }

            .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
                border: 1px solid #aaa;
                background: #ddd;
                background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
                background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
                background:         linear-gradient(top, #ddd 0%, #bbb 100%);
                border-top-right-radius: 0;
                border-bottom-right-radius: 0;
            }
            .dd3-handle:before { 
                content: \"=\"; display: block; position: absolute; left: 0; 
                top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
            .dd3-handle:hover { background: #ddd; }
            

            /**
             * Socialite
             */

            .socialite { display: block; float: left; height: 35px; }
            /**
            * tambahan 
            */
            .icoo a{
                margin: 4px;
            }
            .icoo .edit_menu{color:#0000FF}
            .icoo .hapus_menu{color:#FF0000}
            .icoo .tambah_menu{color:#000000}
            .icoo .infonav{color:#00FF00}
            .kuning{color:#f39c12}
            
            @media (min-width: 1024px) {
            #kedit{
                position: fixed;
                width: 35%;
                bottom: 4%;
            }
            }
        ");
        ;
    }
    
    function aktifkan($par,$id,$val=true) {
        $def=dekrip($par,1);
        if($def_database)$def_database.=".";
        $this->db->where($def['id'],$id)
            ->update(($def['def_database']?$def['def_database'].".":"").$def['tabel'],array('aktif'=>($val=="false"?2:1)));
        if($val=="false")
        $this->db->where($def['id'],$id)
            ->delete(($def['def_database']?$def['def_database'].".":"").'ref_role_nav');
        redirect($def['ret_list']);        
    }
    
    
    /**
    *  menu nav
    */
    function nav($kodeapp='',$def_database=""){
        $menunya ="";
        $id_app=0;
        $this->def_database=$def_database;
        if($def_database)$this->def_database.=".";
        if(($kodeapp)&&($id_app=$this->db->query(
            "select id_aplikasi FROM ".$this->def_database."ref_aplikasi where folder='$kodeapp'"
            )
            ->row('id_aplikasi'))){
            $this->id_app=$id_app;
            $menunya=$this->ranting();
        }
        #cek($this->db->last_query());
        $div=
             div('---','id="comen"')
             .
             div(
                div(
                    $menunya #$div_dage
                ,'class="dd" id="nestable"')
                    
                /*.div(''
                ,'class="dd" id="nestable"')*/
             ,'class="cf nestable-lists"')
             .div('','style="height:140px;margin 40px;width:50%"')
             ;
        $divkanan=
            tag('a','<i class="fa fa-file "></i>','href="#" class="btn btn-primary pull-right" id="daftar"')
            ." "
            .tag('a','<i class="fa fa-plus "></i>','href="#" class="btn btn-primary pull-right" id="tambah"')
            ." "
            .tag('a','<i class="fa fa-save "></i>','href="#" class="btn btn-success pull-right" id="simpan"')
            ."<br>"
            .div('z','class="" id="kedit"');
        $requ=  reqjs('assets/plugins/nestable/jquery.nestable.js');
        
        $csse = $this->cssnya();
        
        $send_param=enkrip(array(
            'ret_list'=>uri_string(),
            'id_app'=>$id_app,
            'def_database'=>$def_database,
            'id'=>$this->def['id'],
            'tabel'=>$this->def['tabel'],
        ));
        
       
        $pilih_app=array();
        if($apps=$this->general_model->datagrab(array(
              'tabel'=>$this->def_database.'ref_aplikasi',
              'where'=>array('aktif'=>1),
              'order'=>'urut',
              #'select'=>,
             )))
             foreach ($apps->result() as $app) 
                $pilih_app[$app->folder]="$app->folder -> $app->nama_aplikasi";
             array_unshift($pilih_app,'- Pilih App -');
             #$pilih_app[0]="Pilih App";
        
        
        $data['tabel']=div(
            div(
                div('c','id="comen"'),'class="col-md-6"')
                .div(form_dropdown('dr',$pilih_app,$kodeapp,'class="form-control pull-right" id="pilihref"'),'class="col-md-6"')
            
            .div($div,'class="col-md-6"')
            .div($divkanan,'class="col-md-6"')
        ,'class="row"')
        
        ;
        $data['include_script']=$requ;
        $data['script']=$this->skrip_daun(
            array(
                'url_cekdb'=>'urut_menu/'.$id_app,
                'def_database'=>$def_database,
                'this_uri'=>uri2('1,2,'),
                'send_param'=>$send_param,
                'uri3'=>uri(3),
            )) ;
        $data['css']=$csse;
        $data['title']='Editor Menu Navigasi';
        $data['menu_nav']=load_cnt('dm','Builder','dmnav',uri2('1,2,3'));
        $data['heads']=1;
        $data['content']='dm/default';
        $this->load->view('dm/home',$data); 
    }
    
    function daun($d) {
        $x=$d->id_nav;
        return 
            div(
                span('&nbsp;'
                  
                    
                ,'class="dd3-kanan"')
                .form_checkbox('c'.$x,$x,$d->aktif==1,'class="centang" val="'.$x.'"')
                #---------------tex nya 
                .'&nbsp; <i class="'.($d->fa>""?"fa fa-".$d->fa:"").'" ></i> '.$d->judul.($d->kode?"(".$d->kode.")":"").' '.$x.' '
                .span(
                    '<div class="btn-group"  style="margin-left: 5px;">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#" 
                        style="margin: -18px 5px 0 5px;padding: 0;width: 12px;"
                        >
                        &nbsp;&nbsp;<span class="fa fa-cog"></span>
                    </a>'
                    .'<ul class="dropdown-menu pull-right">'
                        .li('<a class="edit_menu" val="'.$x.'"><i class="fa fa-edit"></i> Edit</a>')
                        .li('<a class="hapus_menu" val="'.$x.'"><i class="fa fa-minus"></i> Hapus</a>')
                        .li('<a class="infonav" val="'.$x.'"><i class="fa fa-bookmark"></i> Wenang</a>')
                        .li('<a class="nambah_menu" val="'.$x.'"><i class="fa fa-plus"></i> Tambah</a>')
                        .li('<a class="dup_menu" val="'.$x.'"><i class="fa fa-plus"></i> Duplikat</a>')
                    .'</ul>'
                    
                    .'</div>'
                    )
            ,'class="dd3-content '.($d->kode?"kuning":"").'"');
    }
    
    function skrip_daun($parm) {
        extract($parm);
        $jq=
        #tag_jqready
        ("
        function cekdb(data) {
            $('#comen').html('Process');
            $.ajax({
              url: '".site_url($this_uri.$url_cekdb)."'".($def_database?"+'/".$def_database."'":"").",
              type: 'POST',
              data:'g='+data,
              cache: false,
              dataType: 'json',
              success: function(msg) {
                  console.log(msg);
                  $('#comen').html('done');
              },
              error:function(error){
                $('#comen').html('..');
              }
          });
        }
        

        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        })
        
        function load_menu(edit,id,next_id) {
            var kirim=id;
            if(next_id)kirim+='/'+next_id;
            var urls=[
                '".site_url($this_uri.'wenang')."/',
                '".site_url($this_uri.'edit_menu')."/',
                '".site_url($this_uri.'lizt_menu')."/',
                '".site_url($this_uri.'dup_menu')."/'
                ];
            $.ajax({
                  url: urls[edit]+kirim,
                  type: 'POST',                  
                  cache: false,
                  dataType: 'json',
                  data:'g=".$send_param."',
                  success: function(hasil) {
                      $('#kedit').html(hasil.dta).show();
                      //console.log(hasil);
                  },
                  error:function(error){
                  
                  }
              });
        }
        
        $('#tambah').click(function () {
            load_menu(1,0,0);
        });
        
        $('.edit_menu').click(function () {
            load_menu(1,$(this).attr('val'),0);
        });
        
        $('.nambah_menu').click(function () {
            load_menu(1,0,$(this).attr('val'));
        });
        
        $('.dup_menu').click(function () {
            load_menu(3,$(this).attr('val'),0);
        });
        
        $('.hapus_menu').click(function () {
            if(confirm('bener mau di hapus ?'))
                window.location='".site_url($this_uri.'hapus_menu').'/'.$send_param.'/'."'+$(this).attr('val');
        });
        
        $('.infonav').click(function () {
            load_menu(0,$(this).attr('val'),0);
        });
        
        $('#daftar').click(function () {
            load_menu(2,0,0);
        });
        
        $('#simpan').click(function () {
            //alert(e);
            var outp=window.JSON.stringify($('#nestable').nestable('serialize'));
                cekdb(outp);
            console.log(outp);
        });
        
        $('#pilihref').change(function () {
            //alert($(this).val());
            if(confirm('bener mau di ganti aplikasinya ?'))
                window.location='".site_url($this_uri.$uri3)."/'+$(this).val()".($def_database?"+'/".$def_database."'":"").";
        });
        
        $('.centang').click(function () {
            if(confirm('bener mau di klik ?'+$(this).prop('checked')))
                window.location='".site_url($this_uri.'aktifkan').'/'
                    .$send_param
                    .'/'."'+$(this).attr('val')+'/'+$(this).prop('checked');
            return false;
        });
        
        
        ");
        
        return $jq;
    }
    
    function ranting($dahan=0) {
        #nav ----
        $li="";
        $sql_jm="SELECT id_par_nav,COUNT(*)jm 
        FROM ".$this->def_database."nav GROUP BY id_par_nav";
        $tabel_nav=array(
             'tabel'=>array(
             
                $this->def_database.'nav n'=>'',
                "( $sql_jm )n2"=>array('n2.id_par_nav=n.id_nav','left'),
                ),
             'where'=>array('n.id_aplikasi'=>$this->id_app),
             'select'=>'n.*,n2.jm',
             'order'=>'n.id_par_nav,n.urut',
        );
        if($dahan==0){
            $tabel_nav['where']['(ISNULL(n.`id_par_nav`) OR n.`id_par_nav`=0)']=null;
        }else{
            $tabel_nav['where']['n.id_par_nav']=$dahan;
        }
        if(($nav=$this->general_model->datagrab($tabel_nav))&&($nav->num_rows()>0))
        foreach ($nav->result() as $d)
            $li.=li(    
                    div(''
                    ,'class="dd-handle dd3-handle"')
                    .$this->daun($d)                    
                    .($d->jm > 0?$this->ranting($d->id_nav):'')
                    ,'class="dd-item dd3-item" data-id="'.$d->id_nav.'"'
                    )
                    ;
        return ol($li,'class="dd-list"');
    }
    
    
    /**
    * menampilkan menu dg pligin nested       
    * milik nav
    */
    function ol_li2($data) {
        #nav ----
        $li="";
        foreach ($data as $x => $d) 
            $li.=li(    
                    div(''
                    ,'class="dd-handle dd3-handle"')
                    .div(
                        span(' '
                            .form_checkbox('c'.$x,$x,$d['aktif']#$this->cek_is_bidang($d['text'])
                        )
                        .span(' '
                            .'<a class="edit_menu" val="'.$x.'"><i class="fa fa-edit"></i></a>'
                            .'<a class="hapus_menu" val="'.$x.'"><i class="fa fa-minus"></i></a>'
                            .'<a class="infonav" val="'.$x.'"><i class="fa fa-bookmark"></i></a>'                            
                            .'<a class="nambah_menu" val="'.$x.'"><i class="fa fa-plus"></i></a>'                            
                        ,'class="icoo"')
                        
                        #--------------E- drop down        
                            ,'class="dd3-kanan"').
                        #---------------tex nya 
                        ' '.$d['text'].($d['kode']?"(".$d['kode'].")":"").' '.$x.' '
                    ,'class="dd3-content"')
                    
                    .(isset($d['anak']) && (is_array($d['anak']))?$this->ol_li2($d['anak']):'')
                    ,'class="dd-item dd3-item" data-id="'.$x.'"'
                    )
                    ;
        return ol($li,'class="dd-list"');
    }
    
    function gettes(){
        $this->benchmark->mark('a1');
        $p=post(null);
        #susunan dage
        $g=json_decode($p['g'],true);
        #yang bidang
        $c=json_decode($p['c'],true);
        #dde colom with id
        $dde=json_decode( un_de($p['dde']),true);
        #akhiran nam unit
        $suf=trim($p['s']);
        $lensuf=strlen($suf);
        #nama tabel
        $nt=$p['nt'];
        #start id from 
        $stf=$p['stf'];
        $default_induk=$p['pmrt'];
        #sudah ada id_bidang kah
        #$is_id=$p['iid'];
        #data asli
        $dage= dekrip($p['dage'],true);
        
        $this->dage=$dage;
        
        $insert_unit=$insert_bid=$update_tmp=
        $bidang=$unit=$update
            =array();
        
        foreach ($dage as $x => $dg) {
            $update[]=array(
                'where'=>array('f22'=>$dg['text'],'f23'=>$dg['up'],),
                'upd'=>array('id_bidang'=>$stf+$x),
            );
            if( in_array('c'.$x,$c)){
                #ini bidang
                if(!@$dg['next']){
                    #Khusus yang nda ada unit nya blos selelsai
                    $unit[]=array(
                        'id_unit'=>$stf+$x,
                        'id_par_unit'=>0,
                        'unit'=>(substr($dg['text'],-$lensuf)==$suf?$dg['text']:$dg['text']." $suf"),
                        'alamat'=>$dg['text'],
                        'telp'=>$suf,
                        'aktif'=>2,
                    );
                    $bidang[]=array(
                        'id_bidang'=>$stf+$x,
                        'id_par_bidang'=>0,
                        'id_unit'=>$stf+$x,
                        'nama_bidang'=>$dg['text'],
                        'level'=>1,
                        'aktif'=>2,
                    );  
                }else{
                    $idparbid=$idunit=0;
                    if(in_array('c'.$dg['next'],$c)){
                        #jika atasnya bidang juga
                        if((@$dage[ $dg['next']]['next'])){
                            #jika ada atas dari atasnya juga
                            if(in_array('c'.$dage[ $dg['next']]['next'],$c)){
                                #jika di atas ada bidang lagi
                                $idparbid=$stf+$dg['next'];
                                IF(@$dage[$dage[$dg['next']]['next']]['next']){
                                    $idunit=$stf+$dage[$dage[$dg['next']]['next']]['next'];
                                }else{
                                    $idunit=$stf+$default_induk;#***
                                }
                            }else{
                                #jika atasnya masih bidang
                                $idparbid=$stf+$dg['next'];
                                $idunit=$stf+$dage[$dg['next']]['next'];
                            }
                        }else{
                            $idparbid=$stf+$dg['next'];
                            $idunit=$stf+$dg['next'];
                        }
                    }else{
                        #atasnya unit
                        $idunit=$stf+$dg['next'];
                    }
                    $bidang[]=array(
                        'id_bidang'=>$stf+$x,
                        'id_par_bidang'=>$idparbid,
                        'id_unit'=>$idunit,
                        'nama_bidang'=>$dg['text'],
                        'level'=>1,
                        'aktif'=>2,
                    );

                }
                
            }else{
                #ini unit
                $unit[]=array(
                    'id_unit'=>$stf+$x,
                    'id_par_unit'=>(@$dg['next']?$stf+$dg['next']:0),
                    'unit'=>(substr($dg['text'],-$lensuf)==$suf?$dg['text']:$dg['text']." $suf"),
                    'alamat'=>$dg['text'],
                    'telp'=>$suf,
                    'aktif'=>2,
                );
                $bidang[]=array(
                    'id_bidang'=>$stf+$x,
                    'id_par_bidang'=>0,
                    'id_unit'=>$stf+$x,
                    'nama_bidang'=>$dg['text'],
                    'level'=>1,
                    'aktif'=>2,
                );
            }
        
        }
        
        /*$this->dde=$dde;
        foreach ($g as $ge) 
            $this->pasang_anak($ge,0,$c);
        */    
        
        
        $this->benchmark->mark('a2');

  
        #save
        $save=1;
        
        #cek field saat ini
        $field=load_controller('dm','builder','list_fields',$nt,'Field');
        $is_id=(in_array('id_bidang',$field)?1:2);
        
        if($is_id==1){
            $sql_del="ALTER TABLE $nt DROP COLUMN id_bidang";
            $sql_del_unit="DELETE FROM ref_unit WHERE id_unit >=$stf and id_unit <=".($stf+9999);
            $sql_del_bidang="DELETE FROM ref_bidang WHERE id_bidang >=$stf and id_bidang <=".($stf+9999);
            $this->db->query($sql_del);
            $this->db->query($sql_del_unit);
            $this->db->query($sql_del_bidang);
        }
        
        if($save)
        if($is_id==2){
            $sql_alter="ALTER TABLE `$nt` ADD COLUMN `id_bidang` INT(11) NULL";
            $this->db->query($sql_alter);
            if(count($unit)>0)$this->db->insert_batch('ref_unit',$unit);
            if(count($bidang)>0)$this->db->insert_batch('ref_bidang',$bidang);
        }
        
        $this->benchmark->mark('a3');

        if($save)
        if(count($update)>0)
            foreach ($update as $up) 
                $this->db->where($up['where'])->update($nt,$up['upd']);
        
       
        $this->benchmark->mark('a4');

        $sql_cek=
        "SELECT
            t.`f1` nip
            ,t.`f3` nama
            ,t.`f22`
            ,t.`f23`
            ,rb.`nama_bidang` bidang
            ,u.`alamat` a
            ,u2.`alamat`a2
            ,IF(t.`f22`=rb.`nama_bidang`,1,2)bid
            ,IF(t.`f23`=u.`alamat`,1,2)un
            ,IF(t.`f23`=u2.`alamat`,1,2)un2
        FROM $nt t
        LEFT JOIN ref_bidang rb ON rb.`id_bidang`=t.id_bidang
        LEFT JOIN ref_unit u ON u.`id_unit`=rb.`id_unit`
        LEFT JOIN ref_unit u2 ON u2.`id_unit`=u.`id_par_unit`
        WHERE IF(t.`f23`=u.`alamat`,1,IF(t.`f23`=u2.`alamat`,1,2))=2
        ";
        $no=1;
        $tbl="";
        
        if(($tes=$this->db->query($sql_cek))&&($tes->num_rows()>0)){
            $tbl.=tr(th('No')
                    .th('nip').th('nama').th('f22').th('f23')
                    .th('bidang').th('abidang').th('unit')
                    .th('bid').th('un').th('un2')
                );
            foreach ($tes->result() as $res) 
                $tbl.=tr(
                    td($no++)
                    .td($res->nip).td($res->nama).td($res->f22).td($res->f23)
                    .td($res->bidang).td($res->a).td($res->a2)
                    .td($res->bid).td($res->un).td($res->un2)
                );
           $tbl=table($tbl,'class="table table-condensed"');
        }
        
     
        die(json_encode(array(
            'benc'=>"t12: ".$this->benchmark->elapsed_time('a1','a2')
                ." t23: ".$this->benchmark->elapsed_time('a2','a3')
                ." t34: ".$this->benchmark->elapsed_time('a3','a4'),
            'dage'=>$dage,
            'is_id'=>$is_id,
            'update'=>$update,
            'suf'=>$suf,
            'stf'=>$stf,
            'bidang'=>$bidang,
            'unit'=>$unit,
            'tbl'=>$tbl,
            'default_induk'=>$default_induk,
        )));
        
        
        #dari bidang buat unit -> bid alas unit kehut
        #dari unit buat bidang -> unit puske bidang puske
        #semua unit tambah sufix -> biak numfor
        #simpan unit dan simpan id nya 1:1
        #simpan bidang dn id nya 1:1
        #isi id_bidang di tmp_peg dengan compare nama bidang dan unit nya
        #tinggal buat jabtan dg id_bidang
    }
    
    function pasang_anak($d,$id_induk,$bid) {
        if(in_array('c'.$d['id'],$bid)){
            #bidang
            $this->bidang[$d['id']]=array(
                'text'=>$this->dde[$d['id']],
                'id_unit'=>$id_induk,
            );
        }else{
            #unit
            $this->unit[$d['id']]=array(
                'text'=>$this->dde[$d['id']],
                'id_par_unit'=>$id_induk
            );
            $this->bidang[$d['id']]=array(
                'text'=>$this->dde[$d['id']],
                'id_unit'=>$d['id'],
            );
        }   
        if(@$d['children'])
        foreach ($d['children'] as $d2) $this->pasang_anak($d2,$d['id'],$bid);
    }
    
    function find_pemerintah($ar) {
        $ketemu=0;
        foreach ($ar as $x => $v) 
        if(substr($v,0,10)=='PEMERINTAH'){
            $ret=$x;break;
        }
        if($ketemu){
            return $ret;
        }else{
            return false;
        }
    }
    
    function kirim_menu() {
        extract($p=post(null));
        cek($p);
    }
    
    function edit_menu($id,$par=0,$menunya="") {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])$def_database.=".";
        }
        $ok=$ret=$js=$fr=$data="";
        $new=0;
        $menu=null;
        if($id==0){
            $new=1;
            if($par)
                $fr=form_label('Par : '.$par)."<br>\n";
            #$menu=(object)array('id_par_nav'=>$par,'judul2'=>"sudah terisi");
            
        }else{
            $menu=$this->general_model->datagrab(array(
               'tabel'=>array(
                $def_database.'nav n'=>'',
                $def_database.'nav n2'=>array('n2.id_nav=n.id_par_nav','left'),
               ),
               'where'=>array('n.id_nav'=>$id),
               'select'=>'n.*,n2.judul judul2',
              ))->row();
            $fo=array(9,false,'Par Menu','id_par_nav',uri2('1,2,').'auto_menu/'.$id_app.($def_database?"/".$def_database:""),'','judul2');
            $this->load->library('Defa');
            list($fr,$data)=$dataa=$this->defa->inp9_select2( $fo,$menu,$data); 
            #load_cnt(uri(1),'Def','inp9_select2',$fo,$menu,$data);
            $js.=implode("\n",$data['include'])."\n";
            $data['script'].=
                "$('#upinmenu').click(function () {
                    alert($('#enkmenu').val());
                })
            ";
            $js.=tag_jqready( $data['script']);
        }
        $js.=tag_jqready("$('.btrmv').click(function () { $('#kedit').html('');})");
        $data=array();
        
        $hidden_form=array(
            'id_nav'=>($new?"":$menu->id_nav),
            #'id_par_nav'=>($new?$par:$menu->id_par_nav),
            'ret'=>$ret_save,
            'fldt'=>$def_database.'nav',
            'fldid'=>'id_nav',
            'id_aplikasi'=>($new?$id_app:$menu->id_aplikasi),
            'fld'=>enkrip(array(
                'id_par_nav','id_aplikasi','ref','kode','tipe','judul','link','fa','aktif'
            )),
            'urut'=>($new?0:$menu->urut),            
        );
        if($new && $par){
            $hidden_form['id_par_nav']=$par;
        }
        $ret=/*form_open(uri2('1,2,').'save_menu','savemenu',$hidden_form)
                
             .div(
                div(
                    div(
                    div(
                        '<input id="enkmenu" type=text value="'
                            .enkrip($menu,1)
                        .'" class="form-control"/>'
                        .'<span class="input-group-addon btn" id="upinmenu"><i class="fa fa-check"></i></span>'
                        ,'class="input-group"')
                    .'<br>'
                      .$fr
                      .form_label('Judul')
                    .form_input('judul',($new?"":$menu->judul),'class="form-control"')
                      .form_label('Link')
                    .form_input('link',($new?"":$menu->link),'class="form-control"')
                      .form_label('Fa')
                    .form_input('fa',($new?"":$menu->fa),'class="form-control"')
                    .div(
                        div(
                             form_label('Kode')
                             .form_input('kode',($new?"":$menu->kode),'class="form-control"')
                        ,'class="col-lg-6"')
                        .div(
                            form_label('Referensi')
                            .form_dropdown('ref',array(1=>'Bukan','Ya'),($new?1:$menu->ref),'class="form-control"')
                        ,'class="col-lg-6"')
                      ,'class="row"')
                    .div(
                        div(
                             form_label('Tipe')
                             .form_dropdown('tipe',array(1=>'Kewenangan','Menu'),($new?2:$menu->tipe),'class="form-control"')
                        ,'class="col-lg-6"')
                        .div(
                            form_label('Aktif')
                            .form_dropdown('aktif',array('Non Aktif','Aktif'),($new?"1":$menu->aktif),'class="form-control"')
                        ,'class="col-lg-6"')
                      ,'class="row"')
                    )
                    ."<input type=submit name='' value='Simpan' class='btn btn-success'/>"# form_submit('','Simpan')
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            .form_close()*/
             div(
                div(div('<button class="btn btn-box-tool btrmv" data-widget="remove"><i class="fa fa-times"></i></button>','class="pull-right box-tools"'),'class="box-header"')
                .$this->form_menu($new,$hidden_form,$menu,$fr)
            ,'class="box"')
            .$js;
            
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function form_menu($new=1,$hidden="",$menu="",$fr="") {
        return form_open(uri2('1,2,').'save_menu',array('id'=>'savemenu'),$hidden)
                
             .div(
                div(
                    div(
                    div(
                        '<input id="enkmenu" type=text value="'
                            .enkrip($menu,1)
                        .'" class="form-control"/>'
                        .'<span class="input-group-addon btn" id="upinmenu"><i class="fa fa-check"></i></span>'
                        ,'class="input-group"')
                    .'<br>'
                    .div('F:"'.implode_array($menu).'"'
                        ,'id=imar style="height: 42px;overflow-y: auto;border: 1px solid #ccc;"').'<br>'
                      .$fr
                      .form_label('Judul')
                    .form_input('judul',($new==1?"":$menu->judul),'class="form-control"')
                      .form_label('Link')
                    .form_input('link',($new==1?"":$menu->link),'class="form-control"')
                      .form_label('Fa')
                    .form_input('fa',($new==1?"":$menu->fa),'class="form-control"')
                    .div(
                        div(
                             form_label('Kode')
                             .form_input('kode',($new==1?"":$menu->kode),'class="form-control"')
                        ,'class="col-lg-6"')
                        .div(
                            form_label('Referensi')
                            .form_dropdown('ref',array(1=>'Bukan','Ya'),($new==1?1:$menu->ref),'class="form-control"')
                        ,'class="col-lg-6"')
                      ,'class="row"')
                    .div(
                        div(
                             form_label('Tipe')
                             .form_dropdown('tipe',array(1=>'Kewenangan','Menu'),($new==1?2:$menu->tipe),'class="form-control"')
                        ,'class="col-lg-6"')
                        .div(
                            form_label('Aktif')
                            .form_dropdown('aktif',array('Non Aktif','Aktif'),($new==1?"1":$menu->aktif),'class="form-control"')
                        ,'class="col-lg-6"')
                      ,'class="row"')
                    )
                    ."<input type=submit name='' value='Simpan $new' class='btn btn-success'/>"# form_submit('','Simpan')
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            .form_close();
    }
    
    function dup_menu($id,$par=0) {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])$def_database.=".";
        }
        $ok=$ret=$js=$fr=$data="";
        $new=3;
        $menu=$this->general_model->datagrab(array(
           'tabel'=>array(
            $def_database.'nav n'=>'',
            $def_database.'nav n2'=>array('n2.id_nav=n.id_par_nav','left'),
           ),
           'where'=>array('n.id_nav'=>$id),
           'select'=>'n.*,n2.judul judul2',
          ))->row();
        $fo=array(9,false,'Par Menu','id_par_nav',uri2('1,2,').'auto_menu/'.$id_app.($def_database?"/".$def_database:""),'','judul2');
        $this->load->library('Defa');
            
        list($fr,$data)=$dataa=$this->defa->inp9_select2( $fo,$menu,$data); 
        #load_cnt(uri(1),'Def','inp9_select2',$fo,$menu,$data);
        $js.=implode("\n",$data['include'])."\n";
        $js.=tag_jqready( $data['script']
            ." $('.btrmv').click(function () { $('#kedit').html('');})");
        $data=array();
        
        $hidden_form=array(
            #'id_nav'=>($new?"":$menu->id_nav),
            #'id_par_nav'=>($new?$par:$menu->id_par_nav),
            'id_par_nav'=>$menu->id_par_nav,
            'ret'=>$ret_save,
            'fldt'=>$def_database.'nav',
            'fldid'=>'id_nav',
            'id_aplikasi'=>$menu->id_aplikasi,
            'fld'=>enkrip(array(
                'id_par_nav','id_aplikasi','ref','kode','tipe','judul','link','fa','aktif','urut'
            )),
            'urut'=>($menu->urut +1),            
        );
        $ret=
            div(
                div(div('<button class="btn btn-box-tool btrmv" data-widget="remove"><i class="fa fa-times"></i></button>','class="pull-right box-tools"'),'class="box-header"')
                .$this->form_menu($new,$hidden_form,$menu,$fr)
                ,'class="box"')
        /*form_open(uri2('1,2,').'save_menu','savemenu',$hidden_form)
                
             .div(
                div(
                    div('Dup Form :"'.implode_array($menu).'"<br>'
              .$fr
              .form_label('Kode')
            .form_input('kode',($menu->kode),'class="form-control"')
              .form_label('Referensi')
            .form_dropdown('ref',array(1=>'Bukan','Ya'),($menu->ref),'class="form-control"')
              .form_label('Tipe')
            .form_dropdown('tipe',array(1=>'Kewenangan','Menu'),($menu->tipe),'class="form-control"')
              .form_label('Judul')
            .form_input('judul',($menu->judul),'class="form-control"')
              .form_label('Link')
            .form_input('link',($menu->link),'class="form-control"')
              .form_label('Fa')
            .form_input('fa',($menu->fa),'class="form-control"')
              .form_label('Aktif')
            .form_dropdown('aktif',array('Non Aktif','Aktif'),($menu->aktif),'class="form-control"')
            )
            ."<input type=submit name='' value='Simpan' class='btn btn-success'/>"# form_submit('','Simpan')
            
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            .form_close()*/
            .$js;
            
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function wenang($id_nav) {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        $def_database="";
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])
                $def_database.=".";
        }
        $ok=$ret=$js=$menu="";$awal=$role=array();
        $js=tag_jqready("
            $('#savewenang').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                  url: $(this).attr('action'),
                  type: 'POST',
                  cache: false,
                  dataType: 'json',
                  data:$(this).serialize(),
                  contentType: 'application/x-www-form-urlencoded',
                  success: function(msg) {
                      $('#kedit').html('');
                  },
                  error:function(error){
                  
                  }
                });
                return false;
            });
            $('.btrmv').click(function() { $('#kedit').html('')})
        ");
        $sql_nav="SELECT rr.*,rn.id_role_nav
            FROM ".$def_database."ref_role rr
            LEFT JOIN ".$def_database."ref_role_nav rn ON rn.id_role=rr.id_role AND rn.id_nav=$id_nav
            WHERE rr.id_aplikasi=$id_app 
            order by rr.id_role
            ";
            
        if(($body=$this->db->query($sql_nav))&&($body->num_rows()>0))foreach ($body->result() as $re){
            $menu.=form_label(form_checkbox('hay[]',$re->id_role,$re->id_role_nav)
                ." ".$re->nama_role)."<br>\n";
            $role[]=$re->id_role;
            $awal[]=$re->id_role_nav;
        }
        $hidden=array(
                'id_nav'=>($id_nav),
                'role'=>implode(',',$role),
                'awwal'=>implode(',',$awal),
                #'ret'=>$ret_save,
                'id_aplikasi'=>$id_app,
                'def_database'=>$def_database,
                );
        
        $ret=div(
            div(div('<button class="btn btn-box-tool btrmv" data-widget="remove"><i class="fa fa-times"></i></button>','class="pull-right box-tools"'),'class="box-header"')
            .div(form_open(uri2('1,2,').'save_wenang',array('id'=>'savewenang'),$hidden)
             .div(
                div(
                    div('Form Kewenangan:'.'<br>'
                    .$menu
            ."<input name=auo type=submit value='Simpan' class='btn btn-success'/>"#.form_submit('','Simpan')
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"'))
            .form_close(),'class="box-body"')
            ,'class="box"')
            .$js;
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function lizt_menu() {
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])
                $def_database.=".";
        }
        
        $sql_nav="SELECT *
            FROM ".$def_database."nav
            WHERE id_aplikasi=$id_app and aktif=1
            order by urut,ref desc
            ";
        $menu="\$nav = array(<br>\n";
        $nav=array();
        if(($body=$this->db->query($sql_nav))&&($body->num_rows()>0))foreach ($body->result_array() as $re)
            $nav[$re['id_nav']]=$re;
        foreach ($nav as $x => $n) {
            
            $menu.="array(0,"
                .$n['ref'].","
                ."'".$n['kode']."',"
                .$n['tipe'].","
                ."'".$n['judul']."',"
                .($n['id_par_nav']>0 && !empty($nav[$n['id_par_nav']])?"'".$nav[$n['id_par_nav']]['judul']:"'")."',"
                ."'".$n['link']."',"
                ."'".$n['fa']."'),<br>\n";
        }
        $en=enkrip($nav,1);
        #cek($nav);
        $menu.="),<br>\n";
        $ok=1;
        $ret=
            div(
            div(div('<button class="btn btn-box-tool btrmv" data-widget="remove"><i class="fa fa-times"></i></button>','class="pull-right box-tools"'),'class="box-header"')
            .div(
                div(
                    div(
                        div(
                            form_input('fr',
                                $en
                            ,'class="form-control"')    
                            .span(
                                '<i class="fa fa-check"></i>'
                            ,'class="input-group-addon btn"')
                        ,'class="input-group"')
                    ,'class="col-lg-12"')
                    .div('Form Kewenangan:"'.'"<br>'
                        .table(tr(td($menu)),'class="table table-condensed table-nonfluid""')
                    ,'class="col-md-11 col-md-offset-1 table-responsive no-padding"  style="height: 450px;"')
            ,'class="row"')
            ,'class="edit box-body"')
            ,'class="box"').
            tag_jqready("$('.btrmv').click(function() { $('#kedit').html('')})")
            //style="overflow-y: scroll;"
            ;
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function save_wenang() {
        $p=post(null);
        extract($p);
        $buat=$hapus=array();
        if($role)$role=explode(',',$role);
        if($awwal)$awwal=explode(',',$awwal);
        foreach ($role as $x => $v) {
            if(!empty($hay) && in_array($v,$hay)){
                if(empty($awwal[$x]))$buat[]=$v;                
            }else{
                if($awwal[$x])$hapus[]=$awwal[$x];
            }
        }
        if(is_array($buat))foreach ($buat as $idrole) 
            $this->db->insert($def_database.'ref_role_nav',array(
                'id_role'=>$idrole,
                'id_nav'=>$id_nav,
            ));
        if(is_array($hapus))foreach ($hapus as $idrolenav) 
            $this->db->where('id_role_nav',$idrolenav)->delete($def_database.'ref_role_nav');
        /*stop(array(
            "b"=>$buat,"h"=>$hapus,'p'=>$p,));
        */
        if(@$ret){
            redirect($ret);
        }else{
            die(json_encode(array('ret'=>1)));
            /*stop(array(
                $p,
                $fld,
                $save
            ));*/
        }
    }
    
    function save_menu() {
        $p=post(null);
        if(@$p['fldt']&&@$p['fld']&&@$p['fldid']){
            $fld=dekrip($p['fld'],true);
            $save=array();
            foreach ($fld as $f) if(($f=='id_par_nav')&&(!$p[$f])){
                $save[$f]=NULL;
            }else{
                $save[$f]=$p[$f];
            }
            if(@$p[$p['fldid']]){
                $this->db->where(array($p['fldid']=>$p[$p['fldid']],))
                ->update($p['fldt'],$save);
            }else{
                $this->db->insert($p['fldt'],$save);
            }
            
            if(@$p['ret']){
                redirect($p['ret']);
            }else{
                stop(array(
                    $p,
                    $fld,
                    $save
                ));
            }
        }
    }
    
    function kop() {
        $form=array(
            array(1,)
        ) ;
        
       form_open(uri(1).'/'.uri(2).'/savekop','',array(
       ))
       
       .form_close() ;
    }
    
    function savekop() {
        $p=post(null);
        cek($p);
    }
    
    function hapus_menu($par,$id) {
        $p=dekrip($par,1);
        $this->db->where('id_nav',$id)->delete(($p['def_database']?$p['def_database'].".":"").'nav');
        redirect($p['ret_list']);
    }
    
    function ngurut($dt,$id_par=null){
        if(is_array($dt)){
            $this->nap[]=array(
                'id'=>$dt['id'],
                'data'=>array(
                    'id_par_nav'=>$id_par,
                    'urut'=>$this->urut,
                ),
            );
            $this->urut++;
            if(isset($dt['children']))
                foreach ($dt['children'] as $v) 
                    $this->ngurut($v,$dt['id']);
        }
    }
    
    function urut_menu($id_app,$def_database="") {
        $p=post(null);
        if($def_database)$def_database.=".";
        $this->urut=1;
        if($p['g']){
            $pg=json_decode($p['g'],1);
            if(is_array($pg))
                foreach ($pg as $v)
                    $this->ngurut($v);
        }
        if(is_array($this->nap))
            foreach ($this->nap as $nv)
                $this->db->where('id_nav',$nv['id'])
                    ->update($def_database.'nav',$nv['data']);
        
        /*stop(array(
        'x'=> $this->nap,
        $p,
        json_decode($p['g'],1),
        ));*/
        #redirect($p['ret_list']);
        echo 1;
    }
    
    function auto_menu($id_app,$def_database="") {
    
        $q = $this->input->post('q');
        $q1 = $this->input->post('page');
        #if($def_database)$def_database.=".";
        
        $res = $this->general_model->datagrab(array(
            'tabel' => $def_database.'nav',
            
            'select' => "id_nav id, judul data",
            'where'=>array(
                "tipe"=>2,
                "id_aplikasi" => $id_app,
                "judul like '%$q%'"=> null,
                ),
            
            'limit' => 20, 'offset' => 0,
        ));
          
        $re=obj2ar($res->result());
        array_unshift($re,array('id'=>'','data'=>"--tanpa par--"));
           # cek($re);
        die(json_encode($re));
        
    }
   
    
  }
?>