<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  /**
  *  Edito nav v:002
  */
  class Navikan extends CI_Controller {
    var $def;
    var $debug=0;
    var $def_database;
    
    /*var $unit=array();
    var $bidang=array();
    var $dde=array();
    var $dage=array();
    
    var $pohong;
    var $partabel;
    #var $jml=0;*/
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cmd');
        if(!sesi('id_pegawai'))show_404();
    }
    function index(){
        redirect(uri2('1,2,').'nav/ref_bidang');
    }
    
    function defkan($def=0) {
        $sql_ref_jab="( select 
                    rj.*,rb.id_unit,rb2.id_unit id_unit2 
                    from ".$this->def_database."ref_jabatan rj 
                    left join ".$this->def_database."ref_bidang rb on rb.id_bidang=rj.id_bidang
                    LEFT JOIN ".$this->def_database."ref_jabatan rj2 ON rj2.id_jabatan=rj.id_par_jabatan
                    LEFT JOIN ".$this->def_database."ref_bidang rb2 ON rb2.id_bidang=rj2.id_bidang
                    )";
        $sql_ubidjab="( select 
                    u1.*,rb.nama_bidang,rj.nama_jabatan
                    from ".$this->def_database."ref_ubidjab u1 
                    left join ".$this->def_database."ref_bidang rb on rb.id_bidang=u1.id_bidang
                    LEFT JOIN ".$this->def_database."ref_jabatan rj ON rj.id_jabatan=u1.id_jabatan
                    )";
        $myarr=array(
            'ref_jabatan'=>array(
                'id'=>'id_jabatan',
                'id_par'=>'id_par_jabatan',
                'text'=>array('nama_jabatan'),
                'where'=>'id_unit',
                'order'=>'urut',
                'def_where'=>1,#1000165
                'filter_tabel'=>'ref_unit',
                'filter_id'=>'id_unit',
                'filter_text'=>'unit',
                'order_filter'=>'unit',
                #'script'=>'skrip_daun',
                'multi_table'=>1,
                'tabel'=>$sql_ref_jab,
                'tabel_save'=>'ref_jabatan',
                'where_other'=>array('aktif'=>1),
                'wdahan0'=>'(ISNULL(n.id_par_jabatan) OR n.id_par_jabatan=0 OR id_unit<>id_unit2)',
            ),
            'nav'=>array(
                'id'=>'id_nav',
                'id_par'=>'id_par_nav',
                'text'=>array('judul'),
                'where'=>'id_aplikasi',
                'order'=>'urut',
                'def_where'=>1,#1000165
                'filter_tabel'=>'ref_aplikasi',
                'filter_id'=>'id_aplikasi',
                'filter_text'=>'nama_aplikasi',
                'script'=>'nav',
                'daun'=>"daun_nav",
            ),
            'ref_bidang'=>array(
                'id'=>'id_bidang',
                'id_par'=>'id_par_bidang',
                'text'=>array('nama_bidang'),
                'where'=>'id_unit',
                'order'=>'urut',
                'def_where'=>1,#1000165
                'filter_tabel'=>'ref_unit',
                'filter_id'=>'id_unit',
                'filter_text'=>'unit',
            ),
            'ref_unit'=>array(
                'id'=>'id_unit',
                'id_par'=>'id_par_unit',
                'text'=>array('unit'),                
                'order'=>'urut',
                'where'=>'aktif',
                'def_where'=>1,                
                'filter_array'=>array(1=>'Aktif','Non Aktif'),
            ),
            'ref_aplikasi'=>array(
                'id'=>'id_aplikasi',
                #'id_par'=>'id_aplikasi',
                'text'=>array('nama_aplikasi'),
                'where'=>'aktif',
                'def_where'=>1,#1000165
                'order'=>'urut',
                'no_par'=>1,
                /*'filter_tabel'=>'ref_aplikasi',
                'filter_id'=>'id_aplikasi',
                'filter_text'=>'nama_aplikasi',*/
            ),
            'ref_ubidjab'=>array(
                'id'=>'id_ubidjab',
                'id_par'=>'id_par_ubidjab',
                'text'=>array('nama_jabatan'),
                'where'=>'aktif',
                'def_where'=>1,#1000165
                'order'=>'urut',
                /*'no_par'=>1,*/
                'filter_tabel'=>'ref_unit',
                'filter_id'=>'id_unit',
                'filter_text'=>'unit',
                'multi_table'=>1,
                'tabel'=>$sql_ubidjab,
                'tabel_save'=>'ref_jabatan',
                'where_other'=>array('aktif'=>1),
                //'wdahan0'=>'(ISNULL(n.id_par_jabatan) OR n.id_par_jabatan=0 OR id_unit<>id_unit2)',
            ),
            
        );
        if($def){
            $this->def=$myarr[$def];
            if(empty($this->def['multi_table']))
                $this->def['tabel']=$def;
            return 1;
        }else{
            return array_keys($myarr);
        }
    }
    
    function cssnya() {
        return /*$csse = */#tag_css
        ("
            .cf:after { visibility: hidden; display: block; font-size: 0; content: \" \"; clear: both; height: 0; }
            * html .cf { zoom: 1; }
            *:first-child+html .cf { zoom: 1; }

            a { color: #2996cc; }
            a:hover { text-decoration: none; }

            .small { color: #666; font-size: 0.875em; }
            .large { font-size: 1.25em; }
                    .dd { position: relative; display: block; margin: 0; padding: 0; max-width: 400px; list-style: none; font-size: 13px; line-height: 20px; }

            .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
            .dd-list .dd-list { padding-left: 30px; }
            .dd-collapsed .dd-list { display: none; }

            .dd-item,
            .dd-empty,
            .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

            .dd-handle { 
                display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                background: #fafafa;
                background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                -webkit-border-radius: 3px;
                        border-radius: 3px;
                box-sizing: border-box; -moz-box-sizing: border-box;
            }
            .dd-handle:hover { color: #2ea8e5; background: #fff; }
            
            .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
            .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
            .dd-item > button[data-action=\"collapse\"]:before { content: \"-\"; }

            .dd-placeholder,
            .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
            .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
                background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                  -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                     -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                          linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-size: 60px 60px;
                background-position: 0 0, 30px 30px;
            }

            .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
            .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
            .dd-dragel .dd-handle {
                -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
                        box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            }

            /**
             * Nestable Extras
             */

            .nestable-lists { display: block; clear: both;  width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

            #nestable-menu { padding: 0; margin: 20px 0; }

            #nestable-output,
            #nestable2-output,
            #nestable3-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

            #nestable2 .dd-handle {
                color: #fff;
                border: 1px solid #999;
                background: #bbb;
                background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
                background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
                background:         linear-gradient(top, #bbb 0%, #999 100%);
            }
            #nestable2 .dd-handle:hover { background: #bbb; }
            #nestable2 .dd-item > button:before { color: #fff; }

            @media only screen and (min-width: 700px) {

                .dd { float: left; width: 98%; }
                .dd + .dd { margin-left: 2%; }

            }

            .dd-hover > .dd-handle { background: #2ea8e5 !important; }

            /**
             * Nestable Draggable Handles
             */

            .dd3-content { 
                display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                background: #fafafa;
                background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                -webkit-border-radius: 3px;
                        border-radius: 3px;
                box-sizing: border-box; -moz-box-sizing: border-box;
                width:500px;
            }
            .dd3-content:hover { color: #2ea8e5; background: #fff; }

            .dd-dragel > .dd3-item > .dd3-content { margin: 0; }

            .dd3-item > button { margin-left: 30px; }

            .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
                border: 1px solid #aaa;
                background: #ddd;
                background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
                background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
                background:         linear-gradient(top, #ddd 0%, #bbb 100%);
                border-top-right-radius: 0;
                border-bottom-right-radius: 0;
            }
            .dd3-handle:before { 
                content: \"=\"; display: block; position: absolute; left: 0; 
                top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
            .dd3-handle:hover { background: #ddd; }
            

            /**
             * Socialite
             */

            .socialite { display: block; float: left; height: 35px; }
            /**
            * tambahan 
            */
            .icoo a{
                margin: 4px;
            }
            .icoo .edit_menu{color:#0000FF}
            .icoo .hapus_menu{color:#FF0000}
            .icoo .tambah_menu{color:#000000}
            .icoo .infonav{color:#00FF00}
            .kuning{color:#f39c12}
            
            @media (min-width: 1024px) {
            #kedit{
                position: fixed;
                width: 35%;
                bottom: 4%;
            }
            }
        ");
        ;
    }
    
    function list_nav() {
        $def=$this->defkan();
        $tabel="";
        foreach ($def as $d) $tabel.=li( 
                span(
                  '<i class="fa fa-ellipsis-v"></i>'  
                ,'class="handle ui-sortable-handle"')
                .anchor(uri2('1,2,').'nav/'.$d,$d,'class="btn btn-default text"')
            );
        $data['tabel']=div(
            div(ul( $tabel ,'class="todo-list ui-sortable"'),'class="col-md-6"')
        ,'class="row"');
        
        $data['title']="Ngurutz";
        $data['menu_nav']=load_cnt('dm','Builder','dmnav',uri2('1,2,3'));
        $data['content']='dm/default';
        $this->load->view('dm/home',$data); 
    }
    
    /**
    *  menu nav
    */
    function nav($defnya='',$kodeapp='',$def_database=""){
        if(!$defnya)redirect(uri2('1,2,3,').'ref_bidang');
        $menunya ="";
        $id_app=0;
        $this->defkan($defnya);
        $this->def_database=$def_database;
        if($def_database)$this->def_database.=".";
        if($kodeapp!=''){
            $this->def['def_where']=$kodeapp;
            $where=$this->def['where'];
            if($id_app=$this->db->query(
            "select id_aplikasi FROM ".$this->def_database."ref_aplikasi where $where='$kodeapp'"
            )
            ->row('id_aplikasi'))
            $this->id_app=$id_app;
            
            
        }
        $menunya=$this->ranting();
        #cek($this->db->last_query());
        $div=
             div('---','id="comen"')
             .
             div(
                div(
                    $menunya #$div_dage
                ,'class="dd" id="nestable"')
                    
                #.div('','class="dd" id="nestable"')
             ,'class="cf nestable-lists"')
             .div('','style="height:140px;margin 40px;width:50%"')
             ;
        $divkanan=
            tag('a','<i class="fa fa-file "></i>','href="#" class="btn btn-primary pull-right" id="daftar"')
            ." "
            .tag('a','<i class="fa fa-plus "></i>','href="#" class="btn btn-primary pull-right" id="tambah"')
            ." "
            .tag('a','<i class="fa fa-save "></i>','href="#" class="btn btn-success pull-right" id="simpan"')
            ."<br>"
            .div('z','class="" id="kedit"');
        $requ=  reqjs('assets/plugins/nestable/jquery.nestable.js');
        
        $csse = $this->cssnya();
        
        $send_param=enkrip(array(
            'ret_list'=>uri_string(),
            'def_database'=>$def_database,
            'id'=>$this->def['id'],
            'tabel'=>$this->def['tabel'],
            'id_app'=>$id_app,            
        ));

        $jq=
        ("
        function cekdb(data) {
            $('#comen').html('Process');
            $.ajax({
              url: '".site_url(uri2('1,2,').'urut_menu/'.enkrip($this->def))."'".($def_database?"+'/".$def_database."'":"").",
              type: 'POST',
              data:'g='+data,
              cache: false,
              dataType: 'json',
              success: function(msg) {
                  console.log(msg);
                  if(msg.ret==1){
                    $('#comen').html('done');
                  }else{
                    $('#comen').html('Err');
                  }
              },
              error:function(error){
                $('#comen').html('..');
              }
          });
        }
        

        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        })
        
        function load_menu(edit,id,next_id) {
            var kirim=id;
            if(next_id)kirim+='/'+next_id;
            var urls=[
                '".site_url(uri2('1,2,').'wenang/')."/',
                '".site_url(uri2('1,2,').'edit_menu/')."/',
                '".site_url(uri2('1,2,').'lizt_menu/')."/',
                '".site_url(uri2('1,2,').'dup_menu/')."/'
                ];
            $.ajax({
                  url: urls[edit]+kirim,
                  type: 'POST',                  
                  cache: false,
                  dataType: 'json',
                  data:'g=".$send_param."',
                  success: function(hasil) {
                      $('#kedit').html(hasil.dta).show();
                      //console.log(hasil);
                  },
                  error:function(error){
                  
                  }
              });
        }
        
        $('#tambah').click(function () {
            load_menu(1,0,0);
        });
        
        $('.edit_menu').click(function () {
            load_menu(1,$(this).attr('val'),0);
        });
        
        $('.nambah_menu').click(function () {
            load_menu(1,0,$(this).attr('val'));
        });
        
        $('.dup_menu').click(function () {
            load_menu(3,$(this).attr('val'),0);
        });
        
        $('.hapus_menu').click(function () {
            if(confirm('bener mau di hapus ?'))
                window.location='".site_url(uri2('1,2,').'hapus_menu').'/'
                    .$send_param
                    .'/'."'+$(this).attr('val');
        });
        
        $('.centang').click(function () {
            if(confirm('bener mau di klik ?'+$(this).prop('checked')))
                window.location='".site_url(uri2('1,2,').'aktifkan').'/'
                    .$send_param
                    .'/'."'+$(this).attr('val')+'/'+$(this).prop('checked');
        });
        
        $('.infonav').click(function () {
            load_menu(0,$(this).attr('val'),0);
        });
        
        $('#daftar').click(function () {
            load_menu(2,0,0);
        });
        
        $('#simpan').click(function () {
            //alert(e);
            var outp=window.JSON.stringify($('#nestable').nestable('serialize'));
                cekdb(outp);
            console.log(outp);
        });
        
        $('#pilihref').select2();
        
        $('#pilihref').change(function () {
            //alert($(this).val());
            if(confirm('bener mau di ganti aplikasinya ?'))
                window.location='".site_url(uri2('1,2,3,4'))."/'+$(this).val()".($def_database?"+'/".$def_database."'":"").";
        });
        ");
        $drop_down=div('_','class="col-md-6"');;
        $def=$this->def;
        $pilih_app=array();
        if(!empty($def['filter_tabel'])){
            $pilih_app['']="-Pilih--";
            
            $grape=array(
                  'tabel'=>$this->def_database.$def['filter_tabel'],
                  'where'=>array('aktif'=>1),
                  #'select'=>,
                 );
            if(!empty($def['order_filter'])) $grape['order']=$def['order_filter'];
                 
            if($apps=$this->general_model->datagrab($grape))
                 foreach ($apps->result() as $app) 
                    $pilih_app[$app->$def['filter_id']]=$app->$def['filter_text'];
        }
        if(@$def['filter_array'])$pilih_app=$def['filter_array'];
        
        $drop_down=div(($pilih_app?form_dropdown('dr',$pilih_app,$kodeapp,'class="form-control pull-right" id="pilihref"'):"-"),'class="col-md-6"');
        $data['tabel']=div(
            div(
                div('c','id="comen"'),'class="col-md-6"')
                .$drop_down
            .div($div,'class="col-md-6"')
            .div($divkanan,'class="col-md-6"')
        ,'class="row"')
        ;
        $data['no_role']=1;
        $data['include_script']=$requ;
        $data['script']=$jq;
        /*if(empty($def['script']))$data['script']="";
        switch($def['script']){
            case "nav":
                $data['script']=$this->skrip_daun(
                    array(
                        'url_cekdb'=>'urut_menu/'.$id_app,
                        'def_database'=>$def_database,
                        'this_uri'=>uri2('1,2,'),
                        'send_param'=>$send_param,
                        'uri3'=>uri(3),
                    )) ;
            break;
        }*/
        $data['css']=$csse;
        $data['first_title']=anchor(uri2('1,2,').'list_nav',"<< ",'class="btn btn-default"');
        $data['title']=(!empty($this->def['multi_table'])?$defnya:$this->def['tabel']);
        $data['menu_nav']=load_cnt('dm','Builder','dmnav',uri2('1,2,3'));
        $data['content']='dm/default';
        $this->load->view('dm/home',$data); 
    }
    
    function daun($d) {
        $def=$this->def;
        $x=$d->$def['id'];
        return 
            div(
                span(' '
                    .form_checkbox('c'.$x,$x,$d->aktif==1,'class="centang" val="'.$x.'"')
                    
                    .span(' '
                        #.'<a class="edit_menu" val="'.$x.'"><i class="fa fa-edit"></i></a>'
                        .'<a class="hapus_menu" val="'.$x.'"><i class="fa fa-minus"></i></a>'
                        #.'<a class="infonav" val="'.$x.'"><i class="fa fa-bookmark"></i></a>'                            
                        #.'<a class="nambah_menu" val="'.$x.'"><i class="fa fa-plus"></i></a>'                            
                    ,'class="icoo"')
                ,'class="dd3-kanan"').
                #---------------tex nya 
                ' '.$d->$def['text'][0].(@$d->kode?"(".$d->kode.")":"").' '.$x.' '
            ,'class="dd3-content '.(@$d->kode?"kuning":"").'"');
    }
    
    function ranting($dahan=0) {
        #nav ----
        $li=$wherer="";
        $def=$this->def;
        $tanpa_par=@$def['no_par']==1;
        $multi_tabel=!empty($def['multi_table']);
        
        if(!$tanpa_par){
            $sql_jm=
                $multi_tabel
                ?
                "SELECT $def[id_par],COUNT(*)jm 
                FROM $def[tabel] tg GROUP BY $def[id_par]"
                :
                "SELECT $def[id_par],COUNT(*)jm 
                FROM ".$this->def_database."$def[tabel] GROUP BY $def[id_par]";
            $tabel_nav=array(
                 'tabel'=>array(
                    $this->def_database."$def[tabel] n"=>'',
                    "( $sql_jm )n2"=>array("n2.$def[id_par]=n.$def[id]",'left'),
                    ),
                 'select'=>'n.*,n2.jm',
                 'order'=>"n.$def[id_par],n.$def[order]",
            );
            if(!empty($def['where_other'])){
                if($multi_tabel){
                    foreach ($def['where_other'] as $whk => $whv) 
                        $wherer[]="( $whk = $whv)";
                }else{
                    $tabel_nav['where']=$def['where_other'];
                }
            }
            if(@$def['where']&&@$def['def_where']){
                if($multi_tabel){
                    $wherer[]="( ".$def['where']." = ".$def['def_where']." )";
                }else{
                    $tabel_nav['where'][$def['where']]=$def['def_where'];
                }
            }
            
            
            if($dahan==0){
                if($multi_tabel){
                    if(!empty($def['wdahan0'])){
                        $wherer[]=$def['wdahan0'];
                    }else{
                        $wherer[]="(ISNULL(n.$def[id_par]) OR n.$def[id_par]=0)";
                    }
                }else{
                    $tabel_nav['where']["(ISNULL(n.$def[id_par]) OR n.$def[id_par]=0)"]=null;
                }
            }else{
                if($multi_tabel){
                    $wherer[]="( n.$def[id_par] = $dahan )";
                }else{
                    $tabel_nav['where']["n.$def[id_par]"]=$dahan;
                }
            }
        }else{
            $tabel_nav=array(
                'tabel'=>$def['tabel'],
                'order'=>$def['order'],
            );
        }        
        if($multi_tabel ){
            $sqldef=
                "Select "
                .$tabel_nav['select']." "
                ." from $def[tabel] n
                    left join ( $sql_jm )n2 on n2.$def[id_par]=n.$def[id]"
                .(is_array($wherer)?" Where ".implode(' and ',$wherer):"")
                .(!empty($def['order'])?" Order BY ".$def['order']." ":"")
                ;
            $nav=$this->db->query($sqldef);
        }else{
            $nav=$this->general_model->datagrab($tabel_nav);
        }
        if($nav->num_rows()>0)
            foreach ($nav->result() as $d)
                $li.=li(    
                    div(''
                    ,'class="dd-handle dd3-handle"')
                    .(!empty($def['daun'])?$this->$def['daun']($d):$this->daun($d))
                    .(!$tanpa_par && $d->jm > 0?$this->ranting($d->$def['id']):'')
                    ,'class="dd-item dd3-item" data-id="'.$d->$def['id'].'"'
                    )
                    ;
        return ol($li,'class="dd-list"');
        
    }
    
    function edit_menu($id,$par=0) {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])$def_database.=".";
        }
        $ok=$ret=$js=$fr=$data="";
        $new=0;
        $menu=null;
        if($id==0){
            $new=1;
            if($par)
                $fr=form_label('Par : '.$par)."<br>\n";
            #$menu=(object)array('id_par_nav'=>$par,'judul2'=>"sudah terisi");
            
        }else{
            $menu=$this->general_model->datagrab(array(
               'tabel'=>array(
                $def_database.'nav n'=>'',
                $def_database.'nav n2'=>array('n2.id_nav=n.id_par_nav','left'),
               ),
               'where'=>array('n.id_nav'=>$id),
               'select'=>'n.*,n2.judul judul2',
              ))->row();
            $fo=array(9,false,'Par Menu','id_par_nav',uri2('1,2,').'auto_menu/'.$id_app.($def_database?"/".$def_database:""),'','judul2');
            list($fr,$data)=$dataa=load_cnt(uri(1),'Def','inp9_select2',$fo,$menu,$data);
            $js.=implode("\n",$data['include'])."\n";
            $js.=tag_jqready( $data['script']);
        }
        $data=array();
        
        $hidden_form=array(
            'id_nav'=>($new?"":$menu->id_nav),
            #'id_par_nav'=>($new?$par:$menu->id_par_nav),
            'ret'=>$ret_save,
            'fldt'=>$def_database.'nav',
            'fldid'=>'id_nav',
            'id_aplikasi'=>($new?$id_app:$menu->id_aplikasi),
            'fld'=>enkrip(array(
                'id_par_nav','id_aplikasi','ref','kode','tipe','judul','link','fa','aktif','urut'
            )),
            'urut'=>($new?0:$menu->urut),            
        );
        if($new && $par)$hidden_form['id_par_nav']=$par;
        $ret=form_open(uri2('1,2,').'save_menu','savemenu',$hidden_form)
                
             .div(
                div(
                    div('Form :"'.implode_array($menu).'"<br>'
              .$fr
              .form_label('Kode')
            .form_input('kode',($new?"":$menu->kode),'class="form-control"')
              .form_label('Referensi')
            .form_dropdown('ref',array(1=>'Bukan','Ya'),($new?1:$menu->ref),'class="form-control"')
              .form_label('Tipe')
            .form_dropdown('tipe',array(1=>'Kewenangan','Menu'),($new?2:$menu->tipe),'class="form-control"')
              .form_label('Judul')
            .form_input('judul',($new?"":$menu->judul),'class="form-control"')
              .form_label('Link')
            .form_input('link',($new?"":$menu->link),'class="form-control"')
              .form_label('Fa')
            .form_input('fa',($new?"":$menu->fa),'class="form-control"')
              .form_label('Aktif')
            .form_dropdown('aktif',array('Non Aktif','Aktif'),($new?"1":$menu->aktif),'class="form-control"')
            )
            ."<input type=submit name='' value='Simpan' class='btn btn-success'/>"# form_submit('','Simpan')
            
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            .form_close()
            .$js;
            
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function save_menu() {
        $p=post(null);
        if(@$p['fldt']&&@$p['fld']&&@$p['fldid']){
            $fld=dekrip($p['fld'],true);
            $save=array();
            foreach ($fld as $f) $save[$f]=$p[$f];
            if(@$p[$p['fldid']]){
                $this->db->where(array($p['fldid']=>$p[$p['fldid']],))
                ->update($p['fldt'],$save);
            }else{
                $this->db->insert($p['fldt'],$save);
            }
            
            if(@$p['ret']){
                redirect($p['ret']);
            }else{
                stop(array(
                    $p,
                    $fld,
                    $save
                ));
            }
        }
    }
    
    
    function hapus_menu($par,$id) {
        $def=dekrip($par,1);
        $this->db->where($def['id'],$id)->delete((@$def['def_database']?$def['def_database'].".":"").$def['tabel']);
        redirect($def['ret_list']);
    }
    
    function ngurut($dt,$id_par=null){
        if(is_array($dt)){
            $tmpd=array(
                'id'=>$dt['id'],
                'data'=>array(
                    'urut'=>$this->urut,
                ),
            );
            if(!empty($this->def_id_par))$tmpd['data'][$this->def_id_par]=$id_par;
            $this->nap[]=$tmpd;
            $this->urut++;
            if(isset($dt['children']))
                foreach ($dt['children'] as $v) 
                    $this->ngurut($v,$dt['id']);
        }
    }
    
    function urut_menu($par,$def_database="") {
        $def=dekrip($par,1);
        if(!empty($def['id_par']))$this->def_id_par=$def['id_par'];
        $p=post(null);
        #stop(array($p,$def));
        if($def_database)$def_database.=".";
        $this->urut=1;
        if($p['g']){
            $pg=json_decode($p['g'],1);
            if(is_array($pg))
                foreach ($pg as $v)
                    $this->ngurut($v);
        }
        $tabelsave=(!empty($def['tabel_save'])?$def['tabel_save']:$def['tabel']);
        if(!empty($this->nap)&& is_array($this->nap)){
            foreach ($this->nap as $nv)
                $this->db->where($def['id'],$nv['id'])
                    ->update($def_database.$tabelsave,$nv['data']);
        
            /*stop(array(
            'x'=> $this->nap,
            $p,
            json_decode($p['g'],1),
            ));*/
            #redirect($p['ret_list']);
            die(json_encode(array('ret'=>1))) ;
        }else{
            die(json_encode(array('ret'=>2))) ;
        }
    }
    
    function aktifkan($par,$id,$val=true) {
        $def=dekrip($par,1);
        if($def_database)$def_database.=".";
        $this->db->where($def['id'],$id)
            ->update(($def['def_database']?$def['def_database'].".":"").$def['tabel'],array('aktif'=>($val=="false"?2:1)));
        redirect($def['ret_list']);        
    }
    
    function auto_menu($id_app,$def_database="") {
    
        $q = $this->input->post('q');
        $q1 = $this->input->post('page');
        #if($def_database)$def_database.=".";
        
        $res = $this->general_model->datagrab(array(
            'tabel' => $def_database.'nav',
            
            'select' => "id_nav id, judul data",
            'where'=>array(
                "tipe"=>2,
                "id_aplikasi" => $id_app,
                "judul like '%$q%'"=> null,
                ),
            
            'limit' => 20, 'offset' => 0,
        ));
          
        $re=obj2ar($res->result());
        array_unshift($re,array('id'=>'','data'=>"--tanpa par--"));
           # cek($re);
        die(json_encode($re));
        
    }
   
    function daun_nav($d) {
        $x=$d->id_nav;
        return 
            div(
                span('&nbsp;'
                  
                    
                ,'class="dd3-kanan"')
                .form_checkbox('c'.$x,$x,$d->aktif==1,'class="centang" val="'.$x.'"')
                #---------------tex nya 
                .' '.$d->judul.($d->kode?"(".$d->kode.")":"").' '.$x.' '
                .span(
                    '<div class="btn-group"  style="margin-left: 5px;">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#" 
                        style="margin: -18px 5px 0 5px;padding: 0;width: 12px;"
                        >
                        &nbsp;&nbsp;<span class="fa fa-cog"></span>
                    </a>'
                    .'<ul class="dropdown-menu pull-right">'
                        .li('<a class="edit_menu" val="'.$x.'"><i class="fa fa-edit"></i> Edit</a>')
                        .li('<a class="hapus_menu" val="'.$x.'"><i class="fa fa-minus"></i> Hapus</a>')
                        .li('<a class="infonav" val="'.$x.'"><i class="fa fa-bookmark"></i> Wenang</a>')
                        .li('<a class="nambah_menu" val="'.$x.'"><i class="fa fa-plus"></i> Tambah</a>')
                        .li('<a class="dup_menu" val="'.$x.'"><i class="fa fa-plus"></i> Duplikat</a>')
                    .'</ul>'
                    
                    .'</div>'
                    )
            ,'class="dd3-content '.($d->kode?"kuning":"").'"');
    }
    
    function dup_menu($id,$par=0) {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])$def_database.=".";
        }
        $ok=$ret=$js=$fr=$data="";
        $new=1;
        $menu=$this->general_model->datagrab(array(
           'tabel'=>array(
            $def_database.'nav n'=>'',
            $def_database.'nav n2'=>array('n2.id_nav=n.id_par_nav','left'),
           ),
           'where'=>array('n.id_nav'=>$id),
           'select'=>'n.*,n2.judul judul2',
          ))->row();
        $fo=array(9,false,'Par Menu','id_par_nav',uri2('1,2,').'auto_menu/'.$id_app.($def_database?"/".$def_database:""),'','judul2');
        list($fr,$data)=$dataa=load_cnt(uri(1),'Def','inp9_select2',$fo,$menu,$data);
        $js.=implode("\n",$data['include'])."\n";
        $js.=tag_jqready( $data['script']);
        $data=array();
        
        $hidden_form=array(
            #'id_nav'=>($new?"":$menu->id_nav),
            #'id_par_nav'=>($new?$par:$menu->id_par_nav),
            'id_par_nav'=>$menu->id_par_nav,
            'ret'=>$ret_save,
            'fldt'=>$def_database.'nav',
            'fldid'=>'id_nav',
            'id_aplikasi'=>$menu->id_aplikasi,
            'fld'=>enkrip(array(
                'id_par_nav','id_aplikasi','ref','kode','tipe','judul','link','fa','aktif','urut'
            )),
            'urut'=>($menu->urut +1),            
        );
        $ret=form_open(uri2('1,2,').'save_menu','savemenu',$hidden_form)
                
             .div(
                div(
                    div('Dup Form :"'.implode_array($menu).'"<br>'
              .$fr
              .form_label('Kode')
            .form_input('kode',($menu->kode),'class="form-control"')
              .form_label('Referensi')
            .form_dropdown('ref',array(1=>'Bukan','Ya'),($menu->ref),'class="form-control"')
              .form_label('Tipe')
            .form_dropdown('tipe',array(1=>'Kewenangan','Menu'),($menu->tipe),'class="form-control"')
              .form_label('Judul')
            .form_input('judul',($menu->judul),'class="form-control"')
              .form_label('Link')
            .form_input('link',($menu->link),'class="form-control"')
              .form_label('Fa')
            .form_input('fa',($menu->fa),'class="form-control"')
              .form_label('Aktif')
            .form_dropdown('aktif',array('Non Aktif','Aktif'),($menu->aktif),'class="form-control"')
            )
            ."<input type=submit name='' value='Simpan' class='btn btn-success'/>"# form_submit('','Simpan')
            
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            .form_close()
            .$js;
            
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function wenang($id_nav) {
        $ret_save=uri2('1,2,').'tesnes';
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])
                $def_database.=".";
        }
        $ok=$ret=$js=$menu=$role=$awal="";
        $sql_nav="SELECT rr.*,rn.id_role_nav
            FROM ".$def_database."ref_role rr
            LEFT JOIN ".$def_database."ref_role_nav rn ON rn.id_role=rr.id_role AND rn.id_nav=$id_nav
            WHERE rr.id_aplikasi=$id_app 
            order by rr.id_role
            ";
            
        if(($body=$this->db->query($sql_nav))&&($body->num_rows()>0))foreach ($body->result() as $re){
            $menu.=form_label(form_checkbox('hay[]',$re->id_role,$re->id_role_nav)
                ." ".$re->nama_role)."<br>\n";
            $role[]=$re->id_role;
            $awal[]=$re->id_role_nav;
        }
        $ret=form_open(uri2('1,2,').'save_wenang','savewenang',array(
                'id_nav'=>($id_nav),
                'role'=>$role,
                'awwal'=>$awal,
                'ret'=>$ret_save,
                'id_aplikasi'=>$id_app,
                "def_database"=>$def_database,
                ))
                
             .div(
                div(
                    div('Form Kewenangan:"'.'"<br>'
                    .$menu
            ."<input type=submit name='' value='Simpan' class='btn btn-success'/>"#.form_submit('','Simpan')
            
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"'))
            .form_close()
            .$js;
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
    function save_wenang() {
        $p=post(null);extract($p);
        $buat=$hapus="";
        foreach ($role as $x => $v) {
            if(in_array($v,$hay)){
                if(!$awwal[$x])$buat[]=$v;                
            }else{
                if($awwal[$x])$hapus[]=$awwal[$x];
            }
        }
        if(is_array($buat))foreach ($buat as $idrole) 
            $this->db->insert($def_database.'ref_role_nav',array(
                'id_role'=>$idrole,
                'id_nav'=>$id_nav,
            ));
        if(is_array($hapus))foreach ($hapus as $idrolenav) 
            $this->db->where('id_role_nav',$idrolenav)->delete($def_database.'ref_role_nav');
        /*stop(array(
            "b"=>$buat,"h"=>$hapus,'p'=>$p,));
        */
        if(@$ret){
            redirect($ret);
        }else{
            stop(array(
                $p,
                $fld,
                $save
            ));
        }
    }
    
    function lizt_menu() {
        $id_app=0;
        if($po=post('g')){
            $p=dekrip($po,1);
            $ret_save=$p['ret_list'];
            $id_app=$p['id_app'];
            if($def_database=$p['def_database'])
                $def_database.=".";
        }
        
        $sql_nav="SELECT *
            FROM ".$def_database."nav
            WHERE id_aplikasi=$id_app 
            order by urut
            ";
        $menu="\$nav = array(<br>\n";
        $nav=array();
        if(($body=$this->db->query($sql_nav))&&($body->num_rows()>0))foreach ($body->result_array() as $re)
            $nav[$re['id_nav']]=$re;
        foreach ($nav as $x => $n) {
            
            $menu.="array(0,"
                .$n['ref'].","
                ."'".$n['kode']."',"
                .$n['tipe'].","
                ."'".$n['judul']."',"
                .($n['id_par_nav']>0 && !empty($nav[$n['id_par_nav']])?"'".$nav[$n['id_par_nav']]['judul']:"'")."',"
                ."'".$n['link']."',"
                ."'".$n['fa']."'),<br>\n";
        }
        $menu.="),<br>\n";
        $ok=1;
        $ret=
            div(
                div(
                    div('Form Kewenangan:"'.'"<br>'
                    .$menu
                ,'class="col-md-10 col-md-offset-1"')
            ,'class="row"')
            ,'class="edit" style="height: 230px;overflow-y: scroll;"')
            ;
        die(json_encode(array('dta'=>$ret,'hsl'=>$ok)));
    }
    
  }
?>