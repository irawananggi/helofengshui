<?php if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Builder {
    var $ver='0.29';
    var $CI;

    public function __construct () {
        $this->CI =& get_instance();
        $this->CI->load->helper('cmd');
        $this->CI->load->model('general_model');
    }  
    
    /*function init() {
        if(!$this->CI){
            $this->CI =& get_instance();
            $this->CI->load->config('cmd');
            $this->CI->load->helper('cmd');
            $this->CI->load->model('general_model');
        }
    }
    */
    function menu($alamat_db) {
       return json_encode(array(
            'sign' => 1,
            'text' => 'Pilih Menu.<br>'
                .anchor($alamat_db.'/set_db','perbarui DB','class="btn btn-danger"')
                ."&nbsp;"
                .anchor($alamat_db.'/cmp','compare','class="btn btn-success"')
                ,
        ));
    }

    /** v:0.1
    * nama_tabel
    */
    function is_tabel_exist($tname) {
//        $this->init();
        $sql="SHOW TABLES LIKE '$tname'";
        return $this->CI->db->query($sql)->num_rows();
    }    

    /** v:0.1
    * nama_tabel
    */
    function find_constraint($tname){
//        $this->init();
        $tes=$this->CI->db->query("SHOW TABLES LIKE '".$tname."'");
        if($tes->num_rows()>0){
            $hsl=$this->CI->general_model->datagrab(array(
                     'tabel'=>'INFORMATION_SCHEMA.TABLE_CONSTRAINTS',
                     'where'=>array(
                        'TABLE_SCHEMA'=>$this->CI->db->database,
                        'TABLE_NAME'=>$tname,
                        'CONSTRAINT_TYPE'=>'FOREIGN KEY',
                     ),
                     'select'=>'CONSTRAINT_NAME',
            ));
            if($hsl->num_rows()>0)
            {
                return $hsl->result();
            }else{
                return false;
            }
        }else{
                return false;
        }
    }
    
    /** v:0.1
    * nama_tabel
    */
    function show_create($tname) {
//        $this->init();
        $sql="SHOW CREATE TABLE $tname";
        if($hsl=$this->CI->db->query($sql)->result_array()){
            return $hsl[0]['Create Table'];
        }else{
            return false;
        }
    }
    
    /** v:0.1
    * nama_tabel(s)
    */
    function clear_tabel($aname) {
//        $this->init();
        if(is_array($aname)){
            foreach ($aname as $d)
                /*$adaconst=false;*/
                if($cont=$this->find_constraint($d))
                    foreach ($cont as $r) 
                        if($r->CONSTRAINT_NAME)
                            /*cek(array('n:'=>$d,'d'=>$r->CONSTRAINT_NAME,'q'=>"ALTER TABLE $d DROP FOREIGN KEY ".$r->CONSTRAINT_NAME));*/
                            $this->CI->db->query("ALTER TABLE $d DROP FOREIGN KEY ".$r->CONSTRAINT_NAME);
                        /*cek(array('n:'=>$d,'d'=>$cont));*/
            foreach ($aname as $d)
                $this->CI->db->query('DROP TABLE IF EXISTS '.$d.';');

            return true;
        }else{
            return false;
        }
    }

    /** v:0.1
    * nama_tabel, fields, autoinc=0, ret_sql=0
    */
    function create_tabel($tname,$field,$auto_inc=0,$ret_s=0) {
//        $this->init();
        $sql="CREATE TABLE $tname (\n $field \n) ENGINE=InnoDB ".($auto_inc?"AUTO_INCREMENT=$auto_inc":'')." DEFAULT CHARSET=utf8";
        if($ret_s){
            return $sql;      
        }else{
            $this->CI->db->query($sql);
        }
    }
    
    /** v:0.1
    * nama_tabel
    */
    function alter_field($tname,$alters,$tipe=0) {
//        $this->init();
        $par_alters=array();
        switch($tipe){
            case 0:$ope="ADD";break;
            case 1:
                $ope="DROP COLUMN";
                if(is_array($alters)){
                    foreach ($alters as  $al) {
                        $tmp_alters=preg_split('/\s+/',$al, -1, PREG_SPLIT_NO_EMPTY);
                        $par_alters[]=$tmp_alters[0];
                    }
                    $alters=$par_alters;
                }else{
                    $par_alters=preg_split('/\s+/',$alters, -1, PREG_SPLIT_NO_EMPTY);
                    $alters=$par_alters[0];
                }
            break;
            case 2:$ope="CHANGE";
                foreach ($alters as $x => $alt) if(!in_array($x,array('PRIMARY','KEY','UNIQUE')))$par_alters[]="$x ".trim($alt,',');
                $alters=$par_alters;
            break;
        }
        
        $this->CI->db->query("ALTER TABLE $tname  $ope ".implode(", $ope ",$alters)." ;");
        return $this->CI->db->last_query();
    }
    
    function create_tabels($adata) {
        if(is_array($adata)){
            foreach ($adata as $t=>$d)
                $this->create_tabel($t,$d['c'],@$d['r_auto_inc']);/*
                $this->db->query("CREATE TABLE $t ( ".$d['c']." ) ENGINE=InnoDB ".(@$d['r_auto_inc']?"AUTO_INCREMENT=".$d['r_auto_inc']:'')." DEFAULT CHARSET=utf8");*/
            foreach ($adata as $t=>$d)
                if((isset($d['j']))&&($d['j']))
                    $this->alter_field($t,$d['j']);/*
                    $this->db->query("ALTER TABLE $t ".'ADD '.implode(", ADD ",$d['j'])." ;");*/
            return true;
        }else{
            return false;
        }
    }
    
    function proses_db($app='',$stabel='',$code_process='') {
        #$this->init();
        $dtapp = @$app['app'];
        $nav=@$app['nav'];
        $kewenangan=@$app['plus']['kewenangan'];
        $id_app=
        $log=array('ada_update_tabel'=>0,);
        $respon=array('sisip'=>0,'update'=>0);
        //$stabel
       
       #--required tabel:
       if(@$stabel['required']){
           foreach ($stabel['required'] as $tname) 
            if(!$this->is_tabel_exist($tname)){
                $odie = json_encode(array(
                    'sign' => 1,
                    'text' => 'Pemutakhiran Database gagal dilakukan.<br>'.$tname." not found",//.$teks,
                    /*'total' => '0',
                    'done' => 'xx',
                    'kolom' => 'xx',//$db_build['kolom'],
                    'koldone' => 'xx',//$db_build['koldone']*/
                ));
                return $odie;
           }
       }
       
       $ctabel=array();
       #------$dtapp
       if($this->is_tabel_exist('ref_aplikasi')){
           if ($id_aplikasi = $this->CI->general_model->datagrab(array(
                'tabel' => 'ref_aplikasi',
                'where' => array('folder' => $dtapp['folder'])
                ))->row('id_aplikasi')) {
                    #-----------------------jika sudah ada aplkasi
                    $id_app=$id_aplikasi;
                    
                    if(!$code_process){
                        if(@$stabel['clear'])
                            $this->clear_tabel($stabel['clear']);
                            
                        #--- cek tabel yang kurang
                        
                        
                        if($respon_docompare=$this->do_compare_tables($stabel['clear_prefix'],$this->sort_ctabel( $stabel['create']),@$stabel['exclude_clear_prefix'])){
                            $cmp_db=$respon_docompare['cmp_db'];
                            $respon=array_merge($respon,$respon_docompare['respon']);
                            
                        }
                        
                        if((@$stabel['insert'])&&(count($stabel['insert'])>0))
                            foreach ($stabel['insert'] as $t=>$d)
                                if(in_array($t,$cmp_db['tambah'])){
                                    $this->CI->db->query("INSERT INTO $t $d ;");
                                    $respon['sisip']++;
                        }
                    }
                    
                    $this->set_nav(array(
                        'id_aplikasi'=>$id_app,
                        'nav'=>$nav,
                        'kewenangan'=>$kewenangan,
                    ));

                    /*foreach ($this->sort_ctabel( $stabel['create'] ) as $tname => $ct) {
                        if(!$this->update_field_table($tname,$ct['c'])){
                            #----- jika belum buat  
                            $this->create_tabel($tname,$ct['c']);
                            if((isset($ct['j']))&&($ct['j']))$this->alter_field($tname,$ct['j']);
                        }
                    }*/
                    #--e- cek tabel yang kurang
                    
                    #---cek nav
                    #----- jika belum ada buat
                    #---cek kewenangan
                    #----- jika belum ada buat
                    
                    //update
                    //clear nav
                    //-----------------------------------------
                    
                    //-----------------------------------------/
                    #-update ref_aplikasi
                    if(!empty($stabel['force_app']))
                        $this->CI->db->where(array('id_aplikasi'=>$id_app))
                            ->update('ref_aplikasi',$dtapp);
                    
            }else{
                
                $this->force_db(compact('dtapp', 'nav', 'kewenangan', 'stabel'));
                $respon['tambah']=count(@$stabel['create']);
                $respon['hapus']=($stabel['clear']?count(@$stabel['clear']):0);
                $respon['sisip']=count(@$stabel['insert']);
            }
       }else{
           #create ref_aplikasi
           $ctabel=load_cnt(uri(1),'Constcreate','ctabel');
       }
       $itabel =array();
       $this->create_tabels($ctabel);    
       foreach ($itabel as $t=>$d)
                        if(in_array($t,$cmp_db['tambah'])){
                            $this->CI->db->query("INSERT INTO $t $d ;");
       }
        
        
        $odie = json_encode(array(
            'sign' => 1,
            'text' => 'Pemutakhiran Database berhasil dilakukan.<br>'//.$teks,
            .' <br>Clear : '. @$respon['hapus']
            .' <br>Create : '.@$respon['tambah']
            .' <br>Update : '.@$respon['update']
            .' <br>Insert : '.$respon['sisip'],//$db_build['total'],
            /*'done' => count(@$stabel['clear'])." clear, ".count($stabel['create']).' create, '.count(@$stabel['insert']).' insert',//$db_build['done'],
            'kolom' => 'xx',//$db_build['kolom'],
            'koldone' => 'xx',//$db_build['koldone']*/
        ));
        
        return $odie;
        
    }    
    
    /**
    *  dtapp, nav, kewenangan, stabel
    */
    function force_db($param) {
//        $this->init();
        extract($param);
        #---------------------------jika aplkasi baru:
            #---buar ref_aplikasi
            $dtapp['aktif'] =1;
            $dtapp['urut'] = 0;
            if(
                $u = $this->CI->general_model->datagrab(array(
                    'tabel' => 'ref_aplikasi',
                    'select' => 'max(urut) +1 as urutan,max(id_aplikasi) +1 as id_aplikasian'
                ))->row())
                    $dtapp['id_aplikasi'] = $u->id_aplikasian;
                    $dtapp['urut'] = $u->urutan;
            
            //get id
            $this->CI->db->where(array('kode_aplikasi'=>$dtapp['kode_aplikasi']))->delete('ref_aplikasi');
            $this->CI->db->insert('ref_aplikasi',$dtapp);
            $id_app=$this->CI->db->insert_id();
            #--e-buar ref_aplikasi
            
            #--- hapus tabel prefix
            #--clear clear_table
            if(@$stabel['clear_prefix'])
                $this->clear_tabel_prefix($stabel['clear_prefix'],@$stabel['exclude_clear_prefix']);

            if(@$stabel['clear'])
                $this->clear_tabel($stabel['clear']);
            #-e-clear clear_table

            #--- buat tabel
            #--create tabel
            if($stabel['create'])$this->create_tabels($stabel['create']);
            /*            if(isset($d['j']))
            $this->db->query("ALTER TABLE $t ".'ADD '.implode(", ADD ",$d['j'])." ;");*/

            foreach ($stabel['insert'] as $t=>$d)
                $this->CI->db->query("INSERT INTO $t $d ;");

            #-e-create tabel
            
            #--- set nav
            return $this->set_nav(array(
                'id_aplikasi'=>$id_app,
                'nav'=>$nav,
                'kewenangan'=>$kewenangan,
            ));
            #--E-set nav
            
            #--- set kewenangan
            
            
    }
    
    /*
    *  array(
    *   id_aplikasi
    *   nav
    *   kewenangan
    * )
    */
    function set_nav($dt) {
//        $this->init();
        if(!is_array($dt)) return false;
        if(!$id_app=$dt['id_aplikasi'])return false;
        if(!$nav=$dt['nav'])return false;
        
        #--- jika ada aplikasi 
        if($tes_role=$this->CI->general_model->datagrab(array(
                    'tabel'=>'ref_role',
                    'where'=>array('id_aplikasi'=>$id_app),
                   ))){
                       $clear_role=array();
                       foreach ($tes_role->result() as $r)$clear_role[]=$r->id_role;
                       if(count($clear_role)>0)$this->CI->db->where_in('id_role',$clear_role)->delete('ref_role_nav');
                   }
                $this->CI->db->where(array('id_aplikasi'=>$id_app))->delete('ref_role');
                $this->CI->db->where(array('id_aplikasi'=>$id_app))->delete('nav');
        $my_nav = array();
        $no = 0;

        foreach($nav as $dat) {

            $or_null = empty($dat[6])?" OR link IS NULL":null;

            $where_kode = array(
                'id_aplikasi' => $id_app,
                'ref' => $dat[1],
                'judul' => $dat[4]);

            if(@$dat[6])$where_kode["(link = '".$dat[6]."'".$or_null.")"]=null;
            if (!empty($dat[2])) $where_kode['kode'] = $dat[2];

            $jml = $this->CI->general_model->datagrab(array(
                'tabel' => 'nav', 
                'where' => $where_kode,
                'select' => 'count(*) as jml'))->row('jml');
            /*cek($jml);
            cek($this->db->last_query());*/
            if ($jml == 0) { 
                $id_par = null;
                if (!empty($dat[5])) 
                    $id_par = $this->CI->general_model->datagrab(array(
                        'tabel' => 'nav', 
                        'where' => array(
                            'id_aplikasi' => $id_app,
                            'judul' => $dat[5],
                            'ref' => $dat[1]),
                        'select' => 'id_nav'))->row('id_nav');
                
                $max_num = $this->CI->general_model->datagrab(array(
                    'tabel' => 'nav','where' => array('id_aplikasi' => $id_app),
                    'select' => 'MAX(urut) as max_num'
                ))->row('max_num');
                
                $simpan = array(
                    'id_aplikasi' => $id_app,
                    'judul' => $dat[4],
                    'tipe' => $dat[3],
                    'ref' => $dat[1],
                    'kode' => $dat[2],
                    'urut' => $max_num+1,
                    /*'separator' => 0,*/
                    'aktif' => 1
                );
                if(@$dat[6])$simpan['link'] = $dat[6];
                if(@$dat[7])$simpan['fa'] = $dat[7];
                                
                if (!empty($id_par)) $simpan['id_par_nav'] = $id_par;
         
                $my_nav[]=$this->CI->general_model->save_data('nav',$simpan);
                $no+=1;
                //$nav_plus[] = $no.'. Menu <b>'.$dat[4].'</b> ditambahkan ...';
                    
            }
        }
        //kewenangan
        if($kewenangan=@$dt['kewenangan'])
        /*if(isset($kewenangan))*/{
            if(!$id_role=$this->CI->general_model->datagrab(array(
                  'tabel'=>'ref_role',
                  'where'=>array('nama_role'=>$kewenangan,'id_aplikasi'=>$id_app),
                 ))->row('id_role'))
                    if($this->CI->db->insert('ref_role',array('id_aplikasi'=>$id_app,'nama_role'=>$kewenangan)))
                        $id_role=$this->CI->db->insert_id();
                        
                     
            $savemy_nav=array();
            foreach ($my_nav as $r)
                $savemy_nav[]=array(
                    'id_role'=>$id_role,
                    'id_nav'=>$r,
                );
            if(count($savemy_nav)>0)
             $this->CI->db->insert_batch('ref_role_nav',$savemy_nav);

            #----------------------------------------role pegawai
            
            if($id_peg=sesi('id_pegawai'))
                $this->CI->db->insert('pegawai_role',array('id_pegawai'=>$id_peg,'id_role'=>$id_role));
        }
        return true;
    }
    
    /** v:0.2
    * nama_tabel(s), nama_ex_tabel(s)
    */
    function clear_tabel_prefix($prefix='',$exclude_prefix="") {
        if($aprefix=$this->find_prefix_tabel($prefix,$exclude_prefix)){
            return $this->clear_tabel($aprefix);
        }else{
            return false;
        }        
    }
    
    /** v:0.4
    * nama_tabel(s), nama_ex_tabel(s)
    */
    function find_prefix_tabel($prefix,$exclude_prefix="") {
//        $this->init();
        $wher= $whernot=$aprefix=array();
        $mydb=$this->CI->db->database;
        $tablein="Tables_in_$mydb";
        if(!is_array($prefix))$prefix=(array)$prefix;
        
        if($exclude_prefix){
            if(!is_array($exclude_prefix))$exclude_prefix=(array)$exclude_prefix;
            foreach ($exclude_prefix as $pre) $whernot[]="`Tables_in_$mydb` LIKE '$pre%'";            
        }
        foreach ($prefix as $pre) $wher[]="`Tables_in_$mydb` LIKE '$pre%'";
        
        $sql="SHOW TABLES FROM `$mydb` WHERE (".implode(' OR ',$wher).') '.(count($whernot)>0?" AND NOT ".implode(' AND NOT ',$whernot):"");
        #stop($sql);
        if(($prefix)&&($tes=$this->CI->db->query($sql))&&($tes->num_rows()>0))
            foreach ($tes->result() as $r) 
                $aprefix[]=$r->$tablein;
                
        
        return (count($aprefix)>0?$aprefix:false);
    }
    
    /**
    *  sort tabel with no constraint 
    */
    function sort_ctabel($d) {
        $ret_t=array();
        foreach ($d as $tname => $t) if(!@$t['j'])$ret_t[$tname]=$t;
        foreach ($d as $tname => $t) if(!@$ret_t[$tname])$ret_t[$tname]=$t;
        return $ret_t;
    }
    
    function sql_fields($dt,$include_comma=0) {
        /*$h1=explode(",\n",$dt);
        foreach ($h1 as $x => $r) $h1[$x]=trim($r);
        if(substr($h1[0],0,6)=='CREATE') array_shift($h1);
        if(substr(end($h1),0,4)==') EN')array_pop($h1);*/
        /*~(\s*[,][\n]\s*|\s*\(\n\s*|\s*\n\) *\s*)~*/
        $h1=preg_split("/(\s*\n\s*|\s*\(\n\s*|\s*\n\)\s*)/",
            ($include_comma?$dt:
            preg_replace(array(
                '/^\s*/m',
                '~([,]*)(?=\r\n)~m',
                "~(,+$)~m"
                ),'',$dt))
        , -1, PREG_SPLIT_NO_EMPTY);
        #$h1=preg_split('~(\s*(,\n)\s*|\s*\(\n\s*|\s*\n\) *\s*)~', $dt, -1, PREG_SPLIT_NO_EMPTY);#explode("\n",$dt);
        if(substr($h1[0],0,6)=='CREATE') array_shift($h1);
        if(substr(end($h1),0,8)==') ENGINE')array_pop($h1);
        return $h1;
    }
        
    function update_field_table($ntabel,$sql0) {
        if($cmp=$this->cmp_field($ntabel,$sql0)){
            extract($cmp);
            if(count($tambah)>0)
                $this->alter_field($ntabel,$tambah);
                
            if(count($hapus)>0){
                $this->alter_field($ntabel,array_keys($hapus),1);
            }
            
            if(count($update)>0){
                $this->alter_field($ntabel,$update,2);
            }
            //cek(array($tambah,$hapus,$haps,$t));
            return array('tambah'=>count($tambah),'hapus'=>count($hapus));
        }else{
            return false;
        }
        
    }
    
    function cmp_field($ntabel,$sql0) {
        $tambah_fn=$hapus_fn=$update=array();
        if($this->is_tabel_exist($ntabel)){
            $t=array(
                1=> $this->sql_fields( $sql0),
                2=> $this->sql_fields( $this->show_create($ntabel)),
            );
            $tambah=array_diff($t[1],$t[2]);
            foreach ($tambah as $x => $tmb) { 
                $tm0=preg_split('/ /',$tmb,2);
                $tambah[$tm0[0]]=$tmb;
                unset($tambah[$x]);
                $tambah_fn[]=$tm0[0];
            }
            $hapus=array_diff($t[2],$t[1]);
            foreach ($hapus as $x => $hap) { 
                $tm0=preg_split('/ /',$hap,2);
                $hapus[$tm0[0]]=$hap;
                unset($hapus[$x]);
                $hapus_fn[]=$tm0[0];
            }
            if($sama_fn=array_intersect($tambah_fn,$hapus_fn)){
                foreach ($sama_fn as $x => $sam) {
                   $update[$sam]=$tambah[$sam];
                }
            }
            
            $tambah=array_diff_key($tambah,array_flip( $sama_fn));
            $hapus=array_diff_key($hapus,array_flip( $hapus_fn));
            return compact('tambah','hapus','update');
        }else{
            return false;
        }
    }
    
    function list_fields($ntabel) {
//        $this->init();
        return $this->CI->db->query("SHOW FULL FIELDS FROM $ntabel");
    }
    
    /** v:0.3
    * nama_tabel(s), daftar_crt_tabel, nama_ex_tabel(s)
    */
    function compare_create_table($prefix,$stable_create,$exclude_prefix="") {
        if($cmp=$this->find_prefix_tabel($prefix,$exclude_prefix)){
            $tbl=array_keys($stable_create);
            $tambah=array_diff($cmp,$tbl);
            $hapus=array_diff($tbl,$cmp);
            $sama=array_intersect($cmp,$tbl);
            if(count($sama)>0)
            foreach ($sama as $sa) {
               $sama2[]=$sa;
            }
            /*foreach ($cmp as $x => $vd) {
                $tes=load_controller('dm','builder','find_prefix_tabel','present')
            }
            cek(array('tambah'=>$tambah,'hapus'=>$hapus));
            echo "<pre>";*/
            
            $daftar="\n";
            foreach ($cmp as $r) {
                $j="";
                if($sqlx=$this->sql_fields($this->show_create($r))){
                    $en=end($sqlx);
                    if(substr($en,0,10)=="CONSTRAINT"){
                        $j=array_pop($sqlx);
                        $jm=count($sqlx)-1;
                        $sqlx[$jm]=substr($sqlx[$jm],0,-1);
                        $j=strstr($j,'FOREIGN');
                    }
                    $daftar.= "'$r'=>array(\n"
                ."    'c'=>\n"
                ."      \"".implode(",\n      ",$sqlx)."\","
                .(@$j?
                    "\n    'j'=>array(\n"
                    ."      '".$j."',"
                    ."\n    ),":"")
                ."\n),\n";
                }
            }
            /*echo "</pre>";*/
            return compact('tambah','hapus','daftar');
        }
        
    }

    function list_fld($ntable) {
//       $this->init();
       return $this->CI->db->query("DESCRIBE ".$ntable);
    }
    
    #-------------------------------------------- develop
    
    /**
    *  array(
    *   id_aplikasi
    *   nav
    *   kewenangan
    * )
    */
    function set_nav2($dt) {
//        $this->init();
        if(!is_array($dt)) return false;
        if(!$id_app=$dt['id_aplikasi'])return false;
        if(!$nav=$dt['nav'])return false;
        
        $save_role=
        $save_nav=
            array();
            
        #-----------------------------role
        if(($tes_role=$this->CI->general_model->datagrab(array(
                    'tabel'=>'ref_role',
                    'where'=>array('id_aplikasi'=>$id_app),
                   )))&&($tes_role->num_rows()>0))
            foreach ($tes_role->result() as $r) 
                $save_role[]=$r->id_role;
        $ada_role=count($save_role)>0;
        
        #-----------------------------nav
        if(($tes_nav=$this->CI->general_model->datagrab(array(
                    'tabel'=>'nav',
                    'where'=>array('id_aplikasi'=>$id_app),
                   )))&&($tes_nav->num_rows()>0))
            foreach ($tes_nav->result() as $r)
                $save_nav[]=$r->id_nav;
                   
        $my_nav = array();
        $no = 0;

        foreach($nav as $dat) {

            $or_null = empty($dat[6])?" OR link IS NULL":null;

            $where_kode = array(
                'id_aplikasi' => $id_app,
                'ref' => $dat[1],
                'judul' => $dat[4]);

            if(@$dat[6])$where_kode["(link = '".$dat[6]."'".$or_null.")"]=null;
            if (!empty($dat[2])) $where_kode['kode'] = $dat[2];

            $jml = $this->CI->general_model->datagrab(array(
                'tabel' => 'nav', 
                'where' => $where_kode,
                'select' => 'count(*) as jml'))->row('jml');
            /*cek($jml);
            cek($this->db->last_query());*/
            if ($jml == 0) { 
                $id_par = null;
                if (!empty($dat[5])) {
                    
                    $cek_par = $this->CI->general_model->datagrab(array(
                        'tabel' => 'nav', 
                        'where' => array(
                            'id_aplikasi' => $id_app,
                            'judul' => $dat[5],
                            'ref' => $dat[1]),
                        'select' => 'id_nav'))->row('id_nav');
                    $id_par = $cek_par;
                }
                
                $max_num = $this->CI->general_model->datagrab(array(
                    'tabel' => 'nav','where' => array('id_aplikasi' => $id_app),
                    'select' => 'MAX(urut) as max_num'
                ))->row('max_num');
                $simpan = array(
                    'id_aplikasi' => $id_app,
                    'judul' => $dat[4],
                    'tipe' => $dat[3],
                    'ref' => $dat[1],
                    'kode' => $dat[2],
                    'urut' => $max_num+1,
                    /*'separator' => 0,*/
                    'aktif' => 1
                );
                if(@$dat[6])$simpan['link'] = $dat[6];
                if(@$dat[7])$simpan['fa'] = $dat[7];
                                
                if (!empty($id_par)) $simpan['id_par_nav'] = $id_par;
         
                $my_nav[]=$this->CI->general_model->save_data('nav',$simpan);
                $no+=1;
                //$nav_plus[] = $no.'. Menu <b>'.$dat[4].'</b> ditambahkan ...';
                    
            }
        }
        //kewenangan
        if($kwenangan=$dt['kewenangan'])
        if(isset($kwenangan)){
            if(!$id_nav=$this->CI->general_model->datagrab(array(
                  'tabel'=>'ref_role',
                  'where'=>array('nama_role'=>$kwenangan,'id_aplikasi'=>$id_app),
                 ))->row('id_role'))
                    if($this->CI->db->insert('ref_role',array('id_aplikasi'=>$id_app,'nama_role'=>$kwenangan)))
                        $id_nav=$this->CI->db->insert_id();
                        
                     
            $savemy_nav=array();
            foreach ($my_nav as $r)
                $savemy_nav[]=array(
                    'id_role'=>$id_nav,
                    'id_nav'=>$r,
                );
            if(count($savemy_nav)>0)
             $this->CI->db->insert_batch('ref_role_nav',$savemy_nav);
        }
        return true;
    }
    
    function findz($start,$end,$from) {
        $pattern = sprintf(
            '/%s(.+?)%s/ims',
            preg_quote($start, '/'), preg_quote($end, '/')
        );

        if (preg_match($pattern, $from, $matches)) {
            list(, $match) = $matches;
            return $match;
        }else{
            return false;
        }
        
    }
    
    function dnav2nav2($dnav,$id_app) {
      $nav=$dn=array();
      foreach ($dnav as $r) $dn[$r[0]]=$r;
      foreach ($dn as $x => $r) {
          $nv=array(
            $r[5],
            $r[3],
            ($r[4]?$r[4]:$r[6]),
            (@$dn[$r[1]]?$dn[$r[1]][6]:Null),
            $r[7],
          );
          $nav[]=$nv;
          //$nv=array();
      }
      /*stop(array(
        'dnav'=>$dnav,
        'nav'=>$nav,
        ));*/
      return $nav;
    }  

    #-------------------------------------------e- develop
    function ceksesi() {
        return sesi('id_pegawai');
    }
    
    /** v:0.2
    * nama_tabel(s), daftar_crt_tabel, nama_ex_tabel(s)
    */
    function comparetables($prefix,$stable_create,$exclude_prefix="") {
        $tbldb=array();
        if($tbl__=$this->find_prefix_tabel($prefix,$exclude_prefix))
            $tbldb=$tbl__;
        
            $tbl=array_keys($stable_create);
            $hapus=array_diff($tbldb,$tbl);
            $tambah=array_diff($tbl,$tbldb);
            $sama=array_intersect($tbldb,$tbl);
            $bedasama=array();
            foreach ($sama as $sa) 
               if(($cmpf=$this->cmp_field($sa,$stable_create[$sa]['c']))&&(count($cmpf['tambah'])+count($cmpf['hapus'])+count($cmpf['update'])>0))
                $bedasama[$sa]=$cmpf;
            return compact('tambah','hapus','sama','bedasama');
        
        
    }
    
    function do_compare_tables($prefixs,$screate,$exclude_prefix="") {
        $respon=array('tambah'=>0,'hapus'=>0,'update'=>0);
        $cmp_db=$this->comparetables($prefixs,$screate,$exclude_prefix);
        #stop(array($cmp_db));
        if(count($cmp_db['tambah'])>0){
            $ctabel=array();
            foreach ($cmp_db['tambah'] as $c1) 
                $ctabel[$c1]=$screate[$c1];
            $this->create_tabels($ctabel);
            $respon['tambah']=count($cmp_db['tambah']);
        }
        
        if(count($cmp_db['hapus'])>0){
            #stop(array($exclude_prefix, $cmp_db['hapus']) );
            $this->clear_tabel($cmp_db['hapus'] );
            $respon['hapus']=count($cmp_db['hapus']);
        }
            
        if(count($cmp_db['bedasama'])>0) {
            foreach ($cmp_db['bedasama'] as $bsname => $bs) {
                if(count($bs['hapus'])>0)
                    $this->alter_field($bsname,$bs['hapus'],1);
                if(count($bs['tambah'])>0)
                    $this->alter_field($bsname,$bs['tambah']);
                if(count($bs['update'])>0)
                    $this->alter_field($bsname,$bs['update'],2);
            }
            $respon['update']=count($cmp_db['bedasama']);
        }   
            
        return compact('cmp_db','respon');
    }
    
    /** v:0.2
    * nama_tabel(s), nama_ex_tabel(s)
    */
    function daftar_tabel($prefix,$exclude_prefix=""){
           if($tbldb=$this->find_prefix_tabel($prefix,$exclude_prefix)){
            $daftar="\n";
            foreach ($tbldb as $r) {
                $j="";
                if($sqlx=$this->sql_fields($this->show_create($r),1)){
                    $en=end($sqlx);
                    if(substr($en,0,10)=="CONSTRAINT"){
                        $j=array_pop($sqlx);
                        $jm=count($sqlx)-1;
                        $sqlx[$jm]=substr($sqlx[$jm],0,-1);
                        $j=strstr($j,'FOREIGN');
                    }
                    $daftar.= "'$r'=>array(\n"
                ."    'c'=>\n"
                ."      \"".implode("\n      ",$sqlx)."\","
                .(@$j?
                    "\n    'j'=>array(\n"
                    ."      '".$j."',"
                    ."\n    ),":"")
                ."\n),\n";
                }
            }
            /*echo "</pre>";*/
            return $daftar;
        }
    }
    
    /** v:0.2
    * nama_tabel(s), stabel->create, nama_ex_tabel(s)
    */
    function daftar_cmp($prefix,$screate,$exclude_prefix="") {
        return array_merge(
            array(
                'daftar'=>$this->daftar_tabel($prefix,$exclude_prefix),
            ),
            $this->comparetables($prefix,$screate,$exclude_prefix)
            );
    }
    
    /**
    * hapus navigasi yg tidak ber aplikasi
    */
    function clean_nav() {
//        $this->init();
        return $this->CI->db->query("DELETE nav
                    FROM nav
                    LEFT JOIN ref_aplikasi ON ref_aplikasi.id_aplikasi=nav.id_aplikasi
                    WHERE ISNULL(ref_aplikasi.id_aplikasi)");
    }
    
    function update_temporary() {
//        $this->init();
        $sql="ALTER TABLE nav CHANGE kode kode VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci NULL;";
        return $this->CI->db->query($sql);
    }
    
    /** v:0.1
    * taampil isi tabel
    */
    function show_insert($ntabel,$filter="") {
//        $this->init();
        $fld=$isi=array();
        if(is_array($filter))$fld=$filter;
        if(($dt=$this->CI->db->limit(100)->get($ntabel))&&($dt->num_rows()>0))
            foreach ($dt->result_array() as $x => $r) {
                if(count($fld)==0)$fld=array_keys($r);
                $isi_=array();
                if(count($fld)>0) foreach ($fld as $f) $isi_[]=htmlentities($r[$f]);
                $isi[]="('".implode("','",$isi_)."')";
            }
        if(count($isi)>0){
            return "(`".implode("`,`",$fld)."`) values ".implode(",\n",$isi);   
        }else{
            return false;
        }
    }
    
    function num_rows_table($ntable) {
        return $this->CI->db->query("select count(*)jm from `$ntable`")->row('jm');
    }
    
    function list_insert($ntabel="",$fields="",$return=0) {
        $ret="";
        if(!$ntabel){
            $ret= $return
                ?'<div class="row"><div class="col-lg-6">'
                :"<div><div>";
            if(($listtabel=$this->find_prefix_tabel('%'))&&(is_array($listtabel)))
                foreach ($listtabel as $x => $ls) 
                    $ret.= ($x+1).': '
                        .anchor(uri_string().'/'.$ls,$ls."(".($this->num_rows_table($ls)).")"." >>",'target="ifr"')." | "
                        .anchor(uri2('1,2').'/list_create/'.$ls,"Create"." >>",'target="ifr"')." | "
                        .anchor(uri2('1,2').'/daftar_tabel/'.$ls,"db"." >>",'target="ifr"')
                        ."<br>";
            $ret.= "</div>";
            $ret.= $return
                ?'<div class="col-lg-6">'
                :'<div style="
                    display: block;
                    width: 60%;
                    height: 90%;
                    padding: 0;
                    overflow: hidden;
                    position: fixed;
                    top:0;
                    right:0;
                    ">';
            $ret.=  '<iframe name="ifr" style="position: fixed;width: 39%;height: 80%;"></iframe>';
            $ret.= '</div></div>';
            //style="float: right;position: fixed;top: 2px;right: 2px; resize: both;overflow: auto;"
        }elseif($ntabel){
            $tanda="~";
            if($fields)$fields=explode($tanda,$fields);
            $daftar=$this->show_insert($ntabel,$fields);
            $a1=preg_split('/ /',$daftar);
            $a2=preg_split('/[,()`]/',$a1[0],-1,1);
            $ret.= "<pre>'".$ntabel."'=>\"".$daftar."\",</pre>";
            #cek(implode($tanda,$a2));
            $ret.= anchor(uri_string().'/'.implode($tanda,$a2),'->');
        }
        if($return){
            return $ret;
        }else{
            echo $ret;
        }
    }
    function list_create($ntabel="") {
        echo "<pre>'";
        
        if(!$ntabel){
            if(($listtabel=$this->find_prefix_tabel('%'))&&(is_array($listtabel)))
                foreach ($listtabel as $x => $ls) echo anchor(uri_string().'/'.$ls,$ls." >>")."<br>";
        }elseif($ntabel){
            echo $this->show_create($ntabel);
        }
        echo "</pre>'";
    }
    
    /**
    * nav khusus
    */
    function dmnav($url="") {
        $nav=array(
            'dm/Home/daftar'=>'Dash',
            'dm/Navi/nav'=>'Nav',
            'dm/Navikan/list_nav'=>'Ngurutz',
            'dm/Def/list_table'=>'Table',
            'dm/Ref/data/kewenangan'=>'Kewenangan',
            'dm/Ref/data/operator'=>'Operator',
            'dm/Ref/data/ref_app'=>'App',
            'dm/Ref/cekinde'=>'cek inde',
            'dm/Ref/backup_db'=>'BackUpDb',
            'dm/Tes/build'=>'Build',
            'dm/Ref/data/parameter'=>'Param',
            'dm/Ref/icon'=>'Icon',
        );
        $menu_nav="";
        foreach ($nav as $x => $v) 
            $menu_nav.=act_nav($x,$v,$url);
        return $menu_nav;
    }
    
    function backup_db($param="") {
        #ini_set('memory_limit', '-1');
        $this->CI->load->dbutil();
        $myfile='Bckdb_'.$this->CI->db->database.'_'.date('Ymd_His').'.gz';

        // Backup your entire database and assign it to a variable
        if(is_array($param)){
            $backup =& $this->CI->dbutil->backup($param);
        }else{
            $backup =& $this->CI->dbutil->backup();
        }        

        // Load the file helper and write the file to your server
        $this->CI->load->helper('file');
        write_file('/path/to/'.$myfile, $backup);

        // Load the download helper and send the file to your desktop
        $this->CI->load->helper('download');
        force_download($myfile, $backup); ;
    }
    
    #find field =>SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='id_par_jabatan'
}

?>