<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  class Tes extends CI_Controller {
    var $debug=0;
    var $ntabel='tmp_table';
    var $fid='id_table';
    public function __construct()
    {
        parent::__construct();
        #$this->load->model('_');
        $this->load->helper('cmd');
        $this->load->library('Defa');
    }
    
    /**
    * initial database
    */
    function db() {
          $tabel_db=array(
            $this->ntabel=>array(
                'c'=>
                  "`id_table` int(11) NOT NULL AUTO_INCREMENT,
                  `nama` varchar(50) NOT NULL,
                  `dt` text,
                  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id_table`)",
            ),
          );
          
          $cmp_db=load_cnt('dm','Builder','do_compare_tables',array_keys($tabel_db),$tabel_db);
          
          cek($cmp_db);
    }
    
    function index(){
        show_404();
    }
    
    #pilihan nama coding
    function ac_tabel() {
        $q = $this->input->post('q');
        $q1 = $this->input->post('page');

        $tabelnya=array(
                'tabel' => $this->ntabel,
                'where' => array("(UPPER(nama) LIKE UPPER('%$q%')"=> null),
                'select' => 'id_table id, nama as data',
                'limit' => 10, 'offset' => 0
        );
        
        $res = $this->general_model->datagrab($tabelnya);
        die(json_encode($res->result()));
    }
    
    #form tag
    function bform($id,$tag) {
        $idv= $this->load_tb($id);
        echo div(
            form_label($tag)."&nbsp;"
            .form_input($tag,(!empty($idv[$tag])?$idv[$tag]:""),'class="form-control"')
        );
    }
    
    #simpan field
    function simpan_fr($id) {
        $p=post();
        $dt=$this->load_tb($id);
        foreach ($p as $px => $pv) $dt[$px]=$pv;
        #cek(array($p,$dt));
        $en=enkrip($dt);
        $de=dekrip($en,1);
        #stop(array($en.$de));
        $this->db->where(array($this->fid=>$id))
            ->update($this->ntabel,array('dt'=> enkrip($dt,1)));
        redirect('dm/tes/build/'.$id);
    }
    
    #load isi dt 
    function ld_tb($id) {
        $param['tabel']=$this->ntabel;
        if($id)$param['where']=array($this->fid=>$id);
        if(($dt=$this->general_model->datagrab($param))
            &&($dt->num_rows()>0)){
                if($id){
                    $rdt=$dt->row();    
                    if($rdt->dt){
                        return $rdt->dt;
                    }else{
                        return false;
                    }
                }else{
                    return $dt->result();
                }
            
        }else{
            return false;
        }
    }
    
    #load dt tabel
    function load_tb($id=0) {
        if($rdt=$this->ld_tb($id)){
            return dekrip($rdt,1,1);
        }else{
            return array();
        }
    }
    
    #main
    function build($id="") {
        $tabel=div('','id="isi"');
        $pilihan=$this->general_model->combo_box(array(
                    'tabel'=>$this->ntabel,
                    'key'=>'id_table',
                    'val'=>array('nama',),
                    'pilih'=>'- Pilih Nama -',
                ));
        $tombol=div('Nama : ','class="input-group-btn"')
                .form_dropdown(
                        'nama',$pilihan
                        ,$id
                        ,'class="form-control" id="pilih_nama"'
                );
                
        $tombol_design=div(
                anchor('','<i class="fa fa-slideshare"></i>'
                ,'class="btn btn-warning" id="desing"')    
            ,'class="input-group-btn"');
            
        $tombol_show=div(
                anchor('','<i class="fa fa-home"></i>'
                ,'class="btn btn-primary" id="sow"')    
            ,'class="input-group-btn"');
            
        $tombol_cek=div(
                anchor('','<i class="fa fa-copyright"></i>'
                ,'class="btn btn-default" id="cex"')    
            ,'class="input-group-btn"');
            
        $tombol_edit=div(
                anchor('','<i class="fa fa-pencil"></i>'
                ,'class="btn btn-warning" id="edt"')    
            ,'class="input-group-btn"');
            
        $tombol_tambah=
            div(
            anchor('','<i class="fa fa-plus"></i>'
                ,'class="btn btn-success btn-act"  act="'
                    .site_url(uri2('1,2,').'save_db').'"
                    msg="
                    <br>Nama
                    <br><input name=nama type=text style=\'width: 100%;\'/>
                    <br>dt
                    <br><input name=dt type=text style=\'width: 100%;\'/>
                    " bt_done=BUAT 
                   '
            )
            ,'class="input-group-btn"');
            
        $tombol_hapus=
            div(
            anchor('','<i class="fa fa-trash"></i>'
                ,'class="btn btn-danger btn-act"  act="'
                    .site_url(uri2('1,2,').'del_db').'"
                    msg="hapus ?
                        <input type=hidden name=id value=999 />
                        <input type=hidden name=ret value=\''.uri_string().'\' />" 
                   t_text="Hapus"'
            )
            ,'class="input-group-btn"');
            
        /*$script=$this->script_select2(
                '#pilih_nama'
                ,uri2('1,2,').'ac_tabel'
                ,"console.log( e.currentTarget.value);"
                );*/
        $data['no_role']=1;
        $data['include_script']="";
        $data['tabel']=$tabel;
        $data['tombol']=
            div(
                $tombol
                .$tombol_design
                .$tombol_show
                .$tombol_cek
                .$tombol_edit
                .$tombol_tambah
                .$tombol_hapus
            ,'class="input-group margin"');
            
        $data['out_script']="
            function ayax(id,nguri) {
                var uri=[
                    '".site_url('dm/tes/ref')."'
                    ,'".site_url('dm/tes/desg')."'
                    ,'".site_url('dm/tes/cek')."'
                    ,'".site_url('dm/tes/edt')."'
                    ];
                $.ajax({
                  url: uri[nguri]+'/'+id,
                  type: 'POST',
                  cache: false,
                  success: function(msg) {
                      $('#isi').html(msg);
                  },
                  error:function(error){
                    console.log(error);
                  }
                });
                return false;
            }";
            
        $data['script']="
            $('.btn-act').click(function() {
                   var ac = $(this).attr('act');
                   var msg = $(this).attr('msg');
                   var ulng = $(this).attr('ulang');
                   var icn = $(this).attr('t_icn');
                   var tl = $(this).attr('t_text');
                   var bt_done = $(this).attr('bt_done');
                   var icn2=(icn?icn:'fa-bookmark-o');
                   var btn_ver = '<i class=\"fa '+icn2+' fa-btn\"></i> '+(tl?tl:'Konfirmasi');
                   bt_done =(bt_done ?bt_done :btn_ver);
                   $('.form-title').html(btn_ver);
                   $('.form-delete-msg').html(msg);
                   if (ac) $('#form_modal_dialog').attr('action',ac);
                   if(tl=='Hapus')$('#modal-delete input[name=id]').val($('#pilih_nama').val());
                   console.log('ul '+$('#modal-delete input[name=id]').val()+' '+$('#pilih_nama').val());
                   if(((ulng)&&(ulng==1))||(!ulng)){
                       $('.form-delete-url').children().html(bt_done).show();
                       $('.form-delete-url').children().prop('type', 'submitform');
                   }else{
                        $('.form-delete-url').children().hide();
                   }
                   $('#modal-delete').modal('show');
                   return false;
                });
            $('#pilih_nama').change(function() {
                 ayax($(this).val(),0);
            });
            $('#desing').click(function() {
                ayax($('#pilih_nama').val(),1);return false;
            });
            $('#sow').click(function() {
                ayax($('#pilih_nama').val(),0);return false;
            });
            $('#cex').click(function() {
                ayax($('#pilih_nama').val(),2);return false;
            });
            $('#edt').click(function() {
                ayax($('#pilih_nama').val(),3);return false;
            });
            ".($id?"ayax($id,0);\n":"")
        ;
        $data['css']="";
        $data['title']='Build';
        $data['menu_nav']=load_cnt('dm','Builder','dmnav',uri2('1,2,3'));
        
        #$data['css_load']=div('css_load');
        
        $data['content']='dm/dm_standard';
        $this->load->view('dm/home',$data); 
    }
    
    function save_db() {
        extract(get());
        $id=0;
        $dtgrab=array(
             'tabel'=>$this->ntabel,
             'where'=>array('nama'=>$nama),
             );
        if(($edt=$this->general_model->datagrab($dtgrab))&&($edt->num_rows()>0)){
            $id=$edt->row($this->fid);
        }else{
            $dtgrab=array(
                'tabel'=>$this->ntabel,
                'data'=>array('nama'=>$nama,),
            );
            if(!empty($dt))$dtgrab['data']['dt']=$dt;
            $id=$this->general_model->simpan_data($dtgrab);
        }
        redirect(uri2('1,2,').'build/'.$id);
    }
    
    #extract code
    function xcode($id) {
        $xdt=$this->load_tb($id);
        $xdt['inti']=$id;
        return $xdt;
    }
    
    #tes coding
    function ref($ntipe=null,$search = null,$offset = null) {
        $this->benchmark->mark('awalz');
        $search_param="";
        //cek(post(null));die();
        #if(is_null($ntipe)) $this->index();
        //$key = $this->input->post('search');
        $p=post(null);
        if(isset($p['search']))redirect(change_search_uri(uri_string(),$p['search']));
        
        //if($key = post('search'))redirect(uri_string().'/'.in_de(array('search'=>$key)));
        $modex = "list_data";
        if (!empty($search)) {
            $o = un_de($search);/*if(empty($o['modex']))$o['modex']=$modex;$modex = !empty($o['modex'])?$o['modex']:"list_data";*/
            if(empty($o['modex'])){$o['modex']=$modex;}else{$modex = $o['modex'];}
            $form_id = @$o['form_id'];
            $search_param = !empty($o['search']) ? $o['search'] : null;
        } 

        if($ntipe)$param=$this->xcode($ntipe);
        
        if(!empty($o['ret']))$param['first_title']=anchor($o['ret'],'<< Kembali','class="btn btn-default"');
        if(!empty($o['where']))$param['where']=$o['where'];
        if(!empty($o['tambah_hidden']))$param['tambah_hidden']=$o['tambah_hidden'];
            
        if(!empty($o['ret_save']))$param['ret_save']=$o['ret_save'];
        if(is_array(@$param)){
            $param = array_merge_recursive(array(
                    'search' => $search_param,
                    'param_search'=>$search,
                    'module' => uri2('1,2,3,'),
                    'page'  => $ntipe,
                    'this'  => uri(2),
            ),$param);
            $param['heads']=array('select2');
            $this->benchmark->mark('akhirz');
            if($this->debug) echo komen($this->benchmark->elapsed_time('awalz','akhirz'));
            
            #$this->load->library('Defa');
            
            switch($modex) {
                case "list_data" : 
                    if(isset($param['redir'])){
                        redirect($param['redir_path'].'/'.$param['redir'].'/'.$param['redir_func'].'/'. @$param['redir_param']);
                        //load_controller($param['redir_path'],$param['redir'],$param['redir_func'], $param['redir_param']); 
                    }else{
                        if(!(in_array($offset ,array('excel','cetak')))){
                            $param['return_object']=1;
                            if(is_role(uri(1),'DEBUG'))$param['debug_list']=1;
                            if($ntipe && !empty($param['tabel']))$data=$this->defa->list_data( $param, $offset); 
                            //$data=load_cnt('dm','def','list_data', $param, $offset); 
                            $data['content'] = "dm/standard_view";
                            $this->load->view('dm/home', $data);
                        }else{
                            $this->defa->list_data( $param, $offset); 
                            //load_controller('dm','def','list_data', $param, $offset); 
                        }
                    }
                    break;
                #case "form_mode" : load_controller('dm','def','form_data', $param, $form_id); break;//$this->folder
                case "form_mode" : 
                    if(@$o['ret_save'])$param['ret_save']=$o['ret_save'];
                    $this->defa->form_data( $param, $form_id); 
                    break;//$this->folder
                #case "save_mode" : load_controller('dm','def','save_data',$param); break;    //$this->folder
                case "save_mode" : 
                    $this->defa->save_data($param); 
                    break;    //$this->folder
            }
        }else{
            show_404();
        }
            
     }
    
      

    private function script_select2($id,$url,$onchange,$other_script=", width: '500px'") {
            return "
            \$(document).ready(function(){ 
                \$('$id').select2({
                ajax: {
                    url: '".site_url($url)."',
                    dataType: 'json',
                    type: 'POST',
                    delay: 250,
                    cache: true,
                    data: function (params) {
                            console.log(params);
                          return {                      
                            q: params.term, 
                            page: params.page
                          };
                        },
                    processResults: function (data, params) {
                          params.page = params.page || 1;
                          return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.data,
                                    id: item.id
                                }
                            })
                          };
                        },
                  }$other_script
            });
            \$('$id').on('change', function (e) { $onchange });        
        });
        ";
        }
    
    function btnkan($tag) {
        return tag('a',$tag,'class="btn btn-default btn-ac" pr="'.$tag.'"');
    }
    
    function desg($id=""){
        if(!$id)return false;
        $pr=$ipr=$npr=array();
        $apr=$this->load_param();
        $isipr=$this->load_tb($id);
        $z=$y=1;
        $bagi=(ceil(count($apr)/4));
        foreach ($apr as $x => $vpr) {
            $btn=$this->btnkan($vpr);
            $pr[$x]=$btn;
            $ipr[$x]=$btn;
            if( $z++ ==($bagi*$y))$y++;
            $npr[$y][]=$btn;
        }
        $tabel=
            table(
                tr(td_($npr[1]))
                .tr(td_($npr[2]))
                .tr(td_($npr[3]))
                .tr(td_($npr[4]))
            )
            .tag('a','Tabel','class="btn btn-warning" id="bt-tabel"')
            ."<br><br>"
            .div('','class="" id="isi-body"')
        ;         
        
        #cek(array($isipr,$bagi,$npr));
        $data=array(
            'tabel'=>$tabel,
        );
        $data['out_script']="
            function izi(tag) {
                $.ajax({
                  url: '".site_url('dm/tes/bform/'.$id)."'+'/'+tag,
                  type: 'POST',
                  cache: false,
                  success: function(msg) {
                      $('.form-delete-msg').html(msg);
                      $('#modal-delete').modal('show');
                  },
                  error:function(error){
                      console.log(error);
                  }
                });
                return false;
            }
        ";
        $data['script']="
            $('.btn-ac').click(function() {
                   var tl = $(this).attr('pr');
                   
                   bt_done ='<i class=\"fa fa-bookmark-o fa-btn\"></i> simpan';
                   
                   $('.form-title').html(bt_done);
                   $('#form_modal_dialog').attr('action','".site_url('dm/tes/simpan_fr/'.$id)."');
                   $('#form_modal_dialog').attr('method','POST');
                   
                   $('.form-delete-url').children().html(bt_done).show();
                   $('.form-delete-url').children().prop('type', 'submitform');
                   izi(tl);
                   return false;
                });
            $('#bt-tabel').click(function() {
                $.ajax({
                  url: '".site_url(uri2('1,2,').'body')."/'+$id,
                  type: 'POST',                  
                  cache: false,
                  success: function(msg) {
                      $('#isi-body').html(msg);
                  },
                  error:function(error){
                    console.log(error);
                  }
                });
            });
            ";
        $this->load->view('dm/dm_standard',$data); 
    }
    
    function load_param($id=0) {
        $par=array(
            1=>'title',
            2=>'first_title',
            3=>'last_title',
            4=>'tabel',
            5=>'tabel_save',
            6=>'where',
            7=>'tambah_hidden',
            8=>'id',
            9=>'eid',
            10=>'where_id',
            11=>'can',
            12=>'kolom',
            13=>'select',
            14=>'kolom_data',
            15=>'kolom_tampil',
            16=>'kolom_cari',
            17=>'order',
            18=>'tombol',
            19=>'tombol_tambah',
            20=>'extra_tombol',
            21=>'no_search',
            22=>'aksidikiri',
            23=>'ret_save',
            24=>'save_to',
            25=>'ret',
            26=>'form',
            27=>'form_script',
            28=>'list_script',
            29=>'no_aksi',
            30=>'heads',
            31=>'baris_custom',
            32=>'pageuri',
            33=>'pageper',
            34=>'pageplus',
            35=>'debug_list',
            36=>'tabs',
            37=>'bc',
            38=>'tombol_aksi',
        );
        if($id){
            return $par[$id];
        }else{
            return $par;
        }
    }    
    
    #autocomplete body tabel
    function ac_bd_tabel() {
        $q = $this->input->post('q');
        $q1 = $this->input->post('page');
        #$sql="SHOW TABLES LIKE '%$q%' limit 5";
        $sql="SELECT TABLE_NAME id, TABLE_NAME data FROM INFORMATION_SCHEMA.TABLES WHERE 
             TABLE_SCHEMA = '".$this->db->database."' AND TABLE_NAME LIKE '%$q%'  LIMIT 0,5;";
    
        $res = $this->db->query($sql);
        die(json_encode($res->result()));
    }
    
    function body_tabel($ntabel) {
        $isi="";
        if(is_array($ntabel)){
            $isi=implode_array($ntabel);
        }else{
            $isi=form_dropdown('ntabel',array(),$ntabel,'class="ntabel"')
                .tag('a','<i class="fa fa-check"></>','class="btn btn-success" id="cek"');
        }
        
        return div(
            $isi
        ,'class="input-group margin"');
    }
    
    #tes khusus tabel
    function body($id) {
        $dt=$this->load_tb($id);
        $tabel=
            form_open(uri2('1,2,').'sv_tabls','class="form_tbls"')
            .div('','id="tabl"')
            .div($this->body_tabel($dt['tabel']))
            .form_close()
            ;            
        $data['title']=$dt['title'];
        $data['tabel']=$tabel;
        $data['include']=reqcss('assets/plugins/select2/select2.css').reqjs('assets/plugins/select2/select2.js');
        $data['out_script']=
            $this->script_select2('.ntabel',uri2('1,2,').'ac_bd_tabel','')
            ."";
        $data['script']="
            var tabl=$('#tabl'),
                pil=$('.ntabel');
            
            $('.form_tbls')
                .on('click','#cek',function() {
                    var inp = '<div class=tbls>'
                        +'<a class=\"btn btn-danger remove\" ><i class=\"fa fa-trash\"></i></a>'
                        +'<a class=\"btn btn-default join\" ><i class=\"fa fa-bug\"></i></a>'
                        +'<input type=hidden name=tabl[] value=\"'+pil.val()+'\" />'    
                        +'<span class=nm_tbl> '+pil.val()+' </span>'
                        +'</div>';
                    $(inp).appendTo(tabl);                    
                })
                .on('click','.remove',function() {
                    if(confirm('di hapus ??'))
                        $(this).parents('.tbls').remove();
                })
                .on('click','.join',function() {
                    var ints='<input name=apa value=99 type=text />';
                    var tablx=$(this).parents('.tbls');
                    $(ints).appendTo(tablx);
                    alert($(this).parents('.tbls').find('input[name=\"tabl[]\"]').val());
                });
        ";/*
                    */
        #if(confirm('ini '+$(this).parents('.tbls').val())) $(this).parents('.tbls').val();
        $this->load->view('dm/default',$data);
    }
  
    #tes
    function cek($id) {
        $dt=$this->load_tb($id);
        $edt=$this->ld_tb($id);
        echo "";
        cek(array($dt,implode_array($dt),strlen(implode_array($dt)),$edt,strlen($edt)));
    }
    
    #hapus
    function del_db() {
        $p=get();#stop(array($p,$p['id']));
        if(!empty($p['id'])){
            $this->db->where(array($this->fid => $p['id']))->delete($this->ntabel);
            redirect($p['ret']);
        }        
    }
    
    #edit 
    function edt($id){
        if($ddt=$this->general_model->datagrab(array(
                'tabel'=>$this->ntabel,
                'where'=>array($this->fid=>$id)
               ))){
            $dt=$ddt->row();
            
        echo form_open(uri2('1,2,').'save_db','method="GET"')
            .div(
                div(
                    div(
                        form_label('nama','nama')
                        .form_input('nama',$dt->nama,'class="form-control" id="nama"')
                    ,'class="form-group"')
                    .div(
                        form_label('Dt','dt')
                        .form_input('dt',$dt->dt,'class="form-control" id="dt"')
                    ,'class="form-group"')
                    .'<button name="" type="submit" class="btn btn-success">Simpan</button>'
                ,'class="col-lg-12"')
            ,'class="row"')
            .form_close();
        }
    }
    
    function abc() {
        $htm="c2NvcXNtZzR0ZG5iamR0MWwxOGMyMGRtYTE=";
        cek(array(
            'a'=>$htm,
            'b'=>base64_decode( $htm ),
        ));
    }
  }
?>
