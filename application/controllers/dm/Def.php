<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/*
*  ver 1.0158
*/
class Def extends CI_Controller {
  var $debug=0;
  var $fold='dm';
  var $ver='1.02';
  public function __construct()
  {
      parent::__construct();
      $this->load->helper('cmd');
      if(not_login(uri_string()))redirect('login');
      $this->benchmark->mark('awalzz');
      $this->load->config('cmd');          
  }
  
  private function array_add($a1, $a2) {  // ...
      // adds the values at identical keys together
      $aRes = $a1;
      foreach (array_slice(func_get_args(), 1) as $aRay) {
          foreach (array_intersect_key($aRay, $aRes) as $key => $val) $aRes[$key] += $val;
          $aRes += $aRay; 
      }
      return $aRes; 
  }

  function daftar_tabel($prefix) {
      cek(load_cnt('dm','Builder','daftar_tabel',$prefix));
  }
  
  function list_table($ntabel="",$fields="") {
      $ret=load_cnt('dm','Builder','list_insert',$ntabel,$fields,1);        
      if(!$ntabel){
        $data['title']='List Tabel';
        $data['tabel']=$ret;
        $data['menu_nav']=load_cnt('dm','Builder','dmnav');
        $data['content']='dm/default';
        $this->load->view('dm/home',$data); 
      }else{
          echo $ret;
      }
  }
  
  function list_create($ntabel="") {
      load_cnt('dm','Builder','list_create',$ntabel);
  }
  
  /**
  * v:2.5
  * pageper, pageuri
  */
  function list_data($o = null, $offset = null) {
      $this->load->library('Defa');                            
      return $this->defa->list_data( $o, $offset);
  }
  
  
  /**
  * v:2.5
  * pageper, pageuri
  */
  function form_data($o = null, $offset = null) {
      $this->load->library('Defa');                            
      return $this->defa->form_data( $o, $offset);
  }
  

    /**
    * v:2.5
    * pageper, pageuri
    */
    function save_data($o = null) {
      $this->load->library('Defa');
      return $this->defa->save_data( $o);
    }
  

    function general_delete($par) {
        $this->load->library('Defa');                            
        return $this->defa->general_delete( $par);
    }
    

    /**
    *  text, ar &&apa&=>menjadi
    */
    function changeto($text,$r) {
        if(is_object($r))$r=json_decode(json_encode($r),1);
        if(is_array($r)){
            foreach ($r as $x=>$y){
                $pat[]='/&&'.$x.'&/';
                $rep[]=$y;
            } 
            return preg_replace($pat,$rep,$text);
        }else{
            return $text.implode_array($r)."xx".$text;
        }
    }
    
    /**
    *  x y =
    */
    function compareit($x,$y,$op) {
        $ret=false;
        switch($op){
            case '=':$ret=$x==$y;break;
            case '>':$ret=$x>$y;break;
            case '>=':$ret=$x>=$y;break;
            case '<':$ret=$x<$y;break;
            case '<=':$ret=$x<=$y;break;
        }
        return $ret;
    }
    
    function explode_alt($p) {
        $ret='';
        foreach ($p as $x => $v) 
          $ret.=' '.$x.'="'.$v.'"';
        return $ret;
    }
    
  function cr($e) {
        return $this->general_model->check_role(sesi('id_pegawai'),$e);
  }
  
  /** v:1.0
    * path 'tnde/'
    * [type]
    * nama_file
    */
  function upload_file($param) {
        extract($param);
        $upath = './uploads/'.(@$path?$path:'');
        if (!is_dir($upath)) mkdir($upath,0777,TRUE);
            
        $this->load->library('upload');
        
        $this->upload->initialize(array(
            'upload_path' => $upath,
            'allowed_types' => (@$type?$type:'jpg|jpeg|png|gif|pdf|word|doc|docx|xls|xlsx'),
        ));
        $this->upload->do_upload($nama_file);
        $data_upload = $this->upload->data();
        
        if ($onerror = $this->upload->display_errors('&nbsp','&nbsp')){
            return false;
        }else {
            return $data_upload['file_name'];
        };
  }
    
  /** v:1.0
    * link address
    */
  function bc($link) {
        $bc=array();
        if(
        $d=$this->general_model->datagrab(array(
            'tabel'=>array(
                'nav n0'=>'',
                'nav n1'=>array('n1.id_nav=n0.id_par_nav','left'),
                'nav n2'=>array('n2.id_nav=n1.id_par_nav','left'),
                'nav n3'=>array('n3.id_nav=n2.id_par_nav','left'),
            ),
            'where'=>array('n0.link'=>$link),
            'select'=>'n0.*,n1.judul judul1,n1.link link1
                ,n2.judul judul2,n2.link link2
                ,n3.judul judul3,n3.link link3',
           ))->row()){
               if($d->judul3 >'')if($d->link3){$bc[$d->link3]=$d->judul3;}else{$bc[]=$d->judul3;}
               if($d->judul2 >'')if($d->link2){$bc[$d->link2]=$d->judul2;}else{$bc[]=$d->judul2;}
               if($d->judul1 >'')if($d->link1){$bc[$d->link1]=$d->judul1;}else{$bc[]=$d->judul1;}
               if($d->judul >'') $bc[$d->link]=$d->judul;
           }       
    
        return $bc;
  }
  
  /** v 15
  * 1regu, 2label, 3nama 4url_complete 5css 6fiels_name_on_Def 
  * 7function_onchange 
  * 8array('id,'data)
  * 9array(
  *     param_data=>,data:??   -> (new send post with parameter on form) 
  *     param_process          ->      on process data
  * )  
  * 10_Other_attr
  *  ret:: fr, data[include. script]
  * 
  */
  function inp9_select2($fo,$def='',$data='') {
      $param_data=$param_process="";
      list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      if(@$fo[9])extract($fo[9]);
      if(!@$data['include']['select2'])
        $data['include']['select2']=reqcss('assets/plugins/select2/select2.css').reqjs('assets/plugins/select2/select2.js');
      
      if(!$fo6=@$fo[6])die($label2.' param ke 6 wajib di isi!!');
      $f7=@$fo[7];
      $data9=(@$def?array(@$def->$nama3=>@$def->$fo6,):array());
      if(!@$data['script'])$data['script']='';
      
        /*if($this->debug)cek(array($data9));*/
      $f8_=array('id'=>'id','data'=>'data');
      if(($f8=@$fo[8])&&(is_array($f8)))$f8_=$f8;
      $fr = '<p>'
        .form_label($label2)
        .form_dropdown($nama3,$data9,@$def->$nama3,
            'class="'.$apa5.' form-control" id="'.$nama3.'" '.@$fo[10].' style="width: 100%" '.($requ1  ? 'required' : ''))
        .'</p>';
        $data['script'].="
        \$('#".$nama3."').select2({
            val:'".(@$def->$nama3?$def->$nama3:'null')."',
            ajax: {
                url: '".site_url($apa4)."',
                dataType: 'json',
                type: 'POST',
                delay: 250,
                cache: true,
                data: function (params) {
                      return {
                        q: params.term, // search term
                        page: params.page$param_data
                      };
                    },
                processResults: function (data, params) {
                      params.page = params.page || 1;
                      return {
                        results: $.map(data, function (item) {
                            $param_process
                            return {
                                text: item.$f8_[data],
                                id: item.$f8_[id]
                            }
                        })
                      };
                }
            }
        });"
        .(@$f7?"\$('#".$nama3."').on('change', function (e) { ".$f7." /* e.currentTarget.value */ });":'');
      return array($fr,$data);
  }
  
  function inp11_select2($fo,$def='',$data) {
      list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      
      if(!@$data['include']['select2'])
        $data['include']['select2']=reqcss('assets/plugins/select2/select2.css').reqjs('assets/plugins/select2/select2.js');
      if(!@$data['script'])$data['script']='';
      
      $fr = '<p>'
            .form_label($label2)
            .form_dropdown($nama3,$apa4,@$def->$nama3,'class="'.$fo[5].' form-control " id="'.$nama3.'" style="width: 100%" '.($requ1  ? 'required' : ''))
            .'</p>';
            $data['script'].="\$(document).ready(function(){
                    \$('#$nama3').select2({
                            minimumInputLength: 2,
                            multiple:true,
                            delay: 250,
                            ajax: {
                    url: '".site_url($fo[6])."',
                    dataType: 'json',
                    type: 'POST',
                    delay: 250,
                    cache: true,
                    data: function (params) {
                            console.log(params);
                          return {
                            q: params.term, // search term
                            page: params.page
                          };
                        },
                    processResults: function (data, params) {
                          params.page = params.page || 1;
                          return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.data,
                                    id: item.id
                                }
                            })
                          };
                    }
                }
                    })
                    });";
            /*$ada_dataselect=true;*/
      return array($fr,$data);
  }
  
  function inp8_typeahead($fo,$def='',$data) {
      //list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      
      if(!@$data['include']['typeahead'])
        $data['include']['typeahead']=reqjs('assets/plugins/typeahead/typeahead.min.js');
      if(!@$data['script'])$data['script']='';
      
      $fr = '<p class="form-group">'
            .form_label($fo[2])
            .form_input($fo[3],@$def->$fo[3],'id="'.$fo[3].'" autocomplete="off" class="typeahead '.$fo[5].'" '.($requ1  ? 'required' : ''))
            .'</p>'; 
       
      $data['script'].="
            function display".$fo[3]."(item) {
                \$('#".$fo[3]."').removeAttr('value');
                \$('#".$fo[3]."').attr('value', item.value);                    
                \$('#".$fo[3]."').val(item.value);
            }
            \$('#".$fo[3]."').typeahead({
                ajax: '".site_url('tnde/autocomplete/'.$fo[4])."',
                displayField: 'data',
                limit: 10,
                valueField: '".$fo[3]."',
                onSelect: display".$fo[3]."
            });
      ";
      return array($fr,$data);
  }
  
  function inp3_textarea($fo,$def='',$data) {
      $this->load->helper('tinymce');
      list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      $style=(@$fo[6]?$fo[6]:'height: 75px');
      $fr = '<p>'.form_label($label2)
        .form_textarea($nama3,html_entity_decode(@$def->$nama3),'class="'.$apa4.' textarea form-control" style="'.$style.'" '.($requ1  ? 'required' : '')).'</p>'; 
      if(!$apa5){
        $data['include']['tinymce']=reqjs('assets/tinymce/tinymce.min.js');
        $data['script'].=tinymce();
      }
      return array($fr,$data);
  }
  
  function inp1($fo,$def='',$data) {
    date_default_timezone_set($this->config->item('waktu_server'));
    $extra_form =$form_data= null;
        
      list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      
      if ($apa5){
        switch ($apa5) {

            case "formatnumber":
                if($def)$form_data = !empty($def->$nama3)?numberToCurrency($def->$nama3):null;
                $extra_form = ' onkeyup="formatNumber(this)"';
            break;

            case "datepicker":
                if($def)$form_data = (isset($def->$nama3)?date("d-m-Y",strtotime($def->$nama3)):null);//$def->$nama3;//
                $extra_form ='autocomplete="off"';  
                $data['script'].="
                    $('#".$nama3."').datepicker({
                        format: 'dd-mm-yyyy',
                  todayBtn: 'linked',
                  clearBtn: true,
                  language: 'id',
                  autoclose: true,
                  todayHighlight: true});

                  $('#".$nama3."').change(function(){
                    $('#".$nama3."').removeAttr('value');
                  });
                ";
            break;

            case "yearpicker":
                if($def)$form_data = !empty($def->$nama3)?date("Y",strtotime($def->$nama3)):null;
                $data['script'].="
                  \$('#".$nama3."').datepicker({
                        format: 'yyyy',
                        viewMode: 'years', 
                        minViewMode: 'years',
                      //todayBtn: 'linked',
                      clearBtn: true,
                      language: 'id',
                      autoclose: true,
                      //todayHighlight: true
                  });

                  \$('#".$nama3."').change(function(){
                    \$('#".$nama3."').removeAttr('value');
                  });
                ";
            break; 

            case "timepicker":
                if($def)$form_data = $def->$nama3;
                $addclass= 'bootstrap-timepicker';
                  $data['script'].="
                        $('#".$nama3."').timepicker({
                          showInputs: false,
                          showMeridian: false
                        });\n";
                  $data['include']['timepick']=reqcss('assets/plugins/timepicker/bootstrap-timepicker.min.css').reqjs('assets/plugins/timepicker/bootstrap-timepicker.min.js');
            break;

            case "airdatepicker":
                if($def)
                    $form_data = (isset($def->$nama3)?date("d-m-Y",strtotime($def->$nama3)):null);//$def->$nama3;//
                $extra_form ='class="datepicker-here" data-language="id" autocomplete="off"';  
                $data['script'].="
                    var current".$nama3."Date = new Date();
                    $('#".$nama3."').datepicker().data('datepicker').selectDate(
                        new Date("
                        .($def
                        ?date("Y,m,d",strtotime($def->$nama3))
                        :"current".$nama3."Date.getFullYear(), 
                        current".$nama3."Date.getMonth(), current".$nama3."Date.getDate()")
                        .")
                    );
                ";
                $data['include']['airdatepicker']=
                    reqcss('assets/plugins/air-datepicker/datepicker.min.css')."\n"
                    .reqjs('assets/plugins/air-datepicker/datepicker.min.js')."\n"
                    .reqjs('assets/plugins/air-datepicker/datepicker.id.js')."\n"
                    ;
            break;
            
        }
        
      } else {
        $form_data = @$def->$nama3;
      }
            
      if (!empty($fo[6])) 
        $extra_form = $fo[6];
      $fr = '<div class="'.@$addclass.'"><p>'
                .form_label($label2)
                .form_input($nama3,$form_data,' class="'.@$apa4.' form-control" id="'.$nama3.'" '.($requ1  ? 'required' : '').' '.$extra_form)
            .'</p></div>'
      ;
      if (!empty($fo[7])) 
        $data['script'].="
            \$('#".$nama3."').change(function() {
                    var id = $(this).val();
                    \$('#".$fo[8]."').attr('type', 'hidden');

                console.log(id);
               
                \$.ajax({
                    url: '".site_url($fo[7])."',
                    async: false,
                    type: 'POST',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data) {
                     $('#".$fo[8]."').attr('value', data);
                     $('#".$fo[8]."').attr('type', 'text');
                     $('#label".$fo[8]."').removeAttr('class');
                    }
                })
            });
        ";
      
            
      return array($fr,$data);
  }
  
  function inp2_dropd($fo,$def='',$data) {
      list(,$requ1,$label2,$nama3,$apa4,$apa5)=$fo;
      
      if(!@$data['script'])$data['script']='';
      
      if (@$fo[6]) {
            # code...
            $data['script'].="
                \$('#".$fo[7]."').ready(function(){
                    var item = \$('#".$fo[7]."');
                    item.empty();
                    var data = '".@$def->$fo[7]."';
                    if (data != null){
                    var idjenis = '".@$def->$nama3."';  

                    \$.ajax({
                    url: '".site_url($fo[6])."',
                    async: false,
                    type: 'GET',
                    data: 'id='+idjenis,
                    dataType: 'json',

                    success: function(data) {
                       // var obj = [];
                        \$('#".$fo[7]."').append('<option value=".@$def->$fo[7]." selected=selected>--PILIH--</option>');  
                    $.each(data, function(key, val){
                   // obj.push(val.item);
                    console.log(val.item);
                  
                        \$('#".$fo[7]."').append('<option value='+val.id+'>'+val.item+'</option>');  
                            })
                            //end each
                            }
                        })
                        }
                });

                \$('#".$nama3."').change(function() {
                    var id = $(this).val();
                   
                    \$('#".$fo[7]."').empty();
                    console.log(id);
                   
                    \$.ajax({
                        url: '".site_url($fo[6])."',
                        async: false,
                        type: 'GET',
                        data: 'id='+id,
                        dataType: 'json',

                        success: function(data) {
                           // var obj = [];
                            \$('#".$fo[7]."').append('<option value=>--PILIH--</option>');  
                        $.each(data, function(key, val){
                           // obj.push(val.item);
                            console.log(val.item);
                          
                          \$('#".$fo[7]."').append('<option value='+val.id+'>'+val.item+'</option>');  
                        })
                        //end each
                           
                        }
                    })

                });
            "; 
      }

      $fr = '<p>'.form_label($label2).form_dropdown($nama3,$apa4,@$def->$nama3,'class="'.$apa5.' " id="'.$nama3.'" style="width: 100%" '.($requ1  ? 'required' : '')).'</p>'; 
      return array($fr,$data);
  }
  
  /**
  *  general delete
  *  id,(id_tabel,tabel,ret)
  */
  function del($id,$parm) {
            extract(un_de($parm));
            if(isset($id_tabel) && isset($tabel))
                $this->db->where(array($id_tabel=>$id))->delete($tabel);
            redirect($ret);
            //cek($p);
  }
  
  /**
  * v:0.1 nama_tabel, kode, data='',struktur(kode,data)
  */
  function kode_param($ntabel,$kode,$data='',$kode_dt='') {
        $kode_data=array('kode','data');
        if($kode_dt)$kode_data=$kode_dt;
        $adata=array('tabel'=>$ntabel,);
        if(is_array($kode)){
            $adata['where']=array("$kode_data[0] in('".implode("','",$kode)."')"=>null);
            $adata['select']=$kode_data[0].','.$kode_data[1];
            if(($db_data=$this->general_model->datagrab($adata))&&($db_data->num_rows()>0)) 
                foreach ($db_data->result() as $rec) 
                    $dbdata[$rec->$kode_data[0]]=$rec->$kode_data[1];
        }else{
            $adata['where']=array($kode_data[0]=>$kode);
            $adata['select']=$kode_data[1];
            $dbdata=$this->general_model->datagrab($adata)->row($kode_data[0]);
        }             
                    
        $ret=null;
        if($data){
            if($dbdata){
                return $this->db->where(array($kode_data[0]=>$kode))
                    ->update($ntabel,array($kode_data[0]=>$data));        
            }else{
                return $this->db->insert($ntabel,array($kode_data[0]=>$kode,$kode_data[1]=>$data));
            }
        }else{
            return $dbdata;
        }
    }
    
    /**
    * cek in_de
    */
    function cekinde($d) {cek(un_de($d));}
    
    /** v:0.01
    * tes regex
    */  
    function regex() {
      if($p=post(null)){
            $ret="";
            extract($p);
            switch($tipe){
              case 1:$ret=preg_match($kode,$tex);
              break;
              case 2:$ret=preg_replace($kode,$repl,$tex);
              break;
              case 3:$ret=preg_split($kode,$tex,@$ops1,@$ops2);
              break;
            }
            echo "<pre>";
            var_dump($ret);
            echo "</pre>";
      }else{
          $param=array(
              'def'=>array(
                'tipe'=>'3',
                'kode'=>'/s+/',
                'tex'=>"`id_kop` int(2) NOT NULL AUTO_INCREMENT",
                'ops1'=>"-1",
              ),  
              'id'=>1,  
              'return_view'=>1,
              'save_to'=>uri(1).'/'.uri(2).'/'.uri(3),
              'form'=>array(
                array(10,true,'Regex','tipe',array(
                    '1'=>'preg_match',
                    'preg_replace',
                    'Preg_split'
                    ),'combo-box',''),
                array(1,1,'Kode','kode','',''),
                array(1,1,'Replace','repl','',''),
                array(3,0,'Text','tex','','-'),
                array(1,1,'opsi1','ops1','',''),
                array(1,1,'opsi2','ops2','',''),
                array(50,'',
                    "<p>".form_button('','Preview','id="prev" idkop="&&id_kop&" class="btn btn-primary"'
                        )
                        ."</p>".
                        "<p><div id='preview'>".''
                     //$this->show(1,'view')
                     ."</div></p>",
                    '','','',''
                ),
              ),
              'form_script'=>
                "\$('#prev').click(function () {
                    var ini=$('#form_data');
                    //alert();
                    console.log(ini.attr('action'));
                        $.ajax({
                            url: ini.attr('action'),
                            type: 'POST',
                            cache: false,
                            //dataType: 'json',
                            data: ini.serialize(),
                            success: function(msg) {
                                console.log('berhasil'+msg);
                                $('#preview').html(msg );
                            },
                            error:function(x, t, m){
                                $('#preview').html('Error '+ t );
                            }
                        });
                        return false;
                    });
                    ",
          );
          $this->form_data($param);
      }
    }
    
  function delete($id,$param) {
        $par=un_de($param);
        $par['id']=$id;
        $this->general_delete( in_de($par));
  }
        
  function cekpost() {
      stop(post());
  }  
  
  
  /**
  *  general change to item tabel ,common use to change status
  *  id,(id_tabel,tabel,ret,field,changeto)
  */
  function cto($id,$parm) {
    extract(un_de($parm));
    if(isset($id_tabel) && isset($tabel)&& isset($field)&& isset($changeto)){
        $tabel=find_1st_table($tabel);
        $this->db->where(array($id_tabel=>$id))->update($tabel,array($field=>$changeto));
    }   
    redirect($ret);//cek($p);
  }
  
  /**
    * redirect with par-> requ:url,key,add
    */
  function redir($par,$u1="",$u2="",$u3="",$u4="",$u5="") {
        extract(un_de($par));
        $sr=array();
        if(!empty($add))$sr=$add;
        if($u1)$sr[$key[0]]=$u1;
        if($u2)$sr[$key[1]]=$u2;
        if($u3)$sr[$key[2]]=$u3;
        if($u4)$sr[$key[3]]=$u4;
        if($u5)$sr[$key[4]]=$u5;
        redirect(add_uri_search($url,$sr));
  } 
  
  function rst_operator($param= null,$id = 0){
        extract(un_de($param));
        $simpan=array('password'=>md5('qwerty'));
        $this->db->where(array('id_pegawai'=>$id))->update('peg_pegawai',$simpan);
        setflashsesi('ok','Reset Done');
        if(!empty($ret))redirect($ret);
  }    

  /**
  *  general delete all
  *  (where[id_tabel->id],tabel,ret)
  */
  function hapus($parm) {
    extract(un_de($parm));
    if(isset($tabel)){
        if(isset($id_tabel))$where=array($id_tabel=>$id);
        $this->db->where($where)->delete($tabel);
    }
    redirect($ret);
    //cek($p);
  }
  
  /**
  *  general multi delete all
  *  mdel=>array(where[id_tabel->id],tabel),ret
  */
  function mhapus($parm) {
    extract(un_de($parm));
    if(isset($mdel)){
        if(!is_array($mdel))$mdel=(array)$mdel;
        foreach ($mdel as $d) {
            if(isset($d['id_tabel']))$where=array($d['id_tabel']=>$d['id']);
            $this->db->where($d['where'])->delete($d['tabel']);
        }
    }
    if(isset($mq)){
        if(!is_array($mq))$mq=(array)$mq;
        foreach ($mq as $q) {
            $this->db->query($q);
        }
    }
    redirect($ret);
    //cek($p);
  }

  
}
?>