<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Modref extends CI_Controller {

	function __construct() {
	
		parent::__construct();
		
	}
	
	public function index() {
		
		$this->show_navi();
	
	}
	
	function tabel_sub($lev,$a,$id) {
		
		$sub_bid = $this->general_model->datagrab(array(
			'tabel'=> 'nav',
			'where' => array('id_par_nav' => $id),
			'order' => 'urut'
		));
		
		if ($sub_bid->num_rows() > 0) {
			foreach($sub_bid->result() as $row) {
			
				$cell = array();
				for ($j=0;$j < $lev; $j++) {
					if ($j>0) $cell[] = array('data'=>'','width' => '20');
				}
				if ($row->tipe == 2) {
				$links = array(
					anchor('#','<i class="fa fa-pencil"></i>','class="btn-edit" act="'.site_url('referensi/modref/form_data/'.in_de(array('id_aplikasi' => $a,'id' => $row->id_nav,'id_par' => $id))).'"')
				);
				}
				$sub_bidd = $this->general_model->datagrab(array('tabel' => 'nav', 'where' => array('id_par_nav' => $row->id_nav)));
				if ($sub_bidd->num_rows() == 0)  {
						$links[] = anchor('#','<i class="fa fa-trash"></i>','class="btn-delete" act="'.site_url('referensi/modref/removing/'.in_de(array('id' => $row->id_nav,'app' => $a))).'" msg="Apakah Modul Referensi <b>'.$row->judul.'</b> akan dihapus?"');
				}
			
					$tipe = ($row->tipe == 1) ? '<span class="label bg-yellow"><i class="fa fa-key"> </i></span>' : '<span class="label bg-purple"><i class="fa fa-navicon"></i></span>';
					$icon = (!empty($row->fa)) ? $row->fa :  'circle-o';
					$kode = !empty($row->kode) ? '<b>'.$row->kode.'</b>' : null;
					$nama = !empty($row->judul) ? ' '.$row->judul : null;
					$btn_sub = ($row->tipe == 2) ? ' <a href="#" class="btn-edit badge bg-green" act="'.site_url('referensi/modref/form_data/'.in_de(array('id_aplikasi' => $a,'id_par' => $row->id_nav))).'"><i class="fa fa-plus-square"></i> &nbsp; Sub</a>' : null;
					$cell[] = array('data'=> $tipe.' &nbsp; <i class="fa fa-'.$icon.'"></i> &nbsp; '.$kode.$nama.$btn_sub,'colspan'=> 8-$lev);
					$cell[] = implode(' &nbsp ',$links);
						
					$this->table->add_row($cell);
					if ($sub_bidd->num_rows() > 0 and $lev < 5) $this->tabel_sub($lev+1,$a,$row->id_nav);	
			}
		}
		
	}
	
	function tabel($a) {
		
		$nav = $this->general_model->datagrab(array(
			'tabel'=> 'nav',
			'where' => array('ref = 2 AND id_par_nav IS NULL and id_aplikasi = "'.$a.'"' => null),
			'order' => 'urut',
		));
		
		$root_btn = anchor(
			'#',
			'<span class="badge bg-green"><i class="fa fa-plus-square"></i> &nbsp; Root Modul Referensi</span>',
			'class="btn-edit" act="'.site_url('referensi/modref/form_data/'.in_de(array('status' => 'root','id_aplikasi' => $a))).'"');
		
		if ($nav->num_rows() > 0) {
            $this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered table-condensed gap">'));
            $this->table->set_empty("&nbsp;");	
			$this->table->set_heading(
				array('data' => 'Modul Referensi &nbsp; '.$root_btn,'colspan' => '7'),
				array('data' => 'Aksi', 'width' => '70'));
	    
				foreach ($nav->result() as $row) {

					$links = array(
						
						anchor('#','<i class="fa fa-pencil"></i>','class="btn-edit" act="'.site_url('referensi/modref/form_data/'.in_de(array('id_aplikasi' => $a,'id' => $row->id_nav))).'"')
						
					);
						
					$subs = $this->general_model->datagrab(array('tabel' => 'nav', 'where' => array('id_par_nav' => $row->id_nav)));
					if ($subs->num_rows() == 0)  {
						$links[] = anchor('#','<i class="fa fa-trash"></i>','class="btn-delete" act="'.site_url('referensi/modref/removing/'.in_de(array('id' => $row->id_nav,'app' => $a))).'" msg="Apakah Modul Referensi <b>'.$row->judul.'</b> akan dihapus?"');
					}
					
					$tipe = ($row->tipe == 1) ? '<span class="label bg-yellow"><i class="fa fa-key"> </i></span>' : '<span class="label bg-purple"><i class="fa fa-navicon"></i></span>';
					$icon = (!empty($row->fa)) ? $row->fa :  'circle-o';
					$kode = !empty($row->kode) ? '<b>'.$row->kode.'</b>' : null;
					$nama = !empty($row->judul) ? ' '.$row->judul : null;
					$btn_sub = ($row->tipe == 2) ? ' <a href="#" class="btn-edit badge bg-green" act="'.site_url('referensi/modref/form_data/'.in_de(array('id_aplikasi' => $a,'id_par' => $row->id_nav))).'"><i class="fa fa-plus-square"></i> &nbsp; Sub</a>' : null;
				
					$this->table->add_row(
						array('data'=> $tipe.' &nbsp; <i class="fa fa-'.$icon.'"></i> &nbsp; '.$kode.$nama.$btn_sub,'colspan'=> 7),
						implode(' &nbsp ',$links)
					);
					if ($nav->num_rows() > 0) $this->tabel_sub(2,$a,$row->id_nav);
				}
				return  $this->table->generate();
			
		} else {
		
			return  '<div class="alert"">Belum ada Modul Referensi Aplikasi yang terdaftar &nbsp; '.$root_btn.'</div>';
		
		}
		
	}
	
	function show_navi($par = null) {
		
		if (!empty($par)) $par = un_de($par);
	
		$data['breadcrumb'] = array('' => 'Pengaturan', 'referensi/modref' => 'Modul Referensi');
		
		$pil = $this->input->post('pil');
		$pil = !empty($pil) ? $pil : (!empty($par['app'])?$par['app']:null);
		$where = (!empty($pil)) ? array('id_aplikasi' => $pil) : array('id_aplikasi' => 1);
		
		$apps = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => $where))->row();
		$active = ($apps->aktif > 0) ? '<span class="label label-info" style="font-weight: 100; letter-spacing: 2px">Aktif</span>' : '<span class="label" style="font-weight: 100; letter-spacing: 2px">Non-Aktif</span>';
		$data['title']	= 'Pengaturan Navigasi '.$apps->nama_aplikasi.' '.$active;
		$app_combo_data = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','order' => 'urut','where' => array('id_par_aplikasi IS NULL' => null)));
		
		$app_combo = array('' => ' -- Pilih Aplikasi -- ');
		foreach($app_combo_data->result() as $ap) {
			
			$app_child = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','order' => 'urut','where' => array('id_par_aplikasi = '.$ap->id_aplikasi => null)));
			$app_combo[$ap->id_aplikasi] = $ap->nama_aplikasi;
			foreach($app_child->result() as $apc) { $app_combo[$apc->id_aplikasi] = ' -- '.$apc->nama_aplikasi; }
			
		}
		
		$data['script'] = '
			$(document).ready(function() {
				$("select").select2();
				$(".change-pil").change(function() {
					$("#form_pilih").submit();
				});	
			});
		';
		
		$data['tombol'] = 
			form_open('referensi/modref','id="form_pilih"').
				form_dropdown('pil',$app_combo,!empty($pil)?$pil:1,'class="combo-box change-pil" style="width: 100%"').
			form_close();
	
		$data['title'] = 'Modul Referensi';
		$data['descript'] = 'Referensi per Aplikasi';
		if (!empty($pil)) $data['tabel'] = $this->tabel($pil);	
		else $data['tabel'] = '<div class="alert">Pilih Aplikasi</div>';
	
		$data['content'] = "umum/standard_view";
		$this->load->view('home', $data);
		
	}
	
	function form_data($param) {
	
		$o = un_de($param);
	
		$apps = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('id_aplikasi' => $o['id_aplikasi'])))->row();
		$data['title'] = (!empty($o['id'])) ? "Ubah" : "Tambah";
		$data['title'].= " Modul Referensi";
		$def = !empty($o['id']) ? $this->general_model->datagrab(array('tabel' => 'nav','where' => array('id_nav' => $o['id'])))->row():null;
		
		$tipe_combo = array(
			'' => ' -- Pilih Jenis Modul Referensi -- ',
			'1' => 'Kewenangan',
			'2' => 'Navigasi',
		);
		
		$ag_combo = array(
			'' => ' -- Pilih -- ',
			'1' => 'Ya',
			'2' => 'Tidak'
		);
		
		$form_par = '<p>'.form_label('Aplikasi').form_input('',$apps->nama_aplikasi,'readonly="readonly" class="form-control"').'</p>';
		
		if (!empty($o['id_par'])) {
			$par =  $this->general_model->datagrab(array('tabel' => 'nav','where' => array('id_nav' => $o['id_par'])))->row();
			$form_par.= form_hidden('id_par_nav',$o['id_par']).form_label('Navigasi Atas').form_input('',$par->kode.' '.$par->judul,'readonly="readonly" class="form-control"');
		}
	
		$id_nav = !empty($o['id'])?$o['id']:null;
				
		$data['form_link'] = 'referensi/modref/saving';
		$data['form_data'] =
			$form_par.
			'<p>'.form_label('Jenis Modul Referensi').form_dropdown('tipe',$tipe_combo,@$def->tipe,'class="combo-box form-control" style="width: 100%" required').'</p>'.
			'<p>'.form_label('Kode Modul Referensi').form_input('kode',@$def->kode,'class="form-control"').'</p>'.
			'<p>'.form_label('Judul').form_input('judul',@$def->judul,'class="form-control" required').'</p>'.
			'<p>'.form_label('Link').form_textarea('link',@$def->link,'class="form-control" style="height: 60px"').'</p>'.
			'<p>'.form_label('Icon').form_input('fa',@$def->fa,'class="form-control"').'</p>'.
			'<p>'.form_label('Separator').form_dropdown('separator',$ag_combo,@$def->separator,'class="combo-box form-control" style="width: 100%"').'</p>'.
			'<p>'.form_label('Aktif').form_dropdown('aktif',$ag_combo,@$def->aktif,'class="combo-box form-control" style="width: 100%"').'</p>'.
			form_hidden('id_nav',$id_nav).form_hidden('id_aplikasi',$o['id_aplikasi']);
		$this->load->view('umum/form_view', $data);
	
	}
	
	function saving() {

		$id = $this->input->post('id_nav');
		$id_par = $this->input->post('id_par_nav');
		$id_app = $this->input->post('id_aplikasi');
		
		$simpan = array(
			'id_aplikasi' => $id_app,
			'ref' => 2,
			'tipe' => $this->input->post('tipe'),
			'kode' =>  $this->input->post('kode'),
			'judul' => $this->input->post('judul'),
			'fa' => $this->input->post('fa'),
			'link' => $this->input->post('link'),
			'separator' => $this->input->post('separator'),
			'aktif' => $this->input->post('aktif')
		);
		
		if (!empty($id_par)) $simpan['id_par_nav'] = $id_par;
		
		if (empty($id)) {
			
			$u = $this->general_model->datagrab(array(
				'tabel' => 'nav',
				'select' => 'max(urut) as urut_nav',
				'where' => array('id_aplikasi' => $id_app)
			))->row();
			
			$simpan['urut'] = !empty($u->urut_nav) ? $u->urut_nav : 1;
			
		} else {
			
			$this->urutkan($id_app,$id_par);
			
		}
		
		$par = $this->input->post('id_par_navi');
		if (!empty($par)) $simpan['id_par_navi'] = $par;

		$this->general_model->save_data('nav',$simpan,'id_nav',$id);
		$this->session->set_flashdata('ok', 'Modul Referensi berhasil disimpan');
		redirect('referensi/modref/show_navi/'.in_de(array('app' => $id_app)));
		
	}
	
	function urutkan($id, $par = null) {
		
		$where = array('id_aplikasi' => $id);
		
		$where =  (!empty($par)) ? array_merge_recursive($where,array('id_par_nav' => $par)) : array_merge_recursive($where,array('id_par_nav IS NULL' => null));
		
		$u = $this->general_model->datagrab(array(
				'tabel' => 'nav',
				'where' => $where,
				'order' => 'urut'
			));

		$no = 1;
		foreach($u->result() as $ur) {
			
			$this->general_model->save_data('nav',array('urut' => $no),'id_nav',$ur->id_nav);
			$no+=1;
			
		}
	
		
	}
	
	function removing($par){
	
		$o = un_de($par);
		$this->general_model->delete_data('nav','id_nav',$o['id']);
		$this->urutkan($app);
		$this->session->set_flashdata('ok', 'Modul Referensi berhasil dihapus');
		redirect('referensi/modref/show_navi/'.in_de(array('app' => $o['app'])));
		
	}
}