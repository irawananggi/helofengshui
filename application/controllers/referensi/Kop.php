<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  /**
  * ver 1.4
  */
  class Kop extends CI_Controller {
    var $debug=0;
    var $multiopd=1;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cmd');
        $defkop=array(
            'struct'=>'<table id="kopf" class="kof">
<tr>
<td width="100"><img src="&&logo&" alt="logo"  width="100"></td>
<td>
<table class="kof">
<tr><td><div id="xpmrt" class="trcenter">&&pemrt&</div></td></tr>
<tr><td><div id="xkab" class="trcenter">&&kab&</div></td></tr>
<tr><td><div id="xalamat" class="trcenter">&&alamat&</div></td></tr>
<tr><td><div id="xdesa" class="trcenter">&&desa&</div></td></tr>
</table>
</td>
</tr>
<tr><td colspan="2"><hr id="garis_bawah"></td></tr>
</table>',
             'css'=>"
#kopf{width:100%}
.kof {width: 100%;margin: 0;padding: 0}
.kof tr {padding: 0}
.kof tr td {padding: 0}
.trcenter{text-align:center;}
#xpmrt{
font-size: 32px;font-stretch: expanded;
font-family: Arial, Helvetica, sans-serif;
text-transform: uppercase;
font-weight: bolder;
margin: 0;
line-height: 1.0;
}
#xkab {font-size: 38px;font-stretch: expanded;
font-family: Arial, Helvetica, sans-serif;
text-transform: uppercase;
font-weight: bolder;
}

#xalamat {
font-size: 18px;font-stretch: expanded;
font-weight: bolder;
}
#xdesa {
font-family: Arial, Helvetica, sans-serif;
font-size: 28px;
font-stretch: expanded;line-height: 1.15;
}
#xgaris_bawah {}
",
        );
        $this->param= array(
            'title' => 'Kop Surat',
            'tabel' =>  array(
                'kop k'=>'',
                'ref_unit ru'=>array('ru.id_unit=k.id_unker','left'),
            ),
                        
            //'where'=>array('s.`statusnya`'=>1),
            "select"=>"k.*,ru.unit",
            'id' => 'id_kop',
            'tombol' => array(array('1','Tambah')),
            'can' => array('edit','delete'),//bisa hapus dan edit
            'kolom' =>      array('Nama','Pemerintahan','Kabupaten','Desa','Unit'),//header tabel
            'kolom_data' => array('nama','id_unker','logo','pemrt','kab','desa','alamat','struct','css'),//jangan di kasih nama tabel di depan nya
            'kolom_tampil' => array('nama','pemrt','kab','desa','unit'),//jangan di kasih nama tabel di depan nya
            'ret'=>array(
                'fold'=>uri(1),
                'def'=>uri(2),
                'func'=>'ret_prev',
            ),
            
            'form'=>array(
                array(1,1,'Nama','nama','',''),
                ($this->multiopd==1
                    ? array(49, !isrole(sesi('id_pegawai'),'ADMOPD'),
                        "<input type='hidden' name='id_unker' value='".sesi('id_unit')."' />" ,
                        false,'Unker','id_unker',uri(1).'/'.uri(2).'/autocomplete_unker','','unit')
                    : array(0,1,2,'id_unker',sesi('id_unit'))
                ),
                    
                array(7,0,'Logo','logo','','kop'),
                array(1,0,'Pemerintahan','pemrt','',''),
                array(1,0,'Kabupaten','kab','',''),
                array(1,0,'Desa','desa','',''),
                array(1,0,'Alamat','alamat','',''),
                
                array(50,'','<div id="sen" >','','','',''),
                array(53,
                    "<input type='hidden' name='struct' value='".htmlspecialchars($defkop['struct'])."' />" ,
                    0,'Struktur','struct','','-'),
                array(53,
                    "<input type='hidden' name='css' value='".($defkop['css'])."' />" ,
                    0,'Css','css','','-'),
                array(50,'','</div>','','','',''),
                array(50,'',"<input type='hidden' id=retview name='retview' value='0' />",'','','',''),
                
                array(50,'','',
                    "<p>".form_button('','Preview','id="prev" idkop="&&id_kop&" class="btn btn-primary"'
                        )
                        ."</p>".
                        "<p><div id='preview' class='box-body table-responsive no-padding'>".''
                     //$this->show(1,'view')
                     ."</div></p>",
                    '','','',''
                ),
                /*array(50,'',
                    "<p><div id='preview'>".''
                     //$this->show(1,'view')
                     ."</div></p>",
                    '','','',''
                ),*/
            ),
            'form_script'=>
                "
                var seng=$('#sen');
                if($('#form-box input[name=new]').val()==1){
                    //seng.css('display','block');
                }else{
                    seng.css('display','none');    
                }
                
                \$('#prev').click(function () {
                    $('#retview').val(1);
                    var ini=$('#form_data');
                    //alert();
                    console.log(ini.attr('action'));
                    var form = new FormData(ini[0]);
                    $.ajax({
                        url: ini.attr('action'),
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        data: form,
                        processData: false,
                        contentType: false,
                        success: function(msg) {
                            console.log('berhasil'+msg.data);
                            $('#preview').html(msg.data );
                            $('#retview').val(0);
                            //contentloader('".site_url( uri(1).'/'.uri(2).'/show')."' + '/'+$(this).attr('idkop'),'#preview');
                        },
                        error:function(x, t, m){
                            $('#retview').val(0);
                            $('#preview').html('Error '+ t );
                        }
                    });
                    
                    return false;
                });
                \$('#prev').dblclick(function () {
                    //alert('i was click twice');
                    //console.log(seng.css('display'));
                    if(seng.css('display') !== 'none'){
                        seng.css('display','none');
                    }else{
                        seng.css('display','block');
                    }
                    return false;
                });
                ",
        );
//        if(($this->multiopd==1)&&(!isrole(sesi('id_pegawai'),'ADMOPD')))
//            $this->param['where']=array('id_unker'=>sesi('id_unit'));            
    }
    
    function index(){
        redirect(uri(1).'/'.uri(2). '/daftar');
    }
    
    function daftar($search = null,$offset = null) {
        $this->benchmark->mark('awalz');
        //cek(sesi('id_unker'));
        $key = post('search');
        $modex = "list_data";
        $search_param = null;
        if (!empty($key)) {
            $search_param = $key;
            
        } else if (!empty($search)) {
            $o = un_de($search);
            $modex = !empty($o['modex'])?$o['modex']:"list_data";
            $form_id = @$o['form_id'];
            $search_param = !empty($o['search']) ? $o['search'] : null;
        } 
        $folder_app=$this->uri->segment(1);
        $param=$this->param;
        
        $param['inti']=uri(3);
        if(@$o['new'])$param['new']=1;
        
        if(is_array(@$param)){
            $param = array_merge_recursive(array(
                    'search' => $search_param,
                    'module' => $folder_app.'/'.$this->uri->segment(2).'/',
                    'page'  => uri(3),
                    'this'  => 'ref'
            ),$param);
                    
            $this->benchmark->mark('akhirz');
            if($this->debug) echo komen($this->benchmark->elapsed_time('awalz','akhirz'));
        
            $this->load->library('Defa');
            
            switch($modex) {
                case "list_data" : 
                    if(isset($param['redir'])){
                        #load_controller($param['redir_path'],$param['redir'],$param['redir_func'], $param['redir_param']); 
                        redirect($param['redir_path'].'/'.$param['redir'].'/'.$param['redir_func'].'/'. @$param['redir_param']);
                    }else{
                        if(!(in_array($offset ,array('excel','cetak')))){
                            $param['return_object']=1;
                            $data=$this->defa->list_data( $param, $offset); 
                            #$data=load_controller('dm','def','list_data', $param, $offset); 
                            #cek($this->db->last_query());
                            $data['content'] = "dm/standard_view";
                            $this->load->view('dm/home', $data);
                        }else{
                            $this->defa->list_data( $param, $offset); 
                            #load_controller('dm','def','list_data', $param, $offset); 
                        }
                    }
                    break;
                case "form_mode" : 
                    #load_controller('dm','def','form_data', $param, $form_id); 
                    if(@$o['ret_save'])$param['ret_save']=$o['ret_save'];
                    $this->defa->form_data( $param, $form_id); 
                    break;
                case "save_mode" : 
                    #load_controller('dm','def','save_data',$param); 
                    $this->defa->save_data($param); 
                    break;    
            }
        }
    
    }
    
    function show($id,$ret="",$id_unit=0){
        $wh=($id_unit>0?array('id_unker'=>$id_unit):array('id_kop'=>$id));
        if(
            ($dt=$this->general_model->datagrab(array(
                'tabel'=>'kop',
                'where'=>$wh,
               ))->result_array())&&(count($dt[0]))){
            $dt=$dt[0];
            #cek(count($dt));
            $dt['logo']=base_url('uploads/kop/'.$dt['logo']);
            #cek(array($dt,html_escape($dt['kop'])));
            $retn_v=replaceto($dt['struct'],$dt).tag_css($dt['css']);
            if($ret=='json'){
               die(json_encode(array('data'=>$retn_v)));
            }elseif($ret=='view'){
               return $retn_v;
            }else{
               echo $retn_v;
            }
       }else{
            return false;       
       }
    }
    
    function show_khusus($id_unit=0){
        $dt_struct='
            <table id="kopf" class="kof">
            <tr>
            <td width="90"><img src="&&logo&" alt="logo"  width="90"></td>
            <td>
            <table class="kof">
            <tr><td><div id="xpmrt" class="trcenter">&&pemrt&</div></td></tr>
            <tr><td><div id="xkab" class="trcenter">&&kab&</div></td></tr>
            </table>
            </td>
            </tr>
            <tr><td colspan="2"><hr id="garis_bawah"></td></tr>
            </table>';
        $dt_css=
            "#kopf{width:100%}
            .kof {width: 100%;margin: 0;padding: 0}
            .kof tr {padding: 0}
            .kof tr td {padding: 0}
            .trcenter{text-align:center;}
            #xpmrt{
            font-size: 34px;font-stretch: expanded;
            font-family: Arial, Helvetica, sans-serif;
            text-transform: uppercase;
            font-weight: bolder;
            margin: 0;
            line-height: 1.0;
            }
            #xkab {font-size: 27px;font-stretch: expanded;
            font-family: Arial, Helvetica, sans-serif;
            text-transform: uppercase;
            font-weight: bolder;
            }
            ";
        if(
            ($dt=$this->general_model->datagrab(array(
                'tabel'=>'kop',
                'where'=>array('id_kop'=>1),
               ))->result_array())&&(count($dt[0]))){
            $dt=$dt[0];
            #cek(count($dt));
            $dt['logo']=base_url('uploads/kop/'.$dt['logo']);
            $dt['struct']=$dt_struct;
            if($id_unit){
                $dt['kab']=$this->general_model->datagrabe(array(
                            'tabel'=>'ref_unit',
                            'where'=>array('id_unit'=>$id_unit),
                            'select'=>'unit',                            
                           ))->row('unit');
            }else{
                $dt['kab']=sesi('unit');
            }
            
            #cek(array($dt,html_escape($dt['kop'])));
            $retn_v=replaceto($dt['struct'],$dt).tag_css($dt_css);
            return $retn_v;            
       }else{
            return false;       
       }
    }
     
    function autocomplete_unker() {
        $q = $this->input->post('q');
        $q1 = $this->input->post('page');

        $res = $this->general_model->datagrab(array(
            'tabel' => 'ref_unit',
            'search' => array('unit' => $q),
            'select' => "id_unit as id, unit as data",
            'limit' => 10, 'offset' => 0
        ));
        die(json_encode($res->result()));

    }
     
  function ret_prev($o) {
      $op=dekrip($o,1,1);
      #cek($op);
      if(($op['id'])&&(@$op['o']['data']['retview']==1)){
        return $this->show($op['id'],'json');
      }else{
        redirect($op['o']['module'].$op['o']['page']);
      }
  }  

  /**
  * initial database
  */
  function db() {
      $tabel_db=array(
        'kop'=>array(
            'c'=>
              "`id_kop` int(2) NOT NULL AUTO_INCREMENT,
              `id_unker` int(11) DEFAULT '1',
              `nama` varchar(50) DEFAULT NULL,
              `logo` varchar(100) DEFAULT NULL,
              `pemrt` varchar(200) DEFAULT NULL,
              `kab` varchar(200) DEFAULT NULL,
              `desa` varchar(240) DEFAULT NULL,
              `alamat` varchar(200) DEFAULT NULL,
              `struct` text,
              `css` text,
              PRIMARY KEY (`id_kop`)",
        ),
      );
      
      $cmp_db=load_cnt('dm','Builder','do_compare_tables',array_keys($tabel_db),$tabel_db);
      
      cek($cmp_db);
  }
  
 
}
?>