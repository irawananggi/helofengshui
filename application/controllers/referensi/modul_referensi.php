<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modul_referensi extends CI_Controller {

	function __construct() {
	
		parent::__construct();
		
	}
	
	function navigasi() {
		
		$role = array(
		'Hak Data' => array(
			'ADM01' => 'Tambah',
			'ADM02' => 'Ubah/Sunting',
			'ADM03' => 'Hapus'
		),
		'Khusus' => array(
			'RF99' => array('Aktivitas Pengguna','referensi/log'),
			'RF99n' => 'divider',
			'RF01' => array('Aplikasi','referensi/aplikasi'),
			'RF02' => array('Parameter','referensi/pengaturan'),
			'RF03' => array('Operator','referensi/operator'),
			'RF04' => array('Kewenangan','referensi/kewenangan')),
		'Umum' => array(
			'RF05' => array('Pejabat Penetap','referensi/umum/ke/penetap'),
			'RF06' => array('Penandatangan','referensi/umum/ke/penandatangan'),
			'RF06r' => 'divider',
			//'RF07' => array('Kop Surat',
			'RF08' => array('Jenis Kelamin','referensi/umum/ke/jenis_kelamin'),
			'RF09' => array('Golongan Darah','referensi/umum/ke/golongan_darah'),
			'RF10' => array('Status Keluarga','referensi/umum/ke/keluarga_status'),
			'RF25' => array('Status Anak','referensi/umum/ke/status_anak'),
			'RF11' => array('Pekerjaan','referensi/umum/ke/pekerjaan'),
			'RF12' => array('Keluarga','referensi/umum/ke/keluarga'),
			'RF13' => array('Status Kawin','referensi/umum/ke/kawin_status'),
			'RF14' => array('Agama','referensi/umum/ke/agama'), //'RF15' => array('Gelar Akademik','referensi/umum/ke/gelar'),
			'RF27' => array('Jenjang Pendidikan','referensi/umum/ke/studi_jenjang'),
			'PF28' => array('Bentuk Pendidikan','referensi/umum/ke/studi_bentuk'),
			'RF15a' => 'divider',
			'RF16' => array('Provinsi','referensi/umum/ke/propinsi'),
			'RF17' => array('Kabupaten','referensi/umum/ke/kabupaten'),
			'RF18' => array('Kecamatan','referensi/umum/ke/kecamatan'),
			'RF19' => array('Kelurahan/Desa','referensi/umum/ke/kelurahan'), //'RF20' => array('Suku','referensi/umum/ke/suku'),
			
			'RF21' => array('Kementerian','referensi/umum/ke/kementerian'),
			'RF22' => array('Unit Kerja','referensi/unit'),
			'RF24' => array('Unit Organisasi','referensi/bidang'),
			'RF24a' => 'divider',
			'RF26' => array('Satuan','referensi/umum/ke/satuan')
			
		));
		
		$app_active = get_stationer(array('app_active'));
		$app =  $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('id_aplikasi IN ('.implode(",",$app_active).') AND aktif = 1' => null)));
		
		foreach($app->result() as $ap) {
			 $naveg = load_controller($ap->folder,'referensi_'.$ap->folder,'navigasi');
			
			 if (!empty($naveg)) {
				 $naveg = array($ap->nama_aplikasi => $naveg);
				 $role = array_merge_recursive($role,$naveg);
			 }
			 $app_kind = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('id_aplikasi IN ('.implode(',',$app_active).')' => null,'id_par_aplikasi' => $ap->id_aplikasi),'order' => 'urut'));
			 foreach($app_kind->result() as $aps) {
				$naveg_c = load_controller($aps->folder,'referensi_'.$aps->folder,'navigasi');
				if (!empty($naveg_c)) {
					$naveg_c = array($aps->nama_aplikasi => $naveg_c);
					$role = array_merge_recursive($role,$naveg_c);
				}
			 }
		}
		
		return $role;
		
	}
	
	
	
	

	
}
?>