<?php if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Pengaturan extends CI_Controller {
	
	function __construct() {
	
		parent::__construct();
		login_check($this->session->userdata('login_state'));

	}

	public function index() {
	
		$this->form_profil();
		
	}
	

	function get_app() {
		
		$app_active = $this->general_model->get_param('app_active');
		return $this->general_model->datagrab(array(
			'tabel' => 'ref_aplikasi',
			'where' => array('id_aplikasi IN ('.$app_active.') AND aktif = 1' => null)));

	}

	function form_profil() {
	
		$data['title'] = 'Pengaturan Parameter';
		$data['breadcrumb'] = array('' => 'Pengaturan', 'umum/profil' => 'Umum');
        if (!empty($this->dircut)) $data['dir_cut'] = $this->dircut;
        
		$app = $this->get_app();
		$tab = array();
		foreach($app->result() as $ap) {
		$tabulate = load_controller($ap->folder,'parameter_'.$ap->folder,'tab');
		
		if (!empty($tabulate)) {
			$tabulate = array_merge_recursive($tabulate,array('folder' => $ap->folder,'nama' => $ap->nama_aplikasi));
			$tab[] = $tabulate;
			}
		}
		
		$data['tab'] = $tab;
		$data['content'] = "umum/umum_view";
		$this->load->view('home', $data);
	}
	
	function save_setting() {
	
		$app = $this->get_app();
		$tab = array();

		foreach($app->result() as $ap) { load_controller($ap->folder,'parameter_'.$ap->folder,'save_data'); }
		
		$this->session->set_flashdata('ok','Penyimpanan pengaturan umum berhasil dilakukan');
		redirect('referensi/pengaturan');
		
	}
	
	// -- Pengaturan Aplikasi -- 
	
	function aplikasi() {
		
		$s = $this->session->userdata('login_state');
		if ($s == 'root') {
	
			$data['breadcrumb'] = array('' => 'Root', 'referensi/pengaturan/dasar' => 'Dasar', 'pengaturan/aplikasi' => 'Aplikasi');
				
			$offset = !empty($offset) ? $offset : null;
			
			$query = $this->general_model->datagrab(array('tabel'=>'ref_aplikasi','order' => 'urut','where' => array("id_par_aplikasi IS NULL" => null)));
			
			
			$this->table->set_template(array('table_open'=>'<table class="table table-striped table-bordered table-condensed table-nonfluid">'));
			$this->table->set_heading(array('data'=>'No','style'=>'width:20px;text-align:center'),'',array('data' => 'Nama/Kode Aplikasi','colspan' => 2),'Deskripsi',array('data' => 'Aksi','colspan' => 2));
		
			$no = 1;
			foreach($query->result() as $row) {
				$ext = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('id_par_aplikasi' => $row->id_aplikasi)));
				
				$path_ava = './assets/logo/'.$row->folder.'.png';
				$ava = (file_exists($path_ava)) ? base_url().'assets/logo/'.$row->folder.'.png' : base_url().'assets/logo/referensi.png';
				
				$rows = array(
					array('data'=>$no,'class'=>'text-center'),
					'<div class="app-icon" style="background: '.$row->warna.'"><img src="'.$ava.'"/></div>',					
					array('data' => $row->nama_aplikasi.' ('.$row->kode_aplikasi.')','colspan' => 2),
					$row->deskripsi);
					$rows[] = anchor('#','<i class="fa fa-pencil"></i>','class="btn-edit" act="'.site_url('referensi/aplikasi/form_data/'.in_de(array('id' => $row->id_aplikasi,'status' => 'root'))).'"');
					$rows[] = anchor('#','<i class="fa fa-trash"></i>','class="btn-delete" act="'.site_url('pengaturan/aplikasi/delete_aplikasi/'.$row->id_aplikasi).'" msg="Apakah Anda ingin menghapus data <b>'.$row->kode_aplikasi.'</b>?"');
					$rows[] = ($row->aktif == 1) ? anchor('referensi/pengaturan/saklar/off/'.$row->id_aplikasi,'<span class="badge bg-blue" style="width: 100%">Aktif</span>') : anchor('referensi/pengaturan/saklar/on/'.$row->id_aplikasi, '<span class="badge">Non Aktif</span>');
				
					$this->table->add_row($rows);
					
					foreach($ext->result() as $e) {
						
						$path_ava_e = './assets/logo/'.$e->folder.'.png';
						$ava_e = (file_exists($path_ava_e)) ? base_url().'assets/logo/'.$e->folder.'.png' : base_url().'assets/logo/referensi.png';
						
						$rowsd = array('','','<div class="app-icon" style="background: '.$row->warna.'"><img src="'.$ava.'" style="width: 26px;"/></div>',					
						$e->nama_aplikasi.' ('.$e->kode_aplikasi.')',$e->deskripsi);
						$rowsd[] = anchor('#','<i class="fa fa-pencil"></i>','class="btn-edit" act="'.site_url('referensi/aplikasi/form_data/'.in_de(array('id' => $e->id_aplikasi,'status' => 'root'))).'"');
						$rowsd[] = anchor('#','<i class="fa fa-trash"></i>','class="btn-delete" act="'.site_url('pengaturan/aplikasi/delete_aplikasi/'.$e->id_aplikasi).'" msg="Apakah Anda ingin menghapus data <b>'.$e->kode_aplikasi.'</b>?"');
						$rowsd[] = ($e->aktif == 1) ?  anchor('referensi/pengaturan/saklar/off/'.$e->id_aplikasi,'<span class="badge bg-blue">Aktif</span>') :  anchor('referensi/pengaturan/saklar/on/'.$e->id_aplikasi,'<span class="badge">Non Aktif</span>');
					
						$this->table->add_row($rowsd);
					}
					$no++;
				}
			$data['tabel'] = $this->table->generate();
			
			$data['tombol'] = anchor('#','<i class="icon-white icon-plus"></i> Tambah Aplikasi','class="btn-edit btn btn-success" act="'.site_url('referensi/aplikasi/form_data/'.in_de(array('status' => 'root'))).'"');
			
			$data['total'] = $query->num_rows();
			$data['title'] 		= 'Pengaturan Aplikasi';
			$data['content'] 	= "umum/standard_view";
			
		} else {
			$data['content'] 	= "umum/forbidden_view";
		}
		$this->load->view('home', $data);
	}
	
	function saklar($t,$id) {
		$simpan = ($t == 'on') ? array('aktif' => 1) : array('aktif' => 0);
		$this->general_model->save_data('ref_aplikasi',$simpan,'id_aplikasi',$id);
		
		$this->session->set_flashdata('ok','Status Aplikasi berhasil diubah');
		redirect('referensi/pengaturan/aplikasi');
	}
	
	function delete_aplikasi($id){

			$delete = $this->general_model->delete_data('ref_aplikasi','id_aplikasi',$id);
			
			if($delete) $this->session->set_flashdata('ok','Data berhasil dihapus');
			else $this->session->set_flashdata('ok','Data gagal dihapus');
			
			redirect('pengaturan/aplikasi');
	}

	function parameter() {

		$data['breadcrumb'] = array('' => 'Manajemen', 'referensi/pengaturan/pengaturan' => 'Pengaturan Parameter');

		$got = $this->general_model->get_param(array(
			'aplikasi',
			'aplikasi_code',
			'aplikasi_s',
			'aplikasi_logo',
			'aplikasi_logo_ext',
			'pemerintah',
			'pemerintah_s',
			'pemerintah_logo',
			'instansi',
			'instansi_s',
			'instansi_code',
			'copyright',
			'multi_unit',
			'demo',
			'main_color'),2);

		$dat = 
		'<link href="'.base_url().'assets/plugins/colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="'.base_url().'assets/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>
		<link href="'.base_url().'assets/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="'.base_url().'assets/plugins/iCheck/icheck.min.js"></script>';
		
		$dat .= form_open('referensi/pengaturan/save_aturan','id="form_save" enctype="multipart/form-data"').'<div class="row box-body">';
			$dat .= '<div class="col-lg-6">';
				$dat .= '<div class="alert text-center" style="min-height: 130px; background: '.$got['main_color'].'; color: #fff">';
				
				$path_logo = !empty($got['aplikasi_logo']) ? FCPATH.'logo/'.$got['aplikasi_logo'] : null;
				$logos = (file_exists($path_logo) and !empty($got['aplikasi_logo'])) ? base_url().'logo/'.$got['aplikasi_logo'] : base_url().'assets/logo/logo.png';
				
				$dat .= '<img src="'.$logos.'" style="width: 100px;">';
				
				$dat .= form_upload('logo_app');
				$dat .= '</div>';
				$dat .= '<p>';
				$dat .= form_hidden('param[]','aplikasi_logo').form_hidden('vale[]',!empty($got['aplikasi_logo'])?$got['aplikasi_logo']:null);
				$dat .= '<p><label>'.form_checkbox('reset_logo',1,FALSE,'class="flat-blue"').' &nbsp; Reset Logo</label></p>';
				$dat .=	form_label('Aplikasi').form_hidden('param[]','aplikasi');
				$dat .= form_input('vale[]',$got['aplikasi'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Aplikasi (Kode HTML)').form_hidden('param[]','aplikasi_code');
				$dat .= form_input('vale[]',$got['aplikasi_code'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Singkatan Aplikasi').form_hidden('param[]','aplikasi_s');
				$dat .= form_input('vale[]',$got['aplikasi_s'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>'.
					form_label('Multi Unit Kerja').
					form_hidden('param[]','multi_unit').
					form_dropdown('vale[]',array('' => ' -- Pilih -- ',1 => 'YA',2 => 'TIDAK'),!empty($got['multi_unit'])?$got['multi_unit']:null,'class="combo-box form-control" style="width: 100%"').'</p>';
				
				$c1 = (!empty($got['demo']) and $got['demo'] == 1) ? 'checked' : null;
				$c2 = (!empty($got['demo']) and $got['demo'] == 2) ? 'checked' : null;
				
				$dat .=
				'<p>'.
				form_label('Warna Dasar').form_hidden('param[]','main_color').'<div class="input-group colorize" >
				    <input name="vale[]" type="text" class="form-control" value="'.(!empty($got['main_color'])?$got['main_color']:null).'" />
				    <span class="input-group-addon"><i></i></span>
				</div>
				</p>
				<p>
				
				<label>Demo</label>
				<div class="form-group">
					<input type="hidden" name="param[]" value="demo"/>
                    <label>
                      <input type="radio" value="1" name="vale[]" class="flat-blue" '.$c1.' required/> Ya
                    </label> &nbsp; 
                    <label>
                      <input type="radio" value="2" name="vale[]" class="flat-blue" '.$c2.' required/> Tidak
                    </label>
				</div></p>
				';
				/*
				$dat .= '<p>';
				$dat .= form_label('Aplikasi Logo Ekternal<small> *Kosongkan bila tak dibutuhkan</small>').form_hidden('param[]','aplikasi_logo_ext');
				$dat .= form_input('vale[]',!empty($got['aplikasi_logo_ext'])?$got['aplikasi_logo_ext']:null,'class="form-control"');
				$dat .= '</p>';
				*/
			$dat .= '</div>';
			$dat .= '<div class="col-lg-6">';
				$dat .= '<div class="alert text-center" style="min-height: 130px; background: '.$got['main_color'].'; color: #fff">';
				
				$path_inst_logo = !empty($got['pemerintah_logo']) ? FCPATH.'logo/'.$got['pemerintah_logo'] : null;
				$logo_instansi = (file_exists($path_inst_logo) and !empty($got['pemerintah_logo'])) ? base_url().'logo/'.$got['pemerintah_logo'] : base_url().'assets/logo/brand.png';
				
				$dat .= '<img src="'.$logo_instansi.'" style="width: 100px; min-hight: 100px;">';
				$dat .= form_upload('logo_pemerintah');
				$dat .= '</div>';
				$dat .= '<p><label>'.form_checkbox('reset_logo_pemerintah',1,FALSE,'class="flat-blue"').' &nbsp; Reset Logo Pemerintah</label></p>';
				$dat .= '<p>';
				$dat .= form_hidden('param[]','pemerintah_logo').form_hidden('vale[]',!empty($got['pemerintah_logo'])?$got['pemerintah_logo']:null);
				$dat .= form_label('Naungan Instansi (Kepemerintahan)').form_hidden('param[]','pemerintah');
				$dat .= form_input('vale[]',$got['pemerintah'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Singkatan Naungan').form_hidden('param[]','pemerintah_s');
				$dat .= form_input('vale[]',$got['pemerintah_s'],'class="form-control"');
				$dat .= '</p>';
				/*
				$dat .= '<p>';
				
				$dat .= form_label('Naungan Logo Eksternal <small> *Kosongkan bila tak dibutuhkan</small>').form_hidden('param[]','pemerintah_logo_ext');
				$dat .= form_input('vale[]',!empty($got['pemerintah_logo_ext'])?$got['pemerintah_logo_ext']:null,'class="form-control"');
				$dat .= '</p>';
				*/
				$dat .= '<p>';
				$dat .= form_label('Instansi').form_hidden('param[]','instansi');
				$dat .= form_input('vale[]',$got['instansi'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Instansi (Kode HTML)').form_hidden('param[]','instansi_code');
				$dat .= form_input('vale[]',$got['instansi_code'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Singkatan Instansi').form_hidden('param[]','instansi_s');
				$dat .= form_input('vale[]',$got['instansi_s'],'class="form-control"');
				$dat .= '</p>';
				$dat .= '<p>';
				$dat .= form_label('Copyright').form_hidden('param[]','copyright');
				$dat .= form_input('vale[]',$got['copyright'],'class="form-control"');
				$dat .= '</p>';
			$dat .= '</div>
			<div class="col-lg-12 text-center">
				<button class="btn btn-success btn-save"><i class="fa fa-save"></i> &nbsp; Simpan Pengaturan</button>
				<button class="btn btn-danger btn-reset" act="'.site_url('referensi/pengaturan/reset_pengaturan').'"><i class="fa fa-cog"></i> &nbsp; Reset Pengaturan !</button>
			</div>
			
			';
		$dat .= '</div>'.form_close().'
		<script type="text/javascript">
			$(document).ready(function(){
				$(".btn-save").click(function() {
					$("#form_save").submit();
				});
				$(".colorize").colorpicker();
				$("select").select2();
				$("input[type=\'radio\'].flat-blue, input[type=\'checkbox\'].flat-blue").iCheck({
					checkboxClass: "icheckbox_minimal-blue",
					radioClass: "iradio_minimal-blue"
				});
				$(".btn-reset").click(function() {
				   $(".form-delete-msg").html("Apakah anda yakin mereset pengaturan?");
				   $(".form-title").html("Konfirmasi Reset Pengaturan");
				   $(".form-delete-url").attr("href",$(this).attr("act")).children().html("Reset !");
				   $("#modal-delete").modal("show");
				   return false;
				});
			});
		</script>';
		$data['tabel'] = $dat;
		
		$data['title'] 	 = '<i class="fa fa-cog"></i> &nbsp; Pengaturan Dasar';
		$data['content'] = "umum/standard_view";
		$this->load->view('home', $data);
	}
	
	function save_aturan() {
		
		$param = $this->input->post('param');
		$vale = $this->input->post('vale');
		$i = 0;
		$simpan = array();
		foreach ($param as $p) {
			if ($param[$i] == "aplikasi_logo") {
				if (!empty($_FILES['logo_app']['name'])) {
					$logo_app_lama = $this->general_model->datagrab(array('tabel'=>'parameter', 'select'=>'val', 'where'=>array('param'=>$param[$i])));
					
					$path_app_logo = FCPATH.'logo/'.$logo_app_lama->row('val');
					if (file_exists($path_app_logo)) unlink($path_app_logo);
					$path_appthumb_logo = FCPATH.'logo/thumbnails/'.$logo_app_lama->row('val');
					$delete_thumb = unlink($path_appthumb_logo);
					
					$config['upload_path'] = './logo/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size']	= '1000000';
					$config['max_width']  = '1024000';
					$config['max_height']  = '7680000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('logo_app')){
						$data['error'] = $this->upload->display_errors();
						echo $data['error'];
					} else {
						$data = $this->upload->data();
						$vale[$i] = $data['file_name'];
						$konfigurasi = array(
							'source_image'=>$data['full_path'],
							'new_image'=>'./logo/thumbnails/',
							'maintain_ration' => true,
							'width' => 400,
							'height' =>300
							);
						$this->load->library('image_lib', $konfigurasi);
						$this->image_lib->resize();
					}
					
				}
				
				if ($this->input->post('reset_logo')) {
					$logo_app_lama = $this->general_model->datagrab(array('tabel'=>'parameter', 'select'=>'val', 'where'=>array('param'=>$param[$i])));
					
					$path_app_logo = FCPATH.'logo/'.$logo_app_lama->row('val');
					if (file_exists($path_app_logo)) unlink($path_app_logo);
					$path_appthumb_logo = FCPATH.'logo/thumbnails/'.$logo_app_lama->row('val');
					$delete_thumb = unlink($path_appthumb_logo);
				}
			} else if ($param[$i] == "pemerintah_logo") {
				if (!empty($_FILES['logo_pemerintah']['name'])) {
					$logo_pemerintah_lama = $this->general_model->datagrab(array('tabel'=>'parameter', 'select'=>'val', 'where'=>array('param'=>$param[$i])));
					
					$path_pem_logo = FCPATH.'logo/'.$logo_pemerintah_lama->row('val');
					if (file_exists($path_pem_logo)) unlink($path_pem_logo);
					
					$path_pemthumb_logo = FCPATH.'logo/thumbnails/'.$logo_pemerintah_lama->row('val');
					if (file_exists($path_pemthumb_logo)) unlink($path_pemthumb_logo);
		
					$config['upload_path'] = './logo/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size']	= '1000000';
					$config['max_width']  = '1024000';
					$config['max_height']  = '7680000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('logo_pemerintah')){
						$data['error'] = $this->upload->display_errors();
						echo $data['error'];
					}else {
						$data = $this->upload->data();
						$vale[$i] = $data['file_name'];
						$konfigurasi = array(
							'source_image'=>$data['full_path'],
							'new_image'=>'./logo/thumbnails/',
							'maintain_ration' => true,
							'width' => 400,
							'height' =>300
							);
						$this->load->library('image_lib', $konfigurasi);
						$this->image_lib->resize();
					}
					
				}
				if ($this->input->post('reset_logo_pemerintah')) {
					echo "a";
					$logo_pemerintah_lama = $this->general_model->datagrab(array('tabel'=>'parameter', 'select'=>'val', 'where'=>array('param'=>$param[$i])));
					
					$path_pem_logo = FCPATH.'logo/'.$logo_pemerintah_lama->row('val');
					if (file_exists($path_pem_logo)) unlink($path_pem_logo);
					
					$path_pemthumb_logo = FCPATH.'logo/thumbnails/'.$logo_pemerintah_lama->row('val');
					if (file_exists($path_pemthumb_logo)) unlink($path_pemthumb_logo);
				}
			}
			
			$g = $this->general_model->datagrab(array('tabel' => 'parameter','where' => array('param' => $param[$i])));
			if ($g->num_rows() > 0) $this->general_model->save_data(array('tabel' => 'parameter','data' => array('val' => $vale[$i]),'where' => array('param' => $param[$i])));
			else $this->general_model->save_data(array('tabel' => 'parameter','data' => array('param' => $param[$i],'val' => $vale[$i])));

			$i+=1;
		}
		$this->session->set_flashdata('ok','Pengaturan berhasil disimpan');
		redirect('referensi/pengaturan/parameter');
	}

	function impor() {
		
		$data['breadcrumb'] = array('' => 'Manajemen', 'referensi/pengaturan/impor' => 'Impor Kepegawaian');

		$data['tabel'] = 
			form_open('referensi/pengaturan/impor_proses','id="form_impor"').
			'<p>'.form_label('URL Impor').form_textarea('tujuan','http://localhost/yes/simpegdiy/home/impor','class="form-control" style="height: 80px"').'</p>'.
			'<p>'.form_submit('btn_submit','Periksa','class="btn btn-success"').'</p>'.form_close();
		
		$data['title'] 	 = '<i class="fa fa-download"></i> &nbsp; Impor Kepegawaian';
		$data['content'] = "umum/standard_view";
		$this->load->view('home', $data);
		
	}
	
	function impor_proses($search = null,$offset = null) {
		
		//cek(un_de($search));
		
		$tuj = $this->input->post('tujuan');
		
		$se = array();
		
		if ($search) {
			$o = un_de($search);
			$se['tx'] = @$o['tx'];
			$se['imp'] = @$o['imp'];
			$se['off'] = $offset;
			$se['t'] = site_url('referensi/pengaturan/impor_proses/');
		} else {
			$se['imp'] = $tuj;
			$se['off'] = $offset;
			$se['t'] = site_url('referensi/pengaturan/impor_proses/');
		}
	
		
		$data['title'] 	 = '<i class="fa fa-download"></i> &nbsp; Impor Kepegawaian';
		$data['tujuan'] = $se['imp'];
		$data['offs'] = $se['off'];
		$data['se'] = in_de($se);
		
		$data['offset'] = !empty($offset) ? $offset : null;
		
		$data['content'] = "referensi/impor_view";
		$this->load->view('home', $data);
		
	}
	
	function importing() {
		
		$pilih = $this->input->post('pilih');
		$nip = $this->input->post('nip');
		$nip_lama = $this->input->post('nip_lama');
		$nama = $this->input->post('nama');
		$g_depan = $this->input->post('gelar_depan');
		$g_belakang = $this->input->post('gelar_belakang');
		$unit = $this->input->post('unit');
		$bidang = $this->input->post('bidang');
		$gol = $this->input->post('gol');
		$tmt_pangkat = $this->input->post('tmt_pangkat');
		$jabatan = $this->input->post('jabatan');
		$tmt_jab = $this->input->post('tmt_jab');
		$eselon = $this->input->post('eselon');
		$bup = $this->input->post('bup');
		
		$gnd = $this->input->post('id_jeniskelamin');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$tmpt = strtoupper($this->input->post('tempat_lahir'));
		$cpns_tmt = $this->input->post('tmt_cpns');
		$mkg_bulan = $this->input->post('mkg_bulan');
		$mkg_tahun = $this->input->post('mkg_tahun');
		$alamat = $this->input->post('alamat');
		$no_nik = $this->input->post('ktp');
		$no_npwp = $this->input->post('npwp');
		$agama = strtoupper($this->input->post('agama'));

		$total = 0;

		foreach($pilih as $p => $v) {
			
			// Pegawai
			
			$c_nip = $this->general_model->datagrab(array('tabel' => 'peg_pegawai','where' => array('nip' => $nip[$v]),'select' => 'count(nip) as jml,id_pegawai'))->row();
			if (empty($c_nip->jml)) {
				$total += 1;
				/* -- Tempat Lahir -- */
				
				if (!empty($tmpt)) {
					$c_tempat = $this->general_model->datagrab(array('tabel' => 'ref_lokasi','where' => array('lokasi' => $tmpt[$v]),'select' => 'count(id_lokasi) as jml,id_lokasi'))->row();
					if (empty($c_tempat->jml)) $id_tmpt = $this->general_model->save_data('ref_lokasi', array('lokasi' => $tmpt[$v]));
					else $id_tmpt = $c_tempat->id_lokasi;
				}
				
				if (!empty($agama[$v])) {
					/* -- Agama -- */
					$c_agama = $this->general_model->datagrab(array('tabel' => 'ref_agama','where' => array('agama' => $agama[$v]),'select' => 'count(id_agama) as jml,id_agama'))->row();
					if (empty($c_agama->jml)) $id_agama = $this->general_model->save_data('ref_agama', array('agama' => $agama[$v]));	
					else $id_agama = $c_agama->id_agama;
				}
				$nip_simpan = !empty($nip[$v]) ? $nip[$v] : $nip_lama[$v];
				
				$s_peg = array(
					'username' => $nip_simpan,
					'password' => md5($nip_simpan),//md5('qwerty'),
					'id_tipe_pegawai' => 1,
					'nip_lama' => $nip_lama[$v],
					'nip' => $nip[$v],
					'nama' => $nama[$v],
					'gelar_depan' => $g_depan[$v],
					'gelar_belakang' => $g_belakang[$v],
					'id_jeniskelamin' => !empty($gnd[$v])?$gnd[$v]:1,
					'id_tempat_lahir' => !empty($id_tmpt)?$id_tmpat:0,
					'id_agama' => !empty($id_agama)?$id_agama:0,
					'tanggal_lahir' => @$tgl_lahir[$v],
					'cpns_tmt' => @$cpns_tmt[$v],
					'mkg_bulan' => @$mkg_bulan[$v],
					'mkg_tahun' => @$mkg_tahun[$v],
					'no_nik' => @$no_nik[$v],
					'no_npwp' => @$no_npwp[$v]
				); $id_peg = $this->general_model->save_data('peg_pegawai',$s_peg);
			} else {
				$id_peg = $c_nip->id_pegawai;
			}
			
			// Unit
			
			$c_unit = $this->general_model->datagrab(array('tabel' => 'ref_unit','where' => array('unit' => $unit[$v]),'select' => 'count(id_unit) as jml,id_unit'))->row();
			if (empty($c_unit->jml)) {
				$id_unit = $this->general_model->save_data('ref_unit', array('unit' => $unit[$v],'level_unit' => '1'));
			} else {
				$id_unit = $c_unit->id_unit;
			}
			
			// Bidang
			
			$c_bid = $this->general_model->datagrab(array('tabel' => 'ref_bidang','where' => array('nama_bidang' => $bidang[$v],'id_unit' => $id_unit),'select' => 'count(id_bidang) as jml,id_bidang'))->row();
			
			$ur_bid = $this->general_model->datagrab(array('tabel' => 'ref_bidang','where' => array('id_unit' => $id_unit),'select' => 'MAX(urut) as urutan'))->row();
			
			if (empty($c_bid->jml)) {
				$s_bid = array(
					'id_unit' => $id_unit,
					'nama_bidang' => $bidang[$v],
					'level' => 1,
					'urut' => $ur_bid->urutan+1	
				);
				$id_bidang = $this->general_model->save_data('ref_bidang',$s_bid);
			} else {
				$id_bidang = $c_bid->id_bidang;
			}
			
			// Jabatan
			
			$c_jab = $this->general_model->datagrab(array('tabel' => 'ref_jabatan','where' => array('nama_jabatan' => $jabatan[$v]),'select' => 'count(id_jabatan) as jml,id_jabatan'))->row();
			if (empty($c_jab->jml)) {
				$s_jab = array(
					'id_eselon' => $eselon[$v],
					'id_jab_jenis' => (($eselon[$v] == '9') ? 2 : 1),
					'nama_jabatan' => $jabatan[$v],
					'stat_jab' => (($eselon[$v] == '9') ? '2' : '1'),
					'bup' => $bup[$v]	
				);
				$id_jab = $this->general_model->save_data('ref_jabatan',$s_jab);
			} else {
				$id_jab = $c_jab->id_jabatan;
			}
			
			/* -- Simpan Jabatan -- */
		
			$where_jab = array(
				'id_pegawai' => $id_peg,
				'id_jabatan' => $id_jab,
				'id_unit' => $id_unit,
				'id_bidang' => $id_bidang,
				'tmt_jabatan' => $tmt_jab[$v]);
			$cek_jab = $this->general_model->datagrab(array('tabel' => 'peg_jabatan','where' => $where_jab));
		
			if ($cek_jab->num_rows() == 0) {
			
			$this->general_model->save_data('peg_jabatan',array('status' => 0),'id_pegawai',$id_peg);
			
			$simpan_jabatan = array(
				'id_pegawai' => $id_peg,
				'id_jabatan' => $id_jab,
				'id_unit' => $id_unit,
				'id_bidang' => $id_bidang,
				'tmt_jabatan' => $tmt_jab[$v],
				'id_status_pegawai' => '2',
				'status' => '1'
			); $this->general_model->save_data('peg_jabatan',$simpan_jabatan);
			
			}
			
			/* -- Simpan Golru -- */
			
			$where_golru = array(
				'id_pegawai' => $id_peg,
				'id_golru_jenis' => '1',
				'id_golru' => $gol[$v],
				'tmt_pangkat' => $tmt_pangkat[$v]);
			$cek_golru = $this->general_model->datagrab(array('tabel' => 'peg_pangkat','where' => $where_golru));

			if ($cek_golru->num_rows() == 0) {
			
			$this->general_model->save_data('peg_pangkat',array('status' => 0),'id_pegawai',$id_peg);
			
			$simpan_golru = array(
				'id_pegawai' => $id_peg,
				'id_golru_jenis' => '1',
				'id_golru' => $gol[$v],
				'tmt_pangkat' => $tmt_pangkat[$v],
				'status' => '1'
			); $this->general_model->save_data('peg_pangkat',$simpan_golru);
			
			}
			
		}
	
		$tujuan = $this->input->post('tujuan');
		$se = $this->input->post('se');
		$offs = $this->input->post('offs');
		
		$this->session->set_flashdata('ok', $total. ' data pegawai berhasil diproses');
		
		redirect('referensi/pengaturan/impor_proses/'.$se.'/'.$offs);
		
		
	}
	
	function reset_pengaturan() {
		
		$param = array(
			'aplikasi',
			'aplikasi_code',
			'aplikasi_s',
			'aplikasi_logo_ext',
			'aplikasi_logo',
			'pemerintah',
			'pemerintah_s',
			'pemerintah_logo',
			'pemerintah_logo_ext',
			'ibukota',
			'instansi',
			'instansi_s',
			'instansi_code',
			'copyright',
			'multi_unit',
			'main_color');
			
		for($i = 0; $i < count($param); $i++) {
			$this->general_model->delete_data('parameter','param',$param[$i]);
		}
		
		/*-- Init Aplikasi --*/
		
		$appdata = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('aktif' => 1)));

		$app_active = array();
		foreach($appdata->result() as $res) {
			$path = './application/controllers/'.$res->folder;
			if(file_exists($path)) $app_active[] = $res->id_aplikasi;
		}
		
		$par = $this->general_model->get_param($param,1);	

		for($i = 0; $i < count($param); $i++) {
			if (!in_array($param[$i],$par)) {
			$conf = @$this->config->config[$param[$i]];
				if (!empty($conf)) {
					$simpan = array(
						'param' => $param[$i],
						'val' => $conf
					); $this->general_model->save_data('parameter',$simpan);

				}
			}
		}	

		$active = $this->general_model->get_param('app_active');
		
		if (empty($active)) $this->general_model->save_data('parameter',array('param' => 'app_active','val' => implode(',',$app_active)));
		else $this->general_model->save_data('parameter',array('val' => implode(',',$app_active)),'param','app_active');
		
		$this->session->set_flashdata('ok','Reset Pengaturan berhasil dilakukan');
		redirect('referensi/pengaturan/parameter');
		
	}
	

}