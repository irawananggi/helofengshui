<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* v:0.11
*/
class Db2 extends CI_Controller {
    var $ver='0.13';
    var $requdm='0.17';
    var $dt=array(
        'app'=>array(
            'nama_aplikasi' => 'Referensi',
            'folder' => 'referensi',
            'kode_aplikasi' => 'REF',
            'deskripsi' => 'Sistem Informasi Manajemen Referensi',
            'warna' => '#5BBA3C',
        ),
        'plus'=>array(
            'kewenangan'=>'Admin Referensi',
        ),
        'nav'=>array(
        ),
    );
    
    var $stabel=array(
    #persyaratan installasi yang membutuhkan tabel agar berjalan sempurna
    /*'required'=>array(
        'peg_pegawai',
        'present_rekap',
    ),*/
    #hapus ulang tabel berprefix ini khusus fasilitas force [support array]
    'clear_prefix'=>'ref_',
    #excluded clear prefix
    'exclude_clear_prefix'=>array(
      'ref_aplikasi',
      'ref_role',
      'ref_role_nav',
      'ref_role_unit',
      ),
    #hapus atau buat baru tabel ini dan isi nya jika ada
    'clear'=>array(
        'ref_agama',
        'ref_cuti',
        'ref_diklatpim',
        'ref_eselon',
        'ref_golru',
        'ref_golru_jenis',
        'ref_gol_darah',
        'ref_hukdis',
        'ref_hukdis_level',
        'ref_jabatan_jenis',
        'ref_jenis_kelamin',
        'ref_kartu',
        'ref_penghargaan',
        'ref_pensiun',
        'ref_status_anak',
        'ref_status_istri',
        'ref_status_kawin',
        'ref_status_keluarga',
        'ref_status_pegawai',
    ),
    #struktur tabel, ambil dari cmp
    'create'=>array(
        'ref_agama'=>array(
            'c'=>
              "`id_agama` int(1) NOT NULL AUTO_INCREMENT,
              `agama` varchar(50) NOT NULL,
              PRIMARY KEY (`id_agama`),
              UNIQUE KEY `agama` (`agama`)",
        ),
        'ref_akreditasi'=>array(
            'c'=>
              "`id_akreditasi` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `akreditasi` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id_akreditasi`)",
        ),
        'ref_anggaran'=>array(
            'c'=>
              "`id_anggaran_tipe` int(3) NOT NULL AUTO_INCREMENT,
              `kode_anggaran_tipe` varchar(50) NOT NULL,
              `anggaran_tipe` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_anggaran_tipe`)",
        ),
        'ref_anggaran_tipe'=>array(
            'c'=>
              "`id_anggaran_tipe` int(1) NOT NULL AUTO_INCREMENT,
              `kode_anggaran_tipe` varchar(15) DEFAULT NULL,
              `anggaran_tipe` varchar(50) NOT NULL,
              PRIMARY KEY (`id_anggaran_tipe`)",
        ),
        'ref_anjab'=>array(
            'c'=>
              "`id_anjab` int(11) NOT NULL AUTO_INCREMENT,
              `id_par_anjab` int(11) DEFAULT NULL,
              `analisis_jabatan` varchar(150) NOT NULL,
              `deskripsi` text,
              `is_child` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_anjab`)",
        ),
        'ref_arsip_ekstensi'=>array(
            'c'=>
              "`id_ekstensi` int(3) NOT NULL AUTO_INCREMENT,
              `ekstensi` varchar(80) NOT NULL DEFAULT '',
              `keterangan` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_ekstensi`)",
        ),
        'ref_arsip_jenis'=>array(
            'c'=>
              "`id_arsip_jenis` int(11) NOT NULL AUTO_INCREMENT,
              `id_aplikasi` int(1) NOT NULL,
              `kode_arsip_jenis` varchar(15) DEFAULT NULL,
              `arsip_jenis` varchar(50) NOT NULL,
              `direktori` varchar(50) NOT NULL,
              `id_par` int(11) DEFAULT NULL,
              `urut` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_arsip_jenis`),
              KEY `id_aplikasi` (`id_aplikasi`)",
        ),
        'ref_arsip_kolom'=>array(
            'c'=>
              "`id_arsip_kolom` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `id_arsip_jenis` int(11) DEFAULT NULL,
              `kolom` varchar(128) DEFAULT NULL,
              `tipe` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_arsip_kolom`)",
        ),
        'ref_aset_lain'=>array(
            'c'=>
              "`id_asetlain` int(11) NOT NULL AUTO_INCREMENT,
              `jenis_aset_lain` varchar(200) DEFAULT NULL,
              PRIMARY KEY (`id_asetlain`)",
        ),
        'ref_bagian'=>array(
            'c'=>
              "`id_bag` int(11) NOT NULL AUTO_INCREMENT,
              `id_lev` int(11) NOT NULL COMMENT 'ref_lev',
              `id_bidang` int(11) DEFAULT NULL COMMENT 'ref_bidang',
              `aturan` text,
              `urutan` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_bag`),
              KEY `id_lev` (`id_lev`),
              KEY `id_bidang` (`id_bidang`)",
        ),
        'ref_barang'=>array(
            'c'=>
              "`id_barang` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `id_tipe_barang` int(11) DEFAULT NULL,
              `id_jenis_barang` int(11) DEFAULT NULL,
              `nama_barang` varchar(128) DEFAULT NULL,
              PRIMARY KEY (`id_barang`)",
        ),
        'ref_barang_aset'=>array(
            'c'=>
              "`id_brg` int(11) NOT NULL AUTO_INCREMENT,
              `id_brg_parent` int(11) DEFAULT NULL,
              `id_brg_bind` int(11) DEFAULT NULL,
              `kode_barang` varchar(50) NOT NULL,
              `nama_barang` varchar(200) NOT NULL,
              `jenis` char(1) NOT NULL,
              `is_child` char(1) DEFAULT NULL,
              `level` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_brg`)",
        ),
        'ref_beasiswa'=>array(
            'c'=>
              "`id_beasiswa` int(3) NOT NULL AUTO_INCREMENT,
              `kode_beasiswa` varchar(15) DEFAULT NULL,
              `beasiswa` varchar(50) NOT NULL,
              PRIMARY KEY (`id_beasiswa`)",
        ),
        'ref_bentuk_pendidikan'=>array(
            'c'=>
              "`id_bentuk_pendidikan` int(11) NOT NULL AUTO_INCREMENT,
              `id_jenjang` int(1) DEFAULT NULL,
              `id_kementerian` int(3) DEFAULT NULL,
              `kode_bentuk_pendidikan` varchar(20) DEFAULT NULL,
              `bentuk_pendidikan` varchar(80) NOT NULL,
              `singkatan_pendidikan` varchar(20) DEFAULT NULL,
              `kode_bkn` varchar(15) DEFAULT NULL,
              PRIMARY KEY (`id_bentuk_pendidikan`),
              KEY `id_jenjang` (`id_jenjang`,`id_kementerian`),
              KEY `id_kementerian` (`id_kementerian`)",
        ),
        'ref_bidang'=>array(
            'c'=>
              "`id_bidang` int(11) NOT NULL AUTO_INCREMENT,
              `id_par_bidang` int(11) DEFAULT NULL,
              `id_unit` int(11) unsigned NOT NULL,
              `penetapan` date DEFAULT NULL,
              `kode_bidang` varchar(20) DEFAULT NULL,
              `nama_bidang` varchar(255) NOT NULL,
              `urut` int(1) NOT NULL,
              `level` int(1) DEFAULT NULL,
              `pos_x` int(3) DEFAULT NULL,
              `pos_y` int(3) DEFAULT NULL,
              `status` int(1) DEFAULT NULL,
              `aktif` int(1) DEFAULT '1',
              PRIMARY KEY (`id_bidang`),
              KEY `id_par_bidang` (`id_par_bidang`,`id_unit`),
              KEY `id_subunit` (`id_unit`),
              KEY `urut` (`urut`)",
        ),
        'ref_bidang_kait'=>array(
            'c'=>
              "`id_bidang_kait` int(11) NOT NULL AUTO_INCREMENT,
              `jenis` int(1) NOT NULL,
              `id_bidang` int(11) NOT NULL,
              `id_kait` int(11) NOT NULL,
              PRIMARY KEY (`id_bidang_kait`),
              KEY `id_bidang` (`id_bidang`,`id_kait`)",
        ),
        'ref_bidang_studi'=>array(
            'c'=>
              "`id_bidang_studi` int(11) NOT NULL AUTO_INCREMENT,
              `bidang_studi` varchar(80) NOT NULL,
              PRIMARY KEY (`id_bidang_studi`)",
        ),
        'ref_bidang_urusan'=>array(
            'c'=>
              "`id_bidang_urusan` int(3) NOT NULL AUTO_INCREMENT,
              `id_urusan` int(3) NOT NULL,
              `kode_bidang_urusan` varchar(15) DEFAULT NULL,
              `bidang_urusan` varchar(50) NOT NULL,
              PRIMARY KEY (`id_bidang_urusan`),
              KEY `id_urusan` (`id_urusan`)",
        ),
        'ref_bso'=>array(
            'c'=>
              "`id_box` int(11) NOT NULL AUTO_INCREMENT,
              `id_box_par` int(11) DEFAULT NULL,
              `id_subunit` int(11) NOT NULL,
              `id_eselon` int(5) NOT NULL,
              `tipe` varchar(10) NOT NULL DEFAULT '',
              `posisi_x` int(3) DEFAULT NULL,
              `posisi_y` int(3) DEFAULT NULL,
              `judul` varchar(250) DEFAULT NULL,
              `addon` int(3) DEFAULT NULL,
              PRIMARY KEY (`id_box`)",
        ),
        'ref_cuti'=>array(
            'c'=>
              "`id_cuti` int(1) NOT NULL AUTO_INCREMENT,
              `cuti` varchar(100) NOT NULL,
              `status_cuti` int(1) NOT NULL,
              PRIMARY KEY (`id_cuti`),
              UNIQUE KEY `cuti` (`cuti`)",
        ),
        'ref_diklatpim'=>array(
            'c'=>
              "`id_diklatpim` int(3) NOT NULL AUTO_INCREMENT,
              `diklatpim` varchar(80) NOT NULL,
              `keterangan` varchar(200) DEFAULT NULL,
              PRIMARY KEY (`id_diklatpim`),
              UNIQUE KEY `diklatpim` (`diklatpim`)",
        ),
        'ref_diklatteknis'=>array(
            'c'=>
              "`id_diklatteknis` int(11) NOT NULL AUTO_INCREMENT,
              `kode_diklat` varchar(20) NOT NULL,
              `nama_diklat` varchar(200) NOT NULL,
              PRIMARY KEY (`id_diklatteknis`)",
        ),
        'ref_dpa'=>array(
            'c'=>
              "`id_dpa` int(11) NOT NULL AUTO_INCREMENT,
              `no_dpa` varchar(75) NOT NULL,
              `id_urusan` int(11) DEFAULT NULL,
              `id_bidang_urusan` int(11) DEFAULT NULL,
              `id_unit` int(11) NOT NULL,
              `id_bidang` int(11) NOT NULL,
              `id_program` int(11) NOT NULL,
              `id_kegiatan` int(11) NOT NULL,
              `id_rek` int(11) NOT NULL,
              `id_anggaran_tipe` int(1) NOT NULL,
              `pa` int(11) DEFAULT NULL,
              `bp` int(11) DEFAULT NULL,
              `pptk` int(11) NOT NULL,
              `penetapan` date DEFAULT NULL,
              `lokasi` varchar(100) DEFAULT NULL,
              `sumber_dana` varchar(100) DEFAULT NULL,
              `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `op` int(11) NOT NULL,
              `status` int(1) NOT NULL,
              PRIMARY KEY (`id_dpa`),
              KEY `id_unit` (`id_unit`),
              KEY `id_bidang` (`id_bidang`),
              KEY `id_program` (`id_program`),
              KEY `id_kegiatan` (`id_kegiatan`),
              KEY `id_rek` (`id_rek`),
              KEY `id_anggaran_tipe` (`id_anggaran_tipe`),
              KEY `pptk` (`pptk`),
              KEY `op` (`op`)",
        ),
        'ref_dpa_rekening'=>array(
            'c'=>
              "`id_rek_dpa` int(11) NOT NULL AUTO_INCREMENT,
              `id_dpa` int(11) NOT NULL,
              `id_rek` int(11) NOT NULL,
              `total` decimal(25,2) DEFAULT NULL,
              `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `op` int(11) NOT NULL,
              PRIMARY KEY (`id_rek_dpa`)",
        ),
        'ref_eselon'=>array(
            'c'=>
              "`id_eselon` int(1) NOT NULL AUTO_INCREMENT,
              `kode_bkn` varchar(20) DEFAULT NULL,
              `eselon` varchar(20) NOT NULL,
              `gol_terendah` int(1) DEFAULT NULL,
              `gol_tertinggi` int(1) DEFAULT NULL,
              `urut` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_eselon`),
              UNIQUE KEY `eselon` (`eselon`)",
        ),
        'ref_faktor_jabatan'=>array(
            'c'=>
              "`id_faktor` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `id_jab_jenis` int(11) DEFAULT NULL,
              `faktor_jabatan` varchar(250) DEFAULT NULL,
              PRIMARY KEY (`id_faktor`)",
        ),
        'ref_fakultas'=>array(
            'c'=>
              "`id_fakultas` int(11) NOT NULL AUTO_INCREMENT,
              `kode_fakultas` varchar(15) DEFAULT NULL,
              `fakultas` varchar(50) NOT NULL,
              PRIMARY KEY (`id_fakultas`),
              UNIQUE KEY `fakultas` (`fakultas`),
              UNIQUE KEY `kode_fakultas` (`kode_fakultas`)",
        ),
        'ref_gaji'=>array(
            'c'=>
              "`id_gaji` int(11) NOT NULL AUTO_INCREMENT,
              `id_golru` int(11) NOT NULL,
              `mkg` int(1) NOT NULL,
              `gaji` decimal(20,2) NOT NULL,
              `tahun` char(4) NOT NULL,
              PRIMARY KEY (`id_gaji`),
              KEY `id_golru` (`id_golru`)",
        ),
        'ref_gelar'=>array(
            'c'=>
              "`id_gelar` int(11) NOT NULL AUTO_INCREMENT,
              `gelar` varchar(20) NOT NULL,
              PRIMARY KEY (`id_gelar`),
              UNIQUE KEY `gelar` (`gelar`)",
        ),
        'ref_gol_darah'=>array(
            'c'=>
              "`id_gol_darah` int(1) NOT NULL AUTO_INCREMENT,
              `gol_darah` varchar(10) NOT NULL,
              PRIMARY KEY (`id_gol_darah`),
              UNIQUE KEY `gol_darah` (`gol_darah`)",
        ),
        'ref_golru'=>array(
            'c'=>
              "`id_golru` int(1) NOT NULL AUTO_INCREMENT,
              `id_golru_jenis` int(1) NOT NULL,
              `golongan` varchar(20) NOT NULL,
              `pangkat` varchar(50) NOT NULL,
              `urut` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_golru`),
              KEY `id_golru_jenis` (`id_golru_jenis`)",
        ),
        'ref_golru_jenis'=>array(
            'c'=>
              "`id_golru_jenis` int(1) NOT NULL AUTO_INCREMENT,
              `kode` varchar(15) DEFAULT NULL,
              `golru_jenis` varchar(50) NOT NULL,
              PRIMARY KEY (`id_golru_jenis`)",
        ),
        'ref_golru_pendidikan'=>array(
            'c'=>
              "`id_gol_pend` int(11) NOT NULL AUTO_INCREMENT,
              `id_bentuk_pendidikan` int(11) NOT NULL,
              `gol_puncak` int(1) DEFAULT NULL,
              `gol_dasar` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_gol_pend`)",
        ),
        'ref_guru_jenis'=>array(
            'c'=>
              "`id_guru_jenis` int(3) NOT NULL AUTO_INCREMENT,
              `id_bidang_studi` int(3) NOT NULL,
              `guru_jenis` varchar(80) NOT NULL,
              PRIMARY KEY (`id_guru_jenis`),
              KEY `id_bidang_studi` (`id_bidang_studi`)",
        ),
        'ref_hukdis'=>array(
            'c'=>
              "`id_hukdis` int(3) NOT NULL AUTO_INCREMENT,
              `id_hukdis_level` int(1) NOT NULL,
              `hukdis` varchar(80) NOT NULL,
              `kondisi` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_hukdis`),
              KEY `id_hukdis_level` (`id_hukdis_level`)",
        ),
        'ref_hukdis_level'=>array(
            'c'=>
              "`id_hukdis_level` int(1) NOT NULL AUTO_INCREMENT,
              `hukdis_level` varchar(30) NOT NULL,
              PRIMARY KEY (`id_hukdis_level`)",
        ),
        'ref_iso'=>array(
            'c'=>
              "`id_iso` int(11) NOT NULL AUTO_INCREMENT,
              `iso` varchar(150) NOT NULL,
              PRIMARY KEY (`id_iso`)",
        ),
        'ref_jabatan'=>array(
            'c'=>
              "`id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
              `id_jab_jenis` int(1) NOT NULL,
              `id_eselon` int(1) NOT NULL,
              `nama_jabatan` varchar(200) NOT NULL,
              `bup` int(1) NOT NULL,
              `stat_jab` int(1) NOT NULL,
              `id_bidang` int(11) DEFAULT NULL,
              `aktif` int(1) DEFAULT '1',
              PRIMARY KEY (`id_jabatan`),
              KEY `id_jab_jenis` (`id_jab_jenis`,`id_eselon`),
              KEY `id_eselon` (`id_eselon`)",
        ),
        'ref_jabatan_baru'=>array(
            'c'=>
              "`id_jabatan_baru` int(11) NOT NULL AUTO_INCREMENT,
              `id_jabatan` int(11) DEFAULT NULL,
              `id_subid` int(11) DEFAULT NULL,
              `nama_jabatan` varbinary(250) DEFAULT NULL,
              PRIMARY KEY (`id_jabatan_baru`)",
        ),
        'ref_jabatan_jenis'=>array(
            'c'=>
              "`id_jab_jenis` int(1) NOT NULL AUTO_INCREMENT,
              `jenis_jabatan` varchar(50) NOT NULL,
              PRIMARY KEY (`id_jab_jenis`)",
        ),
        'ref_jenis_barang'=>array(
            'c'=>
              "`id_jenis_barang` int(11) NOT NULL AUTO_INCREMENT,
              `kode_jenis_barang` varchar(15) NOT NULL,
              `jenis_barang` varchar(128) NOT NULL,
              PRIMARY KEY (`id_jenis_barang`)",
        ),
        'ref_jenis_file_upload'=>array(
            'c'=>
              "`id_ref` int(5) NOT NULL AUTO_INCREMENT,
              `jenis_arsip` varchar(50) NOT NULL,
              `keterangan` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id_ref`)",
        ),
        'ref_jenis_kelamin'=>array(
            'c'=>
              "`id_jeniskelamin` int(1) NOT NULL AUTO_INCREMENT,
              `jenis_kelamin` varchar(20) NOT NULL,
              PRIMARY KEY (`id_jeniskelamin`)",
        ),
        'ref_jenis_sertifikasi'=>array(
            'c'=>
              "`id_jenis_sertifikasi` int(3) NOT NULL AUTO_INCREMENT,
              `jenis_sertifikasi` varchar(30) NOT NULL,
              PRIMARY KEY (`id_jenis_sertifikasi`)",
        ),
        'ref_jenjang'=>array(
            'c'=>
              "`id_jenjang` int(1) NOT NULL AUTO_INCREMENT,
              `kode_jenjang` varchar(20) DEFAULT NULL,
              `jenjang` varchar(80) NOT NULL,
              `singkatan_jenjang` varchar(20) DEFAULT NULL,
              PRIMARY KEY (`id_jenjang`)",
        ),
        'ref_jkel'=>array(
            'c'=>
              "`id_jenis_kelamin` int(1) NOT NULL AUTO_INCREMENT,
              `jenis_kelamin` varchar(10) NOT NULL,
              PRIMARY KEY (`id_jenis_kelamin`)",
        ),
        'ref_jurusan'=>array(
            'c'=>
              "`id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
              `kode_jurusan` varchar(15) DEFAULT NULL,
              `jurusan` varchar(50) NOT NULL,
              `kode_bkn` varchar(15) DEFAULT NULL,
              `id_pendidikan` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_jurusan`),
              UNIQUE KEY `jurusan` (`jurusan`),
              UNIQUE KEY `kode_jurusan` (`kode_jurusan`)",
        ),
        'ref_kabupaten'=>array(
            'c'=>
              "`id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
              `id_propinsi` int(11) NOT NULL,
              `jenis` int(1) DEFAULT NULL,
              `kode_kabupaten` varchar(20) DEFAULT NULL,
              `kabupaten` varchar(150) NOT NULL,
              `luas` varchar(20) DEFAULT NULL,
              `dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id_kabupaten`),
              KEY `id_propinsi` (`id_propinsi`)",
        ),
        'ref_kartu'=>array(
            'c'=>
              "`id_kartu` int(3) NOT NULL AUTO_INCREMENT,
              `kode_kartu` varchar(15) DEFAULT NULL,
              `kartu` varchar(50) NOT NULL,
              `keterangan` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_kartu`)",
        ),
        'ref_kecamatan'=>array(
            'c'=>
              "`id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
              `id_kabupaten` int(11) NOT NULL,
              `kode_kecamatan` varchar(20) NOT NULL,
              `kecamatan` varchar(150) NOT NULL,
              `dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id_kecamatan`),
              KEY `id_kabupaten` (`id_kabupaten`)",
        ),
        'ref_keluarga'=>array(
            'c'=>
              "`id_keluarga` int(1) NOT NULL AUTO_INCREMENT,
              `kode_keluarga` varchar(20) DEFAULT NULL,
              `keluarga` varchar(80) NOT NULL,
              PRIMARY KEY (`id_keluarga`)",
        ),
        'ref_kelurahan'=>array(
            'c'=>
              "`id_kelurahan` int(11) NOT NULL AUTO_INCREMENT,
              `id_kecamatan` int(11) NOT NULL,
              `jenis` int(1) DEFAULT NULL COMMENT '1=kelurahan,2=desa',
              `kode_kelurahan` varchar(20) DEFAULT NULL,
              `kelurahan` varchar(150) NOT NULL,
              `kodepos` varchar(10) DEFAULT NULL,
              `dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id_kelurahan`)",
        ),
        'ref_kementerian'=>array(
            'c'=>
              "`id_kementerian` int(3) NOT NULL AUTO_INCREMENT,
              `kode_kementerian` varchar(20) NOT NULL,
              `kementerian` varchar(80) NOT NULL,
              `singkatan` varchar(20) DEFAULT NULL,
              PRIMARY KEY (`id_kementerian`)",
        ),
        'ref_kepemilikan'=>array(
            'c'=>
              "`id_kepemilikan` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `id_status_kepemilikan` int(1) DEFAULT NULL,
              `kepemilikan` varchar(128) DEFAULT NULL,
              `milik` varchar(128) DEFAULT NULL,
              `keterangan` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_kepemilikan`)",
        ),
        'ref_kesejahteraan'=>array(
            'c'=>
              "`id_kesejahteraan` int(3) NOT NULL AUTO_INCREMENT,
              `kode_kesejahteraan` varchar(15) DEFAULT NULL,
              `kesejahteraan` varchar(50) NOT NULL,
              PRIMARY KEY (`id_kesejahteraan`)",
        ),
        'ref_kib'=>array(
            'c'=>
              "`id_kib` int(1) NOT NULL AUTO_INCREMENT,
              `kode_kib` varchar(20) NOT NULL,
              `nama_kib` varchar(50) NOT NULL,
              PRIMARY KEY (`id_kib`)",
        ),
        'ref_kode_posisi'=>array(
            'c'=>
              "`id_p` int(11) NOT NULL AUTO_INCREMENT,
              `nama_pos` varchar(30) NOT NULL,
              `ket` varchar(200) DEFAULT NULL,
              PRIMARY KEY (`id_p`),
              UNIQUE KEY `nama_pos` (`nama_pos`)",
        ),
        'ref_kode_surat'=>array(
            'c'=>
              "`id` int(8) NOT NULL AUTO_INCREMENT,
              `kode` varchar(10) NOT NULL,
              `keterangan` varchar(100) NOT NULL,
              PRIMARY KEY (`id`)",
        ),
        'ref_kompetensi'=>array(
            'c'=>
              "`id_kompetensi` int(3) NOT NULL AUTO_INCREMENT,
              `kode_ujian_kompetensi` varchar(64) DEFAULT NULL,
              `ujian_kompetensi` varchar(255) NOT NULL,
              PRIMARY KEY (`id_kompetensi`)",
        ),
        'ref_kontrak'=>array(
            'c'=>
              "`id_kontrak` int(3) NOT NULL AUTO_INCREMENT,
              `kode_kontrak` varchar(15) DEFAULT NULL,
              `nama_kontrak` varchar(120) DEFAULT NULL,
              PRIMARY KEY (`id_kontrak`)",
        ),
        'ref_kontrak_jabatan'=>array(
            'c'=>
              "`id_kontrak_jabatan` int(3) NOT NULL AUTO_INCREMENT,
              `kode_jabatan` varchar(15) DEFAULT NULL,
              `nama_jabatan` varchar(120) DEFAULT NULL,
              `jenis` int(1) NOT NULL,
              PRIMARY KEY (`id_kontrak_jabatan`)",
        ),
        'ref_kontrak_periode'=>array(
            'c'=>
              "`id_kontrak_periode` int(3) NOT NULL AUTO_INCREMENT,
              `kontrak_periode` varchar(120) DEFAULT NULL,
              PRIMARY KEY (`id_kontrak_periode`)",
        ),
        'ref_kop'=>array(
            'c'=>
              "`id_kop` int(11) NOT NULL,
              `instansi` varchar(80) NOT NULL,
              `alamat` varchar(200) DEFAULT NULL,
              `telepon` varchar(150) DEFAULT NULL,
              `logo` varchar(100) DEFAULT NULL,
              `kepala` int(11) NOT NULL,
              `tempat` varchar(50) DEFAULT NULL,
              `nama_kabupaten` varchar(200) DEFAULT NULL,
              `email` varchar(150) DEFAULT NULL,
              PRIMARY KEY (`id_kop`),
              KEY `kepala` (`kepala`)",
        ),
        'ref_kriteria_penilaian'=>array(
            'c'=>
              "`id_kriteria` int(11) NOT NULL AUTO_INCREMENT,
              `id_par_kriteria` int(11) DEFAULT NULL,
              `kriteria` varchar(200) NOT NULL,
              PRIMARY KEY (`id_kriteria`)",
        ),
        'ref_ktj'=>array(
            'c'=>
              "`id_ktj` int(11) NOT NULL AUTO_INCREMENT,
              `id_eselon` int(1) DEFAULT NULL,
              `kode` varchar(20) DEFAULT NULL,
              `nama_ktj` varchar(200) NOT NULL,
              `bobot` float DEFAULT NULL,
              PRIMARY KEY (`id_ktj`)",
        ),
        'ref_lemari'=>array(
            'c'=>
              "`id_lemari` int(11) NOT NULL AUTO_INCREMENT,
              `kode` varchar(20) NOT NULL,
              `deskripsi` varchar(100) DEFAULT NULL,
              PRIMARY KEY (`id_lemari`)",
        ),
        'ref_lembaga'=>array(
            'c'=>
              "`id_lembaga` int(11) NOT NULL AUTO_INCREMENT,
              `kode_lembaga` varchar(20) DEFAULT NULL,
              `lembaga_pendidikan` varchar(80) NOT NULL,
              `lembaga` varchar(255) DEFAULT NULL,
              `predikat` varchar(50) DEFAULT NULL,
              `lokasi` varchar(100) DEFAULT NULL,
              PRIMARY KEY (`id_lembaga`)",
        ),
        'ref_lev'=>array(
            'c'=>
              "`id_level` int(11) NOT NULL AUTO_INCREMENT,
              `nama_lev` varchar(50) NOT NULL,
              `admin` int(1) DEFAULT '0',
              PRIMARY KEY (`id_level`)",
        ),
        'ref_level_bid'=>array(
            'c'=>
              "`id_lev_bid` int(1) NOT NULL,
              `nama` varchar(20) NOT NULL,
              PRIMARY KEY (`id_lev_bid`)",
        ),
        'ref_lokasi'=>array(
            'c'=>
              "`id_lokasi` int(11) NOT NULL AUTO_INCREMENT,
              `lokasi` varchar(150) NOT NULL,
              PRIMARY KEY (`id_lokasi`)",
        ),
        'ref_lokasi_aset'=>array(
            'c'=>
              "`id_lokasi` int(11) NOT NULL AUTO_INCREMENT,
              `id_unit` int(11) NOT NULL,
              `kode_skpd` varchar(20) NOT NULL,
              `kode_unker` varchar(10) NOT NULL,
              `kode_ruang` varchar(10) NOT NULL,
              `nama_unker` varchar(200) NOT NULL,
              `nama_ruang` varchar(200) NOT NULL,
              PRIMARY KEY (`id_lokasi`)",
        ),
        'ref_negeri_swasta'=>array(
            'c'=>
              "`id_ref` int(1) NOT NULL AUTO_INCREMENT,
              `ket` varchar(20) NOT NULL,
              PRIMARY KEY (`id_ref`)",
        ),
        'ref_pekerjaan'=>array(
            'c'=>
              "`id_pekerjaan` int(11) NOT NULL AUTO_INCREMENT,
              `kode_pekerjaan` varchar(20) DEFAULT NULL,
              `pekerjaan` varchar(80) NOT NULL,
              `nama_pekerjaan` varchar(50) NOT NULL,
              PRIMARY KEY (`id_pekerjaan`)",
        ),
        'ref_pemerintah_adat'=>array(
            'c'=>
              "`id_pemerintah_adat` int(11) NOT NULL AUTO_INCREMENT,
              `id_wilayah_adat` int(11) NOT NULL,
              `pemerintah_adat` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_pemerintah_adat`)",
        ),
        'ref_pemilik_aset'=>array(
            'c'=>
              "`id_pemilik` int(11) NOT NULL AUTO_INCREMENT,
              `kode_pemilik` varchar(6) NOT NULL,
              `pemilik` varchar(55) NOT NULL,
              PRIMARY KEY (`id_pemilik`)",
        ),
        'ref_penandatangan'=>array(
            'c'=>
              "`id_penandatangan` int(11) NOT NULL AUTO_INCREMENT,
              `penandatangan` int(11) NOT NULL,
              `pengatasnama` int(11) NOT NULL,
              PRIMARY KEY (`id_penandatangan`),
              KEY `penetap` (`penandatangan`),
              KEY `wakil` (`pengatasnama`)",
        ),
        'ref_pendidikan'=>array(
            'c'=>
              "`id_pendidikan` int(11) unsigned NOT NULL AUTO_INCREMENT,
              PRIMARY KEY (`id_pendidikan`)",
        ),
        'ref_penetap'=>array(
            'c'=>
              "`id_penetap` int(11) NOT NULL AUTO_INCREMENT,
              `penetap` varchar(120) DEFAULT NULL,
              `id_pegawai` int(11) DEFAULT NULL,
              `kop` int(1) DEFAULT '1',
              PRIMARY KEY (`id_penetap`)",
        ),
        'ref_penetap_riwayat'=>array(
            'c'=>
              "`id_penetap_riwayat` int(11) NOT NULL AUTO_INCREMENT,
              `id_penetap` int(11) NOT NULL,
              `tgl_penetapan` date NOT NULL,
              `status` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_penetap_riwayat`)",
        ),
        'ref_penghargaan'=>array(
            'c'=>
              "`id_penghargaan` int(3) NOT NULL AUTO_INCREMENT,
              `penghargaan` varchar(150) NOT NULL,
              `berkala` int(1) NOT NULL,
              PRIMARY KEY (`id_penghargaan`)",
        ),
        'ref_pensiun'=>array(
            'c'=>
              "`id_pensiun` int(1) NOT NULL AUTO_INCREMENT,
              `kode_pensiun` varchar(30) NOT NULL,
              `nama_pensiun` varchar(150) NOT NULL,
              `tipe_pensiun` int(1) NOT NULL,
              PRIMARY KEY (`id_pensiun`)",
        ),
        'ref_periode'=>array(
            'c'=>
              "`id_periode` int(11) NOT NULL AUTO_INCREMENT,
              `id_status_periode` int(1) NOT NULL,
              `nama_periode` varchar(80) DEFAULT NULL,
              `awal` date NOT NULL,
              `akhir` date NOT NULL,
              PRIMARY KEY (`id_periode`)",
        ),
        'ref_prodi'=>array(
            'c'=>
              "`id_prodi` int(11) NOT NULL AUTO_INCREMENT,
              `kode_prodi` varchar(50) DEFAULT NULL,
              `prodi` varchar(255) NOT NULL,
              `mod_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id_prodi`)",
        ),
        'ref_program'=>array(
            'c'=>
              "`id_program` int(3) NOT NULL AUTO_INCREMENT,
              `id_unit` int(3) NOT NULL,
              `penetapan` date DEFAULT NULL,
              `kode_program` varchar(150) DEFAULT NULL,
              `nama_program` varchar(150) NOT NULL,
              PRIMARY KEY (`id_program`)",
        ),
        'ref_propinsi'=>array(
            'c'=>
              "`id_propinsi` int(11) NOT NULL AUTO_INCREMENT,
              `kode_propinsi` varchar(20) DEFAULT NULL,
              `propinsi` varchar(128) NOT NULL DEFAULT '',
              `ibukota` varchar(128) DEFAULT NULL,
              `dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id_propinsi`)",
        ),
        'ref_rak'=>array(
            'c'=>
              "`id_rak` int(11) NOT NULL AUTO_INCREMENT,
              `kode` varchar(20) NOT NULL,
              `deskripsi` varchar(100) DEFAULT NULL,
              PRIMARY KEY (`id_rak`)",
        ),
        'ref_rekanan'=>array(
            'c'=>
              "`id_rekanan` int(11) NOT NULL AUTO_INCREMENT,
              `nama_perusahaan` varchar(200) NOT NULL,
              `direktur` varchar(30) NOT NULL,
              `alamat` varchar(200) NOT NULL,
              `telepon` varchar(20) NOT NULL,
              `fax` varchar(20) NOT NULL,
              PRIMARY KEY (`id_rekanan`)",
        ),
        'ref_rekening'=>array(
            'c'=>
              "`id_rek` int(11) NOT NULL AUTO_INCREMENT,
              `id_rek_parent` int(11) DEFAULT NULL,
              `id_rek_bind` int(11) DEFAULT NULL,
              `id_rek_revisi` int(11) DEFAULT NULL,
              `kode_rekening` varchar(50) NOT NULL,
              `nama_rekening` varchar(150) NOT NULL,
              `jenis` char(1) NOT NULL,
              `is_child` char(1) DEFAULT NULL,
              `level` int(1) DEFAULT NULL,
              `status` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_rek`)",
        ),
        'ref_rentang'=>array(
            'c'=>
              "`id_rentang` int(11) NOT NULL AUTO_INCREMENT,
              `nilai_rendah` int(3) NOT NULL,
              `nilai_tinggi` float NOT NULL,
              `nama_rentang` varchar(50) NOT NULL,
              PRIMARY KEY (`id_rentang`)",
        ),
        'ref_ruang'=>array(
            'c'=>
              "`id_ruang` int(11) NOT NULL AUTO_INCREMENT,
              `kode` varchar(20) NOT NULL,
              `deskripsi` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id_ruang`)",
        ),
        'ref_sat_output'=>array(
            'c'=>
              "`id_sat_output` int(11) NOT NULL AUTO_INCREMENT,
              `nama_sat_output` varchar(50) NOT NULL,
              PRIMARY KEY (`id_sat_output`),
              UNIQUE KEY `nama_sat_output` (`nama_sat_output`)",
        ),
        'ref_sat_waktu'=>array(
            'c'=>
              "`id_sat_waktu` int(11) NOT NULL AUTO_INCREMENT,
              `nama_sat_waktu` varchar(50) NOT NULL,
              PRIMARY KEY (`id_sat_waktu`)",
        ),
        'ref_satuan'=>array(
            'c'=>
              "`id_satuan` int(3) NOT NULL AUTO_INCREMENT,
              `nama_satuan` varchar(30) NOT NULL,
              PRIMARY KEY (`id_satuan`)",
        ),
        'ref_sertifikasi'=>array(
            'c'=>
              "`id_sertifikasi` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `sertifikasi` varchar(128) DEFAULT NULL,
              `keterangan` text,
              PRIMARY KEY (`id_sertifikasi`)",
        ),
        'ref_sifat'=>array(
            'c'=>
              "`id_sifat` int(5) NOT NULL AUTO_INCREMENT,
              `sifat` varchar(50) NOT NULL,
              PRIMARY KEY (`id_sifat`)",
        ),
        'ref_sifat_surat'=>array(
            'c'=>
              "`id_sifat` int(1) NOT NULL AUTO_INCREMENT,
              `nama_sifat` varchar(50) NOT NULL,
              PRIMARY KEY (`id_sifat`)",
        ),
        'ref_skpd'=>array(
            'c'=>
              "`id_skpd` int(11) NOT NULL AUTO_INCREMENT,
              `nama_skpd` varchar(150) NOT NULL,
              `alamat_kantor` varchar(200) DEFAULT NULL,
              `telp_kantor` varchar(20) DEFAULT NULL,
              `id_parent` int(11) DEFAULT NULL,
              PRIMARY KEY (`id_skpd`),
              UNIQUE KEY `uniq` (`nama_skpd`)",
        ),
        'ref_status_anak'=>array(
            'c'=>
              "`id_statusanak` int(1) NOT NULL AUTO_INCREMENT,
              `status_anak` varchar(75) NOT NULL,
              PRIMARY KEY (`id_statusanak`)",
        ),
        'ref_status_istri'=>array(
            'c'=>
              "`id_status_istri` int(1) NOT NULL AUTO_INCREMENT,
              `status_istri` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id_status_istri`)",
        ),
        'ref_status_kawin'=>array(
            'c'=>
              "`id_statuskawin` int(1) NOT NULL AUTO_INCREMENT,
              `statuskawin` varchar(20) NOT NULL,
              PRIMARY KEY (`id_statuskawin`)",
        ),
        'ref_status_keluarga'=>array(
            'c'=>
              "`id_statuskeluarga` int(1) NOT NULL AUTO_INCREMENT,
              `statuskeluarga` varchar(20) NOT NULL,
              PRIMARY KEY (`id_statuskeluarga`)",
        ),
        'ref_status_kepemilikan'=>array(
            'c'=>
              "`id_status_kepemilikan` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `status_kepemilikan` varchar(128) DEFAULT NULL,
              PRIMARY KEY (`id_status_kepemilikan`)",
        ),
        'ref_status_pegawai'=>array(
            'c'=>
              "`id_status_pegawai` int(1) NOT NULL AUTO_INCREMENT,
              `urut` int(1) NOT NULL,
              `tipe` int(1) NOT NULL,
              `status_usul` int(1) NOT NULL,
              `nama_status` varchar(200) NOT NULL,
              `keterangan` text,
              PRIMARY KEY (`id_status_pegawai`)",
        ),
        'ref_status_pengadaan'=>array(
            'c'=>
              "`id_status_pengadaan` int(1) unsigned NOT NULL AUTO_INCREMENT,
              `status_pengadaan` varchar(64) DEFAULT NULL,
              PRIMARY KEY (`id_status_pengadaan`)",
        ),
        'ref_status_periode'=>array(
            'c'=>
              "`id_status_periode` int(1) unsigned NOT NULL AUTO_INCREMENT,
              `status_periode` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
              PRIMARY KEY (`id_status_periode`)",
        ),
        'ref_status_riwayat'=>array(
            'c'=>
              "`id_status_riwayat` int(1) NOT NULL AUTO_INCREMENT,
              `status_riwayat` varchar(80) NOT NULL,
              `alert` varchar(50) NOT NULL,
              PRIMARY KEY (`id_status_riwayat`)",
        ),
        'ref_sudah_belum'=>array(
            'c'=>
              "`id_ref` int(1) NOT NULL AUTO_INCREMENT,
              `ket` varchar(20) NOT NULL,
              PRIMARY KEY (`id_ref`)",
        ),
        'ref_suku'=>array(
            'c'=>
              "`id_suku` int(11) NOT NULL AUTO_INCREMENT,
              `kode_suku` varchar(20) NOT NULL,
              `suku` varchar(150) NOT NULL,
              PRIMARY KEY (`id_suku`)",
        ),
        'ref_supplier'=>array(
            'c'=>
              "`id_supplier` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `nama` varchar(128) DEFAULT NULL,
              `alamat` varchar(128) DEFAULT NULL,
              `telp` varchar(20) DEFAULT NULL,
              `email` varchar(50) DEFAULT NULL,
              PRIMARY KEY (`id_supplier`)",
        ),
        'ref_syarat'=>array(
            'c'=>
              "`id_syarat` int(11) NOT NULL AUTO_INCREMENT,
              `kode_syarat` varchar(20) NOT NULL,
              `syarat` text,
              PRIMARY KEY (`id_syarat`)",
        ),
        'ref_tarif_perjalanan'=>array(
            'c'=>
              "`id_tarif` int(11) NOT NULL AUTO_INCREMENT,
              `id_periode` int(3) NOT NULL,
              `id_golru` int(1) NOT NULL,
              `id_eselon` int(1) NOT NULL,
              `tarif` decimal(20,2) NOT NULL,
              `hotel` decimal(20,2) NOT NULL,
              `representatif` decimal(20,2) NOT NULL,
              PRIMARY KEY (`id_tarif`),
              KEY `id_periode` (`id_periode`),
              KEY `id_golru` (`id_golru`),
              KEY `id_eselon` (`id_eselon`)",
        ),
        'ref_tempat_lahir'=>array(
            'c'=>
              "`id_tempat_lahir` int(11) NOT NULL AUTO_INCREMENT,
              `tempat_lahir` varchar(75) NOT NULL,
              PRIMARY KEY (`id_tempat_lahir`)",
        ),
        'ref_tipe_barang'=>array(
            'c'=>
              "`id_tipe_barang` int(11) NOT NULL AUTO_INCREMENT,
              `kode_tipe_barang` varchar(15) NOT NULL,
              `tipe_barang` varchar(128) NOT NULL,
              PRIMARY KEY (`id_tipe_barang`)",
        ),
        'ref_tipe_pegawai'=>array(
            'c'=>
              "`id_tipe_pegawai` int(1) unsigned NOT NULL AUTO_INCREMENT,
              `tipe_pegawai` varchar(75) NOT NULL,
              `jenis` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
              PRIMARY KEY (`id_tipe_pegawai`)",
        ),
        'ref_unit'=>array(
            'c'=>
              "`id_unit` int(11) NOT NULL AUTO_INCREMENT,
              `id_par_unit` int(11) DEFAULT NULL,
              `kode_unit` varchar(20) DEFAULT NULL,
              `unit` varchar(255) NOT NULL,
              `alamat` varchar(255) DEFAULT NULL,
              `telp` varchar(12) DEFAULT NULL,
              `id_kepala` int(11) DEFAULT NULL,
              `level_unit` int(1) DEFAULT NULL,
              `bso_image` varchar(50) DEFAULT NULL,
              `bso_uu` text,
              `aktif` int(1) NOT NULL,
              `urut` int(1) DEFAULT NULL,
              PRIMARY KEY (`id_unit`)",
        ),
        'ref_wilayah_adat'=>array(
            'c'=>
              "`id_wilayah_adat` int(11) NOT NULL AUTO_INCREMENT,
              `wilayah_adat` varchar(255) DEFAULT NULL,
              `letak_geografis` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_wilayah_adat`)",
        ),
    ),
    'insert'=>array(
        'ref_agama'=>"(`id_agama`,`agama`) values ('27','Buda'),
            ('24','Budha'),
            ('23','Hindu'),
            ('15','Islam'),
            ('17','Katholik'),
            ('25','Konghucu'),
            ('26','Kristen'),
            ('18','Lainnya'),
            ('16','Protestan')",
        'ref_cuti'=>"(`id_cuti`,`cuti`,`status_cuti`) values ('2','Cuti Tahunan','2'),
            ('3','Cuti Sakit','1'),
            ('4','Cuti Besar','1'),
            ('5','Cuti Bersalin','1'),
            ('6','Cuti Alasan Penting (Menikah)','1'),
            ('7','Cuti Karena Alasan Penting (Keluarga Sakit/Meninggal)','1'),
            ('8','Cuti di Luar Tanggungan Negara','1')",
        'ref_diklatpim'=>"(`id_diklatpim`,`diklatpim`,`keterangan`) values ('2','SPADA',''),
            ('5','SEPALA',''),
            ('6','ADUMLA',''),
            ('7','SEPADYA',''),
            ('10','SESPA',''),
            ('11','SESPANAS',''),
            ('13','SPAMEN',''),
            ('14','SPATI',''),
            ('16','LEMHANAS',''),
            ('17','DIKLAT PIM TK.II',''),
            ('18','DIKLAT PIM TK.III',''),
            ('19','DIKLAT PIM TK.IV',''),
            ('20','DIKLAT PRAJABATAN',''),
            ('23','DIKLAT PIM TK. I',''),
            ('26','SPAMA',''),
            ('28','SEPADA',''),
            ('43','ADUM',''),
            ('44','DIKLAT PIM IV','')",
        'ref_eselon'=>"(`id_eselon`,`kode_bkn`,`eselon`,`gol_terendah`,`gol_tertinggi`,`urut`) values ('1','21','II.A','15','16','4'),
            ('2','22','II.B','14','15','5'),
            ('3','31','III.A','13','14','6'),
            ('4','32','III.B','12','13','7'),
            ('5','41','IV.A','11','12','8'),
            ('6','42','IV.B','10','11','9'),
            ('7','51','V.A','9','10','10'),
            ('8','52','V.B','8','9','11'),
            ('12','12','I.B','16','17','3'),
            ('13','','Non Eselon','','','12'),
            ('25','','Kepala Sekolah','','','2'),
            ('26','','I.A','','','1')",
        'ref_golru'=>"(`id_golru`,`id_golru_jenis`,`golongan`,`pangkat`,`urut`) values ('1','0','I/a','Juru Muda','1'),
            ('2','0','I/b','Juru Muda Tingkat I','2'),
            ('3','0','I/c','Juru','3'),
            ('4','0','I/d','Juru Tingkat I','4'),
            ('5','0','II/a','Pengatur Muda','5'),
            ('6','0','II/b','Pengatur Muda Tingkat I','6'),
            ('7','0','II/c','Pengatur','7'),
            ('8','0','II/d','Pengatur Tingkat I','8'),
            ('9','0','III/a','Penata Muda','9'),
            ('10','0','III/b','Penata Muda Tingkat I','10'),
            ('11','0','III/c','Penata','11'),
            ('12','0','III/d','Penata Tingkat I','12'),
            ('13','0','IV/a','Pembina','13'),
            ('14','0','IV/b','Pembina Tingkat I','14'),
            ('15','0','IV/c','Pembina Utama Muda','15'),
            ('16','0','IV/d','Pembina Utama Madya','16'),
            ('17','0','IV/e','Pembina Utama','17')",
        'ref_golru_jenis'=>"(`id_golru_jenis`,`kode`,`golru_jenis`) values ('1','001','Reguler'),
            ('2','002','Pilihan'),
            ('3','003','Istimewa'),
            ('4','004','Pengabdian'),
            ('5','005','Anumerta'),
            ('6','006','Pengangkatan CPNS'),
            ('7','','Pengangkatan Pertama'),
            ('8','','Pilihan Jabatan Struktural'),
            ('9','','Pilihan Jabatan Fungsional Tertentu'),
            ('10','','Pilihan Prestasi Kerja Luar Biasa'),
            ('11','','Pilihan Penemuan Baru'),
            ('12','','Pilihan Menjadi Pejabat Negara'),
            ('13','','Pilihan Penyesuaian Ijazah'),
            ('14','','Pilihan Tugas Belajar'),
            ('15','','Pilihan Ujian Dinas'),
            ('16','','Pilihan lainnya')",
        'ref_gol_darah'=>"(`id_gol_darah`,`gol_darah`) values ('9',''),
            ('2','A'),
            ('12','A+'),
            ('13','A-'),
            ('4','AB'),
            ('3','B'),
            ('10','B+'),
            ('11','B-'),
            ('1','O')",
        'ref_hukdis'=>"(`id_hukdis`,`id_hukdis_level`,`hukdis`,`kondisi`) values ('2','1','Teguran Lisan',''),
            ('3','1','Teguran Tertulis',''),
            ('4','1','Pernyataan Tidak Puas Secara Tertulis',''),
            ('5','2','Penundaan KGB',''),
            ('6','2','Penundaan Kenaikan Pangkat',''),
            ('7','2','Penurunan Pangkat',''),
            ('8','3','Pemindahan',''),
            ('9','3','Pembebasan Jabatan',''),
            ('10','3','Pemberhentian (Dengan Hormat Tidak Atas Permintaan Sendiri)',''),
            ('11','3','Pemberhentian (Tidak Dengan Hormat)',''),
            ('12','3','Pemberhentian (Dengan Hormat Tidak Atas Permintaan Sendiri)','1'),
            ('13','3','Pemberhentian (Tidak Dengan Hormat)','1')",
        'ref_hukdis_level'=>"(`id_hukdis_level`,`hukdis_level`) values ('1','Ringan'),
            ('2','Sedang'),
            ('3','Berat')",
        'ref_jabatan_jenis'=>"(`id_jab_jenis`,`jenis_jabatan`) values ('1','JABATAN STRUKTURAL'),
            ('2','JABATAN FUNGSIONAL TERTENTU'),
            ('3','JABATAN FUNGSIONAL UMUM'),
            ('4','JABATAN RANGKAP (STRUKTURAL DAN FUNGSIONAL)'),
            ('5','CALON JABATAN FUNGSIONAL TERTENTU')",
        'ref_jenis_kelamin'=>"(`id_jeniskelamin`,`jenis_kelamin`) values ('1','Laki-laki'),
            ('2','Perempuan')",
        'ref_kartu'=>"(`id_kartu`,`kode_kartu`,`kartu`,`keterangan`) values ('1','01','Karpeg',''),
            ('2','02','Karsu/Karis',''),
            ('3','03','BPJS',''),
            ('4','04','Taspen','')",
        'ref_penghargaan'=>"(`id_penghargaan`,`penghargaan`,`berkala`) values ('1','Bintang Republik Indonesia Adi','1'),
            ('2','Bintang Mahaputera Adipradana','1'),
            ('3','Bintang Mahaputera Utama','1'),
            ('4','Bintang Jasa Utama','1'),
            ('5','Bintang Jasa Pratama','1'),
            ('6','Bintang Jasa Nararya','1'),
            ('7','Bintang Jasa Pembangunan','1'),
            ('8','Satyalancana Karyasatya 10 Tahun','2'),
            ('9','Satyalancana Karyasatya 20 Tahun','2'),
            ('10','Satyalancana Karyasatya 30 Tahun','2')",
        'ref_pensiun'=>"(`id_pensiun`,`kode_pensiun`,`nama_pensiun`,`tipe_pensiun`) values ('1','01','Perpanjangan Pensiun','2'),
            ('2','02','Pensiun (Atas Permintaan Sendiri)','1'),
            ('3','03','Pensiun (Karena Sakit)','1'),
            ('4','04','Pensiun (Reguler)','1'),
            ('5','05','Meninggal Dunia','1'),
            ('6','06','Pensiun (Mutasi Keluar Instansi)','1'),
            ('7','','PENSIUN (HUKUMAN DINAS)','1')",
        'ref_status_anak'=>"(`id_statusanak`,`status_anak`) values ('1','Anak Kandung (AK)'),
            ('3','Anak Angkat (AA)'),
            ('4','Anak Tiri (AT)')",
        'ref_status_istri'=>"(`id_status_istri`,`status_istri`) values ('1','Perjaka / Gadis'),
            ('2','Duda / Janda')",
        'ref_status_kawin'=>"(`id_statuskawin`,`statuskawin`) values ('1','Belum Kawin'),
            ('2','Kawin'),
            ('3','Janda'),
            ('4','Duda'),
            ('5','Lajang')",
        'ref_status_keluarga'=>"(`id_statuskeluarga`,`statuskeluarga`) values ('1','Kandung'),
            ('2','Angkat'),
            ('3','Mantan Suami/Istri')",
        'ref_status_pegawai'=>"(`id_status_pegawai`,`urut`,`tipe`,`status_usul`,`nama_status`,`keterangan`) values ('1','1','1','0','CPNS',''),
            ('2','2','1','0','PNS',''),
            ('3','3','2','0','Pensiun (BUP)',''),
            ('4','4','2','0','Meninggal dunia',''),
            ('5','5','2','0','Berhenti',''),
            ('6','6','2','0','Mutasi Keluar Antar Provinsi',''),
            ('7','7','2','0','Mutasi Keluar Dalam Provinsi',''),
            ('8','8','2','0','Mutasi Keluar Kabupaten/Kota',''),
            ('9','9','2','0','Mutasi Antar Bagian Dalam SKPD Dan Mutasi Antar SKPD',''),
            ('10','10','1','0','Kontrak',''),
            ('11','3','1','0','Tugas Belajar',''),
            ('12','4','2','0','Pensiun',''),
            ('13','5','2','0','Mutasi Masuk',''),
            ('14','6','2','0','Mutasi Keluar',''),
            ('15','7','2','0','Berhenti',''),
            ('16','0','1','0','PEGAWAI PERBANTUAN','')",
        
    ),
    /*'alter'=>array(
        'add'=>array(
            'ref_aplikasi'=>array(
                "status"=>"INT(1) DEFAULT 1  NULL",
            ),
        ),
    ),*/

);
      
    function __construct() {
    
        parent::__construct();
        login_check($this->session->userdata('login_state'));
        
        $fold=$this->dt['app']['folder'];
        /*
        * 0     1   2    3     4     5        6
        * noapp,ref,kode,tipe,judul,judul_par,link
        */
        $nav = array(
            array(null,2,null,2,'Khusus',null,null,'cog'),
            array(null,2,null,2,'Aplikasi','Khusus','referensi/aplikasi','shield'),
            array(null,2,null,2,'Kewenangan','Khusus','referensi/kewenangan',null),
            array(null,2,null,2,'Operator','Khusus','referensi/operator',null),
            array(null,2,null,2,'Umum',null,null,'cube'),
            array(null,2,null,2,'Personal','Umum',null,'user'),
            array(null,2,null,2,'Jenis Kelamin','Personal','referensi/umum/ke/jenis_kelamin',null),
            array(null,2,null,2,'Golongan Darah','Personal','referensi/umum/ke/golongan_darah',null),
            array(null,2,null,2,'Status Keluarga','Personal','referensi/umum/ke/keluarga_status',null),
            array(null,2,null,2,'Status Anak','Personal','referensi/umum/ke/status_anak',null),
            array(null,2,null,2,'Pekerjaan','Personal','referensi/umum/ke/pekerjaan',null),
            array(null,2,null,2,'Keluarga','Personal','referensi/umum/ke/keluarga',null),
            array(null,2,null,2,'Status Kawin','Personal','referensi/umum/ke/kawin_status',null),
            array(null,2,null,2,'Agama','Personal','referensi/umum/ke/agama',null),
            array(null,2,null,2,'Administratif','Umum',null,'file-text-o'),
            array(null,2,null,2,'Penetap','Administratif','referensi/umum/ke/penetap',null),
            array(null,2,null,2,'Kementerian','Administratif','referensi/umum/ke/kementerian',null),
            array(null,2,null,2,'Jenjang Pendidikan','Administratif','referensi/umum/ke/studi_jenjang',null),
            array(null,2,null,2,'Bentuk Pendidikan','Administratif','referensi/umum/ke/studi_bentuk',null),
            array(null,2,null,2,'Lembaga','Administratif','referensi/umum/ke/studi_lembaga',null),
            array(null,2,null,2,'Fakultas','Administratif','referensi/umum/ke/fakultas',null),
            array(null,2,null,2,'Program Studi','Administratif','referensi/umum/ke/prodi',null),
            array(null,2,null,2,'Jurusan','Administratif','referensi/umum/ke/jurusan',null),
            array(null,2,null,2,'Ujian Kompetensi','Administratif','referensi/umum/ke/kompetensi',null),
            array(null,2,null,2,'Propinsi','Administratif','referensi/umum/ke/propinsi',null),
            array(null,2,null,2,'Kabupaten','Administratif','referensi/umum/ke/kabupaten',null),
            array(null,2,null,2,'Kecamatan','Administratif','referensi/umum/ke/kecamatan',null),
            array(null,2,null,2,'Kelurahan/Desa','Administratif','referensi/umum/ke/kelurahan',null),
            
            array(null,2,null,2,'SIMPEG',null,null,'cube'),
            array(null,2,null,2,'Unit Kerja','SIMPEG','referensi/unit',null),
            array(null,2,null,2,'Unit Organisasi','SIMPEG','referensi/bidang',null),
            array(null,2,null,2,'Status Pegawai','SIMPEG','referensi/umum/ke/status_pegawai',null),
            array(null,2,null,2,'Golongan Ruang/Pangkat','SIMPEG','referensi/umum/ke/pangkat',null),
            array(null,2,null,2,'Jenis Kenaikan Pangkat','SIMPEG','referensi/umum/ke/pangkat_jenis',null),
            array(null,2,null,2,'Jabatan','SIMPEG','referensi/umum/ke/jabatan',null),
            array(null,2,null,2,'Eselon','SIMPEG','referensi/umum/ke/eselon',null),
            array(null,2,null,2,'Cuti','SIMPEG','referensi/umum/ke/cuti',null),
            array(null,2,null,2,'Penghargaan','SIMPEG','referensi/umum/ke/penghargaan',null),
            array(null,2,null,2,'Diklat Penjenjangan','SIMPEG','referensi/umum/ke/diklatpim',null),
            array(null,2,null,2,'Diklat Teknis/Fungsional','SIMPEG','referensi/umum/ke/diklatteknis',null),
            array(null,2,null,2,'Hukuman Disiplin,null','SIMPEG','referensi/umum/ke/hukdis',null),
            array(null,2,null,2,'Level Hukuman Disiplin','SIMPEG','referensi/umum/ke/hukdis_level',null),
            array(null,2,null,2,'Kartu Pegawai','SIMPEG','referensi/umum/ke/kartu',null),
            array(null,2,null,2,'Pensiun','SIMPEG','referensi/umum/ke/status_pensiun',null),
            array(null,2,null,2,'Anggaran','SIMPEG','referensi/umum/ke/tipe_anggaran',null)
        );
        $this->dt['nav']=$nav;

    }

    public function index() {
        $this->setdb();
    }

    function init() {
        $init=$this->dt['app'];
        $init['judul']=$init['nama_aplikasi'];
        return $init;
    }
    
    function setdb() {
        $this->load->helper('cmd');
        die(load_cnt('dm','Builder','menu',uri2('1,2')));
    }
    
    function set_db($n = '') {
        $tabel = array();

        $this->load->library('../controllers/dm/builder');
        if($this->builder->ver >= $this->requdm){
            #$odie=$this->builder->process_db($app,$tabel,$nav);
            $odie=$this->builder->proses_db($this->dt,$this->stabel);
            #$odie = load_controller('tnde','builder','process_db',$app,$tabel,$nav,$stabel);//
        }else{
            $odie=json_encode(array(
            'sign' => 1,
            'text' => 'Pemutakhiran Database Gagal dilakukan.<br> required dm minimum versi : '.$this->requdm,//$db_build['total'],
        ));
        }
        if ($n) {
            redirect('/');
        } else {
            die($odie);    
        }
    }
    
    function hapus_tabel_prefix($prefiz) {
        $this->load->library('../controllers/dm/builder');
        return $this->builder->clear_tabel_prefix($prefiz);
    }
    
    function cmp() {
        $daftar=load_controller('dm','builder','daftar_tabel',$this->stabel['clear_prefix'],@$this->stabel['exclude_clear_prefix']);
        extract(load_controller('dm','builder','comparetables',$this->stabel['clear_prefix'],$this->stabel['create'],@$this->stabel['exclude_clear_prefix']));
        cek(array(
            'daftar'=>$daftar,
            'tambah'=>$tambah,
            'hapus'=>$hapus,
            'sama'=>$sama,
            'beda'=>$bedasama,
        ));
    }
    
    function force() {
        $this->load->helper('cmd');
        stop(load_controller('dm','builder','force_db',array(
            'dtapp'=>$this->dt['app'],
            'nav'=>$this->dt['nav'], 
            'kewenangan'=>$this->dt['plus']['kewenangan'], 
            'stabel'=>$this->stabel,)
        ));
    }
}
?>