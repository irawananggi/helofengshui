<?php
  if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
  class App_doc {
    var $debug=0;
    var $tb_nama='ref_app_doc';
    var $id_tb="id_ref_app_doc";
    public function __construct()
    {
        #parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->helper('cmd');
    }
    
    function index(){
    }
    
    function get_tombol() {
        $ret=array();
        $id_tb=$this->id_tb;
        if(($getdoc=$this->CI->general_model->datagrabe(array(
                 'tabel'=> array(
                    'ref_aplikasi ra'=>'',
                    $this->tb_nama.' d'=>array('d.id_app=ra.id_aplikasi and d.status=1','left'),
                    ),
                 'where'=>array('ra.aktif'=>1),
                 'select'=>"ra.id_aplikasi,ra.nama_aplikasi,d.*,if(isnull(d.$id_tb),0,1)ada",
                )))&&($getdoc->num_rows()>0))
            foreach ($getdoc->result() as $r) {
                $ret[$r->id_aplikasi]=array(
                    'doc'=>$r->doc,
                    'ver'=>$r->ver,
                    'tombol'=>($r->ada==1
                        ?tag('a','<i class="fa fa-edit"></i>','class="btn btn-xs btn-edit" title="Edit Dokumen" act="'.
                            site_url(uri('1').'/app_doc/dt/'.in_de(array(
                                'id_app'=>$r->id_aplikasi,
                                'nama_app'=>$r->nama_aplikasi,
                                'modex'=>'form_mode',
                                'form_id'=>$r->$id_tb,
                            )))
                        .'"')
                        :tag('a','<i class="fa fa-plus"></i>','class="btn btn-xs btn-edit" title="Tambah Dokumen" act="'.
                            site_url(uri('1').'/app_doc/dt/'.in_de(array(
                                'id_app'=>$r->id_aplikasi,
                                'nama_app'=>$r->nama_aplikasi,
                                'ret_save'=>uri_string(),
                                'modex'=>'form_mode',
                                'new'=>1,
                                'form_id'=>'',
                            )))
                        .'"')
                    ),
                );  
        }
        return $ret;
    }
    
    function get_param($o="") {
        $param= array(
            'title' => 'Dokumen Aplikasi',
            'tabel' =>  array(
                'ref_aplikasi ra'=>'',
                $this->tb_nama.' d'=>array('ra.id_aplikasi=d.id_app and d.status=1','left'),
            ),
                        
            'where'=>array('ra.`aktif`'=>1),
            "select"=>"d.*,ra.nama_aplikasi",
            'id' => $this->id_tb,
            'eid' => 'd.'.$this->id_tb,
            'tabel_save' => $this->tb_nama,
            'tombol' => array(),
            'can' => array('edit','delete'),//bisa hapus dan edit
            'kolom' =>      array('Nama','Pemerintahan','Kabupaten','Desa','Unit'),//header tabel
            'kolom_data' => array('id_app','doc','ver'),//jangan di kasih nama tabel di depan nya
            'kolom_tampil' => array('nama','pemrt','kab','desa','unit'),//jangan di kasih nama tabel di depan nya
            /*'ret'=>array(
                'fold'=>uri(1),
                'def'=>uri(2),
                'func'=>'ret_prev',
            ),*/
            #'debug_form'=>1,
            'form'=>array(
                array(-3,'<label>Aplikasi : '.$o['nama_app'].'</label>'),
                array(7,0,'Dokumen','doc','','doc'),
                array(1,0,'Ver','ver','',''),
                array(0,0,'','id_app',$o['id_app'],'id_app'),
            ),
            
            'form_script'=>
                "
                ",
        );
        return $param;
    }
    function dt($search = null,$offset = null) {
        $this->CI->benchmark->mark('awalz');
        //cek(sesi('id_unker'));
        $key = post('search');
        $modex = "list_data";
        $search_param = null;
        if (!empty($key)) {
            $search_param = $key;
            
        } else if (!empty($search)) {
            $o = un_de($search);
            if(!empty($o['modex']))$modex = $o['modex'];
            $form_id = @$o['form_id'];
            if(!empty($o['search']))$search_param = $o['search'];
        } 
        $folder_app=$this->CI->uri->segment(1);
        $param=$this->get_param($o);
        
        $param['inti']=uri(3);
        if(@$o['new'])$param['new']=1;
        
        if(is_array(@$param)){
            $param = array_merge_recursive(array(
                    'search' => $search_param,
                    'module' => uri2('1,2,'),
                    'page'  => uri(3),
            ),$param);
                    
            $this->CI->benchmark->mark('akhirz');
            if($this->debug) echo komen($this->CI->benchmark->elapsed_time('awalz','akhirz'));
        
            $this->CI->load->library('Defa');
            
            switch($modex) {
                case "list_data" : 
                    if(isset($param['redir'])){
                        redirect($param['redir_path'].'/'.$param['redir'].'/'.$param['redir_func'].'/'. @$param['redir_param']);
                    }else{
                        if(!(in_array($offset ,array('excel','cetak')))){
                            $param['return_object']=1;
                            $data=$this->CI->defa->list_data( $param, $offset); 
                            $data['content'] = "dm/standard_view";
                            $this->CI->load->view('dm/home', $data);
                        }else{
                            $this->CI->defa->list_data( $param, $offset);
                        }
                    }
                    break;
                case "form_mode" : 
                    if(@$o['ret_save'])$param['ret_save']=$o['ret_save'];
                    $this->CI->defa->form_data( $param, $form_id); 
                    break;
                case "save_mode" : 
                    $this->CI->defa->save_data($param); 
                    break;    
            }
        }
    
    }
    
  }
?>
