<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;


require APPPATH . 'controllers/Midtrans/Config.php';
require APPPATH . 'controllers/Midtrans/CoreApi.php';
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

define( 'API_ACCESS_KEY', 'AAAAfIemvrw:APA91bEqHB8ub4fYgb0AA_TtgZYIkAk0M65Bw0kdYnZsQ6WkmSjZ1H9A6VUDdPlHh7NqVe9QoownDrslpGh3JAPYRs4bu37qSbMaz-7Ob7kWmJVf8JW-LVulM2Tw4h8OLpirRi5xVdLQ' );

class Notification extends REST_Controller
{
    public function index_post(){
        $notif = file_get_contents('php://input');
        $notification_body = json_decode($notif, true);
        $order_id = $notification_body['order_id'];
        $transaction_id = $notification_body['transaction_id'];
        $status_code = $notification_body['status_code'];
        $transaction_status = $notification_body['transaction_status'];
        $payment_type = $notification_body['payment_type'];
        $va_number = $notification_body['va_numbers'];
        $fraud_status = $notification_body['fraud_status'];
        $va_number = $va_number[0]['va_number'];
            //untuk ambil fcm_id user (hanya contoh)
          $histori_transaksi =  $this->mymodel->getbywhere('permintaan',"order_id=",$order_id,"row");

          $id_histori_transaksi = $histori_transaksi->id;
          $sblm_tgl = konversi_tanggal("D, j M Y",substr($histori_transaksi->tgl_expired,0,10),"id");
         
          $fcm_member = $this->mymodel->getbywhere('member','member_id',$histori_transaksi->member_id,"row");
          $id_fcm_member = $fcm_member->fcm_id;
          


            $this->load->library("firebasenotif");
           if(!$histori_transaksi)
            $msg = array('status' => 0, 'message'=>'Terjadi kesalahan | histori_transaksi Tidak ditemukan');
            
            if ($transaction_status== 'settlement'){
                $status_permintaan = "Selesai";
                $kode_status = "4";
                $ss = $this->send_notif("Pembayaran Berhasil", "Selamat! Pembayaran telah terkonfirmasi", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");
            }
            else if($transaction_status == 'pending'){
                $status_permintaan = "Menunggu pembayaran";
                $kode_status = "1";
                            
                $ss = $this->send_notif("Pembayaran Pending", "Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum $sblm_tgl WIB..", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

            }
            else if ($transaction_status == 'expire' OR $transaction_status == 'Failure') {
                $status_permintaan = "expire";
                $kode_status = "3";
                     $ss = $this->send_notif("Pembayaran Ditolak", "Pesanan expired", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");
            }else if ($transaction_status == 'cancel' OR $transaction_status == 'Failure') {
                $status_permintaan = "cancel";
                $kode_status = "2";
                     $ss = $this->send_notif("Pembayaran Ditolak", "Pesanan cancel", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

            }
           
            $data = array(
              "status_permintaan" => $status_permintaan,
              "kode_status" => $kode_status,
              "no_va" => (($va_number == NULL)?NULL:$va_number),
              "payment_type" => (($payment_type == NULL)?NULL:$payment_type)
              );
            $this->mymodel->update('permintaan',$data,'id',$histori_transaksi->id);


            $msg = array('status' => 1, 'message'=>'Sukses ', 'data'=>$ss);
            $this->response($msg);
    }

  
  public function send_notif($title,$desc,$id_fcm,$kode_status='', $id_histori_transaksi='',$click_action='', $extends_data=[], $sound='', $priority='high')
  {
    /*cek($title);
    cek($desc);
    cek($id_fcm);
    cek($kode_status);
    cek($id_histori_transaksi);
    die();*/
    $Msg = array(
      'body' => $desc,
      'title' => $title,
      "content_available" => true,
      "priority" => "high",
      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
      "android_channel_id"=>"fengshui"
    );
    $data = array(
      "priority" => "high",
      "content_available" => true,
      "kode_status" => $kode_status,
      "id_histori_transaksi" => $id_histori_transaksi
    );
    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
   
    curl_close( $ch );
 
    $cek_respon = explode(',',$result);
    /*cek($result);
    die();*/
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
  }
}
