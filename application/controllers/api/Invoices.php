<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';

//require APPPATH . '/libraries/REST_Controller.php';

class Invoices extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
  

    public function index_post()
    {
       $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
      if ($token != '') {
          $member = $this->mymodel->getbywhere('member','token',$token,"row");
          $id_member = $member->member_id;
          

          if (!empty($member)) {
            /*$permintaan = $this->mymodel->withquery("select * from permintaan where kategori_id='".$kategori_id."' and member_id='".$member->member_id."' and kode_status ='".$member->member_id."' order by id DESC ",'row');*/

            $kategori_id = $this->post('kategori_id');
            $form_datas = json_decode($this->post("form_datas"));

            $kategori = $this->mymodel->getbywhere('kategori','id',$kategori_id,"row");
            $cek_data = $this->mymodel->getbywhere('permintaan',"member_id='$member->member_id' AND kategori_id='$kategori_id' AND kode_status='1'",null,"result");
            $cek_data2 = $this->mymodel->getbywhere('permintaan',"member_id='$member->member_id' AND kategori_id='$kategori_id' AND kode_status='1'",null,"row");

            $get_voucher = $this->mymodel->getbywhere('voucher','id',@$cek_data2->voucher_id,"row");


                if (!empty($kategori_id)) {
                  $kategori = $this->mymodel->getbywhere('kategori','id',$kategori_id,"row");
                  $id = "HF-".date("Ymdhis").$kategori_id;
                  $total = $kategori->harga;
                 
                  $data = array(
                    "kategori_id" => $kategori_id,
                    "total_bayar" => $kategori->harga,
                    "nama_langganan" =>$member->nama_lengkap,
                    "order_id" => $id,
                    "merchant_id" => "G921317011",
                    "client_key" => "SB-Mid-client-InpddDpe3dYs0hX2",
                    "server_key" => "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY"
                  );
                  
                  //if production uncomment this
                  /*$data = array(
                    "kategori_id" => $kategori_id,
                    "total_bayar" => $kategori->harga,
                    "nama_langganan" =>$member->nama_lengkap,
                    "order_id" => $id,
                    "merchant_id" => "G921317011",
                    "client_key" => "Mid-client-pRQOVZj2nE94s0ee",
                    "server_key" => "Mid-server-FwNC99hl8rYgJAKNnasMqreG"
                  );*/


                  if(empty($cek_data2->voucher_id)){
                  $total = $total_bayar;
                    $data = array(
                      "kategori_id" => $kategori_id,
                      "name_kategori" => $kategori->name_in,
                      "foto_kategori" => base_url("uploads/file/".$kategori->foto),
                      //"tgl_order" => $get_permintaan->tgl_bayar ,
                      "kode_voucher" => '',
                      "kode_voucher_rupiah" => 'Rp ',
                      "harga_kategori" => $kategori->harga,
                      "harga_kategori_rupiah" => 'Rp '.numberToCurrency($kategori->harga),
                      "sub_total" => $total,
                      "sub_total_rupiah" => 'Rp '.numberToCurrency($total),
                      "nama_langganan" =>$member->nama_lengkap,
                      "order_id" => $id,
                      "merchant_id" => "G921317011",
                      "client_key" => "SB-Mid-client-InpddDpe3dYs0hX2",
                      "server_key" => "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY"
                    );
                 }else{
                    if($get_voucher->dis_id  == '1'){
                      $cek_harga_v = (($kategori->harga * $get_voucher->nominal)/100);
                      $total_bayar = ($kategori->harga - $cek_harga_v);
                    }else{
                      $total_bayar = ($kategori->harga - $get_voucher->nominal);
                    } 
                    $total = $total_bayar;
                    $data = array(
                      "kategori_id" => $kategori_id,
                      "name_kategori" => $kategori->name_in,
                      "foto_kategori" => base_url("uploads/file/".$kategori->foto),
                      //"tgl_order" => $get_permintaan->tgl_bayar,
                      "kode_voucher" => $total_voucher,
                      "kode_voucher_rupiah" => 'Rp '.numberToCurrency($total_voucher),
                      "harga_kategori" => $kategori->harga,
                      "harga_kategori_rupiah" => 'Rp '.numberToCurrency($kategori->harga),
                      "sub_total" => $total,
                      "sub_total_rupiah" => 'Rp '.numberToCurrency($total),
                      "nama_langganan" =>$member->nama_lengkap,
                      "order_id" => $id,
                      "merchant_id" => "G921317011",
                      "client_key" => "SB-Mid-client-InpddDpe3dYs0hX2",
                      "server_key" => "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY"
                    );
                 }

                    $dat_voucher_id = $cek_data2->voucher_id;

                    $get_midtrans = $this->payment_midtrans($id,$total,$kategori_id,$member->member_id,$dat_voucher_id);
                    /* cek(count($cek_data));
                     cek($cek_data2->token);
                     cek($total_bayar);
                     die();*/
                     if(count($cek_data) > 0){
                      
                         $data_upload = array( 
                            'kategori_id' => $kategori_id,
                            'total_bayar' => $total_bayar,
                            'tgl_bayar' => date('Y-m-d H:i:s'),
                            'tgl_expired' => date('Y-m-d H:i:s',strtotime('+1 day')),
                            'order_id'=>$id,
                            'token' => $get_midtrans,
                            'created_at' => date("Y-m-d H:i:s")
                          );
                        $this->mymodel->update('permintaan',$data_upload,'id',$cek_data2->id);



                     }else{
                            $data_in = array(
                              "kategori_id" => $kategori_id,
                              "member_id" => $member->member_id,
                              /*'voucher_id' =>$voucher_id,
                              'total_voucher' =>$total_voucher,*/
                              'harga_kategori' =>$kategori->harga,
                              "tgl_bayar" => date('Y-m-d H:i:s'),
                              "tgl_expired" => date('Y-m-d H:i:s',strtotime('+1 day')),
                              "total_bayar" => $total_bayar,
                              "order_id" => $id,
                              "token" => $get_midtrans,
                              "created_at" => date("Y-m-d H:i:s")
                            );
                          
                            $in = $this->mymodel->insert("permintaan",$data_in);



                     }

                    


            $data_result = $this->mymodel->getbywhere('permintaan','id',$parent_id,'result');
            foreach ($data_result as $key => $value) {
              
              $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
              $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');



              $value->foto_kategori = base_url("uploads/file/".$get_kategori->foto);
              $value->name_in = $get_kategori->name_in;
              $value->name_en = $get_kategori->name_en;
              $value->name_man = $get_kategori->name_man;
              $value->deskripsi_in = $get_kategori->deskripsi_in;
              $value->deskripsi_en = $get_kategori->deskripsi_en;
              $value->deskripsi_man = $get_kategori->deskripsi_man;
              $value->ketentuan_in = $get_kategori->ketentuan_in;
              $value->ketentuan_en = $get_kategori->ketentuan_en;
              $value->ketentuan_man = $get_kategori->ketentuan_man;
              $value->hub_cs = $get_kategori->hub_cs;
              $value->kategori_kematian = $get_kategori->kategori_kematian;
              $value->harga = $get_kategori->harga;
              $value->name_sp_in = $get_status->name_sp_in;
              $value->name_sp_en = $get_status->name_sp_en;
              $value->name_sp_man = $get_status->name_sp_man;


              $value->detail_form = $this->mymodel->getbywhere('permintaan_detail','permintaan_id',$value->id,'result');

              foreach ($value->detail_form as $key3 => $value3) {

                $form_kategori_2 = $this->mymodel->getbywhere('form_kategori','id',$value3->form_kategori_id,'row');
                
                $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$form_kategori_2->jenis_form_id,'row');

                $value3->name_cat_in = $form_kategori_2->name_cat_in;
                $value3->name_cat_en = $form_kategori_2->name_cat_en;
                $value3->name_cat_mann = $form_kategori_2->name_cat_man;
              }

            }

        if (!empty($get_midtrans)) {
          //$permintaan = $this->mymodel->getbywhere('permintaan','kategori_id',$kategori_id,"row");
          $permintaan = $this->mymodel->withquery("select * from permintaan where kategori_id='".$kategori_id."' and member_id='".$member->member_id."' order by id DESC ",'row');

            /*$datax['email'] = $member->email;
            $datax['nama'] = $member->nama_lengkap;
            $datax['phone'] = $member->phone;
            $datax['name_in'] = $kategori->name_in;
            $datax['created_at'] = $permintaan->created_at;
            $datax['order_id'] = $permintaan->order_id;

            $this->load->library('email');      

            $econfig['protocol'] = "smtp";
            $econfig['smtp_host'] = "ssl://smtp.googlemail.com";
            $econfig['smtp_port'] = 465;
            $econfig['smtp_user'] = "helofengshui@gmail.com"; 
            $econfig['smtp_pass'] = "Helofengshui2021";
            $econfig['charset'] = "utf-8";
            $econfig['mailtype'] = "html";

            $this->email->initialize($econfig);
            $this->email->set_newline("\r\n");

            $this->email->from('helofengshui@gmail.com', 'Helofengshui');
            $this->email->to('helofengshui@gmail.com');

            $this->email->subject('Transaksi Baru');

            $this->email->message($this->load->view('html_invoice_new',$datax,true));
            $send_email = $this->email->send();*/




           /* // Tampilkan pesan sukses atau error
            cek($kategori->name_in);
            die();*/

          //$up = $this->mymodel->getbywhere("member",$data2,'id_member',$member->id_member);
          //sandbox
          //$msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=>'https://app.sandbox.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
          //production
          $msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=> 'https://app.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
          $status="200";
         // $this->export_invoice($id,"pending","Belum Dipilih");
        }

                  }
                  else{
                      $msg = array('status' => 0, 'message'=>'Input tidak valid', 'token'=> "",'data'=> new \stdClass());
                      $status="422";
                  }
                }else{
                  $msg = array('status' => 1, 'message'=>'Member tidak ditemukan', 'token'=> "",'data'=>new \stdClass());
                  $status="401";
                }
        }else{
          $msg = array('status' => 0, 'message'=>'Token Tidak ditemukan', 'token'=> "",'data'=>new \stdClass() );
          $status="401";
        }        
        $this->response($msg,$status);
        die();
    }



    public function payment_midtrans($id,$total,$kategori_id,$member_id,$dat_voucher_id){
      
      //Set Your server key
       //\Midtrans\Config::$serverKey = "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY";
      \Midtrans\Config::$serverKey = "Mid-server-FwNC99hl8rYgJAKNnasMqreG";

      // Uncomment for production environment
     \Midtrans\Config::$isProduction = true;
       // \Midtrans\Config::$isProduction = false;

      \Midtrans\Config::$isSanitized = true;
      //\Midtrans\Config::$isSanitized = false;
      \Midtrans\Config::$is3ds = false;

      // Mandatory for Mandiri bill payment and BCA KlikPay
      // Optional for other payment methods

      $transaction_details = array(
        'order_id' => $id,
        'gross_amount' => (int) $total, // no decimal allowed
      );

      $item1_details = array(
          'id' => $id,
          'price' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->harga,
          'quantity' => 1,
          'name' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->name_in
          );

      if($dat_voucher_id != NULL){
          $get_permintaan = $this->mymodel->getbywhere('permintaan',"member_id='$member_id' AND kategori_id='$kategori_id' AND kode_status='1'",null,"row");
          $get_voucher = $this->mymodel->getbywhere('voucher',"id='$get_permintaan->voucher_id'",null,"row");
          $item1_details2 = array(
            'id' => 'v_'.$id,
            'price' => -$get_permintaan->total_voucher,
            'quantity' => 1,
            'name' => 'Diskon Voucher'
          );
      }
      $item_details = array ($item1_details,$item1_details2);

      $get_member = $this->mymodel->getbywhere('member','member_id',$member_id,'row');

      $customer_details = array(
       'email'         => $get_member->email, //mandatory
      );

      // Fill transaction details
      $transaction = array(
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
      

      $snapToken = \Midtrans\Snap::getSnapToken($transaction);
      return $snapToken;
    }


    public function payment_midtrans_lama($id,$total,$kategori_id,$member_id){
     
      //Set Your server key
       //\Midtrans\Config::$serverKey = "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY";
     \Midtrans\Config::$serverKey = "Mid-server-FwNC99hl8rYgJAKNnasMqreG";

      // Uncomment for production environment
       \Midtrans\Config::$isProduction = true;
      //\Midtrans\Config::$isProduction = false;

      \Midtrans\Config::$isSanitized = true;
     // \Midtrans\Config::$isSanitized = false;
      \Midtrans\Config::$is3ds = false;

      // Mandatory for Mandiri bill payment and BCA KlikPay
      // Optional for other payment methods

      $transaction_details = array(
        'order_id' => $id,
        'gross_amount' => (int) $total, // no decimal allowed
      );

      $item1_details = array(
          'id' => $id,
          'price' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->harga,
          'quantity' => 1,
          'name' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->name_in
          );
      $item_details = array ($item1_details);

      $get_member = $this->mymodel->getbywhere('member','member_id',$member_id,'row');

      $customer_details = array(
       'email'         => $get_member->email, //mandatory
      );

      // Fill transaction details
      $transaction = array(
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
      

      $snapToken = \Midtrans\Snap::getSnapToken($transaction);
      return $snapToken;
    }

}
