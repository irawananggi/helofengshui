<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';


define( 'API_ACCESS_KEY', 'AAAAfIemvrw:APA91bEqHB8ub4fYgb0AA_TtgZYIkAk0M65Bw0kdYnZsQ6WkmSjZ1H9A6VUDdPlHh7NqVe9QoownDrslpGh3JAPYRs4bu37qSbMaz-7Ob7kWmJVf8JW-LVulM2Tw4h8OLpirRi5xVdLQ' );
//require APPPATH . '/libraries/REST_Controller.php';

class Notif_app extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
  

    public function index_post()
    {
       $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
        if ($token != '') {
          $member = $this->mymodel->getbywhere('member','token',$token,"row");
          $id_member = $member->member_id;



          if (!empty($member)) {
            $kategori_id = $this->post('kategori_id');
            $id_produk = $this->post('id_produk');
            $kode_status = $this->post('kode_status');
            


          $histori_transaksi =   $this->mymodel->getbywhere('permintaan',"member_id='$id_member' AND kategori_id='$kategori_id' AND metode_trans='2' AND kode_status='$kode_status'",null,"row");

          $id_histori_transaksi = $histori_transaksi->id;


          $fcm_member = $this->mymodel->getbywhere('member','member_id',$id_member,"row");
          $id_fcm_member = $fcm_member->fcm_id;

          $sblm_tgl = konversi_tanggal("D, j M Y",substr($histori_transaksi->tgl_expired,0,10),"id");

            if (!empty($kategori_id)) {
                $cek_data = $this->mymodel->getbywhere('permintaan',"member_id='$id_member' AND kategori_id='$kategori_id' AND metode_trans='2' AND status_order='0'",null,"result");
                $cek_data2 = $this->mymodel->getbywhere('permintaan',"member_id='$id_member' AND kategori_id='$kategori_id' AND metode_trans='2' AND status_order='0'",null,"row");
               
                if($kode_status == 1){
                  $ss = $this->send_notif("Pembayaran Pending", "Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum $sblm_tgl WIB..", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

                  $status = 'Menunggu Pembayaran';
                }elseif($kode_status == 2){
                  
                  $ss = $this->send_notif("Cancel", "Selamat! Pembayaran telah terkonfirmasi", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

                  $status = 'Cancel';
                }elseif($kode_status == 4){
                  
                  $ss = $this->send_notif("Pembayaran Berhasil", "Pesanan cancel", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

                  $status = 'Selesai';
                }else{
                  
                  $ss = $this->send_notif("Pembayaran Ditolak", "Pesanan expire", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

                  $status = 'Expire';
                }

                if(count($cek_data) > 0){
                  if($kode_status == 1){
                    $data_upload = array( 
                      'status_order' => '0',
                      'kode_status' => $kode_status,
                      'status_permintaan' => $status
                    );
                  }else{
                    $data_upload = array( 
                      'status_order' => '1',
                      'kode_status' => $kode_status,
                      'status_permintaan' => $status
                    );
                  }
                   
                   $in =  $this->mymodel->update('permintaan',$data_upload,'id',$cek_data2->id);
                  $msg = array('status' => 1, 'message'=>'Data berhasil di update',  'data' => array());
                }
                else{
                
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan',  'data' => array());
              }
            } 
            else{
                $msg = array('status' => 0, 'message'=>'Input tidak valid','data'=>array());
                $status="422";
            }
          }else{
            $msg = array('status' => 0, 'message'=>'Member Tidak ditemukan','data'=>array());
            $status="401";
          }
        }else{
          $msg = array('status' => 0, 'message'=>'Token Tidak ditemukan','data'=>array());
          $status="401";
        }       
        $this->response($msg,$status);
        die();
    }

  public function send_notif($title,$desc,$id_fcm,$kode_status='', $id_histori_transaksi='',$click_action='', $extends_data=[], $sound='', $priority='high')
  {
    /*cek($title);
    cek($desc);
    cek($id_fcm);
    cek($kode_status);
    cek($id_histori_transaksi);
    die();*/
    $Msg = array(
      'body' => $desc,
      'title' => $title,
      "content_available" => true,
      "priority" => "high",
      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
      "android_channel_id"=>"fengshui"
    );
    $data = array(
      "priority" => "high",
      "content_available" => true,
      "kode_status" => $kode_status,
      "id_histori_transaksi" => $id_histori_transaksi
    );
    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
   
    curl_close( $ch );
 
    $cek_respon = explode(',',$result);
    /*cek($result);
    die();*/
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
  }
}
