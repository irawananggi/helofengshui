<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Histori_transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");

          //$kode_transaksi = $this->post('kode_transaksi');
          if (isset($mem)) {
            $member_id = $this->get("member_id");
            $member = $this->mymodel->getbywhere('permintaan','member_id',$member_id,"row");
            //$get_permintaan = $this->mymodel->getbywhere('permintaan','tracking_id',$kode_transaksi,'row');
            if (isset($member)) {
              
                $data = $this->mymodel->getbywheresort('permintaan',"member_id = $mem->member_id","",'created_at',"DESC");
                 // $data = $data = $this->mymodel->withquery("select * from permintaan where member_id = '".$mem->member_id."' GROUP BY member_id  order by hitung desc",'result');
                  //$member_id = $this->get("member_id");
                  //$member_id = $this->get("member_id");

                foreach ($data as $key => $value) {
                
                  $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
                  $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');
                  $string = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_in)); 
                  // ----- remove control characters ----- 
                  $string = str_replace("\r", '', $string);
                  $string = str_replace("\n", ' ', $string);
                  $string = str_replace("\t", ' ', $string);
                  $string = str_replace("&nbsp;", ' ', $string);
                  $string = trim(preg_replace('/ {2,}/', ' ', $string));

                 
                  $string2 = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_en)); 
                  // ----- remove control characters ----- 
                  $string2 = str_replace("\r", '', $string2);
                  $string2 = str_replace("\n", ' ', $string2);
                  $string2 = str_replace("\t", ' ', $string2);
                  $string2 = str_replace("&nbsp;", ' ', $string2);
                  $string2 = trim(preg_replace('/ {2,}/', ' ', $string2));

                 
                  $string3 = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_man)); 
                  // ----- remove control characters ----- 
                  $string3 = str_replace("\r", '', $string3);
                  $string3 = str_replace("\n", ' ', $string3);
                  $string3 = str_replace("\t", ' ', $string3);
                  $string3 = str_replace("&nbsp;", ' ', $string3);
                  $string3 = trim(preg_replace('/ {2,}/', ' ', $string3));

                 
                  $value->jawaban_in = $string;
                  $value->jawaban_en = $string2;
                  $value->jawaban_man = $string3;
                  $value->member_id = $value->member_id;
                  $value->kategori_id = $value->kategori_id;
                  $value->name_in = $get_kategori->name_in;
                  $value->name_en = $get_kategori->name_en;
                  $value->name_man = $get_kategori->name_man;
                  $value->foto = base_url('uploads/file/'.$get_kategori->foto);
                  $value->icon = base_url('uploads/file/'.$get_kategori->icon);
                  $value->name_sp_in = $get_status->name_sp_in;
                  $value->name_sp_en = $get_status->name_sp_en;
                  $value->name_sp_man = $get_status->name_sp_man;
                
              }
              if (!empty($data)) {
                if($id){
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data[0]);
                } else {
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
                }
              }else {
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
              }
            }else {
                $msg = array('status' => 0, 'message'=>'Member Id Tidak Ditemukan ');
            }
        }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}