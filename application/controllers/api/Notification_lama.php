<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;


require APPPATH . 'controllers/Midtrans/Config.php';
require APPPATH . 'controllers/Midtrans/CoreApi.php';
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Notification extends REST_Controller
{
    public function index_post(){
            $notif = file_get_contents('php://input');
            $notification_body = json_decode($notif, true);
          /*  $order_id = $notif->order_id; 
            $transaction_id = $notif->transaction_id;
            $status_code = $notification_body['status_code'];
            $transaction_status = $notif->transaction_status;
            $payment_type = $notif->payment_type;
            $fraud_status = $notif->fraud_status;
*/
            $order_id = $notification_body['order_id'];
            $transaction_id = $notification_body['transaction_id'];
            $status_code = $notification_body['status_code'];
            $transaction_status = $notification_body['transaction_status'];
            $payment_type = $notification_body['payment_type'];
            $fraud_status = $notification_body['fraud_status'];
          


            

            //untuk ambil fcm_id user (hanya contoh)
          $histori_transaksi =  $this->mymodel->getbywhere('permintaan',"transaction_id='".$transaction_id."' and order_id=",$order_id,"row");

          $id_histori_transaksi = $histori_transaksi->id;

          $fcm_member = $this->mymodel->getbywhere('member','member_id',$histori_transaksi->member_id,"row");
          $id_fcm_member = $fcm_member->fcm_id;
         

           if(!$histori_transaksi)
            $msg = array('status' => 0, 'message'=>'Terjadi kesalahan | histori_transaksi Tidak ditemukan');
           
            switch($status_code){
                case '200':

                    if($payment_type=='credit_card'){
                        if($transaction_status=='settlement'){
                        //Notif Sukses
                        $status_permintaan = "Sedang diproses";
                        $kode_status = 2;
                        $this->sendPushNotification(
                        //fcm id belum dinamis
                        /*'fHaXx0MrTuiD4jDtVc0A32:APA91bGAyid7yxTjt5ljiz0Yk1aKXVZ742rIVMSSBN99bDQ-7qaLXmG8j1iHHHLUPZTT9t7egDAMY_HqBBKKkD508qbH46izb9pnp0VDYHZsj1vbU7o44fdLLevgyNNZbeE5vrgCqNM7', */
                        $id_fcm_member,
                        'Pembayaran Berhasil', 
                        'Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.', 
                        $kode_status,
                        $id_histori_transaksi);
                        }
                        else {
                        //Notif Pending
                        $status_permintaan = "Menunggu pembayaran";
                        $kode_status = 1;
                        $this->sendPushNotification(
                            //fcm id belum dinamis
                             $id_fcm_member,
                            'Pembayaran Pending', 
                            'Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum Selasa, 20:30 WIB..', 
                            $kode_status,
                            $id_histori_transaksi);
                        }
                    }

                    else {
                    //Notif Sukses
                    $status_permintaan = "Sedang diproses";
                        $kode_status = 2;
                    $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member, 
                        'Pembayaran Berhasil', 
                        'Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.', 
                        $kode_status, 
                        $id_histori_transaksi);
                    }

                      
                    break;
                case '201':
                    //Notif Pending
                    $status_permintaan = "Menunggu pembayaran";
                    $kode_status = 2;
                    $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member,
                        'Pembayaran Pending', 
                        'Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum Selasa, 20:30 WIB.', 
                        $kode_status,
                        $id_histori_transaksi);

                    break;

                case '202' :
                   if($payment_type=='credit_card'){
                        //Notif denied
                    $status_permintaan = "denied";
                    $kode_status = 5;
                    $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member,
                        'Pembayaran Ditolak', 
                        'Pesanan ditolak oleh provider', 
                        $kode_status,
                        $id_histori_transaksi);
                   }
                   else {
                            //Notif expired
                    $status_permintaan = "Dibatalkan";
                    $kode_status = 3;
                    $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member, 
                        'Transaksi Dibatalkan', 
                        'Transaksi Dibatalkan karena melebihi tenggat waktu', 
                        $kode_status,
                        $id_histori_transaksi);
                   }
                    break;
            }

            $data = array(
              "status_permintaan" => $status_permintaan,
              "kode_status" => $kode_status
              );
            $this->mymodel->update('permintaan',$data,'id',$histori_transaksi->id);
            // $histori_transaksi->save();

            $this->response($msg);
            return response('Ok', 200)->header('Content-Type', 'text/plain');

       /* } catch (\Exception $e) {
            return response('Error', 404)->header('Content-Type', 'text/plain');
        }*/
    }

    static function sendPushNotification($fcm_token, $title, $message, $kode_status, $id_histori_transaksi , $id = null,$action = null) {  
     
        $url = "https://fcm.googleapis.com/fcm/send";            
        $header = [
            'authorization: key=AAAAfIemvrw:APA91bEqHB8ub4fYgb0AA_TtgZYIkAk0M65Bw0kdYnZsQ6WkmSjZ1H9A6VUDdPlHh7NqVe9QoownDrslpGh3JAPYRs4bu37qSbMaz-7Ob7kWmJVf8JW-LVulM2Tw4h8OLpirRi5xVdLQ',
            'content-type: application/json'
        ];    
     
        $notification = [
            // 'title' =>$title,
            'body' => $message,
            "content_available" => true,
            "click_action"=> "FLUTTER_NOTIFICATION_CLICK",
            "priority" => "high",
            "android_channel_id"=> "fengshui"
        ];

        $data = [
            "kode_status" => $kode_status,
            "id_histori_transaksi " => $id_histori_transaksi ,
            "priority" => "high",
            "content_available" > true
        ];

        $extraNotificationData = ["message" => $data,"id" =>$id,'action'=>$action];
     
        $fcmNotification = [
            'to'        => $fcm_token,
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
     
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
     
        $result = curl_exec($ch);    
        curl_close($ch);
     
        return $result;
    }
}
