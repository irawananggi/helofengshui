<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Buat_permintaan extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $kategori_id = $this->post('kategori_id');
          $status_permintaan = $this->post('status_permintaan');
          $form_datas = json_decode($this->post("form_datas"));

           $datax = array(
              "member_id" => $mem->member_id,
              "kategori_id" => $kategori_id,
              "kode_status" => 1,
              "status_permintaan" => 'Menunggu pembayaran',
              );

          $in =   $this->mymodel->insert('permintaan',$datax);
          $parent_id = $this->mymodel->getlast('permintaan','id')->id;
          foreach($form_datas as $k => $v){
              $detail["permintaan_id"] = $parent_id;
              $detail["member_id"] = $mem->member_id;
              foreach($v as $ki => $vi){
                  $detail[$ki] = $vi;
              }

              $save = $this->mymodel->insert("permintaan_detail", $detail);
          }

          if (!empty($save)) {
            $msg = array('status' => 1, 'message'=>'Berhasil Simpan data' ,'data'=>$data);
          }else {
            $msg = array('status' => 0, 'message'=>'Data gagal disimpan' ,'data'=>array());
          }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}