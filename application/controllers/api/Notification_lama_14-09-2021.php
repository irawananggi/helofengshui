<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;


require APPPATH . 'controllers/Midtrans/Config.php';
require APPPATH . 'controllers/Midtrans/CoreApi.php';
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Notification extends REST_Controller
{
    public function index_post(){
        $notif = file_get_contents('php://input');
        $notification_body = json_decode($notif, true);
        $order_id = $notification_body['order_id'];
        $transaction_id = $notification_body['transaction_id'];
        $status_code = $notification_body['status_code'];
        $transaction_status = $notification_body['transaction_status'];
        $payment_type = $notification_body['payment_type'];
        $fraud_status = $notification_body['fraud_status'];
       

            //untuk ambil fcm_id user (hanya contoh)
          $histori_transaksi =  $this->mymodel->getbywhere('permintaan',"transaction_id='".$transaction_id."' and order_id=",$order_id,"row");

          $id_histori_transaksi = $histori_transaksi->id;

          $sblm_tgl = konversi_tanggal("D, j M Y",substr($histori_transaksi->tenggat_waktu,0,10),"id");
          $fcm_member = $this->mymodel->getbywhere('member','member_id',$histori_transaksi->member_id,"row");
          $id_fcm_member = $fcm_member->fcm_id;
          
         /* cek($transaction_id);
        cek($order_id);
        cek($id_fcm_member);
        die();*/
          //$id_fcm_member = 'cyW7iNs2SKuqJ2ljVgiArT:APA91bGbmKYicY3BEcW1P8Of6PsjukxsPh89_lM2d970RTWfCMBJjBcHqE6MNxZ5-oQe1dJxWGxLfp5mzWpTf0SrSdCsAxAEXoUYGeaa3wRNcndHwWHLjaEQHV25AeYWvJ-iwjSOaqCu';

            $this->load->library("firebasenotif");
           if(!$histori_transaksi)
            $msg = array('status' => 0, 'message'=>'Terjadi kesalahan | histori_transaksi Tidak ditemukan');
            

            switch($status_code){
                case '200':

                    if($payment_type=='credit_card'){
                        if($transaction_status=='settlement'){
                        //Notif Sukses
                        $status_permintaan = "Sedang diproses";
                        $kode_status = 2;


                        /*$id_fcm_member = "cyW7iNs2SKuqJ2ljVgiArT:APA91bGbmKYicY3BEcW1P8Of6PsjukxsPh89_lM2d970RTWfCMBJjBcHqE6MNxZ5-oQe1dJxWGxLfp5mzWpTf0SrSdCsAxAEXoUYGeaa3wRNcndHwWHLjaEQHV25AeYWvJ-iwjSOaqCu";*/
                        $ss = $this->firebasenotif->notif("Pembayaran Berhasil", "Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

                        /*$this->sendPushNotification(
                        $id_fcm_member,
                        'Pembayaran Berhasil', 
                        'Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.', 
                        $kode_status,
                        $id_histori_transaksi);*/
                        }
                        else {
                        //Notif Pending
                        $status_permintaan = "Menunggu pembayaran";
                        $kode_status = 1;
                        


                        /*$id_fcm_member = "cyW7iNs2SKuqJ2ljVgiArT:APA91bGbmKYicY3BEcW1P8Of6PsjukxsPh89_lM2d970RTWfCMBJjBcHqE6MNxZ5-oQe1dJxWGxLfp5mzWpTf0SrSdCsAxAEXoUYGeaa3wRNcndHwWHLjaEQHV25AeYWvJ-iwjSOaqCu";*/
                            
                        $ss = $this->firebasenotif->notif("Pembayaran Pending", "Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum $sblm_tgl WIB..", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");
                                    
                        }
                    }

                    else {
                    //Notif Sukses
                    $status_permintaan = "Sedang diproses";
                    $kode_status = 2;

                    $ss = $this->firebasenotif->notif("Pembayaran Berhasil", "Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");


                   /* $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member, 
                        'Pembayaran Berhasil', 
                        'Selamat! Pembayaran telah terkonfirmasi. Transaksimu sedang diproses.', 
                        $kode_status, 
                        $id_histori_transaksi);
                    }*/

                      }
                    break;

                case '201':
                    //Notif Pending
                    $status_permintaan = "Menunggu pembayaran";
                    $kode_status = 1;
                    
                     $ss = $this->firebasenotif->notif("Pembayaran Pending", "Pesanan telah terkonfirmasi. Lakukan pembayaran sebelum $sblm_tgl WIB.", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");

      

                    break;

                case '202' :
                   if($payment_type=='credit_card'){
                        //Notif denied
                    $status_permintaan = "denied";
                    $kode_status = 5;


                     $ss = $this->firebasenotif->notif("Pembayaran Ditolak", "Pesanan ditolak oleh provider", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");


/*
                    $this->sendPushNotification(
                        //fcm id belum dinamis
                         $id_fcm_member,
                        'Pembayaran Ditolak', 
                        'Pesanan ditolak oleh provider', 
                        $kode_status,
                        $id_histori_transaksi);
                   */
                   } else {
                            //Notif expired
                    $status_permintaan = "Dibatalkan";
                    $kode_status = 3;
                    

                     $ss = $this->firebasenotif->notif("Transaksi Dibatalkan", "Transaksi Dibatalkan karena melebihi tenggat waktu", $id_fcm_member,$kode_status,$id_histori_transaksi, "FLUTTER_NOTICATION_CLICK");


                                    
                   }
                    break;
            }
      
            $data = array(
              "status_permintaan" => $status_permintaan,
              "kode_status" => $kode_status
              );
            $this->mymodel->update('permintaan',$data,'id',$histori_transaksi->id);
            $msg = array('status' => 1, 'message'=>'Sukses ', 'data'=>$ss);
            $this->response($msg);
    }

  
}
