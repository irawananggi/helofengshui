<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';

//require APPPATH . '/libraries/REST_Controller.php';

class Checkout_app extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
  

    public function index_post()
    {
       $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
        if ($token != '') {
          $member = $this->mymodel->getbywhere('member','token',$token,"row");
          $id_member = $member->member_id;

          if (!empty($member)) {
            $kategori_id = $this->post('kategori_id');
            $form_datas = json_decode($this->post("form_datas"));
            
            if (!empty($kategori_id)) {
                $kategori = $this->mymodel->getbywhere('kategori','id',$kategori_id,"row");
                $id = "HF-".date("Ymdhis").$kategori_id;
                $total = $kategori->harga;
                $cek_data = $this->mymodel->getbywhere('permintaan',"member_id='$id_member' AND kategori_id='$kategori_id' AND metode_trans='2' AND status_order='0'",null,"result");
                $cek_data2 = $this->mymodel->getbywhere('permintaan',"member_id='$id_member' AND kategori_id='$kategori_id' AND metode_trans='2' AND status_order='0'",null,"row");

                if(count($cek_data) > 0){
                   $data_upload = array( 
                      'metode_trans' => '2',
                      'id_produk' => $kategori->id_produk,
                      'tgl_bayar' => date('Y-m-d H:i:s'),
                      'tgl_expired' => date('Y-m-d H:i:s',strtotime('+1 day')),
                      'order_id'=>$id,
                      'token' => '',
                      'created_at' => date("Y-m-d H:i:s")
                    );
                   $in =  $this->mymodel->update('permintaan',$data_upload,'id',$cek_data2->id);
                  $parent_id = $this->mymodel->getlast('permintaan','id')->id;
                }
                else{
                  $data_in = array(
                    "kategori_id" => $kategori_id,
                    "metode_trans" => '2',
                    "id_produk" => $kategori->id_produk,
                    "member_id" => $member->member_id,
                    "tgl_bayar" => date('Y-m-d H:i:s'),
                    "tgl_expired" => date('Y-m-d H:i:s',strtotime('+1 day')),
                    "total_bayar" => $kategori->harga,
                    'status_order' => "0",
                    'kode_status' => "1",
                    "status_permintaan" => 'Menunggu pembayaran',
                    "order_id" => $id,
                    "token" => 'er',
                    "created_at" => date("Y-m-d H:i:s")
                  );
                  $in = $this->mymodel->insert("permintaan",$data_in);
                  $parent_id = $this->mymodel->getlast('permintaan','id')->id;
                    foreach($form_datas as $k => $v){
                      $detail["permintaan_id"] = $parent_id;
                      $detail["member_id"] = $id_member;
                      foreach($v as $ki => $vi){
                          $detail[$ki] = $vi;
                      }
                      $save = $this->mymodel->insert("permintaan_detail", $detail);
                    }
                }


                $get_trans = $this->mymodel->getbywhere('permintaan','id',$parent_id,"row");
                if($get_trans->kode_status == 1){
                  $status = 'Menunggu Pembayaran';
                }elseif($get_trans->kode_status == 2){
                  $status = 'Selesai';
                }else{
                  $status = 'Expire';
                }
                $data = array(
                    "kategori_id" => $kategori_id,
                    "id_produk_apple" => $kategori->id_produk,
                    "total_bayar" => $kategori->harga,
                    "nama_langganan" =>$member->nama_lengkap,
                    "order_id" => $id,
                    "status" => $status,
                  );

                
                $msg = array('status' => 1, 'message'=>'Invoice Created',  'data' => $data);
            } 
            else{
                $msg = array('status' => 0, 'message'=>'Input tidak valid','data'=>array());
                $status="422";
            }
          }else{
            $msg = array('status' => 0, 'message'=>'Member Tidak ditemukan','data'=>array());
            $status="401";
          }
        }else{
          $msg = array('status' => 0, 'message'=>'Token Tidak ditemukan','data'=>array());
          $status="401";
        }       
        $this->response($msg,$status);
        die();
    }

}
