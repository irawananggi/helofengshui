<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Detail_kategori extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }       
            $id = $this->get('id');

            $get_kategori = $this->mymodel->getbywhere('kategori','id',$id,'row');

            $data = $this->mymodel->getbywheresort('form_kategori','kategori_id',$get_kategori->id,'jenis_form_id','ASC','result');
             /* cek(count($data));
            die();*/
            /*
            $data = $this->mymodel->getbywhere('form_kategori','kategori_id',$get_kategori->id,'result');*/
            foreach ($data as $key => $value) {
              
             $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$value->jenis_form_id,'row');
             $value->jenis_input = $get_jenis_form->name_jf;
             /* $value->foto = base_url("uploads/file/".$get_kategori->foto);
              $value->name_in = $get_kategori->name_in;
              $value->deskripsi_in = $get_kategori->deskripsi_in;
              $value->icon = base_url("uploads/file/".$get_kategori->icon);
              $value->ketentuan_in = $get_kategori->ketentuan_in;
              $value->harga = $get_kategori->harga;
              $value->jenis_input = $get_jenis_form->name_jf;
*/

             /* $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$value->jenis_form_id,'row');
              $value->jenis_form_id = $get_jenis_form->nama_jf;
              $value->name_cat_in = $get_jenis_form->name_cat_in;
              $value->name_cat_en = $get_jenis_form->name_cat_en;
              $value->name_cat_man = $get_jenis_form->name_cat_man;*/
              //$value->link = site_url('promo/product/'.$value->Kategori_id);
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }


          $this->response($msg);

    }
}
