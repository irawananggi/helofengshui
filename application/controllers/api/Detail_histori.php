<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Detail_histori extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }       
          $id = $this->get('id');
          $data = $this->mymodel->getbywhere('permintaan','id',$id,'result');
          foreach ($data as $key => $value) {
            
            $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
            $get_telpon = $this->mymodel->getbywhere('parameter','param','notelp','row');
            $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');

            $string = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_in)); 
            // ----- remove control characters ----- 
            $string = str_replace("\r", '', $string);
            $string = str_replace("\n", ' ', $string);
            $string = str_replace("\t", ' ', $string);
            $string = str_replace("&nbsp;", ' ', $string);
            $string = trim(preg_replace('/ {2,}/', ' ', $string));

           
            $string2 = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_en)); 
            // ----- remove control characters ----- 
            $string2 = str_replace("\r", '', $string2);
            $string2 = str_replace("\n", ' ', $string2);
            $string2 = str_replace("\t", ' ', $string2);
            $string2 = str_replace("&nbsp;", ' ', $string2);
            $string2 = trim(preg_replace('/ {2,}/', ' ', $string2));

           
            $string3 = preg_replace ('/<[^>]*>/', ' ', html_entity_decode($value->jawaban_man)); 
            // ----- remove control characters ----- 
            $string3 = str_replace("\r", '', $string3);
            $string3 = str_replace("\n", ' ', $string3);
            $string3 = str_replace("\t", ' ', $string3);
            $string3 = str_replace("&nbsp;", ' ', $string3);
            $string3 = trim(preg_replace('/ {2,}/', ' ', $string3));

           
            /*$value->jawaban_in = $string;
            $value->jawaban_en = $string2;
            $value->jawaban_man = $string3; */
            $value->jawaban_in = $value->jawaban_in;
            $value->jawaban_en = $value->jawaban_en;
            $value->jawaban_man = $value->jawaban_man;
            $value->foto_kategori = base_url("uploads/file/".$get_kategori->foto);
            $value->name_in = $get_kategori->name_in;
            $value->name_en = $get_kategori->name_en;
            $value->name_man = $get_kategori->name_man;
            $value->deskripsi_in = $get_kategori->deskripsi_in;
            $value->deskripsi_en = $get_kategori->deskripsi_en;
            $value->deskripsi_man = $get_kategori->deskripsi_man;
            $value->ketentuan_in = $get_kategori->ketentuan_in;
            $value->ketentuan_en = $get_kategori->ketentuan_en;
            $value->ketentuan_man = $get_kategori->ketentuan_man;
            $value->hub_cs = $get_kategori->hub_cs;
            $value->notelp = '62'.$get_telpon->val;
            $value->kategori_kematian = $get_kategori->kategori_kematian;
            $value->harga = $get_kategori->harga;
            $value->name_sp_in = $get_status->name_sp_in;
            $value->name_sp_en = $get_status->name_sp_en;
            $value->name_sp_man = $get_status->name_sp_man;
            $value->url_invoice = (($value->kode_status != 1 AND $value->payment_type != NULL)?'':'https://app.veritrans.co.id/snap/v2/vtweb/'.$value->token);


            $value->detail_form = $this->mymodel->getbywhere('permintaan_detail','permintaan_id',$value->id,'result');

            foreach ($value->detail_form as $key3 => $value3) {
              $form_kategori_2 = $this->mymodel->getbywhere('form_kategori','id',$value3->form_kategori_id,'row');
              
              $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$form_kategori_2->jenis_form_id,'row');

              $value3->jenis_form_id = $form_kategori_2->jenis_form_id;
              $value3->name_cat_in = $form_kategori_2->name_cat_in;
              $value3->name_cat_en = $form_kategori_2->name_cat_en;
              $value3->name_cat_mann = $form_kategori_2->name_cat_man;
            }
            $value->status_testimoni = $this->mymodel->getbywhere('testimoni','permintaan_id',$value->id,'result');

            foreach ($value->status_testimoni as $key4 => $value4) {
              $value4->isi = $value4->isi;
            }
          }
          if (!empty($data)) {
            $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
          }else {
            $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
          }


          $this->response($msg);

    }
}
