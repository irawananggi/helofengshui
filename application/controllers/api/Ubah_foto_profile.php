<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Ubah_foto_profile extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    // insert new data to account
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {
              $uploaddir = './uploads/member/';
              $img = explode('.', $_FILES['img']['name']);
              $extension = end($img);
              $file_name =  md5(date('y-m-d h:i:s').$_FILES['img']['name']).".".$extension;
              $uploadfile = $uploaddir.$file_name;
              $status = 0;
              unlink($uploaddir.$mem->img_file);
              if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                /*if ($mem->img_file!="custom.png") {
                  // unlink($uploaddir.$mem->foto);
                }*/
                $data_upload = array('img_file'=>$file_name);
                $this->mymodel->update('member',$data_upload,'member_id',$mem->member_id);
                $datax = $this->mymodel->getbywhere('member','token',$token,"row");
                $img_file = base_url('/uploads/member/'.$datax->img_file);
                
                $msg = array('status'=>1,'message'=>'Upload Foto Berhasil','data' => $img_file);
              } else {
                  $msg = array('status'=>0,'message'=>'Gagal Upload Foto');
              }
          }else {
              $msg = array('status' => 1, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

    }


}
?>
