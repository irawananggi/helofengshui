<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Voucher extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");

          //$kode_transaksi = $this->post('kode_transaksi');
          if (isset($mem)) {
            $search = $this->get("search");
            $date_saiki=date('Y-m-d');

            /*$date_saiki=date('Y-m-d');
            $data = @$this->general_model->datagrabs(array(
              'tabel'=>'voucher','where'=>array('code_voucher'=>$id,'start_date <= "'.$date_saiki.'" AND end_date >= "'.$date_saiki.'"'=>NULL)
            ));*/


              if(!empty($search)){
                $data = $this->mymodel->withquery("SELECT * 
                      FROM voucher 
                      WHERE NOT EXISTS 
                          (SELECT voucher_id
                     FROM permintaan 
                     WHERE voucher.id = permintaan.voucher_id AND permintaan.member_id = $mem->member_id) AND start_date <= '".$date_saiki."' AND end_date >= '".$date_saiki."' AND status=1 name_voucher like '%".$search."%' ",'result');
              } else{
                $data = $this->mymodel->withquery("SELECT * 
                      FROM voucher 
                      WHERE NOT EXISTS 
                          (SELECT voucher_id
                     FROM permintaan 
                     WHERE voucher.id = permintaan.voucher_id AND permintaan.member_id = $mem->member_id) AND start_date <= '".$date_saiki."' AND end_date >= '".$date_saiki."' AND status=1 ",'result');
                }
               
                foreach ($data as $key => $value) {
                  /*$get_voucher_user = $this->mymodel->getbywhere('permintaan',"member_id='".$mem->member_id."' and voucher_id=",$value->id,'result');*/
                  $get_member_voucher = $this->mymodel->getbywhere('member_voucher',"member_id='".$mem->member_id."' and voucher_id=",$value->id,'result');
                  
                  if(count($get_member_voucher) == 0){
                    $value->status = 'Gunakan';
                  }else{

                    $value->status = 'Batalkan';
                  }
                  if($value->dis_id == '1'){

                    $value->nominal_string = $value->nominal.'%';
                  }else{

                    $value->nominal_string = 'Rp '.numberToCurrency($value->nominal);

                  }
                
                    $value->min_s_string = 'Rp '.numberToCurrency($value->min_s);
              }
              if (!empty($data)) {
                if($id){
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data[0]);
                } else {
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
                }
              }else {
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
              }
        }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}