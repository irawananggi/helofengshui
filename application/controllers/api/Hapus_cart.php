<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Hapus_cart extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          
          if (isset($mem)) {
              
              $get_cart = $this->mymodel->getbywhere('cart','member_id',$mem->member_id,"row");
              if($get_cart->voucher_id != NULL){

                $hapus = $this->mymodel->delete2('member_voucher','member_id',$mem->member_id,'voucher_id',$get_cart->voucher_id);
              }
              $hapus2 = $this->mymodel->delete('cart_detail','member_id',$mem->member_id);
              $hapus3 = $this->mymodel->delete('cart','member_id',$mem->member_id);
              if ($hapus OR $hapus2 OR $hapus3) {                
                $msg = array('status' => 1, 'message'=>'Berhasil menghapus cart' ,'data'=>array());
              }else {
                $msg = array('status' => 0, 'message'=>'Gagal menghapus cart' ,'data'=>array());
              } 
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }
}