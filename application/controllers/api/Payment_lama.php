<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found

/*use Controller\api\Midtrans\config;
use Controllers\api\Midtrans\CoreApi as MidtransCoreApi;*/

require APPPATH . 'controllers/Midtrans/Config.php';
require APPPATH . 'controllers/Midtrans/CoreApi.php';/*
require APPPATH . 'controllers/Midtrans/ApiRequestor.php';
require APPPATH . 'controllers/Midtrans/Sanitizer.php';
require APPPATH . 'controllers/Midtrans/Snap.php';
require APPPATH . 'controllers/Midtrans/SnapApiRequestor.php';*/
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Payment extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function buatPermintaan_post()
    {

    
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
 
          if (isset($mem)) {

            $result = null;
            $order_id = $this->input->post('order_id');
            $payment_type = $this->input->post('payment_type');
            $bank_name = $this->input->post('bank_name');
            $token_id = $this->input->post('token_id');
            $store = $this->input->post('store'); 
            $internet_banking_type = $this->input->post('internet_banking_bank');
            $klik_bca_user_id = $this->input->post('klik_bca_user_id'); 
            $e_wallet_type = $this->input->post('e_wallet_type');
            $acquirer_type = $this->input->post('acquirer_type');
            $cardless_type = $this->input->post('cardless_type');
            $kategori_id = $this->post('kategori_id');
            $form_datas = json_decode($this->post("form_datas"));

          $kategori = $this->mymodel->getbywhere('kategori','id',$kategori_id,"row");
          $id_s = md5(uniqid().$mem->member_id.$kategori_id.date('Y-m-d'));
          $id_member = $mem->member_id;
          
           $transaction = array(
                "transaction_details" => [
                    "gross_amount"=> $kategori->harga,
                    "order_id"=> $order_id
                ],
                "customer_details" => [
                    "email" => $mem->email,
                    "first_name" => $mem->nama_lengkap,
/*                    "last_name" => $mem->nama_lengkap,*/
                    "phone" => $mem->phone
                ],
    
                "item_details" => array(
                    [
                        "id" => $id_s,
                        "price" => $kategori->harga,
                        "quantity" => 1,
                        "name" => $kategori->name_in
                     ]
                ),
            );


            switch($payment_type) {
              case 'bank_transfer' : $result = $this->chargeBankTransfer($order_id, $transaction, $bank_name,$id_s,$id_member,$kategori_id,$form_datas);
              break;
              case 'credit_card' : $result = $this->chargeCreditCard($order_id, $token_id, $transaction,$id_s,$id_member,$kategori_id,$form_datas);
              break;
              case 'counter' :  $result = $this->chargeOverCounter($order_id,$transaction, $store,$id_s,$id_member,$kategori_id,$form_datas);
              break;
              case 'internet_banking' : $result = $this->chargeInternetBanking($order_id, $transaction, $internet_banking_type, $klik_bca_user_id,$id_s,$id_member,$kategori_id,$form_datas);
              break;
              case 'e_wallet' : $result = $this->chargeEwallet($order_id, $transaction, $e_wallet_type, $acquirer_type,$id_s,$id_member,$kategori_id,$form_datas);
              break;
              case 'cardless_credit' : $result = $this->chargeCardlessCredit($order_id, $transaction, $cardless_type,$id_s,$id_member,$kategori_id,$form_datas);
              break;
            }

           return $result;

              //$msg = array('status' => 1, 'message'=>'Token ada' ,'data'=>$result);
           
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }



    function chargeBankTransfer($order_id, $transaction_object, $bank_name,$id_s,$id_member,$kategori_id,$form_datas){
       
            $transaction = $transaction_object; 
 
            if($bank_name=='echannel'){
                $transaction['payment_type'] = 'echannel'; 
                $transaction['echannel'] = [
                    "bill_info1" => "Payment For:",
                    "bill_info2" => "debt"
                ];
            }
            else {
                $transaction['payment_type'] = 'bank_transfer'; 
                $transaction['bank_transfer'] = [
                    "bank" => $bank_name,
                ];
            }


          /*  $transaction['custom_expiry'] = [
                "order_time": date('yyyy-MM-dd hh:mm:ss Z'),
                "expiry_duration": 60,
                "unit": "minute"
            ];
*/
           

            $charge = CoreApi::charge($transaction);
            if(!$charge){
              
                $msg = array('status' => 0, 'message'=>'Terjadi Kesalahan dalam charge');
            }

            $name_va_number=NULL;
            $name_bank=NULL;
            if($bank_name=='permata'){
              $name_va_number = $charge->permata_va_number;
              $name_bank = 'permata';
            }
            else if($bank_name=='echannel'){
              $name_va_number = $charge->bill_key;
              $name_bank = 'echannel';
            }
            else {
              $name_va_number = $charge->va_numbers[0]->va_number;
              $name_bank =  $charge->va_numbers[0]->bank;

            }
/*
            $histori_transaksi->tenggat_waktu = '';*/
            $tenggat_waktu = '2';

            $datax = array(
                "member_id" => $id_member,
                "order_id" => $charge->order_id,
                "transaction_id" => $charge->transaction_id,
                "token_id" => $charge->token_id,
                "payment_type" => $charge->payment_type,
                "bank" => $name_bank,
                "store" => $charge->store,
                "va_number" => $name_va_number,
                "redirect_url" => $charge->redirect_url,
                "payment_code" => $charge->payment_code,
                "bill_key" => $charge->bill_key,
                "tenggat_waktu" => $tenggat_waktu,
                "kategori_id" => $kategori_id,
                "kode_status" => 1,
                "status_permintaan" => 'Menunggu pembayaran',
              );
           /*cek($datax);
            die();*/
            $in =   $this->mymodel->insert('permintaan',$datax);

            $parent_id = $this->mymodel->getlast('permintaan','id')->id;
              foreach($form_datas as $k => $v){
                  $detail["permintaan_id"] = $parent_id;
                  $detail["member_id"] = $id_member;
                  foreach($v as $ki => $vi){
                      $detail[$ki] = $vi;
                  }

                  $save = $this->mymodel->insert("permintaan_detail", $detail);
              }


            $data_result = $this->mymodel->getbywhere('permintaan','id',$parent_id,'result');
            foreach ($data_result as $key => $value) {
              
              $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
              $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');



              $value->foto_kategori = base_url("uploads/file/".$get_kategori->foto);
              $value->name_in = $get_kategori->name_in;
              $value->name_en = $get_kategori->name_en;
              $value->name_man = $get_kategori->name_man;
              $value->deskripsi_in = $get_kategori->deskripsi_in;
              $value->deskripsi_en = $get_kategori->deskripsi_en;
              $value->deskripsi_man = $get_kategori->deskripsi_man;
              $value->ketentuan_in = $get_kategori->ketentuan_in;
              $value->ketentuan_en = $get_kategori->ketentuan_en;
              $value->ketentuan_man = $get_kategori->ketentuan_man;
              $value->hub_cs = $get_kategori->hub_cs;
              $value->kategori_kematian = $get_kategori->kategori_kematian;
              $value->harga = $get_kategori->harga;
              $value->name_sp_in = $get_status->name_sp_in;
              $value->name_sp_en = $get_status->name_sp_en;
              $value->name_sp_man = $get_status->name_sp_man;


              $value->detail_form = $this->mymodel->getbywhere('permintaan_detail','permintaan_id',$value->id,'result');

              foreach ($value->detail_form as $key3 => $value3) {

                $form_kategori_2 = $this->mymodel->getbywhere('form_kategori','id',$value3->form_kategori_id,'row');
                
                $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$form_kategori_2->jenis_form_id,'row');

                $value3->name_cat_in = $form_kategori_2->name_cat_in;
                $value3->name_cat_en = $form_kategori_2->name_cat_en;
                $value3->name_cat_mann = $form_kategori_2->name_cat_man;
              }

            }

            
             if (!empty($data_result)) {
                $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data_result);
              }else {
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
              }

          $this->response($msg);
    }

    
    function chargeCreditCard($order_id, $token_id, $transaction_object){
       /* cek($transaction_object);
        die();*/
            $credit_card = array(
                'token_id' =>  $token_id,
                'authentication' => true,
            );

            $transaction = $transaction_object;
            $transaction['payment_type'] = 'credit_card'; 
            $transaction['credit_card'] = $credit_card;
            $charge = CoreApi::charge($transaction);

            if(!$charge){
                $msg = array('status' => 0, 'message'=>'Terjadi Kesalahan | Gagal Charge');
            }
            $tenggat_waktu = '2';

            $datax = array(
                "member_id" => $id_member,
                "order_id" => $charge->order_id,
                "transaction_id" => $charge->transaction_id,
                "token_id" => $charge->token_id,
                "payment_type" => $charge->payment_type,
                "bank" => $name_bank,
                "store" => $charge->store,
                "va_number" => $name_va_number,
                "redirect_url" => $charge->redirect_url,
                "payment_code" => $charge->payment_code,
                "bill_key" => $charge->bill_key,
                "tenggat_waktu" => $tenggat_waktu,
                "kategori_id" => $kategori_id,
                "kode_status" => 1,
                "status_permintaan" => 'Menunggu pembayaran',
              );
           /*cek($datax);
            die();*/
            $in =   $this->mymodel->insert('permintaan',$datax);

            $parent_id = $this->mymodel->getlast('permintaan','id')->id;
              foreach($form_datas as $k => $v){
                  $detail["permintaan_id"] = $parent_id;
                  $detail["member_id"] = $id_member;
                  foreach($v as $ki => $vi){
                      $detail[$ki] = $vi;
                  }

                  $save = $this->mymodel->insert("permintaan_detail", $detail);
              }

            $data_result = $this->mymodel->getbywhere('permintaan','id',$parent_id,'result');
            foreach ($data_result as $key => $value) {
              
              $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
              $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');



              $value->foto_kategori = base_url("uploads/file/".$get_kategori->foto);
              $value->name_in = $get_kategori->name_in;
              $value->name_en = $get_kategori->name_en;
              $value->name_man = $get_kategori->name_man;
              $value->deskripsi_in = $get_kategori->deskripsi_in;
              $value->deskripsi_en = $get_kategori->deskripsi_en;
              $value->deskripsi_man = $get_kategori->deskripsi_man;
              $value->ketentuan_in = $get_kategori->ketentuan_in;
              $value->ketentuan_en = $get_kategori->ketentuan_en;
              $value->ketentuan_man = $get_kategori->ketentuan_man;
              $value->hub_cs = $get_kategori->hub_cs;
              $value->kategori_kematian = $get_kategori->kategori_kematian;
              $value->harga = $get_kategori->harga;
              $value->name_sp_in = $get_status->name_sp_in;
              $value->name_sp_en = $get_status->name_sp_en;
              $value->name_sp_man = $get_status->name_sp_man;


              $value->detail_form = $this->mymodel->getbywhere('permintaan_detail','permintaan_id',$value->id,'result');

              foreach ($value->detail_form as $key3 => $value3) {

                $form_kategori_2 = $this->mymodel->getbywhere('form_kategori','id',$value3->form_kategori_id,'row');
                
                $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$form_kategori_2->jenis_form_id,'row');

                $value3->name_cat_in = $form_kategori_2->name_cat_in;
                $value3->name_cat_en = $form_kategori_2->name_cat_en;
                $value3->name_cat_mann = $form_kategori_2->name_cat_man;
              }

            }

            
             if (!empty($data_result)) {
                $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data_result);
              }else {
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
              }

          $this->response($msg);
            // $histori_transaksi = new HistoriTransaksi();
            // $histori_transaksi->order_id = $histori_transaksi_id;
            // $histori_transaksi->transaction_id = $charge->transaction_id;
            // $histori_transaksi->status = "PENDING";

            // perlu redirect_url

            //tambahkan redirect_url, payment_type di detail history transaksi
            // $histori_transaksi->redirect_url = $charge->redirect_url;
            // $histori_transaksi->payment_type = $charge->payment_type;
            // $histori_transaksi->tenggat_waktu = masukkan tenggat waktu;

            //waktu expire bisa dicek di link berikut:
            //https://api-docs.midtrans.com/#code-2xx
            
            //jika ingin custom tenggat waktu tambahkan field berikut, credit card method tidak bisa custom, dan qris & shopeepay max 60 menit
            // "custom_expiry": {
            //     "order_time": "2016-12-07 11:54:12 +0700",
            //     "expiry_duration": 60,
            //     "unit": "minute"
            // }

            // if(!$histori_transaksi->save())
            // return false;

           /* return ['code' => 1, 'message' => 'Berhasil', 'data' => 'data yang mas return seperti di detail histori', 'result' => $charge];*/

       

    }

    function chargeOverCounter($order_id, $transaction_object, $store){
        
        try {
            
            $transaction = $transaction_object; 
            $transaction['payment_type'] = 'cstore'; 
            $transaction['cstore'] = [
                "store" => $store,
            ];

            $charge = CoreApi::charge($transaction);
            if(!$charge){
                return ['code' => 0, 'message' => 'Terjadi Kesalahan dalam charge'];
            }


            // $histori_transaksi = new HistoriTransaksi();
            // $histori_transaksi->order_id = $charge->$order_id;
            // $histori_transaksi->transaction_id = $charge->transaction_id;
            // $histori_transaksi->status = "PENDING";

            //tambahkan payment_type, payment_code, store, tenggat waktu, di detail history transaksi
            // $historyTransaksi->payment_type = $charge->payment_type;
            // $historyTransaksi->payment_code = $charge->payment_code;
            if($store=='alfamart'){
                // $historyTransaksi->store = 'alfamart';
            }
            else {
                // $historyTransaksi->store = 'indomaret';

            }
            // $historyTransaksi->tenggat_waktu = masukkan tenggat waktu;

            //waktu expire bisa dicek di link berikut:
            //https://api-docs.midtrans.com/#code-2xx
            
            //jika ingin custom tenggat waktu tambahkan field berikut, credit card method tidak bisa custom, dan qris & shopeepay max 60 menit
            // "custom_expiry": {
            //     "order_time": "2016-12-07 11:54:12 +0700",
            //     "expiry_duration": 60,
            //     "unit": "minute"
            // }

            // if(!$histori_transaksi->save())
            // return false;


            return ['code' => 1, 'message' => 'Success', 'data' => 'data yang mas return seperti di detail histori', 'result' => $charge];

        } catch (\Exception $e) {
            dd($e);
            //ini sesuaikan saja sama style masnya
            return ['code' => 0, 'message' => 'Terjadi Kesalahan'];
        }

    }

    function chargeInternetBanking($order_id, $transaction_object, $internet_banking_type, $klikbcaUserID){
        
        try {
            
            $transaction = $transaction_object; 
            $transaction['payment_type'] = $internet_banking_type; 

            if($internet_banking_type=='bca_klikpay' || $internet_banking_type=='cimb_clicks'){
                $transaction[$internet_banking_type] = [
                    "description" => 'Pembelian barang', // required
                ];
            }
            else if($internet_banking_type=='bca_klikbca'){
                $transaction[$internet_banking_type] = [
                    "description" => 'Pembelian barang', // required
                    "user_id" => $klikbcaUserID // required
                ];

            }

           

            $charge = CoreApi::charge($transaction);
            if(!$charge){
                return ['code' => 0, 'message' => 'Terjadi Kesalahan dalam charge'];
            }


            // $histori_transaksi = new HistoriTransaksi();
            // $histori_transaksi->order_id = $charge->$order_id;
            // $histori_transaksi->transaction_id = $charge->transaction_id;
            // $histori_transaksi->status = "PENDING";

            
            // perlu redirect_url

            //tambahkan redirect_url, payment_type, tenggat waktu di detail history transaksi
            // $historyTransaksi->redirect_url = $charge->redirect_url;
            // $historyTransaksi->payment_type = 'internet_banking';
            // $historyTransaksi->tenggat_waktu = masukkan tenggat waktu;

            //waktu expire bisa dicek di link berikut:
            //https://api-docs.midtrans.com/#code-2xx
            
            //jika ingin custom tenggat waktu tambahkan field berikut, credit card method tidak bisa custom, dan qris & shopeepay max 60 menit
            // "custom_expiry": {
            //     "order_time": "2016-12-07 11:54:12 +0700",
            //     "expiry_duration": 60,
            //     "unit": "minute"
            // }

            // if(!$histori_transaksi->save())
            // return false;


            return ['code' => 1, 'message' => 'Success', 'data' => 'data yang mas return seperti di detail histori', 'result' => $charge];

        } catch (\Exception $e) {
            dd($e);
            //ini sesuaikan saja sama style masnya
            return ['code' => 0, 'message' => 'Terjadi Kesalahan'];
        }

    }

    
    function chargeEwallet($order_id, $transaction_object, $e_wallet_type, $acquirer_type){
        
        try {
            
            $transaction = $transaction_object; 
            $transaction['payment_type'] = $e_wallet_type; 

            if($e_wallet_type=='qris'){
                $transaction[$e_wallet_type] = [
                    "acquirer" => $acquirer_type, // bisa gopay atau shopeepay
                ];
            }
            else if($e_wallet_type=='gopay'){
                $transaction[$e_wallet_type] = [
                    "enable_callback" => true, 

                    //callback ketika user telah menyelesaikan pembayaran di aplikasi gojek, bisa berupa http atau deeplink, default nya ada dashboard midtrans bagian finish
                    // "callback_url" => "someapps://callback" 
                ];

            }

            else if($e_wallet_type=='shopeepay'){
                $transaction[$e_wallet_type] = [
                    //callback ketika user telah menyelesaikan pembayaran di aplikasi gojek, bisa berupa http atau deeplink, default nya ada dashboard midtrans bagian finish
                    // "callback_url" => "someapps://callback" 
                ];

            }

           

            $charge = CoreApi::charge($transaction);
            if(!$charge){
                return ['code' => 0, 'message' => 'Terjadi Kesalahan dalam charge'];
            }


            // $histori_transaksi = new HistoriTransaksi();
            // $histori_transaksi->order_id = $charge->$order_id;
            // $histori_transaksi->transaction_id = $charge->transaction_id;
            // $histori_transaksi->status = "PENDING";

            
            // perlu redirect_url

            //tambahkan redirect_url, payment_type, tenggat waktu di detail history transaksi
            if($e_wallet_type=='gopay'){
            // $history_transaksi->redirect_url = $charge->actions[1]->url;
            }
            else {
                // $history_transaksi->redirect_url = $charge->actions[0]->url;
            }   
                
            // $history_transaksi->payment_type = 'e_wallet';

            // $history_transaksi->tenggat_waktu = masukkan tenggat waktu;

            //waktu expire bisa dicek di link berikut:
            //https://api-docs.midtrans.com/#code-2xx
            
            //jika ingin custom tenggat waktu tambahkan field berikut, credit card method tidak bisa custom, dan qris & shopeepay max 60 menit
            // "custom_expiry": {
            //     "order_time": "2016-12-07 11:54:12 +0700",
            //     "expiry_duration": 60,
            //     "unit": "minute"
            // }

            // if(!$histori_transaksi->save())
            // return false;


            return ['code' => 1, 'message' => 'Success', 'data' => 'data yang mas return seperti di detail histori', 'result' => $charge];

        } catch (\Exception $e) {
            dd($e);
            //ini sesuaikan saja sama style masnya
            return ['code' => 0, 'message' => 'Terjadi Kesalahan'];
        }

    }

    function chargeCardlessCredit($order_id, $transaction_object, $cardless_type){
        
        try {
            
            $transaction = $transaction_object; 
            $transaction['payment_type'] = $cardless_type; 


            $charge = CoreApi::charge($transaction);
            if(!$charge){
                return ['code' => 0, 'message' => 'Terjadi Kesalahan dalam charge'];
            }


            // $histori_transaksi = new HistoriTransaksi();
            // $histori_transaksi->order_id = $charge->$order_id;
            // $histori_transaksi->transaction_id = $charge->transaction_id;
            // $histori_transaksi->status = "PENDING";

            
            // perlu redirect_url

            //tambahkan redirect_url, payment_type, tenggat waktu, di detail history transaksi
            // $history_transaksi->redirect_url = $charge->redirect_url;
            // $history_transaksi->payment_type = 'cardless_credit';
            // $history_transaksi->tenggat_waktu = masukkan tenggat waktu;

            //waktu expire bisa dicek di link berikut:
            //https://api-docs.midtrans.com/#code-2xx
            
            //jika ingin custom tenggat waktu tambahkan field berikut, credit card method tidak bisa custom, dan qris & shopeepay max 60 menit
            // "custom_expiry": {
            //     "order_time": "2016-12-07 11:54:12 +0700",
            //     "expiry_duration": 60,
            //     "unit": "minute"
            // }

            // if(!$histori_transaksi->save())
            // return false;


            return ['code' => 1, 'message' => 'Success', 'data' => 'data yang mas return seperti di detail histori', 'result' => $charge];

        } catch (\Exception $e) {
            dd($e);
            //ini sesuaikan saja sama style masnya
            return ['code' => 0, 'message' => 'Terjadi Kesalahan'];
        }

    }







    public function getTokenCreditCard_post(){
          
            $client_key = $this->input->post('client_key');
            $card_number = $this->input->post('card_number');
            $card_exp_month = $this->input->post('card_exp_month');
            $card_exp_year = $this->input->post('card_exp_year');
            $card_cvv = $this->input->post('card_cvv');

            $cc_data = [
                'client_key' => $client_key,
                'card_number' => $card_number,
                'card_exp_month' => $card_exp_month,
                'card_exp_year' => $card_exp_year,
                'card_cvv' => $card_cvv,
            ];

            $data = http_build_query($cc_data);
            $token = CoreApi::token($data);
           
            

            if (!empty($token)) {
                $token_id = json_decode($token->original);
                $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$token);
              }else {
                $msg = array('status' => 0, 'message'=>'Ada yang salah | Credit Card Tidak Valid / Didukung');
              }

          $this->response($msg);

/*

            if(!$token){
                return ['code' => 0, 'message' => 'Ada yang salah | Credit Card Tidak Valid / Didukung'];
            }

            $token_id = json_decode($token->original);
            return ['code' => 1, 'message' => 'Sukses', 'result' => $token_id];

*/
         
    }


}