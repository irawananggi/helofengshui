<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Member_address extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $data = $this->mymodel->getbywhere('member_address','member_id',$mem->member_id,'result');
            foreach ($data as $key => $value) {
              if ($value->updated_at==null) {
                $value->updated_at = "";
              }
              $value->province = $this->get_province($value->province_id)->name;
              $value->city = $this->get_city($value->province_id,$value->city_id)->name;
              $value->subdistrict = $this->get_subdistrict($value->subdistrict_id)->name;
              if (empty($value->province)) {
              $value->province = "Tidak Ditemukan";
              }
              if (empty($value->city)) {
                $value->city = "Tidak Ditemukan";
              }
              if (empty($value->subdistrict)) {
                $value->subdistrict = "Tidak Ditemukan";
              }
              $value->alamat_destinasi = $value->village.", ".$value->subdistrict.", ".$value->city.", ".$value->province;
            }
            
            
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else if (empty($data)) {
             $msg = array('status' => 1, 'message'=>'Alamat Tidak ditemukan!' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

    public function get_province($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;
    }

}