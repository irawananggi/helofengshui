<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Cek_transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $data = $this->mymodel->withquery("select * from htrans 
              where member_id='".$mem->member_id."' 
              order by htrans_id desc",'result');
            foreach ($data as $key => $value) {
              if ($value->updated_at==null) {
                $value->updated_at = "";
              }
              $get_status = $this->mymodel->getbywhere('transaction_status','transaction_status_id',$value->transaction_status_id,'row');
              $value->status_pesanan = $get_status->description_in_indonesia;
              if ($value->status_pesanan==null) {
                $value->status_pesanan = "";
              }
              $value->total_price = "Rp ".number_format($value->total_price,0,"",".");
            //alamat by id
              $idp = $this->get_province($value->province_id)->name;
              $idc = $this->get_city($value->province_id,$value->city_id)->name;
              $ids = $this->get_subdistrict($value->subdistrict_id)->name;
              if ($idp==null) {
                $idp = "Tidak Ditemukan";
              }
              if ($idc==null) {
                $idc = "Tidak Ditemukan";
              }
              if ($ids==null) {
                $ids = "Tidak Ditemukan";
              }
              if ($value->updated_at == null) {
                $value->updated_at = "";
              }
              $value->province = $idp;
              $value->city = $idc;
              $value->subdistrict = $ids;

              //get detail transaction
              $value->detail_trans = $this->mymodel->getbywhere("dtrans","htrans_id",$value->htrans_id,"result");
              foreach ($value->detail_trans as $key2 => $value2) {
                $get_product = $this->mymodel->getbywhere("product","product_id",$value2->product_id,"row");
                $value2->product_name = $get_product->product_name;
                $value2->price = "Rp.".number_format($get_product->price,0,"",".");
                if ($value2->updated_at == null) {
                  $value2->updated_at = "";
                }
              }
              //get detail pengaman
              $value->detail_pengaman = $this->mymodel->getbywhere("dpengaman","htrans_id",$value->htrans_id,"result");
              foreach ($value->detail_pengaman as $key3 => $value3) {
                $get_biaya_lain =  $this->mymodel->getbywhere("additional_price","id",$value3->id_pengaman,"row");
                $value3->keterangan = $get_biaya_lain->keterangan;
                $value3->biaya = $get_biaya_lain->biaya;
                $value3->jenis = $get_biaya_lain->jenis;
              }
              //get histori poin
              if (!empty($this->mymodel->getbywhere("histori_poin","htrans_id",$value->htrans_id,"row"))) {
                $value->transaksi_poin = $this->mymodel->getbywhere("histori_poin","htrans_id",$value->htrans_id,"row");
              }
              else{
                $value->transaksi_poin = array();
              }

            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else if (empty($data)) {
             $msg = array('status' => 1, 'message'=>'Tidak ada transaksi' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

    public function get_province($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;
    }

}