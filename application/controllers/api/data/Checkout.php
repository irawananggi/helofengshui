<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAnrD16lI:APA91bH0MHVh9XsRh6tL8y5ZlGeE7z_-x1jBRPyajmKvUWnuN5wgKA8GspRjOvvf_t0-4Qo3MbugqZmRwLSsi73KHjQuXKL3skxDmj1GskZnyjIO5Vdrb0rQ0hZM1Mw6IGQaiRa-xgRo' );
use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
//require_once('midtrans/Midtrans.php');

class Checkout extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $member_address_id = $this->post('member_address_id');
          $ongkir = $this->post('ongkir');
          $kurir = $this->post('courier');
          $voucher_code = "";
          $total_diskon_member = 0;
          $total_diskon_umum = 0;
          $total_diskon_voucher = 0;
          $total_diskon_poin = 0;
          $total_diskon = 0;
          
          if (!empty($mem)) {
            $get_cart = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
            $get_alamat = $this->mymodel->getbywhere('member_address','member_address_id',$member_address_id,'result');
            $get_company_address = $this->mymodel->getlast("company_address","company_address_id");
            $member_pengaman = $this->mymodel->getbywhere("member_pengaman","member_id",$mem->member_id,"result");

            $get_biaya_tambahan = 0;
            if ($this->post('biaya_lain') == 1) {
              $get_biaya_tambahan = $this->mymodel->getfirst("biaya_lain","id")->biaya;
            }
            //Pilih Bank Tujuan
            $bank_id = $this->post('bank_id');
            if ($this->post('voucher_code')=="") {
              $voucher_code  = "";
            }else{
              $voucher_code = $this->post('voucher_code');
            }
            $brg =0;
            $total = 0;
            $total = $total + $get_biaya_tambahan;
            //get cart product
            foreach ($get_cart as $key => $value) {
              $brg = $brg + $value->quantity;
              $bil1 = $value->quantity;
              $bil2 = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row')->price;
              $hitung = $bil1 * $bil2;
              $total = $total + $hitung;
            }
            $total_asli = $total;
            $total_pengaman = 0;
            $brg_pengaman = 0;
            //get pengaman
            foreach ($member_pengaman as $key => $value) {
              $brg_pengaman = $brg_pengaman + $value->quantity;
              $bilangan1 = $value->quantity;
              $bilangan2 = $this->mymodel->getbywhere("additional_prices","id",$value->id_pengaman,"row")->biaya;
              $hitung_pengaman = $bilangan1*$bilangan2;
              $total = $total + $hitung_pengaman;
              $total_pengaman = $total_pengaman+$hitung_pengaman;
            }
            $total = $total + $ongkir;
            $tgl = date('dmY');
            //$angka = rand(1000,9999);
            $get_nomor = $this->mymodel->getlastwhere('htrans','is_deleted',"0",'htrans_id')->tracking_id;
            $angka = substr($get_nomor,10);
            $angka = (int)$angka;
            $angka++;
            if ($angka<10) {
              $angka = "0000".$angka;
            }else if($angka<100){
              $angka = "000".$angka;
            }else if($angka<1000){
              $angka = "00".$angka;
            }else if($angka<10000){
              $angka = "0".$angka;
            }

            //trk-datetime-random(100-1000)
            $track = "TR".$tgl."".$angka;
            //insert htrans
            
            $province_id;$city_id;$subdistrict_id;$village;$street;$zipcode;
            foreach ($get_alamat as $key => $value) {
              $province_id = $value->province_id;
              $city_id = $value->city_id;
              $subdistrict_id = $value->subdistrict_id;
              $village = $value->village;
              $street = $value->street;
              $zipcode = $value->zipcode;
            }
            if ($this->post('gunakan_voucher') == "1") {
              //Hitung Voucher Diskon
              $get_diskon = array();
              if ($voucher_code != "") {
                $get_diskon = $this->mymodel->getbywhere('voucher','code',$voucher_code,'row');
                //$total = $total-$get_diskon->discount;
                $total_diskon = $get_diskon->discount + $total_diskon;
                $total_diskon_voucher = $get_diskon->discount;
                $data_diskon = array(
                  "is_deleted" => 1
                );
                $this->mymodel->update('member_voucher',$data_diskon,"member_id='$mem->member_id' and voucher_id=",$get_diskon->voucher_id);
              }
            }

            if ($this->post('gunakan_poin') == "1") {
              $get_poin = $this->mymodel->getbywhere("histori_poin","member_id",$mem->member_id,"result");
              $hitung_poin = 0;
              foreach ($get_poin as $key => $value) {
                if ($value->status_poin == "didapatkan") {
                  $hitung_poin = $hitung_poin + $value->poin;
                }
                else if($get_poin == "digunakan"){
                  $hitung_poin = $hitung_poin - $value->poin;
                }
              }
              $total = $total - $hitung_poin;
              $total_diskon_poin = $hitung_poin;
            }

            if ($this->post("gunakan_diskon_member") == "1") {
              //Cek Diskon Khusus dan Umum
              $member_discount = $this->db->from('member_discount')->where('member_id', $this->post('member_id'))->get()->result();
              //diskon member dulu dpt engga
              if(count($member_discount) > 0){
                if($total_asli > $member_discount[0]->min_purchase){ 
                  
                  if($member_discount[0]->discount_type == "percent"){
                    $total_diskon_member = $total_asli * ($member_discount[0]->total_discount / 100);
                  }else{
                    $total_diskon_member = $member_discount[0]->total_discount;
                  }
                  //$array['row_member_diskon'] = $member_discount;
                  $total_diskon = $total_diskon_member;
                }
              }
            }
            else{
              //kalau diskon member tdk dpt sekalipun ada tp g lolos minimal kasih diskon umum
              //if($total_diskon_member == 0){  
                $diskon_umum = $this->db->from('diskon')->get()->result();
                if(count($diskon_umum) > 0){  
                  if($total_asli > $diskon_umum[0]->total_belanja){
                    if($diskon_umum[0]->jenis_potongan == "percent"){ 
                      $total_diskon_umum = $total_asli * ($diskon_umum[0]->total_potongan / 100);
                    }else{
                      $total_diskon_umum = $diskon_umum[0]->total_potongan;
                    }
                    //$array['row_diskon_umum'] = $diskon_umum;
                    $total_diskon = $total_diskon_umum;
                  }
                }
              //}
            }

            $total = $total - $total_diskon;
            //cek_stok
            $empty_stock = array();
            foreach ($get_cart as $key => $value) {
              $get_produk = $this->mymodel->getbywhere("product","product_id",$value->product_id,"row");
              $cek_stok = $get_produk->quantity - $value->quantity;
              if ($cek_stok < 0) {
                $pr = array(
                  "product_id" => $value->product_id,
                  "product_name" => $get_produk->product_name
                );
                array_push($empty_stock, $pr);
              }
            }
            $data = array();
            if ($this->post("is_cod") == "1" && empty($empty_stock)) {
                $data = array(
                "htrans_id" => "",
                "total_quantity" => $brg,
                "total_price" => $total,
                "total_sub_price" => $total_asli,
                "total_pengaman" => $total_pengaman,
                "biaya_lain" => $get_biaya_tambahan,
                //"total_discount" => $total_diskon,
                "diskon_member" =>  $total_diskon_member,
                "diskon_umum" => $total_diskon_umum,
                "diskon_voucher" => $total_diskon_voucher,
                "diskon_poin" => $total_diskon_poin,
                "ongkir" => "0",
                "kurir" => "COD",
                "member_id" => $mem->member_id,
                "tracking_id" => $track,
                "province_id" =>$get_company_address->province_id,
                "city_id" => $get_company_address->city_id,
                "subdistrict_id" => $get_company_address->subdistrict_id,
                "village" => $get_company_address->village,
                "street" => $get_company_address->street,
                "zipcode" => $get_company_address->zipcode,
                "created_at" => date('Y-m-d H:i:s'),
                "expired_date" => date('Y-m-d H:i:s',strtotime("+1 hours")),
                "is_deleted" => 0,
                "transaction_status_id" => 1,
                "is_cod" => $this->post("is_cod")
                );
              }
              else if($this->post("is_cod") != "1" && empty($empty_stock)){
                $data = array(
                "htrans_id" => "",
                "total_quantity" => $brg,
                "total_price" => $total,
                "total_sub_price" =>  $total_asli,
                "total_pengaman" => $total_pengaman,
                "biaya_lain" => $get_biaya_tambahan,
                //"total_discount" => $total_diskon,
                "diskon_member" =>  $total_diskon_member,
                "diskon_umum" => $total_diskon_umum,
                "diskon_voucher" => $total_diskon_voucher,
                "diskon_poin" => $total_diskon_poin,
                "ongkir" => $ongkir,
                "kurir" => $kurir,
                "member_id" => $mem->member_id,
                "tracking_id" => $track,
                "province_id" =>$province_id,
                "city_id" => $city_id,
                "subdistrict_id" => $subdistrict_id,
                "village" => $village,
                "street" => $street,
                "zipcode" => $zipcode,
                "created_at" => date('Y-m-d H:i:s'),
                "expired_date" => date('Y-m-d H:i:s',strtotime("+1 hours")),
                "is_deleted" => 0,
                "transaction_status_id" => 1,
                "is_cod" => $this->post("is_cod")
                );
              }
            
            
            if ($total>0 && $brg>0 && empty($empty_stock)) {
              $in = $this->mymodel->insert('htrans',$data);
              if (!empty($this->mymodel->getlastwhere('htrans',"member_id='".$mem->member_id."' and is_deleted=",'0','htrans_id'))) {
                $data['htrans_id'] = $this->mymodel->getlastwhere('htrans',"member_id='".$mem->member_id."' and is_deleted=",'0','htrans_id')->htrans_id;
                if ($this->post("gunakan_poin") == 1 && $total_diskon_poin>0) {
                  $save_poin = array(
                    "member_id" => $mem->member_id,
                    "htrans_id" => $data['htrans_id'],
                    "poin" => $total_diskon_poin,
                    "status_poin" => "digunakan",
                    "created_at" => date("Y-m-d H:i:s")
                  );
                  $this->mymodel->insert("histori_poin",$save_poin);
                }
                //convert dapat poin
                
                if ($total_diskon_poin == 0) {
                  $setting_poin = $this->mymodel->getlast("poin","id");
                  $hitung_dpt_poin = ($total_asli/$setting_poin->nominal)*$setting_poin->poin;
                  $data['potensi_poin_didapatkan'] = $hitung_dpt_poin;
                  $save_poin = array(
                    //"member_id" => $mem->member_id,
                    "htrans_id" => $data['htrans_id'],
                    "poin" => $hitung_dpt_poin,
                    "status_poin" => "didapatkan",
                    "created_at" => date("Y-m-d H:i:s")
                  );
                }
              }
              $data['inttotal_price'] = $data['total_price'];
              $data['total_price'] = "Rp ".number_format($data['total_price'],0,"",".");
              $htrans_id = $this->mymodel->getlast('htrans','htrans_id')->htrans_id;
              $get_bank = $this->mymodel->getbywhere('company_bank','bank_id',$bank_id,'row');
              
                $data3 = array(
                    "htrans_id" => $htrans_id,
                    "bank_id" => $bank_id,
                    "no_rekening" => $get_bank->no_rekening,
                    "owner" => $get_bank->owner
                    );
              if ($total>0 && $brg>0 && empty($empty_stock)) {
                $in3 = $this->mymodel->insert('ptrans',$data3);
              }
              //$kb="022";
              $kb = $this->mymodel->getbywhere('bank','bank_id',$get_bank->bank_id,'row');
              $get_bank->nama_bank = $kb->name;
              $data['kode_bank'] = $kb->code;
              $data['rekening'] = $get_bank->no_rekening;
              $data['owner'] = $get_bank->owner;
              $desc_trans_status = $this->mymodel->getbywhere('transaction_status','transaction_status_id',$data['transaction_status_id'],'row')->description_in_indonesia;
              $data['status_transaksi'] = $desc_trans_status;
              if ($this->post("is_cod") == "1") {
                $get_company_address = $this->mymodel->getlast("company_address","company_address_id");
                //alamat by id
                $idp = $this->get_province($get_company_address->province_id)->name;
                $idc = $this->get_city($get_company_address->province_id,$get_company_address->city_id)->name;
                $ids = $this->get_subdistrict($get_company_address->subdistrict_id)->name;
                if ($idp==null) {
                  $idp = "Tidak Ditemukan";
                }
                if ($idc==null) {
                  $idc = "Tidak Ditemukan";
                }
                if ($ids==null) {
                  $ids = "Tidak Ditemukan";
                }
                
                $data['alamat_destinasi'] = $get_company_address->village.", ".$ids.", ".$get_company_address->zipcode.", ".$idc.", ".$idp;
              }
              else{
                //alamat by id
                $idp = $this->get_province($data['province_id'])->name;
                $idc = $this->get_city($data['province_id'],$data['city_id'])->name;
                $ids = $this->get_subdistrict($data['subdistrict_id'])->name;
                if ($idp==null) {
                  $idp = "Tidak Ditemukan";
                }
                if ($idc==null) {
                  $idc = "Tidak Ditemukan";
                }
                if ($ids==null) {
                  $ids = "Tidak Ditemukan";
                }
                
                $data['alamat_destinasi'] = $data['village'].", ".$ids.", ".$idc.", ".$idp." - ".$data['zipcode'];
              }
              $data['expired_date'] = date("d-M-Y H:i:s",strtotime($data['expired_date']));

              //insert kode cantik
              $data4 = array(
                "kode" => $this->post('kode_cantik'),
                "htrans_id" => $htrans_id,
                "member_id" => $mem->member_id,
                "created_at" => date('Y-m-d H:i:s')
                );
              if ($total>0 && $brg>0 && empty($empty_stock)) {
                $total += $this->post('kode_cantik');
                $in4 = $this->mymodel->insert('kode_cantik',$data4);
              }

              //insert dtrans
              $htrans_id = $this->mymodel->getlast('htrans','htrans_id')->htrans_id;
              $in2 = "";
              foreach ($get_cart as $key => $value) {
                $data2 = array(
                "dtrans_id" => "",
                "htrans_id" => $htrans_id,
                "product_id" => $value->product_id,
                "product_variants_id" => $value->product_variants_id,
                "quantity" => $value->quantity,
                "price" => $this->mymodel->getbywhere('product','product_id',$value->product_id,'row')->price,
                "created_at" =>date('Y-m-d H:i:s'),
                "is_deleted" => 0
                );
                if ($total>0 && $brg>0) {
                    $in2 = $this->mymodel->insert('dtrans',$data2);
                }
              }
              
              //insert detail pengaman
              $in5 = "";
              foreach ($member_pengaman as $key => $value) {
                $data5 = array(
                "htrans_id" => $htrans_id,
                "id_pengaman" => $value->id_pengaman,
                "quantity" => $value->quantity,
                "price" => $this->mymodel->getbywhere('additional_prices','id',$value->id_pengaman,'row')->biaya
                );
                if ($total_pengaman>0) {
                    $in5 = $this->mymodel->insert('dpengaman',$data5);
                }
              }
              $data['kode_cantik'] = $this->post('kode_cantik');
              if ($in && $in2 && $in3 && $in4 && empty($empty_stock)) {
                foreach ($get_cart as $key => $value) {
                  $get_produk = $this->mymodel->getbywhere("product","product_id",$value->product_id,"row");
                  $k = $get_produk->quantity - $value->quantity;
                    $pr = array(
                      "quantity" => $k
                    );
                    $this->mymodel->update("product",$pr,"product_id",$value->product_id);
                }
                //$get_midtrans = $this->payment_midtrans($track,$total,$htrans_id,$mem->member_id);
                $this->mymodel->delete('member_cart','member_id',$mem->member_id,'result');
                $this->mymodel->delete('member_pengaman','member_id',$mem->member_id,'result');
              if ($mem->fcm_id != "") {
                //send notif
                $this->send_notif("Transaksi Anda Menunggu Pembayaran","Transaksi anda ".$track." senilai Rp. ".number_format(($total+$this->input->post("kode_cantik")),0,"",".").", sedang menunggu pembayaran", $mem->fcm_id, array('title' =>"Transaksi Anda Menunggu Pembayaran" , 'message' => "Transaksi anda ".$track." senilai Rp. ".number_format(($total+$this->input->post("kode_cantik")),0,"",".").", sedang menunggu pembayaran", 'tipe' => 'detail_transaksi','content' => array("id_transaksi"=>$htrans_id, "order_id"=>$track )) );
              }
              $banks = $this->mymodel->getall("company_bank");
              $email['username'] = $mem->first_name;
              $email['carts'] = $get_cart;
              $email['pengaman_carts'] = $member_pengaman;
              $email['banks'] = $banks;
              $email['company_bank'] = $get_bank;
              $email['subtotalbrg'] = $total_asli;
              $email['totalpay'] = $total;
              $email['totalpengaman'] = $total_pengaman;
              $email['totaldiskon'] = $total_diskon_member+$total_diskon_umum+$total_diskon_voucher+$total_diskon_poin;
              $email['totalongkir'] = $ongkir;
              $email['biayalain'] = $get_biaya_tambahan;
              $email['tracking_id'] = $track;
              $email['alamat_pengiriman'] = $data['street']." ".$data['village'].", ".$ids.", ".$idc.", ".$idp." - ".$data['zipcode'];
              $email['notelp_pengiriman'] = $mem->phone;
              $this->send_email_file($mem->email, $email);
                $msg = array('status' => 1, 'message'=>'Berhasil Insert data'  ,'data'=>$data);
              }
              else{
                $msg = array('status' => 0, 'message'=>'Data Keranjang Kosong'  ,'data'=>null);
              }

            }
            else if(empty($empty_stock)){
              $msg = array('status' => 0, 'message'=>'Data Keranjang Kosong'  ,'data'=>null);
            }
            else{
              $msg = array('status' => 0, 'message'=>'Pesanan melebihi stok'  ,'data'=>null);
            }

          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

    public function get_province($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
      */
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;
    }

    public function cek_mutasi($nominal){
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, 'https://app.moota.co/api/v1/bank/DZ4jA0pbzAo/mutation/search/'.$nominal);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_HTTPHEADER, [
          'Accept: application/json',
          'Authorization: Bearer WL0BDlfQ7FvHTHvcKGaQvQSy5UhdPl6M76nLrDByj7gdOMqzsA'
      ]);
      $response = curl_exec($curl);
      return $response;
    }

      public function send_notif($title,$desc,$fcm_id,$data)
  {
    $Msg = array(
      'body' => $desc,
      'title' => $title
    );

    $fcmFields = array(
      'to' => $fcm_id,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    //echo $result."\n\n";
  }

  public function send_email_file($to='',$data)
  {
    $this->load->library('email');

    // extract($data);
      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.gmail.com',
          'smtp_port' => 465,
          'smtp_user' => 'dev.jakarta.bd@gmail.com',
          'smtp_pass' => 'JAKARTAbd2020',
          'mailtype'  => 'html',
          'smtp_timeout' => 20,
          'charset'   => 'utf-8'
      );


      
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      // $htmlContent = '<h1>Sending email via SMTP server</h1>';
      // $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';
      $this->email->to($to);

      $data_['msg'] = ".";
      // $data_['code'] = "<a href='".site_url('Member/success?t='.$data)."' target='_blank'> Klik Disini </a>";
      $data_['title'] = "Pesanan JBD - Jakarta Bubble Drink";
      // $htmlContent = " <div>Halo $username, Pesanan kamu sudah kita terima dan akan diproses<div>"; //$this->load->view('email_konfirmasi',$data_,true);
      $htmlContent = $this->load->view("email/waiting_payment", $data, true);
      //$htmlContent = $this->load->view("email_cart", $data, true);
      $this->email->from('no-reply@jbd.com', 'JBD - Jakarta Bubble Drink');
      $this->email->subject('[No Reply] Pesanan JBD - Jakarta Bubble Drink');
      $this->email->message($htmlContent);

      //Send email
      $this->email->send();
      // echo $this->email->print_debugger();
  }

  public function get_bulan($bulan){
    $bln = "";
    if ($bulan == 1) {
      $bln = "Januari";
    }else if ($bulan == 2) {
      $bln = "Februari";
    }else if ($bulan == 3) {
      $bln = "Maret";
    }else if ($bulan == 4) {
      $bln = "April";
    }else if ($bulan == 5) {
      $bln = "Mei";
    }else if ($bulan == 6) {
      $bln = "Juni";
    }else if ($bulan == 7) {
      $bln = "Juli";
    }else if ($bulan == 8) {
      $bln = "Agustus";
    }else if ($bulan == 9) {
      $bln = "September";
    }else if ($bulan == 10) {
      $bln = "Oktober";
    }else if ($bulan == 11) {
      $bln = "November";
    }else if ($bulan == 12) {
      $bln = "Desember";
    }
    return $bln;
  }
  public function get_hari($h){
    $hari = "";
    if ($h == 1) {
      $hari = "Senin";
    }else if ($h == 2) {
      $hari = "Selasa";
    }else if ($h == 3) {
      $hari = "Rabu";
    }else if ($h == 4) {
      $hari = "Kamis";
    }else if ($h == 5) {
      $hari = "Jumat";
    }else if ($h == 6) {
      $hari = "Sabtu";
    }else if ($h == 7) {
      $hari = "Minggu";
    }
    return $hari;
  }
}