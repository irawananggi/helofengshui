<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Pencarian_produk_keyword extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {

      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $kata = $this->post('keyword');
      $data = $this->mymodel->withquery("select * from product where product_name like '%".$kata."%'",'result');
      foreach ($data as $key => $value) {
        $value->icon = $this->mymodel->getbywhere('product_image','product_id',$value->product_id,'row')->img_file;
            if (empty($value->icon)) {
              $value->icon ="";
            }
            if ($value->updated_at==null) {
            $value->updated_at = "";
            }
          }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
    }
}