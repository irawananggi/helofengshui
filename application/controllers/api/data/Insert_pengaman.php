<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Insert_pengaman extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$id_cart = $this->post('cart_id');
          $id_pengaman = $this->post('id_pengaman');
          $qty = $this->post('qty');
          $get_data = $this->mymodel->getbywhere('member_pengaman','member_id',$mem->member_id,'result');
          $data = array();$cek=0;
          $grand=0;
          foreach ($get_data as $key => $value) {
            if ($value->id_pengaman == $id_pengaman) {
             $cek++;
             $qty = $qty + $value->quantity;
             $data = array(
              "member_id" => $value->member_id,
              "id_pengaman" => $value->id_pengaman,
              "quantity" => $qty
              );
            }
          }
          if (!empty($mem)) {
            if ($cek>0) {
              $this->mymodel->update("member_cart",$data,"member_id='".$mem->member_id."' and product_id=",$product_id);
            }else{
              $data = array(
                "member_id" => $mem->member_id,
                "id_pengaman" => $id_pengaman,
                "quantity" => $qty
                );
              $this->mymodel->insert('member_pengaman',$data);
            }
            if (!empty($data)) {
              //$get_product = $this->mymodel->getbywhere('product','product_id',$data['product_id'],'row');
          //get grand
          $member_pengaman = $this->mymodel->getbywhere('member_pengaman','member_id',$mem->member_id,'result');
            foreach ($member_pengaman as $key => $value) {
              $get_pengaman = $this->mymodel->getbywhere('additional_prices','id',$value->id_pengaman,'row');
              $h = $get_pengaman->biaya*$value->quantity;
              $grand = $grand + $h;
            }
              $grand = "Rp ".number_format($grand,0,"",".");
              $msg = array('status' => 1, 'message'=>'Berhasil Insert data','grand_total'=>$grand,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak valid' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}