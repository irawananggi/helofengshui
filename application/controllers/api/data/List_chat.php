<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class List_chat extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
          
            if (!empty($token)) {
              $member = $this->mymodel->getbywhere("member","token",$token,"row");
              // $data = $this->mymodel->withquery("select distinct product_id from chat where member_id = '".$member->member_id."' order by chat_id desc","result");
              
              $data = $this->mymodel->withquery("SELECT DISTINCT chat.product_id FROM chat LEFT JOIN product ON chat.product_id = product.product_id WHERE chat.member_id = '".$member->member_id."' AND product.is_deleted = 0 ORDER BY chat.chat_id DESC","result");
              
              //echo $this->db->last_query();
              if (!empty($data)) {
                foreach ($data as $key => $value) {
                  $get_product = $this->mymodel->getbywhere("product","product_id",$value->product_id,"row");
                  if (!empty($get_product)) {
                    $value->product_name = $get_product->product_name;
                  }
                  else{
                    $value->product_name = "";
                  }
                }
                $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data'=>$data);
                $status="200";
              }
              else{
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
                $status="422";
              }
            }else{
                $msg = array('status' => 0, 'message'=>'Token anda kosong' ,'data'=>array());
                $status="422";
            }

        $this->response($msg);//$this->response($msg,$status);
    }
}
