<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Histori_return extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (!empty($mem)) {

            $data = $this->mymodel->getbywhere('return_transaksi','member_id',$mem->member_id,'result');
            foreach ($data as $key => $value) {
              $value->img_file = base_url("assets/img/return_transaksi/").$value->img_file;
              if ($value->updated_at==null) {
                $value->updated_at = "";
              }
              else{
                $value->updated_at = date("d/m/Y",strtotime($value->updated_at));
              }
              $get_status = $this->mymodel->getbywhere("return_status","id",$value->return_status_id,"row");
              $value->status = $get_status->status;
              $value->data_product = $this->mymodel->getbywhere("product","product_id",$value->product_id,"row");
              $value->tgl_pengajuan = date("d/m/Y",strtotime($value->created_at));
              
              if($value->return_status_id == "3"){
                $value->tgl_diterima = date("d/m/Y",strtotime($value->approve_at));
                if($value->return_status_id == "6"){
                  $value->tgl_pengiriman = date("d/m/Y",strtotime($value->shipping_at));
                  if($value->return_status_id == "7"){
                    $value->tgl_refund = date("d/m/Y",strtotime($value->refund_at));
                    if($value->return_status_id == "4"){
                      $value->tgl_selesai = date("d/m/Y",strtotime($value->finish_at));
                    }
                  }
                }
              }
              if($value->return_status_id == "5"){
                $value->tgl_ditolak = date("d/m/Y",strtotime($value->reject_at));
                $value->detail_penolakan = $this->mymodel->getbywhere("return_ditolak","return_transaksi_id",$value->id,"row");
              }
              if ($value->approve_at == null) {
                $value->approve_at = "";
              }
              if ($value->shipping_at == null) {
                $value->shipping_at = "";
              }
              if ($value->refund_at == null) {
                $value->refund_at = "";
              }
              if ($value->finish_at == null) {
                $value->finish_at = "";
              }
              if ($value->reject_at == null) {
                $value->reject_at = "";
              }
              unset($value->data_product->updated_at);
            }
            
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}