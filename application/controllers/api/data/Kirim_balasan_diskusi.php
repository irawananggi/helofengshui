<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Kirim_balasan_diskusi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $id_diskusi = $this->post('id_diskusi');
          $balasan = $this->post('balasan');
          if (isset($mem)) {
            $get_diskusi = $this->mymodel->getbywhere("diskusi","diskusi_id",$id_diskusi,"row");
            $data = array(
              "member_id" => $mem->member_id,
              "product_id" => $get_diskusi->product_id,
              "diskusi" => $balasan,
              "is_penjual" => 0,
              "pertanyaan_id" => $id_diskusi,
              "created_at" => date("Y-m-d H:i:s")
            );
            if (!empty($data)) {
              $this->mymodel->insert("diskusi",$data);
              $msg = array('status' => 1, 'message'=>'Berhasil Insert Data');
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak valid');
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}