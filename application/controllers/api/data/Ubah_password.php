<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Ubah_password extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $password_lama = md5($this->post('password_lama'));
            $pl = $this->mymodel->getbywhere('member','password',$password_lama,"row");
            if (isset($pl)) {

            $password_baru = md5($this->post('password_baru'));
            $konfirmasi_password_baru = md5($this->post('konfirmasi_password_baru'));
           

              if ($password_baru == $konfirmasi_password_baru) {
                $data = array(
                  "password" => $password_baru
                  );
                
                if (!empty($data)) {
                  $this->mymodel->update('member',$data,'member_id',$mem->member_id);
                  $msg = array('status' => 1, 'message'=>'Berhasil Update profile' ,'data'=>$data);
                }else {
                  $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
                }
              }else {
                  $msg = array('status' => 0, 'message'=>'Password yang Anda Masukkan Tidak Sama' ,'data'=>array());
              }
            }else {
                $msg = array('status' => 0, 'message'=>'Password lama salah' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}