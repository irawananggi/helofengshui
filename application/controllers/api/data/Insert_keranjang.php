<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Insert_keranjang extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$id_cart = $this->post('cart_id');
          $product_id = $this->post('product_id');
          $qty = $this->post('qty');
          if (empty($this->post('variant_id'))) {
            $variant_id = "0";
          }
          else{
            $variant_id = $this->post('variant_id');
          }
          $get_data = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
          $data = array();$cek=0;
          $grand=0;
          foreach ($get_data as $key => $value) {
            if ($value->product_id == $product_id && $value->product_variants_id == $variant_id) {
             $cek++;
             $qty = $qty + $value->quantity;
             $data = array(
              "member_id" => $value->member_id,
              "product_id" => $value->product_id,
              "product_variants_id" => $value->product_variants_id,
              "quantity" => $qty
              );
            }
          }
          if (!empty($mem)) {
            if ($cek>0) {
              $this->mymodel->update("member_cart",$data,"member_id='".$mem->member_id."' and product_id=",$product_id);
            }else{
              $data = array(
                "member_cart_id" => "",
                "member_id" => $mem->member_id,
                "product_id" => $product_id,
                "product_variants_id" => $variant_id,
                "quantity" => $qty
                );            
              $this->mymodel->insert('member_cart',$data);
            }
            if (!empty($data)) {
              //$get_product = $this->mymodel->getbywhere('product','product_id',$data['product_id'],'row');
          //get grand
          $get_cart = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
            foreach ($get_cart as $key => $value) {
              $get_product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
              $h = $get_product->price*$value->quantity;
              $grand = $grand + $h;
            }
              $grand = "Rp ".number_format($grand,0,"",".");
              $msg = array('status' => 1, 'message'=>'Berhasil Insert data','grand_total'=>$grand,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak valid' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}