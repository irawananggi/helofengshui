<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Detail_histori_transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $kode_transaksi = $this->get('kode_transaksi');
          if (!empty($mem)) {
            $get_htrans = $this->mymodel->getbywhere('htrans','tracking_id',$kode_transaksi,'row');
            $data = $this->mymodel->getbywhere('dtrans','htrans_id',$get_htrans->htrans_id,'result');
            $kode_cantik = $this->mymodel->getbywhere("kode_cantik", "htrans_id", $get_htrans->htrans_id, "row");
            foreach ($data as $key => $value) {
              $value->resi = $get_htrans->resi;
              $value->kurir = $get_htrans->kurir;
              $value->ongkir = $get_htrans->ongkir;
              $get_product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
              $get_product_variant = $this->mymodel->getbywhere('product_variants','id',$value->product_variants_id,'row');
              $get_image = $this->mymodel->getbywheresort('product_image','product_id',$value->product_id,'id_product_image','DESC');
              $value->nama_produk = $get_product->product_name;
              $value->nama_variant = $get_product_variant->variant_name;
              if ($value->updated_at == null) {
                $value->updated_at = "";
              }
              foreach ($get_image as $key => $valuee) {
                $value->product_image = base_url('assets/img/product/'.$valuee->img_file);
              }
              $value->kode_cantik = $kode_cantik->kode;
              $value->price = "Rp ".number_format($value->price,0,"",".");
              //get detail transaction
              $value->detail_trans = $this->mymodel->getbywhere("dtrans","htrans_id",$value->htrans_id,"result");
              foreach ($value->detail_trans as $key2 => $value2) {
                $get_product = $this->mymodel->getbywhere("product","product_id",$value2->product_id,"row");
                $value2->product_name = $get_product->product_name;
                $value2->price = "Rp.".number_format($get_product->price,0,"",".");
                if ($value2->updated_at == null) {
                  $value2->updated_at = "";
                }
                
              }
              //get detail pengaman
              $value->detail_pengaman = $this->mymodel->getbywhere("dpengaman","htrans_id",$value->htrans_id,"result");
              foreach ($value->detail_pengaman as $key3 => $value3) {
                $get_biaya_lain =  $this->mymodel->getbywhere("additional_prices","id",$value3->id_pengaman,"row");
                $value3->keterangan = $get_biaya_lain->keterangan;
                $value3->biaya = $get_biaya_lain->biaya;
                $value3->jenis = $get_biaya_lain->jenis;
              }
              //get histori poin
              if (!empty($this->mymodel->getbywhere("histori_poin","htrans_id",$value->htrans_id,"row"))) {
                $value->transaksi_poin = $this->mymodel->getbywhere("histori_poin","htrans_id",$value->htrans_id,"row");
              }
              else{
                $value->transaksi_poin = array();
              }
              //cek is_review
              $cek_review = $this->mymodel->getbywhere("product_ratting","product_id='".$value->product_id."' and htrans_id=",$value->htrans_id,"row");
              $value->is_review = false;
              if (!empty($cek_review)) {
                $value->is_review = true;
              }
              //cek is_return_product
              $cek_return = $this->mymodel->getbywhere("return_transaksi","kode_transaksi='".$get_htrans->tracking_id."' and product_id=",$value->product_id,"row");
              $value->is_return_product = false;
              if (!empty($cek_return)) {
                $value->is_return_product = true;
              }

            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else if (empty($data)) {
             $msg = array('status' => 1, 'message'=>'Data kosong' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}