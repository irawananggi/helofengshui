<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Chat extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }

function compressImage($source_image, $compress_image) {
      $image_info = getimagesize($source_image);
      if ($image_info['mime'] == 'image/jpeg') {
      $source_image = imagecreatefromjpeg($source_image);
      imagejpeg($source_image, $compress_image, 1);
      } elseif ($image_info['mime'] == 'image/gif') {
      $source_image = imagecreatefromgif($source_image);
      imagegif($source_image, $compress_image, 1);
      } elseif ($image_info['mime'] == 'image/png') {
      $source_image = imagecreatefrompng($source_image);
      imagepng($source_image, $compress_image, 0.01);
      }
      return $compress_image;
      }
      public function firebase($id_categori_chat,$date_time)
      {
        if (strpos(strtolower($date_time), "jan") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "feb") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "mar") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "apr") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "mei") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "jun") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "jul") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "ags") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "sep") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "okt") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "nov") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }else
        if (strpos(strtolower($date_time), "des") !== false) {
          $this->sendfb($id_categori_chat,$date_time);
        }
      }
      public function firee($key,$bln,$i,$value)
      {
        $FIREBASE = "https://rejeki-sejuta-bintang.firebaseio.com/";
        if (strpos(strtolower($key), $bln) !== false) {
              $dd = convert_bulan_angka(ucfirst(strtolower($key)));
              $arr = explode(" ",$dd);
              if (!empty($arr[2])) {
                $key = $arr[2]."-".$arr[1]."-".$arr[0];
              }else {
                $key = "2018-".$arr[1]."-".$arr[0];
              }
              //echo "$key <br>";
              $datachild = $value;
              foreach ($datachild as $key1 => $value1) {
                //echo "$key1 <br>";
                $NODE_PATCH = "c$i/$key/$key1.json";
                //print_r($value1);
                $data = $value1;
                $json = json_encode( $data );
                $curl1 = curl_init();
                curl_setopt( $curl1, CURLOPT_URL, $FIREBASE . $NODE_PATCH );
                curl_setopt( $curl1, CURLOPT_CUSTOMREQUEST, "PUT" );
                curl_setopt( $curl1, CURLOPT_POSTFIELDS, $json );
                curl_setopt( $curl1, CURLOPT_RETURNTRANSFER, true );
                $response1 = curl_exec( $curl1 );
                curl_close( $curl1 );
              }
          }
      }

    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");

          if (isset($mem)) {
              $pesan =$this->post('pesan');
              if(empty($pesan)) $pesan="";
              $data = array(
              'member_id' => $mem->member_id,
              'pesan' => $pesan,
              'created_at' => date('Y-m-d H:i:s')
             );

                  $in =   $this->mymodel->insert('chat',$data);
                  if ($in) {
                    $msg = array('status'=>1,'message'=>'Chat Berhasil Terkirim','data'=>$data);
                  }else {
                    $msg = array('status'=>0,'message'=>'Gagal insert','data'=>$this->db->last_query());
                  }

                //$this->firebase($this->post('id_chat_category'),$this->post('date_time'));

          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}