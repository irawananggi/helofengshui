<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require_once('sms/api_sms_class_reguler_json.php');
require 'phpmailer/PHPMailerAutoload.php';
ob_start();

class Resend_verifikasi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function send_sms($no,$kode){
    $userkey = 'dc85ab108a7c';
    $passkey = '5s3qrrnyg5';
    $telepon = $no;
    $otp = $kode;
    $url = 'https://gsm.zenziva.net/api/sendOTP/';
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_URL, $url);
    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
    curl_setopt($curlHandle, CURLOPT_POST, 1);
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
        'userkey' => $userkey,
        'passkey' => $passkey,
        'nohp' => $telepon,
        'kode_otp' => $otp
    ));
    $results = json_decode(curl_exec($curlHandle), true);
    curl_close($curlHandle);
    }
    public function send_wa($no,$kode){
      $userkey = 'dc85ab108a7c';
      $passkey = '5s3qrrnyg5';
      $telepon = $no;
      $message = 'Pendaftaran akun Jakarta Bubble Drink. Kode OTP anda : '.$kode;
      $url = 'https://gsm.zenziva.net/api/sendWA/';
      $curlHandle = curl_init();
      curl_setopt($curlHandle, CURLOPT_URL, $url);
      curl_setopt($curlHandle, CURLOPT_HEADER, 0);
      curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
      curl_setopt($curlHandle, CURLOPT_POST, 1);
      curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
          'userkey' => $userkey,
          'passkey' => $passkey,
          'nohp' => $telepon,
          'pesan' => $message
      ));
      $results = json_decode(curl_exec($curlHandle), true);
      curl_close($curlHandle);
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$get_kode = $this->mymodel->getbywhere('verification_code','member_id',$mem->member_id,'row');
          $message="";
          if (isset($mem)) {
              $phone = $mem->phone;
              $kd = $this->mymodel->getbywhere("verification_code",'member_id',$mem->member_id,'','row');
              $send = $this->send_sms($phone,$kd->code);
              $send1 = $this->send_wa($phone,$kd->code);

              $msg = array('status' => 1, 'message'=>"Sukses Kirim kode verifikasi",'phone'=>$phone);
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}