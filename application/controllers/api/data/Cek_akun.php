<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Cek_akun extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

            $email = $this->post('email');
            $data = $this->mymodel->getbywhere('member','email',$email,'result');
            foreach ($data as $key => $value) {
              if ($value->updated_at == null) {
                $value->updated_at = "";
              }
              if ($value->last_login == null) {
                $value->last_login = "";
              }
            }
            if (empty($data)) {
              $msg = array('status' => 1, 'message'=>'Email Belum Terdaftar' ,'data'=>array());
            }else {
              $msg = array('status' => 0, 'message'=>'Email Sudah Terdaftar' ,'data'=>$data);
            }


          $this->response($msg);

    }
}