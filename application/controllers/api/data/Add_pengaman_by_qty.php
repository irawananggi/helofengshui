<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Add_pengaman_by_qty extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$id_cart = $this->post('cart_id');
          $id_pengaman = $this->post('id_pengaman');
          if (!empty($mem)) {
            $grand=0;
            $member_pengaman = $this->mymodel->withquery("select * from member_pengaman where member_id='".$mem->member_id."'
              and id_pengaman='".$id_pengaman."'",'result');
            $qty =0;
            foreach ($member_pengaman as $key => $value) {
                $qty = $value->quantity+1;
            }
            $data = array(
              "quantity" => $qty
              );
            if (!empty($data) && !empty($member_pengaman)) {
              foreach ($member_pengaman as $key => $value) {
                $this->mymodel->update('member_pengaman',$data,"member_id='".$mem->member_id."' and id_pengaman=",$value->id_pengaman);
              }
            }else {
              $data = array(
              "quantity" => 1,
              "member_id" => $mem->member_id,
              "id_pengaman" => $id_pengaman
              );
              $this->mymodel->insert("member_pengaman",$data);
              $msg = array('status' => 1, 'message'=>'Berhasil Tambah Pengaman' ,'data'=>array());
            }
            //get grand
              $member_pengaman = $this->mymodel->getbywhere('member_pengaman','member_id',$mem->member_id,'result');
              if (!empty($member_pengaman)) {
                foreach ($member_pengaman as $key => $value) {
                  $get_pengaman = $this->mymodel->getbywhere('additional_prices','id',$value->id_pengaman,'row');
                  $h = $get_pengaman->biaya*$value->quantity;
                  $grand = $grand + $h;
                }
                $grand = "Rp ".number_format($grand,0,"",".");
                $msg = array('status' => 1, 'message'=>'Berhasil Ubah Data','grand_total'=>$grand,'data'=>$data); 
              }
              else{
                $grand = "Rp 0";
                $msg = array('status' => 1, 'message'=>'Berhasil Hapus Data, Pengaman Tambahan Kosong','grand_total'=>$grand,'data'=>array()); 
              }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}