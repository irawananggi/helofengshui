<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Cek_email extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

            $email = $this->get('email');
            $data = $this->mymodel->getbywhere('member','email',$email,'row');
            //cek email terdaftar            
            if (!empty($data)) {
              
              $msg = array('status' => 1, 'message'=>'Email Sudah Terdaftar' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Email Belum Terdaftar' ,'data'=>array());
            }


          $this->response($msg);

    }
}