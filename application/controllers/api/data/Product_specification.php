<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Product_specification extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
            $product_id = $this->get('product_id');
            $data = $this->mymodel->getbywhere('product_specification',"product_id=",$product_id,'result');
            $gambar = $this->mymodel->getbywheresort('product_image','product_id',$product_id,'id_product_image','ASC','result');
            foreach ($gambar as $key => $valuee) {
              $gambar = $valuee->img_file;
            }
            foreach ($data as $key => $value) {
              //$value->product_image = base_url('assets/img/product/'.$gambar);
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
    }
}