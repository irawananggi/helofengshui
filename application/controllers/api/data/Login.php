<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require_once('sms/api_sms_class_reguler_json.php');
require 'phpmailer/PHPMailerAutoload.php';
//ob_start();

class Login extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    //perlu ditop up lagi
    public function send_sms($no,$kode){
    $userkey = 'dc85ab108a7c';
    $passkey = '5s3qrrnyg5';
    $telepon = $no;
    $otp = $kode;
    $url = 'https://gsm.zenziva.net/api/sendOTP/';
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_URL, $url);
    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
    curl_setopt($curlHandle, CURLOPT_POST, 1);
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
        'userkey' => $userkey,
        'passkey' => $passkey,
        'nohp' => $telepon,
        'kode_otp' => $otp
    ));
    $results = json_decode(curl_exec($curlHandle), true);
    curl_close($curlHandle);
    }
    public function send_wa($no,$kode){
      $userkey = 'dc85ab108a7c';
      $passkey = '5s3qrrnyg5';
      $telepon = $no;
      $message = 'Pendaftaran akun Jakarta Bubble Drink. Kode OTP anda : '.$kode;
      $url = 'https://gsm.zenziva.net/api/sendWA/';
      $curlHandle = curl_init();
      curl_setopt($curlHandle, CURLOPT_URL, $url);
      curl_setopt($curlHandle, CURLOPT_HEADER, 0);
      curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
      curl_setopt($curlHandle, CURLOPT_POST, 1);
      curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
          'userkey' => $userkey,
          'passkey' => $passkey,
          'nohp' => $telepon,
          'pesan' => $message
      ));
      $results = json_decode(curl_exec($curlHandle), true);
      curl_close($curlHandle);
    }

    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $email = $this->post('email');
      $password = $this->post('password');
      $from = $this->post('from');
     /* cek($email);
      cek($password);
      cek($from);
      die();*/
      $msg ="";
      if ($email !="" && $password!="") {
          $cek = $this->mymodel->getbywhere('member',"email='$email' or phone='$email'",null,"row");
          $success = 0;
          $message = "";
          $stats = "";
          $data_diri = 0;

          if (!empty($cek)) {
            $data = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') and password=",md5($password),"row");

            if (!empty($data)) {
              $success = 1;
                $message = "Login Berhasil";
                $stats = "Silahkan melengkapi data diri";
                $data2 = array(
                  "last_login" => date('Y-m-d H:i:s')
                  );
                $this->mymodel->update('member',$data2,'phone',$data->phone);
                $data = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') and password=",md5($password),"row");

                
            }else{
              $data = new stdClass();
              $success =0;
              $message = "Password Salah";
              $stats = "Password Salah";
            }
            if ($success == 0 ) {
              $msg = array('status'=>$success,'message'=>$message,'stats'=>$stats,'data' => $datax, 'data_diri' => $data_diri);
            }else{
              $data_diri = 1;
              $datax = $this->mymodel->getbywhere('member','email',$email,"row");
              
              if(empty($datax->img_file)){
                   $img_file = NULL;
                  }else{
                    $img_file = base_url('/uploads/member/'.$datax->img_file);
                  }
               $datax = array(
                "member_id" => $datax->member_id,
                "nama_lengkap" => $datax->nama_lengkap,
                "email" => $datax->email,
                "img_file" => $img_file,
                "password" => $datax->password,
                "token" => $datax->token,
                "fcm_id" => $datax->fcm_id,
                "is_active" => $datax->is_active,
                "last_login" => $datax->last_login,
                "created_at" => $datax->created_at
                ); 
                 
             
              
              $msg = array('status'=>$success,'message'=>$message,'stats'=>$stats,'data' => $datax  , 'data_diri' => $data_diri);
            }

          }else{
            $msg = array('status'=>0,'message'=>"Email tidak terdaftar",'data' => null, "data_diri" => $data_diri);
          }

        }else if($email==""){
            $msg = array('status' => 0, 'message'=>'Email Harus Diisi');
        }else if($password==""){
            $msg = array('status' => 0, 'message'=>'Password Harus Diisi');
        }
        $this->response($msg);
    }

    public function get_province($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
      */
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;
    }
    public function send_email($file="",$to='',$title,$desc,$data)
    {
      $to = urldecode($to);
      $mail = new PHPMailer;
      // Konfigurasi SMTP
      $mail->isSMTP();
      $mail->SMTPDebug =0;
      // $mail->Host = 'mail.namagz.com';
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPOptions = array(
         'ssl' => array(
           'verify_peer' => false,
           'verify_peer_name' => false,
           'allow_self_signed' => true
          )
      );
      $mail->SMTPAuth = true;
      // $mail->Username = 'syauqi@namagz.com';
      // $mail->Password = 'koroko11';
      $mail->Username = 'dev.jakarta.bd@gmail.com';
      $mail->Password = 'JAKARTAbd2020';
      $mail->SMTPSecure = 'ssl';
      $mail->Port = 465;

      $mail->addReplyTo('no-reply@jakartabubbledrink.com', 'JBD Developer Team');
      $mail->setFrom('no-reply@jakartabubbledrink.com', 'JBD Developer Team');

      // Menambahkan penerima
      $mail->addAddress($to);

      // Menambahkan beberapa penerima


      // Subjek email
      $mail->Subject = '[No Reply] Pendaftaran Member JBD';

      // Mengatur format email ke HTML
      $mail->isHTML(true);

      // Konten/isi
       $data_['title'] = $title;
       $data_['msg'] = $desc;
       $data_['code'] = $data;
       $mailContent = $this->load->view('email_verifikasi',$data_,true);
      $mail->Body = $mailContent;
      // Menambahakn lampiran

      // Kirim email
      if(!$mail->send()){
          echo 'Pesan tidak dapat dikirim.';
          echo 'Mailer Error: ' . $mail->ErrorInfo;
      }else{
          //echo 'Pesan telah terkirim ';
      }
    }

}
