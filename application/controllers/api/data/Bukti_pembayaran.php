<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Bukti_pembayaran extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $htrans_id = $this->post('htrans_id');
          /*$bank_id = $this->post('bank_id');
          $norek = $this->post('norek');
          $owner = $this->post('owner');*/
          $get_htrans = $this->mymodel->getbywhere('htrans','htrans_id',$htrans_id,'row');
          if (isset($mem)) {
              $uploaddir = './assets/img/transaction/';
              $img = explode('.', $_FILES['img']['name']);
              $extension = end($img);
              $file_name =  md5(date('y-m-d h:i:s').$_FILES['img']['name']).".".$extension;
              $uploadfile = $uploaddir.$file_name;
              $status = 0;

              if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                if ($get_htrans->img_file!="") {
                  unlink($uploaddir.$get_htrans->img_file);
                }
                $data_upload = array( 
                  'img_file'=>$file_name,
                  'updated_at' => date('Y-m-d H:i:s'),
                  'transaction_status_id' => 2
                  );
                $this->mymodel->update('htrans',$data_upload,'htrans_id',$htrans_id);
                $msg = array('success'=>1,'message'=>'Upload Bukti Foto Berhasil');
                } else {
                $msg = array('success'=>0,'message'=>'Gagal Upload Bukti Foto');
                }
                $data_trans = array(
                  "description_in_indonesia" => "Sedang Diproses",
                  "description_in_english" => "In Progress"
                  );
                //$this->mymodel->update('htrans',$data_trans,'transaction_status_id',$get_htrans->transaction_status_id);
            
            if (!empty($data_upload)) {
              $msg = array('status' => 1, 'message'=>'Berhasil Update data' ,'data'=>$data_trans,'data_upload'=>$data_upload);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}