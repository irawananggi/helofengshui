<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAnrD16lI:APA91bH0MHVh9XsRh6tL8y5ZlGeE7z_-x1jBRPyajmKvUWnuN5wgKA8GspRjOvvf_t0-4Qo3MbugqZmRwLSsi73KHjQuXKL3skxDmj1GskZnyjIO5Vdrb0rQ0hZM1Mw6IGQaiRa-xgRo' );

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Produk_diterima extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          
          if (!empty($mem)) {
            $htrans_id = $this->post("htrans_id");
            $data = array(
              "transaction_status_id" => 3,
              "updated_at" => date("Y-m-d H:i:s")
            );
            
            if (!empty($data)) {
              $up = $this->mymodel->update("htrans",$data,"htrans_id",$htrans_id);
              $get_id = $this->mymodel->getbywhere("htrans","htrans_id",$htrans_id,"row");
              $kode_cantik = $this->mymodel->getbywhere("kode_cantik","htrans_id",$htrans_id,"row")->kode;
              $mem = $this->mymodel->getbywhere('member','member_id',$get_id->member_id,'row');
              if ($mem->fcm_id != "") {
                //send notif
                $this->send_notif("Transaksi Telah Selesai","Transaksi anda ".$get_id->tracking_id." senilai Rp. ".number_format(($get_id->total_price+$kode_cantik),0,"",".")." telah selesai, berikan kami rating atau feedback untuk mendapatkan poin", $mem->fcm_id, array('title' =>"Transaksi Telah Selesai" , 'message' => "Transaksi anda ".$get_id->tracking_id." senilai Rp. ".number_format(($get_id->total_price+$kode_cantik),0,"",".")." telah selesai, berikan kami rating atau feedback untuk mendapatkan poin", 'tipe' => 'detail_transaksi','content' => array("id_transaksi"=>$get_id->htrans_id, "order_id"=>$get_id->tracking_id )) );
              }
              $msg = array('status' => 1, 'message'=>'Berhasil Terima barang' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

  public function send_notif($title,$desc,$fcm_id,$data)
  {
    $Msg = array(
      'body' => $desc,
      'title' => $title
    );

    $fcmFields = array(
      'to' => $fcm_id,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    //echo $result."\n\n";
  }
}