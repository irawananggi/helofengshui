<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Get_poin_member extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {
            $get_poin = $this->mymodel->getbywhere('histori_poin','member_id',$mem->member_id,'result');
            $data = 0;
            foreach ($get_poin as $key => $value) {
              if ($value->status_poin == "didapatkan") {
                $data = $data + $value->poin;
              }
              else if($get_poin == "digunakan"){
                $data = $data - $value->poin;
              }
            }
            if ($data > 0) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil poin' ,'poin'=>$data);
            }else if ($data == 0) {
             $msg = array('status' => 1, 'message'=>'Tidak ada poin!' ,'poin'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'poin'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}