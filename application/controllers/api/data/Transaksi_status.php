<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Transaksi_status extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $transaction_status_id = $this->post('transaction_status_id');
          $verifikasi = $this->post('verifikasi');
          
          if (isset($mem)) {
            $data = array();
            if ($verifikasi==1) {
            $data = array(
              "description_in_indonesia" => "Dikirim oleh Penjual",
              "description_in_english" => "Sending by Seller"
              );
            }else if($verifikasi==2){
              $data = array(
              "description_in_indonesia" => "Dikirim oleh Kurir",
              "description_in_english" => "Sending by Courier"
              );
            }else if($verifikasi==3){
              $data = array(
              "description_in_indonesia" => "Sudah Diterima",
              "description_in_english" => "Already Accepted"
              );
            }else if($verifikasi==4){
              $data = array(
              "description_in_indonesia" => "Pesanan Dibatalkan",
              "description_in_english" => "Order Canceled"
              );
            }
            
            if (!empty($data)) {
              $this->mymodel->update('transaction_status',$data,'transaction_status_id',$transaction_status_id);
              $msg = array('status' => 1, 'message'=>'Berhasil Update data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}