<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Keranjang extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {
            $grand =0;
            $data = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
            foreach ($data as $key => $value) {
              $get_product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
              $get_image = $this->mymodel->getlastwhere('product_image','product_id',$value->product_id,'id_product_image',"ASC");
              $get_variant = $this->mymodel->getbywhere("product_variants","id",$value->product_variants_id,"row");
              
                            
              $hitung = $get_product->price * $value->quantity;
              $grand = $grand + $hitung;
              $value->nama_product = $get_product->product_name;
              if (!empty($get_variant)) {
                $value->variant_name = $get_variant->variant_name;
              }
              else{
                $value->variant_name = "";
              }
              $value->description = $get_product->description;
              $value->product_image = base_url('assets/img/product/'.$get_image->img_file);
              $value->harga = "Rp ".number_format($get_product->price,0,"",".");
              $value->intharga = (int)$get_product->price;
              $value->subtotal = "Rp ".number_format($hitung,0,"",".");
              $value->intsubtotal = $hitung;
              $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
                where product_id = '".$value->product_id."'","result");
              $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
                  where product_id = '".$value->product_id."'","result");
              $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
                  where product_id = '".$value->product_id."'","result");
              foreach ($total_trans as $key => $valuee) {
                $value->hitung_transaksi = $valuee->total_transaksi;
              }
              foreach ($average_rate as $key => $valuee) {
                $value->average_rate = round($valuee->average_rate);
              }
              foreach ($total_review as $key => $valuee) {
                $value->total_rate = $valuee->total_review;
              }
            }

            $voucher = null;
            $voucher_id = 0;
            $diskon_voucher_name="";
            $diskon_voucher_code="";
            $intvoucherdiscount=0;
            $diskon_voucher="";

            if (!empty($this->mymodel->getbywhere("member_voucher","is_deleted='0' and member_id=",$mem->member_id,"row"))) {
              $mem_voucher = $this->mymodel->getbywhere("member_voucher","is_deleted='0' and member_id=",$mem->member_id,"row")->voucher_id;
              $voucher = $this->mymodel->getbywhere("voucher","voucher_id",$mem_voucher,"row");
              $grand -= $voucher->discount;
            }

            if(!is_null($voucher)){
              $voucher_id = $voucher->voucher_id;
              $diskon_voucher_name = $voucher->title;
              $diskon_voucher_code = $voucher->code;
              $intvoucherdiscount=$voucher->discount;
              $diskon_voucher = "Rp.".number_format($voucher->discount,0,"",".");
            }

            //get member pengaman
            $subtotal_pengaman = 0;
            if (!empty($this->mymodel->getbywhere("member_pengaman","member_id",$mem->member_id,"result"))) {
              $pengaman_tambahan = $this->mymodel->getbywhere("member_pengaman","member_id",$mem->member_id,"result");
              foreach ($pengaman_tambahan as $key2 => $value2) {
                $get_pengaman = $this->mymodel->getbywhere("additional_prices","id",$value2->id_pengaman,"row");
                $value2->keterangan = $get_pengaman->keterangan;
                $value2->jenis = $get_pengaman->jenis;
                $value2->biaya = "Rp.".number_format($get_pengaman->biaya,0,"",".");
                $subtotal_pengaman = $subtotal_pengaman + ($get_pengaman->biaya*$value2->quantity);
              }
              $grand = $grand + $subtotal_pengaman;
            }
            else{
              $pengaman_tambahan = array();
            }
            $subtotal_pengaman = "RP.".number_format($subtotal_pengaman,0,"",".");
            $int_grand = $grand;
            $grand = "Rp ".number_format($grand,0,"",".");
            //potensi poin
            $hitung_dpt_poin =0;
            $setting_poin = $this->mymodel->getlast("poin","id");
            $hitung_dpt_poin = ($int_grand/$setting_poin->nominal)*$setting_poin->poin;
            $hitung_dpt_poin = "Rp.".number_format($hitung_dpt_poin,0,"",".");
            if (!empty($data)) {
              $msg = array(
                'status' => 1, 
                'message'=>'Berhasil ambil data' ,
                'grand_total'=>$grand, 
                'intgrand' => $int_grand, 
                'voucher' => [

                  "voucher_id" => $voucher_id, 
                  "voucher_name" => $diskon_voucher_name, 
                  "voucher_code" => $diskon_voucher_code, 
                  "discount" => $diskon_voucher, 
                  "intdiscount" => $intvoucherdiscount, 
                ],
                'potensi_poin' => $hitung_dpt_poin ,
                'data'=>$data, 
                'detail_pengaman' => $pengaman_tambahan, 
                'subtotal_pengaman' => $subtotal_pengaman);
            }else if (empty($data)) {
             $msg = array(
               'status' => 1, 
               'message'=>'Keranjang Kosong!' ,
               'grand_total'=>$grand, 
               'intgrand' => 0, 
               'voucher' => [

                 "voucher_id" => $voucher_id, 
                 "voucher_name" => $diskon_voucher_name, 
                 "voucher_code" => $diskon_voucher_code, 
                 "discount" => $diskon_voucher, 
                 "intdiscount" => $intvoucherdiscount, 
               ],
               'potensi_poin' => $hitung_dpt_poin ,
               'data'=>array(), 
               'detail_pengaman' => $pengaman_tambahan, 
               'subtotal_pengaman' => $subtotal_pengaman);
            }else {
              $msg = array(
                'status' => 0, 
                'message'=>'Data tidak ditemukan' ,
                'grand_total'=>$grand, 
                'intgrand' => 0, 
                'voucher' => [

                  "voucher_id" => $voucher_id, 
                  "voucher_name" => $diskon_voucher_name, 
                  "voucher_code" => $diskon_voucher_code, 
                  "discount" => $diskon_voucher, 
                  "intdiscount" => $intvoucherdiscount, 
                ],
                'potensi_poin' => $hitung_dpt_poin ,
                'data'=>array(), 
                'detail_pengaman' => $pengaman_tambahan, 
                'subtotal_pengaman' => $subtotal_pengaman);
            }
          }else {
              $msg = array('status' => 0, 
              'message'=>'Token Tidak Ditemukan ',
              'grand_total'=>0, 
              'intgrand' => 0, 
              'voucher' => [

                "voucher_id" => 0, 
                "voucher_name" => "", 
                "voucher_code" => "", 
                "discount" => "", 
                "intdiscount" => 0, 
              ],
              'potensi_poin' => 0 ,
              'data'=>array(), 
              'detail_pengaman' => 0, 
              'subtotal_pengaman' => 0);
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','grand_total'=>"Rp 0",'data'=>array());
        $this->response($msg);
      }
    }
}