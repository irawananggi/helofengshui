<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Recommended_product extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
      $mem="";
      $data = array();
      if ($token!='') {
        $mem = $this->mymodel->getbywhere('member','token',$token,"row");
        $get_keyword = $this->mymodel->getlastwhere('save_pencarian',"member_id",$mem->member_id,'save_pencarian_id');
        if (empty($get_keyword)) {
        //Random
          $data = $this->mymodel->getbywherelimitsort('product','is_deleted','0','0',"10",'RAND()','');
          foreach ($data as $key => $value) {
            if ($value->updated_at == null) {
              $value->updated_at = "";
            }
            $value->price = "Rp ".number_format($value->price,0,"",".");
            $value->quantity = number_format($value->quantity,0,"",".");
            $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
                where product_id = '".$value->product_id."'","result");
            $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
                where product_id = '".$value->product_id."'","result");
            $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
                where product_id = '".$value->product_id."'","result");
            foreach ($total_trans as $key => $valuee) {
              $value->hitung = $valuee->total_transaksi;
            }
            foreach ($average_rate as $key => $valuee) {
              $value->average_rate = round($valuee->average_rate);
            }
            foreach ($total_review as $key => $valuee) {
              $value->total_rate = $valuee->total_review;
            }

            if (!empty($mem)) {
                $cek_wishlist = $this->mymodel->getbywhere('wishlist',"product_id='".$value->product_id."' and member_id=",$mem->member_id,'row');
                if (empty($cek_wishlist)) {
                  $value->is_wishlist = 0;
                }else{
                  $value->is_wishlist = 1;
                }
              }else{
                $value->is_wishlist = 0;
              }
            $get_img = $this->mymodel->getbywheresort('product_image','product_id',$value->product_id,'product_id','desc');
            $gambar = "";
            foreach ($get_img as $key => $valuee) {
            $gambar = base_url("assets/img/product/".$valuee->img_file);
            }
            $value->img = $gambar;
            $value->list_variant = $this->mymodel->getbywhere("product_variants","product_id",$value->product_id,"result");
          }

        }else{
        //Pencarian Like last Keyword pencarian
          $data = $this->mymodel->withquery("select * from product where product_name like '%".$get_keyword->keyword."%' and is_deleted='0' LIMIT 0,10","result");
          foreach ($data as $key => $value) {
            if ($value->updated_at == null) {
              $value->updated_at = "";
            }
            $value->price = "Rp ".number_format($value->price,0,"",".");
            $value->quantity = number_format($value->quantity,0,"",".");
            $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
                where product_id = '".$value->product_id."'","result");
            $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
                where product_id = '".$value->product_id."'","result");
            $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
                where product_id = '".$value->product_id."'","result");
            foreach ($total_trans as $key => $valuee) {
              $value->hitung = $valuee->total_transaksi;
            }
            foreach ($average_rate as $key => $valuee) {
              $value->average_rate = round($valuee->average_rate);
            }
            foreach ($total_review as $key => $valuee) {
              $value->total_rate = $valuee->total_review;
            }
            $get_img = $this->mymodel->getbywheresort('product_image','product_id',$value->product_id,'product_id','desc');
            $gambar = "";
            foreach ($get_img as $key => $valuee) {
            $gambar = base_url("assets/img/product/".$valuee->img_file);
            }
            $value->img = $gambar;

          }

        }
        $page = $this->get('page');
        if ( $page < 1) { $page=1; }
        $total = $this->get('total');
        $start = ($page - 1) * $total;
        
        if (!empty($data)) {
          $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
        }else {
                                                      
            $data = $this->mymodel->getbywherelimitsort('product','1','1','0',"10",'RAND()','');
              foreach ($data as $key => $value) {
                  if ($value->updated_at == null) {
                      $value->updated_at = "";
                  }
                              $value->price = "Rp ".number_format($value->price,0,"",".");
                              $value->quantity = number_format($value->quantity,0,"",".");
                              $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans
                                                                       where product_id = '".$value->product_id."'","result");
                                                                       $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting
                                                                                                                 where product_id = '".$value->product_id."'","result");
                                                                                                                 $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting
                                                                                                                                                           where product_id = '".$value->product_id."'","result");
                                                                                                                                                           foreach ($total_trans as $key => $valuee) {
                                                                                                                                                           $value->hitung = $valuee->total_transaksi;
                                                                                                                                                           }
                                                                                                                                                           foreach ($average_rate as $key => $valuee) {
                                                                                                                                                           $value->average_rate = round($valuee->average_rate);
                                                                                                                                                           }
                                                                                                                                                           foreach ($total_review as $key => $valuee) {
                                                                                                                                                           $value->total_rate = $valuee->total_review;
                                                                                                                                                           }
                                                                                                                                                           $get_img = $this->mymodel->getbywheresort('product_image','product_id',$value->product_id,'product_id','desc');
                                                                                                                                                           $gambar = "";
                                                                                                                                                           foreach ($get_img as $key => $valuee) {
                                                                                                                                                           $gambar = base_url("assets/img/product/".$valuee->img_file);
                                                                                                                                                           }
                                                                                                                                                           $value->img = $gambar;
                                                                                                                                                           
                                                                                                                                                           }
                                                                                                                                                           
                                                                                                                                                           $page = $this->get('page');
                                                                                                                                                           if ( $page < 1) { $page=1; }
                                                                                                                                                           $total = $this->get('total');
                                                                                                                                                           $start = ($page - 1) * $total;
                                                                                                                                                           
                                                                                                                                                       if (!empty($data)) {
                                                                                                                                                       $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
                                                                                                                                                       }else {
                                                                                                                                                       $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
                                                                                                                                                       }
                                                                                                                                                       $this->response($msg);
        }

        $this->response($msg);
      }else{
      //Random Product
        $data = $this->mymodel->getbywherelimitsort('product','1','1','0',"10",'RAND()','');
        foreach ($data as $key => $value) {
            if ($value->updated_at == null) {
              $value->updated_at = "";
            }
            $value->price = "Rp ".number_format($value->price,0,"",".");
            $value->quantity = number_format($value->quantity,0,"",".");
            $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
                where product_id = '".$value->product_id."'","result");
            $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
                where product_id = '".$value->product_id."'","result");
            $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
                where product_id = '".$value->product_id."'","result");
            foreach ($total_trans as $key => $valuee) {
              $value->hitung = $valuee->total_transaksi;
            }
            foreach ($average_rate as $key => $valuee) {
              $value->average_rate = round($valuee->average_rate);
            }
            foreach ($total_review as $key => $valuee) {
              $value->total_rate = $valuee->total_review;
            }
            $get_img = $this->mymodel->getbywheresort('product_image','product_id',$value->product_id,'product_id','desc');
            $gambar = "";
            foreach ($get_img as $key => $valuee) {
            $gambar = base_url("assets/img/product/".$valuee->img_file);
            }
            $value->img = $gambar;

          }

        $page = $this->get('page');
        if ( $page < 1) { $page=1; }
        $total = $this->get('total');
        $start = ($page - 1) * $total;
        
        if (!empty($data)) {
          $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
        }else {
          $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
        }
        $this->response($msg);
      }

    }

}
