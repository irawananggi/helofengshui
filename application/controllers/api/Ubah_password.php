<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Ubah_password extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $password_lama = md5($this->post('password_lama'));

        $pl = $this->mymodel->getbywhere('member',"member_id='$mem->member_id' AND password='$password_lama'",null,"row");
        
        
            //$pl = $this->mymodel->getbywhere('member','password',$password_lama,"row");
            if (isset($pl)) {

            $password_baru = md5($this->post('password_baru'));
            $konfirmasi_password_baru = md5($this->post('konfirmasi_password_baru'));
           

              if ($password_baru == $konfirmasi_password_baru) {
                $data = array(
                  "password" => $password_baru
                  );
                
                if (!empty($data)) {
                  $message = array(
                    "ind" => 'Berhasil Update profile',
                    "eng" => 'Successfully updated profile',
                    "man" => '已成功更新個人資料'
                  );
                  $this->mymodel->update('member',$data,'member_id',$mem->member_id);
                  $msg = array('status' => 1, 'message'=>array($message) ,'data'=>$data);
                }else {

                  $message = array(
                    "ind" => 'Data tidak ditemukan',
                    "eng" => 'Data not found',
                    "man" => '未找到數據'
                  );
                  $msg = array('status' => 0, 'message'=>array($message) ,'data'=>array());
                }
              }else {
                
                  $message = array(
                    "ind" => 'Password yang Anda Masukkan Tidak Sama',
                    "eng" => 'The Password You Entered Is Not the Same',
                    "man" => '您輸入的密碼不一樣'
                  );
                  $msg = array('status' => 0, 'message'=>array($message) ,'data'=>array());
              }
            }else {
                  $message = array(
                    "ind" => 'Password lama salah',
                    "eng" => 'Wrong old password',
                    "man" => '舊密碼錯誤'
                  );
                $msg = array('status' => 0, 'message'=>array($message) ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}