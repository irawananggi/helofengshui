<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');
define( 'API_ACCESS_KEY', 'AAAAfIemvrw:APA91bEqHB8ub4fYgb0AA_TtgZYIkAk0M65Bw0kdYnZsQ6WkmSjZ1H9A6VUDdPlHh7NqVe9QoownDrslpGh3JAPYRs4bu37qSbMaz-7Ob7kWmJVf8JW-LVulM2Tw4h8OLpirRi5xVdLQ' );


defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'controllers/api/midtrans/Midtrans.php';

class Upgrade_premium extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    }


    public function index($val = "")
    {
    $arr = json_decode(file_get_contents("php://input"));
    //print_r($arr);
    if (empty($arr)){ 
        exit("Data empty.");
    } 
    else {
    \Midtrans\Config::$isProduction = false;
    \Midtrans\Config::$serverKey = 'SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY';
    //\Midtrans\Config::$serverKey = 'Mid-server-vMGps8Of7pnpRPZwEbuwHUzr';
    $notif = new \Midtrans\Notification();

    $transaction = $notif->transaction_status;
    $type = $notif->payment_type;
    $order_id = $notif->order_id;
    $fraud = $notif->fraud_status;
    $get_order = $this->mymodel->getbywhere("transaksi","order_id",$order_id,'row');
    $get_member = $this->mymodel->getbywhere("member","id_member",$get_order->id_member,'row');
    $title = "";
    $desc = "";

    if ($transaction == 'capture') {
      // For credit card transaction, we need to check whether transaction is challenge by FDS or not
      if ($type == 'credit_card'){
        if($fraud == 'challenge'){
          // TODO set payment status in merchant's database to 'Challenge by FDS'
          // TODO merchant should decide whether this transaction is authorized or not in MAP
          echo "Transaction order_id: " . $order_id ." is challenged by FDS";
          }
          else {
          // TODO set payment status in merchant's database to 'Success'
          echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
          }
        }
      }
      else if ($transaction == 'settlement'){
      // TODO set payment status in merchant's database to 'Settlement'
      //$order_id = $_REQUEST['order_id'];
      //$transaction_status = $_REQUEST['transaction_status'];
      $data = array(
        "status_transaksi" => "2"
      );
      $up1 = $this->mymodel->update("transaksi",$data,"order_id",$order_id);
      if ($up1) {
      $data2 = array();
        if ($get_order->id_langganan == "1") {
          $data2 = array(
            "id_langganan" => $get_order->id_langganan,
            "tgl_langganan" => date("Y-m-d H:i:s"),
            "exp_langganan" => date("Y-m-d H:i:s",strtotime('+ 1 month'))
          );
        }else if($get_order->id_langganan == "2"){
          $data2 = array(
            "id_langganan" => $get_order->id_langganan,
            "tgl_langganan" => date("Y-m-d H:i:s"),
            "exp_langganan" => date("Y-m-d H:i:s",strtotime('+ 1 year'))
          );
        }else if($get_order->id_langganan == "3"){
          $data2 = array(
            "id_langganan" => $get_order->id_langganan,
            "tgl_langganan" => date("Y-m-d H:i:s"),
            "exp_langganan" => date("Y-m-d H:i:s",strtotime('+ 1 week'))
          );
        }
      $up = $this->mymodel->update("member",$data2,'id_member',$get_order->id_member);
                            
      $msg = array('status' => 1, 'message'=>'Berhasil Upgrade Member', 'data'=> $data);
      $status="200";
      }
      else{
        $msg = array('status' => 1, 'message'=>'Gagal Update Data','data'=>new \stdClass());
        $status="422";
      }
      if ($get_member->id_languange == "1") {
        $title = "Notifikasi Pembayaran AHA";
        $desc = "Pembayaran Berhasil";
      }
      else if($get_member->id_languange == "2"){
        $title = "AHA Payment Notification";
        $desc = "Payment Success";
      }
      $this->send_user($title,$desc,$get_member->fcm_id);
      echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        $this->export_invoice($order_id,$transaction,$type);
      }
      else if($transaction == 'pending'){
      // TODO set payment status in merchant's database to 'Pending'
        $data = array(
          "status_transaksi" => "1"
        );
        $up1 = $this->mymodel->getbywhere("transaksi",$data,"order_id",$order_id);
        echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        $this->export_invoice($order_id,$transaction,$type);
      }
      else if ($transaction == 'deny') {
      // TODO set payment status in merchant's database to 'Denied'
      echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
      }
      else if ($transaction == 'expire') {
      // TODO set payment status in merchant's database to 'expire'
      $data = array(
        "status_transaksi" => "3"
      );
      $up1 = $this->mymodel->update("transaksi",$data,"order_id",$order_id);
      if ($get_member->id_languange == "1") {
        $title = "Notifikasi Pembayaran AHA";
        $desc = "Pembayaran telah kadaluarsa";
      }
      else if($get_member->id_languange == "2"){
        $title = "AHA Payment Notification";
        $desc = "Payment has been expired";
      }
      $this->send_user($title,$desc,$get_member->fcm_id);
      echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
      }
      else if ($transaction == 'cancel') {
      // TODO set payment status in merchant's database to 'Denied'
      $data = array(
        "status_transaksi" => "4"
      );
      $up1 = $this->mymodel->update("transaksi",$data,"order_id",$order_id);
      if ($get_member->id_languange == "1") {
        $title = "Notifikasi Pembayaran AHA";
        $desc = "Pesanan Dibatalkan";
      }
      else if($get_member->id_languange == "2"){
        $title = "AHA Payment Notification";
        $desc = "Order has been cancelled";
      }
      $this->send_user($title,$desc,$get_member->fcm_id);
      echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
        $this->export_invoice($order_id,$transaction,$type);
      }
    }
  }

  public function send_user($title,$desc,$id_fcm)
  {
    $Msg = array(
      'body' => $desc,
      'title' => $title,
      'click_action'=>"FLUTTER_NOTIFICATION_CLICK"
    );
    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg
      // 'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    
    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    if ($berhasil=='1') {
      return true;
    }
    else if($berhasil=='0'){
      return false;
    }
    echo $result . "\n\n";
  }


}
