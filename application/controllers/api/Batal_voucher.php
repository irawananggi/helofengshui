<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';
class Batal_voucher extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $voucher_code = $this->post('code_voucher');
          
          $get_voucher_id = $this->mymodel->getbywhere('voucher','code_voucher',$voucher_code,'row');
          $cek_kode = $this->mymodel->getbywhere('member_voucher',"member_id='".$mem->member_id."' and voucher_id=",$get_voucher_id->id,'result');
          

          if (isset($mem)) {
            $get_cart = $this->mymodel->getbywhere('cart',"member_id='$mem->member_id'",null,"row");
            $kategori = $this->mymodel->getbywhere('kategori','id',$get_cart->kategori_id,"row");
              

              $id =$get_cart->order_id;
              $total = $kategori->harga;
              $kategori_id = $get_cart->kategori_id;
              $get_midtrans = $this->payment_midtrans($id,$total,$kategori_id,$mem->member_id);

               $data_v = array(
                'voucher_id' =>NULL,
                'total_voucher' =>NULL,
                'total_bayar' =>$total,
                'token' =>$get_midtrans,
              );
              $update_cart = $this->mymodel->update('cart',$data_v,'id',$get_cart->id);

              if (!empty($get_midtrans)) {
          
                $cart = $this->mymodel->withquery("select * from cart where kategori_id='".$kategori_id."' and member_id='".$mem->member_id."' order by id DESC ",'row');

                $datax['email'] = $mem->email;
                $datax['nama'] = $mem->nama_lengkap;
                $datax['phone'] = $mem->phone;
                $datax['name_in'] = $kategori->name_in;
                $datax['created_at'] = $cart->created_at;
                $datax['order_id'] = $cart->order_id;

              //$msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=>'https://app.sandbox.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
              //production
              $msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=> 'https://app.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
              $status="200";
             // $this->export_invoice($id,"pending","Belum Dipilih");
            }
               $hapus = $this->mymodel->delete2('member_voucher','member_id',$mem->member_id,'voucher_id',$get_voucher_id->id);
              if ($hapus) {                
                $msg = array('status' => 1, 'message'=>'Berhasil Menghapus data voucher' ,'data'=>array());
              }else {
                $msg = array('status' => 0, 'message'=>'Voucher Gagal dihapus' ,'data'=>array());
              } 
            
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }

    public function payment_midtrans($id,$total,$kategori_id,$member_id){
     
      //Set Your server key
       //\Midtrans\Config::$serverKey = "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY";
     \Midtrans\Config::$serverKey = "Mid-server-FwNC99hl8rYgJAKNnasMqreG";

      // Uncomment for production environment
      \Midtrans\Config::$isProduction = true;
      //\Midtrans\Config::$isProduction = false;

      \Midtrans\Config::$isSanitized = true;
      //\Midtrans\Config::$isSanitized = false;
      \Midtrans\Config::$is3ds = false;

      // Mandatory for Mandiri bill payment and BCA KlikPay
      // Optional for other payment methods

      $transaction_details = array(
        'order_id' => $id,
        'gross_amount' => (int) $total, // no decimal allowed
      );

      $item1_details = array(
          'id' => $id,
          'price' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->harga,
          'quantity' => 1,
          'name' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->name_in
          );
      $item_details = array ($item1_details);

      $get_member = $this->mymodel->getbywhere('member','member_id',$member_id,'row');

      $customer_details = array(
       'email'         => $get_member->email, //mandatory
      );

      // Fill transaction details
      $transaction = array(
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
      

      $snapToken = \Midtrans\Snap::getSnapToken($transaction);
      return $snapToken;
    }

}