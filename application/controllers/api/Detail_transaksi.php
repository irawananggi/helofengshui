<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';

//require APPPATH . '/libraries/REST_Controller.php';

class Detail_transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
  

    public function index_get()
    {
       $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
      if ($token != '') {
          $member = $this->mymodel->getbywhere('member','token',$token,"row");
          $id_member = $member->member_id;
          
          if (!empty($member)) {
            
            $get_data_cart = $this->mymodel->getbywhere('cart','member_id',$id_member,"row");
            
            $kategori_id = $get_data_cart->kategori_id;
            
                if (!empty($kategori_id)) {
                  $kategori = $this->mymodel->getbywhere('kategori','id',$kategori_id,"row");
                  $id = "HF-".date("Ymdhis").$kategori_id;
                  
                  $get_cart = $this->mymodel->getbywhere('cart',"member_id='$member->member_id'",null,"row");
                 
                  $dat_voucher_id = $get_cart->voucher_id;
                  
                  $get_voucher = $this->mymodel->getbywhere('voucher','id',$get_cart->voucher_id,"row");

                  if(empty($get_cart->voucher_id)){
                    $voucher_id = NULL;
                  }else{
                    $voucher_id = $get_cart->voucher_id;
                  }

                  

                  if($get_voucher->dis_id  == '1'){
                    $total_voucher = (($kategori->harga * $get_voucher->nominal)/100);
                    $total_bayar = ($kategori->harga - $cek_harga_v);
                  }else{
                    $total_voucher = $get_voucher->nominal;
                    $total_bayar = ($kategori->harga - $cek_harga_v);
                  }
                  if($voucher_id == NULL){
                    $total_bayar = $total_bayar;
                  }else{
                    $total_bayar = $kategori->harga;

                  }
                 if(empty($get_cart->voucher_id)){
                  $total = $total_bayar;
                    $data = array(
                      "kategori_id" => $kategori_id,
                      "name_kategori" => $kategori->name_in,
                      "foto_kategori" => base_url("uploads/file/".$kategori->icon),
                      //"tgl_order" => $get_cart->tgl_bayar ,
                      "kode_voucher" => '',
                      "kode_voucher_rupiah" => 'Rp ',
                      "harga_kategori" => $kategori->harga,
                      "harga_kategori_rupiah" => 'Rp '.numberToCurrency($kategori->harga),
                      "sub_total" => $total,
                      "sub_total_rupiah" => 'Rp '.numberToCurrency($total),
                      "nama_langganan" =>$member->nama_lengkap,
                      "order_id" => $id,
                      "merchant_id" => "G921317011",                      
                      "client_key" => "Mid-client-pRQOVZj2nE94s0ee",
                      "server_key" => "Mid-server-FwNC99hl8rYgJAKNnasMqreG"
                      //"client_key" => "SB-Mid-client-InpddDpe3dYs0hX2",
                      //"server_key" => "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY"
                    );
                 }else{
                    if($get_voucher->dis_id  == '1'){
                      $cek_harga_v = (($kategori->harga * $get_voucher->nominal)/100);
                      $total_bayar = ($kategori->harga - $cek_harga_v);
                    }else{
                      $total_bayar = ($kategori->harga - $get_voucher->nominal);
                    } 
                    $total = $total_bayar;
                    $data = array(
                      "kategori_id" => $kategori_id,
                      "name_kategori" => $kategori->name_in,
                      "foto_kategori" => base_url("uploads/file/".$kategori->icon),
                      //"tgl_order" => $get_cart->tgl_bayar,
                      "kode_voucher" => $total_voucher,
                      "kode_voucher_rupiah" => 'Rp '.numberToCurrency($total_voucher),
                      "harga_kategori" => $kategori->harga,
                      "harga_kategori_rupiah" => 'Rp '.numberToCurrency($kategori->harga),
                      "sub_total" => $total,
                      "sub_total_rupiah" => 'Rp '.numberToCurrency($total),
                      "nama_langganan" =>$member->nama_lengkap,
                      "order_id" => $id,
                      "merchant_id" => "G921317011",                      
                      "client_key" => "Mid-client-pRQOVZj2nE94s0ee",
                      "server_key" => "Mid-server-FwNC99hl8rYgJAKNnasMqreG"
                      //"client_key" => "SB-Mid-client-InpddDpe3dYs0hX2",
                      //"server_key" => "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY"
                    );
                 }
                                   
                /*  //if production uncomment this
                  $data = array(
                    "kategori_id" => $kategori_id,
                    "total_bayar" => $kategori->harga,
                    "nama_langganan" =>$member->nama_lengkap,
                    "order_id" => $id,
                    "merchant_id" => "G921317011",
                    "client_key" => "Mid-client-pRQOVZj2nE94s0ee",
                    "server_key" => "Mid-server-FwNC99hl8rYgJAKNnasMqreG"
                  );*/


                

                 
                    $get_midtrans = $this->payment_midtrans($id,$total,$kategori_id,$member->member_id,$dat_voucher_id);
                     $cek_data = $this->mymodel->getbywhere('cart',"member_id='$member->member_id'",null,"result");
                     $cek_data2 = $this->mymodel->getbywhere('cart',"member_id='$member->member_id'",null,"row");


                     
                     if(count($cek_data) > 0){
                      
                         $data_upload = array( 
                            'kategori_id' => $kategori_id,
                            /*'voucher_id' =>$voucher_id,
                            'total_voucher' =>$total_voucher,
                            'harga_kategori' =>$kategori->harga,*/
                            'total_bayar' => $total_bayar,
                            'tgl_bayar' => date('Y-m-d H:i:s'),
                            'tgl_expired' => date('Y-m-d H:i:s',strtotime('+1 day')),
                            'order_id'=>$id,
                            'token' => $get_midtrans,
                            'created_at' => date("Y-m-d H:i:s")
                          );
                        $this->mymodel->update('cart',$data_upload,'id',$cek_data2->id);



                     }else{
                            $data_in = array(
                              "kategori_id" => $kategori_id,
                              "member_id" => $member->member_id,
                              /*'voucher_id' =>$voucher_id,
                              'total_voucher' =>$total_voucher,*/
                              'harga_kategori' =>$kategori->harga,
                              "tgl_bayar" => date('Y-m-d H:i:s'),
                              "tgl_expired" => date('Y-m-d H:i:s',strtotime('+1 day')),
                              "total_bayar" => $total_bayar,
                              "order_id" => $id,
                              "token" => $get_midtrans,
                              "created_at" => date("Y-m-d H:i:s")
                            );
                          
                            $in = $this->mymodel->insert("cart",$data_in);



                     }
            $get_cart2 = $this->mymodel->getbywhere('cart',"member_id='$member->member_id'",null,"row");
                 
           $data['tgl_order']=$get_cart2->tgl_bayar;
                    

                    if (!empty($get_midtrans)) {
                      //$cart = $this->mymodel->getbywhere('cart','kategori_id',$kategori_id,"row");
                      $cart = $this->mymodel->withquery("select * from cart where kategori_id='".$kategori_id."' and member_id='".$member->member_id."' order by id DESC ",'row');

                        $datax['email'] = $member->email;
                        $datax['nama'] = $member->nama_lengkap;
                        $datax['phone'] = $member->phone;
                        $datax['name_in'] = $kategori->name_in;
                        $datax['created_at'] = $cart->created_at;
                        $datax['order_id'] = $cart->order_id;

                        /*$this->load->library('email');      

                        $econfig['protocol'] = "smtp";
                        $econfig['smtp_host'] = "ssl://smtp.googlemail.com";
                        $econfig['smtp_port'] = 465;
                        $econfig['smtp_user'] = "helofengshui@gmail.com"; 
                        $econfig['smtp_pass'] = "Helofengshui2021";
                        $econfig['charset'] = "utf-8";
                        $econfig['mailtype'] = "html";

                        $this->email->initialize($econfig);
                        $this->email->set_newline("\r\n");

                        $this->email->from('helofengshui@gmail.com', 'Helofengshui');
                        $this->email->to('helofengshui@gmail.com');

                        $this->email->subject('Transaksi Baru');

                        $this->email->message($this->load->view('html_invoice_new',$datax,true));
                        $send_email = $this->email->send();*/



                        
                       /* // Tampilkan pesan sukses atau error
                        cek($kategori->name_in);
                        die();*/

                      //$up = $this->mymodel->getbywhere("member",$data2,'id_member',$member->id_member);
                      //sandbox
                      //$msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=>'https://app.sandbox.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
                      //production
                      $msg = array('status' => 1, 'message'=>'Invoice Created', 'token'=> $get_midtrans,'redirect_url'=> 'https://app.midtrans.com/snap/v2/vtweb/'.$get_midtrans, 'data' => $data);
                      $status="200";
                     // $this->export_invoice($id,"pending","Belum Dipilih");
                    }

                  }
                  else{
                      $msg = array('status' => 0, 'message'=>'Input tidak valid', 'token'=> "",'data'=> $data);
                      $status="422";
                  }
                }else{
                  $msg = array('status' => 1, 'message'=>'Member tidak ditemukan', 'token'=> "",'data'=>$data);
                  $status="401";
                }
        }else{
          $msg = array('status' => 0, 'message'=>'Token Tidak ditemukan', 'token'=> "",'data'=>$data );
          $status="401";
        }        
        $this->response($msg,$status);
        die();
    }

    public function payment_midtrans($id,$total,$kategori_id,$member_id,$dat_voucher_id){
        
      //Set Your server key
      // \Midtrans\Config::$serverKey = "SB-Mid-server-FU1BRgrZU8Lweh2GvAB_qRhY";
     \Midtrans\Config::$serverKey = "Mid-server-FwNC99hl8rYgJAKNnasMqreG";

      // Uncomment for production environment
      \Midtrans\Config::$isProduction = true;
      //\Midtrans\Config::$isProduction = false;

      \Midtrans\Config::$isSanitized = true;
      //\Midtrans\Config::$isSanitized = false;
      \Midtrans\Config::$is3ds = false;

      // Mandatory for Mandiri bill payment and BCA KlikPay
      // Optional for other payment methods

      $transaction_details = array(
        'order_id' => $id,
        'gross_amount' => (int) $total, // no decimal allowed
      );

      $item1_details = array(
          'id' => $id,
          'price' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->harga,
          'quantity' => 1,
          'name' => $this->mymodel->getbywhere("kategori",'id',$kategori_id,'row')->name_in
          );
      if($dat_voucher_id != NULL){

          $get_cart = $this->mymodel->getbywhere('cart',"member_id='$member_id'",null,"row");
          $get_voucher = $this->mymodel->getbywhere('voucher',"id='$get_cart->voucher_id'",null,"row");
          $item1_details2 = array(
            'id' => 'v_'.$id,
            'price' => -$get_cart->total_voucher,
            'quantity' => 1,
            'name' => 'Diskon Voucher'
          );

        $item_details = array ($item1_details,$item1_details2);
      }else{

        $item_details = array ($item1_details);
      }


      $get_member = $this->mymodel->getbywhere('member','member_id',$member_id,'row');

      $customer_details = array(
       'email'         => $get_member->email, //mandatory
      );

      // Fill transaction details
      $transaction = array(
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
      

      $snapToken = \Midtrans\Snap::getSnapToken($transaction);
      return $snapToken;
    }

}
