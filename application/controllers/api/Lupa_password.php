<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require APPPATH . 'libraries/Email.php';
require 'phpmailer/PHPMailerAutoload.php';
//ob_start();

class Lupa_password extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $email = $this->post('email');
      $msg ="";
      if ($email !="") {
          $cek = $this->mymodel->getbywhere('member',"email='$email'",null,"row");
          $data_diri = 0;
          if (!empty($cek)) {
            $cek_token_email = $this->mymodel->getbywhere('member_email',"member='$cek->member_id'",null,"result");
            if(count($cek_token_email) == 0){
              $token_email = md5(uniqid().$this->post('email').date('Y-m-d H:i:s'));
              $data = array(
                "token_email" => $token_email,
                "member" => $cek->member_id
                );     
              $this->mymodel->insert('member_email',$data);

            }else{
              $token_email = md5(uniqid().$this->post('email').date('Y-m-d H:i:s'));
              $data = array(
                "token_email" => $token_email
                );     
              $this->mymodel->update('member_email',$data,'member',$cek->member_id);
            }
           
            $token_email = $this->mymodel->getbywhere('member_email',"member='$cek->member_id'",null,"row");
            $url = "https://helofengshui.com/reset_pass/index/".in_de(array('id'=>$token_email->token_email));
             $data = array(
                  "email" =>$cek->email,
                  "link" =>$url
                  );


            $datax['email'] = $cek->email;
            $datax['nama'] = $cek->nama_lengkap;
            $datax['link'] = "https://helofengshui.com/reset_pass/index/".in_de(array('id'=>$token_email->token_email));

            $this->load->library('email');      

            $econfig['protocol'] = "smtp";
            $econfig['smtp_host'] = "ssl://smtp.googlemail.com";
            $econfig['smtp_port'] = 465;
            $econfig['smtp_user'] = "helofengshui@gmail.com"; 
            $econfig['smtp_pass'] = "helofengshui1234";
            $econfig['charset'] = "utf-8";
            $econfig['mailtype'] = "html";

            $this->email->initialize($econfig);
            $this->email->set_newline("\r\n");

            $this->email->from('helofengshui@gmail.com', 'Helofengshui');
            $this->email->to($email);

            $this->email->subject('Reset Password Akun');

            $this->email->message($this->load->view('html_email',$datax,true));

            // Tampilkan pesan sukses atau error
            if ($this->email->send()) {
               $message = array(
                "ind" => 'Silahkan Cek Email Kamu Untuk Melakukan Reset Password Akun Fengshui',
                "eng" => 'Please Check Your Email To Reset Fengshui Account Password',
                "man" => '請檢查您的電子郵件以重置風水帳戶密碼'
              );
            } else {

              $message = array(
                "ind" => 'Error! email tidak dapat dikirim.',
                "eng" => 'Error! email could not be sent.',
                "man" => '錯誤！ 電子郵件無法發出'
              ); 


                $message = 'Please Check Your Email To Reset Fengshui Account Password';
            }



$message = 'Please Check Your Email To Reset Fengshui Account Password';


            $msg = array('status'=>1,'message'=>$message,'data'=>$data);
           
          }else{
             $message_data = array(
                "ind" => 'Email tidak terdaftar',
                "eng" => 'Email is not registered',
                "man" => '郵箱未註冊'
              ); 
         
            $msg = array('status'=>0,'message'=>array($message_data),'data' => null, "data" =>array());
          }

        }else if($email==""){
          $message = array(
                "ind" => 'Email Harus Diisi',
                "eng" => 'Email Required',
                "man" => '需要電子郵件'
              );
            $msg = array('status' => 0, 'message'=>array($message));
        }else if($password==""){
            $message = array(
                "ind" => 'Password Harus diisi',
                "eng" => 'Password Required',
                "man" => '需要密碼'
              );
            $msg = array('status' => 0, 'message'=>array($message_data));
        }
        $this->response($msg);
    }

}
