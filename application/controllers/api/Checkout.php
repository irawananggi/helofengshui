<?php
header("Access-Control-Allow-Origin: *"); header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); header('Access-Control-Request-Headers: origin, x-requested-with');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require_once('midtrans/Midtrans.php');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Email.php';
require APPPATH . 'libraries/Format.php';
class Checkout extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          

          if (isset($mem)) {
            $get_cart = $this->mymodel->getbywhere('cart',"member_id='$mem->member_id'",null,"row");
            $get_cart_detail = $this->mymodel->getbywhere('cart_detail','cart_id',$get_cart->id,"result");
           
                if(!empty(@$get_cart->voucher_id)){
                  $data_s = array(
                  "status_v" => "1"
                  );

                  $save_status =   $this->mymodel->update('member_voucher',$data_s,"member_id='".$mem->member_id."' and voucher_id=",$get_cart->voucher_id);
                }
                if(!empty($get_cart)){
                  $data_in = array(
                    "kategori_id" => $get_cart->kategori_id,
                    "member_id" => $get_cart->member_id,
                    'voucher_id' =>$get_cart->voucher_id,
                    'total_voucher' =>$get_cart->total_voucher,
                    'harga_kategori' =>$get_cart->harga_kategori,
                    "tgl_bayar" => $get_cart->tgl_bayar,
                    "tgl_expired" => $get_cart->tgl_expired,
                    "total_bayar" => $get_cart->total_bayar,
                    'kode_status' => "1",
                    "status_permintaan" => 'Menunggu pembayaran',
                    "order_id" => $get_cart->order_id,
                    "token" => $get_cart->token,
                    "created_at" => $get_cart->created_at
                  );
               
                  $in = $this->mymodel->insert("permintaan",$data_in);
                  $parent_id = $this->mymodel->getlast('permintaan','id')->id;

                  $data_cart_detail= $this->general_model->datagrab(array(
                       'tabel'=>array(
                        'cart_detail a' => ''
                       ),
                       'where'=>array('a.cart_id' =>$get_cart->id),
                      ));
                  foreach ($data_cart_detail->result() as $key) {
                    $vcvc = array(
                      'tabel'=>'permintaan_detail',
                        'data'=>array(
                          'permintaan_id'  => $parent_id,
                          'member_id'  => $mem->member_id,
                          'form_kategori_id'  => $key->form_kategori_id,
                          'isi'  => $key->isi,
                          'created_at'  => $key->created_at,
                        )
                        );
                                

                    $update = $this->general_model->save_data($vcvc);

                  }

                  
                  if ($update) {

                      $get_cart = $this->mymodel->getbywhere('cart','member_id',$mem->member_id,"row");
                      


                  $cek_data_tran = $this->mymodel->withquery("select * from permintaan where member_id='$mem->member_id' AND kategori_id='$get_cart->kategori_id' ","result");

                  if(count($cek_data_tran) > 0){

                     $data_mem_tran = array(
                      "status_permintaan" => 'Cancel',
                      "kode_status" =>2
                      );
                   $updates =  $this->mymodel->update('permintaan',$data_mem_tran,"member_id='".$mem->member_id."' and kode_status=1 and id !='$parent_id' and kategori_id=",$get_cart->kategori_id);
                  }


                      $hapus2 = $this->mymodel->delete('cart_detail','member_id',$mem->member_id);
                      $hapus3 = $this->mymodel->delete('cart','member_id',$mem->member_id);             
                    $msg = array('status' => 1, 'message'=>'Transaksi tersimpan' ,'data'=>array());
                  }else {
                    $msg = array('status' => 0, 'message'=>'Data gagal disimpan' ,'data'=>array());
                  } 
                }else {
                    $msg = array('status' => 0, 'message'=>'Cart tidak ditemukan' ,'data'=>array());
                  } 
            
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }

}