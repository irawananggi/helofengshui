<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Fileupload extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    // insert new data to account
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {
           /* cek(count($_FILES['files']['name']));
            die();*/
            $jml_di_upload = count($_FILES['files']['name']);

             if ($jml_di_upload >= 1) {
               /* cek($jml_di_upload);
                die();*/
                $uploaddir = './uploads/form_file/';
                $msgs = array();
                for ($i=0; $i < $jml_di_upload; $i++) { 
                  $img = explode('.', $_FILES['files']['name'][$i]);
                  $extension = end($img);
                  $file_name =  md5(date('y-m-d h:i:s').$_FILES['files']['name'][$i]).".".$extension;
                  $uploadfile = $uploaddir.$file_name;
                 /* cek($msg[]);
                  die();*/
                  if (move_uploaded_file($_FILES['files']['tmp_name'][$i], $uploadfile)) {
                    $data_upload = array('files'=>$file_name);
                    $msgs[] = array('file' => base_url('uploads/form_file/'.$file_name));
                    $msg = array('status'=>1,'message'=>'Upload Foto Berhasil','data' =>$msgs);
                    
                  } else {
                    $msg[] = array('status'=>0,'message'=>'Gagal Upload Foto');
                  }
                }

                $this->response($msg);






              }else {
                  $msg = array('status' => 0, 'message'=>'Data tidak boleh kosong ');
                  $this->response($msg);
              }
          }else {
              $msg = array('status' => 1, 'message'=>'Token Tidak Ditemukan ');
              $this->response($msg);
          }
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

    }


}
?>
