<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Histori_transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");

          //$kode_transaksi = $this->post('kode_transaksi');
          if (isset($mem)) {
            $member_id = $this->get("member_id");
            $member = $this->mymodel->getbywhere('permintaan','member_id',$mem->member_id,"row");
            //$get_permintaan = $this->mymodel->getbywhere('permintaan','tracking_id',$kode_transaksi,'row');
            if (isset($member)) {
              
                $data = $this->mymodel->getbywheresort('permintaan',"member_id = $mem->member_id","",'created_at',"DESC");
                  $member_id = $this->get("member_id");
                foreach ($data as $key => $value) {
                
                  $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');
                  $get_status = $this->mymodel->getbywhere('status_permintaan','id',$value->kode_status,'row');

                  $value->member_id = $value->member_id;
                  $value->kategori_id = $value->kategori_id;
                  $value->name_in = $get_kategori->name_in;
                  $value->name_en = $get_kategori->name_en;
                  $value->name_man = $get_kategori->name_man;
                  $value->foto = base_url('uploads/file/'.$get_kategori->foto);
                  $value->icon = base_url('uploads/file/'.$get_kategori->icon);
                  $value->id = $get_status->id;
                  $value->name_sp_in = $get_status->name_sp_in;
                  $value->name_sp_en = $get_status->name_sp_en;
                  $value->name_sp_man = $get_status->name_sp_man;
                
              }
              if (!empty($data)) {
                if($id){
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data[0]);
                } else {
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
                }
              }else {
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
              }
            }else {
                $msg = array('status' => 0, 'message'=>'Member Id Tidak Ditemukan ');
            }
        }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

    public function get_province($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }*/
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      /*$curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: 37f68525a24987cb69e10d7b86aac5d5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = json_decode($response);
        foreach ($ubah as $key => $value) {
          $ubah = $value->results;
        }
        return $ubah;
      }
  */
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;
    }

}