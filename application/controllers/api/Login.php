<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require 'phpmailer/PHPMailerAutoload.php';
//ob_start();

class Login extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $email = $this->post('email');
      $password = $this->post('password');
      $msg ="";
      if ($email !="" && $password!="") {
          $cek = $this->mymodel->getbywhere('member',"email='$email' or phone='$email'",null,"row");
          $success = 0;
          $message = "";
          $stats = "";
          $data_diri = 0;

          if (!empty($cek)) {
            $data = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') and password=",md5($password),"row");
           
            if (!empty($data)) {
              $data_active  = $this->mymodel->getbywhere('member',"email='$email' AND is_active=","1","row");
              
              if (!empty($data_active)) {
                $data_diri = 1;
                $datax = $this->mymodel->getbywhere('member','email',$email,"row");
                
                  if(empty($datax->img_file)){
                     $img_file = NULL;
                  }else{
                    $img_file = base_url('/uploads/member/'.$datax->img_file);
                  }
                  $datax = array(
                    "member_id" => $datax->member_id,
                    "nama_lengkap" => $datax->nama_lengkap,
                    "phone" => $datax->phone,
                    "email" => $datax->email,
                    "img_file" => $img_file,
                    "password" => $datax->password,
                    "token" => $datax->token,
                    "fcm_id" => $datax->fcm_id,
                    "is_active" => $datax->is_active,
                    "last_login" => $datax->last_login,
                    "created_at" => $datax->created_at
                  ); 
                
                  $msg = array('status'=> 1,'message'=>$message,'stats'=>$stats,'data' => $datax  , 'data_diri' => $data_diri);
              }else{
                $message_data = array(
                      "ind" => 'Akun Sudah di Nonaktifkan',
                      "eng" => 'Account has been deactivated',
                      "man" => '帐户已停用'
                    ); 
                $msg = array('status' => 0, 'message'=>array($message_data));
              }
            }else{
                $message_data = array(
                      "ind" => 'Akun telah dihapus',
                      "eng" => 'Account has been deleted',
                      "man" => '帐号已被删除'
                    ); 
                $msg = array('status' => 0, 'message'=>array($message_data));
              }

          }else{
             $message_data = array(
                "ind" => 'Email tidak terdaftar',
                "eng" => 'Email is not registered',
                "man" => '郵箱未註冊'
              ); 
         
            $msg = array('status'=>0,'message'=>array($message_data),'data' => null, "data_diri" => $data_diri);
          }

        }else if($email==""){

             $message_data = array(
                "ind" => 'Email Harus diisi',
                "eng" => 'Email Required',
                "man" => '需要電子郵件'
              ); 
         
            $msg = array('status' => 0, 'message'=>$message_data);
        }else if($password==""){

             $message_data = array(
                "ind" => 'Password Harus diisi',
                "eng" => 'Password Required',
                "man" => '需要密碼'
              ); 
            $msg = array('status' => 0, 'message'=>$message_data);
        }
        $this->response($msg);
    }

}
