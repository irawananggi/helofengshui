<?php

require APPPATH . 'controllers/Midtrans/ApiRequestor.php';

class CoreApi extends CI_Controller
{
    public static function charge($params)
    {

        
        $payloads = array(
            'payment_type' => 'credit_card'
        );


        if (array_key_exists('item_details', $params)) {
            $gross_amount = 0;
            foreach ($params['item_details'] as $item) {
                $gross_amount += $item['quantity'] * $item['price'];
            }
            $payloads['transaction_details']['gross_amount'] = $gross_amount;
        }
        $payloads = array_replace_recursive($payloads, $params);

        
        
        if (Config::$isSanitized) {
            Sanitizer::jsonRequest($payloads);
        }
        
        $result = ApiRequestor::post(
            Config::getBaseUrl() . '/charge',
            Config::$serverKey,
            $payloads
        );
       
        /*die(json_encode($payloads));*/
        return $result;
    }

    /**
     * Capture pre-authorized transaction
     *
     * @param string $param Order ID or transaction ID, that you want to capture
     */
    public static function capture($param)
    {
        $payloads = array(
        'transaction_id' => $param,
        );

        $result = ApiRequestor::post(
            Config::getBaseUrl() . '/capture',
            Config::$serverKey,
            $payloads
        );

        return $result;
    }

    public static function token($credit_card){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => Config::getBaseUrl() . '/token?' . $credit_card,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode(Config::$serverKey . ':')
            )
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return  $response;
        //return response()->json($response);
    }

    

    public static function expire($order_id,$param)
    {
      /*  cek($param);
          die();
        
       */
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $order_id . '/expire',
            Config::$serverKey,
            $param
        );
    }

}
