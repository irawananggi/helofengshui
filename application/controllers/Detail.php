<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends CI_Controller {


	function __construct() {
	
		parent::__construct();
		
	}

	public function blog($permalink=NULL) {

		$data['vals'] = $this->general_model->get_param(array('bahasa'),2);
		$vals = $this->general_model->get_param(array('bahasa'),2);
		$from_blog = array(
			'blog tj' => ''
		);
		$select = 'tj.*';
		$where = array('tj.permalink'=>$permalink);
		$data['blog'] = $this->general_model->datagrab(array('tabel'=>$from_blog,'where'=>$where))->row();



		$from_link = array(
			'link tj' => '',
			'jenis_link b' => array('b.id=tj.jenis_link_id','left') 
		);
		$select = 'tj.*,b.*';
		$where1 = array('jenis_link_id'=>1);
		$where2 = array('jenis_link_id'=>2);
		$data['data_link_1'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where1));
		$data['data_link_2'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where2));





		$this->load->view('landing/detail_blog',$data);
	}
	public function syarat_ketentuan() {
		
		$data['vals'] = $this->general_model->get_param(array('bahasa'),2);
		$vals = $this->general_model->get_param(array('bahasa'),2);
		
		$from_link = array(
			'link tj' => '',
			'jenis_link b' => array('b.id=tj.jenis_link_id','left') 
		);
		$select = 'tj.*,b.*';
		$where1 = array('jenis_link_id'=>1);
		$where2 = array('jenis_link_id'=>2);
		$data['data_link_1'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where1));
		$data['data_link_2'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where2));


		$data['syarat'] = $this->general_model->datagrab(array('tabel'=>'halaman','where'=>array('id'=>3)))->row();



		$this->load->view('landing/detail_syarat',$data);
	}
	
}


