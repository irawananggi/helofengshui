<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class voucher extends CI_Controller {
  var $dir = 'fengshui/voucher';
  var $bulan = array(
      1 => 'Januari',
      2 => 'Februari',
      3 => 'Maret',
      4 => 'April',
      5 => 'Mei',
      6 => 'Juni',
      7 => 'Juli',
      8 => 'Agustus',
      9 => 'September',
      10 => 'Oktober',
      11 => 'November',
      12 => 'Desember');

  function __construct() {
    parent::__construct();
    if (not_login(uri_string()))redirect('login');
    date_default_timezone_set('Asia/Jakarta');
    
  }

  public function index() {
    $this->list_data();
  }

  public function list_data($offset = null,$search=null) {
    $id_operator = $this->session->userdata('id_pegawai');
    
    if(@$_POST['key']!=''){
      $key = $_POST['key'];
      $this->session->set_userdata('kunci',$key);
    }else{
      if($offset != '') $key = $this->session->userdata('kunci');
      else $this->session->unset_userdata('kunci'); $key = '';
    }
    $offset = !empty($offset) ? $offset : null;
    $fcari = null;
    $search_key = $this->input->post('key');
    if (!empty($search_key)) {
      $fcari = array(
        'judul'     => $search_key,
      );  
      $data['for_search'] = $fcari['judul'];
    } else if ($search) {
      $fcari = array(
        'judul'     => @un_de($search),
      );
      $data['for_search'] = $fcari['judul'];
    }
    $from = array(
      'voucher tj' => '',
    );
    $select = 'tj.*';
    $where = array();
    $config['base_url'] = site_url($this->dir.'/list_data/');
    $config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.id DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();

    $data['search'] =@$_POST['key'];
    $data['total']  = $config['total_rows'];
    $config['per_page']   = '';
    $config['uri_segment']  = '6';
    $this->pagination->initialize($config);
    $data['links'] = $this->pagination->create_links();
    $lim = ($offset == "cetak" or $offset == "excel") ? null : $config['per_page'];
    $offs = ($offset == "cetak" or $offset == "excel") ? null : $offset;
    $st = get_stationer();  
    $data['data'] = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select, 'search'=>$fcari,'where'=>$where,'limit'=>$lim, 'offset'=>$offs));
    
    $data['offs'] = $offs;

    $btn_tambah = anchor(site_url($this->dir.'/add_data'),'<i class="icon-icon-awesome-plus text-12"></i> Tambah Data','class="btn btn-warning radius-5 btn-sm" act="#"  title="Klik untuk Tambah Data"');
    $data['tombol'] = @$btn_tambah;
    $data['extra_tombol'] = 
        '<div class="col-search">
                  <div class="form-inline-group left-i">
                    <i class="icon-search in-left"></i>
                    <input type="text" id="searchField" class="form-control" placeholder="Cari">
                  </div>
                </div>'
                ;
    $title = 'Voucher';
    if ($offset == "cetak") {
      $data['title'] = '<h3>'.$title.'</h3>';
      $data['content'] = $tabel;
      $this->load->view('umum/print',$data);
    } else if ($offset == "excel") {
      $data['file_name'] = $title.'.xls';
      $data['title'] = '<h3>'.$data['title'].'</h3>';
      $data['content'] = $tabel;
      $this->load->view('umum/excel',$data);
    } else {
      $data['title']    = $title;
      $data['tabel'] = $tabel;
      
      
      $data['title']    = $title;
      $data['content'] = 'fengshui/voucher_view';
      $this->load->view('home', $data);
    }
  }

  
  function cek_name_voucher(){
    
    $dt = $this->general_model->datagrab(array(
        'select' => '*',
        'tabel' => array(
          'voucher' => ''
        ),
        'where'=>array(
          'name_voucher'=> $this->input->post('name_voucher')
          )
        )
      )->num_rows();

      $data_upload = $dt;
      die(json_encode($data_upload));
  }

  function add_data($id = null){

    if($id!=NULL){
      //$p = un_de($id);
      $data['dt'] = $this->general_model->datagrab(array(
        'select' => '*',
        'tabel' => array(
          'voucher' => ''
        ),
        'where'=>array(
          'id'=> $id
          )
        )
      )->row();
    }
    
    
    $data['top']  = (!empty($id) ? 'Ubah' : 'Tambah Data');
    $data['head'] = (!empty($id) ? 'Ubah' : 'Buat');
    $data['title']  = 'Voucher '.$data['top'];
    $data['tombol'] = (!empty($id) ? 'Update' : 'Simpan');
    $data['id'] = $id;
   


    $data['content'] = 'fengshui/voucher_add';
    $this->load->view('home', $data);
  }

    function simpan_data(){
      $id = $this->input->post('id');
      $name_voucher = $this->input->post('name_voucher');
      $min_s = $this->input->post('min_s');
      $nominal = $this->input->post('nominal');
      $dis_id = $this->input->post('dis_id');
      $start_date = $this->input->post('created_at1');
      $end_date = $this->input->post('created_at2');

      $code = rand(1000, 9999);

      $words = explode(" ",$name_voucher);
      $acronym = "";

      foreach ($words as $w) {
        $acronym .= $w[0];
      }


      /*$par = array(
          'tabel'=>'voucher',
          'data'=>array(
            'name_voucher'=>$name_voucher,
            'code_voucher'=>$acronym.''.$code,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'min_s'=>$min_s,
            'dis_id'=>$dis_id,
            'nominal'=>$nominal,
            'created_at'=>date('Y-m-d H:i:s'),
            ),
          );*/
     $cek_name_v = $this->general_model->datagrab(array('tabel'=>'voucher','where'=>array('id'=>$id)))->row();
     
     if($cek_name_v->name_voucher != $name_voucher){
      $par = array(
          'tabel'=>'voucher',
          'data'=>array(
            'name_voucher'=>$name_voucher,
            'code_voucher'=>$acronym.''.$code,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'min_s'=>$min_s,
            'dis_id'=>$dis_id,
            'nominal'=>$nominal,
            'created_at'=>date('Y-m-d H:i:s'),
            ),
          );
     if($id != NULL){
        $par['where'] = array('id'=>$id);
      }
    }else{
      $par = array(
          'tabel'=>'voucher',
          'data'=>array(
            'name_voucher'=>$name_voucher,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'min_s'=>$min_s,
            'dis_id'=>$dis_id,
            'nominal'=>$nominal,
            'created_at'=>date('Y-m-d H:i:s'),
            ),
          );
      if($id != NULL){
        $par['where'] = array('id'=>$id);
      }
    }

    $id_par = $this->general_model->save_data($par);
    

    $this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
     
        redirect($this->dir);

    }
  

  
  function edit_status($id=null) {
    
    $datax = $this->general_model->datagrab(array('tabel'=>'voucher','where'=>array('id'=>$id)))->row();
    if($datax->status == 1){
      $datas = array('status'  => '0');
      $this->general_model->save_data('voucher',$datas,'id',$id);
      $this->session->set_flashdata('ok','Data Berhasil di Non aktifkan');
    }else{
      $datas = array('status'  => '1');
      $this->general_model->save_data('voucher',$datas,'id',$id);
    $this->session->set_flashdata('ok','Data Berhasil diaktifkan');

    }

    redirect($this->dir.'/list_data');
  }
  function delete_data($id=null) {

    $del = $this->general_model->delete_data('voucher','id',$id);
    
    if ($del) {
      $this->session->set_flashdata('ok','Data voucher Berhasil di Hapus');
    }else{
      $this->session->set_flashdata('fail','Data voucher Gagal di Hapus');
    }
    redirect($this->dir);
  }
}
