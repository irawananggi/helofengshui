<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimoni extends CI_Controller {
	var $dir = 'fengshui/Testimoni';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	public function index() {
		$this->list_data();
	}

	public function list_data($offset = null,$search=null) {
		$id_operator = $this->session->userdata('id_pegawai');
		
		if(@$_POST['key']!=''){
			$key = $_POST['key'];
			$this->session->set_userdata('kunci',$key);
		}else{
			if($offset != '') $key = $this->session->userdata('kunci');
			else $this->session->unset_userdata('kunci'); $key = '';
		}
		$offset = !empty($offset) ? $offset : null;
		$fcari = null;
		$search_key = $this->input->post('key');
		if (!empty($search_key)) {
			$fcari = array(
				'nama' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['nama'];
		} else if ($search) {
			$fcari = array(
				'nama' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['nama'];
		}
		$from = array(
			'testimoni tj' => ''
		);
		$select = 'tj.*';
		$where = array();
		$config['base_url']	= site_url($this->dir.'/list_data/');
		$config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.id DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();

		$data['search']	=@$_POST['key'];
		$data['total']	= $config['total_rows'];
		$config['per_page']		= '';
		$config['uri_segment']	= '6';
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$lim = ($offset == "cetak" or $offset == "excel") ? null : $config['per_page'];
		$offs = ($offset == "cetak" or $offset == "excel") ? null : $offset;
		$st = get_stationer();	
		$data['data_article'] = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select, 'search'=>$fcari,'where'=>$where,'limit'=>$lim, 'offset'=>$offs));
		
		$data['offs'] = $offs;
		$data['extra_tombol'] = 
				form_open($this->dir.'/list_data/','id="form_search" role="form"').
				'<div class="form-inline-group left-i border-icon">
                     <i class="icon-search in-left"> <button class="btn btn-default btn-flat in-left" style="margin-left: -34px;"></button></i>
                      <input name="key" type="text" placeholder="Pencarian ..." id="searchField"  class="form-control pull-right" value="'.@$search_key.'">
                     
				</div>'.
				form_close();
		$data['tombol'] = '';
		$title = 'Testimoni';
		if ($offset == "cetak") {
			$data['title'] = '<h3>'.$title.'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/print',$data);
		} else if ($offset == "excel") {
			$data['file_name'] = $title.'.xls';
			$data['title'] = '<h3>'.$data['title'].'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/excel',$data);
		} else {
			$data['title'] 		= $title;
			$data['tabel'] = $tabel;
			
			
			$data['title'] 		= $title;
			$data['content'] = 'fengshui/testimoni_view';
			$this->load->view('home', $data);
		}
	}

	function edit_status($id=null) {
		
		$datax = $this->general_model->datagrab(array('tabel'=>'testimoni','where'=>array('id'=>$id)))->row();
		if($datax->status == 1){
			$datas = array('status'	=> '0');
			$this->general_model->save_data('testimoni',$datas,'id',$id);
			$this->session->set_flashdata('ok','Data Berhasil di Non aktifkan');
		}else{
			$datas = array('status'	=> '1');
			$this->general_model->save_data('testimoni',$datas,'id',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');

		}

		redirect($this->dir.'/list_data');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '2');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('fail','Berita Berhasil dinonaktifkan');
		redirect($this->dir.'/list_data');
	}
	function get_status_article($sec, $status){
		/*$query = $this->universal_model->data_list('kategori',null,null,array('code' => $sec, 'status' => $status));*/
		$query = $this->general_model->datagrab(array('tabel'=>'kategori','where'=>array('code'=>$sec,'status'=>$status)));
		
		return $query->num_rows();
	}
	
	function get_permalink($code = NULL, $data = NULL){
		die(json_encode(array('hasil' => permalink(@$code, @$data))));
	}
	


	function add_data($id = null){
		if($id!=NULL){
			//$p = un_de($id);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'kategori' => ''
				),
				'where'=>array(
					'id'=> $id
					)
				)
			)->row();
		}
		$data['title']	= (!empty($id) ? 'Ubah' : 'Tambah').' Data';
		$data['id']	= $id;
		
			$data['content'] = 'fengshui/kategori_add';
			$this->load->view('home', $data);
	}


    function simpan_data(){
    	$id = $this->input->post('id');
    	$title = $this->input->post('title');
    	$id_unit = $this->input->post('id_unit');
    	$id_pegawai = $this->session->userdata('id_pegawai');
    	/*$id_unit = $this->session->userdata('id_unit');
    	
    	cek($id_pegawai);
    	cek($id_unit);
    	die();*/
    	$par = array(
					'tabel'=>'kategori',
					'data'=>array(
						'title'=>$title,
						'id_operator'=>$id_pegawai,
						'id_unit'=>$id_unit,
						'status'=>1,
						),
					);

				if($id != NULL)	$par['where'] = array('id'=>$id);

				$sim = $this->general_model->save_data($par);
				$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
     
            redirect($this->dir);

    }


	function delete_article($id=null) {
		/*cek($sec);
		cek($id);
		die();*/
		$data = array(
				'status'	=> '2'
			);

		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Data Publikasi Berhasil dibuang');
		redirect($this->dir.'/list_data/'.$status);
		//$del = $this->general_model->delete_data('kategori','id',$id);
		/*if ($del) {
			$this->session->set_flashdata('ok','Data articles Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data articles Gagal di Hapus');
		}*/
	}
	
	function restore_data($sec,$id=null,$status) {
		/*cek($status);
		die();*/
		$data = array('status'	=> '3');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Data Publikasi Berhasil di Kembalikan');
		redirect($this->dir.'/list_data/'.$status);
	}

	function delete_data($id=null) {
		$del = $this->general_model->delete_data('kategori','id',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Publikasi Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Publikasi Gagal di Hapus');
		}
		redirect($this->dir);
	}

	function truncate($str, $len) {
	  $tail 	= max(0, $len-10);
	  $trunk 	= substr($str, 0, $tail);
	  $trunk 	.= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len-$tail))));
	  return $trunk;
	}  
	function pdf2($search=null) {
		
		$this->load->library('pdf');

		$from = array(
			'testimoni tj' => ''
		);
		$data['data_article'] = $this->general_model->datagrab(array('tabel'=>$from, 'search'=>$fcari, 'order'=>'id DESC'));
		$data['title'] = 'Data Testimoni';
        $html = $this->load->view('fengshui/testimoni_pdf_view', $data, true);
        $this->pdf->createPDF($html, 'mypdf', false);
		
	}

	function pdf($search=null) {
		$fcari = null;
		$search_key = $search;
		if (!empty($search_key)) {
			$fcari = array(
				'nama' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['nama'];
		} else if ($search) {
			$fcari = array(
				'nama' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['nama'];
		}
		
		$fcari = str_replace("%20", " ", $fcari);
		
		/*cek($this->load->library('pdf'));
		die();*/
		$this->load->library('pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->AddPage('');
        //$pdf->Write(0, 'Data Kategori', '', 0, 'L', true, 0, false, false, 0);
        $pdf->SetFont('');
 		$from = array(
			'testimoni tj' => ''
		);
		$data['data'] = $this->general_model->datagrab(array('tabel'=>$from, 'search'=>$fcari, 'order'=>'id DESC'));
		$data['title'] = 'Data Blog';
		$tabel = '
    <style type="text/css">
        table{
        	font-size:10px;
        	width:100%;
            padding: 5px;
            border-spacing:0;border-collapse:collapse
            }
            tr,td,th{
                padding:10px
            }
            th{
                text-align: center;padding:10px
            }
        .table{
            border-collapse:collapse!important
            }
            
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important;
                    padding:50px;
                }

    </style>';
          if ($data['data']->num_rows() > 0) { 
            $tabel .= '
            <h4 class="">Data Blog</h4>
            <table class="table table-striped table-bordered"  style="width: 100%;">
              <thead>
               <tr>
                <td scope="col" class="text-2 medium" style="width:30px">No</td>
                <td scope="col" class="text-2 medium">Nama</td>
                <td scope="col" class="text-2 medium" style="width:200px">Isi</td>
                <td scope="col" class="text-2 medium">Tanggal</td>
                <td scope="col" class="text-2 medium text-center">Non Aktifkan</td>
              </tr>
              </thead>
              <tbody>';
              $no=1;
              foreach ($data['data']->result() as $row) { 
              	
              $tabel .= '
               <tr>
                  <td class="align-middle" style="width: 30px;">'.$no.'</td>
                  <td class="align-middle">
                    '.$row->nama.'
                  </td>
                  <td class="align-middle" style="width:200px">
                    '.$row->isi.'
                  </td>
                  <td class="align-middle">
                    '.tanggal_indo(date('Y-m-d', strtotime($row->created_at))).'
                  </td>
                  <td class="align-middle text-center">';
                 
                  if($row->status == 1){
                    $checked = "Aktif";
                  }else{
                    $checked = "Non Aktif";
                  }

                     $tabel .= $checked.'
                  </td>
                </tr>
                ';
              $no++;
            }
         
            
              
            $tabel .= ' </tbody>
          </table>';
          
                 }else{
             $tabel .= '<div class="alert">Data masih kosong ...</div>';
          } 

        $html = $tabel;

        $pdf->writeHTML($html);
        ob_end_clean();
        $pdf->Output('file-pdf-codeigniter.pdf', 'I');
		
	}
}
