<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {
	var $dir = 'fengshui/Kategori';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	public function index() {
		$this->list_data();
	}

	public function list_data($offset = null,$search=null) {
		$id_operator = $this->session->userdata('id_pegawai');
		
		if(@$_POST['key']!=''){
			$key = $_POST['key'];
			$this->session->set_userdata('kunci',$key);
		}else{
			if($offset != '') $key = $this->session->userdata('kunci');
			else $this->session->unset_userdata('kunci'); $key = '';
		}
		$offset = !empty($offset) ? $offset : null;
		$fcari = null;
		$search_key = $this->input->post('key');
		if (!empty($search_key)) {
			$fcari = array(
				'name_in' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['name_in'];
		} else if ($search) {
			$fcari = array(
				'name_in' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['name_in'];
		}

		$from = array(
			'kategori tj' => ''
		);
		$select = 'tj.*';
			
		$where = array();
		
		$config['base_url']	= site_url($this->dir.'/list_data/');
		$config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.id DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();

		$data['search']	=@$_POST['key'];
		$data['total']	= $config['total_rows'];
		$config['per_page']		= '';
		$config['uri_segment']	= '6';
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$lim = ($offset == "cetak" or $offset == "excel") ? null : $config['per_page'];
		$offs = ($offset == "cetak" or $offset == "excel") ? null : $offset;
		

		$data['data_article'] = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select, 'search'=>$fcari,'where'=>$where,'limit'=>$lim, 'offset'=>$offs));
		$btn_tambah = anchor(site_url($this->dir.'/add_data'),'<i class="icon-icon-awesome-plus text-12"></i> Tambah Data','class="btn btn-warning radius-5 btn-sm" act="#"	title="Klik untuk tambah data"');
		$btn_cetak =
				'<a href="#" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>';
		$data['tombol'] = @$btn_tambah;
		$data['extra_tombol'] = 
				form_open($this->dir.'/list_data/','id="form_search" role="form"').
				'<div class="form-inline-group left-i border-icon">
                     <i class="icon-search in-left"> <button class="btn btn-default btn-flat in-left" style="margin-left: -34px;"></button></i>
                      <input name="key" type="text" placeholder="Pencarian ..." id="searchField"  class="form-control pull-right" value="'.@$search_key.'">
                     
				</div>'.
				form_close();
		
			$data['title'] = 'Kategori';
			$data['content'] = 'fengshui/kategori_view';
			$this->load->view('home', $data);
		
	}
/*
	function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Berita Berhasil diaktifkan');
		redirect($this->dir.'/list_data');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '2');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('fail','Berita Berhasil dinonaktifkan');
		redirect($this->dir.'/list_data');
	}
	*/
	


	function add_data($id = null){
		if($id!=NULL){
			//$p = un_de($id);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'kategori' => ''
				),
				'where'=>array(
					'id'=> $id
					)
				)
			)->row();

		/*	$data['dt_kategori'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'form_kategori' => ''
				),
				'where'=>array(
					'kategori_id'=> $id
					)
				)
			)->row();*/

			$data['dt_form_kategori'] = $this->general_model->datagrab(array(
				'tabel'	=> array(
					'form_kategori' => ''
				),
				'where'=>array(
					'kategori_id'=> $id
					)
				)
			);


		}
		
		$data['title']	= 'Kategori';
		$data['head']	= (!empty($id) ? 'Ubah' : 'Tambah').' Data';
		$data['tombol']	= (!empty($id) ? 'Update' : 'Simpan');
		$data['id']	= $id;
		
			$data['content'] = 'fengshui/kategori_add';
			$this->load->view('home', $data);
	}


    function simpan_data(){
		
    	$id = $this->input->post('id_k');
		
    	$name_in = $this->input->post('name_in');
    	$deskripsi_in = $this->input->post('deskripsi_in');
    	$ketentuan_in = $this->input->post('ketentuan_in');
    	$name_en = $this->input->post('name_en');
    	$deskripsi_en = $this->input->post('deskripsi_en');
    	$ketentuan_en = $this->input->post('ketentuan_en');
    	$name_man = $this->input->post('name_man');
    	$deskripsi_man = $this->input->post('deskripsi_man');
    	$ketentuan_man = $this->input->post('ketentuan_man');
    	$harga = $this->input->post('harga');
    	$id_produk = $this->input->post('id_produk');
    	//$foto = $this->input->post('foto');
    	//$icon = $this->input->post('icon');
    	$hub_cs = (($this->input->post('hub_cs') == NULL)?'off':$this->input->post('hub_cs'));
    	$kategori_kematian = (($this->input->post('kategori_kematian') == NULL)?'off':$this->input->post('kategori_kematian'));
    	$created_at =date('Y-m-d H:i:s');


    	$par = array(
			'tabel'=>'kategori',
			'data'=>array(
				'id_produk'=>$id_produk,
				'name_in'=>$name_in,
				'deskripsi_in'=>$deskripsi_in,
				'ketentuan_in'=>$ketentuan_in,
				'name_en'=>$name_en,
				'deskripsi_en'=>$deskripsi_en,
				'ketentuan_en'=>$ketentuan_en,
				'name_man'=>$name_man,
				'deskripsi_man'=>$deskripsi_man,
				'ketentuan_man'=>$ketentuan_man,
				'harga'=>$harga,
				'hub_cs'=>$hub_cs,
				'kategori_kematian'=>$kategori_kematian,
				'created_at'=>$created_at,
			),
		);

			if($id != NULL)	$par['where'] = array('id'=>$id);
	
	    	$this->load->library('Upload');

			$fi= $_FILES;
			
			$gambar = $fi['foto']['name'];
			$icon = $fi['icon']['name'];
			if(!empty($gambar)) {
				if($id != NULL){
					$cek_gambar = $this->general_model->datagrabs(array('tabel'=>'kategori','where'=>array('id'=>@$id),'select'=>'foto'))->row();
					$path = './uploads/file';
					if (!is_dir($path)) mkdir($path,0777,TRUE);
					$path_pasfoto = $path.'/'.$cek_gambar->foto;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}

		    	$config = array(
					'allowed_types' => 'png|jpg|gif|jpeg',
					'upload_path' => 'uploads/file',
					'overwrite' => TRUE,
					'file_name' =>'foto_'.$fi['foto']['size'].date("Y-m-d"),
				);
				$this->upload->initialize($config);
				$this->upload->do_upload('foto');
				$data_upload = $this->upload->data();
				$foto2 = $data_upload['file_name'];
				$par['data']['foto'] = $foto2;
			}

			if(!empty($icon)) {
				if($id != NULL){
					$cek_gambar = $this->general_model->datagrabs(array('tabel'=>'kategori','where'=>array('id'=>@$id),'select'=>'icon'))->row();
					$path = './uploads/file';
					if (!is_dir($path)) mkdir($path,0777,TRUE);
					$path_pasfoto = $path.'/'.$cek_gambar->icon;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}

		    	$config2 = array(
					'allowed_types' => 'png|jpg|gif|jpeg',
					'upload_path' => 'uploads/file',
					'overwrite' => TRUE,
					'file_name' =>'icon_'.$fi['icon']['size'].date("Y-m-d"),
				);
				$this->upload->initialize($config2);
				$this->upload->do_upload('icon');
				$data_upload = $this->upload->data();
				$icon2 = $data_upload['file_name'];
				$par['data']['icon'] = $icon2;
				
			}


			$sim = $this->general_model->save_data($par);
			$jmlfile = $this->input->post('jmlfile');
			$id_frm = $this->input->post('id_frm');


			$jenis_form_ids = $this->input->post('jenis_form_ids');
			foreach ($jenis_form_ids as $key => $value) {
				$pars1 = array(
					'tabel'=>'form_kategori',
					'data'=>array(
						'jenis_form_id' => $value
						),
					);
				$pars1['where'] = array('id'=>$key);

				$simx = $this->general_model->save_data($pars1);
			}
			
			$name_cat_ins = $this->input->post('name_cat_ins');
			foreach ($name_cat_ins as $key => $value) {
				$pars1 = array(
					'tabel'=>'form_kategori',
					'data'=>array(
						'name_cat_in' => $value
						),
					);
				$pars1['where'] = array('id'=>$key);

				$simx = $this->general_model->save_data($pars1);
			}
			$name_cat_ens = $this->input->post('name_cat_ens');
			foreach ($name_cat_ens as $key => $value) {
				$pars2 = array(
					'tabel'=>'form_kategori',
					'data'=>array(
						'name_cat_en' => $value
						),
					);
				$pars2['where'] = array('id'=>$key);

				$simx = $this->general_model->save_data($pars2);
			}
			$name_cat_mans = $this->input->post('name_cat_mans');
			foreach ($name_cat_mans as $key => $value) {
				$pars3 = array(
					'tabel'=>'form_kategori',
					'data'=>array(
						'name_cat_man' => $value
						),
					);
				$pars3['where'] = array('id'=>$key);

				$simx = $this->general_model->save_data($pars3);
			}
			
			$count_tot__file = 0;
			for($u=0; $u < $jmlfile; $u++){
					if($id != NULL){
						$data[$u]['kategori_id'] = $id;
					}else{
						$data[$u]['kategori_id'] = $sim;
					}
					$data[$u]['jenis_form_id'] = $this->input->post('jenis_form_id'.$u);
					$data[$u]['name_cat_in'] = $this->input->post('name_cat_in'.$u);
					$data[$u]['name_cat_en'] = $this->input->post('name_cat_en'.$u);
					$data[$u]['name_cat_man'] = $this->input->post('name_cat_man'.$u);
					
					
						$this->general_model->save_data('form_kategori',$data[$u]);

					
					

			}

		$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
        redirect($this->dir);

    }


	function delete_data($id=null) {
		$del = $this->general_model->delete_data('kategori','id',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Publikasi Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Publikasi Gagal di Hapus');
		}
		redirect($this->dir);
	}


	function hapus_element($kategori_id=null,$id=null) {
		/*cek($kategori_id);
		cek($id);
		die();*/
		$del = $this->general_model->delete_data('form_kategori','id',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Form Input Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Form Input Gagal di Hapus');
		}
		redirect($this->dir.'/add_data/'.$kategori_id);
	}
	function pdf($search=null) {
		
		$fcari = null;
		$search_key = $search;
		if (!empty($search_key)) {
			$fcari = array(
				'name_in' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['name_in'];
		} else if ($search) {
			$fcari = array(
				'name_in' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['name_in'];
		}
		
		$fcari = str_replace("%20", " ", $fcari);
		$this->load->library('pdf1');
        $parameters= array(
                'mode' => 'utf-8',
                'format' => 'A4',    // A4 for portrait
                'default_font_size' => '12',
                'default_font' => 'droidsansfallback',
                'margin_left' => 20,
                'margin_right' => 15,
                'margin_top' => 10,
                'margin_bottom' => 30,
                'margin_header' => 20,
                'margin_footer' => 10,
                'orientation' => 'P' // For some reason setting orientation to "L" alone doesn't work (it should), you need to also set format to "A4-L" for landscape
            );
        $pdf1->useAdobeCJK = true;

        $pdf1 = $this->pdf1->load($parameters);
		$pdf1->useAdobeCJK = true;
		$pdf1->autoLangToFont = true;
		$pdf1->autoScriptToLang = true;
        $pdf1->SetDisplayMode('fullpage');
 		$from = array(
			'kategori tj' => ''
		);
		$data['data'] = $this->general_model->datagrab(array('tabel'=>$from, 'search'=>$fcari, 'order'=>'id DESC'));
		$data['title'] = 'Data Kategori';
		$tabel = '
    <style type="text/css">
        table{
        	font-size:10px;
        	width:100%;
                padding:10px;
            border-spacing:0;border-collapse:collapse
            }
            tr,td,th{
                padding:10px
            }
            th{
                text-align: center;padding:10px
            }
        .table{
            border-collapse:collapse!important
            }
            
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important;
                    padding:10px;
                }

    </style>';
          if ($data['data']->num_rows() > 0) { 
            $tabel .= '
            <h4 class="">Data Kategori</h4>
            <table class="table table-striped table-bordered"  style="width: 100%;">
              <thead>
                <tr>
                  <td scope="col" style="width: 40px;">No</td>
                  <td scope="col" style="width: 150px;">Nama Kategori</td>
                  <td scope="col">Deskripsi </td>
                  <td scope="col">Ketentuan</td>
                  <td scope="col">Harga</td>
                </tr>
              </thead>
              <tbody>';
              $no=1;
              foreach ($data['data']->result() as $row) { 
              $tabel .= '<tr>
                <td  style="width: 40px;">'.$no.'</td>
                <td>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">ID :</span> '.$row->name_in.'
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">EN :</span> '.$row->name_en.'
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">MD :</span> '.$row->name_man.'
                  </div>
                </td>
                <td>
                  <div class="content-table">
                   '.$row->deskripsi_in.'
                  </div>
                  <div class="content-table">
                   '.$row->deskripsi_en.'
                  </div>
                  <div class="content-table">
                    '.$row->deskripsi_man.'
                  </div>
                </td>
                <td>
                  <div class="content-table">
                   '.$row->ketentuan_in.'
                  </div>
                  <div class="content-table">
                   '.$row->ketentuan_en.'
                  </div>
                  <div class="content-table">
                    '.$row->ketentuan_man.'
                  </div>
                </td>
                <td>
                   '.rupiah($row->harga) .'
                </td>
                
              </tr>';
              $no++;
            }
         
            
              
            $tabel .= ' </tbody>
          </table>';
          
                 }else{
             $tabel .= '<div class="alert">Data masih kosong ...</div>';
          } 

        $html = $tabel;

        $pdf1->WriteHTML($html);
        ob_end_clean();
        $pdf1->Output('Data Kategori.pdf', 'I');
		
	}

}
