<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	var $dir = 'fengshui/Settings';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	

	function index(){

			//$p = un_de($id_pegawai);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'parameter' => ''
				),
				'where'=>array(
					'param'=> 'notelp'
					)
				)
			)->row();
			

			$data['title']	= 'Ubah Nomor Telepon';
			$data['head']	= 'Ubah Data';
			$data['tombol']	= 'Update';
		
			$data['content'] = 'fengshui/settings_view';
			$this->load->view('home', $data);
	}

    function simpan_data(){
    	$notelp2 = $this->input->post('no_telpon');
    	if(substr($notelp2,0, 1) == 0){
			$no_telpon = substr($notelp2, 1);
		}else{
			$no_telpon = $notelp2;
		}
    	$par = array(
					'tabel'=>'parameter',
					'data'=>array(
						'val'=>$no_telpon,
						),
					);
		$par['where'] = array('param'=>'notelp');
		$sim = $this->general_model->save_data($par);
     
		$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
        redirect($this->dir);

    }


}
