<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reset_password extends CI_Controller {
	var $dir = 'fengshui/Reset_password';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	

	function index($id_pegawai = null){
		if($id_pegawai!=NULL){
			//$p = un_de($id_pegawai);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'peg_pegawai' => ''
				),
				'where'=>array(
					'id_pegawai'=> $id_pegawai
					)
				)
			)->row();
		}
		$data['title']	= ' Reset Password';
		$data['head']	= (!empty($id_pegawai) ? 'Ubah' : 'Tambah').' Data';
		$data['tombol']	= (!empty($id_pegawai) ? 'Update' : 'Simpan');
		$data['id_pegawai']	= $id_pegawai;
		
			$data['content'] = 'fengshui/reset_password';
			$this->load->view('home', $data);
	}

    function simpan_data(){
    	$id_pegawai = $this->input->post('id_pegawai');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');
    	
    	$cek_pass =  $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'peg_pegawai' => ''
				),
				'where'=>array(
					'password'=> $password
					)
				)
			);
    	if($cek_pass->num_rows() > 0){

    	$par = array(
					'tabel'=>'peg_pegawai',
					'data'=>array(
						'username'=>$username,
						),
					);
    	}else{

    	$par = array(
					'tabel'=>'peg_pegawai',
					'data'=>array(
						'username'=>$username,
						'password'=>md5($password),
						),
					);
    	}

		if($id_pegawai != NULL)	$par['where'] = array('id_pegawai'=>$id_pegawai);
		
		$sim = $this->general_model->save_data($par);
		$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
     
        redirect($this->dir.'/index/1');

    }


}
