<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    var $bln=array(1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

    function __construct() {
    
        parent::__construct();
        $this->load->helper('cmd');
        if(not_login(uri_string()))redirect('login');
        $this->load->config('cmd');
        
    }

    public function index() {
        $this->list_data();
    }

    public function list_data() {
        $data['title'] = 'Dashboard';
        $data['content'] = 'fengshui/dashboard_view';
        $this->load->view('home', $data);
    }
}
