<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permintaan extends CI_Controller {
	var $dir = 'fengshui/Permintaan';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	public function index() {
		$this->list_data();
	}

	public function list_data($offset = null,$search=null) {
		$id_operator = $this->session->userdata('id_pegawai');
		
		if(@$_POST['key']!=''){
			$key = $_POST['key'];
			$this->session->set_userdata('kunci',$key);
		}else{
			if($offset != '') $key = $this->session->userdata('kunci');
			else $this->session->unset_userdata('kunci'); $key = '';
		}
		$offset = !empty($offset) ? $offset : null;
		$fcari = null;
		$search_key = $this->input->post('key');
		if (!empty($search_key)) {
			$fcari = array(
				'nama_lengkap' 		=> $search_key,
				'name_in' 			=> $search_key,
				'status_permintaan'	=> $search_key,
			);	
			$data['for_search'] = $fcari['nama_lengkap'];
			$data['for_search'] = $fcari['name_in'];
			$data['for_search'] = $fcari['status_permintaan'];
		} else if ($search) {
			$fcari = array(
				'nama_lengkap' 		=> @un_de($search),
				'name_in' 		=> @un_de($search),
				'status_permintaan' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['nama_lengkap'];
			$data['for_search'] = $fcari['status_permintaan'];
		}
		$from = array(
			'permintaan tj' => '',
			'member b' => array('b.member_id=tj.member_id','left'),
			'kategori c' => array('c.id=tj.kategori_id','left'),
		);
		$select = 'tj.*,tj.id as id, b.*,c.id as kategori_id';
		$where = array();
		$config['base_url']	= site_url($this->dir.'/list_data/');
		$config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.tgl_bayar DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();

		$data['search']	= @$_POST['key'];
		$data['total']	= $config['total_rows'];
		$config['per_page']		= '';
		$config['uri_segment']	= '6';
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$lim = ($offset == "cetak" or $offset == "excel") ? null : $config['per_page'];
		$offs = ($offset == "cetak" or $offset == "excel") ? null : $offset;
		$st = get_stationer();	
		$data['data_article'] = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.tgl_bayar DESC','select'=>$select, 'search'=>$fcari,'where'=>$where,'limit'=>$lim, 'offset'=>$offs));
		
		$data['offs'] = $offs;
		$data['extra_tombol'] = 
				form_open($this->dir.'/list_data/','id="form_search" role="form"').
				'<div class="form-inline-group left-i border-icon">
                     <i class="icon-search in-left"> <button class="btn btn-default btn-flat in-left" style="margin-left: -34px;"></button></i>
                      <input name="key" type="text" placeholder="Pencarian ..." id="searchField"  class="form-control pull-right" value="'.@$search_key.'">
                     
				</div>'.
				form_close();
		$data['tombol'] = '';
		$title = 'Permintaan';
		if ($offset == "cetak") {
			$data['title'] = '<h3>'.$title.'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/print',$data);
		} else if ($offset == "excel") {
			$data['file_name'] = $title.'.xls';
			$data['title'] = '<h3>'.$data['title'].'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/excel',$data);
		} else {
			$data['title'] 		= @$title;
			$data['tabel'] = @$tabel;
			
			
			$data['title'] 		= $title;
			$data['content'] = 'fengshui/permintaan_view';
			$this->load->view('home', $data);
		}
	}

	function permintaan_detail_form($id=null) {
			$data['permintaan_detail'] = $this->general_model->datagrab(array('tabel'=>'permintaan_detail','order'=>'id DESC','where'=>array('permintaan_id'=>$id)));
			/*cek($data['permintaan_detail']->num_rows());
                die();*/
			$data['content'] = 'fengshui/permintaan_detail_form';
			$this->load->view('home', $data);
	}

	function permintaan_detail_view($id=null) {
			$data_kategori = $this->general_model->datagrab(array('tabel'=>'permintaan','where'=>array('id'=>$id)))->row();
			$data['kategori_detail'] = $this->general_model->datagrab(array('tabel'=>'kategori','where'=>array('id'=>$data_kategori->kategori_id)))->row();
			$data['data_jawaban'] = $data_kategori;
			
			/*cek($data['kategori_detail']);
			die();*/
			$data['content'] = 'fengshui/permintaan_detail_view';
			$this->load->view('home', $data);
	}

	function permintaan_ubah_status($id=null) {
			/*cek($id);
			die();*/
			$data['id'] = $id;
			$data['data'] = $this->general_model->datagrab(array('tabel'=>'permintaan','where'=>array('id'=>$id)))->row();
			$data['kode_status'] = $data['data']->kode_status;
			$data['content'] = 'fengshui/permintaan_ubah_status_view';
			$this->load->view('home', $data);
	}

	function lihat_jawaban($id=null) {
			/*cek($id);
			die();*/
			$data['id'] = $id;
			$data['data'] = $this->general_model->datagrab(array('tabel'=>'permintaan','where'=>array('id'=>$id)))->row();
			$data['content'] = 'fengshui/lihat_jawaban';
			$this->load->view('home', $data);
	}

	function save_ubah_status() {
    	$id = $this->input->post('id');
    	$kode_status = $this->input->post('kode_status');
    	if($kode_status == 1){
    		$status_permintaan = 'Menunggu pembayaran';
    	}elseif($kode_status == 2){
    		$status_permintaan = 'Sedang diproses';
    	}elseif($kode_status == 3){
    		$status_permintaan = 'dibatalkan';    		
    	}else{
    		$status_permintaan = 'Selesai';    		
    	}

		$par = array(
				'tabel'=>'permintaan',
				'data'=>array(
					'kode_status'=>$kode_status,
					'status_permintaan'=>$status_permintaan
					),
				);

			if($id != NULL)	$par['where'] = array('id'=>$id);
			$simx = $this->general_model->save_data($par);
		redirect($this->dir);
	}


	function permintaan_berikan_jawaban($id=null) {
			
			$data['id'] = $id;

			$data['data_jawaban'] = $this->general_model->datagrab(array('tabel'=>'permintaan','where'=>array('id'=>$id)))->row();
			/*cek($data['data_jawaban']->jawaban_in);
			die();*/
			$data['content'] = 'fengshui/permintaan_berikan_jawaban_view';
			$this->load->view('home', $data);
	}

	function save_permintaan_berikan_jawaban($id=null) {
    	$id = $this->input->post('id');
    	$jawaban_in = $this->input->post('editor_in');
    	$jawaban_en = $this->input->post('editor_en');
    	$jawaban_man = $this->input->post('editor_man');
    	/*cek($jawaban_in);
    	cek($jawaban_en);
    	cek($jawaban_man);
    	cek($id);
    	die();*/

			
			if($jawaban_in != NULL OR $jawaban_en != NULL  OR $jawaban_man != NULL ){
				$par = array(
					'tabel'=>'permintaan',
					'data'=>array(
						'jawaban_in'=>$jawaban_in,
						'jawaban_en'=>$jawaban_en,
						'jawaban_man'=>$jawaban_man,
						'kode_status'=>'5',
						'status_permintaan'=>'Selesai'
						),
					);

				if($id != NULL)	$par['where'] = array('id'=>$id);
				$simx = $this->general_model->save_data($par);
			}else{
				$par = array(
					'tabel'=>'permintaan',
					'data'=>array(
						'jawaban_in'=>$jawaban_in,
						'jawaban_en'=>$jawaban_en,
						'jawaban_man'=>$jawaban_man,
						'kode_status'=>'4',
						'status_permintaan'=>'Menunggu Jawaban'
						),
					);

				if($id != NULL)	$par['where'] = array('id'=>$id);
				$simx = $this->general_model->save_data($par);
			}
		redirect($this->dir);
	}

	function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Berita Berhasil diaktifkan');
		redirect($this->dir.'/list_data');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '2');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('fail','Berita Berhasil dinonaktifkan');
		redirect($this->dir.'/list_data');
	}
	function get_status_article($sec, $status){
		/*$query = $this->universal_model->data_list('kategori',null,null,array('code' => $sec, 'status' => $status));*/
		$query = $this->general_model->datagrab(array('tabel'=>'kategori','where'=>array('code'=>$sec,'status'=>$status)));
		
		return $query->num_rows();
	}
	
	function get_permalink($code = NULL, $data = NULL){
		die(json_encode(array('hasil' => permalink(@$code, @$data))));
	}
	


	function add_data($id = null){
		if($id!=NULL){
			//$p = un_de($id);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'kategori' => ''
				),
				'where'=>array(
					'id'=> $id
					)
				)
			)->row();
		}
		$data['title']	= (!empty($id) ? 'Ubah' : 'Tambah').' Data';
		$data['id']	= $id;
		
			$data['content'] = 'fengshui/kategori_add';
			$this->load->view('home', $data);
	}


    function simpan_data(){
    	$id = $this->input->post('id');
    	$title = $this->input->post('title');
    	$id_unit = $this->input->post('id_unit');
    	$id_pegawai = $this->session->userdata('id_pegawai');
    	/*$id_unit = $this->session->userdata('id_unit');
    	
    	cek($id_pegawai);
    	cek($id_unit);
    	die();*/
    	$par = array(
					'tabel'=>'kategori',
					'data'=>array(
						'title'=>$title,
						'id_operator'=>$id_pegawai,
						'id_unit'=>$id_unit,
						'status'=>1,
						),
					);

				if($id != NULL)	$par['where'] = array('id'=>$id);

				$sim = $this->general_model->save_data($par);
				$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
     
            redirect($this->dir);

    }


	function delete_article($id=null) {
		/*cek($sec);
		cek($id);
		die();*/
		$data = array(
				'status'	=> '2'
			);

		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Data Publikasi Berhasil dibuang');
		redirect($this->dir.'/list_data/'.$status);
		//$del = $this->general_model->delete_data('kategori','id',$id);
		/*if ($del) {
			$this->session->set_flashdata('ok','Data articles Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data articles Gagal di Hapus');
		}*/
	}
	
	function restore_data($sec,$id=null,$status) {
		/*cek($status);
		die();*/
		$data = array('status'	=> '3');
		$this->general_model->save_data('kategori',$data,'id',$id);
		$this->session->set_flashdata('ok','Data Publikasi Berhasil di Kembalikan');
		redirect($this->dir.'/list_data/'.$status);
	}

	function delete_data($id=null) {
		$del = $this->general_model->delete_data('kategori','id',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Publikasi Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Publikasi Gagal di Hapus');
		}
		redirect($this->dir);
	}

	function truncate($str, $len) {
	  $tail 	= max(0, $len-10);
	  $trunk 	= substr($str, 0, $tail);
	  $trunk 	.= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len-$tail))));
	  return $trunk;
	}  

	function pdf($search=null) {
		$fcari = null;
		$search_key = $search;
		if (!empty($search_key)) {
			$fcari = array(
				'nama_lengkap' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['nama_lengkap'];
		} else if ($search) {
			$fcari = array(
				'nama_lengkap' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['nama_lengkap'];
		}
		$fcari = str_replace("%20", " ", $fcari);
		/*cek($fcari);
		die();*/
		$this->load->library('pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->AddPage('');
        //$pdf->Write(0, 'Data Kategori', '', 0, 'L', true, 0, false, false, 0);
        $pdf->SetFont('');
 		$from = array(
			'permintaan tj' => '',
			'member b' => array('b.member_id=tj.member_id','left'),
		);
		$select = 'tj.*,b.*';
		$data['data'] = $this->general_model->datagrab(array('tabel'=>$from, 'search'=>$fcari, 'order'=>'tj.tgl_bayar DESC'));
		$data['title'] = 'Data Permintaan';
		

        $html = $this->load->view('fengshui/permintaan_pdf_view', $data, true);

        $pdf->writeHTML($html);
        ob_end_clean();
        $pdf->Output('file-pdf-codeigniter.pdf', 'I');
		
		
	}
}
