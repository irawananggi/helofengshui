<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {
	var $dir = 'fengshui/Banner';
	var $bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember');

	function __construct() {
		parent::__construct();
		$this->load->helper('cmd', 'cms_helper');
		if (not_login(uri_string()))redirect('login');
		date_default_timezone_set('Asia/Jakarta');
		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->id_petugas = $id_pegawai;
		
	}

	function cr($e) {
	    return $this->general_model->check_role($this->id_petugas,$e);
    }

	public function index() {
		$this->list_data();
	}

	public function list_data($offset = null,$search=null) {
		$id_operator = $this->session->userdata('id_pegawai');
		
		if(@$_POST['key']!=''){
			$key = $_POST['key'];
			$this->session->set_userdata('kunci',$key);
		}else{
			if($offset != '') $key = $this->session->userdata('kunci');
			else $this->session->unset_userdata('kunci'); $key = '';
		}
		$offset = !empty($offset) ? $offset : null;
		$fcari = null;
		$search_key = $this->input->post('key');
		if (!empty($search_key)) {
			$fcari = array(
				'judul' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['judul'];
		} else if ($search) {
			$fcari = array(
				'judul' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['judul'];
		}
		$from = array(
			'banner tj' => '', 
		);
		$select = 'tj.*,tj.id as id_banner';
		$where = array();
		$config['base_url']	= site_url($this->dir.'/list_data/');
		$config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.id DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();

		$data['search']	= @$_POST['key'];
		$data['total']	= $config['total_rows'];
		$config['per_page']		= '';
		$config['uri_segment']	= '6';
		$this->pagination->initialize($config);
		$data['link'] = $this->pagination->create_links();
		$lim = ($offset == "cetak" or $offset == "excel") ? null : $config['per_page'];
		$offs = ($offset == "cetak" or $offset == "excel") ? null : $offset;
		$st = get_stationer();	
		$data['data_article'] = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select, 'search'=>$fcari,'where'=>$where,'limit'=>$lim, 'offset'=>$offs));
		$btn_tambah = anchor(site_url($this->dir.'/add_data'),'<i class="icon-icon-awesome-plus text-12"></i> Tambah Data','class="btn btn-warning radius-5 btn-sm" act="#"	title="Klik untuk tambah data"');
		$data['tombol'] = @$btn_tambah;
		$data['offs'] = $offs;
		$data['extra_tombol'] = 
				form_open($this->dir.'/list_data/','id="form_search" role="form"').
				'<div class="form-inline-group left-i border-icon">
                     <i class="icon-search in-left"> <button class="btn btn-default btn-flat in-left" style="margin-left: -34px;"></button></i>
                      <input name="key" type="text" placeholder="Pencarian ..." id="searchField"  class="form-control pull-right" value="'.@$search_key.'">
                     
				</div>'.
				form_close();
		$title = 'Banner';
		if ($offset == "cetak") {
			$data['title'] = '<h3>'.$title.'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/print',$data);
		} else if ($offset == "excel") {
			$data['file_name'] = $title.'.xls';
			$data['title'] = '<h3>'.$data['title'].'</h3>';
			$data['content'] = $tabel;
			$this->load->view('umum/excel',$data);
		} else {
			$data['title'] 		= $title;
			$data['tabel'] = $tabel;
			
			
			$data['title'] 		= $title;
			$data['content'] = 'fengshui/banner_view';
			$this->load->view('home', $data);
		}
	}

	

	function add_data($id = null){
		if($id!=NULL){
			//$p = un_de($id);
			$data['dt'] = $this->general_model->datagrab(array(
				'select' => '*',
				'tabel'	=> array(
					'banner' => ''
				),
				'where'=>array(
					'id'=> $id
					)
				)
			)->row();
		}
		$data['title']	= ' banner';
		$data['head']	= (!empty($id) ? 'Ubah' : 'Tambah').' Data';
		$data['tombol']	= (!empty($id) ? 'Update' : 'Simpan');
		$data['id']	= $id;
		
			$data['content'] = 'fengshui/banner_add';
			$this->load->view('home', $data);
	}

    function simpan_data(){
    	$id = $this->input->post('id');
    	$judul = $this->input->post('judul');
    	$deskripsi = $this->input->post('deskripsi');
    	$par = array(
					'tabel'=>'banner',
					'data'=>array(
						'judul'=>$judul,
						'deskripsi'=>$deskripsi,
						'status'=>0,
						),
					);

		if($id != NULL)	$par['where'] = array('id'=>$id);
		$this->load->library('Upload');

			$fi= $_FILES;
			
			$gambar = $fi['foto']['size'];
			
				$config = array(
					'allowed_types' => 'png|jpg|gif|jpeg',
					'upload_path' => 'uploads/banner',
					'overwrite' => TRUE,
					'file_name' =>'foto_'.date("Y-m-d").'_'.$fi['foto']['name'],
				);
				$this->upload->initialize($config);
				$this->upload->do_upload('foto');
				$data_upload = $this->upload->data();
				$foto2 = $data_upload['file_name'];


				if($id != NULL){
					$cek_gambar = $this->general_model->datagrabs(array('tabel'=>'banner','where'=>array('id'=>@$id),'select'=>'foto'))->row();
					/*cek($cek_gambar->foto);
					cek($gambar);
					die();*/
					$path = './uploads/banner';
					
				}

		    		if($gambar != 0) {

						$path_pasfoto = $path.'/'.$cek_gambar->foto;
						if(file_exists($path_pasfoto)) unlink($path_pasfoto);
						$par['data']['foto'] = $foto2;
					}else{
						$par['data']['foto'] = $cek_gambar->foto;

					}


				/**/
				
		$sim = $this->general_model->save_data($par);
		$this->session->set_flashdata('ok', 'Data Berhasil Disimpan...');
     
        redirect($this->dir);

    }


	function delete_data($id=null) {
		$del = $this->general_model->delete_data('banner','id',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data banner Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data banner Gagal di Hapus');
		}
		redirect($this->dir);
	}

  function edit_status($id=null) {
    
    $datax = $this->general_model->datagrab(array('tabel'=>'banner','where'=>array('id'=>$id)))->row();
    if($datax->status == 1){
      $datas = array('status'  => '0');
      $this->general_model->save_data('banner',$datas,'id',$id);
      $this->session->set_flashdata('ok','Data Berhasil di Non aktifkan');
    }else{
      $datas = array('status'  => '1');
      $this->general_model->save_data('banner',$datas,'id',$id);
    $this->session->set_flashdata('ok','Data Berhasil diaktifkan');

    }

    redirect($this->dir.'/list_data');
  }
  
	function pdf($search=null) {
		
		
		$fcari = null;
		$search_key = $search;
		if (!empty($search_key)) {
			$fcari = array(
				'judul' 		=> $search_key,
			);	
			$data['for_search'] = $fcari['judul'];
		} else if ($search) {
			$fcari = array(
				'judul' 		=> @un_de($search),
			);
			$data['for_search'] = $fcari['judul'];
		}
		
		$fcari = str_replace("%20", " ", $fcari);
		$this->load->library('pdf1');
        $parameters= array(
                'mode' => 'utf-8',
                'format' => 'A4',    // A4 for portrait
                'default_font_size' => '12',
                'default_font' => 'droidsansfallback',
                'margin_left' => 20,
                'margin_right' => 15,
                'margin_top' => 10,
                'margin_bottom' => 30,
                'margin_header' => 20,
                'margin_footer' => 10,
                'orientation' => 'P' // For some reason setting orientation to "L" alone doesn't work (it should), you need to also set format to "A4-L" for landscape
            );
        $pdf1->useAdobeCJK = true;

        $pdf1 = $this->pdf1->load($parameters);
		$pdf1->useAdobeCJK = true;
		$pdf1->autoLangToFont = true;
		$pdf1->autoScriptToLang = true;
        $pdf1->SetDisplayMode('fullpage');
 		$from = array(
			'banner tj' => '', 
		);
		$select = 'tj.*,tj.id as id_banner';
		$data['data'] = $this->general_model->datagrab(array('tabel'=>$from, 'search'=>$fcari, 'order'=>'tj.created_at DESC'));
		$data['title'] = 'Data Blog';
		$tabel = '
    <style type="text/css">
        table{
        	font-size:10px;
        	width:100%;
                padding:10px;
            border-spacing:0;border-collapse:collapse
            }
            tr,td,th{
                padding:10px
            }
            th{
                text-align: center;padding:10px
            }
        .table{
            border-collapse:collapse!important
            }
            
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important;
                    padding:10px;
                }

    </style>';
          if ($data['data']->num_rows() > 0) { 
            $tabel .= '
            <h4 class="">Data Banner</h4>
            <table class="table table-striped table-bordered"  style="width: 100%;">
              <thead>
                <tr>
                	<td scope="col" class="text-2 medium">No</td>
	                <td scope="col" class="text-2 medium">Judul</td>
	                <td scope="col" class="text-2 medium">Banner</td>
                </tr>
              </thead>
              <tbody>';
              $no=1;
              foreach ($data['data']->result() as $row) { 
              $tabel .= '
              <tr>
                

				<td class="align-middle" style="width: 60px;">'.$no.'</td>
                <td class="align-middle">
                   '.$row->judul.'
                </td>
                <td class="align-middle">
                    <a href="'.$row->deskripsi.'">'.$row->deskripsi.'</a>
                </td>
              </tr>';
              $no++;
            }
         
            
              
            $tabel .= ' </tbody>
          </table>';
          
                 }else{
             $tabel .= '<div class="alert">Data masih kosong ...</div>';
          } 

        $html = $tabel;

        $pdf1->WriteHTML($html);
        ob_end_clean();
        $pdf1->Output('Data Kategori.pdf', 'I');
		
		
	}
}
