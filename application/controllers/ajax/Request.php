<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Home';
		$this->load->view('home',$data);
	}

	// dataset

	function get_access_kalender(){
		$in= $this->input->get();
		$id = $in['id_ka'];
		

		$from= array(
			'elearn_kalender_akademik ak'=>'',
			'elearn_ref_kegiatan keg'=>'keg.id_kegiatan=ak.id_kegiatan',
		);
		$select= 'ak.id_ka, DATE_FORMAT(ak.mulai,"%d/%m/%Y") mulai, DATE_FORMAT(ak.selesai,"%d/%m/%Y") selesai';
		
		$whe = array('ak.id_ka'=>$id);

		$result = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$whe,
			'select'=>$select,
		))->row_array();
		if(is_array($result) && count($result) > 0) {
			$res = $result;
		}else{
			$res = false;
		}
		
		die(json_encode($res));
	}
	function kalender_ujian(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= array(
			'elearn_kalender_akademik ak'=>'',
			'elearn_ref_kegiatan keg'=>'keg.id_kegiatan=ak.id_kegiatan',
		);
		$select= 'ak.id_ka id, concat(keg.kegiatan, ": ", DATE_FORMAT(ak.mulai,"%d/%m/%Y")," sampai ",DATE_FORMAT(ak.selesai,"%d/%m/%Y")) text';
		$order= 'ak.mulai';
		$whe = array('keg.tipe'=>3);
		if(!empty($in['level_unit'])) $whe['ak.id_level_unit'] = $in['level_unit'];

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$whe,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'where'=>$whe,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}
	function tahun_ajaran(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'elearn_tahun_ajaran';
		$select= 'id_ta id, tahun_ajaran text';
		$order= 'tahun_ajaran';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}
	function level_unit(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'ref_level_unit';
		$select= 'id_level_unit id, lev_unit text';
		$order= 'lev_unit';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}
	function unit(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'ref_unit';
		$select= 'id_unit id, unit text';
		$order= 'unit';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}
	function kurikulum(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'elearn_kuri';
		$select= 'id_kuri id, nama text';
		$where = array();
		if(!empty($in['id_unit'])) $where['id_unit'] = $in['id_unit'];
		$order= 'id_kuri';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$where,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'where'=>$where,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}

	function kelas(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= array(
			'elearn_kelas kelas'=>'',
			'elearn_ref_kelas kl'=>'kl.id_ref_kelas=kelas.id_ref_kelas',
		);
		$select= 'kelas.id_kelas id, kl.kelas text';
		$where = array();
		if(!empty($in['id_unit'])) $where['kl.id_unit'] = $in['id_unit'];
		if(!empty($in['id_kuri'])) $where['kelas.id_kuri'] = $in['id_kuri'];
		$order= 'kelas.id_kelas';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$where,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'where'=>$where,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}

	function mapel(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= array(
			'elearn_kelas_mapel mapel'=>'',
			'elearn_ref_kel klp'=>'klp.id_ref_kel=mapel.id_ref_kel',
		);
		$select= 'mapel.id_mapel id, klp.kelompok text';
		$where = array();
		if(!empty($in['id_kelas'])) $where['mapel.id_kelas'] = $in['id_kelas'];
		$order= 'mapel.id_mapel';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$where,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'where'=>$where,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}
	function tipe_ujian(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'elearn_ref_ujian';
		$select= 'id_tipe_ujian id, tipe_ujian text';
		$where = array();
		$order= 'id_tipe_ujian';

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'where'=>$where,
			'select'=>$select,
			'count'=>'count(*) ct',
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'where'=>$where,
				'select'=>$select,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}

	function kelompok(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= 'elearn_ref_kel';
		$select= 'id_ref_kel id, kelompok text';
		$order= 'kelompok';
		$whe = array('id_ref_kel IN (SELECT id_ref_kel FROM elearn_kelas_mapel WHERE id_pemateri='.sesi('id_pegawai').')'=>NULL);

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'select'=>$select,
			'count'=>'count(*) ct',
			'where'=>$whe,
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'select'=>$select,
				'where'=>$whe,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}

	function _ref_tipe() {
		$results=array();
		$dt=$this->general_model->datagrabe(array(
			'tabel'=>'elearn_ref_tipe_data',
			'order'=>'id_tipe_data',
		));
		if($dt->num_rows()>0)
			foreach ($dt->result() as $r) 
				$results[]=array(
					'id'=>$r->id_tipe_data,
                #'kode'=>$r->kode,
					'text'=>$r->ket,
				);
			return $results;
		}
		function tipe_materi(){
			$results=$this->_ref_tipe();
		/*$results = array(
			array('id'=>'teks', 'text'=>'Teks/Dokumen'),
			array('id'=>'pdf', 'text'=>'Upload PDF'),
			array('id'=>'vid_upload', 'text'=>'Upload Video'),
			array('id'=>'vid_link', 'text'=>'Link Video'),
			array('id'=>'link', 'text'=>'Link Website'),
			array('id'=>'gambar', 'text'=>'Gambar'),
		);*/
		$res = array(
			'results'=>$results,
			'status'=>'success',
			'limit'=>1,
			'pagination'=>array('more'=>false),
		);
		die(json_encode($res));
	}
	function tipe_soal(){
		$results=$this->_ref_tipe();
        /*$results = array(
			array('id'=>'teks', 'text'=>'Teks/Dokumen'),
			array('id'=>'pdf', 'text'=>'Upload PDF'),
			array('id'=>'vid_upload', 'text'=>'Upload Video'),
			array('id'=>'vid_link', 'text'=>'Link Video'),
			array('id'=>'link', 'text'=>'Link Website'),
			array('id'=>'gambar', 'text'=>'Gambar'),
		);*/
		$res = array(
			'results'=>$results,
			'status'=>'success',
			'limit'=>10,
			'pagination'=>array('more'=>false),
		);
		die(json_encode($res));
	}


	function guru(){
		$in= $this->input->get();
		$page = $in['page'];
		$term = @$in['term'];

		$sc = array();
		if(isset($term)){
			$sc = array(
				'text'=>$in['term']
			);
		}

		$from= array(
			'peg_pegawai p'=>'',
			"peg_jabatan pj"=>array('pj.id_pegawai=p.id_pegawai','left'),
			"pegawai_role pr"=>array('pr.id_pegawai=p.id_pegawai','left'),
			"ref_jabatan rj"=>array('rj.id_jabatan=pj.id_jabatan','left'),
		);
		$select= 'p.id_pegawai id, concat(p.nip,"-",p.nama) text';
		$order= 'p.nama';
		$whe = array('pr.id_role'=>4); //pengajar
		if(isset($in['id_unit'])) $whe['pj.id_unit'] = $in['id_unit'];
		if(isset($in['id_guru'])) $whe['p.id_pegawai'] = $in['id_guru'];
		$ct = $this->general_model->datagrabs(array(
			'tabel'=>$from,
			'select'=>$select,
			'count'=>'count(*) ct',
			'where'=>$whe,
			'search'=>$sc
		))->row()->ct;


		$limit = 15;
		if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;


		if($ct > 0){

			$dt = $this->general_model->datagrabs(array(
				'tabel'=>$from,
				'select'=>$select,
				'where'=>$whe,
				'search'=>$sc,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order,
			));
			$dt_result = array();
			$ct_set = 0;
			if(@$in['combo'] && $page == 1) $dt_result[] = array('id'=>'','text'=>'--Pilih--');
			foreach ($dt->result() as $row) {
				
				$dt_result[] = array(
					'id'=>$row->id,
					'text'=>ucwords(strtolower($row->text)),
					
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;

			$res['status']= 'success';
			$res['results']= $dt_result;
			$res['page']= intval($page);
			$res['offset']= intval($offset);
			$res['limit']= intval($limit);
			$res['total']= $ct;
			$res['pagination']= array('more'=>$morePages);

		}else{
			$res['status'] = 'failed';
		}

		die(json_encode($res));
	}

}