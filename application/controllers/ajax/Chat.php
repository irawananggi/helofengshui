<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	function save_message(){
		$in = file_get_contents('php://input');
		$in = json_decode($in, true);
    	// cek($in);
    	// die();
    	// print_r($in);
		$simpan = array(
			'user_encript'=>$in['user_encript'],
			'message'=>$in['message'],
			'record'=>$in['record'],
		);

		if($this->general_model->save_data('elearn_chat', $simpan)){
			$id_chat = $this->db->insert_id();
			$res['status'] = 'success';
			$res['id_chat'] = $id_chat;
			$res['record'] = date('d/m/Y H:i', strtotime($simpan['record']));
		}else{
			$res['status'] = 'failed';
		}
		

		die(json_encode($res));
	}

	function load_message(){
		$in = file_get_contents('php://input');
		$in = json_decode($in, true);
		$page = intval($in['page']);
		if(empty($page)) $page = 1;
		$where = array();
		if(isset($in['this_day'])) $where['DATE_FORMAT(record, "%Y%m%d") = '.date('Ymd')] = null;

		if(isset($in['prev_message'])){
			$last_day = $in['last_day'];
			// print_r($last_day);
			if(empty($last_day)) $last_day = date('d/m/Y H:i:s');
			$last_day = date('Y-m-d',strtotime(str_replace("/", "-", $last_day)));
			$date_req = date('Ymd', strtotime('-1 days', strtotime($last_day)));
			$where['DATE_FORMAT(record, "%Y%m%d") = '.$date_req] = null;
		}

		$ct = $this->general_model->datagrabs(array(
			'tabel'=>'elearn_chat',
			'where'=>$where,
			'select'=>'count(id_chat) count'
		))->row()->count;

		$limit = 1;
		// if(empty($page)) $page = 1;
		$offset = ($page - 1) * $limit;
		$order = 'record';
		if(isset($in['prev_message'])) $order = 'record DESC';

		if($ct > 0){
			$message = $this->general_model->datagrabs(array(
				'tabel'=>'elearn_chat',
				'where'=>$where,
				'limit'=>$limit,
				'offset'=>$offset,
				'order'=>$order
			));
			$results = array();
			foreach ($message->result() as $row) {
				$results[] = array(
					'id_chat'=>$row->id_chat,
					'user_encript'=>$row->user_encript,
					'message'=>$row->message,
					'record'=>date('d/m/Y H:i', strtotime($row->record)),
				);
			}

			$endCount = $offset + $limit;
			$morePages = $ct > $endCount;
			$res['status'] = 'success';
			$res['results'] = $results;
			$res['load_more'] = $morePages;
			$res['page'] = $page;
			$res['end_count'] = $endCount;
			$res['total_data'] = $ct;
		}else{
			$res['status'] = 'failed';
		}

		if(isset($in['prev_message'])) $res['req_day'] = $date_req;
		if(isset($in['prev_message'])) $res['last_day'] = $last_day;

		// $this->general_model->save_data('elearn_chat', $simpan);
		
		die(json_encode($res));
	}

}