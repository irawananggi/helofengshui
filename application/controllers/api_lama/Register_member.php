<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require 'phpmailer/PHPMailerAutoload.php';
ob_start();

class Register_member extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
   /* public function send_sms($no,$kode){
    $userkey = 'dc85ab108a7c';
    $passkey = '5s3qrrnyg5';
    $telepon = $no;
    $otp = $kode;
    $url = 'https://gsm.zenziva.net/api/sendOTP/';
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_URL, $url);
    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
    curl_setopt($curlHandle, CURLOPT_POST, 1);
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
        'userkey' => $userkey,
        'passkey' => $passkey,
        'nohp' => $telepon,
        'kode_otp' => $otp
    ));
    $results = json_decode(curl_exec($curlHandle), true);
    curl_close($curlHandle);
    }
    public function send_wa($no,$kode){
      $userkey = 'dc85ab108a7c';
      $passkey = '5s3qrrnyg5';
      $telepon = $no;
      $message = 'Pendaftaran akun Jakarta Bubble Drink. Kode OTP anda : '.$kode;
      $url = 'https://gsm.zenziva.net/api/sendWA/';
      $curlHandle = curl_init();
      curl_setopt($curlHandle, CURLOPT_URL, $url);
      curl_setopt($curlHandle, CURLOPT_HEADER, 0);
      curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
      curl_setopt($curlHandle, CURLOPT_POST, 1);
      curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
          'userkey' => $userkey,
          'passkey' => $passkey,
          'nohp' => $telepon,
          'pesan' => $message
      ));
      $results = json_decode(curl_exec($curlHandle), true);
      curl_close($curlHandle);
    }*/
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $from = $this->post('from');
      $msg="";/*
        if ($this->post('password')==$this->post('cpassword')) {*/
              $email =$this->post('email');
              $pesan = "Nomor Validasi TokoLift";
              $cek = $this->mymodel->getbywhere('member',"email='$email'",null,"row");
              
              if (!empty($cek)) {
                $msg = array('status' => 0, 'message'=>'Email Sudah Terdaftar' ,'data'=>new stdClass());
              }else{
                  $token = md5(uniqid().$this->post('email').$this->post('password').date('Y-m-d H:i:s'));
                  $data = array(
                  "member_id" => "",
                  "nama_lengkap" => $this->post('nama_lengkap'),
                  "email" => $this->post('email'),
                  "password" => md5($this->post('password')),
                  "token" => $token,
                  "is_active" => 1,
                  "register_from" => 0,
                  "created_at" => date('Y-m-d H:i:s'),
                  "is_deleted" => 0,
                  );

                if (!empty($data)) {
                  $this->mymodel->insert('member',$data);
                  //Insert kode verifikasi
                  $kode = rand(100000,999999);
                  $data2 = array(
                    "member_id" => $this->mymodel->getlast('member','member_id')->member_id,
                    "code" => $kode,
                    "type" => 1
                    );
                  $this->mymodel->insert('verification_code',$data2);
                  $data['kode_verifikasi'] = $kode;
                   //$this->send_sms($phone,$kode);
                   //$this->send_wa($phone,$kode);
                   //$this->send_email("",$this->post('email'),"Pendaftaran Akun JBD","Berikut adalah kode verifikasi member anda di Jakarta Bubble Drink.",$kode);

                  $msg = array('status' => 1, 'message'=>'Berhasil Insert data' ,'data'=>$data);

                }else {
                  $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>new stdClass());
                }
              }

        
      $this->response($msg);

  }
  public function send_email($file="",$to='',$title,$desc,$data)
    {
      $to = urldecode($to);
      $mail = new PHPMailer;
      // Konfigurasi SMTP
      $mail->isSMTP();
      $mail->SMTPDebug =0;
      // $mail->Host = 'mail.namagz.com';
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPOptions = array(
         'ssl' => array(
           'verify_peer' => false,
           'verify_peer_name' => false,
           'allow_self_signed' => true
          )
      );
      $mail->SMTPAuth = true;
      // $mail->Username = 'syauqi@namagz.com';
      // $mail->Password = 'koroko11';
      $mail->Username = 'dev.jakarta.bd@gmail.com';
      $mail->Password = 'JAKARTAbd2020';
      $mail->SMTPSecure = 'ssl';
      $mail->Port = 465;

      $mail->addReplyTo('no-reply@jakartabubbledrink.com', 'JBD Developer Team');
      $mail->setFrom('no-reply@jakartabubbledrink.com', 'JBD Developer Team');

      // Menambahkan penerima
      $mail->addAddress($to);

      // Menambahkan beberapa penerima


      // Subjek email
      $mail->Subject = '[No Reply] Pendaftaran Member JBD';

      // Mengatur format email ke HTML
      $mail->isHTML(true);

      // Konten/isi
       $data_['title'] = $title;
       $data_['msg'] = $desc;
       $data_['code'] = $data;
       $mailContent = $this->load->view('email_verifikasi',$data_,true);
      $mail->Body = $mailContent;
      // Menambahakn lampiran

      // Kirim email
      if(!$mail->send()){
          echo 'Pesan tidak dapat dikirim.';
          echo 'Mailer Error: ' . $mail->ErrorInfo;
      }else{
          //echo 'Pesan telah terkirim ';
      }
    }
}
