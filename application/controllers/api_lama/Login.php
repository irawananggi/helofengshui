<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require 'phpmailer/PHPMailerAutoload.php';
//ob_start();

class Login extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      $email = $this->post('email');
      $password = $this->post('password');
      $from = $this->post('from');
     /* cek($email);
      cek($password);
      cek($from);
      die();*/
      $msg ="";
      if ($email !="" && $password!="") {
          $cek = $this->mymodel->getbywhere('member',"email='$email' or phone='$email'",null,"row");
          $success = 0;
          $message = "";
          $stats = "";
          $data_diri = 0;

          if (!empty($cek)) {
            $data = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') and password=",md5($password),"row");

            if (!empty($data)) {
              $success = 1;
                $message = "Login Berhasil";
                $stats = "Silahkan melengkapi data diri";
                $data2 = array(
                  "last_login" => date('Y-m-d H:i:s')
                  );
                $this->mymodel->update('member',$data2,'phone',$data->phone);
                $data = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') and password=",md5($password),"row");

                
            }else{
              $data = new stdClass();
              $success =0;
              $message = "Password Salah";
              $stats = "Password Salah";
            }
            if ($success == 0 ) {
              $msg = array('status'=>$success,'message'=>$message,'stats'=>$stats,'data' => $datax, 'data_diri' => $data_diri);
            }else{
              $data_diri = 1;
              $datax = $this->mymodel->getbywhere('member','email',$email,"row");
              
              if(empty($datax->img_file)){
                   $img_file = NULL;
                  }else{
                    $img_file = base_url('/uploads/member/'.$datax->img_file);
                  }
               $datax = array(
                "member_id" => $datax->member_id,
                "nama_lengkap" => $datax->nama_lengkap,
                "email" => $datax->email,
                "img_file" => $img_file,
                "password" => $datax->password,
                "token" => $datax->token,
                "fcm_id" => $datax->fcm_id,
                "is_active" => $datax->is_active,
                "last_login" => $datax->last_login,
                "created_at" => $datax->created_at
                ); 
                 
             
              
              $msg = array('status'=>$success,'message'=>$message,'stats'=>$stats,'data' => $datax  , 'data_diri' => $data_diri);
            }

          }else{
            $msg = array('status'=>0,'message'=>"Email tidak terdaftar",'data' => null, "data_diri" => $data_diri);
          }

        }else if($email==""){
            $msg = array('status' => 0, 'message'=>'Email Harus Diisi');
        }else if($password==""){
            $msg = array('status' => 0, 'message'=>'Password Harus Diisi');
        }
        $this->response($msg);
    }

}
