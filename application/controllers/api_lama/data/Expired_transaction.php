<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Expired_transaction extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

          //$mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $get_trans = $this->mymodel->getbywhere('htrans',"transaction_status_id",'1',"result");
          foreach ($get_trans as $key => $value) {
            if (strtotime($value->expired_date) == date("Y-m-d H:i:s")) {
                if ($value->img_file=="") {
                $data_update = array(
                  "transaction_status_id" => "4"
                  );
                $expired = $this->mymode->update("htrans",$data_update,"htrans_id",$value->htrans_id);
                $msg = array('status' => 1, 'message'=>'Berhasil Ubah data (Transaksi Expired)');
              }else{
                $data_update = array(
                  "transaction_status_id" => "2"
                  );
                $expired = $this->mymode->update("htrans",$data_update,"htrans_id",$value->htrans_id);
                $msg = array('status' => 1, 'message'=>'Berhasil Ubah data (Transaksi Sedang diproses)');
              }
            }else{
              continue;
            }
          }          

          $this->response($msg);
      
    }
}