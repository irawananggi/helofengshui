<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Send_chat_file extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token!='') {
        $member = $this->mymodel->getbywhere('member','token',$token,'row');
          if (isset($member)) {
              //$pesan =$this->post('pesan');
              $url =$this->post('url_galery');
              if(empty($pesan)) $pesan="";

              $product_id = $this->post('product_id');
              $galery_type = $this->post('galery_type');

              $data = array(
                'member_id' => $member->member_id,
                'product_id' => $product_id,
                'member_is_sender'=>1,
                'created_at' => date('Y-m-d H:i:s')
              );

              if (!empty($_FILES['galery_chat']['name'])) {
                $uploaddir = './assets/img/chat/';
                $img = explode('.', $_FILES['galery_chat']['name']);
                $extension = end($img);
                $file_name =  md5(date('y-m-d h:i:s').$_FILES['galery_chat']['name']).".".$extension;
                $uploadfile = $uploaddir.$file_name;

                if (move_uploaded_file($_FILES['galery_chat']['tmp_name'], $uploadfile)) {
                  $data["url_galery"] = base_url("assets/img/chat/".$file_name);
                  $data["galery_type"] = $galery_type;
                }
              }

              $in = $this->mymodel->insert('chat',$data);
              if ($in) {
                $msg = array('status'=>1,'message'=>'Chat Berhasil Terkirim','data'=>$data);
              }else {
                $msg = array('status'=>0,'message'=>'Gagal insert','data'=>array());
              }
              //$this->firebase($this->post('id_chat_category'),$this->post('date_time'));

          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }
}
