<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Delete_item_pengaman extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$id_cart = $this->post('cart_id');
          $id_pengaman = $this->post('id_pengaman');
          $grand = 0;
          if (isset($mem)) {
            $data = $this->mymodel->withquery("select * from member_pengaman where member_id='".$mem->member_id."'
              and id_pengaman='".$id_pengaman."'",'result');
            $member_pengaman = $this->mymodel->getbywhere('member_pengaman','member_id',$mem->member_id,'result');
            foreach ($member_pengaman as $key => $value) {
              $get_pengaman = $this->mymodel->getbywhere('additional_prices','id',$value->id_pengaman,'row');
              $grand = $grand + ($get_pengaman->biaya*$value->quantity);
            }
            foreach ($data as $key => $value) {
              $get_pengaman = $this->mymodel->getbywhere('additional_prices','id',$value->id_pengaman,'row');
              $sum = $value->quantity*$get_pengaman->biaya;
              $grand = $grand - $sum;
            }
            if (!empty($data)) {
              $this->mymodel->delete2('member_pengaman','member_id',$mem->member_id,'id_pengaman',$id_pengaman);
              $grand = "Rp ".number_format($grand,0,"",".");
              $msg = array('status' => 1, 'message'=>'Berhasil Hapus data','grand_total'=>$grand);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'grand_total'=>0);
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','grand_total'=>0);
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','grand_total'=>0);
        $this->response($msg);
      }
    }
}
