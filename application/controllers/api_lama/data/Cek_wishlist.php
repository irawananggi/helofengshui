<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Cek_wishlist extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $id_product = $this->get('product_id');
            $data = $this->mymodel->getbywhere('wishlist',"product_id=",$id_product,'result');
            foreach ($data as $key => $value) {
              $product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
              $product_image = $this->mymodel->getbywheresort('product_image','product_id',$id_product,'id_product_image','DESC');
              $img_product;
              foreach ($product_image as $key => $valuee) {
                $img_product = $valuee->img_file;
              }
              $value->product_name = $product->product_name;
              $value->img_product = base_url("assets/img/product/".$img_product);
              $cek_wish = $this->mymodel->getbywhere('wishlist',"product_id = '".$id_product."' and member_id=",$mem->member_id,'result');
              if (!empty($cek_wish)) {
                $value->status_wish = 1;
              }else{
                $value->status_wish = 0;
              }
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}
