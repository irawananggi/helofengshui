<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Wishlist extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            //$data = $this->mymodel->getbywhere('wishlist','member_id',$mem->member_id,'result');
          $data = $this->mymodel->withquery("select w.*,COUNT(t.product_id) as hitung,
          COUNT(pt.product_id) as total_rate,
          AVG(pt.rate) as average_rate 
          from product p, dtrans t, product_ratting pt, wishlist w 
          where p.product_id = t.product_id and
           w.member_id = '".$mem->member_id."'
          GROUP BY w.wishlist_id order by hitung desc",'result');
            foreach ($data as $key => $value) {
              $get_product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
              $value->product_name = $get_product->product_name;
              $value->description = $get_product->description;
              $value->quantity = $get_product->quantity;
              $value->created_at = $get_product->created_at;
              $value->updated_at = $get_product->updated_at;
              if ($value->updated_at == null) {
                $value->updated_at = "";
              }
              $value->price = "Rp ".number_format($get_product->price,0,"",".");
              $get_img = $this->mymodel->getlastwhere('product_image','product_id',$value->product_id,'id_product_image');
              $value->product_img = base_url('assets/img/product/'.$get_img->img_file);
              
              $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
                where product_id = '".$value->product_id."'","result");
              $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
                  where product_id = '".$value->product_id."'","result");
              $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
                  where product_id = '".$value->product_id."'","result");
              foreach ($total_trans as $key => $valuee) {
                $value->hitung = $valuee->total_transaksi;
              }
              foreach ($average_rate as $key => $valuee) {
                $value->average_rate = round($valuee->average_rate);
              }
              foreach ($total_review as $key => $valuee) {
                $value->total_rate = $valuee->total_review;
              }
              if (!empty($mem)) {
                $cek_wishlist = $this->mymodel->getbywhere('wishlist',"product_id='".$value->product_id."' and member_id=",$mem->member_id,'row');
                if (empty($cek_wishlist)) {
                  $value->is_wishlist = 0;
                }else{
                  $value->is_wishlist = 1;
                }
              }else{
                $value->is_wishlist = 0;
              }
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else if (empty($data)) {
             $msg = array('status' => 1, 'message'=>'Wishlist Kosong!' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}