<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class List_diskusi_produk extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

          $id_produk = $this->get('id_produk');
            $get_produk = $this->mymodel->getbywhere('product','product_id',$id_produk,'row');
            $data = $this->mymodel->getbywhere('diskusi',"product_id='".$id_produk."' and pertanyaan_id=","0",'result');
            foreach ($data as $key => $value) {
              
              $value->nama_produk = $get_produk->product_name;
              $tgl = explode("-", date("Y-m-d H:i",strtotime($value->created_at)));
              $potong = explode(" ", $tgl[2]);
              $tanggal = $potong[0];
              $tanggal = $tanggal." ".$this->get_bulan($tgl[1])." ";
              $value->created_at = $tanggal.$tgl[0]." ".$potong[1];
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data kosong / tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);

    }
    public function get_bulan($bulan){
    $bln = "";
    if ($bulan == "01") {
      $bln = "Januari";
    }else if ($bulan == "02") {
      $bln = "Februari";
    }else if ($bulan == "03") {
      $bln = "Maret";
    }else if ($bulan == "04") {
      $bln = "April";
    }else if ($bulan == "05") {
      $bln = "Mei";
    }else if ($bulan == "06") {
      $bln = "Juni";
    }else if ($bulan == "07") {
      $bln = "Juli";
    }else if ($bulan == "08") {
      $bln = "Agustus";
    }else if ($bulan == "09") {
      $bln = "September";
    }else if ($bulan == "10") {
      $bln = "Oktober";
    }else if ($bulan == "11") {
      $bln = "November";
    }else if ($bulan == "12") {
      $bln = "Desember";
    }
    return $bln;
  }

  public function get_hari($hari){
    $day = "";
    if ($hari == 1) {
      $day = "Senin";
    }else if ($hari == 2) {
      $day = "Selasa";
    }else if ($hari == 3) {
      $day = "Rabu";
    }else if ($hari == 4) {
      $day = "Kamis";
    }else if ($hari == 5) {
      $day = "Jumat";
    }else if ($hari == 6) {
      $day = "Sabtu";
    }else if ($hari == 7) {
      $day = "Minggu";
    }
    return $day;
  }
}