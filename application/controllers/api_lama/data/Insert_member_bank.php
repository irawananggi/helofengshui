<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Insert_member_bank extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $norek = $this->post('no_rekening');
          $bank_id = $this->post('bank_id');
          if (isset($mem)) {
            $cek = $this->mymodel->getbywhere("member_bank","bank_id='".$bank_id."' and no_rekening='".$norek."' and member_id=",$mem->member_id,'row');
            if (empty($cek)) {
            $data = array(
              "member_id" => $mem->member_id,
              "bank_id" => $bank_id,
              "no_rekening" => $norek,
              "owner" => $this->post('owner'),
              "created_at" => date('Y-m-d H:i:s'),
              "is_deleted" => 0
              );
            
              if (!empty($data)) {
                $this->mymodel->insert('member_bank',$data);
                $msg = array('status' => 1, 'message'=>'Berhasil Tambah Data' ,'data'=>$data);
              }else {
                $msg = array('status' => 0, 'message'=>'Data Error' ,'data'=>array());
              }
            }else{
              $msg = array('status' => 0, 'message'=>'Nomor Rekening Sudah terdaftar' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}