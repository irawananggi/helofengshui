<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Customer extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {

      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
       $param = array();
      if(isset($headers['token']))
        $token =  $headers['token'];
       
        if ($this->get('member_id')) {
            $param['member_id'] = $this->get('member_id');
        }
        if ($this->get('first_name')) {
            $param['first_name'] = $this->get('first_name');
        }
        if ($this->get('last_name ')) {
            $param['last_name '] = $this->get('last_name ');
        }
       
        foreach ($param as $key => $value) {
            $this->db->where($key,$value);
        }
        $this->db->where('is_deleted','0');
        $data = $this->db->get('member')->result();
        
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
      
    }

    

}