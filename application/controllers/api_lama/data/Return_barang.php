<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAnrD16lI:APA91bH0MHVh9XsRh6tL8y5ZlGeE7z_-x1jBRPyajmKvUWnuN5wgKA8GspRjOvvf_t0-4Qo3MbugqZmRwLSsi73KHjQuXKL3skxDmj1GskZnyjIO5Vdrb0rQ0hZM1Mw6IGQaiRa-xgRo' );

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Return_barang extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    // insert new data to account
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (!empty($mem)) {
            $qty = $this->post('qty');
            $alasan = $this->post('alasan');
            $kode_transaksi = $this->post('kode_transaksi');
            $product_id = $this->post('product_id');
              $uploaddir = './assets/img/return_transaksi/';
              $img = explode('.', $_FILES['img']['name']);
              $extension = end($img);
              $file_name =  md5(date('y-m-d h:i:s').$_FILES['img']['name']).".".$extension;
              $uploadfile = $uploaddir.$file_name;
              $status = 0;
              //cek retur apakah sudah pernah return atau belum
              $cek = $this->mymodel->getbywhere("return_transaksi","kode_transaksi='".$kode_transaksi."' and $product_id=",$product_id,"row");
              if (!empty($cek)) {
                $msg = array('status' => 0, 'message' => 'Produk di transaksi tersebut telah terdaftar');
              }
              else{
                if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                  $data_upload = array(
                    'img_file'=>$file_name,
                    'alasan_return' => $alasan,
                    'product_id' => $product_id,
                    'member_id' => $mem->member_id,
                    'kode_transaksi' => $kode_transaksi,
                    'quantity' => $qty,
                    'created_at' => date("Y-m-d H:i:s"),
                    'is_deleted' => 0,
                    'return_status_id' => 2
                  );
                  $this->mymodel->insert("return_transaksi",$data_upload);

                  $get_id = $this->mymodel->getlastwhere("return_transaksi","is_deleted","0","id");
                  $mem = $this->mymodel->getbywhere('member','member_id',$get_id->member_id,'row');
                  if ($mem->fcm_id != "") {
                    //send notif
                    $this->send_notif("Permintaan Retur Telah Diterima","Permintaan retur anda pada transaksi ".$kode_transaksi.", sedang kami lakukan pengecekan", $mem->id_fcm, array('title' =>"Permintaan Retur Telah Diterima" , 'message' => "Permintaan retur anda pada transaksi ".$kode_transaksi.", sedang kami lakukan pengecekan", 'tipe' => 'detail_retur','content' => array("id_retur"=>$get_id->id, "kode_transaksi"=>$get_id->kode_transaksi )) );
                  }

                  //send email
              $banks = $this->mymodel->getall("company_bank");
              $get_id = $this->mymodel->getbywhere("htrans","tracking_id",$kode_transaksi,"row");
              $carts = $this->mymodel->getbywhere("dtrans","product_id='".$product_id."' and htrans_id=",$get_id->htrans_id,"result");
              $email['username'] = $mem->first_name;
              $email['kode_transaksi'] = $kode_transaksi;
              $email['carts'] = $carts;
              $this->send_email_file($mem->email, $email);
                  $msg = array('status'=>1,'message'=>'Form berhasil ditambahkan');
                } else {
                    $msg = array('status'=>0,'message'=>'Gagal menambahkan data');
                }
              }
          }else {
              $msg = array('status' => 1, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

    }

  public function send_notif($title,$desc,$id_fcm,$data)
  {
    $Msg = array(
      'body' => $desc,
      'title' => $title
    );

    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    //echo $result."\n\n";
  }

  public function send_email_file($to='',$data)
  {
    $this->load->library('email');

    // extract($data);
      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.gmail.com',
          'smtp_port' => 465,
          'smtp_user' => 'dev.jakarta.bd@gmail.com',
          'smtp_pass' => 'JAKARTAbd2020',
          'mailtype'  => 'html',
          'smtp_timeout' => 20,
          'charset'   => 'utf-8'
      );


      
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      // $htmlContent = '<h1>Sending email via SMTP server</h1>';
      // $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';
      $this->email->to($to);

      $data_['msg'] = ".";
      // $data_['code'] = "<a href='".site_url('Member/success?t='.$data)."' target='_blank'> Klik Disini </a>";
      $data_['title'] = "Pesanan JBD - Jakarta Bubble Drink";
      // $htmlContent = " <div>Halo $username, Pesanan kamu sudah kita terima dan akan diproses<div>"; //$this->load->view('email_konfirmasi',$data_,true);
      $htmlContent = $this->load->view("email/request_retur", $data, true);
      //$htmlContent = $this->load->view("email_cart", $data, true);
      $this->email->from('no-reply@jbd.com', 'JBD - Jakarta Bubble Drink');
      $this->email->subject('[No Reply] Pesanan JBD - Jakarta Bubble Drink');
      $this->email->message($htmlContent);

      //Send email
      $this->email->send();
      // echo $this->email->print_debugger();
  }

}
?>
