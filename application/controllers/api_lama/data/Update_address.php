<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Update_address extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $member_address_id = $this->post('member_address_id');
          if (isset($mem)) {
            $data = array(
              "province_id" => $this->post('province_id'),
              "city_id" => $this->post('city_id'),
              "subdistrict_id" => $this->post('subdistrict_id'),
              "village" => $this->post('village'),
              "street" => $this->post('street'),
              "zipcode" => $this->post('zipcode'),
              "jenis" => $this->post('jenis'),
              "updated_at" => date('Y-m-d H:i:s')
              );            
            $update = $this->mymodel->update('member_address',$data,"member_address_id='".$member_address_id."' and member_id=",$mem->member_id);
            if (!empty($data) && $update) {
              $msg = array('status' => 1, 'message'=>'Berhasil Update data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Gagal Update, Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}