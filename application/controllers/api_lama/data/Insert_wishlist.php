<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Insert_wishlist extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $product_id = $this->post('product_id');
          if (isset($mem)) {
            $data = array(
              "wishlist_id" => "",
              "product_id" => $product_id,
              "member_id" => $mem->member_id
              );
            $cek = $this->mymodel->getbywhere('wishlist',"member_id='".$mem->member_id."' and product_id=",$product_id,"row");
            if (!empty($data) && empty($cek)) {
              $this->mymodel->insert('wishlist',$data);
              $msg = array('status' => 1, 'message'=>'Berhasil Insert data' ,'data'=>$data);
            }else if (!empty($cek)) {
              $msg = array('status' => 0, 'message'=>'Produk Sudah berada di Wishlist' ,'data'=>array());
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}