<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require_once('sms/api_sms_class_reguler_json.php');
ob_start();
define( 'API_ACCESS_KEY', 'AAAABMMsQoY:APA91bFjHzOanAUv3_lju701o8nrlLo5TG9wXXxchECcOY7DrKex6ugX3c3Hv5Y0Hdcrlb9tM5nVKAWSzX3BIV32xJuRaP0A3VWkoHtq-CSi4aE62HQP7xg4fJInNkann50m5zlM76uw' );

class Notifikasi_tracking extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }

  public function send_user($title,$desc,$id_fcm)
  {   
    $Msg = array(
      'body' => $desc,
      'title' => $title
    );
    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg
      // 'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    echo $result . "\n\n";
  }

    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
        $tracking = $this->post('track_id');
      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {
            $data = array(
              "title" => $title,
              "description" => $desc,
              "fcm_id" => $mem->fcm_id,
              );
            $get_htrans = $this->mymodel->withquery("select * from htrans where track_id = '".$tracking."'",'result');
            foreach ($get_htrans as $key => $value) {
              $get_stats = $this->mymodel->getbywhere('transaction_status','transaction_status_id
                ',$value->transaction_status_id,'row')->description_in_indonesia;
            $title = "Status Pengiriman Diperbarui";
            $desc = "Pengiriman dengan Nomor Resi ".$value->resi." ".$get_stats;
            $this->send_user($title,$desc,$mem->fcm_id);
            }

            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil Kirim Notifikasi' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Gagal Cek Data' ,'data'=>array());
            }

          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}
