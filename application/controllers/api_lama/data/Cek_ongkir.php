<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Cek_ongkir extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }

    public function rajaongkir($asal,$destinasi,$jenis1,$jenis2,$kurir,$berat){
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=".$asal."&originType=".$jenis1."&destination=".$destinasi."&destinationType=".$jenis2."&weight=".$berat."&courier=".$kurir,
        CURLOPT_HTTPHEADER => array(
          "content-type: application/x-www-form-urlencoded",
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = array();
        foreach (json_decode($response) as $key => $value) {
          return $value->results;
        }
      }
    }
    public function rajaongkir_dimensi($asal,$destinasi,$jenis1,$jenis2,$kurir,$berat,$panjang,$lebar,$tinggi){
      $kurir = "pos";
      $jsonData = array(
      'origin' => $asal,
      'originType' => $jenis1,
      'destination' => $destinasi,
      'destinationType' => $jenis2,
      'weight' => $berat,
      'length' => $panjang,
      'width' => $lebar,
      'height' => $tinggi,
      'courier' => $kurir
      );
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=".$asal."&originType=".$jenis1."&destination=".$destinasi."&destinationType=".$jenis2."&weight=".$berat."&length=".$panjang."&width=".$lebar."&height=".$tinggi."&courier=".$kurir,
        CURLOPT_HTTPHEADER => array(
          "content-type: application/x-www-form-urlencoded",
          "key: 2c1751498d124e1e27ff06ebbaf9923f"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $ubah = array();
        foreach (json_decode($response) as $key => $value) {
          return $value->results;
        }
      }
    }

    public function rajaongkir_json($asal,$destinasi,$jenis1,$jenis2,$kurir,$berat,$panjang,$lebar,$tinggi){

      $url = "https://pro.rajaongkir.com/api/cost";
 
      //Initiate cURL.
      $curl = curl_init($url);
       
      //The JSON data.
      $jsonData = array(
      'origin' => $asal,
      'originType' => $jenis1,
      'destination' => $destinasi,
      'destinationType' => $jenis2,
      'weight' => $berat,
      'length' => $panjang,
      'width' => $lebar,
      'height' => $tinggi,
      'courier' => $kurir
      );
       
      //Encode the array into JSON.
      $jsonDataEncoded = json_encode($jsonData);
       
      //Tell cURL that we want to send a POST request.
      curl_setopt($curl, CURLOPT_POST, 1);
       
      //Attach our encoded JSON string to the POST fields.
      curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonDataEncoded);
       
      //Set the content type to application/json
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"key: 2c1751498d124e1e27ff06ebbaf9923f")); 
       
      //Execute the request
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $response = json_decode($response);
        echo $response;
      }
    }

    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $kurir = $this->post('courier');
          $asal = "";$destinasi = "";
          if (!empty($this->post('asal_id'))) {
            $asal = $this->post('asal_id');
          }else{
            $asal = $this->mymodel->withquery("select * from company_address order by company_address_id ASC ",'result');
            foreach ($asal as $key => $value) {
              if (empty($value->subdistrict_id)) {
                $asal = $value->city_id;
              }else{
                $asal = $value->subdistrict_id;
              }
            }
          }

          if (!empty($this->post('member_address_id'))) {
            $destinasi = $this->mymodel->getbywhere('member_address','member_address_id',$this->post('member_address_id'),'row');
              if (empty($destinasi->subdistrict_id)) {
                $destinasi = $destinasi->city_id;
              }else{
                $destinasi = $destinasi->subdistrict_id;
              }
          }else{
            $destinasi = $this->mymodel->getbywheresort('member_address','member_id',$mem->member_id,'member_address_id','ASC');
            foreach ($destinasi as $key => $value) {
              if (empty($value->subdistrict_id)) {
                $destinasi = $value->city_id;
              }else{
                $destinasi = $value->subdistrict_id;
              }
            }
          }
          $cost =0;
          $cart = array();
          $berat=0;$panjang=0;$lebar=0;$tinggi=0;
          $member_alamat = $destinasi;

          $get_cart = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
              foreach ($get_cart as $key => $value) {
                $get_spec = $this->mymodel->getbywhere('product_specification',"description='berat' or description='Berat' and product_id=",$value->product_id,'row');
                $get_spek = 1000;
                if (!empty($get_spec->value)) {
                  $get_spek = $get_spec->value;
                }
                $hitung = $get_spek * $value->quantity;
                $berat = $berat + $hitung;
              }
          
          if (isset($mem)) {
            /*foreach ($get_spec as $key => $value) {
              if ($value->description == "panjang") {
                $panjang = $value->value;
              }
              if ($value->description == "lebar") {
                $lebar = $value->value;
              }
              if ($value->description == "tinggi") {
                $tinggi = $value->value;
              }
            }*/
          $jenis1;$jenis2;
            $cek = strlen($asal);
            if ($cek==2) {
              $jenis1 = "city";
            }else{
              $jenis1 = "subdistrict";
            }
            $cek2 = strlen($destinasi);
            if ($cek==2) {
              $jenis2 = "city";
            }else{
              $jenis2 = "subdistrict";
            }

            $data = array(
              "name" => "",
              "service" => "",
              "description" => "",
              "total_berat" => $berat." gram",
              "total_ongkir" => "",
              "estimasi" => ""
              );
            if (empty($this->mymodel->getbywhere("courier","courier_name",$kurir,"row"))) {
              $get_kurir = $this->mymodel->getbywhere("jasa_pengiriman","nama",$kurir,"row");
              $get_member_alamat = $this->mymodel->getbywhere("member_address","member_address_id",$this->post('member_address_id'),"row");
              $get_harga = $this->mymodel->getbywhere("ongkir_setharga","kecamatan_id",$get_member_alamat->subdistrict_id,"row");
              if (!empty($get_harga)) {
                $value->harga_pengiriman = $get_harga->harga_pengiriman*$berat;
                $value->estimasi = $get_harga->estimasi." Hari";
              }
              else{
                $jasa_pengiriman = array();
              }
              /*$get_data = array(
                "service" => $get_kurir->nama,
                "description" => "Pengiriman oleh Kurir Khusus JBD",
                "cost" => array(
                  "value" => $hitung_biaya,
                  "etd" => $get_biaya->estimasi,
                  "note" => ""
                ),
              );*/
              $get_data[] = new stdClass();
              $get_data[0]->service = $get_kurir->nama;
              $get_data[0]->description = "Pengiriman oleh Kurir Khusus JBD";
              $get_data[0]->cost[] = new stdClass();
              $get_data[0]->cost[0]->value = $hitung_biaya;
              $get_data[0]->cost[0]->etd = $get_biaya->estimasi;
              $get_data[0]->cost[0]->note = "";
            }
            else{
              $get_data = $this->rajaongkir($asal,$destinasi,$jenis1,$jenis2,$kurir,$berat);
              foreach ($get_data as $key => $value) {
                $get_data = $value->costs;
                $data['name'] = $value->name;
                //$get_data['name'] = $value->name;
              }
              foreach ($get_data as $key => $value) {
                $get_data2 = $value->cost;
                $data['service'] = $value->service;
                $data['description'] = $value->description;
                foreach ($get_data2 as $key => $value2) {
                  if ($value2->etd == null) {
                   $data['estimasi'] = "Tidak ada"; 
                  }else{
                    $data['estimasi'] = $value2->etd." Hari";
                  }
                  $data['total_ongkir'] = "Rp ".number_format($value2->value,0,"",".");
                }
              }
            }
            //var_dump($get_data);
            if (!empty($get_spec) && !empty($get_cart)) {              
              $msg = array('status' => 1, 'message'=>'Berhasil Cek Ongkos Kirim' ,'data'=>$get_data);
            }else if(empty($get_cart)){
              $msg = array('status' => 0, 'message'=>'Data Keranjang Kosong' ,'data'=>array());
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            } 
              
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }
}