<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Produk_rating extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $htrans_id = $this->post('htrans_id');
          $product_id = $this->post('product_id');
          $rate = $this->post('rate');
          $review = $this->post('review');

          if (isset($mem)) {
          
            $data = array(
              "product_ratting_id" => "",
              "product_id" => $product_id,
              "rate" => $rate,
              "review" => $review,
              "htrans_id" => $htrans_id,
              "created_at" => date('Y-m-d H:i:s'),
              "is_deleted" => 0 
              );
            
            if (!empty($data)) {
              $this->mymodel->insert('product_ratting',$data);
              $msg = array('status' => 1, 'message'=>'Berhasil Insert data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}