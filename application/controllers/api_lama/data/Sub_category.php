<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Sub_category extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {

      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
        $id_category = $this->get('category_id');
        $data = $this->mymodel->withquery("select * from category_sub where is_deleted='0' and category_id = '".$id_category."'",'result');
          foreach ($data as $key => $value) {
              if ($value->updated_at == null) {
                $value->updated_at ="";
              }
              $value->icon_file = base_url('assets/img/category_sub/'.$value->icon_file);
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
    }
}