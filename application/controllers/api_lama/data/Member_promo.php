<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Member_promo extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $voucher_code = $this->post('code');
          $get_voucher_id = $this->mymodel->getbywhere('voucher','code',$voucher_code,'row');
          $cek_kode = $this->mymodel->getbywhere('member_voucher',"member_id='".$mem->member_id."' and voucher_id=",$get_voucher_id->voucher_id,'result');

          if (isset($mem)) {
            if (count($get_voucher_id)==0) {
              $msg = array('status' => 0, 'message'=>'Voucher tidak ditemukan ','data'=>array());
            }else{
              if (empty($cek_kode)) {
                  $data = array(
                  "member_voucher_id" => "",
                  "member_id" => $mem->member_id,
                  "voucher_id" => $get_voucher_id->voucher_id,
                  "expired_on" => "",
                  "created_at" => date('Y-m-d H:i:s'),
                  "is_deleted" => 0 
                  );
                
                if (!empty($data)) {
                  $this->mymodel->insert('member_voucher',$data);
                  $data['discount'] = "Rp ".number_format($get_voucher_id->discount,0,"",".");
                  
                  $msg = array('status' => 1, 'message'=>'Berhasil memakai voucher' ,'data'=>$data);
                }else {
                  $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
                } 
              }else{
                $msg = array('status' => 0, 'message'=>'Voucher telah terpakai ','data'=>array());
              }
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','data'=>array());
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','data'=>array());
        $this->response($msg);
      }
    }
}