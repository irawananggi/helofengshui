<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
// require_once('sms/api_sms_class_reguler_json.php');
// require 'phpmailer/PHPMailerAutoload.php';
//ob_start();

class Login_google extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }

    function index_post() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        $email = $this->post('email');

		if ($email == null || $email == '') {
            $this->response(array('status' => 'failed', 'message' => 'Email must be filled!'));
        } else {
            $dataUser = $this->mymodel->getbywhere('member',"(email='$email' or phone='$email') AND is_deleted = 0",'','row');
            // $this->response(array('token' => $dataUser, 'email' => $email));

            if ($dataUser == null) {
                $this->response(array('status' => 'failed'));
            } else {
                $this->session->set_userdata('token',$dataUser->token);
                $this->session->set_userdata('member_id',$dataUser->member_id);
                $this->response(array('status' => 'success'));
            }
        }
    }

}
