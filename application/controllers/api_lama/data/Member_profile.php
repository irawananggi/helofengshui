<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Member_profile extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            $data = $this->mymodel->getbywhere('member','member_id',$mem->member_id,'result');
            foreach ($data as $key => $value) {
              if ($value->updated_at==null) {
                $value->updated_at = "";
              }
              if ($value->tgl_lahir==null) {
                $value->tgl_lahir = "";
              }
              $value->img_file = base_url('assets/img/member/'.$value->img_file);
              $value->total_wishlist = $this->mymodel->withquery("select COUNT(member_id) as total from wishlist where member_id='".$value->member_id."'","row")->total;
              $get_poin = $this->mymodel->getbywhere('histori_poin','member_id',$mem->member_id,'result');
              $total_poin = 0;
              foreach ($get_poin as $key2 => $value2) {
                if ($value2->status_poin == "didapatkan") {
                  $total_poin = $total_poin + $value2->poin;
                }
                else if($get_poin == "digunakan"){
                  $total_poin = $total_poin - $value2->poin;
                }
              }
              $value->total_poin = $total_poin;
              if (!empty($this->mymodel->getbywhere("member_discount","is_deleted='0' and member_id=",$mem->member_id,"row"))) {
                $value->member_discount = $this->mymodel->getbywhere("member_discount","is_deleted='0' and member_id=",$mem->member_id,"row");
              }
              else{
                $value->member_discount = new stdClass();
              }

              $value->province_name = $this->get_province($value->province_id)->name;
              $value->city_name = $this->get_city($value->province_id,$value->city_id)->name;
              $value->subdistrict_name = $this->get_subdistrict($value->city_id)->name;
              //$value->total_review = $this->mymodel->withquery("select COUNT(member_id) as total from product_ratting where member_id='".$value->member_id."'","row")->total;
            }
            
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else if (empty($data)) {
             $msg = array('status' => 1, 'message'=>'Alamat Tidak ditemukan!' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }

    public function get_province($id){
      $data = $this->mymodel->getbywhere("provinces",'id',$id,"row");
      return $data;
    }

    public function get_city($province_id,$city_id){
      $data = $this->mymodel->getbywhere("regencies",'id',$city_id,"row");
      return $data;
    }

    public function get_subdistrict($id){
      $data = $this->mymodel->getbywhere("districts",'id',$id,"row");
      return $data;

    }
}