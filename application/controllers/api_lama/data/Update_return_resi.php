<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Update_return_resi extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    // insert new data to account
    function index_post() {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (!empty($mem)) {
            $id_return = $this->post('id_return');
            $resi = $this->post('resi');
            $kurir = $this->post('kurir');
            $data = array(
              "resi" => $resi,
              "updated_at" => date("Y-m-d H:i:s"),
              "shipping_at" => date("Y-m-d H:i:s"),
              "kurir" => $kurir
            );
              if (!empty($data)) {
                $this->mymodel->update("return_transaksi",$data,"id",$id_return);
                $msg = array('status'=>1,'message'=>'Berhasil ubah data');
              } else {
                  $msg = array('status'=>0,'message'=>'Gagal ubah data');
              }
          }else {
              $msg = array('status' => 1, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

    }

}
?>
