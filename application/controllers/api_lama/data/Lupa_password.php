<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require 'phpmailer/PHPMailerAutoload.php';
ob_start();
class Lupa_password extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    // insert new data to account
    function index_post() {
       $email = $this->post('email');
       if (!empty($email)) {
         $cekemail = $this->mymodel->getbywhere('member','email',$email,'row');
         if (!empty($cekemail)) {
           $this->send_email_file("",$cekemail->email,$cekemail->token);
           $msg = array('status' => 1, 'message'=>'Permintaan Lupa Password terkirim, silahkan cek email tersebut' );
         }else {
           $msg = array('status' => 0, 'message'=>'Email Tidak Terdaftar' );
         }
       }else {
         $msg = array('status' => 0, 'message'=>'Email tidak boleh kosong' );
       }
      $this->response($msg);
    }

    public function send_email_file($file="",$to='',$data)
    {
      $to = urldecode($to);
      $mail = new PHPMailer;
      // Konfigurasi SMTP
      $mail->isSMTP();
      $mail->SMTPDebug =0;
      // $mail->Host = 'mail.namagz.com';
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPOptions = array(
         'ssl' => array(
           'verify_peer' => false,
           'verify_peer_name' => false,
           'allow_self_signed' => true
          )
      );
      $mail->SMTPAuth = true;
      // $mail->Username = 'syauqi@namagz.com';
      // $mail->Password = 'koroko11';
      $mail->Username = 'dev.jakarta.bd@gmail.com';
      $mail->Password = 'JAKARTAbd2020';
      $mail->SMTPSecure = 'ssl';
      $mail->Port = 465;

      $mail->addReplyTo('no-reply@jakartabubbledrink.com', 'JBD Developer Team');
      $mail->setFrom('no-reply@jakartabubbledrink.com', 'JBD Developer Team');

      // Menambahkan penerima
      $mail->addAddress($to);

      // Menambahkan beberapa penerima
      //$mail->addAddress('penerima2@contoh.com');
      //$mail->addAddress('penerima3@contoh.com');

      // Menambahkan cc atau bcc
      //$mail->addCC('syauqiragi06@gmail.com@contoh.com');
      //$mail->addBCC('syauqiragi06@gmail.com@contoh.com');

      // Subjek email
      $mail->Subject = '[No Reply] Lupa Password JBD';

      // Mengatur format email ke HTML
      $mail->isHTML(true);

      // Konten/isi
       $data_['msg'] = "Silahkan melakukan perubahan password anda melalui link dibawah ini";
       $data_['code'] = "$data";
       $data_['title'] = "Lupa Password";
       $mailContent = $this->load->view('email_lupas',$data_,true);
      $mail->Body = $mailContent;
      // Menambahakn lampiran
        //$mail->addAttachment('assets/pdf/'.$file);
      //$mail->addAttachment('lmp/file2.png', 'nama-baru-file2.png'); //atur nama baru

      // Kirim email
      if(!$mail->send()){
        //  echo 'Pesan tidak dapat dikirim.';
        //  echo 'Mailer Error: ' . $mail->ErrorInfo;
      }else{
        //  echo 'Pesan telah terkirim ';
      }
    }

}
?>
