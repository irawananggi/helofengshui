<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Pencarian_produk extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
      $mem="";
      if ($token!='') {
        $mem = $this->mymodel->getbywhere('member','token',$token,"row");
      }
      $id_category=null;$id_sub_category=null;$kata=null;$filter=null;$minimum=null;$maximum=null;
      $hitung="";$total_rate="";$average_rate="";
      if ($this->post('category_id')!=null) {
        $id_category=$this->post('category_id');
      }
      if ($this->post('category_sub_id')!=null) {
        $id_sub_category=$this->post('category_sub_id');
      }
      if ($this->post('keyword')!=null) {
       $kata = $this->post('keyword'); 
      }
      if ($this->post('filter')!=null) {
        $filter=$this->post('filter');
      }
      if ($this->post('minimum')!=null) {
        $minimum = $this->post('minimum');
      }
      if ($this->post('maximum')!=null) {
        $maximum = $this->post('maximum');
      }

      $q = "select * from product ";
      if ($id_category!=null) {
        $q = $q."where is_deleted=0 and category_id = '".$id_category."'";
        if ($id_sub_category!=null) {
          $q = $q." and category_sub_id='".$id_sub_category."'";
        }
      }
      if ($kata!=null) {
        if ($mem!='') {
          $save_pencarian = array(
          "save_pencarian_id" => "",
          "member_id" => $mem->member_id,
          "keyword" => $kata
          );
          $this->mymodel->insert('save_pencarian',$save_pencarian);
        }
        if ($id_category!=null) {
          $q = $q."and product_name like '%".$kata."%'";
        }else{
          $q = $q."where is_deleted=0 and product_name like '%".$kata."%'";
        }
      }
      if ($minimum != null) {
        if ($id_category != null || $kata != null) {
          $q = $q."and price >= '".$minimum."'";
        }else{
          $q = $q."where is_deleted=0 and price >= '".$minimum."'";
        }
      }
      if ($maximum != null) {
        if ($id_category != null || $kata != null || $maximum != null) {
          $q = $q."and price <= '".$maximum."'";
        }else{
          $q = $q."where is_deleted=0 and price <= '".$maximum."'";
        }
      }
      if ($filter!=0 || $filter!=null) {
        if ($filter==1) {
          $q = $q." order by created_at desc";
        }else if($filter==2){
          if ($id_category!=null && $kata==null) {
            $q = "select p.*,COUNT(t.product_id) as hitung from product p, dtrans t 
            where p.is_deleted=0 and p.product_id = t.product_id 
            and category_id ='".$id_category."'
            GROUP BY product_id order by hitung desc";
          }else if ($id_category==null && $kata!=null) {
            $q = "select p.*,COUNT(t.product_id) as hitung from product p, dtrans t 
            where p.is_deleted=0 and p.product_id = t.product_id 
            and product_name like '%".$kata."%'
            GROUP BY product_id order by hitung desc";
          }else{
            $q = "select p.*,COUNT(t.product_id) as hitung from product p, dtrans t 
          where p.is_deleted=0 and p.product_id = t.product_id 
          GROUP BY product_id order by hitung desc";
          }
        }else if ($filter==3) {
          $q = $q." order by price asc";
        }else if ($filter==4) {
          $q = $q." order by price desc";
        }

      }
      
      $data = $this->mymodel->withquery($q,'result');

      foreach ($data as $key => $value) {
        $total_trans = $this->mymodel->withquery("select COUNT(product_id) as total_transaksi from dtrans 
              where product_id = '".$value->product_id."'","result");
        $total_review = $this->mymodel->withquery("select COUNT(product_id) as total_review from product_ratting 
              where product_id = '".$value->product_id."'","result");
        $average_rate = $this->mymodel->withquery("select AVG(rate) as average_rate from product_ratting 
              where product_id = '".$value->product_id."'","result");
        foreach ($total_trans as $key => $valuee) {
          $value->hitung = $valuee->total_transaksi;
        }
        foreach ($average_rate as $key => $valuee) {
          $value->average_rate = round($valuee->average_rate);
        }
        foreach ($total_review as $key => $valuee) {
          $value->total_rate = $valuee->total_review;
        }
        $get_icon = $this->mymodel->getbywhere('product_image','product_id',$value->product_id,'result');
            foreach ($get_icon as $key => $valueee) {
              $value->img = $valueee->img_file;
            }
            if (empty($value->img)) {
              $value->img ="";
            }else{
              $value->img = base_url('assets/img/product/'.$value->img);
            }
            if ($value->updated_at==null) {
            $value->updated_at = "";
            }
        if ($hitung==null) {
          $hitung = 0;
        }
        if ($total_rate==null) {
          $total_rate = 0;
        }
        if ($average_rate==null) {
          $average_rate = 0;
        }
       
        $value->price = "Rp ".number_format($value->price,0,"",".");
        if (!empty($mem)) {
          $cek_wishlist = $this->mymodel->getbywhere('wishlist',"product_id='".$value->product_id."' and member_id=",$mem->member_id,'row');
          if (empty($cek_wishlist)) {
            $value->is_wishlist = 0;
          }else{
            $value->is_wishlist = 1;
          }
        }else{
          $value->is_wishlist = 0;
        }
      }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
    }
}