<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Product_ulasan extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
            $product_id = $this->get('product_id');
            $data = $this->mymodel->getbywhere('product_ratting',"is_deleted=0 and htrans_id !='0' and product_id=",$product_id,'result');
            $nama_lengkap;
            $foto;
            foreach ($data as $key => $value) {
              $get_htrans = $this->mymodel->getbywhere('htrans','htrans_id',$value->htrans_id,'row');
              $get_member = $this->mymodel->getbywhere('member','member_id',$get_htrans->member_id,'result');
              foreach ($get_member as $key => $valuee) {
                $nama_lengkap = $valuee->first_name." ".$valuee->last_name;
                $foto = base_url('assets/img/member/'.$valuee->img_file);
              }
            }

            foreach ($data as $key => $value) {
              if ($value->updated_at == null) {
                $value->updated_at ="";
              }
              $value->nama_lengkap = $nama_lengkap;
              $value->foto_profile = $foto;
            }
            if (!empty($data)) {
              $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }

          $this->response($msg);
    }
}