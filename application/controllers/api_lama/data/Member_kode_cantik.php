<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Member_kode_cantik extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          if (isset($mem)) {

            /*$data = $this->mymodel->getbywhere('kode_cantik',"member_id",$mem->member_id,'row');
            foreach ($data as $key => $value) {
              if ($value->updated_at==null) {
                $value->updated_at = "";
              }
              $value->member_name = $mem->first_name;
            }*/
            $kode = rand(100,999);
            $cek = $this->mymodel->getbywhere('kode_cantik',"member_id='".$mem->member_id."' and kode=",$kode,'row');
            $data = array("kode"=>$kode);
            if (empty($cek)) {
              //$this->mymodel->insert('kode_cantik',$data);
              $msg = array('status' => 1, 'message'=>'Berhasil Mendapatkan Kode' ,'data'=>$data);
            }else {
              $msg = array('status' => 0, 'message'=>'Gagal Mendapatkan Kode, Silahkan ambil kode ulang' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}