<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Verifikasi_member extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          $kode = $this->post('kode_verifikasi');
          $message="";
          if (isset($mem)) {
            $cek = $this->mymodel->getbywhere('verification_code',"code='$kode' and member_id=",$mem->member_id,'row');
              if (!empty($cek)) {
                $data = array(
                  "is_active" => 1
                );
                $this->mymodel->update('member',$data,'token',$mem->token);
                $stats = 1;
                $message = "Berhasil Verifikasi Akun";
              }else{
                $stats=0;
                $message = "Kode Verifikasi Salah";
              }
              $msg = array('status' => $stats, 'message'=>$message);
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}