<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Chat_detail extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];
          
            if (!empty($token)) {
              $member = $this->mymodel->getbywhere("member","token",$token,"row");
              $product_id = $this->get("product_id");
              $data = $this->mymodel->getbywhere("chat","product_id='".$product_id."' and member_id=",$member->member_id,"result");
              
              if (!empty($data)) {
                foreach ($data as $key => $value) {
                  $get_product = $this->mymodel->getbywhere("product","product_id",$value->product_id,"row");
                  $value->product_name = $get_product->product_name;
                  if ($value->galery_type == "0") {
                    $value->galery_type = "Chat";
                  }
                  else{
                    $value->galery_type = "File";
                  }
                  if ($value->member_is_sender == "1") {
                    $value->member_is_sender = true;
                  }
                  else{
                    $value->member_is_sender = false;
                  }
                  $value->nama_lengkap = $member->first_name;
                }
                $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data'=>$data);
                $status="200";
              }
              else{
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
                $status="422";
              }
            }else{
                $msg = array('status' => 0, 'message'=>'Token anda kosong' ,'data'=>array());
                $status="422";
            }

        $this->response($msg);//$this->response($msg,$status);
    }
}
