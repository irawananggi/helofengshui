<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Minus_by_qty extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $mem = $this->mymodel->getbywhere('member','token',$token,"row");
          //$id_cart = $this->post('cart_id');
          $product_id = $this->post('product_id');
          if (empty($this->post('variant_id'))) {
            $variant_id = "0";
          }
          else{
            $variant_id = $this->post('variant_id');
          }
          if (!empty($mem)) {
            $grand=0;
            $get_keranjang = $this->mymodel->withquery("select * from member_cart where member_id='".$mem->member_id."'
              and product_id='".$product_id."' and product_variants_id='".$variant_id."'",'result');
            $qty =0;
            foreach ($get_keranjang as $key => $value) {
              if ($value->quantity > 1) {
                $qty = $value->quantity-1;
              }
              else if ($value->quantity == 0) {
                $qty = $value->quantity-1;
              }
            }
            $data = array(
              "quantity" => $qty
              );
            if (!empty($data) && !empty($get_keranjang)) {
              foreach ($get_keranjang as $key => $value) {
                if ($qty <= 0) {
                  $this->mymodel->delete("member_cart","member_id='".$mem->member_id."' and product_variants_id = '".$value->product_variants_id."' and product_id=",$value->product_id);
                }else{
                  $this->mymodel->update('member_cart',$data,"member_id='".$mem->member_id."' and product_variants_id = '".$value->product_variants_id."' and product_id=",$value->product_id);
                }
              }
              //get grand
              $get_cart = $this->mymodel->getbywhere('member_cart','member_id',$mem->member_id,'result');
              if (!empty($get_cart)) {
                foreach ($get_cart as $key => $value) {
                  $get_product = $this->mymodel->getbywhere('product','product_id',$value->product_id,'row');
                  $h = $get_product->price*$value->quantity;
                  $grand = $grand + $h;
                }
                $grand = "Rp ".number_format($grand,0,"",".");
                $msg = array('status' => 1, 'message'=>'Berhasil Ubah Data','grand_total'=>$grand,'data'=>$data); 
              }
              else{
                $grand = "Rp 0";
                $msg = array('status' => 1, 'message'=>'Berhasil Hapus Data, Keranjang Kosong','grand_total'=>$grand,'data'=>array()); 
              }
            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}