<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Detail_histori extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }       
          $id = $this->get('id');
          $data = $this->mymodel->getbywhere('permintaan','id',$id,'result');
          foreach ($data as $key => $value) {
            
            $get_kategori = $this->mymodel->getbywhere('kategori','id',$value->kategori_id,'row');



            $value->foto_kategori = base_url("uploads/file/".$get_kategori->foto);
            $value->name_in = $get_kategori->name_in;
            $value->name_en = $get_kategori->name_en;
            $value->name_man = $get_kategori->name_man;
            $value->deskripsi_in = $get_kategori->deskripsi_in;
            $value->deskripsi_en = $get_kategori->deskripsi_en;
            $value->deskripsi_man = $get_kategori->deskripsi_man;
            $value->ketentuan_in = $get_kategori->ketentuan_in;
            $value->ketentuan_en = $get_kategori->ketentuan_en;
            $value->ketentuan_man = $get_kategori->ketentuan_man;


            $value->detail_form = $this->mymodel->getbywhere('permintaan_detail','permintaan_id',$value->id,'result');

            foreach ($value->detail_form as $key3 => $value3) {

              $form_kategori_2 = $this->mymodel->getbywhere('form_kategori','id',$value3->form_kategori_id,'row');
              
              $get_jenis_form = $this->mymodel->getbywhere('jenis_form','id',$form_kategori_2->jenis_form_id,'row');

              $value3->name_jf = $form_kategori_2->name_cat_in;
            }

          }
          if (!empty($data)) {
            $msg = array('status' => 1, 'message'=>'Berhasil ambil data' ,'data'=>$data);
          }else {
            $msg = array('status' => 0, 'message'=>'Data tidak ditemukan' ,'data'=>array());
          }


          $this->response($msg);

    }
}
