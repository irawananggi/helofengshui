<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
	var $dir = 'Blog';
	var $app = 0;


	function __construct() {
	
		parent::__construct();
		
	}

	public function all($search=NULL, $offset=NULL) {
		

		$data['vals'] = $this->general_model->get_param(array('bahasa'),2);
		$vals = $this->general_model->get_param(array('bahasa'),2);
		$offset = !empty($offset) ? $offset : null;
		$fcari = null;
		$search_key = $this->input->post('key');
		if (!empty($search_key)) {
			$fcari = array(
				'judul' 		=> $search_key
			);
			$data['for_search'] = $fcari['judul'];
		} else if ($search) {
			$fcari=un_de($search);
			$data['for_search'] = $fcari['judul'];
		}
		$from = array(
			'blog tj' => ''
		);
		$select = 'tj.*';
		$where = array();
		$config['base_url']	= site_url($this->dir.'/all/'.in_de($fcari));
		$config['total_rows'] = $this->general_model->datagrab(array('tabel' => $from, 'order'=>'tj.id DESC', 'select'=>$select, 'search'=>$fcari,'where'=>$where))->num_rows();
		/*cek($config['total_rows']);
		die();*/
		$data['total']	= $config['total_rows'];
		$config['per_page']		= '12';
		$config['uri_segment']	= '4';
		$this->load->library('pagination');
		//$this->load->library('table');
		$this->pagination->initialize($config);
		/*cek($this->pagination->create_links());
		die();*/
		$data['links'] = $this->pagination->create_links();
		//cek($data['links']);
		$lim = $config['per_page'];
		$offs = $offset;
		$st = get_stationer();	
		$dt1 = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select,'limit'=>1));
		$cek_id_last = $dt1->row();

		$dt = $this->general_model->datagrab(array('tabel'=>$from, 'order'=>'tj.id DESC','select'=>$select, 'search'=>$fcari,'where'=>$where, 'limit'=>$lim, 'offset'=>$offs,'where'=>array('id !='=>$cek_id_last->id)));
		



		$data['vals'] = $this->general_model->get_param(array('bahasa'),2);
		$vals = $this->general_model->get_param(array('bahasa'),2);




		if($dt1->num_rows() > 0){
			
			$no = 1;
			$rows1 = '';
			foreach ($dt1->result() as $row1) {
				$rows1 .= 
				'


       <div class="col-md-4">
            <img src="'.base_url('uploads/blog/').$row1->foto.'" class="img-fluid" alt="">
          </div>';

            if ($this->session->userdata('bahasa') == 'in') { 
            	$rows1 .= '
          <div class="col-md-7">
            <h2>'.$row1->judul.'</h2>
            <p class="blog-meta">'.konversi_tanggal("D, j M Y",substr($row1->created_at,0,10),"in").'</p>
            <p>
             '.substr($row1->deskripsi,0,150).' ...
            </p>
            <a href="'.base_url('Detail/blog/').$row1->permalink.'" class="read-more">Selengkapnya <i class="ico-arrow-left"></i></a>
          </div>

				'
				;
		        }elseif ($this->session->userdata('bahasa') == 'en') { 
		        	$rows1 .= '
          <div class="col-md-7">
            <h2>'.$row1->judul_en.'</h2>
            <p class="blog-meta">'.konversi_tanggal("D, j M Y",substr($row1->created_at,0,10),"en").'</p>
            <p>
             '.substr($row1->deskripsi_en,0,150).' ...
            </p>
            <a href="'.base_url('Detail/blog/').$row1->permalink.'" class="read-more">More details <i class="ico-arrow-left"></i></a>
          </div>

				'
				;
		       }else { 


		        	$rows1 .= '
          <div class="col-md-7">
            <h2>'.$row1->judul_man.'</h2>
            <p class="blog-meta">'.konversi_tanggal("D, j M Y",substr($row1->created_at,0,10),"man").'</p>
            <p>
             '.substr($row1->deskripsi_man,0,150).' ...
            </p>
            <a href="'.base_url('Detail/blog/').$row1->permalink.'" class="read-more">更多细节 <i class="ico-arrow-left"></i></a>
          </div>

				'
				;
		       }




						
				//$this->table->add_row($rows1);
				$no++;
			}
			$tabel1 = $rows1;
		}else{
			$tabel1 = '<div class="alert">Data masih kosong ...</div>';
		} 


		if($dt->num_rows() > 0){
			
			$no = 1;
			$rows = '';
			foreach ($dt->result() as $row) {
				$rows .= 
				'
          <div class="col-lg-3 col-md-6 mb-5">
            <a href="'.base_url('Detail/blog/').$row->permalink.'">
              <div class="card">
			  <div class="blog-img">
			  <img src="'.base_url('uploads/blog/').$row->foto.'" class="img-fluid" alt="">
			  </div>
                
                <div class="card-body">';
                	$data_s = strip_tags($row->deskripsi);
                	$data_ss = strip_tags($row->deskripsi_en);
                	$data_sss = strip_tags($row->deskripsi_man);
                	$ss = substr($data_s,0,50);
                	$sss = substr($data_ss,0,50);
                	$ssss = substr($data_sss,0,50);

	                if ($this->session->userdata('bahasa') == 'in') { 
					        		$rows .='<h5 class="card-title">'.$row->judul.'</h5>
			                <p class="card-text">'.$ss.' ...</p>';
					        }elseif ($this->session->userdata('bahasa') == 'en') { 
					       		$rows .='<h5 class="card-title">'.$row->judul_en.'</h5>
			              <p class="card-text">'.$sss.'  ...</p>';
					       }else { 
					        	$rows .='<h5 class="card-title">'.$row->judul_man.'</h5>
			              <p class="card-text">'.$ssss.' ...</p>';
					       }

               $rows .=  '</div>
              </div>
            </a>
          </div>';
				//$this->table->add_row($rows);
				$no++;
			}
			$tabel = $rows;
		}else{
			$tabel = '<div class="alert">Data masih kosong ...</div>';
		} 

		$from_link = array(
			'link tj' => '',
			'jenis_link b' => array('b.id=tj.jenis_link_id','left') 
		);
		$select = 'tj.*,b.*';
		$where1 = array('jenis_link_id'=>1);
		$where2 = array('jenis_link_id'=>2);
		$data['data_link_1'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where1));
		$data['data_link_2'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where2));

		$data['content_tp'] = $tabel1;
		$data['content'] = $tabel;



		$data['total_rows'] = $config['total_rows'];
		$data['pagination'] = $data['links'];


		$this->load->view('landing/all_blog', $data);
		

	}
	
}


