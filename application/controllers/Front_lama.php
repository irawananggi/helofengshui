<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	var $link = 'front/utama';
	var $folder = 'front';
	var $dir = 'front';
	function __construct() {
	
		parent::__construct();
		
	}

	public function index() {
		$this->init();
	}
	
	function init() {
		
		$data['folder'] = $this->folder;
		$this->load->view('fengshui/index',$data);
	}

}


