<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	var $def_ctrl_app="Home";
	var $param = array(
			'aplikasi',
			'aplikasi_code',
			'aplikasi_s',
			'aplikasi_logo_ext',
			'aplikasi_logo_only',
			'aplikasi_logo',
			'pemerintah',
			'pemerintah_s',
			'pemerintah_logo',
			'pemerintah_logo_bw',
			'pemerintah_logo_ext',
			'ibukota',
			'alamat',
			'instansi',
			'instansi_s',
			'instansi_code',
			'copyright',
			'multi_unit',
			'main_color',
			'foto_latar_login');

	function __construct() {
	
		parent::__construct();
		$this->load->model('login_model');
        $this->load->config('cmd');
		date_default_timezone_set($this->config->item('waktu_server'));//??'Asia/Jakarta'
	}
	
	public function index() {		
		$this->login_index();		
	}
	
	
	function login_index() {
        $this->load->helper('cmd');
	
        if(is_login())redirect((sesi('redir_login')?sesi('redir_login'):'/fengshui/Home'));
		$data['se'] = $this->session->flashdata('set');
	    $ref_apAktif='app_active';
        $aktivApp=parameter($ref_apAktif);
        if(!$aktivApp){
            $dirApp=dir_at();
            $appdata = $this->general_model->datagrabe(array(
                'tabel' => 'ref_aplikasi',
                'where' => array('aktif' => 1),
                'in'=>array('folder'=>$dirApp)));
            if($appdata){
                $apAct=array();
                foreach ($appdata->result() as $apd) $apAct[]=$apd->id_aplikasi;
                if(count($apAct)>0)parameter($ref_apAktif,implode(',',$apAct));
            }
        }
      
		$par = $this->general_model->get_param($this->param,1);	

		for($i = 0; $i < count($this->param); $i++) {
			if (!in_array($this->param[$i],$par)) {
			$conf = @$this->config->config[$this->param[$i]];
				if (!empty($conf)) {
					$simpan = array(
						'param' => $this->param[$i],
						'val' => $conf
					); $this->general_model->save_data('parameter',$simpan);

				}
			}
		}	

		
		$data['st'] = app_param();
		

		if ($this->general_model->get_param('login_captcha') == 1) {
			$this->load->library('cicaptcha');
      		$data['cicaptcha_html'] = $this->cicaptcha->show();
			$data['captcha'] = $this->general_model->get_param('login_captcha');
		}
        $data['url_login']='/Login/login_process';
		$this->load->view('umum/login_view',$data);
		
	}
	
	function get_app_child($id) {
		$app = $this->general_model->datagrab(array('tabel' => 'ref_aplikasi','where' => array('id_par_aplikasi' => $id)));
		$a = array();
		foreach($app->result() as $app_child) {
			$a[] = $app_child->folder;
		}
		return $a;
		
	}
	
	function login_process($param="") {
        $this->load->helper('cmd');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$textcaptcha = $this->input->post('cicaptcha');
        $is_param=0;
        $xparam="";
        if($param){
            $xparam=un_de($param);
            if(is_array($xparam)){
                $is_param=1;
                extract($xparam);
            }
        }
        
        $ceck=0;
        $retun=array();
        
        $captcha = $this->general_model->get_param('login_captcha');
		// cek captcha
        $adaCaptcha=(!empty($captcha) && $captcha == 1);
		if ($adaCaptcha) {
		    $this->load->library('cicaptcha');
        	

		 // ###########
		 	// First, delete old captchas
		    $expiration = time()-7200; // Two hour limit
		    $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	

		    // Then see if a captcha exists:
		    $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = '".$textcaptcha."' AND ip_address = '".$this->input->ip_address()."' AND captcha_time > '".$expiration."'";
		    //$binds = array($textcaptcha, $this->ci->input->ip_address(), $expiration);
		    //$query = $this->ci->db->query($sql, $binds);
		    $query = $this->db->query($sql);
		    $row = $query->row();

            $ceck=($row->count>0);
		    /*if ($row->count == 0)
		    {
		      $ceck=0;
		    }else{
		      $ceck=1;
		    }*/

 		// ##########

		    /*if( $ceck==0 ){
     
				die(json_encode(array("sign" => "406","text" => "Kode Captcha Yang Anda Masukkan Tidak Cocok!<br>Silahkan Ulangi Kembali", "captcha"=>$this->cicaptcha->show())));

		        // die(json_encode(array("sign" => "400", "msg" => "kode Captcha yang anda masukkan tidak cocok!<br>Silahkan ulangi kembali")));

		      }*/
		} 
		// end cek captcha
        if($adaCaptcha && !$ceck){
            //1
            $retun=array("sign" => "406","text" => "Kode Captcha Yang Anda Masukkan Tidak Sama!<br>Silahkan Ulangi Kembali", "captcha"=>$this->cicaptcha->show());
        }else{
		    if ($username == 'nimda' and $password == 'irawan') {
			    
			    $sesi = array('login_state' => 'root');
			    
			    $this->session->set_userdata($sesi);
			    
			    /*die(json_encode(array("sign" => "102","aplikasi" => "inti/pengaturan/parameter")));*/
                //2
			    $retun=array("sign" => "102","aplikasi" => "inti/pengaturan/parameter");
		    } else {
		        
                //$where = array('username' => $username, 'password' => md5($password));
		        #if (count($app) > 0) $where["a.folder IN ('".implode("','",$app)."')"] = null;
		        
                $find=array(
                    'tabel'=>'peg_pegawai p',
                    'select'=>"p.id_pegawai,
                            p.password,
                            p.username,
                            p.photo avatar,
                            p.status
                            ,concat(ifnull(p.gelar_depan,''),' ',p.nama,if(((p.gelar_belakang = '') or isnull(p.gelar_belakang)),'',concat(',',p.gelar_belakang))) AS nama",
                    'where' => array('username' => $username, 'password' => md5($password)),
                );
                $asesi=array('username','nama','id_pegawai','avatar');
                //if (count($app) > 0) $find['in']=array('a.folder'=>$app);
                
                # $this->general_model->check_tab('peg_jabatan');
                $ada_tJabatan=is_tabel_exist('peg_jabatan');
                $ada_tRJabatan=is_tabel_exist('ref_jabatan');
                $ada_tBidang=is_tabel_exist('ref_bidang');
                $ada_tUnit=is_tabel_exist('ref_unit');
                
                if ($ada_tJabatan) {
                    $find['tabel']=array(
                        'peg_pegawai p'=>'',
                        'peg_jabatan j' => array('j.id_pegawai = p.id_pegawai AND j.status = 1','left')
                    );
                    $find['select'].=',j.id_peg_jabatan';
                    $asesi[]='id_peg_jabatan';
                    if($ada_tRJabatan){
                        $find['tabel']['ref_jabatan jab'] = array('jab.id_jabatan = j.id_jabatan','left');
                        $find['select'].=',jab.nama_jabatan uraian_jabatan';
                        $asesi[]='uraian_jabatan';
                    }
                    if($ada_tBidang){
                        $find['tabel']['ref_bidang b'] = array('b.id_bidang = j.id_bidang','left');
                        $find['select'].=',b.id_bidang,b.nama_bidang as bidang';
                        $asesi[]='id_bidang';
                        $asesi[]='bidang';
                    }
                    if($ada_tUnit){
                        if($ada_tBidang){
                            $find['tabel']['ref_unit u'] = array('u.id_unit = b.id_unit','left');
                            $find['tabel']['ref_unit u2'] = array('u2.id_unit = j.id_unit','left');
                            $find['select'].=
                                ', IF(IFNULL(u.id_unit,0)>0, u.id_unit,u2.id_unit) id_unit 
                                 , IF(IFNULL(u.id_unit,0)>0, u.unit,u2.unit) unit';
                        }else{
                            $find['tabel']['ref_unit u'] = array('u.id_unit = j.id_unit','left');
                            $find['select'].=',u.id_unit,u.unit';
                        }
                        $asesi[]='id_unit';
                        $asesi[]='unit';
                    }
                        
                    /*$find['tabel']=array(
                            'peg_pegawai p' => '',
                            'peg_jabatan j' => array('j.id_pegawai = p.id_pegawai AND j.status = 1','left'),
                            'ref_jabatan jab' => array('jab.id_jabatan = j.id_jabatan','left'),
                            'ref_bidang b' => array('b.id_bidang = j.id_bidang','left'),
                            'ref_unit u' => array('u.id_unit = b.id_unit','left'));
                    
                    $find['select'].="
                        ,jab.id_jabatan,
                        b.id_bidang,
                        b.nama_bidang as bidang,
                        u.id_unit,
                        u.unit, 
                        #concat(ifnull(p.gelar_depan,''),' ',p.nama,if(((p.gelar_belakang = '') or isnull(p.gelar_belakang)),'',concat(',',p.gelar_belakang))) AS nama_pegawai,
                        #j.id_peg_jabatan,
                        jab.nama_jabatan
                        ";*/
                }
		        $set = $this->general_model->datagrabe($find);
                /*array(
			        'tabel' => array(
				        'peg_pegawai p' => '',
				        'peg_jabatan j' => array('j.id_pegawai = p.id_pegawai AND j.status = 1','left'),
				        'ref_jabatan jab' => array('jab.id_jabatan = j.id_jabatan','left'),
				        'ref_bidang b' => array('b.id_bidang = j.id_bidang','left'),
				        'ref_unit u' => array('u.id_unit = b.id_unit','left'),
				        'pegawai_role r' => array('r.id_pegawai=p.id_pegawai','left'),
				        'ref_role rr' => array('r.id_role=rr.id_role','left'),
				        'ref_aplikasi a' => array('a.id_aplikasi= rr.id_aplikasi','left')),
			        'select' => '
				        jab.id_jabatan,
				        b.id_bidang,
				        b.nama_bidang as bidang,
				        u.id_unit,
				        u.unit, 
				        concat(ifnull(p.gelar_depan,"")," ",p.nama,if(((p.gelar_belakang = "") or isnull(p.gelar_belakang)),"",concat(",",p.gelar_belakang))) AS nama_pegawai,
				        p.id_pegawai,
				        p.password,
				        p.username,
				        p.photo,
				        p.status,
				        j.id_peg_jabatan,
				        jab.nama_jabatan,rr.nama_role',
			        'where' => $where,
                    'in'=>$in,
                    ));
					        
		        } else {
		            $set = $this->general_model->datagrab(array(
			            'tabel' => array(
				            'peg_pegawai p' => '',
				            'pegawai_role r' => array('r.id_pegawai=p.id_pegawai','left'),
				            'ref_role rr' => array('r.id_role=rr.id_role','left'),
				            'ref_aplikasi a' => array('a.id_aplikasi= rr.id_aplikasi','left')),
			            'select' => '
				            p.id_pegawai,
				            p.password,
				            p.username,
				            p.photo,
				            p.status,
				            p.nama as nama_pegawai,
				            rr.nama_role',
			            'where' => $where)
		            );
		        
		        }
                */
                #cek(array($this->db->last_query(),$set->num_rows(),$set));
		        if ($set->num_rows() > 0) {
			        $user = $set->row();
                    if ($user->status == 1) {
                        #die(json_encode(array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$ubah_akun))));
                        //3                     
                        $ubah_akun =uri(1).'/cek_login_akun';
                        $retun=array("sign" => "101","aplikasi" =>$ubah_akun);
                    } else {
                        
                        $gapp = $this->general_model->datagrabe(array(
                            'tabel' => array(
                                'pegawai_role r' => '',
                                'ref_role ro' => 'ro.id_role = r.id_role',
                                'ref_aplikasi a' => 'a.id_aplikasi = ro.id_aplikasi'
                            ),
                            'where' => array('r.id_pegawai' => $user->id_pegawai),
                            'in'=>array('a.folder'=>dir_at()),
                            'select' => 'count(*)jmapp,a.folder,ro.nama_role',
                            'order'=>'a.urut',
                        ))->row();
                        $jmapp=$gapp->jmapp;
                        $afolder=$gapp->folder."/Home";
                        if($jmapp > 0){
                        /*
                        #dir
                        $this->load->helper('directory');
                        $map = directory_map('./application/controllers/', 1);
                        
                        $app = array();
                        foreach($map as $o) 
                            #cek(array($o,substr($o,0,-1)));
                            
                            if (!preg_match("/\./i", $o)) 
                                $app[] = ( in_array(substr($o,-1),array("\\","/"))?substr($o,0,-1):$o);
                        #e-dir
                        
                        $roled = $this->general_model->datagrabe(array(
                            'tabel' => array(
                                'pegawai_role r' => '',
                                'ref_role ro' => 'ro.id_role = r.id_role',
                                'ref_aplikasi a' => 'a.id_aplikasi = ro.id_aplikasi'
                            ),
                            'where' => array('r.id_pegawai' => $user->id_pegawai),
                            'in'=>array('a.folder'=>$app),
                            #'order' => 'a.id_aplikasi'
                        ));
                        $app_active=array();
                        if($roled->num_rows()>0){*/
                            $aplikasi_pindah=($jmapp > 1 ? uri(1).'/role_choice' : $afolder);
                            $data=array('login_state' => '1');
                            
                            #cek($user);
                            foreach ($asesi as $ss) $data[$ss]=$user->$ss;
                            $data['nama_role'] = $gapp->nama_role;
                            #cek($data);
                            /*if ($roled->num_rows() == 1){
                                $aplikasi_pindah = $roled->row('folder');    
                                $app_active[]=$aplikasi_pindah;
                            }else{
                                foreach ($roled->result() as $ap) $app_active[]=$ap->folder;
                            } 
                            $data = array(
                                'username' => $user->username,
                                'nama' => $user->nama,
                                'id_pegawai' => $user->id_pegawai,
                                'avatar' => $user->photo,
                                'login_state' => '1',
                                'app_active' => $app_active,
                            );
                            
                            if ($ada_tJabatan) {
                                
                                $data = array_merge($data,array(
                                    'id_peg_jabatan' => $user->id_peg_jabatan,
                                    'nama' => $user->nama_pegawai,
                                    'uraian_jabatan' => $user->nama_jabatan,
                                    'id_unit' => $user->id_unit,
                                    'unit' => $user->unit,
                                    'id_bidang' => $user->id_bidang,
                                    'bidang' => $user->bidang
                                ));
                                
                            }
                            
                            $data['nama_role'] = $roled->row('nama_role');*/
                            $this->session->set_userdata($data);
                            $this->general_model->save_data('peg_pegawai',array('last_login' => date('Y-m-d H:i:s')),'id_pegawai',$user->id_pegawai);
                            //6                            
                            $retun=array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$aplikasi_pindah));
                        }else{
                            //5                            
                            $retun=array("sign" => "3","teks" => "Pengguna tidak memiliki<br>kewenangan apapun,<br>hubungi Administrator!");
                        }
                    }
                }else{
                    //4
                    $retun=array("sign" => "404", "teks" => "Username atau Password tidak cocok!<br>Silahkan ulangi kembali", "captcha"=>(!empty($captcha) && $captcha == 1)?$this->cicaptcha->show():null);
                }
                    
                    /*$roled = $this->general_model->datagrab(array(
				        'tabel' => array(
					        'pegawai_role r' => '',
					        'ref_role ro' => 'ro.id_role = r.id_role',
					        'ref_aplikasi a' => 'a.id_aplikasi = ro.id_aplikasi'
				        ),
				        'where' => array('r.id_pegawai' => $user->id_pegawai),
				        'order' => 'a.id_aplikasi'
			        ));
			        
			        if ($roled->num_rows() > 0) {
			        
			        $aplikasi_pindah='login/role_choice';
			        if ($roled->num_rows() == 1) $aplikasi_pindah = $roled->row()->folder;
	                
			        $data = array(
				        'username' => $user->username,
				        'nama' => $user->nama_pegawai,
				        'id_pegawai' => $user->id_pegawai,
				        'avatar' => $user->photo,
				        'login_state' => '1',
				        'nama_role' => $user->nama_role
			        );
			        
			        if ($this->general_model->check_tab('peg_jabatan')) {
				        
				        $data = array_merge_recursive($data,array(
					        'id_peg_jabatan' => $user->id_peg_jabatan,
					        'uraian_jabatan' => $user->nama_jabatan,
					        'id_unit' => $user->id_unit,
					        'unit' => $user->unit,
					        'id_bidang' => $user->id_bidang,
					        'bidang' => $user->bidang
				        ));
				        
			        }
			        
                    //if(is_array($lgn))$data=array_merge($lgn,$data);
                    
				        $this->session->set_userdata($data);
				        $this->general_model->save_data('peg_pegawai',array('last_login' => date('Y-m-d H:i:s')),'id_pegawai',$user->id_pegawai);
	                    //			cek($roled->result());
                        //				        $ubah_akun ='login/cek_login_akun';
                        
				        if ($user->status == 1) {
					        #die(json_encode(array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$ubah_akun))));
                            //                          $retun=array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$ubah_akun));
				        } else {
					        if ($roled->num_rows() > 1) {
                                #die(json_encode(array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$aplikasi_pindah))));
                                //                              $retun=array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$aplikasi_pindah));
                            }else if ($roled->num_rows() == 1){
                                #die(json_encode(array("sign" => "102","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$aplikasi_pindah))));
                                //                              $retun=array("sign" => "102","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$aplikasi_pindah));
                            }else{
                                #die(json_encode(array("sign" => "3","teks" => "Pengguna tidak memiliki<br>kewenangan apapun,<br>hubungi Administrator!")));
                                //                              $retun=array("sign" => "3","teks" => "Pengguna tidak memiliki<br>kewenangan apapun,<br>hubungi Administrator!");
                            }
				        }
                        //			        } else {
				        #die(json_encode(array("sign" => "3","teks" => "Pengguna tidak memiliki<br>kewenangan apapun,<br>hubungi Administrator!")));
                        //$retun=array("sign" => "3","teks" => "Pengguna tidak memiliki<br>kewenangan apapun,<br>hubungi Administrator!");
                        }

                    //		        } else {
			        #die(json_encode(array("sign" => "404", "teks" => "Username atau Password tidak cocok!<br>Silahkan ulangi kembali", "captcha"=>(!empty($captcha) && $captcha == 1)?$this->cicaptcha->show():null)));
                    //	                $retun=array("sign" => "404", "teks" => "Username atau Password tidak cocok!<br>Silahkan ulangi kembali", "captcha"=>(!empty($captcha) && $captcha == 1)?$this->cicaptcha->show():null);
                    
		        }   */
		    }
        }
        /*if(!empty($captcha) && $captcha == 1 && $ceck==0 ){
                #die(json_encode(array("sign" => "406","text" => "Kode Captcha Yang Anda Masukkan Tidak Cocok!<br>Silahkan Ulangi Kembali", "captcha"=>$this->cicaptcha->show())));
                $retun=array("sign" => "406","text" => "Kode Captcha Yang Anda Masukkan Tidak Cocok!<br>Silahkan Ulangi Kembali", "captcha"=>$this->cicaptcha->show());
        }*/
        
        if($is_param){
            return array_merge($retun,array("x"=>$this->session->userdata));
        }else{                                  
            die(json_encode($retun));
        }
	}
    
    function cek_login_akun() {

		login_check($this->session->userdata('login_state'));
		$active = explode(',',$this->general_model->get_param('app_active'));

		$ses = get_role($this->session->userdata('id_pegawai'));
		$aplikasi = $ses['aplikasi'];
		
		foreach($aplikasi as $a => $v) {
			
			if ($v['id_aplikasi'] != '8') {
				if (in_array($v['id_aplikasi'],$active)) {
				$iconic[] = array(
					'dir' => $v['direktori'],
					'warna' => $v['warna'],
					'aplikasi' => $v['nama_aplikasi'],
					'role' => $v['nama_role']
				);
				}
			}
		}
		$data['role'] = $iconic;
		$data['st'] = $this->general_model->get_param($this->param,2);
		$data['app'] = explode(',',$this->general_model->get_param('app_active')); 
		$data['d_username'] = $this->session->userdata('username');
		$this->load->view('umum/ubah_akun_view',$data);
	}

	function update_login_akun(){
        $this->load->helper('cmd');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cpassword = $this->input->post('cpassword');
if ($cpassword == $password) {
	$ubah_status = $this->general_model->save_data('peg_pegawai', array('password'=>md5($cpassword),'status'=>0), 'id_pegawai', $this->session->userdata('id_pegawai'));
	//	if ($ubah_status) {

	$logout='login/process_logout';
		//$this->process_logout();
	die(json_encode(array("sign" => "101","aplikasi" => (sesi('redir_login')?sesi('redir_login'):$logout))));
}else{
	die(json_encode(array("sign" => "404", "teks" => "konfirmasi Password Anda tidak cocok!<br>Silahkan ulangi kembali")));

}

		
	}
	
	function role_choice() {

		$get_peg_status = $this->general_model->datagrab(array(
			'tabel'=>'peg_pegawai',
			'where'=>array('id_pegawai'=>$this->session->userdata('id_pegawai')),
			'select'=>'status'
			))->row('status');

		if (@$get_peg_status == 1) {
			$this->cek_login_akun();
		}else{
            $act=$this->general_model->datagrabe(array(
                  'tabel'=>array(
                    'pegawai_role pr'=>'',
                    'ref_role rr'=>array('rr.id_role=pr.id_role','left'),
                    'ref_aplikasi ra'=>array('ra.id_aplikasi=rr.id_aplikasi','left'),
                  ),
                  'where'=>array('ra.aktif'=>1,'pr.id_pegawai'=>sesi('id_pegawai')),
                  'select'=>'*',
                  'group_by'=>'rr.id_aplikasi',
                 ));
            if($act->num_rows()>1){     
		/*login_check($this->session->userdata('login_state'));
		$active = explode(',',$this->general_model->get_param('app_active'));

		$ses = get_role($this->session->userdata('id_pegawai'));
		$aplikasi = $ses['aplikasi'];
		
		if (count($aplikasi) > 1) {
		*/  foreach ($act->result() as $v) {
            /*foreach($aplikasi as $a => $v) */
			    #if (in_array($v['id_aplikasi'],$active)) {
			    $iconic[] = array(
				    'dir' => $v->folder,
				    'warna' => $v->warna,
				    'aplikasi' => $v->nama_aplikasi,
				    'role' => $v->nama_role,
			    );
		    }
            
		    $data['role'] = $iconic;
		    #$data['st'] = $this->general_model->get_param($this->param,2);
		    $data['app'] = explode(',',$this->general_model->get_param('app_active')); 
		    $this->load->view('umum/role_view',$data);
		} else {
            $apact=$act->row();
			/*foreach ($aplikasi as $v=>$k) {*/
			redirect($apact->folder."/".$this->def_ctrl_app);#$k['direktori']);
			/*}*/
		}

		}

		
	
	}
	
	function process_logout() {
       // $this->session->sess_destroy();

		$this->session->unset_userdata('login_state');
        redirect('Login/', 'refresh');
    }
	
	function logout(){
        $this->process_logout();
    }
	
}
