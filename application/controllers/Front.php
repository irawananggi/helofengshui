<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	var $link = 'front/utama';
	var $folder = 'front';
	var $dir = 'front';
	function __construct() {
	
		parent::__construct();
		
    	$this->load->library('session');
    	$this->load->helper('url');
	}

	public function index() {
		$this->init();
	}
	
	function init() {
		
    	
		$data['vals'] = $this->general_model->get_param(array('bahasa'),2);
		$vals = $this->general_model->get_param(array('bahasa'),2);


		$from_link = array(
			'link tj' => '',
			'jenis_link b' => array('b.id=tj.jenis_link_id','left') 
		);
		$select = 'tj.*,b.*';
		$where1 = array('jenis_link_id'=>1);
		$where2 = array('jenis_link_id'=>2);
		$data['data_link_1'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where1));
		$data['data_link_2'] = $this->general_model->datagrab(array('tabel'=>$from_link,'where'=>$where2));


		$data['tentang_fengshui'] = $this->general_model->datagrab(array('tabel'=>'halaman','where'=>array('id'=>1)))->row();
		$data['fitur_fengshui'] = $this->general_model->datagrab(array('tabel'=>'halaman','where'=>array('id'=>2)))->row();
		$data['syarat_dan_ketentuan'] = $this->general_model->datagrab(array('tabel'=>'halaman','where'=>array('id'=>3)))->row();


		$data['testimoni'] = $this->general_model->datagrab(array('tabel'=>'testimoni','where'=>array('status'=>1)));

		$data['blog'] = $this->general_model->datagrab(array('tabel'=>'blog','limit'=>'4','order'=>'created_at DESC'));

	
		$data['folder'] = $this->folder;
		$data['sess'] = session_start();;
		$this->load->view('fengshui/index',$data);

	}
	
	function in($uri) {
		$arraydata = array(
            'bahasa'  => 'in'
    	);
    	$sss = $this->session->set_userdata($arraydata);
		$sesi = $this->session->userdata('bahasa');
	

		$p=un_de($uri);
		$uri1=$p['uri1'];
		$uri2=$p['uri2'];
		$uri3=$p['uri3'];

		if(!empty($uri1) AND !empty($uri2) AND empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2);
		}elseif(!empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2.'/'.$uri3);
		}else{
			$x = redirect(base_url());
		}
		return $x;


		/*$p=un_de($uri);
		$uri1=$p['uri1'];
		$uri2=$p['uri2'];
		$uri3=$p['uri3'];
		$par = array(
					'tabel'=>'parameter',
					'data'=>array(
						'val'=>'in',
						),
					);
		$par['where'] = array('param'=>'bahasa');
		$sim = $this->general_model->save_data($par);
		if(!empty($uri1) AND !empty($uri2) AND empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2);
		}elseif(!empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2.'/'.$uri3);
		}else{
			$x = redirect(base_url());
		}
		return $x;*/
	}
	function en($uri) {
		$arraydata = array(
            'bahasa'  => 'en'
    	);
    	$sss = $this->session->set_userdata($arraydata);
		$sesi = $this->session->userdata('bahasa');
	
	
		$p=un_de($uri);
		$uri1=$p['uri1'];
		$uri2=$p['uri2'];
		$uri3=$p['uri3'];

		if(!empty($uri1) AND !empty($uri2) AND empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2);
		}elseif(!empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2.'/'.$uri3);
		}else{
			$x = redirect(base_url());
		}
		return $x;
	}
	function cn($uri) {
		$arraydata = array(
            'bahasa'  => 'cn'
    	);
    	$sss = $this->session->set_userdata($arraydata);
		$sesi = $this->session->userdata('bahasa');
	
	
		$p=un_de($uri);
		$uri1=$p['uri1'];
		$uri2=$p['uri2'];
		$uri3=$p['uri3'];

		if(!empty($uri1) AND !empty($uri2) AND empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2);
		}elseif(!empty($uri3)){
			$x = redirect(base_url().$uri1.'/'.$uri2.'/'.$uri3);
		}else{
			$x = redirect(base_url());
		}
		return $x;
	}
}


