<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finish_midtrans extends CI_Controller {
	var $dir = 'Finish_midtrans';
	var $app = 0;


	function __construct() {
	
		parent::__construct();
		
	}

	public function index() {
		
		$order_id = $this->input->get('order_id');
		$where = array('order_id'=>$order_id);
		$data['data_mid'] = $this->general_model->datagrab(array('tabel'=>'permintaan','where'=>$where))->row();
		$this->load->view('landing/finish_midtrans', $data);
		

	}
	
}


