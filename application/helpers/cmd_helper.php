<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*  mr.febri@gmail.com
*  ci.213.314
*  v.0230
*/

//session
if ( ! function_exists('sesi')):
    function sesi($ses){
        $CI =& get_instance();
        return $CI->session->userdata($ses);
    }

    function setsesi($ses,$valu=null){
        $CI =& get_instance();
        if(is_array($ses)){
            $CI->session->set_userdata($ses);    
        }else{
            $CI->session->set_userdata($ses,$valu);
        }
    }

    function unsesi($ses){
        $CI =& get_instance();
        return $CI->session->unset_userdata($ses);
    }

    function flashsesi($ses){
        $CI =& get_instance();
        return $CI->session->flashdata($ses);
    }

    function setflashsesi($ses,$valu){
        $CI =& get_instance();
        $CI->session->set_flashdata($ses,$valu);
    }

    function islogin(){return sesi('login');}

    function is_login(){
        $CI =& get_instance();
        return sesi($CI->config->config['login']);
    }

endif;

function not_login($uri){
    if(!is_login()){
        if(!sesi('redir_login'))setsesi('redir_login',$uri);
        return true;
    }else{
        setsesi('redir_login','');return false;
    }
}

//post-get
if ( ! function_exists('post')):
/*function retgetpost($dt,$xss=false){
    $CI =& get_instance();
    return $CI->input->get_post($dt, $xss);
}*/

/*function retget($dt,$xss=false){
    $CI =& get_instance();
    return $CI->input->get($dt, $xss);
}*/

/*function retpost($dt=null,$xss=false){
    $CI =& get_instance();
    return $CI->input->post($dt, $xss);
}*/
function getpost($dt=null,$xss=false){
    $CI =& get_instance();
    return $CI->input->get_post($dt, $xss);
}

function get($dt=null,$xss=false){
    $CI =& get_instance();
    return $CI->input->get($dt, $xss);
}

function post($dt=null,$xss=false){
    $CI =& get_instance();
    return $CI->input->post($dt, $xss);
}
endif;

//htm .85
function ctag($tag,$isi,$attr=""){return "<$tag".($attr?" ".$attr:"").">".$isi."</$tag>";}
function div($isi,$attr=""){return ctag('div',$isi,$attr);}
function span($isi,$attr=""){return ctag('span',$isi,$attr);}
function tr($isi,$attr=""){return ctag('tr',$isi,$attr);}
function th($isi,$attr=""){return ctag('th',$isi,$attr);}
function th_($isi,$attr=""){return _tt('th',$isi,$attr);}
function td($isi='',$attr=""){return ctag('td',$isi,$attr);}
function td_($isi,$attr=""){return _tt('td',$isi,$attr);}
function _tt($td,$isi,$attr=""){
    $ret="";
    if(!is_array($isi))$isi=(array)$isi;
    foreach ($isi as $isi_) 
        if(is_array($isi_)){
            $ret.=ctag($td,$isi_[0],$isi_[1]);
        }else{
            $ret.=ctag($td,$isi_,$attr);
        }
        
    return $ret;
}
function table($isi,$attr=""){return ctag('table',$isi,$attr);}
function tag($tag,$isi,$attr=""){return ctag($tag,$isi,$attr);}
function a($isi,$attr=""){return ctag('a',$isi,$attr);}
if ( ! function_exists('ul')){
    function ul($isi,$attr=""){return ctag('ul',$isi,$attr);}
}
if ( ! function_exists('ol')){
    function ol($isi,$attr=""){return ctag('ol',$isi,$attr);}
}
function li($isi,$attr=""){return ctag('li',$isi,$attr);}
// fmt
function tdf($isi,$attr=""){return ctag('td',($isi?number_format($isi):0),$attr);}
function tdf2($isi,$attr=""){return ctag('td',($isi?number_format($isi,2):0),$attr);}
function tdf2i($isi,$attr=""){return ctag('td',($isi?number_format($isi,2,",","."):0),$attr);}
function tdfi($isi,$attr=""){return ctag('td',($isi?number_format($isi,0,",","."):0),$attr);}
function tdfi_rp($isi,$attr=""){return ctag('td',($isi?"Rp ".number_format($isi,0,",","."):0),$attr);}
function divhidden($dt) {return div(komen($dt),'style="display:none"');}

//load .2
if ( ! function_exists('show_loading')):
function show_loading($assets='assets/images/loading2.gif',$id='loading'){
    return span(span(' Sedang Proses, Tunggu ..','style="font-size:8pt;color:#000;"').span(img($assets)), 'id="'.$id.'"');
}
endif;

//boots .1
function row($isi,$class="",$attr=''){return div($isi,'class="row '.$class.'" '.$attr);}
function rowf($isi,$class="",$attr=''){return div($isi,'class="row-fluid '.$class.'" '.$attr);}

//meta .2
function reqjs($jsname){return ctag('script','','type="text/javascript" src="'.base_url().$jsname.'"');}
function reqcss($cssname){return '<link href="'.base_url($cssname).'" rel="stylesheet" type="text/css" />';}

//data .3
function tesnull(&$dt,$ret=null){return (isset($dt)?$dt:$ret);}

function isit(&$dt,$ret=null){return (isset($dt)?$dt:$ret);}


/*function wdt($dt,$ke,$r=0){
    $tx=".x.X.x.";
    if(is_numeric($ke)){
        return (tesnull($dt)&&tesnull($dt['td'][$r])&&tesnull($dt['th'][$ke])?$dt['td'][$r][$dt['th'][$ke]]:$tx);
    }else{
        return (tesnull($dt)&&tesnull($dt['td'][$r])?$dt['td'][$r][$ke]:$tx);
    }
}
*/

function unser($dt,$parm=0) {
    $a=unserialize($dt);
    if($parm){
        return tesnull($a[$parm]);
    }else{
        return $a;
    }
}


//edit 1.5
/**
* Preg_replace
*/
function chg2($txt,$codetxt,$code2){return preg_replace($codetxt,$code2,$txt);}

function chg2_($dt){ return chg2($dt,'/w+/','_');}
function chg_($dt){ return chg2($dt,'/_/',' ');}

/**
* ucword all
*/
function ucwlower($dt){return  ucwords(strtolower($dt));}

if ( ! function_exists('nfmt')){function nfmt($dt,$isdiv=0,$prepend=""){return ($isdiv? div($prepend.number_format($dt,0,',','.'),'align="right"'):$prepend.number_format($dt,0,',','.'));}}

//date .33
function set_time_id() { date_default_timezone_set('Asia/Jakarta');}

/**
* v:3
*/
function tgl2id($dt=0,$ty=0,$akhir_slice='-',$awal_slice='-'){
    if(!$dt){
        $ret='00-00-00';
    }else{
        $blnl=array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
        $td=explode($awal_slice,$dt);
        switch($ty){
            case 0: $ret=$td[2].$akhir_slice.$td[1].$akhir_slice.$td[0];break;
            case 1: $ret=intval($td[2])." ".$blnl[intval($td[1])]." ".$td[0];break;
            default:$ret=false;
        }
    }
    return $ret;
}

/**
*  split : '-' -> / 
*  d = 1 decimal
*/
function xtgl($dt,$split,$d=1){
    $pard=array(array("%s/%s/%s","%s-%s-%s"),array("%d/%d/%d","%d-%d-%d"));
    $edt=explode($split,$dt);
    if(3==count($edt)){
      return sprintf(('-'==$split?$pard[$d][0]:$pard[$d][1]),$edt[2],$edt[1],$edt[0]);  
    }else{
      return $dt;  
    }
    
}

function tgl2sql($dt){
    $edt=explode('/',$dt);
    if(count($edt)<3)$edt=explode('-',$dt);
    if(3==count($edt)){
      return sprintf('%d-%02d-%02d',$edt[2],$edt[1],$edt[0]);  
    }else{
      return $dt;  
    }
    
}

function ztgl2sql($dt) {
    $bln=array('JAN'=>'01','FEB'=>'02','MAR'=>'03','APR'=>'04','MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08','SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12');
    $etmp=preg_split("/ /", $dt);
//    $edt=explode('-',$etmp[0]);
    $edt=preg_split("/-/",$etmp[0]);
    return $edt[2]."-".$bln[$edt[1]]."-".$edt[0];
}

/**
* ret sgl date / time
*/
function tglsplit($dt,$time=0) {
    $etmp=preg_split("/ /", $dt);
    return $time ? $etmp[1]:$etmp[0];
}

/** oth .11
* 
* vd : var_dump
* debug = 1 
*/
function _cek($dt=null,$vd=0,$debug=1) {
    if($dt && $debug){
        echo "<pre>";
        if(!$vd){
            print_r($dt);
        }else{
            var_dump($dt);
        }
        echo "</pre>";
    }
}

function lz($dt,$num){
    return substr(repeater('0',$num).$dt,-$num);
}

function ada_file($filename) {
    $file_headers = get_headers($filename);
    return (strpos($file_headers[0], '404')!== false?false:true);
}

function _config($param=null){
    return $param ? CI()->config->config[$param]: CI()->config->config;
}

function komen($dt) {
    return "<!-- $dt -->";
}

function stop($dt=null,$vd=0){
    exit(_cek($dt,$vd));
}

function load_lib($app,$controller, $method = 'index',$param1 = null,$param2 = null,$param3 = null) {
        $path = FCPATH . APPPATH . 'libraries/'. $controller . '.php';
        if (file_exists($path)) {
            require_once($path);
            $controller = new $controller();

        return $controller->$method($param1,$param2,$param3);
        }
}

function load_cnt($app,$controller, $method = 'index',$param1 = null,$param2 = null,$param3 = null,$param4 = null,$param5 = null,$param6 = null) {
    //$path = FCPATH . APPPATH . 'controllers/'.$app.'/'. $controller . '.php';
    $path = APPPATH . 'controllers/'.($app=""?$controller . '.php':$app.'/'. $controller . '.php');
    if(preg_match('/\\\/i',FCPATH))$path=preg_replace('/\//i',"\\",$path);
    /*require_once($path);
    _cek(array(file_exists($path),FCPATH,APPPATH, $path,realpath($path),FCPATH,preg_match('/\//i',FCPATH)));*/
    if (file_exists($path)) {
        require_once($path);
        $controller = new $controller();
        return $controller->$method($param1,$param2,$param3,$param4,$param5,$param6);
    }
}

/*
* adminlte2 acordion 
* dt: array( array( 
*   [title] : judul
*   [data] : isinya
*   [warna] : warna
*   [aktif] : 1 / 0
* ))
* [id] : if else
* [titlehead] : judul atas
*/
function accord($dt,$id='accordion',$title_head='') {
    $ret='';
    foreach ($dt as $x => $isi) {
        $idg='acord'.$x;
        $title=isset($isi['title'])?$isi['title']:'Title '.($x+1);
        $data=isset($isi['data'])?$isi['data']:' . . . ';
        $color=isset($isi['warna'])?$isi['warna']:'box-primary';
        $aktif=isset($isi['aktif'])?$isi['aktif']:($x==0?1:0);
        $ret.=div(
            div(
            '<h4 class="box-title">
                <a data-toggle="collapse" data-parent="#'.$id.'" href="#'.$idg.'" aria-expanded="true" class="">'
                .$title
                .'</a></h4>'
            ,'class="box-header with-border"')
            .div(
            div(
                $data
            ,'class="box-body"')
            ,'id="'.$idg.'" class="panel-collapse collapse '.($aktif?'in':'').'" aria-expanded="'.($aktif?'true':'false').'"')
        ,'class="panel box '.$color.'"');
    }
    return $title_head
        .div(
        div(
        $ret
        ,"class='box-group' id='$id'")
        ,'class="box-body"');
}

/**
* ver 1.0
* dt : array( array(
*  title,
*  data,
*  aktif = 1 
* )) 
*/
function tabs($dt) {
    $aul=$atab=array();
    foreach ($dt as $x => $d) {
        $aktif=(($x==0)||isset($d['aktif']));
        if(isset($d['title'])){
            $aul[]=
              tag('li',
                a($d['title']
                    ,($aktif?'aria-expanded="true"':'')
                     .' href="#tab_'.$x.'" data-toggle="tab"'
                    )
              ,($aktif?'class="active"':''))
            ;
            $atab[]=
                div($d['data'],'class="tab-pane'.($aktif?' active':'').'" id="tab_'.$x.'"');
        }
    }
    return
        div(
            tag('ul',implode('',$aul),'class="nav nav-tabs"')
            .div(implode('',$atab),'class="tab-content"')
        ,'class="nav-tabs-custom"');
        /*$x=
              '<ul class="nav nav-tabs">'
              .'<li class="active">
                    <a aria-expanded="true" href="#tab_0" data-toggle="tab">
                        Preview
                    </a>
                </li>'
              
              .($persetujuan ? 
                '<li>
                    <a href="#tab_1" data-toggle="tab">
                        Edit
                    </a>
                </li>':'')
              .($dispo ? 
              '<li>
                <a href="#tab_2" data-toggle="tab">
                    Perbaiki
                </a>
                            </li>':'')//aria-expanded="false"
              .'<li><a href="#tab_4" data-toggle="tab">
                Pembatalan</a></li>'
              .( $peg['peg']->pengarah_no_sk==0
              ?'<li><a href="#tab_3" data-toggle="tab">
                Beri nomor</a></li>':'')
              .'</ul>'
              
              .div(
                    div($view_surat,'class="tab-pane active" id="tab_0"')
                    .($persetujuan ? div($persetujuan,'class="tab-pane" id="tab_1"'):'')
                    .($dispo ? div($dispo,'class="tab-pane" id="tab_2"'):'')
                    .div($batal,'class="tab-pane" id="tab_4"')
                    .div($nomorkan,'class="tab-pane" id="tab_3"')                    
                ,'class="tab-content"');*/
}

/**
* return tag style css
*/
function tag_css($dt) {
    return '<style type="text/css">
            <!--
            '.$dt.'
            -->
            </style>';
}

/**
* return tag javascript
*/
function tag_js($dt) {
    return "<script type=\"text/javascript\">
            $dt 
            </script>";
}

function tag_jqready($dt) {
    return tag_js("\$(document).ready(function(){
                    $dt
                    });");
}

#---enkrip
/**
* enkripsi base 64 v:004
*/
function enkrip($data,$gz=0,$ser=0) {
    /*$setting='memory_limit';
    ini_set($setting, '-1');*/
    $ret="";
    if($ser){
        $ret=serialize($data);
    }else{
        $ret=json_encode($data);
    }
    /*if($gz){
        $data_on = base64_encode(gzcompress(json_encode($data)));
    }else{
        $data_on = base64_encode(json_encode($data));
    }*/
    if($gz)$ret = gzcompress($ret);
    return preg_replace(array('/\=/','/\+/','/\//'), array('-','_','~'), base64_encode($ret));
    /*ini_restore($setting);
    return $ret;*/
}

#--decript
/**
* dekrip v:004
*/
function dekrip($data,$array=false,$gz=0,$ser=0) {
    $on_data = preg_replace(array('/\-/','/\_/','/\~/'), array('=','+','/'), $data);
    $ret=base64_decode($on_data);
    if($gz)$ret=gzuncompress($ret);
    if($ser){
        $ret=unserialize($ret);
    }else{
        $ret=json_decode($ret,$array);
    }
    return $ret;
    /*if($gz){
        return json_decode(gzuncompress($ret),$array);
    }else{
        return json_decode($ret,$array);
    }    */
}

/**
*  hash password pengganti md5 jadul
*/
function bcrypt($xx) {
    return password_hash($xx,PASSWORD_BCRYPT);
}
        
/** v:0.21
*  h3, p, ico, [info], color, href 
*/
function box_cpanel($p) {
        return  
            div(
                div(
                  "<h3>$p[h3]</h3>
                  <p>$p[p]</p>"
                ,'class="inner"')
                .div(
                    '<i class="'.@$p['ico'].'"></i>'
                ,'class=""')
                .(@$p['href']
                    ?anchor($p['href'],
                    (@$p['info']?$p['info']:'More info')
                .' <i class="fa fa-arrow-circle-right"></i>'
                ,'class="small-box-footer"'):
                (@$p['pinfo']?$p['pinfo']:''))
            ,'class="small-box '.(@$p['color']?@$p['color']:'bg-aqua').'"');
}

/**
* add seacr
* v 2
*/
function add_uri_search($uris,$add=array(),$search_ke=4) {
    $uri=explode('/',$uris);
    if($add){
        if(!empty($uri[$search_ke])){
                $sea=un_de($uri[$search_ke]);
                $sea=array_merge($sea,$add);
        }else{
                $sea=$add;
        }
        $uri[$search_ke]=in_de($sea);
    
        return implode('/',$uri);
    }else{
        return $uris;
    }
}   

/**
* add seach
*/
function change_search_uri($uris,$se='',$search_ke=4) {
    return add_uri_search($uris,array('search'=>trim($se),),$search_ke);        
}

/**
* split large insert batch
*/
function insert_batch($ntabel,$datas,$chunk=100,$ignore=0) {
    $CI =& get_instance();
    $CI->db->trans_start();
    $_datas = array_chunk($datas, $chunk);
    if(!$ignore){
        foreach ($_datas as $key => $data) $CI->db->insert_batch($ntabel, $data);
    }else{
        foreach ($_datas as $key => $data) $CI->db->insert_ignore_batch($ntabel, $data);
    }
    $CI->db->trans_complete();
}

/**
* v0.1
*/
function parameter($code,$isi=null,$enc=0) {
    return params("parameter",$code,$isi,array('param','val'),$enc);
}

/** V.0.4
* nama tabel ="parameer"
* kode | array kode
* isi | array isi + kode (update /insert)
* parameter array(kode,data)
*/
function params($ntabel,$code,$isi=null,$field=array('kode','data'),$enc=0) {
    $CI =& get_instance();
    $where=$where_in=$update=$insert=$ret=array();
    list($field0,$field1)=$field;
    #if(!$field)$field=array('kode','data');
    if(!is_array($code)){
        $CI->db->where(array($field0=>$code));
    }else{
        $CI->db->where_in($field0,$code);
    }
    
    $param=$CI->db->get($ntabel);
    if($param&&($param->num_rows()>0))
        foreach ($param->result() as $r) $ret[$r->$field0]=$r->$field1; 
    
    #$lastq=$CI->db->last_query();
    if($isi){
        if(!is_array($code)){
            $code=array($code);
            $isi=array($isi);
        }
        foreach ($code as $xc => $codex) 
        if(count($ret)>0 && isset($ret[$codex])){
            $update[$codex]=($enc?enkrip($isi[$xc],1):$isi[$xc]);
        }else{
            $insert[]=array(
                $field[0]=>$codex,
                $field[1]=>($enc?enkrip($isi[$xc],1):$isi[$xc]),
            );
        }
        
        $ci=count($insert);
        $cu=count($update);
        #stop(array("w"=>$where,"r"=>$ret,"u"=>$update,"f"=>$field,"i"=>$insert,"c"=>$code,"isi"=>$isi,$lastq));
        if($cu>0)
            foreach ($update as $w => $isiw) 
                $CI->db->where(array($field0=>$w))->update($ntabel,array($field1=>$isiw));
        if($ci>0)
            $CI->db->insert_batch($ntabel,$insert);
        return array('i'=>$ci,'u'=>$cu);
    }else{
        if($ret){
            if(!is_array($code)){
                return ($enc?dekrip($ret[$code],1,1):$ret[$code]);
            }else{
                return $ret;
            }
        }else{
            return false;
        }
    }
    
}

/*/ **
* nama tabel
* kode | array kode
* isi | array isi + kode (update /insert)
* parameter array(kode,data)
* /
function params($ntabel,$code,$isi=null,$field=null) {
    $CI =& get_instance();
    $fkode=array('kode','data');
    if($field)$fkode=$field;
    
    if(!is_array($code)){
        $where=array($fkode[0]=>$code);
    }else{
        $where=$code;
    }
    
    if(($param=$CI->db->where($where)->get($ntabel))&&($param->num_rows()>0)){
        $ret=array();
        foreach ($param->result() as $r) $ret[$r->$fkode[0]]=$r->$fkode[1]; 
        if($isi){
            if(is_array($isi)){
                if(!isset($isi[$fkode[0]]))$isi[$fkode[0]]=$code;
                $data=$isi;
            }else{
                $data=array($fkode[1]=>$isi,);
            }
            return $CI->db->where(array($fkode[0]=>$code))->update($ntabel,$data);
        }else{
            if(!is_array($code)){
                return $ret[$code];
            }else{
                return $ret;
            }
        }            
    }else{
        if($isi){
            / *cek(array(
            $kode,$isi,
            ));* /
            if(is_array($isi)){
                if(!isset($isi[$fkode[0]]))$isi[$fkode[0]]=$code;
                $data=$isi;
            }else{
                $data=array($fkode[0]=>$code,$fkode[1]=>$isi);
            }
            return $CI->db->insert($ntabel,$data);
        }else{
            return false;
        }
        
    }
    
}*/

/**
  * range=1 / 1 array mulai
  */
  function buat_tgl($mulai,$range=0) {
        if($range){
            $timestamp = strtotime($mulai);
            $days = array();
            for ($i = 0; $i <= $range; $i++) {
                $days[] = strftime('%Y-%m-%d', $timestamp);
                $timestamp = strtotime('+1 day', $timestamp);
            }
            return $days;
        }else{
            return array($mulai);
        }        
  }  
  
  /**
  * awal, akhir  ,[weekday ke]
  */
  function buat_tgl_range($mulai,$akhir,$pekanke=null) {
        $timestamp = strtotime($mulai);
        $timestamp_akhir = strtotime($akhir);
        $days = array();
        if(!isset($pekanke))$days[] = strftime('%Y-%m-%d', $timestamp);
        while($timestamp!=$timestamp_akhir){
            $timestamp = strtotime('+1 day', $timestamp);
            #cek(array(strftime('%w',$timestamp),$pekanke));
            if(!isset($pekanke) || (isset($pekanke) && strftime('%w',$timestamp)==($pekanke+1)))
                $days[] = strftime('%Y-%m-%d', $timestamp);
        }
        return $days;    
  }  
  
  /**
  * ver:2.0
  * 
  * cl_main, st_main
  * fa_head, before_h3_head, h3_head, after_h3_head, $cl_head, st_head
  * main
  * foot, cl_foot, st_foot  * 
  * if no_foot==1
  */
  function box_adminlte2($p) {
      extract($p);
      return 
      div(
        div(
            (@$fa_head?'<i class="fa '.$fa_head.'"></i>':'')
            .(@$before_h3_head?$before_h3_head:'')
            .(@$h3_head?'<h3 class="box-title">'.$h3_head.'</h3>':'')
            .(@$after_h3_head?$after_h3_head:'')
        ,'class="box-header '.(@$cl_head?$cl_head:'').'"'.(@$st_head?$st_head:''))
        .div(
            (@$main?$main:'')
        ,'class="box-body '.(@$cl_main?$cl_main:'').'"'.(@$st_main?$st_main:''))
        .(@$no_footer==1?'':div(
            (@$foot?$foot:'')
        ,'class="box-footer '.(@$cl_foot?$cl_foot:'').'"'.(@$st_foot?$st_foot:'')))
    ,'class="box '.(@$cl_box?$cl_box:'').'"'.(@$st_box?$st_box:''));
  }
  
  /*
  * get uristring segment ke
  */
  function uri($ke) {
      $CI =& get_instance();
      return $CI->uri->segment($ke);
  }
  
  /*
  * get uristring segment ke to
  */
  function uri2($uri) {
      $CI =& get_instance();
      $ur=explode(',',$uri);
      $uris=array();
      foreach ($ur as $x) 
        $uris[$x]=($CI->uri->segment($x)?$CI->uri->segment($x):"");
      return implode('/',$uris);
  }
  
  /*
  * conv text from &&...& to r
  */
  function replaceto($text,$r) {
        if(is_object($r))$r=json_decode(json_encode($r),1);
        foreach ($r as $x=>$y){
            $pat[]='/&&'.$x.'&/';
            $rep[]=$y;
        } 
        return preg_replace($pat,$rep,$text);
  }
  
  /**
  * cek_role unit
  */
  function roleunit($id_pegawai=0,$folder=0) {
    $CI =& get_instance();
    $ret=array();
    $g  = $CI->db->from('ref_role_unit rou')
            ->join('ref_role rr','rr.id_role=rou.id_role')
            ->join('pegawai_role pr','pr.id_role=rr.id_role')
            ->join('ref_aplikasi ra','ra.id_aplikasi = rr.id_aplikasi')
            ->select('rou.id_unit iu')
            ->where(array(
                'pr.id_pegawai' => ($id_pegawai?$id_pegawai:sesi('id_pegawai')), 
                'ra.folder'=>($folder?$folder:uri(1)),
                ))
            ->get();
    if( $g->num_rows() > 0){
        foreach ($g->result() as $gr) $ret[]=$gr->iu;
        return $ret;
    }else{
        return false;
    } 
  }
  
  
  /**
  * cek_role
  */
  function isrole($id_pegawai=0,$nav_code,$all=0) {
    $CI =& get_instance();
    $where=array(
        'p.id_pegawai' => ($id_pegawai?$id_pegawai:sesi('id_pegawai')), 
        'na.kode' => $nav_code,
        );
    if(!$all)$where['a.folder']=uri(1);
    $g  = $CI->db->from('pegawai_role p')
            ->join('ref_role_nav n','n.id_role = p.id_role')
            ->join('nav na','na.id_nav = n.id_nav')
            ->join('ref_aplikasi a','a.id_aplikasi = na.id_aplikasi')
            ->where($where)
            ->get();
    return ($g->num_rows() > 0) ? TRUE : FALSE;
  }
  
  /**
  * cek_role
  */
  function isroled($nav_code,$id_pegawai=0,$all=0) {
      return isrole($id_pegawai,$nav_code,$all);
  }
  
/**
  * cek_role with folder  + role v.0.2
  */
  function is_role($kode_folder=0,$nav_code='') {
    $CI =& get_instance();
    $wh=array(
        'n.aktif'=>1,
        'ra.folder'=>($kode_folder?$kode_folder:uri(1)),
        'pr.id_pegawai' => sesi('id_pegawai'), 
    );
    if($nav_code)$wh['n.kode']=$nav_code;
    $g  = $CI->db->from('pegawai_role pr')
            ->join("ref_role rr",'rr.id_role=pr.id_role')
            ->join('ref_role_nav rn','rn.id_role = pr.id_role')
            ->join('nav n','n.id_nav = rn.id_nav')
            ->join("ref_aplikasi ra",'ra.id_aplikasi=rr.id_aplikasi')
            ->where($wh)
            ->get();
    return ($g->num_rows() > 0) ? TRUE : FALSE;
  }
  
  /**
  * cek_role with folder  + role v.0.3
  */
  function is_roled($nav_code='',$kode_folder=0) {
      return is_role($kode_folder,$nav_code);
  }
    
  /**
  *  ambil id_unker saat ini dari id_pegawai
  */
  function unker_peg($id_pegawai) {
    $CI =& get_instance();
    $g  = $CI->db->from('peg_jabatan p')
            ->join('ref_jabatan rj','rj.id_jabatan = p.id_jabatan')
            ->join('ref_bidang rb','rb.id_bidang = pj.id_bidang')
            ->where(array('p.id_pegawai' => $id_pegawai, 'p.status' => 1))
            ->get();
    return ($g->num_rows() > 0) ? $g->row('id_unker') : FALSE;
  }
  
  /**
  * convert array to info_string with split=, ver 2
  */
  function implode_array($ar,$split=",\n") {
      $ret=array();
      if(is_array($ar) || is_object($ar))foreach ($ar as $x => $a) $ret[]="[$x]=>".(is_array($a) || is_object($a)?"\n".implode_array($a,$split):$a);
      return implode($split,$ret);
  }
  
  /**
  * format to number indonesia from sql v:0.1
  */
  function sql_format($n,$dec=0) {
      return "FORMAT($n,$dec,'id_ID')";
  }
  
  /**
  * conv object 2 array
  */
  function obj2ar($dat) {
      return json_decode(json_encode($dat),true);
  }
  /**
  * ambil dari domain lain
  */
  function httpPost($url, $data){
    $curl = curl_init($url);
    if(substr($url,0,5)=="https"){
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
    }else{
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
    }
    curl_close($curl);
    return $response;
  }  
  
  /**
  * concat other dtatabase:: array, database
  */
  function via($dt,$pre="") {
      $ret=array();
      foreach ($dt as $d) $ret[$d]=($pre?$pre.".":"").$d;
      return (object)$ret;
  }
  
  ##Array add | substract
  
  function array_add($a1, $a2) {  // ...
      // adds the values at identical keys together
      $aRes = $a1;
      foreach (array_slice(func_get_args(), 1) as $aRay) {
        foreach (array_intersect_key($aRay, $aRes) as $key => $val) $aRes[$key] += $val;
        $aRes += $aRay; }
  return $aRes; }
  
  function array_subtract($a1, $a2) {  // ...
      // adds the values at identical keys together
      $aRes = $a1;
      foreach (array_slice(func_get_args(), 1) as $aRay) {
        foreach (array_intersect_key($aRay, $aRes) as $key => $val) $aRes[$key] -= $val;
        foreach (array_diff_key($aRay, $aRes) as $key => $val) $aRes[$key] = -$val; }
  return $aRes; }
  
  /**
  * ret nav ( $url,$teks )
  */
  function act_nav($url,$teks,$uristring="",$fa="") {
      if(!$uristring)$uristring=uri_string();
        return li(anchor($url,($fa?"<i class='fa fa-$fa'></i>":"").$teks),($uristring==$url?'class="active"':""));
  }
  
  /**
  * cek kusus kewenangan DEBUG
  * deb vd:var_dump debug:show?
  */
  function c_($deb=null,$vd=0,$debug=1){
      if(isrole(0,'DEBUG',1))return _cek($deb,$vd,$debug);
  }
  
  /**
  * cek kusus kewenangan DEBUG
  */
  function komen_($deb=null){
      if(isrole(0,'DEBUG',1))return komen($deb);
  }
  
  #DB
  /**
  *  tname : nama tabel
  */
  function is_tabel_exist($tname) {
        $sql="SHOW TABLES LIKE '$tname'";
        $CI =& get_instance();
        return $CI->db->query($sql)->num_rows();
  }    
  
  /**
  *  tname : nama tabel
  */
  function is_field_exist($field_name,$tname,$database="") {
      $CI =& get_instance();
      if($database=="")$database=$CI->db->database;
      $dbname=($database?"TABLE_SCHEMA = '$database' AND":"");
      $sql="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE $dbname TABLE_NAME = '$tname' AND COLUMN_NAME = '$field_name'";
      return $CI->db->query($sql)->num_rows()>0;
  }    
  
  /**
  *  tname : nama tabel
  */
  function field_list($tname,$database="") {
      $CI =& get_instance();
      if($database=="")$database=$CI->db->database;
      $dbname=($database?"$database.":"");
      $sql="DESCRIBE $dbname$tname";
      $rdb=$CI->db->query($sql);
      $ret_fn=array();
      if($rdb->num_rows()>0)
      foreach ($rdb->result() as $r) $ret_fn[]=$r->Field;
      return $ret_fn;
  }    
  
  function set_bc($url,$title) {
      $CI =& get_instance();
      $simpan=array('title'=>trim($title),'url'=>$url,);
      if($CI->db->where($simpan)->get('bc')->num_rows()==0)$CI->db->insert('bc',$simpan);
  }
  
  function is_app_aktif($folder) {
    $CI =& get_instance();
    return $CI->db->where('folder',$folder)->get('ref_aplikasi')->row('aktif')=='1';
  }
  
  /**
  * return title & alt with same value
  */
  function titlealt($title) {
      return "title='$title' alt='$title'";
  }
  
  /**
  * kode:1: ret 2:json 3:echo
  */
  function ret_it($table,$kode=1,$plus_code="") {
      switch($kode){
          case 1:return $table;break;
          case 2:$ret=array('data'=>$table);if(is_array($plus_code))$ret=array_merge_recursive($ret,$plus_code); die(json_encode($ret));break;
          case 3:echo $table;break;
      }
  }
  
  #new?
  function cget_nav($id_peg,$ref,$app,$id_par = null) {
      
        $pars = array();

        $on_active = null;
        
        $CI =& get_instance();
        $CI->load->database();    
        
        $where = (!empty($id_par)) ? 'n.id_par_nav = '.$id_par : 'n.ref = '.$ref.' AND (id_par_nav IS NULL OR id_par_nav = 0) AND n.id_aplikasi = '.$app;
        // cek($this->db->last_query());
        
        $CI->db->select('n.*, ap.folder, ap.nama_aplikasi');
        $CI->db->from('nav n');
        $CI->db->join('ref_aplikasi ap','ap.id_aplikasi = n.id_aplikasi');
        $CI->db->join('ref_role_nav r','r.id_nav = n.id_nav');
        $CI->db->join('pegawai_role pr', 'pr.id_role = r.id_role');
        $CI->db->where($where, null);    
        $CI->db->where('pr.id_pegawai', $id_peg);
        $CI->db->where('n.tipe', 2);
        $CI->db->where('n.aktif', 1);
        $CI->db->group_by('n.id_nav');
        $CI->db->order_by('n.urut');
        
        $nav = $CI->db->get();
        # cek($CI->db->last_query());

        foreach($nav->result() as $n) {
            // $j = check_uris($n->id_nav);
            /*$CI->db->from('nav n');
            $CI->db->where('id_par_nav', $n->id_nav);    
            $CI->db->where('tipe','2');
            $ch = $CI->db->get();*/
            
            $icon = (!empty($n->fa)) ? $n->fa :  'circle-o';

            // cek(getUriAktif());
            # cek($n);
            /*$ua = getUriAktif();
            if(!empty($ua)) $pars = getPars($ua[0]);*/
            /*if ($ch->num_rows() > 0) {
                // $j= 'active';
                $j = check_uris($n->id_nav, null);
                // cek($j);
                if(in_array($n->id_nav, $pars)) $j = 'active';

                // cek($n->id_nav);
                // cek($pars);
                echo '<li class="treeview '.$j.'">
                <a href="#"><i class="fa fa-'.$icon.'"></i> <span>'.$n->judul.' </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">';
                get_nav($id_peg,$ref,$app,$n->id_nav);
                echo '</ul>';
            } else { */
                $j = uris($n->link);
                $j = !empty($j) ? 'class="active"':null;
                echo "<li ".$j.">".anchor($n->link,'<i class="fa fa-'.$icon.'"></i><span>'.$n->judul.'</span>')."</li>";
                
            /*} */
            
        }
        
        
    }
    
  function cget_ref_nav($id_peg) {
        
        $CI =& get_instance();
        $CI->load->database();    
        
        $CI->db->from('nav n');
        $CI->db->join('ref_aplikasi app','app.id_aplikasi = n.id_aplikasi');
        $CI->db->join('ref_role_nav r','r.id_nav = n.id_nav');
        $CI->db->join('pegawai_role pr', 'pr.id_role = r.id_role');
        $CI->db->where(array(
            'ref' => 2,
            'n.tipe' => 2,
            'n.aktif' => 1,
            'pr.id_pegawai' => $id_peg,
            'app.folder' => 'referensi',
            '(id_par_nav IS NULL OR id_par_nav = 0)' => null
        ));
        $CI->db->order_by('n.urut');
        
        $nav = $CI->db->get();
        stop($nav);
        if($nav)
        foreach($nav->result() as $n) {
            
            $CI->db->from('nav n');
            $CI->db->where('id_par_nav', $n->id_nav);    
            $CI->db->where('tipe','2');
            $CI->db->where('aktif','1');
            $ch = $CI->db->get();
            
            $icon = (!empty($n->fa)) ? $n->fa :  'circle-o';
            if ($ch->num_rows() > 0) {
                $j = check_uris($n->id_nav);
                echo '<li class="treeview '.$j.'">
                <a href="#"><i class="fa fa-'.$icon.'"></i> <span>'.$n->judul.' </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">';
                get_nav($id_peg,2,1,$n->id_nav);
                echo '</ul>';
            } else {
                $j = uris($n->link);
                $j = !empty($j) ? ' class="active"':null;
                echo "<li".$j.">".anchor($n->link,'<i class="fa fa-'.$icon.'"></i> <span>'.$n->judul.'</span>')."</li>";
            } 
            
        }
        
        $ro = get_role($id_peg);
        foreach($ro['aplikasi']  as $k => $v) {
            if ($v['direktori'] != 'referensi') {

                $CI->db->select('n.id_nav');
                $CI->db->from('nav n');
                $CI->db->join('ref_role_nav r','r.id_nav = n.id_nav');
                $CI->db->join('pegawai_role pr', 'pr.id_role = r.id_role');
                $CI->db->where('n.ref = 2 AND (id_par_nav IS NULL OR id_par_nav = 0) AND n.id_aplikasi = '.$v['id_aplikasi'], null);    
                $CI->db->where('pr.id_pegawai', $id_peg);
                $CI->db->where('n.tipe', 2);
                $CI->db->where('n.aktif', 1);
                $CI->db->group_by('n.id_nav');
        
                $cek = $CI->db->get();
        
                if ($cek->num_rows() > 0) {
                // cek($v);
                // cek($v['direktori']);
                $j = check_uris(null, $v['id_aplikasi']);
                // cek($cek->row()->id_nav);
                echo '<li class="treeview '.$j.'">
                <a href="#"><i class="fa fa-cog"></i> <span>'.$v['nama_aplikasi'].' </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">';
                get_nav($id_peg,2,$v['id_aplikasi']);
                echo '</ul>';
                }
            }
        }
  }
  
  function cget_role($id) {    
        
        $CI =& get_instance();
        $CI->load->database();    
            
        $CI->db->from('pegawai_role r');
        $CI->db->join('ref_role ro','ro.id_role = r.id_role','left');
        $CI->db->join('ref_aplikasi a','a.id_aplikasi = ro.id_aplikasi','left');
        $CI->db->where('r.id_pegawai',$id);
        $CI->db->where('a.aktif',1);
        $CI->db->order_by('a.urut');
        $roled = $CI->db->get();
        
        $role = array();
        $aplikasi = array();
        
        foreach($roled->result() as $r) {
            
            $CI->db->from('ref_aplikasi');
            $CI->db->where('id_par_aplikasi',$r->id_aplikasi);
            $an = $CI->db->get();
            
            $anak = array();
            foreach ($an->result() as $ap_a) {
                $app_anak[] = $ap_a->folder;
            }
            
            $aplikasi[$r->folder] = array(
                'id_role' => $r->id_role,
                'id_aplikasi' => $r->id_aplikasi,
                'aplikasi_anak' => $anak,
                'nama_role' => $r->nama_role,
                'nama_aplikasi' => $r->nama_aplikasi,
                'direktori' => $r->folder,
                'warna' => $r->warna);
            
        }
        
        $on = array(
        'aplikasi' => $aplikasi
        ); return $on;

    }
    
    function dir_at($folder="") {
        if(!$folder)$folder="application/controllers/";
        $CI =& get_instance();
        
        $CI->load->helper('directory');
        $map = directory_map('./'.$folder, 1);
        
        $app = array();
        foreach($map as $o) 
            #cek(array($o,substr($o,0,-1)));
            if (!preg_match("/\./i", $o)) 
                $app[] = ( in_array(substr($o,-1),array("\\","/"))?substr($o,0,-1):$o);
        
        return $app;
    }
    
  /**
  * find first table in array
  */
  function find_1st_table($tbl) {
     if(is_array($tbl)){
        $tkey=array_keys($tbl);
        $tbl=substr($tkey[0],0,strpos($tkey[0],' '));
     }
     if(strpos($tbl,' ')>0)$tbl=substr($tbl,0,strpos($tbl,' '));
     return $tbl;
  }
  
  function gettmptbl($id) {
      $CI =& get_instance();
        $paramx=$CI->db->where(array('id_get'=>$id))->get('tmp_get')->row('tmp');
      return $paramx;
  }
  
  function deltmptbl($id) {
      $CI =& get_instance();
      return $CI->db->where(array('id_get'=>$id))->delete('tmp_get');
  }
  
?>