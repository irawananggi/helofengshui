<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*  mr.febri@gmail.com
*  ci.213.314 cmd -> #.0238
* v.05
*/

function app_param($id = null) {
        $CI =& get_instance();
        $CI->load->database();
        if ($id) {
            if(!$dt=$CI->db->where('param',$id)->get('parameter')->row('val'))
                if(!$dt=@$CI->config->item('app')[$id])
                    $dt=@$CI->config->item($id);
            return $dt;
        }else{
            $app=array();
            if(!$app=@$CI->config->item('app')) 
                if($app_config=@$CI->config->item('app_config'))
                    foreach ($app_config as $ap) $app[$ap]=@$CI->config->item($ap);
            
            $app_key=array_keys($app);
            if(($db=$CI->db->where_in('param',$app_key)->get('parameter'))&&($db->num_rows()>0))
                foreach ($db->result() as $r) 
                    $app[$r->param]=$r->val;                                
            return $app;
        }
}

function this_folder() {
    $CI =& get_instance();
    $CI->load->database();
    $fold=$CI->uri->segment(1);
    return $CI->db->where('folder',$fold)->get('ref_aplikasi')->row();
}

function mynip($id_peg) {
    return me($id_peg)->nip;
    /*$CI =& get_instance();
    $CI->load->database();
    return $CI->db->where('id_pegawai',$id_peg)->get('peg_pegawai')->row('nip');*/
}

function me($id_peg) {
    $CI =& get_instance();
    $CI->load->database();
    return $CI->db->where('id_pegawai',$id_peg)->get('peg_pegawai')->row();
}



function get_doc_app($folder) {
        $CI =& get_instance();
        $CI->load->database();
        if(! is_tabel_exist('ref_app_doc'))
            $CI->db->query("CREATE TABLE `ref_app_doc` (
                  `id_ref_app_doc` INT(2) NOT NULL AUTO_INCREMENT,
                  `id_app` INT(4) DEFAULT NULL,
                  `doc` TEXT,
                  `status` INT(1) DEFAULT '1',
                  `ver` VARCHAR(5) DEFAULT '1.00',
                  PRIMARY KEY (`id_ref_app_doc`)
                ) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1");
        
        $sql="SELECT d.*, ra.nama_aplikasi
            FROM `ref_aplikasi` ra
            LEFT JOIN `ref_app_doc` d ON `ra`.`id_aplikasi`=`d`.`id_app` AND d.status=1
            WHERE `ra`.`aktif` =  1 AND ra.`folder`='$folder'";
        return $CI->db->query($sql)->row('doc');
}

function fget_nav($is_ref=1,$with_home=1,$folder="",$id_peg=0,$uri="") {
        $CI =& get_instance();
        $parm=array('no_home'=>!$with_home,'is_ref'=>$is_ref);
        if($folder)$parm['folder']=$folder;
        if($id_peg)$parm['id_peg']=$id_peg;
        if($uri)$parm['uri']=$uri;
        $CI->load->library('Mmenu',$parm);
        echo $CI->mmenu->getmenu();
  }
  

?>