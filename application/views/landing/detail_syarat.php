<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url().'assets/favicon/favicon.ico' ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/fengshui/landing/css/styles.css' ?>">
    <?php 
      if ($this->session->userdata('bahasa') == 'in') {
        echo '<title>Helo fengshui | Syarat dan Ketentuan</title>';
      }elseif ($this->session->userdata('bahasa') == 'en') {
        echo '<title>Hello Feng Shui | Terms and Conditions</title>';
      }else {
        echo '<title>Helo风水 | 条款和条件</title>';
      }

      ?>
    
  </head>

<body>
  <header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                  <?php
                $no = 1 + $offs;
                foreach ($data_link_1->result() as $row) { ?>
                    <a target="_blank" href="<?php echo $row->deskripsi;?>" class="mr-3 sosial-media color-primary" style="margin-right: 0px!important">
                        <img src="<?php echo base_url('uploads/link/'.$row->foto) ;?>" style="width: 33px;height: 30px" />
                    </a>
                <?php }?>
                  <!--   <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-facebook-square"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-instagram"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-twitter-square"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-linkedin"></span>
                    </a> -->
                </div>
                <div class="col-6">
                    <div class="text-right">
                        
                        
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php 
                                $uri_1 = $this->uri->segment(1, 0);
                                $uri_2 = $this->uri->segment(2, 0);
                                $uri_3 = $this->uri->segment(3, 0);
                                $uri = in_de(array('uri1'=>$this->uri->segment(1, 0),'uri2'=>$this->uri->segment(2, 0)));
                                
                                if ($this->session->userdata('bahasa') == 'in') {
                                  echo '<i class="ico-flag id-flag"></i> <span class="lang-text">Bahasa Indonesia</span>';
                                }elseif ($this->session->userdata('bahasa') == 'en') {
                                  echo '<i class="ico-flag en-flag"></i> <span class="lang-text">Inggris</span>';
                                }else {
                                  echo '<i class="ico-flag cn-flag"></i> <span class="lang-text">Mandarin</span>';
                                }

                                ?>

                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="<?php echo site_url('Front/in/'.$uri); ?>"><i class="ico-flag id-flag"></i> Bahasa Indonesia</a>
                                <a class="dropdown-item" href="<?php echo site_url('Front/en/'.$uri); ?>"><i class="ico-flag en-flag"></i> Inggris</a>
                                <a class="dropdown-item" href="<?php echo site_url('Front/cn/'.$uri); ?>"><i class="ico-flag cn-flag"></i> Mandarin</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-nav header-page scoller" id="mainNav">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-3">
                    <a href="<?php echo base_url();?>" class="js-scroll-trigger">
                        <i class="logo"></i>
                    </a>
                </div>
                <div class="col-9">
                    <div class="nav-main">
                        <nav aria-label="breadcrumb">

                            <?php 
                            if ($this->session->userdata('bahasa') == 'in') { ?>
                            <ol class="breadcrumb breadcrumb-transparent  nav justify-content-end">
                              <li class="breadcrumb-item nav-item"><a href="<?php echo base_url();?>">Beranda</a></li>
                              <li class="breadcrumb-item" aria-current="page">Syarat dan Ketentuan</li>
                            </ol>
                          <?php   }elseif ($this->session->userdata('bahasa') == 'en') { ?>
                            <ol class="breadcrumb breadcrumb-transparent  nav justify-content-end">
                              <li class="breadcrumb-item nav-item"><a href="<?php echo base_url();?>">Home</a></li>
                              <li class="breadcrumb-item" aria-current="page">Terms and Conditions</li>
                            </ol>
                          <?php  }else { ?>
                            <ol class="breadcrumb breadcrumb-transparent  nav justify-content-end">
                              <li class="breadcrumb-item nav-item"><a href="<?php echo base_url();?>">首页</a></li>
                              <li class="breadcrumb-item" aria-current="page">条款和条件</li>
                            </ol>
                          <?php   }

                            ?>
                        </nav>


                    </div>
                    <div class="text-right">
                        <a href="#" class="btn-nav-mobile"><i class="ico-menu"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
 <div class="page-content">
    <div class="post-detail">

      <?php 
        if ($this->session->userdata('bahasa') == 'in') { ?>
        
      <h2 class="mt-4 mb-5 text-center"><?php echo $syarat->judul;?></h2>

      <img src="images/image-blog-detail.jpg" class="img-fluid mb-5" alt="">
      <p><?php echo $syarat->deskripsi;?></p>
      <?php   }elseif ($this->session->userdata('bahasa') == 'en') { ?>
        
      <h2 class="mt-4 mb-5 text-center"><?php echo $syarat->judul_en;?></h2>

      <img src="images/image-blog-detail.jpg" class="img-fluid mb-5" alt="">
      <p><?php echo $syarat->deskripsi_en;?></p>
      <?php  }else { ?>
        
      <h2 class="mt-4 mb-5 text-center"><?php echo $syarat->judul_man;?></h2>

      <img src="images/image-blog-detail.jpg" class="img-fluid mb-5" alt="">
      <p><?php echo $syarat->deskripsi_man;?></p>
      <?php   }

        ?>


                            
      
    </div>
  </div>

  <footer>
    <div class="footer-top">
        <div class="container">

       <?php 
        if ($this->session->userdata('bahasa') == 'in') { ?>   
            <h2>Download Sekarang <br> Aplikasinya di</h2>
        <?php  }elseif ($this->session->userdata('bahasa') == 'en') { ?>  
            <h2>Download Now <br> Application in</h2>
        <?php   }else { ?>  
            <h2>现在下载 <br> 应用在 </h2>
        <?php   } ?>

            <div class="download-btn mt-5">

              <?php
                foreach ($data_link_2->result() as $row) { ?>
                   <a href="<?php echo $row->deskripsi;?>" class="btn btn-primary btn-lg mx-2">
                       <img src="<?php echo base_url('uploads/link/'.$row->foto) ;?>" style="width: 16px;height: 20px" /> <?php echo $row->judul;?>
                    </a>
                <?php }?>

                <!-- 
                <a href="#" class="btn btn-primary btn-lg mx-2">
                    <span class="icon-path-12916"></span> Playstore
                </a>
                <a href="#" class="btn btn-primary btn-lg mx-2">
                    <span class="icon-group-285 "></span> Appstore
                </a> -->
            </div>
            <div class="mck-foo">
                <img src="<?php echo base_url().'assets/fengshui/landing/images/app-footer.png' ?>" class="img-fluid" alt="">
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">

       <?php 
        if ($this->session->userdata('bahasa') == 'in') { ?>   
            2020 &copy; Aplikasi Fengshui. Seluruh hak cipta. | <a href="<?php echo base_url().'detail/syarat_ketentuan/'; ?>" class="text-white">Syarat & Kententuan</a>
        <?php  }elseif ($this->session->userdata('bahasa') == 'en') { ?>  
            2020 &copy; Fengshui App. All rights reserved. | <a href="<?php echo base_url().'detail/syarat_ketentuan/'; ?>" class="text-white">Terms & Conditions</a>
        <?php   }else { ?>  
            二千〇二十 &copy; 风水应用. 版权所有. | <a href="<?php echo base_url().'detail/syarat_ketentuan/'; ?>" class="text-white">条款及细则</a>        
          <?php   } ?>



        </div>
    </div>
</footer>
<div class="menu-overlay"></div>
  <script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/popper.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/jquery.easing.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/slick.min.js' ?>"></script>

<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/landing/js/all.js' ?>"></script>
  <script>
    $('.testimoni-slide').slick({
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  </script>
</body>

</html>