
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/viewjs/custom.css'); ?>">
<?php
if(@$arr_css && count($arr_css) > 0) foreach ($arr_css as $css) {
    ?>
    <link href="<?php echo $css['src'];?>" rel="stylesheet" />
    <?php
}
?>

<?php
if(!empty($form_hidden)) foreach ($form_hidden as $fh) {
    echo $fh;
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-default" data-sortable-id="table-basic-1">
            <!-- begin box-header -->
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-6">
                        <?php
                        if(!empty($tombol) && is_array($tombol)) foreach ($tombol as $tomb) echo $tomb;else echo '&nbsp;';
                        ?>
                    </div>
                    <?php if(!empty($filter)){?>
                        <div class="col-sm-6">
                            <div class="btn-group m-t-2">
                                <form class="">
                                    <div class="form-group m-r-1">
                                        <div class="input-group m-b-10">
                                            <input type="text" class="form-control form-control-md" name="q"/>
                                            <span class="input-group-addon"><i class="fa fa-search fa-x2"></i></span>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    <?php };?>
                </div> 
            </div>
            <?php
            if(@$data_fixed) $cust_class = 'data-fixed';
            ?>
            <div class="box-body">
                <?php if(@$info) echo '<div class="row">'.$info.'</div>'; ?>
                <div class="table">
                    <div id="cetak-area" class="table-responsive <?php echo @$cust_class; ?>">
                        <?php echo @$table; ?>
                    </div>
                    <div class="col-sm-12 loading-table text-center">
                        <div class="loader" id="loader-6">
                          <span></span>
                          <span></span>
                          <span></span>
                      </div>
                  </div>
              </div>
              <?php if(!empty($paging)){ ?>
                <footer class="footer-content bg-silver">
                    <div class="pull-left">
                        <div class="text-inverse f-w-400 total-pagination"></div>
                    </div>
                    <div class="pull-right no-padding">
                        <ul class="pagination pagination-sm m-t-0 m-b-0"></ul>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <script src="<?php echo base_url('assets/js/pagination.js'); ?>"></script>
            <?php }?>
            <?php if(!empty($extra_html)) $this->load->view($extra_html); ?>
        </div>
        
    </div>
</div>
</div>
<script type="text/javascript">
    <?php echo @$extra_script; ?>
</script>
<?php
if(!empty($arr_js)) foreach ($arr_js as $src) {
  ?>
  <script <?php echo @$src['type'];?> src="<?php echo $src['src'];?>"></script>
  <?php
}
?>


<!-- modal -->
<div class="modal modal-message fade" id="modal-message">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Message Header</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <!-- <p>Text in a modal</p>
                <p>Do you want to turn on location services so GPS can use your location ?</p> -->
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-white close-modal" data-dismiss="modal">Tutup</a>
                <a href="javascript:;" class="btn btn-primary submit-modal">Lanjutkan</a>
            </div>
        </div>
    </div>
</div>
<!-- ./modal -->

<div class="hide print-head">
    <?php
    $appCapt = app_param();
    $pHead = $this->general_model->datagrabs(array('tabel'=>'ref_unit', 'where'=>array('id_unit'=>sesi('id_unit'))))->row();
    // cek($appCapt);
    ?>
    <div class="printhead-instansi">
      <img src="<?php echo base_url('assets/logo/brand.png'); ?>" alt="" width=80 style="float: left;position: relative; margin-right: 5px;">
      <img src="<?php echo base_url('assets/logo/'.$appCapt['aplikasi_logo']); ?>" alt="" width=80 style="float: right;position: relative; margin-left: 5px;top: 0px;">


      <p><?php echo $appCapt['pemerintah'] ?></p>
      <p><?php echo $pHead->unit ?></p>

      <hr>
    </div>
</div>

<script type="text/javascript">
    // cust remove
$('#modal-message').on('show.bs.modal', function (event) {
    // alert('horee');
    // return false;
      var button = $(event.relatedTarget) // Button that triggered the modal
      // var recipient = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      var act = button.data('act');
      console.log(modal);
      console.log(act);
      if(act == 'hapus'){
        modal.find('.modal-title').text('Konfirmasi Hapus');
        modal.find('.modal-body').text('Lanjutkan menghapus data ? ');
      }
      modal.find('.submit-modal').on('click', function(){           
        if(act == 'hapus') {
            window.location.href = button.data('url');
        }
      });
    });
// ./cust remove
</script>