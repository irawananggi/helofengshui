	<style type="text/css" media="screen">
		.inbox-chat > ul#chat-notif-box {
			padding:3px !important;
		}
		.inbox-chat ul > li {
			padding-bottom:5px !important;
		}

		.inbox-chat .chat-img img{
			margin-right: 5px;
		}

		.inbox-chat .list-group-item, .cursor {
			cursor: pointer;
		}


		.inbox-chat .roomlist-notif-box, .top-chat-prev
		{
			list-style: none;
			padding: 0;
			/*border-left: 1px #dddddd solid;*/
		}
		.inbox-chat .roomlist-notif-box {
			margin-left: 55px;
		}
		
		.inbox-chat .chat
		{
			list-style: none;
			margin: 0;
			padding: 0;
			/*border-left: 1px #dddddd solid;*/
		}

		.inbox-chat .chat .left {
			margin-left: 5px;
			margin-right: 10vw;
			text-align: justify;
		}
		.inbox-chat .chat .right {
			margin-left: 10vw;
			text-align: justify;
		}

		.inbox-chat .chat li
		{
			margin-bottom: 10px;
			padding-bottom: 5px;
			border-bottom: 1px dotted #B3A9A9;
		}

		.inbox-chat .chat li.left .chat-body
		{
			margin-left: 60px;

		}

		.inbox-chat .chat li.right .chat-body
		{
			margin-right: 60px;
		}
		.inbox-chat .chat li.right .chat-body p.text-message
		{
			background-color: papayawhip;

		}


		.inbox-chat .chat li .chat-body p
		{
			margin: 0;
			color: #777777;
		}

		.inbox-chat .text-cite {
			color: #d2d6de;
		}
	</style>

	<?php echo form_hidden('handler_url', site_url('elearn/konsultasi/')); ?>
	<?php echo form_hidden('chat-user', sesi('id_pegawai')); ?>

	<script src="<?php echo base_url('assets/js/elearn/webChatHandler.js');?>" type="text/javascript"></script>
	<script>

		let clientNotifColor = [];
		let prevNowNotifPlaying, roomNotifPlaying, ByIdNotifPlaying = false;
		var chatNotifParam = function () {
			var tmp = null;
			$.ajax({
				async: false,
				'type': "GET",
					// 'global': false,
					'dataType': 'json',
					'url': $('input[name=handler_url]').val()+'getchatparam',
					'success': function (res) {
						tmp = res;
					}
				});
			return tmp;
		}();

		function handleInbox(){
			let inboxHtml = $('ul.nav li.inbox-chat');
			if(!inboxHtml.length) $(`
				<li class="dropdown messages-menu inbox-chat">
				<a href="#chat-notif-box" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="chat-notif-box">
				<i class="fa fa-envelope-o"></i>
				<span class="label label-danger count-new-chat">...</span>
				</a>
				<ul class="dropdown-menu" id="chat-notif-box">
				</ul>
				</li>
				`).insertBefore('li.user-menu');

				$(function(){
					loadRoom();
				});

			function countMsg(){
				let listmsg = $('.inbox-chat #chat-notif-box > li');
				let countNewChat = 0;
				if(listmsg.length > 0){
					$.map(listmsg, function(msg) {
						// return something;
						countNewChat += parseInt($(msg).find('.count-chat').text());
					});
				}
				$('.count-new-chat').text(countNewChat);
			}

			function loadRoom(page, reset){
				reset = reset || 0;
				page = page || 1;
				let tmInterval = 5000;
				if(reset == 1) page=1;

				let Chat = new webChatHandler(chatNotifParam);
				let roomlist = Chat.roomlist(page);			

				if(roomlist.status == 'success'){
					let currPagination = roomlist.pagination;
					let currPage = roomlist.page;
					$.map(roomlist.result, function(item, index) {
						pushRoom(item);
						if(currPage == 1 && index == 0){
							$("ul#chat-notif-box > li.with-"+item.from).prependTo($("#chat-notif-box")).first();
						};
					});
					countMsg();
					if(currPagination.more) {
						loadRoom(currPage + 1, 0);
					}else{
						if(roomNotifPlaying) clearInterval(roomNotifPlaying);
						roomNotifPlaying = setInterval(function(){
							if(prevNowNotifPlaying) {
								if(prevNowNotifPlaying) clearInterval(prevNowNotifPlaying);
							}
							loadRoom(1, 1);
						}, tmInterval);
					}
				}else{
					if(roomNotifPlaying) clearInterval(roomNotifPlaying);
					roomNotifPlaying = setInterval(function(){
						if(prevNowNotifPlaying) {
							if(prevNowNotifPlaying) clearInterval(prevNowNotifPlaying);
						}
						loadRoom(1, 1);
					}, tmInterval);
				}
			}

			function pushRoom(res){
				const {from, to, sender, unik_sender, ct_chat, photo, exist_photo, inisial, last_chat} = res;
				if(!(from in clientNotifColor)) clientNotifColor[from] = getRandomColor();
				let this_clientID = $('input[name=chat-user]').val();
				let li_class;
				let user_ico_class;
				let chat_head;

				li_class = 'roomlist';
				user_ico_class = 'pull-left';
				chat_head = `<strong class="primary-font capt-nama">`+sender+`</strong>
				<small class="pull-right text-muted navi-room">
				<span class="badge bg-red pull-right count-chat">`+ct_chat+`</span>
				<div class="clear-fix"></div>
				<span class="pull-right m-r-3 cursor btn-room-detail"><i class="fa fa-angle-down fa-2x" role="button" data-toggle="collapse" href="#roomlist-notif-box-`+from+`" aria-expanded="false" aria-controls="roomlist-notif-box-`+from+`"></i></span>
				</small>`;

				let boxMessage = `
				<li class="`+li_class+` clearfix m-t-5 m-b-5 with-`+from+` cursor" data-id="`+from+`">
				<span class="chat-img `+user_ico_class+`">
				<img src="`+(exist_photo ? photo : `http://placehold.it/50/`+clientNotifColor[from]+`/fff&text=`+inisial)+`" alt="" class="p-2 img-circle" />
				</span>
				<div class="chat-body clearfix">
				<div class="header p-2">
				`+chat_head+`
				</div>
				<ul class="top-chat-prev">`+last_chat+`</ul>
				<ul class="roomlist-notif-box collapse" id="roomlist-notif-box-`+from+`"></ul>
				</div>
				</li>`;

				if(!$('ul#chat-notif-box > li').hasClass('with-'+from)) $('ul#chat-notif-box').append(boxMessage);
				$('ul#chat-notif-box > li.with-'+from+' .count-chat').text(ct_chat);
				$('ul#chat-notif-box > li.with-'+from+' .top-chat-prev').text(last_chat);
				if(ct_chat == 0) {
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .btn-room-detail i').removeClass('fa-angle-down fa-2x');
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .btn-room-detail i').addClass(' fa-ellipsis-h');
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .count-chat').hide();
					$('ul#chat-notif-box #roomlist-notif-box-'+from).empty();
				}else if(ct_chat > 0) {
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .btn-room-detail i').removeClass(' fa-ellipsis-h');
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .btn-room-detail i').addClass('fa-angle-down fa-2x');
					$('ul#chat-notif-box > li.with-'+from+' .navi-room .count-chat').show();
				}


				$(function(){
					$('#chat-notif-box li.roomlist').unbind('click').on('click', function(event){
						var $item = $( this ),
						$target = $( event.target );
						if($target.is("i.fa.fa-angle-down.fa-2x")){
							let isExpanded = $('#chat-notif-box #roomlist-notif-box-'+$(this).data('id')).attr('aria-expanded');
							if(isExpanded == "true") {
								if(ByIdNotifPlaying) clearInterval(ByIdNotifPlaying);
								$('#chat-notif-box li.with-'+$(this).data('id')+' ul.top-chat-prev').show();
							} else {
								loadChatPrevById($(this).data('id'));
								$('#chat-notif-box li.with-'+$(this).data('id')+' ul.top-chat-prev').hide();
							}
						} else {
							clearInterval(roomNotifPlaying);
							// redirect to konsultasi:
							window.location.href = $('input[name=handler_url]').val();
						}

					});

				});

			}

			function loadChatPrevById(id,page,reset){
				reset = reset || 0;
				page = page || 1;
				if(reset == 1) page=1;
				let tmInterval = 5000;

				let Chat = new webChatHandler(chatNotifParam);
				let chatprevbyid = Chat.chatprevbyid(id,page);
				if(chatprevbyid.status == 'success'){
					let currPagination = chatprevbyid.pagination;
					let currPage = chatprevbyid.page;
					let resultlen = chatprevbyid.result.length;
					$.map(chatprevbyid.result, function(item, index) {

						pushChatPrev(item);
					});
					if(currPagination.more) loadChatPrevById(id,currPage + 1,0);

					else{
						if(ByIdNotifPlaying) clearInterval(ByIdNotifPlaying);
						ByIdNotifPlaying = setInterval(function(){
							if(prevNowNotifPlaying) {
								if(prevNowNotifPlaying) clearInterval(prevNowNotifPlaying);
							}
							loadChatPrevById(id, 1, 1);
						}, tmInterval);
					}
				}else{
					if(ByIdNotifPlaying) clearInterval(ByIdNotifPlaying);
					ByIdNotifPlaying = setInterval(function(){
						if(prevNowNotifPlaying) {
							if(prevNowNotifPlaying) clearInterval(prevNowNotifPlaying);
						}
						loadChatPrevById(id, 1, 1);
					}, tmInterval);
				}
			}

			function pushChatPrev(res){
				let id_chat=res.id_chat, from=res.from, chat=res.chat, record=res.record, read=res.read;
				if(!$('#chat-notif-box li.with-'+from+' ul.roomlist-notif-box li').hasClass('chat-'+id_chat)) $('#chat-notif-box li.with-'+from+' ul.roomlist-notif-box').append(`<li class="chat-`+id_chat+`">
					<p class="p-2 text-message">`+chat+`</p>
					</li>`);
			}

		function getRandomColor() {
			var letters = '0123456789ABCDEF';
			var color = '';
			for (var i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)];
			}
			return color;
		}
	}



	var chatNotif = function () {
		"use strict";
		return {
			init: function () {
				handleInbox();
			}
		};
	}();

	$(document).ready(function() {
		chatNotif.init();
	});

</script>
