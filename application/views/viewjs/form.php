<?php
if(@$arr_css && count($arr_css) > 0) foreach ($arr_css as $css) {
	?>
	<link href="<?php echo $css['src'];?>" rel="stylesheet" />
	<?php
}
?>


<div class="row">
	<div class="col-lg-8">
		<div class="box box-inverse" data-sortable-id="table-basic-5">
			<div class="box-body">
				<form action="<?php echo $aksi; ?>" method="POST" enctype="multipart/form-data">
					<?php
					// cek($def);
					foreach ($forms as $f) {
						// cek($f['name']);
						// cek($def->{$f['name']});
						switch ($f['tipe']) {
							case 'input':
								// code...
							?>
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"><?php echo $f['label'] ?></label>
								<div class="col-md-10">
									<?php echo form_input($f['name'], @$def->{$f['name']}, 'class="form-control"'); ?>
								</div>
							</div>
							<?php
							break;
							case 'textarea':
								// code...
							?>
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"><?php echo $f['label'] ?></label>
								<div class="col-md-10">
									<?php echo form_textarea($f['name'], @$def->{$f['name']}, 'class="form-control text-area"'); ?>
								</div>
							</div>
							<?php
							break;
						}
					}
					?>
					
					<input type="hidden" name="id" value="<?php echo @$def->id;?>">
					<?php echo anchor($this->hal, 'Kembali', 'class="btn btn-default btn-sm pull-left"'); ?>
					<button type="submit" class="btn btn-success btn-sm pull-right">Simpan</button>
				</form>
			</div>            
		</div>    
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		initMce()
	});
</script>

<?php
if(!empty($arr_js)) foreach ($arr_js as $src) {
	?>
	<script <?php echo @$src['type'];?> src="<?php echo $src['src'];?>"></script>
	<?php
}
?>


