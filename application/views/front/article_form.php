<?php $code = $this->uri->segment(4); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.add_cat').hide();
		contentloader('<?php echo site_url("cms/articles/refresh_cat/".$sec."/".$id) ?>','#div-cat');
		contentloader('<?php echo site_url("cms/categories/list_parent_cat/".$sec) ?>','#parent_cat');
		
		<?php if($code == 1 || $code == 2){ ?>
			$('#date_start').datetimepicker();
		<?php } ?>
	});

	function new_cat(){
		$('.add_cat').show();
	}

	function batal_cat(){
		$('.add_cat').hide();
		resetForm("form_cat");
	}

	function simpan_cat(val){
		$.ajax({
			type : "POST",
			url : val.attr('action'),
			data : val.serialize(),
			dataType: "json",
			success : function(msg){
				if (parseInt(msg.sign)==1){
					setTimeout("contentloader('<?php echo site_url("cms/articles/refresh_cat/".$sec."/".$id) ?>','#div-cat')",1000);
					setTimeout("contentloader('<?php echo site_url("cms/categories/list_parent_cat/".$sec) ?>','#parent_cat')",1000);
					$('.add_cat').hide();
					resetForm("form_cat");
				}
			}
		});
		return false;
	}
	
	function getPermalink(data){
		var permalink = data.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '_').toLowerCase();
		$('.url').html('<?php echo site_url('berita')?>/'+permalink);
		$('.hi_permalink').attr('value',permalink);
	}

	tinymce.init({
		selector: "textarea",
		mode: "exact",
		theme: "modern",
		width: '300',
		height: "150",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality filemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image preview media | print | code",
		image_advtab: true,
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		]
	});

	function simpan_artikel() {
		var no = 0;
		$('.on_check').each(function() {
			if ($(this).is(':checked')) no += 1;
		});
		
		if (no > 0) $('#form_article').submit();
		else $('#alert').addClass('alert alert-error').html('Kategori harus dipilih').show();
		
	}
</script>

<?php (isset($default))? $row = $default->row() : $row = ''; ?>

<fieldset>
  <legend>
    <?php echo $title?>
  </legend>
  <div id="alert"></div>
	<?php echo form_open_multipart('cms/articles/save_article','id=form_article');?>
	<?php echo form_hidden('id_art',(!empty($row))?$row->id_article:'');?>
	<input type='hidden' name='sec' value='<?php echo $code?>' class='code'>
	<?php if($code != '3'){ echo form_hidden('date_end','0000-00-00 00:00:00'); } ?>
	
	<div class="pull-left">
		<div class="hero-unit" style="float:left;width:580px;background:#f4edf7;border:1px solid #999;padding:10px!important;">
		<!-- Text input-->
			
			<div class="control-group">
				<p><label for="title">Judul</label>
				<input value="<?php echo (!empty($row))?$row->title:'';?>" id="title" name="title" type="text" autocomplete="off" onkeyup="getPermalink(this.value)" placeholder="" required="" style="width: 76%" title="Judul Artikel Tidak Boleh Kosong">  
				</p>
				  
				<?php if($code == 1 || $code == 2 || $code == 3 || $code == 4 || $code == 14){ ?>
					<p><input type="hidden" name="permalink" class="hi_permalink" value="<?php echo @$row->permalink ?>">
					<span class="permalink">Permalink</span> : <span class="url"></span></p>
				<?php } ?>
			</div>

			<p><textarea name="content"><?php echo (!empty($row))?stripslashes($row->content):'';?></textarea></p>
			
			<?php if($code == '3'){?>
				<p><label>Tanggal Mulai</label>
				<?php echo form_input(array('name'=>'date_start','id'=>'date_start'),!empty($row->date_start)?$row->date_start:null)?></p>
				
				<p><label>Tanggal Selesai</label>
				<?php echo form_input(array('name'=>'date_end','id'=>'date_end'),!empty($row->date_end)?$row->date_end:null)?></p>
			<?php } ?>
			
			<p><label>Lampiran</label>
			<?php echo form_upload(array('name'=>'fupload','style'=>'border:1px solid #ccc'))?></p>
			
			<?php
				if(!empty($row->id_article)) {$cl = 'btn-warning'; $tbl = 'Update';}
				else{ $cl = 'btn-success';  $tbl = 'Simpan';}
			?>
			
			<a class="btn <?php echo $cl?>" onclick="simpan_artikel()"><i class="icon-white icon-ok-sign"></i> <?php echo $tbl?></a>
			<?php if(!empty($row->id_article)){ ?>
			<a href="javascript:history.go(-1)" class="btn btn-danger"><i class="icon-white icon-remove"></i> Batal</a>
            <?php } ?>

  <!--          <div class="btn-group">
  <a class="btn dropdown-toggle btn-success" data-toggle="dropdown" href="#">
    Simpan &nbsp; <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
    <li><?php echo anchor('#','Simpan','class="btn-simpan-only"') ?></li>
    <li><?php echo anchor('#','Simpan dan Tutup','class="btn-simpan-exit"') ?></li>
    <li><?php echo anchor('#','Simpan dan Buat Baru','class="btn-simpan-new"') ?></li>
  </ul>
</div>-->
<input type="hidden" name="pilihan" id="pilihan" value="1"/>

<a class="btn btn-danger" href="<?php echo site_url('/cms/articles/list_data/1')?>"><i class="icon-white icon-remove"></i> Batal</a>

		</div>
	</div>
	<div class="pull-right">
		<?php if($code == 1 || $code == 2 || $code == 4 || $code == 14){ ?>
			<p><label>Tanggal Publikasi</label>
			<input type="text" name="date_start" value="<?php echo @$row->date_start; ?>" id="date_start"></p>
		<?php } ?>
		<div id="div-cat"></div>
		<?php echo form_close();?>
		<?php echo form_open('cms/categories/save_cat_flash/'.$code,array('id'=>'form_cat')); ?>
			<table class="add_cat">
				<input type="hidden" name="code" value="<?php echo $code ?>">
				<tr style="vertical-align:middle">
					<td width="80">Kategori</td>
					<td width="10">:</td>
					<td><input type="text" name="cat" id="category"></td>
				</tr>
				<tr style="vertical-align:middle">
					<td>Slug</td>
					<td>:</td>
					<td><input type="text" name="slug" id="slug"></td>
				</tr>
				<tr style="vertical-align:middle">
					<td>Parent</td>
					<td>:</td>
					<td><span id="parent_cat"></span></td>
				</tr>
				<tr style="vertical-align:middle">
					<td></td>
					<td></td>
					<td><span class="btn btn-success" id="submit_cat" onclick = "simpan_cat($('#form_cat'))"><i class="icon-white icon-ok"></i> Simpan</span>
						<span class="btn btn-danger" id="batal_cat" onclick = "batal_cat()"><i class="icon-white icon-remove"></i> Batal</span>
					</td>
				</tr>
			</table>
		<?php echo form_close(); ?>
	</div>
</fieldset>
<script type="text/javascript" >
	$('#date_start').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('#date_end').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
    $(document).ready(function() {
        
        
        $('.btn-simpan-only').click(function() {
            return kirimin(1);
        });
        $('.btn-simpan-exit').click(function() {
            return kirimin(2);
        });
        $('.btn-simpan-new').click(function() {
            return kirimin(3);
        });
    });
    
    function kirimin(isi){
        if(cekkategori){
        $('#pilihan').val(isi);
        if($('#title').val()){
            $('#form_page').submit();
        }else{
            alert('judul Harap di Isi');
        }
        }
        return false;
    }
    
    function cekkategori(){
        var no = 0;
        $('.on_check').each(function() {
            if ($(this).is(':checked')) no += 1;
        });
        
        if (no > 0) {
            return true;
        }
        else{
            $('#alert').addClass('alert alert-error').html('Kategori harus dipilih').show();
            return false;
        } 
    }
</script>
	