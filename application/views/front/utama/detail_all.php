<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Sekolah Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta name="twitter:title" content="">
<meta name="twitter:image" content="">
<meta name="twitter:url" content="">
<meta name="twitter:card" content="">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">

    <link href="http://localhost/elearning/assets/css/admin-lte.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/animate.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/style-2.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/icomoon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/bootstrap.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/magnific-popup.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/flexslider.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.carousel.min.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.theme.default.min.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/fonts/flaticon/flaticon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/style.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/slide-jadwal/css/style.css';?>">

<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.no-padding{
			padding: 0px !important;
		}
		.classes {
    height: 600px;
}
		h3, .h3 {
    font-size: 22px;
}
#colorlib-hero p {
    padding: 20px;
}
#colorlib-hero p img{
	width:80%;
	height: auto;
}
	</style>


<script src="<?php echo base_url().'assets/front/utama/highcharts/highcharts.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/highcharts/highcharts-3d.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/highcharts/exporting.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/highcharts/export-data.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/highcharts/accessibility.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/slide-jadwal/js/jquery-2.0.3.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/slide-jadwal/js/jquery.carousel.js';?>" type="text/javascript"></script>

</head>
<body>

	<nav class="colorlib-nav" role="navigation">


<?php 

				$cek_lev_unit = $this->general_model->datagrab(array(
				'tabel'=> 'ref_unit','where'=>array('id_unit'=>@$id_unit)))->row();
if($cek_lev_unit->level_unit == 2){
	$bg = '-webkit-linear-gradient(right, #ffffff 0, #ffffff 0%, #003eab 100%) !important;';
}elseif ($cek_lev_unit->level_unit == 3) {
	
	$bg = '-webkit-linear-gradient(right, #ffffff 0, #ffffff 0%, #32a5d2 100%) !important;';
}else{

	$bg = '-webkit-linear-gradient(right, #ffffff 0, #ffffff 0%, #cc3333 100%) !important;';
}
?>

<div class="upper-menu" style="padding:0px;background:<?php echo $bg;?>">
<div class="container">
<div class="row">

<div class="col-md-10">
	<?php
						$ava = empty($par['pemerintah_logo']) ? base_url().'uploads/logo/'.$par['pemerintah_logo'] : base_url().'assets/logo/brand.png'; ?>
							<div class="col-md-2">
							<img src="<?php echo $ava ?>" style="">
							</div>
							<div class="col-md-10" style="padding:15px 0px 0px 0px;">
							<h2 style=" text-shadow: 1px 1px 0px #da251d;width:88%;float:left;margin:0px;font-weight:bold;font-size:24px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $nama_unit; ?></h2>
							<h2 style="text-shadow: 1px 1px 0px #da251d;width:88%;float:left;margin:0px;font-weight:bold;font-size:20px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $par['pemerintah']; ?></h2>
						</div>
						


</div>

<div class="col-xs-2 text-right">
<p>
</p><ul class="colorlib-social-icons" style="
    margin-top: 45px;
">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-linkedin"></i></a></li>
<li><a href="#"><i class="icon-dribbble"></i></a></li>
</ul>
<p></p><!-- 
<p class="btn-apply"><a href="#">Apply Now</a></p> -->
</div>
</div>
</div>
</div>
<div class="top-menu"  style="background: linear-gradient(to right, <?php echo @$set_widget_1->background_title;?>, #96b4d3); ">
<div class="container">
<div class="row">
<div class="col-md-12 text-right menu-1">
<?php echo get_menu_portal(@$id_unit)?>
</div>
</div>
</div>
</div>

<div class="col-xs-12"  style="background:<?php echo $bg;?>">
	<?php echo get_marquee(@$id_unit)?>
</div>
</nav>

<div class="container no-padding" style="margin:0px auto;/*box-shadow: 0px 27px 33px #5d5d5d;*/height: auto !important">
<div class="colorlib-loader" style="display: none;"></div>
<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle colorlib-nav-white"><i></i></a><div id="colorlib-offcanvas"><?php echo get_menu_portal(@$id_unit)?>
</div>

<aside id="colorlib-hero">
<div class="container" style="margin-top:20px;line-height: 35px; text-align: justify;">
	<?php echo $content;?>
</div>
</aside>

</div>
<div id="colorlib-intro">

<footer id="colorlib-footer">
<div class="container">
<div class="row row-pb-md">

	<div class="<?php echo @$set_widget_6->nama_col;?> no-padding" style="height:<?php echo @$set_widget_6->tinggi;?>px; margin-bottom:20px;">
								<div class="pull-left">
									<?php echo @$btn_tambah_6;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_6;?>
								</div>
								<?php
								if(@$dt_widget_6->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_6->num_rows();
									foreach ($dt_widget_6->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();


				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'limit'=>6,'where'=>array('a.status'=>1,'a.id_unit'=>$id_unit)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="col-md-12">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h4>'.$satu->judul.'</h4>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url('.base_url().'uploads/file/berita/'.$ber->berkas_file.');">
<span class="price text-center"><small>'.$ber->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_unit'=>$ber->id_unit))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_unit'=>$ber->id_unit))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1,'id_unit'=>$id_unit)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container2">
					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container2">
										 		<h4 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h4>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_unit'=>$ber->id_unit))).'">'.$ber->title.'</a></li>';
				}
				echo '
					</ul>
					</div>
					</div>
				';
				break;
			case 3:
				
				
				$from_pengumuman = array(
					'front_pengumuman a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_pengumuman = $this->general_model->datagrab(array(
				'tabel'=> $from_pengumuman,'limit'=>6,'where'=>array('a.status'=>1,'a.id_unit'=>$id_unit)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="col-md-12">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h4>'.$satu->judul.'</h4>
</div>





					';
				foreach ($dt_pengumuman->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">

<div class="desc">
<h3><a href="'.site_url('Front/detail_pengumuman/'.in_de(array('id_pengumuman'=>$ber->id_pengumuman,'id_unit'=>$ber->id_unit))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_pengumuman/'.in_de(array('id_pengumuman'=>$ber->id_pengumuman,'id_unit'=>$ber->id_unit))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';

				break;
			case 4:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1,'id_unit'=>$id_unit)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container2">
					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container2">
										 		<h4 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h4>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li><a href="'.site_url('Front/detail_pengumuman/'.in_de(array('id_pengumuman'=>$ber->id_pengumuman,'id_unit'=>$ber->id_unit))).'">'.$ber->title.'</a></li>';
				}
				echo '
					</ul>
					</div>
					</div>
				';
				break;
			case 6:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_runingtext','where'=>array('status'=>1,'id_unit'=>$id_unit)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '<marquee>';
				foreach ($dt_berita->result() as $ber) {
					echo '<span> '.$ber->title.' || </span>';
				}
				echo '</marquee>';
				break;
			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('id_unit'=>$id_unit,'status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container2-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_tautan','order'=>'title ASC','where'=>array('status'=>1,'id_kategori_konten'=>$satu->param1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
				<div class="col-md-12 no-padding" style="margin-bottom:10px;">
						<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
					<h4>'.$satu->judul.'</h4>
					</div>';
					if($satu->param1==1){
						$xx = 'color-3';
					}else{

						$xx = 'color-2';
					}
				foreach ($dt_berita->result() as $ber) {
					echo '			
					<div class="col-md-6">
					<div class="intro-flex">
					<div class="one-third '.$xx.' animate-boxx">
					<span class="icon"><i class="flaticon-market"></i></span>
					<div class="desc">
					<h4 style="color:#fff !important;">'.$ber->title.'</h4>
					<p><a href="'.$ber->link.'" class="view-more">Lihat Portal</a></p>
					</div>
					</div>
					</div>
					</div>
					';
				}

				echo '

</div>
</div>';
				
				
				break;
			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container2">
					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container2">
											 		<h4>'.$satu->judul.'</h4>
										 	</div>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
<div class="about-desc animate-boxx">
<h4 style="font-family: arial;">'.$satu->judul.'</h4>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
       </div>
';
			break;

			
			case 11:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				$from = array(
					'elearn_jadwal_mapel a'=>'',
					'elearn_kelas_mapel b'=>array('b.id_mapel = a.id_mapel','left'),
					'elearn_kelas c'=>array('c.id_kelas = b.id_kelas','left'),
					'elearn_ref_kelas t'=>array('t.id_ref_kelas = c.id_ref_kelas','left'),
					'elearn_ref_kel mapel'=>array('mapel.id_ref_kel = b.id_ref_kel','left'),
					'elearn_kuri d'=>array('d.id_kuri = c.id_kuri','left'),
					'ref_unit e'=>array('e.id_unit = d.id_unit','left')
				);

				$dt_jd = $this->general_model->datagrab(array('tabel'=>$from,'where'=>array('d.id_unit'=>$id_unit,'a.tanggal'=>date('Y-m-d')), 'order'=>'a.tanggal ASC'));	
				echo '

					<div class="container" style="
    background: #fff;margin-bottom:20px;
">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>';
			
					echo '

 
    <link href="'.base_url().'assets/front/utama/slide-jadwal/favicon2.png" rel="shortcut icon" type="image/gif">


<script>
    var BASE_URL = emBaseURL = "";
    var SOUTHAFRICA_V1 = "0";
    var notesUrl = BASE_URL;
    var commentsUrl = BASE_URL;
    var CdnPath = "//www.kelaspintar.id";
    var webMode = \'website\';
    var reqUrl = BASE_URL;
    var misStartTime = 0;
    var misBoardId = 0;
    var viera = 0;
</script>
<section class="additional-features-sec mb50">
	<div class="container start-learning-now">
		<div class="row clearfix mt30">
			<div class="col-sm-offset-1 col-sm-10">
				<div class="mt50">
					
					<script type="text/javascript">
						$( function () {

							if ( $( window ).width() >= 769 ) {
								$( \'.start-learning-now-slider\' ).jCarousel( {
									type: \'slidey-up\',
									carsize: {
										carheight: 240
									},
									//	 auto: 50000,
									speed: 5000,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}
							if ( $( window ).width() <= 768 ) {
								$( \'.start-learning-now-slider\' ).jCarousel( {
									type: \'slidey-up\',
									carsize: {
										carheight: 450
									},
									//	 auto: 5000,
									speed: 50000,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}

						} );
					</script>
					

					 <link href="" rel="shortcut icon" type="image/gif">
					<div class="start-learning-now-slider" style="height: 240px; overflow: hidden; position: relative;">';

                                        if ($dt_jd->num_rows() > 0) {
					foreach ($dt_jd->result() as $row) {
						
							$from = array(
								'elearn_jadwal_mapel a'=>'',
								'elearn_kelas_mapel b'=>array('b.id_mapel = a.id_mapel','left'),
								'elearn_kelas c'=>array('c.id_kelas = b.id_kelas','left'),
								'peg_pegawai d'=>array('d.id_pegawai = b.id_pemateri','left'),
					            'elearn_ref_kelas t'=>array('t.id_ref_kelas = c.id_ref_kelas','left'),
					            'elearn_ref_kel mapel'=>array('mapel.id_ref_kel = b.id_ref_kel','left')
					        );
							$select='a.*, d.nama AS pengajar, t.kelas, mapel.kelompok';
						
							$det_ju = $this->general_model->datagrabs(array(
								'tabel'=>$from,
								'where'=>array('a.id_jmap'=>@$row->id_jmap),
								'select'=>$select
							))->row();
						echo ' <div class="item select" style="width:100%">
							<h2 class="heading em-student-bottom-heading">'.$row->unit.'</h2>
							<br>
							<div class="col-sm-6 col-xs-12 mt50 additional-feature-left-content">
								<p><h3>'.@$row->kelas.'</h3>
								<p> Mata Pelajaran : <h3>'.@$row->kelompok.'</h3><p>
								<p><i class="icon-calendar"></i> : '.konversi_tanggal("D",substr(@$row->tanggal,0,10),"id").', '.tanggal_indo(@$row->tanggal).'</p>
							</div>
							<div class="col-sm-6 col-xs-12 mt50 additional-feature-left-content">
<div class="box-header with-border">
										<h3 class="box-title">Materi : <b>'.$det_ju->judul.'</b></h3>
            						</div>';
            						$jmat = $this->general_model->datagrab(array(
                                            'tabel' => array(
                                                'elearn_jadwal_materi a'=>'',
                                                'elearn_kel_mat b'=>'b.id_kel_mat = a.id_kel_mat',
                                               /*  'elearn_kel_mat_det f'=>'f.id_kel_mat = b.id_kel_mat',
                                                'elearn_materi c'=>'c.id_materi = f.id_materi', */
                                            ),
                                            'select'=>'b.judul, a.*',
                                            'where' => array('a.id_jmap' => @$row->id_jmap)
                                        ));
                                        if ($jmat->num_rows() > 0) {
                                        	foreach ($jmat->result() as $a) {
                                        	echo '<div class="box box-default box-solid">
                                                <div class="box-header with-border">
                                                    <h4 class="box-title" style="font-size: 16px;"> '.$a->judul.'</h4>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
												</div>
												<div class="box-footer no-padding" style=""><ul class="nav nav-pills nav-stacked">';
												$daf_materi = $this->general_model->datagrab(array(
													'tabel' => array(
														'elearn_kel_mat_det f'=>'f.id_kel_mat = b.id_kel_mat',
														'elearn_materi c'=>'c.id_materi = f.id_materi',
													),
													'select'=>'c.judul, f.*',
													'where' => array('f.id_kel_mat' => @$a->id_kel_mat)
												));
												foreach ($daf_materi->result() as $m) {	
													echo '
														<li style="text-align:left"> - '.$m->judul.'</li>';
												}		
												echo '</ul>
												</div>
                                            </div>';
                                            } 
                                    	 }else{
										echo '
											<div class="box-body">
												<div class="alert alert-confirm pull-left">materi belum ditentukan</div><div class="clear"></div>
											</div>';
                                    }
echo'
							</div>
						</div>';
					 }
					}else{
										echo '
											<div class="box-body">
												<div class="alert alert-confirm pull-left">Tidak Ada Kelas berlangsung</div><div class="clear"></div>
											</div>';
                                    }

echo '</div>
				</div>

			</div>
		</div>
	</div>
</section>

					';
				
				echo '
					</div>
				';
				break;
			
			case 12:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				$from = array(
					'elearn_jadwal_ujian a'=>'',
					'elearn_kelas_mapel b'=>array('b.id_mapel = a.id_mapel','left'),
					'elearn_kelas c'=>array('c.id_kelas = b.id_kelas','left'),
					'elearn_ref_kelas t'=>array('t.id_ref_kelas = c.id_ref_kelas','left'),
					'elearn_ref_kel mapel'=>array('mapel.id_ref_kel = b.id_ref_kel','left'),
					'elearn_kuri d'=>array('d.id_kuri = c.id_kuri','left'),
					'ref_unit e'=>array('e.id_unit = d.id_unit','left')
				);

				$dt_jd = $this->general_model->datagrab(array('tabel'=>$from,'where'=>array('d.id_unit'=>$id_unit,'a.tanggal'=>date('Y-m-d')), 'order'=>'a.tanggal ASC'));	
				echo '

					<div class="container" style="
    background: #fff;margin-bottom:20px;
">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>';
			
					echo '

 
    <link href="'.base_url().'assets/front/utama/slide-jadwal/favicon2.png" rel="shortcut icon" type="image/gif">


<script>
    var BASE_URL = emBaseURL = "";
    var SOUTHAFRICA_V1 = "0";
    var notesUrl = BASE_URL;
    var commentsUrl = BASE_URL;
    var CdnPath = "//www.kelaspintar.id";
    var webMode = \'website\';
    var reqUrl = BASE_URL;
    var misStartTime = 0;
    var misBoardId = 0;
    var viera = 0;
</script>
<section class="additional-features-sec mb50">
	<div class="container start-learning-now">
		<div class="row clearfix mt30">
			<div class="col-sm-offset-1 col-sm-10">
				<div class="mt50">
					
					<script type="text/javascript">
						$( function () {

							if ( $( window ).width() >= 769 ) {
								$( \'.start-learning-now-slider12\' ).jCarousel( {
									type: \'slidey-up\',
									carsize: {
										carheight: 240
									},
									//	 auto: 50000,
									speed: 5000,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}
							if ( $( window ).width() <= 768 ) {
								$( \'.start-learning-now-slider12\' ).jCarousel( {
									type: \'slidey-up\',
									carsize: {
										carheight: 450
									},
									//	 auto: 5000,
									speed: 50000,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}

						} );
					</script>
					

					 <link href="" rel="shortcut icon" type="image/gif">
					<div class="start-learning-now-slider12" style="height: 240px; overflow: hidden; position: relative;">';

                                        if ($dt_jd->num_rows() > 0) {
					foreach ($dt_jd->result() as $row) {
						
							$from = array(
								'elearn_jadwal_ujian a'=>'',
								'elearn_kelas_mapel b'=>array('b.id_mapel = a.id_mapel','left'),
								'elearn_kelas c'=>array('c.id_kelas = b.id_kelas','left'),
								'peg_pegawai d'=>array('d.id_pegawai = b.id_pemateri','left'),
					            'elearn_ref_kelas t'=>array('t.id_ref_kelas = c.id_ref_kelas','left'),
					            'elearn_ref_kel mapel'=>array('mapel.id_ref_kel = b.id_ref_kel','left')
					        );
							$select='a.*, d.nama AS pengajar, t.kelas, mapel.kelompok';
						
							$det_ju = $this->general_model->datagrabs(array(
								'tabel'=>$from,
								'where'=>array('a.id_ujian'=>@$row->id_ujian),
								'select'=>$select
							))->row();

                                        if ($dt_jd->num_rows() == 1) {
                                        	$dd = 'itemx selectx';
                                        }else{

                                        	$dd = 'item select';
                                        }
						echo ' <div class="'.$dd.'" style="width:100%">
							<h2 class="heading em-student-bottom-heading">'.$row->unit.'</h2>
							<br>
							<div class="col-sm-6 col-xs-12 mt50 additional-feature-left-content">
								<p><h3>'.@$row->kelas.'</h3>
								<p> Mata Pelajaran : <h3>'.@$row->kelompok.'</h3><p>
							</div>
							<div class="col-sm-6 col-xs-12 mt50 additional-feature-left-content">
<div class="box-header with-border">
										<h3 class="box-title">Materi : <b>'.$det_ju->judul.'</b></h3>
								<br><br><p><i class="icon-calendar"></i> : '.konversi_tanggal("D",substr(@$row->tanggal,0,10),"id").', '.tanggal_indo(@$row->tanggal).'</p>
								<p>Mulai : '.@$row->mulai.' s/d '.@$row->selesai.'</p>
            						</div>';
            						
echo'
							</div>
						</div>';
					 }
					}else{
										echo '
											<div class="box-body">
												<div class="alert alert-confirm pull-left">Tidak Ada Ujian berlangsung</div><div class="clear"></div>
											</div>';
                                    }

echo '</div>
				</div>

			</div>
		</div>
	</div>
</section>

					';
				
				echo '
					</div>
				';
				break;
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
$jumlah_unit = $this->general_model->datagrab(array(
							'tabel'=>'ref_unit', 
                			'where'=>array('aktif'=>1,'id_unit'=>$id_unit,'level_unit !='=>'NULL'),
               				'select'=>"count(*)jm"));


		$from = array(
			'peg_pegawai a' => '',
			'pegawai_role b'=>array('a.id_pegawai=b.id_pegawai','left'),
			'ref_role c'=>array('c.id_role=b.id_role','left'),
			'peg_jabatan d'=>array('d.id_pegawai=a.id_pegawai','left')
		);

		$jumlah_pengajar = $this->general_model->datagrab(array('tabel'=>$from,'where'=>array('d.id_unit'=>$id_unit,'c.id_role'=>4),'select'=>"count(*)jm"));


		$jumlah_peserta = $this->general_model->datagrab(array('tabel'=>$from,'where'=>array('d.id_unit'=>$id_unit,'c.id_role'=>5),'select'=>"count(*)jm"));



				/*$jumlah_pengajar = $this->general_model->datagrab(array(
							'tabel'=>'pegawai_role', 
                			'where'=>array('id_role'=>4),
               				'select'=>"count(*)jm"));*/
				/*$jumlah_peserta = $this->general_model->datagrab(array(
			                'tabel'=>'pegawai_role pr', 
			                'where'=>array('id_role'=>5),
               				'select'=>"count(*)jm"));*/
				$jumlah_kelas = $this->general_model->datagrab(array(
			                'tabel'=>'elearn_ref_kelas', 
			                'where'=>array('id_unit'=>$id_unit),
			                'select'=>"count(*)jm"));



				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/uploads/file/berita/img_bg_1.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container2">
<div class="row">
<div class="col-md-10 col-md-offset-1">

<div class="col-md-4 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_kelas->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_kelas->row('jm').'</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_pengajar->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_pengajar->row('jm').'</span>
<span class="colorlib-counter-label">Guru</span>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_peserta->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_peserta->row('jm').'</span>
<span class="colorlib-counter-label">Siswa</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			case 16:
			//jadwal kelas
			$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
					<div class="about-desc animate-boxx">
					<h4 style="font-family: arial;">'.$satu->judul.'</h4>
						<div class="fancy-collapse-panel">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					';
					echo '

       </div>
       </div>
       </div>
       </div>
';
			break;
			case 17:
			//jadwal ujian
			$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
					<div class="about-desc animate-boxx">
					<h4 style="font-family: arial;">'.$satu->judul.'</h4>
						<div class="fancy-collapse-panel">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					';
					echo '

       </div>
       </div>
       </div>
       </div>
';
			break;
case 18:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '

<figure class="highcharts-figure">
    <div id="container2'.$satu->id_konten.$satu->posisi.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	Highcharts.chart('container2".$satu->id_konten.$satu->posisi.$satu->posisi."', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: '".$satu->judul."'
    },
    accessibility: {
        point: {
            valueSuffix: ' Pengajar'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f} Pengajar</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        data: [
            
            ['SDN1 Canden', 30],
            ['SDN1 Pamulang', 55],
            {
                name: 'SDN1 Inpres Monaco',
                y: 40,
                sliced: true,
                selected: true
            }
        ]
    }]
});

</script>

<style type='text/css'>
	#container2".$satu->id_konten.$satu->posisi.$satu->posisi." {
  height: 400px; 
    border: 1px solid #CCE;
}

.highcharts-figure".$satu->id_konten.$satu->posisi.$satu->posisi.", .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

</style>

				";
				break;

				case 19:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '

<figure class="highcharts-figure">
    <div id="container2'.$satu->id_konten.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container2".$satu->id_konten.$satu->posisi."',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    
    title: {
        text: '".$satu->judul."'
    },
    subtitle: {
       /* text: 'Source: WorldClimate.com'*/
    },
    xAxis: {
        categories: [
            'SDN1 Inpres Monaco',
            'SDN1 Pamulang',
            'SDN1 Canden'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Siswa'
        }
    },
    tooltip: {
        headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
        pointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>' +
            '<td style=\"padding:0\"><b>{point.y:.1f} Siswa</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '".$satu->judul."',
        data: [50, 71, 106]

    }]
});

</script>

<style type='text/css'>
.highcharts-figure".$satu->id_konten.$satu->posisi.", .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    
}

#container2".$satu->id_konten.$satu->posisi." {
    height: 400px;
    border: 1px solid #CCE;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

				";
				break;
			
			case 20:
			//jadwal ujian
			$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
					<div class="about-desc animate-boxx">
					<h4 style="font-family: arial;">'.$satu->judul.'</h4>
						<div class="fancy-collapse-panel">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					';
					echo '

       </div>
       </div>
       </div>
       </div>
';
			break;
			case 21:
			//jadwal ujian
			$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container2">
					<div class="about-desc animate-boxx">
					<h4 style="font-family: arial;">'.$satu->judul.'</h4>
						<div class="fancy-collapse-panel">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					';
					echo '

       </div>
       </div>
       </div>
       </div>
';
			break;
			case 22:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="container2">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h4>Mata Pelajaran</h4>
</div>
	<div id="colorlib-services">

<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>


</div>
</div>
<div class="copy"  style="background: <?php echo $bg;?>;">
<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<p>


<small class="block"> <?php echo $par['instansi'] ?> - <?php echo $par['pemerintah'] ?></a></small><br>
</p>
</div>
</div>
</div>
</div>
</footer>

<div class="gototop js-top">
<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.easing.1.3.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/bootstrap.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.waypoints.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.stellar.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.flexslider-min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/owl.carousel.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.magnific-popup.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/js/magnific-popup-options.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.countTo.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/main.js';?>" type="text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="ext/javascript"></script>
<script type="ext/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="49" defer=""></script></body>
</html>