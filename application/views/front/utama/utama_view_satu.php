<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>E-Learning</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta name="twitter:title" content="">
<meta name="twitter:image" content="">
<meta name="twitter:url" content="">
<meta name="twitter:card" content="">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/animate.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/icomoon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/bootstrap.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/magnific-popup.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/flexslider.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.carousel.min.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.theme.default.min.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/fonts/flaticon/flaticon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/style.css';?>">

<script src="<?php echo base_url().'assets/front/utama/js/modernizr-2.6.2.min.js';?>" type="text/javascript"></script>

<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.no-padding{
			padding: 0px !important;
		}
	</style>
</head>
<body>
<div class="colorlib-loader" style="display: none;"></div>
<div id="page"><a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle colorlib-nav-white"><i></i></a><div id="colorlib-offcanvas"><ul>
<li class="active"><a href="index.html">Home</a></li>
<li class="offcanvas-has-dropdown">
<a href="courses.html">Courses</a>
<ul class="dropdown">
<li><a href="courses-single.html">Courses Single</a></li>
<li><a href="#">Mobile Apps</a></li>
<li><a href="#">Website</a></li>
<li><a href="#">Web Design</a></li>
<li><a href="#">WordPress</a></li>
</ul>
</li>
<li><a href="about.html">About</a></li>
<li><a href="event.html">Events</a></li>
<li><a href="news.html">News</a></li>
<li><a href="contact.html">Contact</a></li>
<li class="btn-cta"><a href="#"><span>Free Trial</span></a></li>
</ul></div>
<nav class="colorlib-nav" role="navigation">
<div class="upper-menu">
<div class="container">
<div class="row">
<div class="col-xs-4">
<marquee>Selamat Datang</marquee>
</div>
<div class="col-xs-6 col-md-push-2 text-right">
<p>
</p><ul class="colorlib-social-icons">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-linkedin"></i></a></li>
<li><a href="#"><i class="icon-dribbble"></i></a></li>
</ul>
<p></p>
<p class="btn-apply"><a href="#">Apply Now</a></p>
</div>
</div>
</div>
</div>
<div class="top-menu"  style="background: <?php echo @$set_widget_1->background_title;?>">
<div class="container">
<div class="row">
<div class="col-md-4" style="padding-top:10px">
	<?php
						$ava = empty($par['pemerintah_logo']) ? base_url().'uploads/logo/'.$par['pemerintah_logo'] : base_url().'assets/logo/brand.png'; ?>
							
							<img src="<?php echo $ava ?>" style="margin-top:-15px;margin-right: 10px;float:left">
							
							<h2 style="margin:0px;font-size:26.4px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $nama_unit ?></h2>
							<h2 style="margin:0px;font-size:20px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $par['pemerintah'] ?></h2>
						
						
<div id="colorlib-logo"></div>

</div>
<div class="col-md-8 text-right menu-1" style="padding-top:30px">
<ul>
<li class="active"><a href="index.html">Home</a></li>
<li class="has-dropdown">
<a href="courses.html">Profil</a>
<ul class="dropdown">
<li><a href="courses-single.html">Tentang Kami</a></li>
<li><a href="#">Visi dan Misi</a></li>
</ul>
</li>
<li><a href="about.html">Alamat</a></li>
<li class="btn-cta"><a href="#"><span>Login</span></a></li>
</ul>
</div>
</div>
</div>
</div>
</nav>


<aside id="colorlib-hero">
	<div class="<?php echo @$set_widget_2->nama_col;?> no-padding" style="height:<?php echo @$set_widget_2->tinggi;?>px; margin-bottom:20px;">
							<div class="box-header with-border">
								<div class="pull-left">
									<?php echo @$btn_tambah_2;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_2;?>
								</div>
							</div>
								<?php
								if(@$dt_widget_2->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_2->num_rows();
									foreach ($dt_widget_2->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();


				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url(https://demoapus.com/edumy/wp-content/uploads/2019/06/1110-614x400.jpg);">
<span class="price text-center"><small>'.$ber->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="#">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="#" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('id_unit'=>$id_unit,'status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="container">
<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Team Pengajar</h2>
<p></p>
</div>
</div>
<div class="row">
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person1.jpg)"></div>
<div class="desc">
<h3>Olivia Young</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person2.jpg)"></div>
<div class="desc">
<h3>Daniel Anderson</h3>
<span>Professor</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person3.jpg)"></div>
<div class="desc">
<h3>David Brook</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person3.jpg)"></div>
<div class="desc">
<h3>Brigeth Smith</h3>
<span>Teacher</span>
</div>
</div>
</div>
</div>
</div>
				';
				break;
			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
											 		<h5 class="widgetheading">'.$satu->judul.'</h5>
										 	</div>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
';
			break;

			case 11:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Pembelajaran</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
<th>No</th><th>Judul</th><th>Hari, Tanggal</th><th>Kelas</th><th>Mata Pelajaran</th></tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">1</td><td>Agama Islam</td><td>Selasa,<br>14 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">2</td><td>Matematika dasar 5 1</td><td>Rabu,<br>15 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">3</td><td>Bahasa Indonesia Kelas 5A</td><td>Kamis,<br>16 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<tr>
<td style="text-align:center">4</td><td>Agama Islam</td><td>Sabtu,<br>18 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">5</td><td>Matematika dasar 5 2</td><td>Rabu,<br>22 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">6</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">7</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">8</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">9</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">10</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
</tbody>
</table>';
				
				
				break;
			case 12:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Ujian</h2>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">20</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">21</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">22</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
';
				
				
				break;
			

			case 13:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '<div class="col-md-12 no-padding" style="margin-bottom:10px;">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Link OPD</h2>
</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
</div>';
				
				
				break;
			
			case 14:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="container">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/assets/front/utama/images/img_bg_2.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">100</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50">1500</span>
<span class="colorlib-counter-label">Peserta</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">500</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50">10</span>
<span class="colorlib-counter-label">OPD</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>
							</aside>



<div id="colorlib-intro">

<footer id="colorlib-footer">
<div class="container">
<div class="row row-pb-md">

	<div class="<?php echo @$set_widget_6->nama_col;?> no-padding" style="height:<?php echo @$set_widget_6->tinggi;?>px; margin-bottom:20px;">
							<div class="box-header with-border">
								<div class="pull-left">
									<?php echo @$btn_tambah_6;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_6;?>
								</div>
							</div>
								<?php
								if(@$dt_widget_6->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_6->num_rows();
									foreach ($dt_widget_6->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				
				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url(https://demoapus.com/edumy/wp-content/uploads/2019/06/1110-614x400.jpg);">
<span class="price text-center"><small>'.$cek_unit->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="#">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="#" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="col-md-3 colorlib-widget" style="margin-bottom:20px;">
										 		<h4>'.$satu->judul.'</h4>
										 	<ul class="colorlib-footer-links">';
				foreach ($dt_berita->result() as $ber) {
					echo '<li><a><i class="icon-check"></i> '.$ber->title.'</a></li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 4:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="containerx">
<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Team Pengajar</h2>
<p></p>
</div>
</div>
<div class="row">
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person1.jpg)"></div>
<div class="desc">
<h3>Olivia Young</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person2.jpg)"></div>
<div class="desc">
<h3>Daniel Anderson</h3>
<span>Professor</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person3.jpg)"></div>
<div class="desc">
<h3>David Brook</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(http://localhost/elearning/assets/front/utama/images/person3.jpg)"></div>
<div class="desc">
<h3>Brigeth Smith</h3>
<span>Teacher</span>
</div>
</div>
</div>
</div>
</div>
				';
				break;
			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

				<div class="col-md-3 colorlib-widget" style="margin-bottom:20px;">
										 		<h4>'.$satu->judul.'</h4>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
';
			break;


			case 11:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Pembelajaran</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
<th>No</th><th>Judul</th><th>Hari, Tanggal</th><th>Kelas</th><th>Mata Pelajaran</th></tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">1</td><td>Agama Islam</td><td>Selasa,<br>14 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">2</td><td>Matematika dasar 5 1</td><td>Rabu,<br>15 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">3</td><td>Bahasa Indonesia Kelas 5A</td><td>Kamis,<br>16 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<tr>
<td style="text-align:center">4</td><td>Agama Islam</td><td>Sabtu,<br>18 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">5</td><td>Matematika dasar 5 2</td><td>Rabu,<br>22 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">6</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">7</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">8</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">9</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">10</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
</tbody>
</table>';
				
				
				break;
			case 12:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Ujian</h2>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">20</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">21</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">22</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
';
				
				
				break;
			

			case 13:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '<div class="col-md-12 no-padding" style="margin-bottom:10px;">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Link OPD</h2>
</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN1 Pamulang</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
	<div class="col-md-4">
	<div class="intro-flex">
	<div class="one-third color-1 animate-boxx">
	<span class="icon"><i class="flaticon-market"></i></span>
	<div class="desc">
	<h3>SDN2 Inpress Monaco	</h3>
	<p><a href="http://localhost/elearning/elearn/view/portal/SDN1_Pamulang" target="_blank" class="view-more">Lihat Portal</a></p>
	</div>
	</div>


	</div>
	</div>
</div>';
				
				
				break;
			
			case 14:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="container">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/assets/front/utama/images/img_bg_2.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">100</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50">1500</span>
<span class="colorlib-counter-label">Peserta</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">500</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50">10</span>
<span class="colorlib-counter-label">OPD</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>


</div>
</div>
<div class="copy">
<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<p>


<small class="block">&copy; 
Copyright &copy; MMK</a>

</small><br>
</p>
</div>
</div>
</div>
</div>
</footer>
</div>
<div class="gototop js-top">
<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.easing.1.3.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/bootstrap.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.waypoints.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.stellar.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.flexslider-min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/owl.carousel.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.magnific-popup.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/js/magnific-popup-options.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.countTo.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/main.js';?>" type="text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="ext/javascript"></script>
<script type="ext/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="49" defer=""></script></body>
</html>