<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>E-Learning</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta name="twitter:title" content="">
<meta name="twitter:image" content="">
<meta name="twitter:url" content="">
<meta name="twitter:card" content="">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">

    <link href="http://localhost/elearning/assets/css/admin-lte.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/animate.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/icomoon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/bootstrap.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/magnific-popup.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/flexslider.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.carousel.min.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.theme.default.min.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/fonts/flaticon/flaticon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/style.css';?>">

<script src="<?php echo base_url().'assets/front/utama/js/modernizr-2.6.2.min.js';?>" type="text/javascript"></script>

<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.no-padding{
			padding: 0px !important;
		}
		h3, .h3 {
    font-size: 22px;
}
	</style>
</head>
<body>
<div class="colorlib-loader" style="display: none;"></div>
<div id="page"><a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle colorlib-nav-white"><i></i></a><div id="colorlib-offcanvas"><ul>
<li class="active"><a href="index.html">Home</a></li>
<li class="offcanvas-has-dropdown">
<a href="courses.html">Courses</a>
<ul class="dropdown">
<li><a href="courses-single.html">Courses Single</a></li>
<li><a href="#">Mobile Apps</a></li>
<li><a href="#">Website</a></li>
<li><a href="#">Web Design</a></li>
<li><a href="#">WordPress</a></li>
</ul>
</li>
<li><a href="about.html">About</a></li>
<li><a href="event.html">Events</a></li>
<li><a href="news.html">News</a></li>
<li><a href="contact.html">Contact</a></li>
<li class="btn-cta"><a href="#"><span>Free Trial</span></a></li>
</ul></div>
<nav class="colorlib-nav" role="navigation">
<div class="top-menu"  style="background: <?php echo @$set_widget_1->background_title;?>">
<div class="container">
<div class="row">
<div class="col-md-4" style="padding-top:10px">
	<?php
						$ava = empty($par['pemerintah_logo']) ? base_url().'uploads/logo/'.$par['pemerintah_logo'] : base_url().'assets/logo/brand.png'; ?>
							
							<img src="<?php echo $ava ?>" style="margin-top:-15px;margin-right: 10px;float:left">
							
							<h2 style="margin:0px;font-size:26.4px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $par['instansi'] ?></h2>
							<h2 style="margin:0px;font-size:20px !important;color: <?php echo @$set_widget_1->color_title;?>;"><?php echo $par['pemerintah'] ?></h2>
						
						
<div id="colorlib-logo"></div>

</div>
<div class="col-md-8 text-right menu-1" style="padding-top:30px">
<?php echo get_menu(@$id_unit)?>
</div>
</div>
</div>
</div>
<div class="upper-menu">
<div class="container">
<div class="row">
<div class="col-xs-9">
<marquee class="marquee">Selamat Datang di E-Learning</marquee>
</div>
<div class="col-xs-3 text-right">
<p>
</p><ul class="colorlib-social-icons">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-linkedin"></i></a></li>
<li><a href="#"><i class="icon-dribbble"></i></a></li>
</ul>
<p></p>
<p class="btn-apply"><a href="#">Apply Now</a></p>
</div>
</div>
</div>
</div>
</nav>

<!-- colom 2 -->
<aside id="colorlib-hero">
	<div class="<?php echo @$set_widget_2->nama_col;?> no-padding" style="height:<?php echo @$set_widget_2->tinggi;?>px; margin-bottom:20px;">
								<div class="pull-left">
									<?php echo @$btn_tambah_2;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_2;?>
								</div>
								<?php
								if(@$dt_widget_2->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_2->num_rows();
									foreach ($dt_widget_2->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();


				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'limit'=>6,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url('.base_url().'uploads/file/berita/'.$ber->berkas_file.');">
<span class="price text-center"><small>'.$ber->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container">
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
					</div>
				';
				break;
			case 3:
				
				
				$from_pengumuman = array(
					'front_pengumuman a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_pengumuman = $this->general_model->datagrab(array(
				'tabel'=> $from_pengumuman,'limit'=>6,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_pengumuman->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">

<div class="desc">
<h3><a href="'.site_url('Front/detail_pengumuman/'.in_de(array('id_pengumuman'=>$ber->id_pengumuman,'id_portal'=>$id_portal))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_pengumuman/'.in_de(array('id_pengumuman'=>$ber->id_pengumuman,'id_portal'=>$id_portal))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';

				break;
			case 4:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container">
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
					</div>
				';
				break;
			case 6:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_runingtext','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '<marquee>';
				foreach ($dt_berita->result() as $ber) {
					echo '<span> '.$ber->title.' || </span>';
				}
				echo '</marquee>';
				break;
			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('id_unit'=>$id_unit,'status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_tautan','order'=>'title ASC','where'=>array('status'=>1,'id_kategori_konten'=>$satu->param1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
				<div class="col-md-12 no-padding" style="margin-bottom:10px;">
						<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
					<h2>'.$satu->judul.'</h2>
					</div>';
					if($satu->param1==1){
						$xx = 'color-3';
					}else{

						$xx = 'color-2';
					}
				foreach ($dt_berita->result() as $ber) {
					echo '			
					<div class="col-md-6">
					<div class="intro-flex">
					<div class="one-third '.$xx.' animate-boxx">
					<span class="icon"><i class="flaticon-market"></i></span>
					<div class="desc">
					<h5 style="color:#fff !important;">'.$ber->title.'</h5>
					<p><a href="'.$ber->link.'" target="_blank" class="view-more">Lihat Portal</a></p>
					</div>
					</div>
					</div>
					</div>
					';
				}

				echo '

</div>
</div>';
				
				
				break;
			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="container">
					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
											 		<h5 class="widgetheading">'.$satu->judul.'</h5>
										 	</div>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
       </div>
';
			break;

			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				$jumlah_unit = $this->general_model->datagrab(array(
							'tabel'=>'ref_unit', 
                			'where'=>array('aktif'=>1,'level_unit !='=>'NULL'),
               				'select'=>"count(*)jm"));
				$jumlah_pengajar = $this->general_model->datagrab(array(
							'tabel'=>'pegawai_role', 
                			'where'=>array('id_role'=>4),
               				'select'=>"count(*)jm"));
				$jumlah_peserta = $this->general_model->datagrab(array(
			                'tabel'=>'pegawai_role pr', 
			                'where'=>array('id_role'=>5),
               				'select'=>"count(*)jm"));
				$jumlah_kelas = $this->general_model->datagrab(array(
			                'tabel'=>'elearn_ref_kelas', 
			                #'where'=>array('id_role'=>5),
			                'select'=>"count(*)jm"));


				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/uploads/file/berita/img_bg_1.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">

<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_unit->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_unit->row('jm').'</span>
<span class="colorlib-counter-label">Sekolah</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_kelas->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_kelas->row('jm').'</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_pengajar->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_pengajar->row('jm').'</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="'.$jumlah_peserta->row('jm').'" data-speed="1000" data-refresh-interval="50">'.$jumlah_peserta->row('jm').'</span>
<span class="colorlib-counter-label">Siswa</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
				case 16:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">

				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Berlangsung</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

              ';
				break;
				case 17:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">
				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Yang Akan Datang</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

';
				break;
			
			case 22:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="container">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>
							</aside>




	<div class="container">
	<div class="row">
	<div class="<?php echo @$set_widget_3->nama_col;?>" style="height:<?php echo @$set_widget_3->tinggi;?>px; margin-bottom:20px;">
								<div class="pull-left">
									<?php echo @$btn_tambah_3;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_3;?>
								</div>
								<?php
								if(@$dt_widget_3->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_3->num_rows();
									foreach ($dt_widget_3->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				
				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'limit'=>6,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url('.base_url().'uploads/file/berita/'.$ber->berkas_file.');">
<span class="price text-center"><small>'.$cek_unit->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
											 		<h5 class="widgetheading">'.$satu->judul.'</h5>
										 	</div>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
				';
				break;
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
';
			break;

			case 11:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Pembelajaran</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
<th>No</th><th>Judul</th><th>Hari, Tanggal</th><th>Kelas</th><th>Mata Pelajaran</th></tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">1</td><td>Agama Islam</td><td>Selasa,<br>14 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">2</td><td>Matematika dasar 5 1</td><td>Rabu,<br>15 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">3</td><td>Bahasa Indonesia Kelas 5A</td><td>Kamis,<br>16 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<tr>
<td style="text-align:center">4</td><td>Agama Islam</td><td>Sabtu,<br>18 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">5</td><td>Matematika dasar 5 2</td><td>Rabu,<br>22 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">6</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">7</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">8</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">9</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">10</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
</tbody>
</table>';
				
				
				break;
			case 12:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Ujian</h2>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">20</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">21</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">22</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
';
				
				
				break;
			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_tautan','order'=>'title ASC','where'=>array('status'=>1,'id_kategori_konten'=>$satu->param1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="col-md-12 no-padding" style="margin-bottom:10px;">
						<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
					<h2>'.$satu->judul.'</h2>
					</div>';
					if($satu->param1==1){
						$xx = 'color-3';
					}else{

						$xx = 'color-2';
					}
				foreach ($dt_berita->result() as $ber) {
					echo '			
					<div class="col-md-6">
					<div class="intro-flex">
					<div class="one-third '.$xx.' animate-boxx">
					<span class="icon"><i class="flaticon-market"></i></span>
					<div class="desc">
					<h5 style="color:#fff !important;">'.$ber->title.'</h5>
					<p><a href="'.$ber->link.'" target="_blank" class="view-more">Lihat Portal</a></p>
					</div>
					</div>
					</div>
					</div>
					';
				}

				echo '

</div>';
				
				
				break;
			
			case 22:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="row">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="containerx">
<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/assets/front/utama/images/img_bg_2.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">100</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50">1500</span>
<span class="colorlib-counter-label">Siswa</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">500</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50">10</span>
<span class="colorlib-counter-label">OPD</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
				case 16:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="col-lg-12">

				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Berlangsung</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

              ';
				break;
				case 17:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">
				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Yang Akan Datang</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

';
				break;

case 18:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure"'.$satu->id_konten.$satu->posisi.$satu->posisi.'">
    <div id="container'.$satu->id_konten.$satu->posisi.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	Highcharts.chart('container".$satu->id_konten.$satu->posisi.$satu->posisi."', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: '".$satu->judul."'
    },
    accessibility: {
        point: {
            valueSuffix: ' Pengajar'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f} Pengajar</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        data: [
            
            ['SDN1 Canden', 30],
            ['SDN1 Pamulang', 55],
            {
                name: 'SDN1 Inpres Monaco',
                y: 40,
                sliced: true,
                selected: true
            }
        ]
    }]
});

</script>

<style type='text/css'>
	#container".$satu->id_konten.$satu->posisi.$satu->posisi." {
  height: 400px; 
    border: 1px solid #CCE;
}

.highcharts-figure".$satu->id_konten.$satu->posisi.$satu->posisi.", .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

</style>

				";
				break;

				case 19:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure'.$satu->id_konten.$satu->posisi.'">
    <div id="container'.$satu->id_konten.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container".$satu->id_konten.$satu->posisi."',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    
    title: {
        text: '".$satu->judul."'
    },
    subtitle: {
       /* text: 'Source: WorldClimate.com'*/
    },
    xAxis: {
        categories: [
            'SDN1 Inpres Monaco',
            'SDN1 Pamulang',
            'SDN1 Canden'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Siswa'
        }
    },
    tooltip: {
        headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
        pointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>' +
            '<td style=\"padding:0\"><b>{point.y:.1f} Siswa</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '".$satu->judul."',
        data: [50, 71, 106]

    }]
});

</script>

<style type='text/css'>
.highcharts-figure".$satu->id_konten.$satu->posisi.", .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    
}

#container".$satu->id_konten.$satu->posisi." {
    height: 400px;
    border: 1px solid #CCE;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

				";
				break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>



	<div class="<?php echo @$set_widget_4->nama_col;?> no-padding" style="height:<?php echo @$set_widget_4->tinggi;?>px; margin-bottom:20px;">
								<div class="pull-left">
									<?php echo @$btn_tambah_4;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_4;?>
								</div>
								<?php
								if(@$dt_widget_4->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_4->num_rows();
									foreach ($dt_widget_4->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				
				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'limit'=>6,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url('.base_url().'uploads/file/berita/'.$ber->berkas_file.');">
<span class="price text-center"><small>'.$cek_unit->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 4:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
											 		<h5 class="widgetheading">'.$satu->judul.'</h5>
										 	</div>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
';
			break;

			case 11:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Pembelajaran</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
<th>No</th><th>Judul</th><th>Hari, Tanggal</th><th>Kelas</th><th>Mata Pelajaran</th></tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">1</td><td>Agama Islam</td><td>Selasa,<br>14 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">2</td><td>Matematika dasar 5 1</td><td>Rabu,<br>15 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">3</td><td>Bahasa Indonesia Kelas 5A</td><td>Kamis,<br>16 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<tr>
<td style="text-align:center">4</td><td>Agama Islam</td><td>Sabtu,<br>18 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">5</td><td>Matematika dasar 5 2</td><td>Rabu,<br>22 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">6</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">7</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">8</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">9</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">10</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
</tbody>
</table>';
				
				
				break;
			case 12:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Ujian</h2>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">20</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">21</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">22</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
';
				
				
				break;
			
			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_tautan','order'=>'title ASC','where'=>array('status'=>1,'id_kategori_konten'=>$satu->param1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="col-md-12 no-padding" style="margin-bottom:10px;">
						<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
					<h2>'.$satu->judul.'</h2>
					</div>';
					if($satu->param1==1){
						$xx = 'color-3';
					}else{

						$xx = 'color-2';
					}
				foreach ($dt_berita->result() as $ber) {
					echo '			
					<div class="col-md-6">
					<div class="intro-flex">
					<div class="one-third '.$xx.' animate-boxx">
					<span class="icon"><i class="flaticon-market"></i></span>
					<div class="desc">
					<h5 style="color:#fff !important;">'.$ber->title.'</h5>
					<p><a href="'.$ber->link.'" target="_blank" class="view-more">Lihat Portal</a></p>
					</div>
					</div>
					</div>
					</div>
					';
				}

				echo '

</div>';
				
				
				break;
			
			case 22:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="row">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="container">
<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/assets/front/utama/images/img_bg_2.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">100</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50">1500</span>
<span class="colorlib-counter-label">Siswa</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">500</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50">10</span>
<span class="colorlib-counter-label">OPD</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;

			
				case 16:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
				<div class="col-lg-12">
				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Berlangsung</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

              ';
				break;
				case 17:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">
				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Yang Akan Datang</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

';
				break;

case 18:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure"'.$satu->id_konten.$satu->posisi.$satu->posisi.'">
    <div id="container'.$satu->id_konten.$satu->posisi.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	Highcharts.chart('container".$satu->id_konten.$satu->posisi.$satu->posisi."', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: '".$satu->judul."'
    },
    accessibility: {
        point: {
            valueSuffix: ' Pengajar'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f} Pengajar</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        data: [
            ['SMPN1 Monaco', 30],
            ['SMPN1 Pamulang', 55],
            {
                name: 'SMPN1 Canden',
                y: 40,
                sliced: true,
                selected: true
            }
        ]
    }]
});

</script>

<style type='text/css'>
	#container".$satu->id_konten.$satu->posisi.$satu->posisi." {
  height: 400px; 
    border: 1px solid #CCE;
}

.highcharts-figure".$satu->id_konten.$satu->posisi.$satu->posisi.", .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

</style>

				";
				break;

				case 19:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);

				echo '<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure'.$satu->id_konten.$satu->posisi.'">
    <div id="container'.$satu->id_konten.$satu->posisi.'"></div>
    <p class="highcharts-description">
    </p>
</figure>
';
				echo "

<script type='text/javascript'>
	var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container".$satu->id_konten.$satu->posisi."',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    
    title: {
        text: '".$satu->judul."'
    },
    subtitle: {
       /* text: 'Source: WorldClimate.com'*/
    },
    xAxis: {
        categories: [
            'SMPN1 Monaco',
            'SMPN1 Pamulang',
            'SMPN1 Canden'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Siswa'
        }
    },
    tooltip: {
        headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
        pointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>' +
            '<td style=\"padding:0\"><b>{point.y:.1f} Siswa</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '".$satu->judul."',
        data: [50, 71, 106]

    }]
});

</script>

<style type='text/css'>
.highcharts-figure".$satu->id_konten.$satu->posisi.", .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    
}

#container".$satu->id_konten.$satu->posisi." {
    height: 400px;
    border: 1px solid #CCE;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

				";
				break;

		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>
						</div>
						</div>
							</aside>
<div id="colorlib-intro">

<footer id="colorlib-footer">
<div class="container">
<div class="row row-pb-md">

	<div class="<?php echo @$set_widget_6->nama_col;?> no-padding" style="height:<?php echo @$set_widget_6->tinggi;?>px; margin-bottom:20px;">
								<div class="pull-left">
									<?php echo @$btn_tambah_6;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_6;?>
								</div>
								<?php
								if(@$dt_widget_6->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_6->num_rows();
									foreach ($dt_widget_6->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				
				$from_berita = array(
					'front_berita a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);


				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> $from_berita,'limit'=>6,'where'=>array('a.status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container">
					<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>'.$satu->judul.'</h2>
</div>





					';
				foreach ($dt_berita->result() as $ber) {
					echo '
					<div class="col-md-4">
<div class="classes">
<div class="classes-img" style="background-image: url('.base_url().'uploads/file/berita/'.$ber->berkas_file.');">
<span class="price text-center"><small>'.$cek_unit->unit.'</small></span>
</div>
<div class="desc">
<h3><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'">'.$ber->title.'</a></h3>
<p>'.substr($ber->content,0,100).' ...</p>
<p><a href="'.site_url('Front/detail_berita/'.in_de(array('id_berita'=>$ber->id_berita,'id_portal'=>$id_portal))).'" class="btn-learn">Selengkapnya <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>




						';
				}
				echo '
					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="col-md-3 colorlib-widget" style="margin-bottom:20px;">
										 		<h4>'.$satu->judul.'</h4>
										 	<ul class="colorlib-footer-links">';
				foreach ($dt_berita->result() as $ber) {
					echo '<li><a><i class="icon-check"></i> '.$ber->title.'</a></li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-3" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 4:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman','where'=>array('status'=>1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'limit'=>'6','where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow','where'=>array('status'=>1)));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="flexslider">
             
<ul class="slides">

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li style="background-image: url('.base_url().'uploads/file/'.$ber->berkas_file.'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>'.$ber->title.'</h1>
</div>
</div>
</div>
</div>
</li>



						';
				}
				echo '

            </ul>
       </div>
';
			break;

			case 9:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

				<div class="col-md-3 colorlib-widget" style="margin-bottom:20px;">
										 		<h4>'.$satu->judul.'</h4>';
			
					echo '<div>'.$satu->param2.'</div>';
				
				echo '
					</div>
				';
				break;
			
			case 10:
			
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'elearn_qna'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="about-desc animate-boxx">
<h2 style="font-family: arial;">'.$satu->judul.'</h2>
	<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#'.$ber->id_qna.'" aria-expanded="false" aria-controls="'.$ber->id_qna.'" class="collapsed">'.$ber->q.'</a>
</h4>
</div>
<div id="'.$ber->id_qna.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<div class="row">
<div class="col-md-12">
<p>'.$ber->a.'</p>
</div>
</div>
</div>
</div>
</div>

						';
				}
				echo '

       </div>
       </div>
       </div>
';
			break;

			case 11:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Pembelajaran</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
<th>No</th><th>Judul</th><th>Hari, Tanggal</th><th>Kelas</th><th>Mata Pelajaran</th></tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">1</td><td>Agama Islam</td><td>Selasa,<br>14 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">2</td><td>Matematika dasar 5 1</td><td>Rabu,<br>15 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">3</td><td>Bahasa Indonesia Kelas 5A</td><td>Kamis,<br>16 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<tr>
<td style="text-align:center">4</td><td>Agama Islam</td><td>Sabtu,<br>18 April 2020</td><td>Kelas 5 A</td><td>Agama</td></tr>
<tr>
<td style="text-align:center">5</td><td>Matematika dasar 5 2</td><td>Rabu,<br>22 April 2020</td><td>Kelas 5 A</td><td>Matematika</td></tr>
<tr>
<td style="text-align:center">6</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">7</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">8</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">9</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
<td style="text-align:center">10</td><td>Bahasa Indonesia Kelas 5A(2)</td><td>Kamis,<br>23 April 2020</td><td>Kelas 5 A</td><td>Bahasa Indonesia</td></tr>
</tbody>
</table>';
				
				
				break;
			case 12:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

<div class="row">
<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Jadwal Ujian</h2>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">20</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">21</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Bahasa Indonesia</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Bahasa Indonesia</p>
</div>
</div>
</div>
				<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">22</span><span class="month">Apr</span></p>
<p class="organizer"><span>Unit: </span> <span>SDN 1 Pamulang</span></p>
<h2><a href="event.html">Agama Islam</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>Kelas 5 A, Mata Pelajaran Agama Islam</p>
</div>
</div>
</div>
';
				
				
				break;
			
			case 8:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_tautan','order'=>'title ASC','where'=>array('status'=>1,'id_kategori_konten'=>$satu->param1)));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="container"><div class="col-md-12 no-padding" style="margin-bottom:10px;">
						<div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-boxx">
					<h2>'.$satu->judul.'</h2>
					</div>';
					if($satu->param1==1){
						$xx = 'color-3';
					}else{

						$xx = 'color-2';
					}
				foreach ($dt_berita->result() as $ber) {
					echo '			
					<div class="col-md-4">
					<div class="intro-flex">
					<div class="one-third '.$xx.' animate-boxx">
					<span class="icon"><i class="flaticon-market"></i></span>
					<div class="desc">
					<h3>'.$ber->title.'</h3>
					<p><a href="'.$ber->link.'" target="_blank" class="view-more">Lihat Portal</a></p>
					</div>
					</div>
					</div>
					</div>
					';
				}

				echo '

</div></div>';
				
				
				break;
			
			case 22:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="row">
	<div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Mata Pelajaran</h2>
</div>
	<div id="colorlib-services">

<div class="containerc">
<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<div class="desc">
<h3>Matematika</h3>
</div>
<span class="icon">
<i class="flaticon-books"></i> 
</span>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Bahasa Indonesia</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Agama</h3>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Bahasa Inggris
</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;
			
			case 15:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(&quot;http://localhost/elearning/assets/front/utama/images/img_bg_2.jpg&quot;); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">100</span>
<span class="colorlib-counter-label">Kelas</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50">1500</span>
<span class="colorlib-counter-label">Siswa</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">500</span>
<span class="colorlib-counter-label">Pengajar</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50">10</span>
<span class="colorlib-counter-label">OPD</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>';
				
				
				break;

				case 16:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">

				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Berlangsung</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

              ';
				break;
				case 17:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_konten'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '
<div class="col-lg-12">
				<div class="box">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <div class="col-md-12 col-md-offset-2 text-center colorlib-heading">
<h2>Kelas Yang Akan Datang</h2>
</div>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                    <li>
                      <!-- drag handle -->
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul><ul class="pagination pagination-sm inline">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div>
</div>

';
				break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>


</div>
</div>
<div class="copy">
<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<p>


<small class="block">&copy; 
Copyright &copy; MMK</a>

</small><br>
</p>
</div>
</div>
</div>
</div>
</footer>
</div>
<div class="gototop js-top">
<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.easing.1.3.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/bootstrap.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.waypoints.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.stellar.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.flexslider-min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/owl.carousel.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.magnific-popup.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/js/magnific-popup-options.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.countTo.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/main.js';?>" type="text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="ext/javascript"></script>
<script type="ext/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="49" defer=""></script></body>
</html>