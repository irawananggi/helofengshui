
 
    <link href="//cdn.extramarks.id/web/images/website/favicon2.png" rel="shortcut icon" type="image/gif">
<link href="//cdn.extramarks.id/web/css/website_v6/bootstrap.min.css?v=05032020" media="screen" rel="stylesheet" type="text/css">
<link href="//www.kelaspintar.id/css/website_v6/font.css?v=05032020" media="screen" rel="stylesheet" type="text/css">
<link href="//cdn.extramarks.id/web/css/website_v6/style.css?v=05032020" media="screen" rel="stylesheet" type="text/css">
<script>
    var BASE_URL = emBaseURL = "";
    var SOUTHAFRICA_V1 = "0";
    var notesUrl = BASE_URL;
    var commentsUrl = BASE_URL;
    var CdnPath = "//www.kelaspintar.id";
    var webMode = 'website';
    var reqUrl = BASE_URL;
    var misStartTime = 0;
    var misBoardId = 0;
    var viera = 0;
</script>
<script type="text/javascript" src="//cdn.extramarks.id/web/js/jquery-2.0.3.min.js"></script>
<section class="additional-features-sec mb50">
	<div class="container start-learning-now">
		<div class="row clearfix mt30">
			<div class="col-sm-offset-1 col-sm-10">
				<div class="mt50">
					<h2 class="heading em-student-bottom-heading">Yuk! Mulai Belajar Sekarang</h2>
					<script type="text/javascript" src="//cdn.extramarks.id/web/js/website_v6/jquery.carousel.js"></script>
					<script type="text/javascript">
						$( function () {

							if ( $( window ).width() >= 769 ) {
								$( '.start-learning-now-slider' ).jCarousel( {
									type: 'slidey-up',
									carsize: {
										carheight: 240
									},
									//	 auto: 5000,
									//speed: 500,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}
							if ( $( window ).width() <= 768 ) {
								$( '.start-learning-now-slider' ).jCarousel( {
									type: 'slidey-up',
									carsize: {
										carheight: 450
									},
									//	 auto: 5000,
									//speed: 500,
									//visible: 1,
									//pauseOnMouseOver: true, // This is the configuration parameter
									//circular: true
								} );
							}

						} );
					</script>
					<img src="//cdn.extramarks.id/web/images/website_v6/additional-feature-screen.png" alt="idea-icon" class="img-responsive additional-feature-screen">
					<div class="start-learning-now-slider" style="height: 240px; overflow: hidden; position: relative;">
						<div class="item select" style="position: absolute; z-index: 1; height: 240px; cursor: pointer; top: 0px; left: 0px;">
							<div class="col-sm-5 col-xs-12 mt50 additional-feature-left-content">
								<h3>Langkah 1</h3>
								<p class="text-p">Pilih Kelas yang kamu inginkan, lalu pilih masa berlangganan yang kamu butuhkan</p>
							</div>
							<div class="col-sm-offset-1 col-xs-12 col-sm-6 carheight-img">
								<img src="//cdn.extramarks.id/web/images/website_v6/additional-feature-img1.png" alt="idea-icon" class="img-responsive">
							</div>
						</div>
						<div class="item" style="position: absolute; z-index: 1; height: 240px; cursor: pointer; top: -240px; left: 0px;">
							<div class="col-sm-5 col-xs-12 mt50 additional-feature-left-content">
								<h3>Langkah 2</h3>
								<p class="text-p">Pastikan kamu sudah memilih kelas dan periode berlangganan yang kamu inginkan, berikan tanda centang pada syarat dan ketentuan pilih menu "Lanjut ke Pembayaran"</p>
							</div>
							<div class="col-sm-offset-1 col-xs-12 col-sm-6 carheight-img">
								<img src="//cdn.extramarks.id/web/images/website_v6/additional-feature-img2.png" alt="idea-icon" class="img-responsive">
							</div>
						</div>
						<div class="item" style="position: absolute; z-index: 1; height: 240px; cursor: pointer; top: -240px; left: 0px;">
							<div class="col-sm-5 col-xs-12 mt50 additional-feature-left-content">
								<h3>Langkah 3</h3>
								<p class="text-p">Pastikan kelas, paket, kode promo (jika kamu memiliki) dan masukan nama Sekolah mu. Klik lanjutkan untuk memilih metode pembayaran yang akan kamu lakukan</p>

							</div>
							<div class="col-sm-offset-1 col-xs-12 col-sm-6 carheight-img">
								<img src="//cdn.extramarks.id/web/images/website_v6/additional-feature-img3.png" alt="idea-icon" class="img-responsive">
							</div>
						</div>

</div>
				</div>

			</div>
		</div>
	</div>
</section>