<?php
if(@$arr_hidden && count($arr_hidden) > 0) foreach ($arr_hidden as $hd) {
	?>
	<input type="hidden" value="<?php echo $hd['value'];?>" name="<?php echo $hd['name']; ?>" />
	<?php
}
?>
<?php
if(@$arr_css && count($arr_css) > 0) foreach ($arr_css as $css) {
	?>
	<link href="<?php echo $css['src'];?>" rel="stylesheet" />
	<?php
}
?>


<div class="row">
	<div class="col-lg-12">
		<div class="box box-inverse" data-sortable-id="table-basic-5">
			<div class="box-header">
				<h4>Kalender Pendidikan</h4>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<!-- <div class="card" style="margin:10px;"> -->
							<!-- <div class="card-header">Parameters</div> -->
							<!-- <div class="card-body"> -->
								<form class="form-horizontal row form-xs">
									<div class="col col-md-12">

										<div class="form-group col-md-3 row" style="display: none">
											<label for="current-year" class="col-sm-12 col-xs-12">Tahun Aktif</label>
											<div class="col col-sm-10">
												<input readonly type="text" class="form-control input-sm" id="current-year" placeholder="Current year" value="2020">
											</div>
										</div>

										<div class="form-group col-md-3 row">
											<label for="id_ta" class="col-sm-12 col-xs-12">Tahun Ajaran</label>
											<div class="col col-sm-10">
												<?php echo form_dropdown('id_ta', array(@$def_combo['ta']['id']=>@$def_combo['ta']['val']), @$def_combo['ta']['val'], 'id="id_ta" class="select-ta form-control combo-box" data-url="'.site_url('ajax/request/tahun_ajaran').'"'); ?>
											</div>

										</div>

										<div class="form-group col-md-3 row"  style="display: none">
											<label for="min-date" class="col-sm-2 control-label">Mulai</label>
											<div class="col col-sm-8">
												<input readonly type="date" class="form-control input-sm" id="min-date" placeholder="Min date">
											</div>
										</div>
										<div class="form-group col-md-3 row"  style="display: none">
											<label for="max-date" class="col-sm-2 control-label">Selesai</label>
											<div class="col col-sm-8">
												<input readonly type="date" class="form-control input-sm" id="max-date" placeholder="Max date">
											</div>
										</div>

										<div class="form-group col-md-3 row">
											<label for="id_level_unit" class="col-sm-12 col-xs-12">Jenjang</label>
											<div class="col col-sm-8">
												<?php 
												echo form_dropdown('id_level_unit', array(@$def_combo['level_unit']['id']=>@$def_combo['level_unit']['val']),@$def_combo['level_unit']['id'], 'id="id_level_unit" class="select-levelunit form-control combo-box" data-url="'.site_url('ajax/request/level_unit').'"'); ?>
											</div>
											
										</div>
										
										<div class="form-group col-md-3 row">
											<label for="style" class="col-sm-12 col-xs-12">Bahasa</label>
											<div class="col col-sm-10">
												<select class="form-control input-sm" id="language">
													<option value="en">English</option>
													<option value="id" selected>Indonesia</option>
												</select>
											</div>
											<div class="col col-sm-2">
												<button id="update-language" type="button" class="m-t-2 btn btn-sm btn-success">Update</button>
											</div>

										</div>

									</div>

								</form>
								<!-- </div> -->
								<!-- </div> -->
							</div>
							<div class="col-md-12">
								<div id="kalender"></div>
							</div>

							<!-- legend -->
							<div class="col-md-12">
								<div id="legend"></div>
							</div>
							<!-- ./legend -->
						</div>

					</div>            
				</div>    
			</div>
		</div>

		<!-- modal -->
		<div class="modal fade" id="event-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Kegiatan</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="hidden" name="event-index" id="event-index">
						<input type="hidden" name="id_kegiatan" id="id_kegiatan">
						<form class="form-horizontal">
							<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js" integrity="sha256-K3qK8ynOxhJVloLac0CTWwr7iFKVDZF4Gd2yEsiAZYA=" crossorigin="anonymous"></script> -->
							<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css" integrity="sha256-bX+rnnNrWmSrL9BjREvIc3tU9uClWcKmoEFJ2VKnUBc=" crossorigin="anonymous" /> -->
							<script src="https://cdn.jsdelivr.net/npm/textarea-helper@0.3.1/textarea-helper.js"></script>

							<div class="form-group col-md-3 row">
								<label for="event-name" class="col-sm-4 control-label">Kegiatan</label>
								<div class="col-sm-8">
									<textarea id="event-name" name="event-name" class="select-event form-control" data-url="<?php echo site_url('ajax/request/unit'); ?>"></textarea>
								</div>
							</div>
							<div class="form-group col-md-3 row">
								<label for="event-tipe" class="col-sm-4 control-label">Status Hari</label>
								<div class="col-sm-8">
									<?php echo form_dropdown('event-tipe', array('0'=>'Netral', '1'=>'Efektif', '2'=>'Libur'), null, 'id="event-tipe" class="form-control"'); ?>
								</div>
							</div>
							<div class="form-group col-md-3 row">
								<label for="event-keterangan" class="col-sm-4 control-label">Keterangan</label>
								<div class="col-sm-8">
									<textarea id="event-keterangan" name="event-keterangan" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group col-md-3 row">
								<label for="min-date" class="col-sm-4 control-label">Tanggal</label>
								<div class="col-sm-8">
									<div class="input-group input-daterange" data-provide="datepicker">
										<input id="min-date" name="event-start-date" type="text" class="form-control" data-date-format="dd-mm-yyyy">
										<div class="input-group-prepend input-group-append">
											<div class="input-group-text">Sampai</div>
										</div>
										<input name="event-end-date" type="text" class="form-control" data-date-format="dd-mm-yyyy">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" id="save-event">
							Save
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="reject-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-warning">Peringatan</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<p class="text-danger">Anda tidak bisa menambahkan kegiatan apapun dihari libur!</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						
					</div>
				</div>
			</div>
		</div>

		<div id="context-menu">
		</div>
		<!-- ./modal -->

		<?php
		if(!empty($arr_js)) foreach ($arr_js as $src) {
			?>
			<script <?php echo @$src['type'];?> src="<?php echo $src['src'];?>"></script>
			<?php
		}
		?>