<html lang="en" class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><head>
<meta charset="utf-8">
<title>E-Learning Best Education Free HTML5 Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="http://webthemez.com">
<!-- css -->
<link href="<?php echo base_url().'assets/front/css/frontend.min.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/template.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/font-awesome.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/bootstrap.min.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/jquery.fancybox.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/flexslider.css' ?>" rel="stylesheet"><!-- 
<link href="<?php echo base_url().'assets/front/css/owl.carousel.css' ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/front/css/jcarousel.css' ?>" rel="stylesheet"> -->
<link href="<?php echo base_url().'assets/front/css/style.css' ?>" rel="stylesheet">
 
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper" class="home-page">

<div class="topbar">
	<div class="container">
    <div class="row">
			<div class="col-lg-12" style="background: <?php echo @$set_warna_latar->header_color;?> !important; height: 80px;">			
				<div class="logo pull-left">
						<?php
						$ava = empty($par['pemerintah_logo']) ? base_url().'uploads/logo/'.$par['pemerintah_logo'] : base_url().'assets/logo/brand.png'; ?>
							
							<img src="<?php echo $ava ?>" style="margin-top:-15px;margin-right: 10px;"><div class="pull-right">
								
							<h2 style="margin:0px;color: <?php echo @$par['color_text_header'];?>;"><?php echo $nama_unit ?></h2>
							<h2 style="margin:0px;color: <?php echo @$par['color_text_header'];?>;"><?php echo $par['pemerintah'] ?></h2>
						</div>
						<div class="clear"></div>
					</div>
				
				<div class="logo_app pull-right" style="margin-right: 15px;">
				<div class="pull-left">
							<h2><?php echo str_replace(' ',' ',@$set_warna_latar->nama_ids) ?></h2>
						</div>
						<?php $ava = file_exists('./assets/logo/'.$folder.'.png') ? base_url().'assets/logo/'.$folder.'.png' : base_url().'assets/logo/logo.png'; ?>
						<img src="<?php echo $ava ?>"  style="width:100px;height: auto;">
						<div class="clear"></div>
					</div>
				<div class="clear"></div>
			</div>
</div>
</div>
  
</div>
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo base_url().'assets/front/img/logo.png' ?>" alt="logo">E-Learning</a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.html">Home</a></li> 
						 <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">About Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="about.html">Our Institute</a></li>
                            <li><a href="#">Our Team</a></li>
                            <li><a href="#">News</a></li> 
                            <li><a href="#">Investors</a></li>
                        </ul>
                    </li> 
						<li><a href="courses.html">Courses</a></li>
                        <li><a href="portfolio.html">Portfolio</a></li>
                        <li><a href="pricing.html">Fees</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="content">	
	<div class="container" style="border: 1px solid #ccc;padding: 15px;">
	    	<div class="row">
	    		

						<div class="elementor <?php echo @$set_widget_2->nama_col;?> no-padding" style="height:<?php echo @$set_widget_2->tinggi;?>px; margin-bottom:20px;">
							<div class="box-header with-border">
								<div class="pull-left">
									<?php echo @$btn_tambah_2;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_2;?>
								</div>
							</div>
							<div class="box-body">
								<?php
								if(@$dt_widget_2->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_2->num_rows();
									foreach ($dt_widget_2->result() as $satu) { 
										
										switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
						<div class="elementor-inner">
							<div class="elementor-section-wrap">
								<div class="elementor-widget-container">
									<div class="widget widget-courses ">
										<div class="row">
											<div class="elementor-element elementor-element-3eefe78 elementor-widget elementor-widget-edumy_heading" data-id="3eefe78" data-element_type="widget" data-widget_type="edumy_heading.default">
												<div class="elementor-widget-container">
											 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
											 	</div>

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						<div class="col-lg-4 col-md-4 col-sm-6">
							<div class="course-grid">
								<div class="course-entry">
									<div class="course-cover">
										<div class="course-cover-thumb">
											<a href="#">
											<img width="614" height="400" src="https://demoapus.com/edumy/wp-content/uploads/2019/06/1110-614x400.jpg" class="attachment-edumy-course-grid size-edumy-course-grid wp-post-image" alt="The Complete Shopify Aliexpress Dropship"> </a>
											<div class="listing-btn-wrapper listing-wishlist">
												<a href="#apus-wishlist-add" class="btn btn-ct-link apus-wishlist-not-login" data-id="655">
													<i class="flaticon-like"></i><span class="wishlist-text">Add to wishlist</span>
												</a>
											</div>
										<div class="course-cover-label">
											<a href="">Preview Course </a>
										</div>
									</div>
								</div>
							<div class="course-detail">
								<div class="course-info-box">

									<div class="course-teacher"><a href="https://demoapus.com/edumy/profile/admin/">'.$cek_unit->unit.'</a></div>

										<a href="https://demoapus.com/edumy/courses/the-complete-shopify-aliexpress-dropship/">
											<h3 class="course-title">'.$ber->title.'</h3>
										</a>
									</div>
									<div class="course-meta-data">
										<p>'.substr($ber->content,0,100).' ...</p>
									</div>
								</div>
							</div>
						</div>
					</div>





						';
				}
				echo '


					</div>
					</div>
					</div>
					</div>
					</div>
					</div>

					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
	<section id="banner">
        <div id="main-slider" class="flexslider">
            <ul class="slides">
             

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li class="flex-active-slide">
			                <img src="'.base_url().'uploads/file/'.$ber->berkas_file.'" alt="">
			                <div class="flex-caption">
			                    <h3>Quality Education</h3> 
								<p>We Teach Students for Sucessful Fututre</p> 
								 
			                </div>
			              </li>




						';
				}
				echo '

            </ul>
        <ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul>
    </div>
 
	</section></div>
';
			break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
							</div>
						</div>



						<!-- col-3 -->
						<div class="elementor <?php echo @$set_widget_3->nama_col;?> no-padding" style="height:<?php echo @$set_widget_3->tinggi;?>px; margin-bottom:20px;">
							<div class="box-header with-border">
								<div class="pull-left">
									<?php echo @$btn_tambah_3;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_3;?>
								</div>
							</div>
							
								<?php
								if(@$dt_widget_3->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_3->num_rows();
									foreach ($dt_widget_3->result() as $satu) {
										
switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
						<div class="elementor-inner">
							<div class="elementor-section-wrap">
								<div class="elementor-widget-container">
									<div class="widget widget-courses ">
										<div class="row">
											<div class="elementor-element elementor-element-3eefe78 elementor-widget elementor-widget-edumy_heading" data-id="3eefe78" data-element_type="widget" data-widget_type="edumy_heading.default">
											<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div>

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						<div class="col-lg-4 col-md-4 col-sm-6">
							<div class="course-grid">
								<div class="course-entry">
									<div class="course-cover">
										<div class="course-cover-thumb">
											<a href="#">
											<img width="614" height="400" src="https://demoapus.com/edumy/wp-content/uploads/2019/06/1110-614x400.jpg" class="attachment-edumy-course-grid size-edumy-course-grid wp-post-image" alt="The Complete Shopify Aliexpress Dropship"> </a>
											<div class="listing-btn-wrapper listing-wishlist">
												<a href="#apus-wishlist-add" class="btn btn-ct-link apus-wishlist-not-login" data-id="655">
													<i class="flaticon-like"></i><span class="wishlist-text">Add to wishlist</span>
												</a>
											</div>
										<div class="course-cover-label">
											<a href="">Preview Course </a>
										</div>
									</div>
								</div>
							<div class="course-detail">
								<div class="course-info-box">

									<div class="course-teacher"><a href="https://demoapus.com/edumy/profile/admin/">'.$cek_unit->unit.'</a></div>

										<a href="https://demoapus.com/edumy/courses/the-complete-shopify-aliexpress-dropship/">
											<h3 class="course-title">'.$ber->title.'</h3>
										</a>
									</div>
									<div class="course-meta-data">
										<p>'.substr($ber->content,0,100).' ...</p>
									</div>
								</div>
							</div>
						</div>
					</div>





						';
				}
				echo '


					</div>
					</div>
					</div>
					</div>
					</div>
					</div>

					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
	<section id="banner">
        <div id="main-slider" class="flexslider">
            <ul class="slides">
             

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li class="flex-active-slide">
			                <img src="'.base_url().'uploads/file/'.$ber->berkas_file.'" alt="">
			                <div class="flex-caption">
			                    <h3>Quality Education</h3> 
								<p>We Teach Students for Sucessful Fututre</p> 
								 
			                </div>
			              </li>




						';
				}
				echo '

            </ul>
        <ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul>
    </div>
 
	</section></div>
';
			break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
						</div>

						<!-- col-4 -->
						<div class="elementor <?php echo @$set_widget_4->nama_col;?> no-padding" style="height:<?php echo @$set_widget_4->tinggi;?>px; margin-bottom:20px;">
							<div class="box-header with-border">
								<div class="pull-left">
									<?php echo @$btn_tambah_4;?>
								</div>
								<div class="pull-right">
									<?php echo @$btn_setting_4;?>
								</div>
							</div>
							
								<?php
								if(@$dt_widget_4->num_rows() > 0){
									$no1 = 1; $jml = $dt_widget_4->num_rows();
									foreach ($dt_widget_4->result() as $satu) {
										
switch (@$satu->id_ref_konten){
			case 1:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
						<div class="elementor-inner">
							<div class="elementor-section-wrap">
								<div class="elementor-widget-container">
									<div class="widget widget-courses ">
										<div class="row">
											<div class="elementor-element elementor-element-3eefe78 elementor-widget elementor-widget-edumy_heading" data-id="3eefe78" data-element_type="widget" data-widget_type="edumy_heading.default">
											<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div>

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						<div class="col-lg-4 col-md-4 col-sm-6">
							<div class="course-grid">
								<div class="course-entry">
									<div class="course-cover">
										<div class="course-cover-thumb">
											<a href="#">
											<img width="614" height="400" src="https://demoapus.com/edumy/wp-content/uploads/2019/06/1110-614x400.jpg" class="attachment-edumy-course-grid size-edumy-course-grid wp-post-image" alt="The Complete Shopify Aliexpress Dropship"> </a>
											<div class="listing-btn-wrapper listing-wishlist">
												<a href="#apus-wishlist-add" class="btn btn-ct-link apus-wishlist-not-login" data-id="655">
													<i class="flaticon-like"></i><span class="wishlist-text">Add to wishlist</span>
												</a>
											</div>
										<div class="course-cover-label">
											<a href="">Preview Course </a>
										</div>
									</div>
								</div>
							<div class="course-detail">
								<div class="course-info-box">

									<div class="course-teacher"><a href="https://demoapus.com/edumy/profile/admin/">'.$cek_unit->unit.'</a></div>

										<a href="https://demoapus.com/edumy/courses/the-complete-shopify-aliexpress-dropship/">
											<h3 class="course-title">'.$ber->title.'</h3>
										</a>
									</div>
									<div class="course-meta-data">
										<p>'.substr($ber->content,0,100).' ...</p>
									</div>
								</div>
							</div>
						</div>
					</div>





						';
				}
				echo '


					</div>
					</div>
					</div>
					</div>
					</div>
					</div>

					</div>
					';
			break;
			case 2:
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_berita'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
				break;
			case 3:
				
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_pengumuman'));

				$judul = str_replace("%20"," ",$satu->judul);
				echo '

					<div class="elementor col-lg-12" style="margin-bottom:20px;">
					<div class="elementor-widget-container">
										 		<h2 class="edumy-heading-title edumy-size-default">'.$satu->judul.'</h2>
										 	</div><ul>';
				foreach ($dt_berita->result() as $ber) {
					echo '<li>'.$ber->title.'</li>';
				}
				echo '
					</ul>
					</div>
				';
			break;

			case 7:
				$from_unit = array(
					'front_portal a' => '',
					'ref_unit c' => array('c.id_unit = a.id_unit','left'),
				);
				$cek_unit = $this->general_model->datagrab(array(
				'tabel'=> $from_unit,'where'=>array('a.id_portal'=>$id_portal)))->row();
				$dt_berita = $this->general_model->datagrab(array(
				'tabel'=> 'front_slideshow'));

				//$title = 'Berita Full';
				$judul = str_replace("%20"," ",$satu->judul);
				echo '
					
					<div class="elementor col-lg-12" style="margin-bottom:20px;">
	<section id="banner">
        <div id="main-slider" class="flexslider">
            <ul class="slides">
             

					';
				foreach ($dt_berita->result() as $ber) {
					echo '
						 <li class="flex-active-slide">
			                <img src="'.base_url().'uploads/file/'.$ber->berkas_file.'" alt="">
			                <div class="flex-caption">
			                    <h3>Quality Education</h3> 
								<p>We Teach Students for Sucessful Fututre</p> 
								 
			                </div>
			              </li>




						';
				}
				echo '

            </ul>
        <ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul>
    </div>
 
	</section></div>
';
			break;

			
		}
			?>
									<?php $no1+=1; }
								}
								?>
						</div>
						
			<div class="col-md-12">
				<div class="aligncenter"><h2 class="aligncenter">Courses</h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus ovident, doloribus omnis minus temporibus perferendis nesciunt..</div>
				<br>
			</div>
		</div>
			<div class="row">
		<div class="skill-home"> <div class="skill-home-solid clearfix"> 
		<div class="col-md-3 text-center">
		<span class="icons c1"><i class="fa fa-book"></i></span> <div class="box-area">
		<h3>Vocational Courses</h3> <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
		</div>
		<div class="col-md-3 text-center"> 
		<span class="icons c2"><i class="fa fa-users"></i></span> <div class="box-area">
		<h3>MassComm Courses</h3> <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
		</div>
		<div class="col-md-3 text-center"> 
		<span class="icons c3"><i class="fa fa-trophy"></i></span> <div class="box-area">
		<h3>Accounts</h3> <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
		</div>
		<div class="col-md-3 text-center"> 
		<span class="icons c4"><i class="fa fa-globe"></i></span> <div class="box-area">
		<h3>Business Management</h3> <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
		</div></div>
		</div></div>
		</div> 
	 
						</div>
		</div>
		</div>
						</div>

	</div>
	</section>
	
	<section class="section-padding gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>Our Institute</h2>
						<p>Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada Pellentesque <br>ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="about-text">
						<p>Grids is a responsive Multipurpose Template. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

						<ul class="withArrow">
							<li><span class="fa fa-angle-right"></span> Lorem ipsum dolor sit amet</li>
							<li><span class="fa fa-angle-right"></span> consectetur adipiscing elit</li>
							<li><span class="fa fa-angle-right"></span> Curabitur aliquet quam id dui</li>
							<li><span class="fa fa-angle-right"></span> Donec sollicitudin molestie malesuada.</li>
						</ul>
						<a href="#" class="btn btn-primary">Learn More</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="about-image">
						<img src="<?php echo base_url().'assets/front/img/about.jpg' ?>" alt="About Images">
					</div>
				</div>
			</div>
		</div>
	</section>	  
	 
	<section id="call-to-action-2">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-sm-9">
					<h3>Welcom to E-Learning EDUCATION INSTITUTE</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus temporibus perferendis nesciunt quam repellendus nulla nemo ipsum odit corrupti</p>
				</div>
				<div class="col-md-2 col-sm-3">
					<a href="#" class="btn btn-primary">Read More</a>
				</div>
			</div>
		</div>
	</section>
	
	<div class="about home-about">
				<div class="container">
						
						<div class="row">
							<div class="col-md-4">
								<!-- Heading and para -->
								<div class="block-heading-two">
									<h3><span>Programes</span></h3>
								</div>
								<p>Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. <br><br>Sed ut perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus.</p>
							</div>
							<div class="col-md-4">
								<div class="block-heading-two">
									<h3><span>Latest News</span></h3>
								</div>		
								<!-- Accordion starts -->
								<div class="panel-group" id="accordion-alt3">
								 <!-- Panel. Use "panel-XXX" class for different colors. Replace "XXX" with color. -->
								  <div class="panel">	
									<!-- Panel heading -->
									 <div class="panel-heading">
										<h4 class="panel-title">
										  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseOne-alt3" class="collapsed">
											<i class="fa fa-angle-right"></i> Accordion Heading Text Item # 1
										  </a>
										</h4>
									 </div>
									 <div id="collapseOne-alt3" class="panel-collapse collapse" style="height: 0px;">
										<!-- Panel body -->
										<div class="panel-body">
										  Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
										</div>
									 </div>
								  </div>
								  <div class="panel">
									 <div class="panel-heading">
										<h4 class="panel-title">
										  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseTwo-alt3" class="collapsed">
											<i class="fa fa-angle-right"></i> Accordion Heading Text Item # 2
										  </a>
										</h4>
									 </div>
									 <div id="collapseTwo-alt3" class="panel-collapse collapse" style="height: 0px;">
										<div class="panel-body">
										  Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
										</div>
									 </div>
								  </div>
								  <div class="panel">
									 <div class="panel-heading">
										<h4 class="panel-title">
										  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseThree-alt3" class="collapsed">
											<i class="fa fa-angle-right"></i> Accordion Heading Text Item # 3
										  </a>
										</h4>
									 </div>
									 <div id="collapseThree-alt3" class="panel-collapse collapse">
										<div class="panel-body">
										  Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
										</div>
									 </div>
								  </div>
								  <div class="panel">
									 <div class="panel-heading">
										<h4 class="panel-title">
										  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseFour-alt3" class="collapsed">
											<i class="fa fa-angle-right"></i> Accordion Heading Text Item # 4
										  </a>
										</h4>
									 </div>
									 <div id="collapseFour-alt3" class="panel-collapse collapse">
										<div class="panel-body">
										  Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
										</div>
									 </div>
								  </div>
								</div>
								<!-- Accordion ends -->
								
							</div>
							
							<div class="col-md-4">
								<div class="timetable">
								  <h3><span class="fa fa-clock-o"></span> Time Table</h3>
								  <hr> 
								  <dl>
									<dt>Monday - Friday:</dt>
									<dd>9am-3pm</dd>
								  </dl>
								  <dl>
									<dt>Saturday:</dt>
									<dd>9am-1pm</dd>
								  </dl>  
								  <h4>Music Classes</h4>
								  <dl>
									<dt>Saturday:</dt>
									<dd>2pm-5pm</dd>
								  </dl>  
								</div>
							</div>
							
						</div>
						
						 						
						 
						<br>
					 
					  </div>
						
					</div>
					
					

	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Our Contact</h5>
					<address>
					<strong>E-Learning company Inc</strong><br>
					JC Main Road, Near Silnile tower<br>
					 Pin-21542 NewYork US.</address>
					<p>
						<i class="icon-phone"></i> (123) 456-789 - 1255-12584 <br>
						<i class="icon-envelope-alt"></i> email@domainname.com
					</p>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Quick Links</h5>
					<ul class="link-list">
						<li><a href="#">Latest Events</a></li>
						<li><a href="#">Terms and conditions</a></li>
						<li><a href="#">Privacy policy</a></li>
						<li><a href="#">Career</a></li>
						<li><a href="#">Contact us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Latest posts</h5>
					<ul class="link-list">
						<li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
						<li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
						<li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Recent News</h5>
					<ul class="link-list">
						<li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
						<li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
						<li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>© E-Learning 2018 All right reserved. By </span><a href="http://webthemez.com" E-Learning="_blank">WebThemez</a>
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-placement="top" title="" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="" data-original-title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup" style="display: none;"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Placed at the end of the document so the pages load faster -->


<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.easing.1.3.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/bootstrap.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.fancybox.pack.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.fancybox-media.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.flexslider.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/animate.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/modernizr.custom.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.isotope.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/jquery.magnific-popup.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/animate.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/front/js/custom.js'?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url().'assets/front/js/owl.carousel.js'?>"></script>
 -->
<script type="text/javascript">
	"use strict";
		var id_portal = "<?php echo $id_portal; ?>";
		$(document).ready(function(){
			get_ids();
		})

			function get_ids(){
				$.ajax({
					url : "<?php echo site_url($dir.'/load_data/');?>/"+id_portal,
					method : "GET",
					dataType : "json",
					success : function(data){
						const{status}=data;
						// console.log(data);
						if(status==1){
							$.map(data.result,function(item,index){
								// console.log(item.posisi);
								// tampilin(item.posisi, id_portal, item.id_ref_konten);
								$.ajax({
									url : `<?php echo site_url($dir.'/tampilin');?>/`+item.posisi+`/`+id_portal+`/`+item.id_ref_konten+`/`+item.judul,
									method : "GET",
									dataType : "JSON",
									success : function(res){
										const{id_ref_konten}=res;
										// console.log(status);
										if(status==1){
											
											$.map(res.result,function(is,idx){
												$('#post-'+item.posisi).append(is.isi);
											})
										}
									}
								});
							})
						}
					}
				});
			}

			function tampilin(posisi, id_portal, id_ref_konten, judul){
				// console.log(posisi)
				let content = null;;
					content = $('#post-'+posisi);
				
				
			}
			
		</script>

</body></html>