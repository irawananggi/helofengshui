<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Eskwela Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta name="twitter:title" content="">
<meta name="twitter:image" content="">
<meta name="twitter:url" content="">
<meta name="twitter:card" content="">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/animate.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/icomoon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/bootstrap.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/magnific-popup.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/flexslider.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.carousel.min.css';?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/owl.theme.default.min.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/fonts/flaticon/flaticon.css';?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/front/utama/css/style.css';?>">

<script src="<?php echo base_url().'assets/front/utama/js/modernizr-2.6.2.min.js';?>" type="text/javascript"></script>

<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="colorlib-loader" style="display: none;"></div>
<div id="page"><a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle colorlib-nav-white"><i></i></a><div id="colorlib-offcanvas"><ul>
<li class="active"><a href="index.html">Home</a></li>
<li class="offcanvas-has-dropdown">
<a href="courses.html">Courses</a>
<ul class="dropdown">
<li><a href="courses-single.html">Courses Single</a></li>
<li><a href="#">Mobile Apps</a></li>
<li><a href="#">Website</a></li>
<li><a href="#">Web Design</a></li>
<li><a href="#">WordPress</a></li>
</ul>
</li>
<li><a href="about.html">About</a></li>
<li><a href="event.html">Events</a></li>
<li><a href="news.html">News</a></li>
<li><a href="contact.html">Contact</a></li>
<li class="btn-cta"><a href="#"><span>Free Trial</span></a></li>
</ul></div>
<nav class="colorlib-nav" role="navigation">
<div class="upper-menu">
<div class="container">
<div class="row">
<div class="col-xs-4">
<p>Welcome to Eskwela</p>
</div>
<div class="col-xs-6 col-md-push-2 text-right">
<p>
</p><ul class="colorlib-social-icons">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-linkedin"></i></a></li>
<li><a href="#"><i class="icon-dribbble"></i></a></li>
</ul>
<p></p>
<p class="btn-apply"><a href="#">Apply Now</a></p>
</div>
</div>
</div>
</div>
<div class="top-menu">
<div class="container">
<div class="row">
<div class="col-md-2">
<div id="colorlib-logo"><a href="index.html">Eskwela</a></div>
</div>
<div class="col-md-10 text-right menu-1">
<ul>
<li class="active"><a href="index.html">Home</a></li>
<li class="has-dropdown">
<a href="courses.html">Courses</a>
<ul class="dropdown">
<li><a href="courses-single.html">Courses Single</a></li>
<li><a href="#">Mobile Apps</a></li>
<li><a href="#">Website</a></li>
<li><a href="#">Web Design</a></li>
<li><a href="#">WordPress</a></li>
</ul>
</li>
<li><a href="about.html">About</a></li>
<li><a href="event.html">Events</a></li>
<li><a href="news.html">News</a></li>
<li><a href="contact.html">Contact</a></li>
<li class="btn-cta"><a href="#"><span>Free Trial</span></a></li>
</ul>
</div>
</div>
</div>
</div>
</nav>
<aside id="colorlib-hero">
<div class="flexslider">
<ul class="slides">
<li style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_1.jpg';?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text animated fadeInUp">
<div class="slider-text-inner text-center">
<h1>Best Online Learning System</h1>
<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p>
</div>
</div>
</div>
</div>
</li>
<li style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
<div class="slider-text-inner text-center">
<h1>Online Free Course</h1>
<p><a href="#" class="btn btn-primary btn-lg btn-learn">Free Trial</a></p>
</div>
</div>
</div>
</div>
</li>
<li style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_3.jpg';?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
<div class="slider-text-inner text-center">
<h1>Education is a Key to Success</h1>
<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p>
</div>
</div>
</div>
</div>
</li>
<li style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_4.jpg';?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
<div class="slider-text-inner text-center">
<h1>Best Online Learning Center</h1>
<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p>
</div>
</div>
</div>
</div>
</li>
</ul>
<ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>
</aside>
<div id="colorlib-intro">
<div class="container">
<div class="row">
<div class="col-md-4 intro-wrap">
<div class="intro-flex">
<div class="one-third color-1 animate-boxx">
<span class="icon"><i class="flaticon-market"></i></span>
<div class="desc">
<h3>Learn Courses Online</h3>
<p><a href="#" class="view-more">View More</a></p>
</div>
</div>
<div class="one-third color-2 animate-boxx">
<span class="icon"><i class="flaticon-open-book"></i></span>
<div class="desc">
<h3>Online Library Store</h3>
<p><a href="#" class="view-more">View More</a></p>
</div>
</div>
<div class="one-third color-3 animate-boxx">
<div class="desc2">
<h3>50% off in all selected Courses</h3>
<p><a href="#" class="view-more">View More</a></p>
</div>
</div>
</div>
</div>
<div class="col-md-8">
<div class="about-desc animate-boxx">
<h2>Welcome to Eskwela</h2>
<p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
<div class="fancy-collapse-panel">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Why choose us?
</a>
</h4>
</div>
<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
<div class="panel-body">
<div class="row">
<div class="col-md-6">
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
</div>
<div class="col-md-6">
 <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingTwo">
<h4 class="panel-title">
<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">What we do?
</a>
</h4>
</div>
<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
<div class="panel-body">
<p>Far far away, behind the word <strong>mountains</strong>, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
<ul>
<li>Separated they live in Bookmarksgrove right</li>
<li>Separated they live in Bookmarksgrove right</li>
</ul>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingThree">
<h4 class="panel-title">
<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Offer Services
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
<div class="panel-body">
<p>Far far away, behind the word <strong>mountains</strong>, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="colorlib-services">
<div class="container">
<div class="row">
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-books"></i>
</span>
<div class="desc">
<h3>Professional Courses</h3>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-professor"></i>
</span>
<div class="desc">
<h3>Experienced Instructor</h3>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-book"></i>
</span>
<div class="desc">
<h3>Practical Training</h3>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
</div>
</div>
</div>
<div class="col-md-3 text-center animate-boxx">
<div class="services">
<span class="icon">
<i class="flaticon-diploma"></i>
</span>
<div class="desc">
<h3>Validated Certificate</h3>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-book"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="1539" data-speed="5000" data-refresh-interval="50">1539</span>
<span class="colorlib-counter-label">Courses</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-student"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="3653" data-speed="5000" data-refresh-interval="50">3653</span>
<span class="colorlib-counter-label">Students</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-professor"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="2300" data-speed="5000" data-refresh-interval="50">2300</span>
<span class="colorlib-counter-label">Teachers</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 animate-boxx">
<div class="counter-entry">
<span class="icon"><i class="flaticon-earth-globe"></i></span>
<div class="desc">
<span class="colorlib-counter js-counter" data-from="0" data-to="200" data-speed="5000" data-refresh-interval="50">200</span>
<span class="colorlib-counter-label">Countries</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="colorlib-classes colorlib-light-grey">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Our Classes</h2>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
</div>
</div>
<div class="row">
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-1.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Developing Mobile Apps</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-2.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Convert PSD to HTML</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-3.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Convert HTML to WordPress</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-2.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Developing Mobile Apps</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-3.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Learned Smoke Effects</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
<div class="col-md-4 animate-boxx">
<div class="classes">
<div class="classes-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/classes-6.jpg';?>);">
<span class="price text-center"><small>$450</small></span>
</div>
<div class="desc">
<h3><a href="#">Convert HTML to WordPress</a></h3>
<p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
<p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="colorlib-testimony" class="testimony-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>What Are The Students Says</h2>
</div>
</div>
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center">
<div class="row animate-boxx">
<div class="owl-carousel1 owl-carousel owl-theme owl-responsive--1 owl-loaded owl-text-select-on">



<div class="owl-stage-outer owl-height" style="height: 286px;"><div class="owl-stage" style="transform: translate3d(-2340px, 0px, 0px); transition: all 0s ease 0s; width: 5460px;"><div class="owl-item cloned" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>John Collins</span>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item cloned" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>Adam Ross</span>
<p>Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>Sophia Foster</span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item active" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>John Collins</span>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>Adam Ross</span>
<p>Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item cloned" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>Sophia Foster</span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div><div class="owl-item cloned" style="width: 780px; margin-right: 0px;"><div class="item">
<div class="testimony-slide">
<div class="testimony-wrap">
<blockquote>
<span>John Collins</span>
<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
</blockquote>
<div class="figure-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);"></div>
</div>
</div>
</div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style="display: none;"><i class="icon-arrow-left3 owl-direction"></i></div><div class="owl-next" style="display: none;"><i class="icon-arrow-right3 owl-direction"></i></div></div><div class="owl-dots" style=""><div class="owl-dot"><span></span></div><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div></div></div></div>
</div>
</div>
</div>
</div>
</div>
<div class="colorlib-trainers">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Our Experienced Professor</h2>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
</div>
</div>
<div class="row">
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>)"></div>
<div class="desc">
<h3>Olivia Young</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>)"></div>
<div class="desc">
<h3>Daniel Anderson</h3>
<span>Professor</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>)"></div>
<div class="desc">
<h3>David Brook</h3>
<span>Teacher</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 animate-boxx">
<div class="trainers-entry">
<div class="trainer-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>)"></div>
<div class="desc">
<h3>Brigeth Smith</h3>
<span>Teacher</span>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="colorlib-event">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Upcoming Events</h2>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
</div>
</div>
<div class="event-flex row-pb-sm">
<div class="half event-img animate-boxx" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);">
</div>
<div class="half">
<div class="row">
<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Organized by:</span> <span>Noah Henderson</span></p>
<h2><a href="event.html">We Held Free Training for Basic Programming</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>291 South 21th Street, Suite 721 New York NY 10016</p>
</div>
</div>
</div>
<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Organized by:</span> <span>Noah Henderson</span></p>
<h2><a href="event.html">You're Invited 1st Anniversary of Eskwela</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>291 South 21th Street, Suite 721 New York NY 10016</p>
</div>
</div>
</div>
<div class="col-md-12 animate-boxx">
<div class="event-entry">
<div class="desc">
<p class="meta"><span class="day">19</span><span class="month">Apr</span></p>
<p class="organizer"><span>Organized by:</span> <span>Noah Henderson</span></p>
<h2><a href="event.html">Practice Workshop 2018</a></h2>
</div>
<div class="location">
<span class="icon"><i class="icon-map"></i></span>
<p>291 South 21th Street, Suite 721 New York NY 10016</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="colorlib-blog colorlib-light-grey">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Recent News</h2>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
</div>
</div>
<div class="row">
<div class="col-md-6 animate-boxx">
<article class="article-entry">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);">
<p class="meta"><span class="day">18</span><span class="month">Apr</span></p>
</a>
<div class="desc">
<h2><a href="blog.html">Creating Mobile Apps</a></h2>
<p class="admin"><span>Posted by:</span> <span>James Smith</span></p>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
</div>
</article>
</div>
<div class="col-md-6">
<div class="f-blog animate-boxx">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_1.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">How to Create Website in Scratch</a></h2>
<p class="admin"><span>04 March 2018</span></p>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
</div>
</div>
<div class="f-blog animate-boxx">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">How to Convert PSD File to HTML File?</a></h2>
<p class="admin"><span>04 March 2018</span></p>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
</div>
</div>
<div class="f-blog animate-boxx">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_3.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">How to Build Games App in Mobile</a></h2>
<p class="admin"><span>04 March 2018</span></p>
<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="colorlib-subscribe" class="subs-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>); background-position: 50% 50%;" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-boxx">
<h2>Subscribe Newsletter</h2>
<p>Subscribe our newsletter and get latest update</p>
</div>
</div>
<div class="row animate-boxx">
<div class="col-md-6 col-md-offset-3">
<div class="row">
<div class="col-md-12">
<form class="form-inline qbstp-header-subscribe">
<div class="col-three-forth">
<div class="form-group">
<input type="text" class="form-control" id="email" placeholder="Enter your email">
</div>
</div>
<div class="col-one-third">
<div class="form-group">
<button type="submit" class="btn btn-primary">Subscribe Now</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<footer id="colorlib-footer">
<div class="container">
<div class="row row-pb-md">
<div class="col-md-3 colorlib-widget">
<h4>About Eskwela</h4>
<p>Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
<p>
</p><ul class="colorlib-social-icons">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-linkedin"></i></a></li>
<li><a href="#"><i class="icon-dribbble"></i></a></li>
</ul>
<p></p>
</div>
<div class="col-md-3 colorlib-widget">
<h4>Quick Links</h4>
<p>
</p><ul class="colorlib-footer-links">
<li><a href="#"><i class="icon-check"></i> About Us</a></li>
<li><a href="#"><i class="icon-check"></i> Testimonials</a></li>
<li><a href="#"><i class="icon-check"></i> Courses</a></li>
<li><a href="#"><i class="icon-check"></i> Event</a></li>
<li><a href="#"><i class="icon-check"></i> News</a></li>
<li><a href="#"><i class="icon-check"></i> Contact</a></li>
</ul>
<p></p>
</div>
<div class="col-md-3 col1rlib-widget">
<h4>Recent Post</h4>
<div class="f-blog">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">Creating Mobile Apps</a></h2>
<p class="admin"><span>18 April 2018</span></p>
</div>
</div>
<div class="f-blog">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_2.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">Creating Mobile Apps</a></h2>
<p class="admin"><span>18 April 2018</span></p>
</div>
</div>
<div class="f-blog">
<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url().'assets/front/utama/images/img_bg_3.jpg';?>);">
</a>
<div class="desc">
<h2><a href="blog.html">Creating Mobile Apps</a></h2>
<p class="admin"><span>18 April 2018</span></p>
</div>
</div>
</div>
<div class="col-md-3 colorlib-widget">
<h4>Contact Info</h4>
<ul class="colorlib-footer-links">
<li>291 South 21th Street, <br> Suite 721 New York NY 10016</li>
<li><a href="tel://1234567920"><i class="icon-phone"></i> + 1235 2355 98</a></li>
<li><a href="mailto:info@yoursite.com"><i class="icon-envelope"></i> info@yoursite.com</a></li>
<li><a href="http://luxehotel.com"><i class="icon-location4"></i> yourwebsite.com</a></li>
</ul>
</div>
</div>
</div>
<div class="copy">
<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<p>


<small class="block">&copy; 
Copyright &copy;<script data-cfasync="false" src="<?php echo base_url().'assets/front/utama/js/email-decode.min.js';?>"></script><script type="7f613dbdfcafd0d7a14d0a38-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="http://localhost/elearning/" target="_blank">Colorlib</a>

</small><br>
<small class="block">Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a>, <a href="http://pexels.com/" target="_blank">Pexels</a></small>
</p>
</div>
</div>
</div>
</div>
</footer>
</div>
<div class="gototop js-top">
<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.easing.1.3.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/bootstrap.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.waypoints.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.stellar.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.flexslider-min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/owl.carousel.min.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.magnific-popup.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/front/utama/js/magnific-popup-options.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/jquery.countTo.js';?>" type="text/javascript"></script>

<script src="<?php echo base_url().'assets/front/utama/js/main.js';?>" type="text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="ext/javascript"></script>
<script type="ext/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="49" defer=""></script></body>
</html>