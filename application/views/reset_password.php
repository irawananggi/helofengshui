<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url().'assets/favicon/favicon.ico' ?>">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url().'assets/fengshui/admin/css/styles.css' ?>">
    <title>Fengshui - ADMIN</title>
     <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/plugins/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <style>
        #box_message { margin-top: 20px; font-size: 130% }
        .exlcm { margin-right: 10px; }
        
    </style>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
    

  <?php
    $this_css='assets/css/opd.css';if(file_exists($this_css))echo reqcss($this_css)."\n";
  ?>
  </head>
 <?php 
  $ok = $this->session->flashdata('ok');
  $fail = $this->session->flashdata('fail');
  ?>
<body>
    <div class="auth-container">
        <div class="text-center">
            <img src="images/logo.svg" width="90" class="img-fluid" alt="">
        </div>
        <?php 
                        
                    $path_inst_logo = !empty($st['pemerintah_logo']) ? FCPATH.'logo/'.$st['pemerintah_logo'] : null;
                    $logo_instansi = (file_exists($path_inst_logo) and !empty($st['pemerintah_logo'])) ? base_url().'logo/'.$st['pemerintah_logo'] : base_url().'assets/logo/brand.png';
                    ?>
                    <div class="logo-brand fit-logo"><img src="<?php echo base_url().'uploads/logo/logo.svg'; ?>" /></div>
                    
        <h4 class="my-4 text-white">Setel ulang kata sandi baru</h4>
                    
        <form action="<?php echo base_url('Reset_pass/auth');?>" name="login_form " id="login_form" method="post" accept-charset="utf-8">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div id="box_message"></div>
                    <?php 
                    if (!empty($ok)) echo '<div class="alert alert-success"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp; '.$ok.'</div>';
                    if (!empty($fail)) echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp; '.$fail.'</div>';
                    ?>

                    <div class="form-inline-group left-i mb-3">
                        <div style="text-align: left;padding:10px;">Kata sandi baru</div>
                       <?php echo form_password('password',NULL,'class="form-control"  style="padding-left:10px" id="pass"')?><i id="satu" class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                    </div>
                    <div class="form-inline-group left-i mb-3">
                        <div style="text-align: left;padding:10px;">Ulangi kata sandi</div>
                        <?php echo form_password('re_password',null,'class="form-control" id="re_password" style="padding-left:10px"')?><i id="dua" class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                    </div>


            <button type="submit" class="btn btn-warning px-5 mt-4 btn-lgn">Simpan Kata Sandi</button>
        
            </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $("#satu").click(function(){               
                if ($(this).hasClass("glyphicon-eye-open")==true) {
                    $("#pass").attr("type", "text");                    
                    $(this).removeClass("glyphicon glyphicon-eye-open");
                    $(this).addClass("glyphicon glyphicon-eye-close");
                    
                } else {
                    $("#pass").attr("type", "password");
                    $(this).removeClass("glyphicon-eye-close");
                    $(this).addClass("glyphicon-eye-open");                 
                }               
            });
            $("#dua").click(function(){               
                if ($(this).hasClass("glyphicon-eye-open")==true) {
                    $("#re_password").attr("type", "text");                    
                    $(this).removeClass("glyphicon glyphicon-eye-open");
                    $(this).addClass("glyphicon glyphicon-eye-close");
                    
                } else {
                    $("#re_password").attr("type", "password");
                    $(this).removeClass("glyphicon-eye-close");
                    $(this).addClass("glyphicon-eye-open");                 
                }               
            });
        });
    </script>
</body>

</html>