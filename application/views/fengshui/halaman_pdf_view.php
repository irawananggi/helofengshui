<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title> PDF <?php echo @$title;?></title>
     <link href="$_SERVER['DOCUMENT_ROOT'].'assets/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        table{
            border-spacing:0;border-collapse:collapse
            }
            td,th{
                padding:5px
            }
            th{
                text-align: center;
            }
        .table{
            border-collapse:collapse!important
            }
            .table td,.table th{
                background-color:#fff!important
                }
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important
                }

    </style>
</head>
<body>
<h1 class="text-center bg-info"><?php echo @$title;?></h1>
   <?php

        if ($data_article->num_rows() > 0) { ?>
            <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium"  style="width: 20px;">No</td>
                <td scope="col" class="text-2 medium">Judul</td>
                <td scope="col" class="text-2 medium" style="width: 350px;">Deskripsi</td>
                <td scope="col" class="text-2 medium">Tanggal</td>
              </tr>
            </thead>
            <tbody>
              
                <?php
                $no = 1 + $offs;
                foreach ($data_article->result() as $row) { ?>
                <tr>
                  <td class="align-middle" style="width: 60px;"><?php echo $no;?></td>
                  <td class="align-middle">
                    <?php echo $row->judul;?>
                  </td>
                  <td class="align-middle">
                    <?php echo $row->deskripsi;?>
                  </td>
                  <td class="align-middle">
                    <?php echo tanggal_indo(date('Y-m-d', strtotime($row->created_at)));?>
                  </td>
                </tr>
               
                <?php 
                $no++;
              }
               ?>
            </tbody>
          </table>
      <?php  }else{ 
          echo '<div class="alert">Data masih kosong ...</div>';
        }
      ?>
</body>
</html>