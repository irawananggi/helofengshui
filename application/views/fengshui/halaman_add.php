
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                 <input type="hidden" name="id" value="" class="form-control main-form-control" placeholder="Judul Halaman ">
                <div class="row">
                      <input type="hidden" name="id" value="<?php echo $dt->id;?>">
                  <div class="col-lg-12 mb-12">
                    <label>Indonesia</label>
                    <div class="form-group">
                      <input type="text" name="judul" value="<?php echo $dt->judul;?>" class="form-control main-form-control" placeholder="Judul Halaman " required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi" id="editor_in" value="<?php echo $dt->deskripsi;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="Deskripsi Halaman" required><?php echo $dt->deskripsi;?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 mb-12">
                    <label>Inggris</label>
                    <div class="form-group">
                      <input name="judul_en" type="text" value="<?php echo $dt->judul_en;?>" class="form-control main-form-control" placeholder="Page Title" required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi_en" id="editor_en" value="<?php echo $dt->deskripsi_en;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="Page Description" required><?php echo $dt->deskripsi_en;?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 mb-12">
                    <label>Mandarin</label>
                    <div class="form-group">
                      <input name="judul_man" type="text" value="<?php echo $dt->judul_man;?>" class="form-control main-form-control" placeholder="页面标题" required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi_man" id="editor_man" value="<?php echo $dt->deskripsi_man;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="页面说明" required><?php echo $dt->deskripsi_man;?></textarea>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 mb-12">
                    <label>Upload Foto </label>
                      <div class="single-file-upload no-label">
                        <input name="foto" type="file" id="upload_foto" hidden="">
                        <label for="upload_foto" class="btn btn-primary text-2 px-5 mb-0">
                          Pilih File
                        </label>
                        <div class="filename"><?php echo $dt->foto;?></div>
                      </div>
                    </div>
                </div>
                  
                </div>

                


                <div class="col-lg-12 mb-12">
                  <div class="mt-5">
                    <button type="submit" class="btn btn-warning px-5"><?php echo $tombol;?></button>
                        <br>
                        <br>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/popper.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.dataTables.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.bootstrap4.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.rowReorder.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.responsive.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap-datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/all.js' ?>"></script>


<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script>
    CKEDITOR.replace('editor_in');
    CKEDITOR.replace('editor_en');
    CKEDITOR.replace('editor_man');
</script>