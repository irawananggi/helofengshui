
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo site_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18">Permintaan - Detail Kategori </span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <div class="row">
                


                <div class="col-lg-4 mb-4">
                  <label>Indonesia</label>
                  <div class="form-group">
                    <input type="text" class="form-control main-form-control" disabled value="<?php  echo $kategori_detail->name_in; ?>">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $kategori_detail->deskripsi_in; ?></textarea>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $kategori_detail->ketentuan_in; ?></textarea>
                  </div>
                </div>
                <div class="col-lg-4 mb-4">
                  <label>Inggris</label>
                  <div class="form-group">
                    <input type="text" class="form-control main-form-control" disabled value="<?php  echo $kategori_detail->name_en; ?>">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $kategori_detail->deskripsi_en; ?></textarea>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $kategori_detail->ketentuan_en; ?></textarea>
                  </div>
                </div>
                <div class="col-lg-4 mb-4">
                  <label>Mandarin</label>
                  <div class="form-group">
                    <input type="text" class="form-control main-form-control" disabled value="<?php  echo $kategori_detail->name_man; ?>">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4" disabled><?php  echo $kategori_detail->deskripsi_man; ?></textarea>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control main-form-control" cols="30" rows="4" disabled><?php  echo $kategori_detail->ketentuan_man; ?></textarea>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-4 mb-4">
                  <label>Harga</label>
                  <div class="form-inline-group left-i">
                    <i class="in-left font-normal">Rp</i>
                    <input type="text" class="form-control main-form-control" disabled value="<?php  echo $kategori_detail->harga; ?>">
                  </div>
                </div>
                <div class="col-lg-4 mb-4">
                  <label>Foto </label>
                  <div>
                    <img src="<?php echo base_url('uploads/file/'.$kategori_detail->foto) ;?>" width="80" alt="">
                  </div>
                </div>
                <div class="col-lg-4 mb-4">
                  <label>Icon </label>
                  <div>
                    <img src="<?php echo base_url('uploads/file/'.$kategori_detail->icon) ;?>" width="50" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>