<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title> PDF <?php echo @$title;?></title>
     <link href="$_SERVER['DOCUMENT_ROOT'].'assets/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
      <style type="text/css">
        table{
          font-size:10px;
          width:100%;
                padding:10px;
            border-spacing:0;border-collapse:collapse
            }
            tr,td,th{
                padding:10px
            }
            th{
                text-align: center;padding:10px
            }
        .table{
            border-collapse:collapse!important
            }
            
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important;
                    padding:10px;
                }

    </style>
</head>
<body>
<h1 class="text-center bg-info"><?php echo @$title;?></h1>
   <?php
          if ($data->num_rows() > 0) { ?>
          <table  class="table table-striped table-bordered"  style="width: 100%;">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium" style="width: 40px;">No</td>
                <td scope="col" class="text-2 medium" style="width: 160px;">Nama Customers</td>
                <td scope="col" class="text-2 medium">Kategori</td>
                <td scope="col" class="text-2 medium">Tgl permintaan</td>
                <td scope="col" class="text-2 medium">Status permintaan</td>
              </tr>
            </thead>
            <tbody>
               <?php 
              $no=1;
              foreach ($data->result() as $row) { 
                $data_kat = $this->general_model->datagrab(array('tabel'=>'kategori','where'=>array('id'=>$row->kategori_id)))->row();
                $data_member = $this->general_model->datagrab(array('tabel'=>'member','where'=>array('member_id'=>$row->member_id)))->row();
                ?>
              <tr>
                <td class="align-middle" style="width: 40px;"><?php echo $no;?></td>
                <td class="align-middle" style="width: 160px;"><?php echo $data_member->nama_lengkap;?></td>
                <td class="align-middle">
                  <a href="<?php echo site_url($this->dir.'/permintaan_detail_form/'.$row->id);?>" class="text-warning underline">ID : <?php echo $data_kat->name_in;?></a>
                </td>
                <td class="align-middle">
                  <?php echo tanggal_indo(date('Y-m-d', strtotime($row->tgl_permintaan))) ;?>
                </td>
                <td class="align-middle">

                   
                   <?php 
                    if ($row->status_permintaan == 'Menunggu pembayaran') { 
                     
                      $color = "color:#c23336 !important";
                      $status_danger = "status_danger";
                    }elseif($row->status_permintaan == 'Sedang diproses') {
                      $color = "color:#7fdccf !important";
                      $status_danger = "status_danger2";

                    }elseif($row->status_permintaan == 'dibatalkan') {
                      $color = "color: #2837ff !important";
                      $status_danger = "status_danger3";

                    }else{
                      $color = "color: #dbbf85 !important";
                      $status_danger = "status_danger4";

                    }?>
                    <style type="text/css">
                    <?php 
                    if ($row->status_permintaan == 'Menunggu pembayaran') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #c23336 !important;
                        }
                     <?php }elseif($row->status_permintaan == 'Sedang diproses') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #7fdccf !important;
                        }

                     <?php }elseif($row->status_permintaan == 'dibatalkan') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #2837ff !important;
                        }
                     <?php }else{ ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #dbbf85 !important;
                        }
                     <?php } ?>
                      </style>
                  <div class="status <?php echo $status_danger;?>" style="<?php echo $color;?>">

                   

                    <?php echo $row->status_permintaan;?>


                  </div>

                </td>
                
              </tr>
              <?php  $no++;
          } ?>
         
            </tbody>
          </table>
          <?php
                 }else{
            echo '<div class="alert">Data masih kosong ...</div>';
          } ?>
        </div>
</body>
</html>