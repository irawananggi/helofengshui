
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                 <input type="hidden" name="id" value="" class="form-control main-form-control" placeholder="Nama kategori ">
                <div class="row">
                  
                      <input type="hidden" name="id_pegawai" value="<?php echo $dt->id_pegawai;?>">
                  <div class="col-lg-6 mb-6">
                  
                    <label>Username</label>
                    <div class="form-group">
                      <input name="username" value="<?php echo $dt->username;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="https://helofengshui.com"></input>
                    </div>
                    <label>Password</label>
                    <div class="form-group">
                      <input name="password" type='password' value="<?php echo $dt->password;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="https://helofengshui.com"></input>
                    </div>
                  </div>
                </div>

                


                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5"><?php echo $tombol;?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
