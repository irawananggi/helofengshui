
  <div class="main-content-inner">
    <div class="subheader-outer">
      <div class="row align-items-center">
        <div class="col-md-12 mb-4">
          <div class="sub-header">
            <div class="col-search">
              <?php echo @$extra_tombol?>
            </div>
            <div class="ml-auto">
                  <a href="<?php echo base_url($this->dir.'/pdf/'.$search);?>" target="_blank" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>
                  <?php echo @$tombol?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main-content-inner-child">
      <?php

        if ($data_article->num_rows() > 0) { ?>
            <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Nama Lengkap</td>
                <td scope="col" class="text-2 medium">Tanggal bergabung </td>
                <td scope="col" class="text-2 medium text-center">Non Aktifkan</td>
              </tr>
            </thead>
            <tbody>
              
                <?php
                $no = 1 + $offs;
                foreach ($data_article->result() as $row) { ?>
                <tr>
                  <td class="align-middle" style="width: 20px;"><?php echo $no;?></td>
                  <td class="align-middle">
                    <div>
                      <div class="user-table">
                        <div class="user-ava">
                          <img src="<?php echo base_url('uploads/member/'.$row->img_file);?>" alt="">
                        </div>
                        <div class="user-text">
                          <p class="mb-0"><?php echo $row->nama_lengkap;?></p>
                          <p class="mb-0 text-12 text-grey"><?php echo $row->email;?></p>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td class="align-middle">
                    <?php echo tanggal_indo(date('Y-m-d', strtotime($row->created_at)));?>
                  </td>
                  <td class="align-middle text-center">
                    <?php 
                    if($row->is_active == 1){
                      $checked = "";
                    }else{
                      $checked = "checked";
                    }
                    ?>

                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" onclick="location.href='<?php echo base_url($this->dir.'/edit_status/'.$row->member_id);?>';" id="customSwitch<?php echo $no;?>" <?php echo $checked;?>>
                      <label class="custom-control-label" for="customSwitch<?php echo $no;?>"></label>
                    </div>
                  </td>
                </tr>
               
                <?php 
                $no++;
              }
               ?>
            </tbody>
          </table>
      <?php  }else{ 
          echo '<div class="alert">Data masih kosong ...</div>';
        }
      ?>
    </div>
  </div>
</div>
 

