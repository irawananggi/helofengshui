
  <div class="main-content-inner">
    <div class="subheader-outer">
      <div class="row align-items-center">
        <div class="col-md-12 mb-4">
          <div class="sub-header">
            <div class="col-search">
              <?php echo @$extra_tombol?>
            </div>
            <div class="ml-auto">
                  <a href="<?php echo base_url($this->dir.'/pdf/'.$search);?>" target="_blank" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>
                  <?php echo @$tombol?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main-content-inner-child">
          <?php

          if ($data->num_rows() > 0) { ?>
          <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="">Status</td>
                <td scope="col" class="text-2 medium">Name</td>
                <td scope="col" class="text-2 medium">Code</td>
                <td scope="col" class="text-2 medium">Start Date</td>
                <td scope="col" class="text-2 medium">End Date</td>
                <td scope="col" class="text-2 medium">Minimal Shop</td>
                <td scope="col" class="text-2 medium">Diskon</td>
                <td scope="col" class="text-2 medium">Nominal Voucher</td>
                <td scope="col" class="text-2 medium">Action</td>
              </tr>
            </thead>
            <tbody>
            <?php
            $no = 1 + $offs;
            foreach ($data->result() as $row) { ?>
              <tr>
                <td style="width: 60px;"><?php echo $no;?></td>
                <td class="">
                    <?php 
                    if($row->status == 1){
                      $checked = "";
                    }else{
                      $checked = "checked";
                    }
                    ?>

                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" onclick="location.href='<?php echo base_url($this->dir.'/edit_status/'.$row->id);?>';" id="customSwitch<?php echo $no;?>" <?php echo $checked;?>>
                      <label class="custom-control-label" for="customSwitch<?php echo $no;?>"></label>
                    </div>
                </td>
                <td>
                  <?php echo $row->name_voucher;?>
                </td>
                <td>
                  <?php echo $row->code_voucher;?>
                </td>
                <td>
                  <?php echo tanggal_indo($row->start_date);?>
                </td>
                <td>
                  <?php echo tanggal_indo($row->end_date);?>
                </td>
                <td>
                  <?php echo numberToCurrency($row->min_s);?>
                </td>
                <td>
                  <?php 
                  if($row->dis_id == 1){
                      $dis = 'Persen';
                  }else{
                      $dis = 'Rupiah';


                  }
                  echo $dis;?>
                </td>
                <td>
                  <?php  
                  if($row->dis_id == 1){
                      $nom = numberToCurrency($row->nominal).'%';
                  }else{
                      $nom = numberToCurrency($row->nominal);
                  }
                  echo $nom;?>
                </td>
                <td>
                  
                  <div class="mb-2">
                     <?php echo anchor(site_url($this->dir.'/add_data/'.$row->id)
                        ,'<i class="icon-fi-sr-pencil"></i>'
                        ,'class="btn btn-action btn-sm" act="#"'
                        )
                    ?>
                   
                  </div>

                </td>
              </tr>
              <?php 
                $no++;
              } ?>
            </tbody>
          </table>
          <?php  }else{ 
            echo '<div class="alert">Data masih kosong ...</div>';
          }
        ?>
        </div>
</div>

