<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title> PDF <?php echo @$title;?></title>
     <link href="$_SERVER['DOCUMENT_ROOT'].'assets/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        table{
            border-spacing:0;border-collapse:collapse
            }
            td,th{
                padding:5px
            }
            th{
                text-align: center;
            }
        .table{
            border-collapse:collapse!important
            }
            .table td,.table th{
                background-color:#fff!important
                }
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important
                }

    </style>
</head>
<body>
<h1 class="text-center bg-info"><?php echo @$title;?></h1>
   
          <?php

          if ($data->num_rows() > 0) { ?>
<table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <td scope="col" class="text-2 medium">No</td>
                  <td scope="col" class="text-2 medium">Nama Kategori</td>
                  <td scope="col" class="text-2 medium">Deskripsi </td>
                  <td scope="col" class="text-2 medium">Ketentuan</td>
                  <td scope="col" class="text-2 medium">Harga</td>
                </tr>
              </thead>
              <tbody>

            <?php 
              $no=1;
              foreach ($data->result() as $row) { ?>
              <tr>
                <td style="width: 60px;"><?php echo $no;?></td>
                <td>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">ID :</span> <?php echo $row->name_in;?>
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">EN :</span> <?php echo $row->name_en;?>
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">MD :</span> <?php echo $row->name_man;?>
                  </div>
                </td>
                <td>
                  <div class="content-table">
                   <?php echo $row->deskripsi_in;?>
                  </div>
                  <div class="content-table">
                   <?php echo $row->deskripsi_en;?>
                  </div>
                  <div class="content-table">
                    <?php echo $row->deskripsi_man;?>
                  </div>
                </td>
                <td>
                  <div class="content-table">
                   <?php echo $row->ketentuan_in;?>
                  </div>
                  <div class="content-table">
                   <?php echo $row->ketentuan_en;?>
                  </div>
                  <div class="content-table">
                    <?php echo $row->ketentuan_man;?>
                  </div>
                </td>
                <td>
                   <?php echo rupiah($row->harga) ;?>
                </td>
                
              </tr>
            <?php  $no++;
          } ?>
         
            
              
            </tbody>
          </table>
          <?php
                 }else{
            echo '<div class="alert">Data masih kosong ...</div>';
          } ?>
</body>
</html>