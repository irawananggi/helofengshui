
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                 <input type="hidden" name="id" value="" class="form-control main-form-control" placeholder="Nama kategori ">
                <div class="row">
                  
                      <input type="hidden" name="id" value="<?php echo $dt->id;?>">
                  <div class="col-lg-6 mb-6">
                    <label>Judul</label>
                    <div class="form-group">
                      <input type="text" name="judul" value="<?php echo $dt->judul;?>" class="form-control main-form-control" placeholder="Judul Banner ">
                    </div>
                    <label>Url</label>
                    <div class="form-group">
                      <textarea name="deskripsi" value="<?php echo $dt->deskripsi;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="https://helofengshui.com"><?php echo $dt->deskripsi;?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-6 mb-6">
                    <div class="col-lg-12 mb-12">
                    <label>Upload Foto </label>
                      <div class="single-file-upload no-label">
                        <input name="foto" type="file" id="upload_foto" hidden="">
                        <label for="upload_foto" class="btn btn-primary text-2 px-5 mb-0">
                          Pilih File
                        </label>
                        <div class="filename"><?php echo $dt->foto;?></div>
                      </div>
                    </div>
                  </div>
                  
                </div>

                


                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5"><?php echo $tombol;?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/popper.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.dataTables.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.bootstrap4.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.rowReorder.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.responsive.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap-datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/all.js' ?>"></script>

<script>

    $(document).on('click', '#add-form', function(e){
    // $('#add-form').click(function (e) {
      var formlength2 = $('.form-input').children(".form-input-list").length+1;
      var formlength = $('.form-input').children(".form-input-list").length;
      e.preventDefault();
      var tmpt = '<div class="form-input-list">'+
        '<h5 class="text-primary mb-3">Form Inputan ' + formlength2 + '</h5>' +
        '<div class="row">' +
        '<div class="col-lg-4 mb-4">' +
        '<input type="hidden" class="form-input-list" name="jmlfile" value="' + formlength2 + '">' +
        '<label>Pilih Jenis Form <span class="text-primary text-10"></span></label>' +
        '<select name="jenis_form_id'+formlength+'" class="form-control searchless">' +
        '<option value="1">Text</option>' +
        '<option value="2">Tanggal</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Indonesia</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_in'+formlength+'" type="text" class="form-control main-form-control" placeholder="Nama kategori ">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Inggris</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_en'+formlength+'" type="text" class="form-control main-form-control" placeholder="Name Category ">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Mandarin</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_man'+formlength+'" type="text" class="form-control main-form-control" placeholder="分类名称">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
      $('.form-input').append(tmpt);
      $('.searchless').select2({
        theme: 'bootstrap4 main-form-control',
        placeholder: "Pilih",
        minimumResultsForSearch: -1
      });
    })
  </script>

