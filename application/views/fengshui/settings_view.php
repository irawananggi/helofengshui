
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                
                <div class="row">
                    <label>No Telepon</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend" style="color: #424242;display: inline-block;padding: 10px 10px 5px 10px;border-radius: 30px;    vertical-align: middle;    background-color: #EEEEEE;">
                      <img alt="ID Flag" src="<?php echo base_url('assets/images/flag-id.svg');?>"> +62
                    </div>
                  <div class="col-lg-6 mb-6">
                    <input name="no_telpon" onkeypress="return event.charCode >= 48 && event.charCode <=57"  maxlength="12" value="<?php echo $dt->val;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="08xxxxxxxx"></input>
                  </div>
                  </div>
                </div>

                


                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5"><?php echo $tombol;?></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
