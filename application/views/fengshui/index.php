<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url() . 'assets/favicon/favicon.ico' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/fengshui/landing/css/styles.css' ?>">
  <?php
  if ($this->session->userdata('bahasa') == 'in') {
    echo '<title>Helo fengshui | Sekarang Fengshui Ada di Dalam Genggamanmu |  Tentang Fengshui App | Fitur Fengshui App </title>';
  } elseif ($this->session->userdata('bahasa') == 'en') {
    echo '<title>Hello Fengshui | Now Feng Shui is in your grasp | Fengshui App Features </title>';
  } else {
    echo '<title>Helo Fengshui | 现在风水在您的掌握中 |  关于风水应用 | 风水应用功能 </title>';
  }

  ?>
</head>

<body>
  <header class="header">
    <div class="header-top">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-sm-7 col-9">
            <?php
            $no = 1 + $offs;
            foreach ($data_link_1->result() as $row) { ?>
              <a target="_blank" href="<?php echo $row->deskripsi; ?>" class="mr-3 sosial-media color-primary" style="margin-right: 0px!important">
                <img src="<?php echo base_url('uploads/link/' . $row->foto); ?>" style="width: 33px;height: 30px" />
              </a>
            <?php } ?>
            <!--   <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-facebook-square"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-instagram"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-twitter-square"></span>
                    </a>
                    <a href="#" class="mr-3 sosial-media color-primary">
                        <span class="icon-icon-awesome-linkedin"></span>
                    </a> -->
          </div>
          <div class="col-lg-6 col-sm-5 col-3">
            <div class="text-right">

              <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php
                  $uri_1 = $this->uri->segment(1, 0);
                  $uri_2 = $this->uri->segment(2, 0);
                  $uri_3 = $this->uri->segment(3, 0);
                  $uri = in_de(array('uri1' => $this->uri->segment(1, 0), 'uri2' => $this->uri->segment(2, 0)));

                  if ($this->session->userdata('bahasa') == 'in') {
                    echo '<i class="ico-flag id-flag"></i> <span class="lang-text">Bahasa Indonesia</span>';
                  } elseif ($this->session->userdata('bahasa') == 'en') {
                    echo '<i class="ico-flag en-flag"></i> <span class="lang-text">Inggris</span>';
                  } else {
                    echo '<i class="ico-flag cn-flag"></i> <span class="lang-text">Mandarin</span>';
                  }

                  ?>

                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="<?php echo site_url('Front/in/' . $uri); ?>"><i class="ico-flag id-flag"></i> Bahasa Indonesia</a>
                  <a class="dropdown-item" href="<?php echo site_url('Front/en/' . $uri); ?>"><i class="ico-flag en-flag"></i> Inggris</a>
                  <a class="dropdown-item" href="<?php echo site_url('Front/cn/' . $uri); ?>"><i class="ico-flag cn-flag"></i> Mandarin</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav scoller" id="mainNav">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-3">
            <a href="#bgHero" class="js-scroll-trigger">
              <i class="logo"></i>
            </a>
          </div>
          <div class="col-9">
            <div class="nav-main">
              <ul class="nav justify-content-end">

                <?php
                if ($this->session->userdata('bahasa') == 'in') { ?>
                  <li class="nav-item">
                    <a class="nav-link active js-scroll-trigger" href="#bgHero">Beranda</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#feature">Fitur</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#testimoni">Testimoni</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
                  </li>


                <?php } elseif ($this->session->userdata('bahasa') == 'en') { ?>
                  <li class="nav-item">
                    <a class="nav-link active js-scroll-trigger" href="#bgHero">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">About</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#feature">Features</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#testimoni">Testimonials</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
                  </li>
                <?php } else { ?>
                  <li class="nav-item">
                    <a class="nav-link active js-scroll-trigger" href="#bgHero">首页</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">关于</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#feature">特征</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#testimoni">感言</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#blog">博客</a>
                  </li>
                <?php } ?>




              </ul>
            </div>
            <div class="text-right">
              <a href="#" class="btn-nav-mobile"><i class="ico-menu"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section class="bg-hero" id="bgHero">
    <div class="hero-inner">
      <?php
      if ($this->session->userdata('bahasa') == 'in') { ?>
        <h1 class="text-center"> Sekarang Fengshui <br>
          <span>Ada di Dalam Genggamanmu</span>
        </h1>
        <p class="mt-3 mb-5">Download Sekarang di</p>
      <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
        <h1 class="text-center">Fengshui <br>
          <span>Is in Your Hands Now</span>
        </h1>
        <p class="mt-3 mb-5">Download Now at</p>
      <?php  } else { ?>
        <h1 class="text-center"> 现在风水 <br>
          <span>在你的掌握中</span>
        </h1>
        <p class="mt-3 mb-5">立即下载</p>
      <?php } ?>

      <div class="download-btn">
        <?php
        foreach ($data_link_2->result() as $row) { ?>
          <a href="<?php echo $row->deskripsi; ?>" class="btn btn-warning btn-lg mx-2">
            <img src="<?php echo base_url('uploads/link/' . $row->foto); ?>" style="width: 30px;height: 30px" /> <?php echo $row->judul; ?>
          </a>
        <?php } ?>


        <!--  <a href="#" class="btn btn-warning btn-lg mx-2">
          <span class="icon-path-12916"></span> Playstore
        </a>
        <a href="#" class="btn btn-warning btn-lg mx-2">
          <span class="icon-group-285 "></span> Appstore
        </a> -->
      </div>
      <div class="phone-app">
        <img src="<?php echo base_url() . 'assets/fengshui/landing/images/phone-app.png' ?>" class="img-fluid" alt="">
      </div>
    </div>
  </section>
  <section class="about" id="about">
    <div class="row align-items-end m-0">
      <div class="col-lg-6 pl-0 mb-5">
        <div class="liner"></div>
        <div class="inner-left">


          <?php
          if ($this->session->userdata('bahasa') == 'in') { ?>
            <h2 class="mb-5">
              Tentang <span class="text-primary">Fengshui App</span>
            </h2>
            <p>
              <?php echo $tentang_fengshui->deskripsi; ?>
            </p>
          <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
            <h2 class="mb-5">
              About <span class="text-primary">Fengshui App</span>
            </h2>
            <p>
              <?php echo $tentang_fengshui->deskripsi_en; ?>
            </p>
          <?php  } else { ?>
            <h2 class="mb-5">
              关于 <span class="text-primary">风水应用</span>
            </h2>
            <p>
              <?php echo $tentang_fengshui->deskripsi_man; ?>
            </p>
          <?php } ?>

        </div>
      </div>
      <div class="col-lg-6">
        <div class="inner-right img-pas">
          <img src="<?php echo base_url() . 'assets/fengshui/landing/images/about.png' ?>" class="img-fluid" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="feature" id="feature">
    <div class="row align-items-center m-0">
      <div class="col-lg-6 order-lg-1 order-2 pl-0 ptt-5">
        <div class="inner-left">
          <div class="row ">


            <?php
            if ($this->session->userdata('bahasa') == 'in') { ?>




              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-taoism-yin-yang"></span>
                  </div>
                  <p>Kategori</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-decorative-chinese-flower"></span>
                  </div>
                  <p>Riwayat</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-chinese-temple"></span>
                  </div>
                  <p>Pembayaran</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-carps"></span>
                  </div>
                  <p>Ganti Bahasa</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-woman-hairstyle"></span>
                  </div>
                  <p>Jawaban</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-teapot"></span>
                  </div>
                  <p>Mudah Digunakan</p>
                </div>
              </div>

            <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>




              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-taoism-yin-yang"></span>
                  </div>
                  <p>Category</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-decorative-chinese-flower"></span>
                  </div>
                  <p>History</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-chinese-temple"></span>
                  </div>
                  <p>Payment</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-carps"></span>
                  </div>
                  <p>Change Language</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-woman-hairstyle"></span>
                  </div>
                  <p>Answer</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-teapot"></span>
                  </div>
                  <p>Easy Use</p>
                </div>
              </div>


            <?php  } else { ?>



              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-taoism-yin-yang"></span>
                  </div>
                  <p>类别</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-decorative-chinese-flower"></span>
                  </div>
                  <p>历史</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt ml-auto">
                  <div class="ico-feature">
                    <span class="icon-chinese-temple"></span>
                  </div>
                  <p>支付</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-carps"></span>
                  </div>
                  <p>改变语言</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-woman-hairstyle"></span>
                  </div>
                  <p>回答</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-6 mb-4">
                <div class="feature-cnt">
                  <div class="ico-feature">
                    <span class="icon-chinese-teapot"></span>
                  </div>
                  <p>易于使用</p>
                </div>
              </div>


            <?php } ?>


          </div>
        </div>
      </div>
      <div class="col-lg-6 order-lg-2 order-1 pr-0 pl-5">
        <div class="liner-right"></div>
        <div class="inner-right">

          <?php
          if ($this->session->userdata('bahasa') == 'in') { ?>
            <h2 class="mb-5">
              Fitur Yang Ada di <span class="text-primary">Fengshui App</span>
            </h2>

            <p>
              <?php echo $fitur_fengshui->deskripsi; ?>
            </p>
          <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
            <h2 class="mb-5">
              Features in <span class="text-primary">Fengshui App </span>
            </h2>

            <p>
              <?php echo $fitur_fengshui->deskripsi_en; ?>
            </p>
          <?php   } else { ?>
            <h2 class="mb-5">
              风水应用 <span class="text-primary">里的功能 </span>
            </h2>

            <p>
              <?php echo $fitur_fengshui->deskripsi_man; ?>
            </p>
          <?php   } ?>



        </div>
      </div>
    </div>
  </section>
  <section class="testimoni" id="testimoni">
    <div class="container">
      <div class="liner-center"></div>
      <?php
      if ($this->session->userdata('bahasa') == 'in') { ?>
        <h2 class="mt-5 mb-4 text-center">
          Testimoni <span class="text-primary">Fengshui</span>
        </h2>
      <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
        <h2 class="mt-5 mb-4 text-center">
          Testimonials <span class="text-primary">Feng Shui</span>
        </h2>
      <?php   } else { ?>
        <h2 class="mt-5 mb-4 text-center">
          感言 <span class="text-primary">风水 </span>
        </h2>
      <?php   } ?>



      <div class="testimoni-slide">
        <?php
        foreach ($testimoni->result() as $row) { 

          $getmember = $this->general_model->datagrab(array('tabel'=>'member','where'=>array('member_id'=>$row->member_id)))->row();
          ?>
          <div>
            <div class="testimoni-item">
              <div class="testi-content">
                <?php echo $row->isi; ?>
              </div>
              <div class="testi-pp">
                <?php 
                  if($getmember->img_file != NULL){ ?>
                    <img src="<?php echo base_url('/uploads/member/' . $getmember->img_file); ?>" alt="">
                <?php 
                  }else{ ?>
                    <img src="<?php echo base_url('/assets/images/blank-profile.png'); ?>" alt="">
                <?php  }
                ?>
                
              </div>
              <p class="testi-username"><?php echo $row->nama; ?></p>
            </div>
          </div>
        <?php
        }
        ?>
        <!--  <div>
          <div class="testimoni-item">
            <div class="testi-content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
              labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
            </div>
            <div class="testi-pp">
              <img src="http://i.pravatar.cc/160" alt="">
            </div>
            <p class="testi-username">Sandra Emily</p>
            <p class="testi-subs">User Happy</p>
          </div>
        </div>
        <div>
          <div class="testimoni-item">
            <div class="testi-content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
              labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
            </div>
            <div class="testi-pp">
              <img src="http://i.pravatar.cc/163" alt="">
            </div>
            <p class="testi-username">Sandra Emily</p>
            <p class="testi-subs">User Happy</p>
          </div>
        </div>
        <div>
          <div class="testimoni-item">
            <div class="testi-content">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
              labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
            </div>
            <div class="testi-pp">
              <img src="http://i.pravatar.cc/161" alt="">
            </div>
            <p class="testi-username">Sandra Emily</p>
            <p class="testi-subs">User Happy</p>
          </div>
        </div> -->
      </div>
    </div>
  </section>
  <section class="blog" id="blog">
    <div class="container">
      <div class="liner-center invert"></div>
      <?php
      if ($this->session->userdata('bahasa') == 'in') { ?>
        <h2 class="text-center mt-4 mb-5">Blog Kami</h2>
      <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
        <h2 class="text-center mt-4 mb-5">Our Blog</h2>
      <?php   } else { ?>
        <h2 class="text-center mt-4 mb-5">我们的博客</h2>
      <?php   } ?>


      <div class="row">

        <?php
        foreach ($blog->result() as $row) { ?>
          <div class="col-lg-3 col-md-6 mb-4">
            <a href="<?php echo base_url() . 'detail/blog/' . $row->permalink; ?>">
              <div class="card">
                <div class="blog-img">
                  <img src="<?php echo base_url() . 'uploads/blog/' . $row->foto; ?>" class="img-fluid" alt="">
                </div>
                <div class="card-body">
                  <?php 
                      $data_s = strip_tags($row->deskripsi);
                      $data_ss = strip_tags($row->deskripsi_en);
                      $data_sss = strip_tags($row->deskripsi_man);
                      $ss = substr($data_s,0,150);
                      $sss = substr($data_ss,0,150);
                      $ssss = substr($data_sss,0,150);
                      
                  
                      if ($this->session->userdata('bahasa') == 'in') { ?>   
                      <h5 class="card-title"><?php echo $row->judul;?></h5>
                      <p class="card-text"><?php echo $ss;?> ...</p>
                      <?php  }elseif ($this->session->userdata('bahasa') == 'en') { ?>  
                      <h5 class="card-title"><?php echo $row->judul_en;?></h5>
                      <p class="card-text"><?php echo $sss;?> ...</p>
                      <?php   }else { ?>  
                      <h5 class="card-title"><?php echo $row->judul_man;?></h5>
                      <p class="card-text"><?php echo $ssss;?> ...</p>
                      <?php   } ?>


                </div>
              </div>
            </a>
          </div>
        <?php
        }
        ?>

        <!-- 
        <div class="col-lg-3 col-md-6 mb-4">
          <a href="#">
            <div class="card">
              <img src="<?php echo base_url() . 'assets/fengshui/landing/images/image-blog.jpg' ?>" class="img-fluid" alt="">
              <div class="card-body">
                <h5 class="card-title">Warna Keberuntungan Menurut Fengshui Rumah</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                  tempor invidunt ut</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-6 mb-4">
          <a href="#">
            <div class="card">
              <img src="<?php echo base_url() . 'assets/fengshui/landing/images/image-blog.jpg' ?>" class="img-fluid" alt="">
              <div class="card-body">
                <h5 class="card-title">Warna Keberuntungan Menurut Fengshui Rumah</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                  tempor invidunt ut</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-6 mb-4">
          <a href="#">
            <div class="card">
              <img src="<?php echo base_url() . 'assets/fengshui/landing/images/image-blog.jpg' ?>" class="img-fluid" alt="">
              <div class="card-body">
                <h5 class="card-title">Warna Keberuntungan Menurut Fengshui Rumah</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                  tempor invidunt ut</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-6 mb-4">
          <a href="#">
            <div class="card">
              <img src="<?php echo base_url() . 'assets/fengshui/landing/images/image-blog.jpg' ?>" class="img-fluid" alt="">
              <div class="card-body">
                <h5 class="card-title">Warna Keberuntungan Menurut Fengshui Rumah</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                  tempor invidunt ut</p>
              </div>
            </div>
          </a>
        </div> -->
      </div>
      <div class="text-center mt-5">

        <?php
        if ($this->session->userdata('bahasa') == 'in') { ?>
          <a href="<?php echo base_url() . 'blog/all' ?>" class="btn btn-primary btn-lg">Lihat Lainya</a>
        <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
          <a href="<?php echo base_url() . 'blog/all' ?>" class="btn btn-primary btn-lg">See others</a>
        <?php   } else { ?>
          <a href="<?php echo base_url() . 'blog/all' ?>" class="btn btn-primary btn-lg">见其他人</a>
        <?php   } ?>
      </div>
    </div>
  </section>
  <footer>
    <div class="footer-top">
      <div class="container">

        <?php
        if ($this->session->userdata('bahasa') == 'in') { ?>
          <h2>Download Sekarang <br> Aplikasinya di</h2>
        <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
          <h2>Download Now <br> Application in</h2>
        <?php   } else { ?>
          <h2>现在下载 <br> 应用在 </h2>
        <?php   } ?>

        <div class="download-btn mt-5">

          <?php
          foreach ($data_link_2->result() as $row) { ?>
            <a href="<?php echo $row->deskripsi; ?>" class="btn btn-primary btn-lg mx-2">
              <img src="<?php echo base_url('uploads/link/' . $row->foto); ?>" style="width: 30px;height: 30px" /> <span style="padding-top: 30px;"><?php echo $row->judul; ?></span>
            </a>
          <?php } ?>

          <!-- 
                <a href="#" class="btn btn-primary btn-lg mx-2">
                    <span class="icon-path-12916"></span> Playstore
                </a>
                <a href="#" class="btn btn-primary btn-lg mx-2">
                    <span class="icon-group-285 "></span> Appstore
                </a> -->
        </div>
        <div class="mck-foo">
          <img src="<?php echo base_url() . 'assets/fengshui/landing/images/app-footer.png' ?>" class="img-fluid" alt="">
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">

        <?php
        if ($this->session->userdata('bahasa') == 'in') { ?>
          2020 &copy; Aplikasi Fengshui. Seluruh hak cipta. | <a href="<?php echo base_url() . 'detail/syarat_ketentuan/'; ?>" class="text-white">Syarat & Kententuan</a>
        <?php  } elseif ($this->session->userdata('bahasa') == 'en') { ?>
          2020 &copy; Fengshui App. All rights reserved. | <a href="<?php echo base_url() . 'detail/syarat_ketentuan/'; ?>" class="text-white">Terms & Conditions</a>
        <?php   } else { ?>
          二千〇二十 &copy; 风水应用. 版权所有. | <a href="<?php echo base_url() . 'detail/syarat_ketentuan/'; ?>" class="text-white">条款及细则</a>
        <?php   } ?>



      </div>
    </div>
  </footer>
  <div class="menu-overlay"></div>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/jquery.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/popper.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/bootstrap.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/jquery.easing.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/slick.min.js' ?>"></script>

  <script type="text/javascript" src="<?php echo base_url() . 'assets/fengshui/landing/js/all.js' ?>"></script>
  <script>
    $('.testimoni-slide').slick({
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  </script>

</body>

</html>