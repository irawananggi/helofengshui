
  <div class="main-content-inner">
    <div class="subheader-outer">
      <div class="row align-items-center">
        <div class="col-md-12 mb-4">
          <div class="sub-header">
            <div class="col-search">
              <?php echo @$extra_tombol?>
            </div>
            <div class="ml-auto">
                  <a href="<?php echo base_url($this->dir.'/pdf/'.$search);?>" target="_blank" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>
                  <!-- <?php echo @$tombol?>
 -->            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main-content-inner-child">
      <?php

        if ($data_article->num_rows() > 0) { ?>
            <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Judul</td>
                <td scope="col" class="text-2 medium">Link</td>
                <td scope="col" class="text-2 medium">Foto</td>
                <td scope="col" class="text-2 medium">Jenis</td>
                <td scope="col" class="text-2 medium text-center">Action</td>
              </tr>
            </thead>
            <tbody>
              
                <?php
                $no = 1 + $offs;
                foreach ($data_article->result() as $row) { ?>
                <tr>
                  <td class="align-middle" style="width: 60px;"><?php echo $no;?></td>
                  <td class="align-middle">
                    <?php echo $row->judul;?>
                  </td>
                  <td class="align-middle">
                    <a href="<?php echo $row->deskripsi;?>"><?php echo $row->deskripsi;?></a>
                  </td>
                  <td class="align-middle">

                    <div>
                      <div class="user-table">
                        <div class="user-ava">
                          <img src="<?php echo base_url('uploads/link/'.$row->foto) ;?>" alt="">
                        </div>
                      </div>
                    </div>
                    
                    
                  </td>
                   <td class="align-middle">
                    <?php echo @$row->nama_jm;?>
                  </td>
                  <td class="align-middle text-center">
                    <div class="custom-control custom-switch">
                      <?php echo anchor(site_url($this->dir.'/add_data/'.$row->id_link),'<i class="icon-fi-sr-pencil"></i>','class="btn btn-action btn-sm" act="#" title="Klik untuk edit data"');?>
                 
                  
                     <!--  <?php echo anchor(site_url($this->dir.'/delete_data/'.$row->id_link),'<i class="icon-fi-sr-trash"></i>','class="btn btn-action btn-sm btn-reset" act="#" title="Klik untuk tambah data"');?>
                  -->
                    </div>
                    
                  </td>
                </tr>
               
                <?php 
                $no++;
              }
               ?>
            </tbody>
          </table>
      <?php  }else{ 
          echo '<div class="alert">Data masih kosong ...</div>';
        }
      ?>
    </div>
  </div>
</div>
 

