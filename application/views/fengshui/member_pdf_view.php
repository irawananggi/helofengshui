<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title> PDF <?php echo @$title;?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        table{
            border-spacing:0;border-collapse:collapse
            }
            td,th{
                padding:5px
            }
            th{
                text-align: center;
            }
        .table{
            border-collapse:collapse!important
            }
            .table td,.table th{
                background-color:#fff!important
                }
                .table-bordered td,.table-bordered th{
                    border:1px solid #ddd!important
                }

    </style>
</head>
<body>
<h1 class="text-center bg-info"><?php echo @$title;?></h1>
   
         <?php

        if ($data->num_rows() > 0) { ?>
            <table id="table" class="table table-main nowrap table-bordered" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Nama Lengkap</td>
                <td scope="col" class="text-2 medium">Tanggal bergabung </td>
                <td scope="col" class="text-2 medium text-center">Non Aktifkan</td>
              </tr>
            </thead>
            <tbody>
              
                <?php
                $no = 1 + $offs;
                foreach ($data->result() as $row) { ?>
                <tr>
                  <td class="align-middle" style="width: 20px;"><?php echo $no;?></td>
                  <td class="align-middle">
                    <div>
                      <div class="user-table">
                        <div class="user-text">
                          <p class="mb-0"><?php echo $row->nama_lengkap;?></p>
                          <p class="mb-0 text-12 text-grey"><?php echo $row->email;?></p>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td class="align-middle">
                    <?php echo tanggal_indo(date('Y-m-d', strtotime($row->created_at)));?>
                  </td>
                  <td class="align-middle text-center" style="text-align: center">
                    <?php 
                    if($row->is_active == 1){
                      $checked = "";
                    }else{
                      $checked = "checked";
                    }
                    ?>
                    <div class="custom-control custom-switch" align="center">
                      <input type="checkbox" class="custom-control-input" onclick="location.href='<?php echo base_url($this->dir.'/edit_status/'.$row->member_id);?>';" id="customSwitch<?php echo $no;?>" <?php echo $checked;?>>
                      <label class="custom-control-label" for="customSwitch<?php echo $no;?>"></label>
                    </div>
                  </td>
                </tr>
               
                <?php 
                $no++;
              }
               ?>
            </tbody>
          </table>
      <?php  }else{ 
          echo '<div class="alert">Data masih kosong ...</div>';
        }
      ?>
</body>
</html>