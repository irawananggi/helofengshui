<!-- 
<div class="main-content">
      <div class="header-menu">
        <a href="#" class="menu-btn">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <g id="fi-sr-menu-burger" transform="translate(0 -4)">
              <rect id="Rectangle_8858" data-name="Rectangle 8858" width="24" height="2" rx="1"
                transform="translate(0 11)" fill="#fff" />
              <rect id="Rectangle_8859" data-name="Rectangle 8859" width="24" height="2" rx="1"
                transform="translate(0 4)" fill="#fff" />
              <rect id="Rectangle_8860" data-name="Rectangle 8860" width="24" height="2" rx="1"
                transform="translate(0 18)" fill="#fff" />
            </g>
          </svg>
        </a>
        <h2 class="title mb-0">Kategori</h2>
        <ul class="nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-bell"></span>
              <span class="notif notif-count">3</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-comment"></span>
              <span class="notif message-count">3</span>
            </a>
          </li>
        </ul>
      </div> -->
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                 <input type="hidden" name="id" value="" class="form-control main-form-control" placeholder="Nama kategori ">
                <div class="row">
                      <input type="hidden" name="id_k" value="<?php echo $dt->id;?>">
                  <div class="col-lg-4 mb-4">
                    <label>Indonesia</label>
                    <div class="form-group">
                      <input type="text" name="name_in" value="<?php echo $dt->name_in;?>" class="form-control main-form-control" placeholder="Nama kategori " required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi_in" value="<?php echo $dt->deskripsi_in;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="Deskripsi singkat kategori" required><?php echo $dt->deskripsi_in;?></textarea>
                    </div>
                    <div class="form-group">
                      <textarea name="ketentuan_in" value="<?php echo $dt->ketentuan_in;?>" class="form-control main-form-control" cols="30" rows="4"
                        placeholder="Ketentuan kategori" required><?php echo $dt->ketentuan_in;?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                    <label>Inggris</label>
                    <div class="form-group">
                      <input name="name_en" type="text" value="<?php echo $dt->name_en;?>" class="form-control main-form-control" placeholder="Name Category " required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi_en" value="<?php echo $dt->deskripsi_en;?>" class="form-control main-form-control" cols="30" rows="4" placeholder="Brief description of the category" required><?php echo $dt->deskripsi_en;?></textarea>
                    </div>
                    <div class="form-group">
                      <textarea name="ketentuan_en" value="<?php echo $dt->ketentuan_en;?>" class="form-control main-form-control" cols="30" rows="4" placeholder="Category provisions" required><?php echo $dt->ketentuan_en;?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                    <label>Mandarin</label>
                    <div class="form-group">
                      <input name="name_man" type="text" value="<?php echo $dt->name_man;?>" class="form-control main-form-control" placeholder="分类名称" required>
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi_man" value="<?php echo $dt->deskripsi_man;?>" class="form-control main-form-control" cols="30" rows="4" placeholder="类别的简要说明" required><?php echo $dt->deskripsi_man;?></textarea>
                    </div>
                    <div class="form-group">
                      <textarea name="ketentuan_man" value="<?php echo $dt->ketentuan_man;?>" class="form-control main-form-control" cols="30" rows="4" placeholder="类别规定" required><?php echo $dt->ketentuan_man;?></textarea>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-lg-4 mb-4">
                   
                    <label>ID Produk</label>
                     <span style="float: right;font-size: 11px;font-style:italic;color:#ccc;">id.app.fengshui.cariJodoh</span>
                    <div class="form-group">
                      <input type="text" name="id_produk" value="<?php echo $dt->id_produk;?>" class="form-control main-form-control" placeholder="id.app.fengshui.cariJodoh " required>
                    </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                    <label>Harga</label>
                    <div class="form-inline-group left-i">
                      <i class="in-left font-normal">Rp</i>
                      <input name="harga" type="text" value="<?php echo $dt->harga;?>" class="form-control main-form-control" placeholder="Masukan Harga" required>
                    </div>
                  </div>
                </div>

                <div class="row"><!-- 
                  <div class="col-lg-4 mb-4">
                    <label>Harga</label>
                    <div class="form-inline-group left-i">
                      <i class="in-left font-normal">Rp</i>
                      <input name="harga" type="text" value="<?php echo $dt->harga;?>" class="form-control main-form-control" placeholder="Masukan Harga" required>
                    </div>
                  </div> -->
                  <div class="col-lg-4 mb-4">
                    <label>Upload Foto </label>
                    <div class="single-file-upload no-label">
                      <input name="foto" type="file" id="upload_foto" hidden="">
                      <label for="upload_foto" class="btn btn-primary text-2 px-5 mb-0">
                        Pilih File
                      </label>
                      <div class="filename"><?php echo $dt->foto;?></div>
                    </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                    <label>Upload Icon </label>
                    <div class="single-file-upload no-label">
                      <input name="icon" type="file" id="upload_icon" hidden="">
                      <label for="upload_icon" class="btn btn-primary text-2 px-5 mb-0">
                        Pilih File
                      </label>
                      <div class="filename"><?php echo $dt->icon;?></div>
                    </div>
                  </div>
                </div>

                <div class="mt-4">
                  <div class="form-input">
                  
                     <?php

                      if($dt->id != NULL){ ?>
                         <?php

                            $no = 1;
                            $nox = 0;
                            foreach ($dt_form_kategori->result() as $rows) { 
                              $from = array(
                                'form_kategori a' => '',
                                'jenis_form b' => array('b.id = a.jenis_form_id','left')
                              );
                              $cb_jns_form = $this->general_model->combo_box(array('tabel'=>'jenis_form','key'=>'id','val'=>array('name_jf')));
                             $edit = anchor(site_url($this->dir.'/edit_data_frm/'.$rows->id),'<i class="icon-fi-sr-pencil"></i>','  data-toggle="modal" data-target="#exampleModal" data-id="'.$rows->id.'" class="btn btn-action btn-sm" act="#" title="Klik untuk tambah data"');
                            ?>

                                <div class="form-input-lists">
                                  <!-- <h5 class="text-primary mb-3">Form Inputan <?php echo $no;?></h5> -->
                                  <div class="row align-items-center">
                                    <div class="col-6">
                                      <h5 class="text-primary mb-3">Form Inputan <?php echo $no;?></h5>
                                    </div>
                                    <div class="col-6">
                                      <div class="text-right">
                                        <?php if($dt_form_kategori->num_rows() == 1){

                                        }else{ ?>
                                          <?php echo anchor(site_url($this->dir.'/hapus_element/'.$rows->kategori_id.'/'.$rows->id),'<i class="icon-fi-sr-trash"></i>','class="btn btn-action btn-sm" act="#" title="Klik untuk Hapus data"');?>
                                      <?php  }
                                        ?>

                                        <!-- <a href="#" class="btn btn-action btn-sm">
                                          <i class="icon-fi-sr-trash"></i>
                                        </a> -->
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">

                                  <input type="hidden" name="id_frm[<?php echo $rows->id;?>]" value="<?php echo $rows->id;?>">
                                    <div class="col-lg-4 mb-4">
                                      <label>Pilih Jenis Form <span class="text-primary text-10"></span></label>
                                    <?php echo form_dropdown('jenis_form_ids['.$rows->id.']', $cb_jns_form, @$rows->jenis_form_id,'class="form-control  searchless combo-box" style="width: 100%" required="required"');?>

            <!-- 
                                      <select name="jenis_form_id0" value="" class="form-control searchless">
                                        <option value="1">Text</option>
                                        <option value="2">Tanggal</option>
                                      </select> -->
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4 mb-4">
                                      <label>Indonesia</label>
                                      <div class="form-group">
                                        <input name="name_cat_ins[<?php echo $rows->id;?>]" value="<?php echo $rows->name_cat_in;?>" type="text" class="form-control main-form-control" placeholder="Nama kategori "  required="required">
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                      <label>Inggris</label>
                                      <div class="form-group">
                                        <input name="name_cat_ens[<?php echo $rows->id;?>]" value="<?php echo $rows->name_cat_en;?>" type="text" class="form-control main-form-control" placeholder="Name Category " required="required">
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                      <label>Mandarin</label>
                                      <div class="form-group">
                                        <input name="name_cat_mans[<?php echo $rows->id;?>]" value="<?php echo $rows->name_cat_man;?>" type="text" class="form-control main-form-control" placeholder="分类名称" required="required">
                                      </div>
                                    </div>
                                  </div>
                                </div>

                          <?php   $no ++;$nox ++;
                        } ?>
                     <?php  }else{ 
                       $cb_jns_form = $this->general_model->combo_box(array('tabel'=>'jenis_form','key'=>'id','val'=>array('name_jf')));
                             

                             ?>
                             <div class="form-input-list">
                                  <h5 class="text-primary mb-3">Form Inputan 1</h5>
                                  <div class="row">

                                  <input type="hidden" class="form-input-list" name="jmlfile" value="1">

                                    <div class="col-lg-4 mb-4">
                                      <label>Pilih Jenis Form <span class="text-primary text-10">* checkbox harus
                                          diisi</span></label>
                                    <?php echo form_dropdown('jenis_form_id0', $cb_jns_form, NULL,'class="form-control  searchless combo-box" style="width: 100%"  required');?>

            <!-- 
                                      <select name="jenis_form_id0" value="" class="form-control searchless">
                                        <option value="1">Text</option>
                                        <option value="2">Tanggal</option>
                                      </select> -->
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4 mb-4">
                                      <label>Indonesia</label>
                                      <div class="form-group">
                                        <input name="name_cat_in0" type="text" class="form-control main-form-control" placeholder="Nama kategori "  required>
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                      <label>Inggris</label>
                                      <div class="form-group">
                                        <input name="name_cat_en0" type="text" class="form-control main-form-control" placeholder="Name Category "  required>
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                      <label>Mandarin</label>
                                      <div class="form-group">
                                        <input name="name_cat_man0" type="text" class="form-control main-form-control" placeholder="分类名称"  required>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                     <?php  }

                     ?>
               
                  </div>
                  <div class="text-right">
                    <a href="#" class="btn btn-primary px-5" id="add-form">Tambah Inputan</a>
                  </div>
                </div>

                <div class="d-flex mt-4">
                  <?php 
                  if($dt->hub_cs == on){
                    $checked = "checked";
                  }else{
                    $checked = "";
                  }
                  if($dt->kategori_kematian == on){
                    $checked2 = "checked";
                  }else{
                    $checked2 = "";
                  }
                   ?>
                  <div class="custom-control custom-checkbox mr-5">
                    <input name="hub_cs" type="checkbox" class="custom-control-input" id="customCheck1" <?php echo $checked;?>>
                    <label class="custom-control-label" for="customCheck1">Tampilkan Tombol Hubungi CS</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input name="kategori_kematian" type="checkbox" class="custom-control-input" id="customCheck2"  <?php echo $checked2;?>>
                    <label class="custom-control-label" for="customCheck2">Kategori Kematian</label>
                  </div>
                </div>

                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5"><?php echo $tombol;?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/popper.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.dataTables.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.bootstrap4.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.rowReorder.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.responsive.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap-datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/all.js' ?>"></script>

<script>

    $(document).on('click', '#add-form', function(e){
    // $('#add-form').click(function (e) {
      var formlength2 = $('.form-input').children(".form-input-list").length + 1;
      var formlength = $('.form-input').children(".form-input-list").length;
      e.preventDefault();
      var tmpt = '<div class="form-input-list">'+
        '<div class="row align-items-center">' +
        '<div class="col-6">' +
        '<h5 class="text-primary mb-3">Form Inputan ' + formlength2 + '</h5>' +
        '</div>' +
        '<div class="col-6">' +
        '<div class="text-right">' +
        '<a href="#" class="btn btn-action btn-sm del-form">' +
        '<i class="icon-fi-sr-trash"></i>' +
        '</a>' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-4 mb-4">' +
        '<input type="hidden" class="form-input-list" name="jmlfile" value="' + formlength2 + '">' +
        '<label>Pilih Jenis Form <span class="text-primary text-10"></span></label>' +
        '<select name="jenis_form_id'+formlength+'" class="form-control searchless" required="required">' +
        '<option value="1">Text</option>' +
        '<option value="2">File</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Indonesia</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_in'+formlength+'" type="text" class="form-control main-form-control" placeholder="Nama kategori " required="required">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Inggris</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_en'+formlength+'" type="text" class="form-control main-form-control" placeholder="Name Category " required="required">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-4 mb-4">' +
        '<label>Mandarin</label>' +
        '<div class="form-group">' +
        '<input name="name_cat_man'+formlength+'" type="text" class="form-control main-form-control" placeholder="分类名称" required="required">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
      $('.form-input').append(tmpt);
      $('.searchless').select2({
        theme: 'bootstrap4 main-form-control',
        placeholder: "Pilih",
        minimumResultsForSearch: -1
      });
    })
     $(document).on('click', '.del-form', function(e){
      e.preventDefault();
      $(this).closest('.form-input-list').remove();
    })
  </script>

