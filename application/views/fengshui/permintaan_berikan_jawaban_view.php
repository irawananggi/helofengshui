
      
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo site_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18">Permintaan - Berikan Jawaban</span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              
               <form action="<?php echo base_url($this->dir.'/save_permintaan_berikan_jawaban/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                <input type="hidden" name="id" value="<?php echo $id;?>" class="form-control main-form-control" placeholder="Nama kategori ">

                <div class="col-lg-12 mb-12">
                  <label>Indonesia</label>
                  <textarea name="editor_in" id="editor_in" rows="10" cols="80" value="<?php echo $data_jawaban->jawaban_in;?>"><?php echo $data_jawaban->jawaban_in;?></textarea>
                </div>
                <br>
                <div class="col-lg-12 mb-12">
                  <label>Inggris</label>
                  <textarea name="editor_en" id="editor_en" rows="10" cols="80" value="<?php echo $data_jawaban->jawaban_en;?>"><?php echo $data_jawaban->jawaban_en;?></textarea>
                </div>
                <br>
                <div class="col-lg-12 mb-12">
                  <label>Mandarin</label>
                  <textarea name="editor_man" id="editor_man" rows="10" cols="80" value="<?php echo $data_jawaban->jawaban_man;?>"><?php echo $data_jawaban->jawaban_man;?></textarea>
                </div>
                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script>
    CKEDITOR.replace('editor_in');
    CKEDITOR.replace('editor_en');
    CKEDITOR.replace('editor_man');
</script>

