
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo base_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18"><?php echo $head;?></span>
            </div>
          </div>
        </div>
<div class="main-content-inner-child">
           <form action="<?php echo base_url($this->dir.'/simpan_data/');?>" id="frmTarget" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_postings">
                      <input type="hidden" name="id" value="<?php echo $dt->id;?>" class="form-control" placeholder="" >
          <div class="card mb-5">
            <div class="card-body">
              <h4 class="text-primary mb-4">Tambah</h4>

                <div class="row row-form">
                  <div class="col-md-6 pr-5">
                    <div class="form-group">
                      <label>Name Voucher</label>
                      <input type="text" name="name_voucher" value="<?php echo $dt->name_voucher;?>" class="form-control" placeholder="" >
                    </div>
                    <div class="form-group">
                      <label>Minimal Belanja</label>
                      <input type="text" name="min_s" value="<?php echo $dt->min_s;?>" class="form-control" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                    </div>

                  </div>
                  <div class="col-md-6 pl-5">

                    
                    <div class="row align-items-center">
                      <div class="col-lg-4 col-md-5 col-5 mb-4">
                        <label class="mb-0">Diskon</label>
                      </div>
                       
                      <div class="col-lg-8 col-md-5 col-5 mb-4">
                        <div class="d-flex">
                          <div class="custom-control custom-radio custom-radio-box">
                            <input type="radio" id="terbitkan" name="dis_id" class="custom-control-input" <?php if (isset($dt->dis_id) && $dt->dis_id=="1") echo "checked";?> value="1" required>
                            <label class="custom-control-label" for="terbitkan">Persen</label>
                          </div>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <div class="custom-control custom-radio custom-radio-box">
                            <input type="radio" id="draf" name="dis_id" class="custom-control-input" <?php if (isset($dt->dis_id) && $dt->dis_id=="2") echo "checked";?> value="2"  required>
                            <label class="custom-control-label" for="draf">Rupiah</label>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="form-group">
                      <label>Nominal Voucher</label>
                      <input type="text" name="nominal" value="<?php echo $dt->nominal;?>" class="form-control" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                    </div>
                    <div class="form-group">
                      <label>Pilih tanggal</label>
                      <div class="d-flex align-items-center">
                        <div class="form-inline-group right-i">
                          <i class="icon-icon-material-date-range right"></i>

                    <input type="text" name="created_at1" value="<?php echo $dt->start_date;?>" class="form-control date" placeholder="dd/mm/yyyy">
                        </div>
                        <div class="px-3">Sampai</div>
                        <div class="form-inline-group right-i">
                          <i class="icon-icon-material-date-range right"></i>
                    <input type="text" name="created_at2" value="<?php echo $dt->end_date;?>" class="form-control date" placeholder="dd/mm/yyyy">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 pt-3">
                    <button type="submit" class="btn btn-primary mr-3">Simpan
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          </form>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url().'assets/bp/admin/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/bp/admin/js/bootstrap-datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/bp/admin/js/timepicker.min.js' ?>"></script>

           <script type="text/javascript" src="./js/all.js"></script>
  <script>
    $('.date').datepicker({
      format: "yyyy-mm-dd",
    });
  </script>