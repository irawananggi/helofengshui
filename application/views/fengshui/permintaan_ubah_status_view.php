   <!--  <div class="main-content">
      <div class="header-menu">
        <a href="#" class="menu-btn">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <g id="fi-sr-menu-burger" transform="translate(0 -4)">
              <rect id="Rectangle_8858" data-name="Rectangle 8858" width="24" height="2" rx="1"
                transform="translate(0 11)" fill="#fff" />
              <rect id="Rectangle_8859" data-name="Rectangle 8859" width="24" height="2" rx="1"
                transform="translate(0 4)" fill="#fff" />
              <rect id="Rectangle_8860" data-name="Rectangle 8860" width="24" height="2" rx="1"
                transform="translate(0 18)" fill="#fff" />
            </g>
          </svg>
        </a>
        <h2 class="title mb-0">Permintaan</h2>
        <ul class="nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-bell"></span>
              <span class="notif notif-count">3</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-comment"></span>
              <span class="notif message-count">3</span>
            </a>
          </li>
        </ul>
      </div> -->
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo site_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18">Permintaan - Ubah Status Permintaan</span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
               <form action="<?php echo base_url($this->dir.'/save_ubah_status/');?>" id="form_add" enctype="multipart/form-data" method="POST" accept-charset="utf-8" name="form_kategori">
                 <input type="hidden" name="id" value="" class="form-control main-form-control" placeholder="Nama kategori ">
                <div class="row">
                  <div class="col-lg-5 mb-5">
                     <input type="hidden" name="id" value="<?php echo $id;?>" class="form-control main-form-control" placeholder="Nama kategori ">
                    <label>Ubah Status</label>

                    <?php 
                      $cb_status_permintaan = $this->general_model->combo_box(array('tabel'=>'status_permintaan','key'=>'id','val'=>array('name_sp_in')));
                      //$optionx = array('1'=>'Menunggu pembayaran','2'=>'Sedang diproses','3'=>'dibatalkan','4'=>'Selesai');
                      echo form_dropdown('kode_status',$cb_status_permintaan,@$kode_status,'class="form-control combo-box" style="width: 100%;"');
                    ?>
                   <!--  <select class="form-control searchless" name="kode_status" id="kode_status" style="width:100%" value="<?php echo $kode_status;?>">
                      <option value="1">Menunggu pembayaran </option>
                      <option value="2">Sedang diproses</option>
                      <option value="3">dibatalkan</option>
                      <option value="4">Selesai</option>
                    </select> -->
                  </div>
                </div>
                <div class="mt-5">
                  <button type="submit" class="btn btn-warning px-5">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>