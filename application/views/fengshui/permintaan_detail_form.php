
   <!--  <div class="main-content">
      <div class="header-menu">
        <a href="#" class="menu-btn">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <g id="fi-sr-menu-burger" transform="translate(0 -4)">
              <rect id="Rectangle_8858" data-name="Rectangle 8858" width="24" height="2" rx="1"
                transform="translate(0 11)" fill="#fff" />
              <rect id="Rectangle_8859" data-name="Rectangle 8859" width="24" height="2" rx="1"
                transform="translate(0 4)" fill="#fff" />
              <rect id="Rectangle_8860" data-name="Rectangle 8860" width="24" height="2" rx="1"
                transform="translate(0 18)" fill="#fff" />
            </g>
          </svg>
        </a>
        <h2 class="title mb-0">Permintaan</h2>
        <ul class="nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-bell"></span>
              <span class="notif notif-count">3</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-comment"></span>
              <span class="notif message-count">3</span>
            </a>
          </li>
        </ul>
      </div> -->
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo site_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18">Permintaan - Detail Form</span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <table id="tableDetail" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium" width="100">Nama Form</td>
                <td scope="col" class="text-2 medium">Isi</td>
              </tr>
            </thead>
            <tbody>
               <?php 
              $no=1;
              foreach ($permintaan_detail->result() as $row) { 
                $data_kat = $this->general_model->datagrab(array('tabel'=>'form_kategori','where'=>array('id'=>$row->form_kategori_id)))->row();
                ?>

              <tr>
                <td class="align-middle"><?php echo $data_kat->name_cat_in;?></td>
                <?php 
                if($data_kat->jenis_form_id == 2){ ?>
                 <td class="align-middle"><a href="<?php echo $row->isi;?>" target="_blank" ><img src="<?php echo $row->isi;?>" style="width: 50px"></a></td>
               <?php   }else{ ?>

                 <td class="align-middle"><?php echo $row->isi;?></td>
              <?php  } ?>

                
              </tr>
              <?php  $no++;
          } ?>
          
              
  
            </tbody>
          </table>
        </div>
      </div>
    </div>