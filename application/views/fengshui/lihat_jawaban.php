
      <div class="main-content-inner">
        <div class="subheader-outer no-border">
          <div class="sub-header mb-4">
            <div class="d-flex align-items-center">
              <a href="<?php echo site_url($this->dir);?>" class="mr-4 link-dark"><span class="icon-fi-sr-arrow-left"></span></a> <span
                class="text-18">Permintaan - Detail Jawaban </span>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <div class="card mb-5">
            <div class="card-body py-5 px-4">
              <div class="row">
                


                <div class="col-lg-12 mb-12">
                  <label>Indonesia</label>
                  <div class="form-group">
                    <textarea id="editor_in"  class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $data->jawaban_in; ?></textarea>
                  </div>
                </div>
                <div class="col-lg-12 mb-12">
                  <label>Inggris</label>
                  <div class="form-group">
                    <textarea id="editor_en"  class="form-control main-form-control" cols="30" rows="4"
                      disabled><?php  echo $data->jawaban_en; ?></textarea>
                  </div>
                </div>
                <div class="col-lg-12 mb-12">
                  <label>Mandarin</label>
                  <div class="form-group">
                    <textarea id="editor_man"  class="form-control main-form-control" cols="30" rows="4" disabled><?php  echo $data->jawaban_man; ?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script>
    CKEDITOR.replace('editor_in');
    CKEDITOR.replace('editor_en');
    CKEDITOR.replace('editor_man');
</script>