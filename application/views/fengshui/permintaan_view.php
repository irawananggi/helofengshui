
      <div class="main-content-inner">
        <div class="subheader-outer">
          <div class="row align-items-center">
            <div class="col-md-12 mb-4">
              <div class="sub-header">
                <div class="col-search">
                  
                  <?php echo @$extra_tombol?>
                  <!-- <form action="#">
                    <div class="form-inline-group left-i border-icon">
                      <i class="icon-search in-left"></i>
                      <input type="text" id="searchField" class="form-control" placeholder="Cari">
                    </div>
                  </form> -->
                </div>
               <!--  <div class="col-filter pl-5 d-flex align-items-center">
                  <span class="text-grey mr-2">Status Permintaan</span>
                  <select class="filter-s">
                    <option value="1">All</option>
                    <option value="2">Kategori</option>
                    <option value="3">Tanggal</option>
                    <option value="4">Status</option>
                  </select>
                </div> -->
                <div class="ml-auto">
                  <a href="<?php echo base_url($this->dir.'/pdf/'.$search);?>" target="_blank" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
           <?php
          if ($data_article->num_rows() > 0) { ?>
          <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Nama Customers</td>
                <td scope="col" class="text-2 medium">Kategori</td>
                <td scope="col" class="text-2 medium">Tanggal permintaan</td>
                <td scope="col" class="text-2 medium">Status permintaan</td>
                <td scope="col" class="text-2 medium">Detail Form</td>
                <td scope="col" class="text-2 medium text-center"></td>
              </tr>
            </thead>
            <tbody>
               <?php 
              $no=1;
              foreach ($data_article->result() as $row) { 
                $data_kat = $this->general_model->datagrab(array('tabel'=>'kategori','where'=>array('id'=>$row->kategori_id)))->row();
                $data_member = $this->general_model->datagrab(array('tabel'=>'member','where'=>array('member_id'=>$row->member_id)))->row();
                ?>
              <tr>
                <td class="align-middle" style="width: 60px;"><?php echo $no;?></td>
                <td class="align-middle" style="width: 60px;"><?php echo $data_member->nama_lengkap;?></td>
                <td class="align-middle">
                  <a href="<?php echo site_url($this->dir.'/permintaan_detail_form/'.$row->id);?>" class="text-warning underline">ID : <?php echo $data_kat->name_in;?></a>
                </td>
                <td class="align-middle">
                  <?php echo tanggal_indo(date('Y-m-d', strtotime($row->tgl_bayar))) ;?>
                </td>
                <td class="align-middle">

                   
                   <?php 
                    if ($row->status_permintaan == 'Menunggu pembayaran') { 
                     
                      $color = "color:#c23336 !important";
                      $status_danger = "status_danger";
                    }elseif($row->status_permintaan == 'dibatalkan') {
                      $color = "color: #2837ff !important";
                      $status_danger = "status_danger3";

                    }elseif($row->status_permintaan == 'expire') {
                      $color = "color: #2837ff !important";
                      $status_danger = "status_danger3";

                    }elseif($row->status_permintaan == 'cancel') {
                      $color = "color: #2837ff !important";
                      $status_danger = "status_danger3";

                    }elseif($row->status_permintaan == 'Cancel') {
                      $color = "color: #2837ff !important";
                      $status_danger = "status_danger3";

                    }else{
                      $color = "color: #dbbf85 !important";
                      $status_danger = "status_danger4";

                    }?>
                    <style type="text/css">
                    <?php 
                    if ($row->status_permintaan == 'Menunggu pembayaran') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #c23336 !important;
                        }
                     <?php }elseif($row->status_permintaan == 'dibatalkan') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #2837ff !important;
                        }
                     <?php }elseif($row->status_permintaan == 'expire') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #2837ff !important;
                        }
                     <?php }elseif($row->status_permintaan == 'cancel') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #2837ff !important;
                        }
                     <?php }elseif($row->status_permintaan == 'Cancel') { ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #2837ff !important;
                        }
                     <?php }else{ ?>
                        .status.<?php echo $status_danger;?>:before {
                            background: #dbbf85 !important;
                        }
                     <?php } ?>
                      </style>
                  <div class="status <?php echo $status_danger;?>" style="<?php echo $color;?>">

                   

                    <?php echo $row->status_permintaan;?>


                  </div>

                </td>
                <td class="align-middle">
                  <a href="<?php echo site_url($this->dir.'/permintaan_detail_view/'.$row->id);?>" class="btn btn-action-warning btn-radius text-14 px-4 py-2">Detail</a>
                </td>
                <td class="align-middle">
                   <?php if ($row->status_permintaan != 'dibatalkan'  AND  $row->status_permintaan != 'Dibatalkan' AND  $row->status_permintaan != 'Menunggu Jawaban' AND  $row->status_permintaan != 'Selesai' AND  $row->status_permintaan != 'expire' AND  $row->status_permintaan != 'cancel' AND  $row->status_permintaan != 'Cancel') { ?>
                      <a href="<?php echo site_url($this->dir.'/permintaan_ubah_status/'.$row->id);?>" class="btn btn-action-warning btn-sm">
                        Ubah Status
                      </a>
                  <?php }?>
                 <!--  <?php if ($row->status_permintaan != 'Menunggu pembayaran' AND $row->status_permintaan != 'dibatalkan'  AND $row->status_permintaan != 'Dibatalkan' AND $row->status_permintaan != 'Menunggu Jawaban') { ?>
                    
                      <a href="<?php echo site_url($this->dir.'/permintaan_berikan_jawaban/'.$row->id);?>" class="btn btn-action-danger btn-sm">
                        Berikan Jawaban
                      </a>
                  <?php }?> -->
                  <?php if ($row->status_permintaan == 'Menunggu Jawaban') { ?>
                    
                      <a href="<?php echo site_url($this->dir.'/permintaan_berikan_jawaban/'.$row->id);?>" class="btn btn-action-danger btn-sm">
                        Berikan Jawaban
                      </a>
                  <?php }?>
                 <?php if ($row->status_permintaan == 'Selesai') { ?>
                    
                      <a href="<?php echo site_url($this->dir.'/lihat_jawaban/'.$row->id);?>" class="btn btn-action-success btn-sm" style=" background: #ddc48e;">
                        Detail Jawaban
                      </a>
                  <?php }?>
 
                </td>
              </tr>
              <?php  $no++;
          } ?>
         
            </tbody>
          </table>
          <?php
                 }else{
            echo '<div class="alert">Data masih kosong ...</div>';
          } ?>
        </div>
      </div>
    </div>