<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <title>Fengshui - ADMIN</title>
  </head>

<body>

  <div class="main-container">
    <div class="sidebar">
    <a href="#" class="menu-close"> <span class="icon-icon-ionic-md-close text-16"></span></a>
    <div class="sidebar-logo">
        <i class="logo"></i>
    </div>
    <div class="sidebar-menu">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link " href="index.html">
            <div class="icon-menu"><i class="icon-fi-sr-apps"></i></div>
            <div>Dashboard</div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="kategori.html">
            <div class="icon-menu">
              <i class="icon-fi-sr-layout-fluid"></i>
            </div>
            <div>
              Kategori
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="member.html">
            <div class="icon-menu">
              <i class="icon-fi-sr-user"></i>
            </div> 
            <div>
              Member
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="permintaan.html">
          <div class="icon-menu">
            <i class="icon-fi-sr-notebook"></i>
          </div> 
          <div>
            Permintaan
          </div>
        </a>
        </li>
      </ul>
    </div>
    <div class="dropdown btn-auth">
      <button class="btn btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Mawar Cantik <span class="icon-fi-sr-angle-small-down arrow-menu"></span>
      </button>
      <div class="dropdown-menu dropdown-menu-right drop-arrow" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="#">Logout</a>
      </div>
    </div>
  </div>
    <div class="main-content">
      <div class="header-menu">
        <a href="#" class="menu-btn">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <g id="fi-sr-menu-burger" transform="translate(0 -4)">
              <rect id="Rectangle_8858" data-name="Rectangle 8858" width="24" height="2" rx="1"
                transform="translate(0 11)" fill="#fff" />
              <rect id="Rectangle_8859" data-name="Rectangle 8859" width="24" height="2" rx="1"
                transform="translate(0 4)" fill="#fff" />
              <rect id="Rectangle_8860" data-name="Rectangle 8860" width="24" height="2" rx="1"
                transform="translate(0 18)" fill="#fff" />
            </g>
          </svg>
        </a>
        <h2 class="title mb-0">Kategori</h2>
        <ul class="nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-bell"></span>
              <span class="notif notif-count">3</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-comment"></span>
              <span class="notif message-count">3</span>
            </a>
          </li>
        </ul>
      </div>
      <div class="main-content-inner">
        <div class="subheader-outer">
          <div class="row align-items-center">
            <div class="col-md-12 mb-4">
              <div class="sub-header">
                <div class="col-search">
                  <form action="#">
                    <div class="form-inline-group left-i border-icon">
                      <i class="icon-search in-left"></i>
                      <input type="text" id="searchField" class="form-control" placeholder="Cari">
                    </div>
                  </form>
                </div>
                <div class="ml-auto">
                  <a href="#" class="btn btn-white btn-ico radius-5 px-3 mr-3"><span
                      class="icon-icon-material-print"></span></a>
                  <a href="kategori-add.html" class="btn btn-warning radius-5 btn-sm"><i
                      class="icon-icon-awesome-plus text-12"></i>
                    Tambah</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <table id="table" class="table table-main nowrap" style="width:100%">
            <thead>
              <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Nama Kategori</td>
                <td scope="col" class="text-2 medium">Deskripsi </td>
                <td scope="col" class="text-2 medium">Ketentuan</td>
                <td scope="col" class="text-2 medium">Harga</td>
                <td scope="col" class="text-2 medium">Foto</td>
                <td scope="col" class="text-2 medium">Icon</td>
                <td scope="col" class="text-2 medium text-center">Action</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="width: 60px;">1.</td>
                <td>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">ID :</span> Nama Bayi
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">EN :</span> Baby Name
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">MD :</span> 宝宝的名字
                  </div>
                </td>
                <td>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                </td>
                <td>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                </td>
                <td>Rp. 13.000</td>
                <td>
                  <img src="images/images-empty.png" width="80" alt="">
                </td>
                <td>
                  <img src="images/icon-sample.png" width="50" alt="">
                </td>
                <td class="text-center">
                  <a href="kategori-edit.html" class="btn btn-action btn-sm">
                    <i class="icon-fi-sr-pencil"></i>
                  </a>
                  <a href="#" class="btn btn-action btn-sm">
                    <i class="icon-fi-sr-trash"></i>
                  </a>
                </td>
              </tr>
              <tr>
                <td style="width: 60px;">2.</td>
                <td>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">ID :</span> Nama Bayi
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">EN :</span> Baby Name
                  </div>
                  <div class="content-table-title">
                    <span class="text-grey id-lang">MD :</span> 宝宝的名字
                  </div>
                </td>
                <td>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                </td>
                <td>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                  <div class="content-table">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna asadda
                  </div>
                </td>
                <td>Rp. 14.000</td>
                <td>
                  <img src="images/images-empty.png" width="80" alt="">
                </td>
                <td>
                  <img src="images/icon-sample.png" width="50" alt="">
                </td>
                <td class="text-center">
                  <a href="kategori-edit.html" class="btn btn-action btn-sm">
                    <i class="icon-fi-sr-pencil"></i>
                  </a>
                  <a href="#" class="btn btn-action btn-sm">
                    <i class="icon-fi-sr-trash"></i>
                  </a>
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/ckeditor.js"></script>
<script type="text/javascript" src="./js/all.js"></script>
  <script>
    $('.datepicker').datepicker({
      format: "dd MM yyyy",
    });
  </script>
</body>

</html>