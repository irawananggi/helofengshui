<?php 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=newsletter.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<style type="text/css">
  tr {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
  thead {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
</style>
        <div class="subheader-outer">
          <div class="row align-items-center">
            <div class="col-md-12">
              <div class="sub-header">
                <div class="title-col">
                  <h2 class="title mb-0 text-primary font-weight-400"><?php echo $total;?><span class="subtitle">Newslatter> <?php echo $name;?></span> </h2>
                </div>
               
              </div>
            </div>
          </div>
        </div>
        <div class="main-content-inner-child">
          <?php

        if ($data_article->num_rows() > 0) { ?>
             <table id="table" class="table table-main nowrap" style="width:100%" border="1">
            <thead>
               <tr>
                <td scope="col" class="text-2 medium">No</td>
                <td scope="col" class="text-2 medium">Email</td>
              </tr>
            </thead>
            <tbody>
              
                <?php
                $no = 1 + $offs;
                foreach ($data_article->result() as $row) { ?>
                <tr align="center">
                <td style="width: 60px;"><?php echo $no;?></td>
                <td><?php echo $row->name_email;?></td>
              
              </tr>
               
                <?php 
                $no++;
              }
               ?>
            </tbody>
          </table>
      <?php  }else{ 
          echo '<div class="alert">Data masih kosong ...</div>';
        }
      ?>

        </div>

