<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url().'assets/favicon/favicon.ico' ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/fengshui/admin/css/styles.css' ?>">
    <title>Fengshui - ADMIN</title>
     <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/plugins/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <style>
        #box_message { margin-top: 20px; font-size: 130% }
        .exlcm { margin-right: 10px; }
        
    </style>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
    

  <?php
    $this_css='assets/css/opd.css';if(file_exists($this_css))echo reqcss($this_css)."\n";
  ?>
  </head>
 <?php 
  $ok = $this->session->flashdata('ok');
  $fail = $this->session->flashdata('fail');
  ?>
<body style="background: white">
    <div class="auth-container">
        <div class="text-center">
            <img src="images/logo.svg" width="90" class="img-fluid" alt="">
        </div>
                    <div class="logo-brand fit-logo"><img src="<?php echo base_url().'assets/images/icon-nottoken.png'; ?>" /></div>
                    
        <h4 class="my-4 text-black" style="color:black">Halaman Tidak di Temukan, Silahkan kembali ke halaman Reset Password Aplikasi</h4>
        
    </div>
</body>

</html>