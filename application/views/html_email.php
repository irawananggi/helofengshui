<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">

    <title><?= $judul; ?></title>
    <style>
        /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */

        /*All the styling goes here*/

        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%;
        }

        body {
            background-color: #f6f6f6;
            font-family: 'Poppins', sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            color: #666666;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%;
        }

        table td {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            vertical-align: top;
        }

        .table-footer td {
            width: 30%;
            padding: 30px;
        }

        .table-footer td a {
            color: #999999;
            text-decoration: none;
        }

        /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%;
            padding-bottom: 20px;
        }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            margin: 0 auto !important;
            /* makes it centered */
            max-width: 777px;
            width: 777px;
            padding-top: 40px;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            margin: 0 auto;
            max-width: 777px;
        }

        /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%;
        }

        .wrapper {
            box-sizing: border-box;
            padding: 20px 47px;
        }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            margin-top: 10px;
            text-align: center;
            width: 100%;
        }

        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center;
        }

        /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: 'Poppins', sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            margin-bottom: 30px;
        }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize;
        }

        p,
        ul,
        ol {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            margin-bottom: 15px;
        }

        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 0px;
        }

        a {
            color: #3498db;
            text-decoration: underline;
        }

        /* -------------------------------------
          BUTTONS
      ------------------------------------- */
        .btn-primary {
            background-color: #222222;
            box-sizing: border-box;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            margin: 0;
            padding: 8px 25px;
            text-decoration: none;
            text-transform: capitalize;
        }




        /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        .mt0 {
            margin-top: 0;
        }

        .my3 {
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .mb0 {
            margin-bottom: 0;
        }

        .pt2 {
            padding-top: 20px;
        }

        .pt0 {
            padding-top: 0;
        }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0;
        }

        .powered-by a {
            text-decoration: none;
        }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            margin: 20px 0;
        }

        /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }


            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }

            #MessageViewBody a {
                color: inherit;
                text-decoration: none;
                font-size: inherit;
                font-family: inherit;
                font-weight: inherit;
                line-height: inherit;
            }

        }

        .header {
            background: url('https://i.ibb.co/HLvrGJm/logo-fengshui.png') no-repeat center center/contain;
            width: 70px;
            text-align: center;
            margin: 0px auto;
            height: auto;
            color: #fff;
        }

        .header table {
            width: 530px;
            margin-left: 082px;
            padding-top: 28px;
        }

        .header table td {
            font-size: 18px;
            text-align: center;
            padding-top: 33px;
        }

        .text-center {
            text-align: center;
        }

        .btn {
            text-decoration: none;
            background: #d42e2d;
            border-radius: 10px;
            padding: 10px 39px;
            display: inline-block;
            font-size: 18px;
            color: #fff;
        }

        .content-data {
            width: 580px;
            margin: auto;
            margin-left: 013px;
        }

        @media only screen and (max-width: 787px) {
            .container {
                max-width: 580px;
                width: 580px;
                padding-top: 0;
            }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                max-width: 580px;
            }

            .header table td {
                font-size: 16px;
                text-align: center;
                padding-top: 42px;
            }

            .header table {
                width: 444px;
                margin-left: 013px;
                padding-top: 40px;
            }

            .content-data {
                margin-left: 002px;
            }
        }

        @media only screen and (max-width: 480px) {
            .header {
                background: url('https://i.ibb.co/HLvrGJm/logo-fengshui.png') no-repeat top center;
                height: auto;
                color: #fff;
                width: 100%;
            text-align: center;
            margin: 0px auto;
                padding-bottom: 297px;
            }

            .content-data {
                margin-left: 0uto;
            }

            .header table {
                width: 100%;
                margin-left: 0;
            }

            .desktop {
                display: none;
            }

            .header table td {
                font-size: 25px;
                text-align: center;
                padding-left: 50px;
                padding-right: 50px;
            }
        }
    </style>
</head>

<body>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
            <td>&nbsp;</td>
            <td class="container">
                <div class="content">
                    <div class="content-wrapper">
                        <div class="content-data">
                             Hi <?php echo $nama;?>,
                            <p style="margin-bottom: 30px;">
                                Anda baru saja meminta perubahan kata sandi pada akun Anda. Untuk melakukan perubahan kata sandi, silahkan klik tombol tautan dibawah ini
                            </p>
                            <div style="width:100%; text-align: center;margin:0px auto">
                            <a style="background: green;color:white; padding:10px" href="<?php echo $link;?>" target='_blank' rel='noopener'>Reset Kata Sandi</a>
                        </div>
                          <p>
                          <p>


Tips keamanan: Ini adalah pesan email permintaan perubahan kata sandi pada akun. Jika Anda tidak merasa melakukan ini, Anda bisa mengabaikan pesan ini atau hubungi kami contact support jika Anda memiliki pertanyaan . <p>
                            <div class="footer-top text-center" style="margin-top: 60px;margin-bottom: 70px;">
                                
                            </div>
                            <div class="footer">
                                <img src="https://i.ibb.co/HLvrGJm/logo-fengshui.png" />
                                <p class="text-center">Helofengshui</p>
                            </div>
                        </div>

                    </div>
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</body>

</html>