<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url().'assets/favicon/favicon.ico' ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/fengshui/admin/css/styles.css' ?>">
    <title>Fengshui - ADMIN</title>
     <!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url().'assets/plugins/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <style>
	    #box_message { margin-top: 20px; font-size: 130% }
	    .exlcm { margin-right: 10px; }
		
	</style>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
	<script type="text/JavaScript">
        $(document).ready(function(){
	        var excl=' <div class="pull-leftc exlcm" style="width: 15px"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></div> &nbsp; ';
	        $(".btn-login").click(function() {
		        $("#login_form").submit();
	        })
	        
            $('#email').focus();
			$("#login_form").on('submit', function (e) {
                e.preventDefault();
                /*.submit(function() {*/
                
                if ($("#email").val()=="" || $("#password").val()==""){
                    $("#box_message").html(excl+'<div class="pull-leftc">&nbsp; Email atau Password harus diisi!</div><div class="clear"></div>').addClass('alert alert-danger');
                        $("#email").focus();
                    } else {
                        $('.btn-kemb').hide();
                        $('#loading').show();
						$('.btn-lgn').attr('disabled','disabled').html('<span class="glyphicon glyphicon-cog fa-spin" aria-hidden="true"></span>&nbsp; Proses Autentifikasi ...');
						$.ajax({
							type: "POST",
							url: $(this).attr('action'),
							data: $(this).serialize(),
							dataType: "json",
							success: function(msg) {
                                $('#loading').hide();
								
                                if (parseInt(msg.sign) == 406) {
                                	$("#box_message").html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'+msg.text).addClass('alert alert-danger').show();
									$("#ajaxcaptcha").empty('');
									$("#ajaxcaptcha").append().html(msg.captcha);
									$("#cicaptcha").val('');
									$('.btn-lgn').removeAttr('disabled').html('Login');
									$('.btn-kemb').show();
                                } else if (parseInt(msg.sign) == 404 || parseInt(msg.sign) == 3) {
									$("#box_message").html(excl+'<div class="pull-leftc">'+msg.teks+'</div><div class="clear"></div>').addClass('alert alert-danger').show();
									$('.btn-lgn').removeAttr('disabled').html('Login');
									$('.btn-kemb').show();
									if (msg.captcha !=null) {
									$("#ajaxcaptcha").empty('');
									$("#ajaxcaptcha").append().html(msg.captcha);
									$("#cicaptcha").val('');
									}
								} else if (parseInt(msg.sign) == 102)  window.location = '<?php echo site_url() ?>'+msg.aplikasi;
								else window.location = '<?php echo site_url() ?>'+msg.aplikasi;
                                
							},
                            error: function(x, t, m) {
                                 $('#loading').hide();
                                 $('.btn-lgn').removeAttr('disabled').html('Login');
                                if(t==="timeout") {
                                    $("#box_message").html(excl+'<div class="pull-leftc">Timeout</div><div class="clear"></div>').addClass('alert alert-danger').show();
                                } else {
                                    $("#box_message").html(excl+'<div class="pull-leftc">'+t+'</div><div class="clear"></div>').addClass('alert alert-danger').show();
                                }
                            }
						});
			
				}
				return false;
				
			});

            $('#loading').hide();   
		});
        </script>

  <?php
    $this_css='assets/css/opd.css';if(file_exists($this_css))echo reqcss($this_css)."\n";
  ?>
  </head>
 <?php 
  $ok = $this->session->flashdata('ok');
  $fail = $this->session->flashdata('fail');
  
  if (!empty($st['main_color'])) $body_manipulate ='background-color: '.$st['main_color'].';';
  ?>
<body>
    <div class="auth-container">
        <div class="text-center">
            <img src="images/logo.svg" width="90" class="img-fluid" alt="">
        </div>
        <?php 
						
					$path_inst_logo = !empty($st['pemerintah_logo']) ? FCPATH.'logo/'.$st['pemerintah_logo'] : null;
					$logo_instansi = (file_exists($path_inst_logo) and !empty($st['pemerintah_logo'])) ? base_url().'logo/'.$st['pemerintah_logo'] : base_url().'assets/logo/brand.png';
					?>
	        		<div class="logo-brand fit-logo"><img src="<?php echo base_url().'uploads/logo/logo.svg'; ?>" /></div>
	        		
        <h4 class="my-4 text-white">Admin Login</h4>
					

       		<?php echo form_open($url_login, array('name' => 'login_form ', 'id' => 'login_form'))?>
		 			<div id="box_message"></div>
				 	<?php 
					if (!empty($ok)) echo '<div class="alert alert-success"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp; '.$ok.'</div>';
					if (!empty($fail)) echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp; '.$fail.'</div>';
					?>

					<div class="form-inline-group left-i mb-3">
						<i class="icon-icon-awesome-user-circle in-left"></i>
			           <?php echo form_input('username',$this->session->flashdata('welcome'),'class="form-control" id="email"')?>
		          	</div>
			        <div class="form-inline-group left-i mb-3">
                <i class="icon-icon-awesome-lock in-left"></i>
			            <?php echo form_password('password',null,'class="form-control" id="password"')?>
			        </div>


            <button type="submit" class="btn btn-warning px-5 mt-4 btn-lgn">Login</button>
        
 			<?php echo form_close()?>
    </div>
</body>

</html>