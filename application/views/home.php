<?php 

 $st = get_stationer();
 $s = $this->session->userdata('login_state');
 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url().'assets/favicon/favicon.ico' ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/fengshui/admin/css/styles.css' ?>">
    <title>Fengshui - ADMIN</title>
  </head>

<body>
<?php 

    $on = get_role($this->session->userdata("id_pegawai"));
    $dircut = !empty($dircut) ? $dircut : $this->uri->segment(1);
    $app = @$on['aplikasi'][$dircut];
  
    $username = $this->session->userdata('username');
    $nama = $this->session->userdata('nama');
    $id_operator = $this->session->userdata('id');

    ?>
  <div class="main-container">
    <div class="sidebar">
    <a href="#" class="menu-close"> <span class="icon-icon-ionic-md-close text-16"></span></a>
    <div class="sidebar-logo">
        <i class="logo"></i>
    </div>
    <div class="sidebar-menu">
      <?php
        if ($s == "root") {
      ?>
                <ul class="sidebar-menu" style="margin-bottom:130px;">
                <?php
                $this->load->view('umum/rootnav_view');
                ?>
                </ul>
                <?php
            } else { 
                fget_nav(($this->uri->segment(1)=='referensi'?2:1),!isset($no_home),"",0,(isset($uri_menu)?$uri_menu:""));
            }
                ?>

      
    </div>
    <div class="dropdown btn-auth">
      <button class="btn btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo $nama;?> <span class="icon-fi-sr-angle-small-down arrow-menu"></span>
      </button>
      <div class="dropdown-menu dropdown-menu-right drop-arrow" aria-labelledby="dropdownMenuButton">
        <?php if($this->session->userdata('id_pegawai') == 1){ ?>
        <a class="dropdown-item" href="<?php echo site_url('/fengshui/Reset_password/index/1');?>">Reset Akun</a>

       <?php }?>
        <a class="dropdown-item" href="<?php echo site_url('/fengshui/Settings');?>">Reset No Telepon</a>
        <a class="dropdown-item" href="<?php echo site_url('/login/process_logout');?>">Logout</a>
      </div>
    </div>
  </div>
  <div class="main-content">
    <div class="header-menu">
        <a href="#" class="menu-btn">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <g id="fi-sr-menu-burger" transform="translate(0 -4)">
              <rect id="Rectangle_8858" data-name="Rectangle 8858" width="24" height="2" rx="1"
                transform="translate(0 11)" fill="#fff" />
              <rect id="Rectangle_8859" data-name="Rectangle 8859" width="24" height="2" rx="1"
                transform="translate(0 4)" fill="#fff" />
              <rect id="Rectangle_8860" data-name="Rectangle 8860" width="24" height="2" rx="1"
                transform="translate(0 18)" fill="#fff" />
            </g>
          </svg>

        </a>
        <h2 class="title mb-0"><?php echo $title;?></h2>
         <?php  
          $warning = $this->session->userdata('peringatan');
            if (!empty($warning) and count($warning) > 0) {
              echo '<div class="alert alert-danger alert-dismissable" style="margin-top: 70px">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Peringatan! </h4>';
                    foreach($warning as $w) {
                      echo '<p>'.$w.'</p>';          
                    }
                    echo '</div>'; 
                } 
                
                $ok = $this->session->flashdata('ok');
                $fail = $this->session->flashdata('fail');
                $msg = array();
                if (!empty($ok)) {
                  $msg['head'] = 'Pesan';
                  $msg['box'] = 'alert-success';
                  $msg['icon'] = 'fa-check';
                  $msg['pesan'] = $ok;
                }
                
                if (!empty($fail)) {
                  $msg['head'] = 'Peringatan';
                  $msg['box'] = 'alert-danger';
                  $msg['icon'] = 'fa-ban';
                  $msg['pesan'] = $fail;
                }
                
                
                if (!empty($msg)) {
                ?>
                <div class="alert <?php echo $msg['box'] ?> alert-dismissable" style="position: absolute;top:10px;left: 363px;">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa <?php echo $msg['icon'] ?>"></i> <?php echo $msg['head'] ?></h4><p><?php echo $msg['pesan'] ?></p></div> 
        <?php } ?>
       <!--  <ul class="nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-bell"></span>
              <span class="notif notif-count">3</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <span class="icon-fi-sr-comment"></span>
              <span class="notif message-count">3</span>
            </a>
          </li>
        </ul> -->
         
      </div>
           <?php if(@$content)$this->load->view($content);?>
    
  </div>

  <script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/popper.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/select2.full.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/jquery.dataTables.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.bootstrap4.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.rowReorder.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/dataTables.responsive.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/bootstrap-datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/ckeditor.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/fengshui/admin/js/all.js' ?>"></script>

<script>
    CKEDITOR.replace('editor1');
</script>
  <script>
    $('.datepicker').datepicker({
      format: "dd MM yyyy",
    });
  </script>
</body>

</html>