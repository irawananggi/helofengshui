<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// konstan milik sikkap

// data dasar:
$peg_dasar = '(select 
            id_pegawai,
            nip,
            nama,
            gelar_depan,
            gelar_belakang,
            mkg_tahun,
            mkg_bulan,
            tanggal_lahir,
            alamat,
            telepon,
            no_npwp, 
            id_jeniskelamin, 
            id_tempat_lahir, 
            status, 
            cpns_tmt from peg_pegawai) p';
define('ON_P', $peg_dasar);


// jabatan:
$peg_jbt = '(SELECT 
    bid.id_bidang,bid.nama_bidang,
    uni.id_unit,uni.unit,uni.aktif unit_aktif,
    jab.id_status_pegawai,jab.tmt_jabatan,jab.id_pegawai,jab.id_jabatan,jab.status, jab.tgl_pelantikan,jab.no_sk,jab.id_atasan,jab.id_atasan_atasan,
    stat.nama_status,stat.tipe,
    rja.nama_jabatan,rja.id_eselon,rja.bup, rja.id_jab_jenis,
    ess.eselon,
    pn.penetap,
    gol.golongan,
    rjj.jenis_jabatan
    FROM peg_pegawai p
    LEFT JOIN peg_jabatan jab ON jab.id_pegawai=p.id_pegawai
    LEFT JOIN ref_status_pegawai stat ON stat.id_status_pegawai=jab.id_status_pegawai 
    LEFT JOIN ref_jabatan rja ON rja.id_jabatan = jab.id_jabatan
    LEFT JOIN ref_jabatan_jenis rjj ON rjj.id_jab_jenis = rja.id_jab_jenis
    LEFT JOIN ref_eselon ess ON ess.id_eselon = rja.id_eselon
    LEFT JOIN ref_bidang bid ON bid.id_bidang = jab.id_bidang
    LEFT JOIN ref_unit uni ON uni.id_unit=bid.id_unit 
    LEFT JOIN ref_penetap pn ON pn.id_penetap=jab.id_penetap 
    LEFT JOIN ref_golru gol ON gol.id_golru=jab.id_golru 
    WHERE jab.status = 1
    GROUP BY p.id_pegawai
    ORDER BY jab.tmt_jabatan DESC) jbt';
define('ON_JBT', $peg_jbt);


// pangkat:
$peg_pkt = '(SELECT 
    pa.id_pegawai,pa.id_golru, gol.golongan, gol.pangkat, pa.no_sk, pa.tgl_sk, pa.tmt_pangkat, gje.golru_jenis, pa.status, pa.id_peg_pangkat
    FROM
    peg_pangkat pa
    LEFT JOIN ref_golru gol ON gol.id_golru=pa.id_golru
    LEFT JOIN ref_golru_jenis gje ON gje.id_golru_jenis=pa.id_golru_jenis
    WHERE pa.status = 1
    GROUP BY pa.id_pegawai
    ORDER BY pa.tmt_pangkat DESC) pkt';
define('ON_PKT', $peg_pkt);


// pensiun:
$peg_pensiun = '(SELECT
    pen.id_pegawai, rpe.nama_pensiun, pen.tmt_sk, pen.no_sk, pen.tgl_sk, rpe.id_pensiun
    FROM peg_pensiun pen
    JOIN ref_pensiun rpe ON rpe.id_pensiun = pen.id_pensiun
    LEFT JOIN peg_jabatan jab ON jab.id_peg_jabatan=pen.id_peg_jabatan
    LEFT JOIN ref_status_pegawai stat ON stat.id_status_pegawai=jab.id_status_pegawai 
    LEFT JOIN ref_bidang bid ON bid.id_bidang = jab.id_bidang
    LEFT JOIN ref_unit uni ON uni.id_unit=bid.id_unit 
    GROUP BY pen.id_pegawai
    ORDER BY pen.`tmt_sk` DESC) pens';
define('ON_PENS', $peg_pensiun);

// peg kgb:
$peg_kgb = '(SELECT 
			k.id_pegawai, k.id_golru, k.tmt_kgb, k.no_sk, k.gaji, k.mkg, gol.golongan, gol.pangkat
			FROM `peg_kgb` k
			LEFT JOIN `ref_golru` gol ON gol.id_golru=k.id_golru
			WHERE k.status =1
			GROUP BY k.id_pegawai
			ORDER BY k.tmt_kgb
			) kgb';
define('ON_KGB', $peg_kgb);

// peg penghargaan:

$peg_phg = '(SELECT
			pe.id_pegawai,pe.tanggal_peroleh,rp.id_penghargaan,rp.penghargaan, rp.berkala
		  	FROM 
			 (SELECT peng.id_peg_penghargaan AS ids,peng.tanggal_peroleh AS tmt_p,peng.id_pegawai AS id_peg
				FROM peg_penghargaan peng
				ORDER BY peng.id_pegawai,peng.tanggal_peroleh DESC) pps
			  JOIN peg_penghargaan pe ON pps.ids = pe.id_peg_penghargaan 
			  JOIN ref_penghargaan rp ON pe.id_penghargaan = rp.id_penghargaan
			  WHERE rp.berkala = 2
			  GROUP BY pe.id_pegawai,pe.tanggal_peroleh,rp.id_penghargaan) phg';
define('ON_PHG', $peg_phg);

$pangkat_awal = '(SELECT 
        pa.id_pegawai,pa.id_golru, gol.golongan, gol.pangkat, pa.tmt_pangkat, pa.mkg_tahun, pa.mkg_bulan
        FROM
        peg_pangkat pa
        LEFT JOIN ref_golru gol ON gol.id_golru=pa.id_golru
        GROUP BY pa.id_pegawai
        ORDER BY pa.tmt_pangkat ASC) pa_awal';
define('ON_PA_AWAL', $pangkat_awal);

// where aktif
$where_peg_aktif = '(
jbt.tipe =1 AND 
jbt.status =1 AND 
jbt.unit_aktif =1 AND 
pens.id_pegawai IS NULL
)';
define('WHERE_PEG_AKTIF', $where_peg_aktif);

// where non aktif:
$where_peg_nonaktif = '(
pens.id_pegawai IS NOT NULL OR 
jbt.tipe !=1 OR
jbt.unit_aktif !=1 OR
jbt.status !=1 OR
jbt.id_pegawai IS NULL OR
jbt.id_unit IS NULL
)';

define('WHERE_PEG_NONAKTIF', $where_peg_nonaktif);


// select pegawai dasar:
$select_p = 'p.nip,p.id_pegawai,p.nama,p.gelar_depan,p.gelar_belakang,p.mkg_tahun,p.mkg_bulan,p.tanggal_lahir,p.alamat,p.telepon,p.no_npwp,concat(ifnull(p.gelar_depan,"")," ",p.nama,if(((p.gelar_belakang = "") or isnull(p.gelar_belakang)),"",concat(" ",p.gelar_belakang))) AS nama_pegawai, cpns_tmt';
define('SELECT_P', $select_p);


// ./konstan milik sikkap